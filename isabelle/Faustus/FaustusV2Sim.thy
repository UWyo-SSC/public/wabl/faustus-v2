(*<*)                                                  
theory FaustusV2Sim
  imports Main Util.MList Util.SList Core.ListTools "HOL-Library.Product_Lexorder" "HOL.Product_Type" "HOL-IMP.Star" FaustusV2Semantics HOL.Relation "HOL-Library.LaTeXsugar"
begin
(*>*)

chapter \<open>Faustus Reaction Semantics\<close>

text \<open>We must first define internal and external reaction rules for Faustus contracts.
  Internal reaction rules are for contract states that do not require external interaction
  for the contract to transition. External reaction rules define how external interactions
  from parties are processed and allow the contract to transition. Like CCS we define an internal
  reaction to be the "\tau" symbol. We prefix symbols with external input information with the
  "\lambda" symbol.\<close>

text \<open>Since time passes for the contract only when transactions are processed by the blockchain we
  define our inputs as a triple of optional input information, and required lower and upper bounds
  of time. This means a deposit, choice, or notification isn't required to advance the current time
  of the contract. Timeout conditions in the contract are entered through an input that only has
  the new time associated.\<close>

type_synonym FInput = "Input option \<times> int \<times> int"

(* Tau transitions = internal_reaction freshChoiceName *)
datatype FullSymbol  = Tau ("\<tau>")
  | Lambda FInput ("\<lambda>\<^sup>_")

type_synonym FConfig = "FContract * FContext * FState * Environment * (TransactionWarning list) * (Payment list)"

type_synonym Restriction = "Input set"

type_synonym Proc = "FConfig \<times> Restriction"

type_synonym TransitionRelation = "FConfig \<Rightarrow> FullSymbol \<Rightarrow> FConfig \<Rightarrow> bool"

fun isFreshChoiceInProcess :: "ChoiceName \<Rightarrow> FConfig \<Rightarrow> bool" where
"isFreshChoiceInProcess cn (c, ctx, s, env, warnings, payments) = (isFreshChoiceInContract cn c \<and> isFreshChoiceInContext cn ctx)"

fun fixFInterval :: "(int \<times> int) \<Rightarrow> FState \<Rightarrow> (Environment \<times> FState)" where
"fixFInterval (low, high) state = (let curMinTime = minTime state in
    let newLow = max low curMinTime in
    let curInterval = (newLow, high) in
    let env = \<lparr> timeInterval = curInterval \<rparr> in
    let newState = state \<lparr> minTime := newLow \<rparr> in
    (env, newState))"

lemma fixFIntervalOnlyTimeDependent:
"minTime state1 = minTime state2 \<Longrightarrow>
    fixFInterval (lower, upper) state1 = (newTimeEnv1, newTimeState1) \<Longrightarrow>
    fixFInterval (lower, upper) state2 = (newTimeEnv2, newTimeState2) \<Longrightarrow>
    minTime newTimeState1 = minTime newTimeState2 \<and> newTimeEnv1 = newTimeEnv2"
  subgoal premises ps proof -
    from ps obtain newLower where new_lower_1: "newLower = max lower (minTime state1)" by auto
    from ps new_lower_1 have new_lower_2: "newLower = max lower (minTime state2)" by auto
    from ps(2) new_lower_1 have "newTimeState1 = state1\<lparr>minTime := newLower\<rparr>" apply auto
      by (metis old.prod.inject)
    moreover from ps(3) new_lower_2 have "newTimeState2 = state2\<lparr>minTime := newLower\<rparr>" apply auto
      by (metis old.prod.inject)
    moreover from ps new_lower_1 new_lower_2 have "newTimeEnv1 = newTimeEnv2" apply auto
      by (metis old.prod.inject)
    ultimately show ?thesis by auto
  qed
  done

inductive small_step_receive_ccs :: "FConfig \<Rightarrow> FInput \<Rightarrow> FConfig \<Rightarrow> bool" ("_ \<rightarrow>\<^sup>_ _" [55, 0, 55]) where
LambdaReceive: "\<lbrakk>
  fixFInterval (lower, upper) state = (newTimeEnv, newTimeState);
  timeInterval newTimeEnv = (realLower, realUpper);
  realLower < t;
  realUpper < t;
  inp \<turnstile> (cases, ctx, newTimeState, newTimeEnv, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC, newCtx, newState, newEnv, newWarnings, newPayments);
  i = (Some inp, lower, upper) \<rbrakk> \<Longrightarrow>
  (When cases t c, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>i (newC t c, newCtx, newState, newEnv, newWarnings, newPayments)" |
CCSTimeout: "\<lbrakk>
  fixFInterval (lower, upper) state = (newTimeEnv, newTimeState);
  timeInterval newTimeEnv = (realLower, realUpper);
  realLower \<ge> t;
  realUpper \<ge> t;
  i = (None, lower, upper) \<rbrakk> \<Longrightarrow>
  (When cases t c, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>i (c, ctx, newTimeState, newTimeEnv, warnings, payments)"

inductive_cases ReceiveE[elim]: "p1 \<rightarrow>\<^sup>\<mu> p1'"

lemma deterministicSmallStepReceive:
"f \<rightarrow>\<^sup>\<mu> f' \<Longrightarrow>
  f \<rightarrow>\<^sup>\<mu> f'' \<Longrightarrow>
  f' = f''"
proof (induction f \<mu> f' arbitrary: f'' rule: small_step_receive_ccs.induct)
  case (LambdaReceive lower upper state newTimeEnv newTimeState realLower realUpper t inp cases ctx warnings payments newC newCtx newState newEnv newWarnings newPayments i c env f'')
  show ?case using LambdaReceive(7) LambdaReceive(1-6) apply (induction "(FaustusV2AST.FContract.When cases t c, ctx, state, env, warnings, payments)" i f'' rule: small_step_receive_ccs.induct)
     apply auto
    by (metis Pair_inject deterministicCasesSemantics)+
qed auto

(*
  These receive semantics represent observable transitions in the LTS. They can be informally
  justified by creating a process that receives an input in front of the contract. That process can 
  send an specific choice input in front of the original input to transition to an inner when.
*)
inductive small_step_receive_ccs_add_choice :: "ChoiceId \<Rightarrow> FConfig \<Rightarrow> FInput \<Rightarrow> FConfig \<Rightarrow> bool" ("_ \<turnstile> _ \<rightarrow>\<^sup>_ _" [55, 0, 55]) where
NoChoiceLambdaReceive: "\<lbrakk>
  f1 \<rightarrow>\<^sup>\<mu> f2;
  \<not>((IChoice cho 0) = inp);
  \<mu> = (Some inp, lower, upper) \<rbrakk> \<Longrightarrow>
  cho \<turnstile> f1 \<rightarrow>\<^sup>\<mu> f2" |
ChoiceLambdaReceive: "\<lbrakk>
  \<not>(\<exists>f . f1 \<rightarrow>\<^sup>\<mu> f);
  f1 \<rightarrow>\<^sup>(Some (IChoice cho 0), lower, upper) f2;
  cho \<turnstile> f2 \<rightarrow>\<^sup>\<mu> f3;
  \<not>((IChoice cho 0) = inp);
  \<mu> = (Some inp, lower, upper) \<rbrakk> \<Longrightarrow>
  cho \<turnstile> f1 \<rightarrow>\<^sup>\<mu> f3" |
CCSTimeout: "\<lbrakk>
  \<mu> = (None, lower, upper);
  f1 \<rightarrow>\<^sup>\<mu> f2 \<rbrakk> \<Longrightarrow>
  freshChoice \<turnstile> f1 \<rightarrow>\<^sup>\<mu> f2"

inductive_cases ReceiveAddChoiceE[elim]: "cho \<turnstile> p1 \<rightarrow>\<^sup>\<mu> p1'"
thm ReceiveE
thm ReceiveAddChoiceE

lemma deterministicSmallStepReceiveAddChoice:
"freshChoice \<turnstile> f \<rightarrow>\<^sup>\<mu> f' \<Longrightarrow>
  freshChoice \<turnstile> f \<rightarrow>\<^sup>\<mu> f'' \<Longrightarrow>
  f' = f''"
proof (induction freshChoice f \<mu> f' arbitrary: f'' rule: small_step_receive_ccs_add_choice.induct)
  case (NoChoiceLambdaReceive f1 \<mu> f2 cho inp lower upper)
  then show ?case apply auto
    by (metis deterministicSmallStepReceive small_step_receive_ccs_add_choice.cases)
next
  case (ChoiceLambdaReceive f1 \<mu> cho lower upper f2 f3 inp f4)
  then show ?case by (smt (verit) ReceiveAddChoiceE deterministicSmallStepReceive old.prod.inject prod_cases6)
next
  case (CCSTimeout \<mu> lower upper f1 f2 freshChoice)
  then show ?case by (metis deterministicSmallStepReceive small_step_receive_ccs_add_choice.cases)
qed

(* Reactions have already been defined in small_step_reduce *)
inductive internal_reaction :: "FConfig \<Rightarrow> FConfig \<Rightarrow> bool" ("_ \<rightarrow>\<^sup>\<T> _" [55, 55]) where
SmallStepReduce: "(c, ctx, s, env, warnings, payments) \<rightarrow>\<^sub>f\<^sub>2 (newC, newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
  (c, ctx, s, env, (convertReduceWarnings warnings), payments) \<rightarrow>\<^sup>\<T> (newC, newCtx, newS, newEnv, (convertReduceWarnings newWarnings), newPayments)"

abbreviation small_step_reacts_ccs :: "FConfig \<Rightarrow> FConfig \<Rightarrow> bool" ("_ \<rightarrow>\<^sup>*\<^sup>\<T> _" [55, 55]) where
"x \<rightarrow>\<^sup>*\<^sup>\<T> y == star internal_reaction x y"

inductive_cases ReactE: "p1 \<rightarrow>\<^sup>\<T> p1'"
thm ReactE

(*
  These reaction semantics that process a notification input can be informally justified by treating this
  as creating a process that can send a notify to the contract any time and hiding the notify.
  This means that INotifies can be treated as a Tau transition 
  External parties can still notify. \<T>\<^sub>n denotes tau transitions induced by INotify.
*)
inductive small_step_react_add_notify :: "FConfig \<Rightarrow> FConfig \<Rightarrow> bool" ("_ \<rightarrow>\<^sup>\<T>\<^sup>n _" [55, 55]) where
BaseReaction: "c1 \<rightarrow>\<^sup>\<T> c2 \<Longrightarrow> c1 \<rightarrow>\<^sup>\<T>\<^sup>n c2" |
NotifyReaction: "\<lbrakk>fixFInterval (newLower, newUpper) state = (newTimeEnv, newTimeState);
  timeInterval newTimeEnv = (realLower, realUpper);
  realLower < t;
  realUpper < t;
  INotify \<turnstile> (cases, ctx, newTimeState, newTimeEnv, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC, newCtx, newState, newEnv, newWarnings, newPayments) \<rbrakk> \<Longrightarrow>
  (When cases t cont, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>\<T>\<^sup>n (newC t cont, newCtx, newState, newEnv, newWarnings, newPayments)"

inductive internal_reaction_add_choice :: "ChoiceName \<Rightarrow> FConfig \<Rightarrow> FConfig \<Rightarrow> bool" ("_ \<turnstile> _ \<rightarrow>\<^sup>\<T> _" [55, 0, 55]) where
BaseReaction: "c1 \<rightarrow>\<^sup>\<T> c2 \<Longrightarrow> choiceName \<turnstile> c1 \<rightarrow>\<^sup>\<T> c2" |
ChoiceReaction: "\<lbrakk>fixFInterval (newLower, newUpper) state = (newTimeEnv, newTimeState);
  timeInterval newTimeEnv = (realLower, realUpper);
  realLower < t;
  realUpper < t;
  (IChoice (ChoiceId freshChoiceName party) i) \<turnstile> (cases, ctx, newTimeState, newTimeEnv, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC, newCtx, newState, newEnv, newWarnings, newPayments) \<rbrakk> \<Longrightarrow>
  freshChoiceName \<turnstile> (When cases t cont, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>\<T> (newC t cont, newCtx, newState, newEnv, newWarnings, newPayments)"

inductive full_semantics :: "Restriction \<Rightarrow> TransitionRelation" ("_ \<turnstile> _ \<midarrow>\<^sup>_\<rightarrow> _" [55, 0, 55]) where
BaseInternalFull: "p1 \<rightarrow>\<^sup>\<T> p2 \<Longrightarrow> full_semantics A p1 \<tau> p2" |
BaseReceiveFull: "\<lbrakk>
  i \<notin> A;
  p1 \<rightarrow>\<^sup>(Some i, lower, upper) p2\<rbrakk> \<Longrightarrow>
  full_semantics A p1 (\<lambda>\<^sup>(Some i, lower, upper)) p2" |
RestrictedReceiveFull: "\<lbrakk>
  i \<notin> A;
  \<not>(\<exists>f . p1 \<rightarrow>\<^sup>\<mu> f);
  \<mu> = (Some i, lower, upper);
  (\<exists>j . j \<in> A \<and> p1 \<rightarrow>\<^sup>(Some j, lower, upper) p3);
  (A \<turnstile> p3 \<midarrow>\<^sup>\<lambda>\<^sup>\<mu>\<rightarrow> p4)\<rbrakk> \<Longrightarrow>
  full_semantics A p1 (\<lambda>\<^sup>\<mu>) p4" |
TimeoutFull: "\<lbrakk>p1 \<rightarrow>\<^sup>(None, lower, upper) p2\<rbrakk> \<Longrightarrow> full_semantics A p1 (\<lambda>\<^sup>(None, lower, upper)) p2"

inductive full_nd_semantics :: "Restriction \<Rightarrow> TransitionRelation" ("_ \<turnstile> _ \<midarrow>\<midarrow>\<^sup>_\<rightarrow> _" [55, 0, 55]) where
BaseInternalFullND: "p1 \<rightarrow>\<^sup>\<T> p2 \<Longrightarrow> full_nd_semantics A p1 \<tau> p2" |
BaseReceiveFullND: "\<lbrakk>
  i \<notin> A;
  p1 \<rightarrow>\<^sup>(Some i, lower, upper) p2\<rbrakk> \<Longrightarrow>
  full_nd_semantics A p1 (\<lambda>\<^sup>(Some i, lower, upper)) p2" |
RestrictedReceiveFullND: "\<lbrakk>
  i \<notin> A;
  \<mu> = (Some i, lower, upper);
  (\<exists>j . j \<in> A \<and> p1 \<rightarrow>\<^sup>(Some j, lower, upper) p3);
  (A \<turnstile> p3 \<midarrow>\<midarrow>\<^sup>\<lambda>\<^sup>\<mu>\<rightarrow> p4)\<rbrakk> \<Longrightarrow>
  full_nd_semantics A p1 (\<lambda>\<^sup>\<mu>) p4" |
TimeoutFullND: "\<lbrakk>p1 \<rightarrow>\<^sup>(None, lower, upper) p2\<rbrakk> \<Longrightarrow> full_nd_semantics A p1 (\<lambda>\<^sup>(None, lower, upper)) p2"

lemma full_nd_simulates_original:
"A \<turnstile> p1 \<midarrow>\<^sup>i\<rightarrow> p2 \<Longrightarrow>
  A \<turnstile> p1 \<midarrow>\<midarrow>\<^sup>i\<rightarrow> p2"
proof (induction "(A)" p1 i p2 rule: full_semantics.induct)
  case (BaseInternalFull p1 p2 A)
  then show ?case by (simp add: BaseInternalFullND)
next
  case (BaseReceiveFull i A p1 lower upper p2)
  then show ?case by (simp add: BaseReceiveFullND)
next
  case (RestrictedReceiveFull i A p1 \<mu> lower upper p3 p4)
  then show ?case by (simp add: RestrictedReceiveFullND)
next
  case (TimeoutFull p1 lower upper p2 A)
  then show ?case by (simp add: TimeoutFullND)
qed

lemma full_semantics_weakening:
"(A \<turnstile> p1 \<midarrow>\<^sup>\<lambda>\<^sup>\<mu>\<rightarrow> p1') \<Longrightarrow>
  \<mu> = (Some i, lower, upper) \<Longrightarrow>
  i \<notin> B \<Longrightarrow>
  ((A \<union> B) \<turnstile> p1 \<midarrow>\<^sup>\<lambda>\<^sup>\<mu>\<rightarrow> p1')"
proof (induction A p1 "\<lambda>\<^sup>\<mu>" p1' arbitrary: \<mu> i lower upper rule: full_semantics.induct)
  case (BaseReceiveFull i A p1 lower upper p2)
  then show ?case by (auto simp add: full_semantics.BaseReceiveFull)
next
  case (RestrictedReceiveFull innerI A p1 \<mu> lower' upper' p3 p4)
  moreover have "i = innerI \<and> lower = lower' \<and> upper = upper'" using RestrictedReceiveFull by auto
  moreover have "i \<notin> (A \<union> B)" using calculation by auto
  moreover obtain j where "j \<in> A \<and> p1 \<rightarrow>\<^sup>(Some j, lower', upper') p3" using calculation by auto
  moreover have "j \<in> (A \<union> B)" using calculation by auto
  ultimately show ?case using RestrictedReceiveFull(6) full_semantics.RestrictedReceiveFull[of i "A \<union> B" p1 \<mu> lower upper p3 p4] by auto
next
  case (TimeoutFull p1 lower upper p2 A)
  then show ?case by auto
qed

lemma full_semantics_union_composition_input:
"(B \<turnstile> p2 \<midarrow>\<^sup>\<lambda>\<^sup>(Some j, lower, upper)\<rightarrow> p2') \<Longrightarrow>
  (A \<union> B) \<turnstile> p2' \<midarrow>\<^sup>\<lambda>\<^sup>\<mu>\<rightarrow> p2'' \<Longrightarrow>
  j \<in> A \<Longrightarrow>
  \<nexists>f. B \<turnstile> p2 \<midarrow>\<^sup>\<lambda>\<^sup>\<mu>\<rightarrow> f \<Longrightarrow>
  \<mu> = (Some i, lower, upper) \<Longrightarrow>
  i \<notin> B \<Longrightarrow>
  (A \<union> B) \<turnstile> p2 \<midarrow>\<^sup>\<lambda>\<^sup>\<mu>\<rightarrow> p2''"
proof (induction "B" p2 "\<lambda>\<^sup>(Some j, lower, upper)" p2' rule: full_semantics.induct)
  case (BaseReceiveFull B p1 p1')
  moreover have "j \<in> A \<union> B" using calculation by auto
  moreover have "i \<notin> A \<union> B" using calculation(6,3) full_semantics.simps [of "A \<union> B" p1' "\<lambda>\<^sup>\<mu>" p2''] by auto
  moreover have "\<nexists>f. p1 \<rightarrow>\<^sup>\<mu> f" by (metis calculation(5,6,7) full_semantics.BaseReceiveFull)
  ultimately show ?case using full_semantics.RestrictedReceiveFull[of i "A \<union> B" p1 \<mu> lower upper p1' p2''] by auto
next
  case (RestrictedReceiveFull arbj B p1 lower upper p3 p4)
  moreover have "\<forall> p3' . (B \<turnstile> p3 \<midarrow>\<^sup>\<lambda>\<^sup>\<mu>\<rightarrow> p3') \<longrightarrow> (B \<turnstile> p1 \<midarrow>\<^sup>\<lambda>\<^sup>\<mu>\<rightarrow> p3')" using full_semantics.RestrictedReceiveFull[of i B p1 \<mu> lower upper p3] RestrictedReceiveFull apply auto
    by (meson BaseReceiveFull)
  moreover have "(A \<union> B) \<turnstile> p3 \<midarrow>\<^sup>\<lambda>\<^sup>\<mu>\<rightarrow> p2''" using calculation by blast
  moreover have "i \<notin> A \<union> B" using RestrictedReceiveFull(7,11,10) by (cases rule: full_semantics.cases, auto)
  ultimately show ?case apply auto
    using full_semantics.RestrictedReceiveFull[of i "A\<union>B" p1 \<mu> lower upper p3 p2''] apply auto
    by (meson BaseReceiveFull)
qed

lemma full_nd_semantics_union_composition_input:
"(B \<turnstile> p2 \<midarrow>\<midarrow>\<^sup>\<lambda>\<^sup>(Some j, lower, upper)\<rightarrow> p2') \<Longrightarrow>
  (A \<union> B) \<turnstile> p2' \<midarrow>\<midarrow>\<^sup>\<lambda>\<^sup>\<mu>\<rightarrow> p2'' \<Longrightarrow>
  j \<in> A \<Longrightarrow>
  \<mu> = (Some i, lower, upper) \<Longrightarrow>
  i \<notin> B \<Longrightarrow>
  (A \<union> B) \<turnstile> p2 \<midarrow>\<midarrow>\<^sup>\<lambda>\<^sup>\<mu>\<rightarrow> p2''"
proof (induction "B" p2 "\<lambda>\<^sup>(Some j, lower, upper)" p2' rule: full_nd_semantics.induct)
  case (BaseReceiveFullND B p1 p1')
  moreover have "j \<in> A \<union> B" using calculation by auto
  moreover have "i \<notin> A \<union> B" using calculation full_nd_semantics.simps[of "A \<union> B" p1' "\<lambda>\<^sup>\<mu>" p2''] by auto
  ultimately show ?case using full_nd_semantics.RestrictedReceiveFullND[of i "A \<union> B" \<mu> lower upper p1 p1' p2''] by auto
next
  case (RestrictedReceiveFullND arbj B lower upper p1 p3 p4)
  moreover have "\<forall> p3' . (B \<turnstile> p3 \<midarrow>\<midarrow>\<^sup>\<lambda>\<^sup>\<mu>\<rightarrow> p3') \<longrightarrow> (B \<turnstile> p1 \<midarrow>\<midarrow>\<^sup>\<lambda>\<^sup>\<mu>\<rightarrow> p3')" using full_nd_semantics.RestrictedReceiveFullND[of i B \<mu> lower upper p1 p3] RestrictedReceiveFullND by auto
  moreover have "(A \<union> B) \<turnstile> p3 \<midarrow>\<midarrow>\<^sup>\<lambda>\<^sup>\<mu>\<rightarrow> p2''" using calculation by blast
  moreover have "i \<notin> A \<union> B" using RestrictedReceiveFullND(6,8,9) by (cases rule: full_nd_semantics.cases, auto)
  ultimately show ?case using full_nd_semantics.RestrictedReceiveFullND[of i "A\<union>B" \<mu> lower upper p1 p3 p2''] by auto
qed

lemma full_semantics_react_is_equivalent_to_internal_reaction:
"p1 \<rightarrow>\<^sup>\<T> p2 \<longleftrightarrow> full_semantics A p1 \<tau> p2"
  apply (auto simp add: BaseInternalFull)
  using full_semantics.cases by blast

lemma full_nd_semantics_react_is_equivalent_to_internal_reaction:
"p1 \<rightarrow>\<^sup>\<T> p2 \<longleftrightarrow> full_nd_semantics A p1 \<tau> p2"
  apply (auto simp add: BaseInternalFullND)
  using full_nd_semantics.cases by blast

(* ADV = (a.(!a.ADV + !c.!a.ADV))
  CON = (c.a...0)
 new a,c (ADV | CON)
*)
lemma full_semantics_receive_empty_set_is_equivalent_to_base_small_step:
"(p1 \<rightarrow>\<^sup>\<mu> p2) \<longleftrightarrow> (full_semantics \<emptyset> p1 (\<lambda>\<^sup>\<mu>) p2)"
  apply auto
   apply (induction p1 \<mu> p2 rule: small_step_receive_ccs.induct, auto)
    apply (simp add: BaseReceiveFull LambdaReceive)
   apply (simp add: TimeoutFull small_step_receive_ccs.CCSTimeout)
  by (smt (verit, best) FullSymbol.distinct(1) FullSymbol.inject empty_iff full_semantics.simps)

lemma full_nd_semantics_receive_empty_set_is_equivalent_to_base_small_step:
"(p1 \<rightarrow>\<^sup>\<mu> p2) \<longleftrightarrow> (full_nd_semantics \<emptyset> p1 (\<lambda>\<^sup>\<mu>) p2)"
  apply auto
   apply (induction p1 \<mu> p2 rule: small_step_receive_ccs.induct, auto)
    apply (simp add: BaseReceiveFullND LambdaReceive)
   apply (simp add: TimeoutFullND small_step_receive_ccs.CCSTimeout)
  by (smt (verit, best) FullSymbol.distinct(1) FullSymbol.inject empty_iff full_nd_semantics.simps)

lemma full_semantics_receive_choice_input_is_equivalent_to_small_step_add_choice_forward:
"((ChoiceId cn pt) \<turnstile> p1 \<rightarrow>\<^sup>\<mu> p2) \<Longrightarrow> (full_semantics {(IChoice (ChoiceId cn pt) 0)} p1 (\<lambda>\<^sup>\<mu>) p2)"
  apply (induction "ChoiceId cn pt" "p1" "\<mu>" "p2" rule: small_step_receive_ccs_add_choice.induct)
  using BaseReceiveFull RestrictedReceiveFull TimeoutFull by (auto)

lemma full_semantics_receive_choice_input_is_equivalent_to_small_step_add_choice_backward:
"(full_semantics {(IChoice (ChoiceId cn pt) 0)} p1 (\<lambda>\<^sup>\<mu>) p2) \<Longrightarrow> ((ChoiceId cn pt) \<turnstile> p1 \<rightarrow>\<^sup>\<mu> p2)"
  apply (induction "{(IChoice (ChoiceId cn pt) 0)}" p1 "(\<lambda>\<^sup>\<mu>)" p2 rule: full_semantics.induct)
    using  NoChoiceLambdaReceive small_step_receive_ccs_add_choice.CCSTimeout ChoiceLambdaReceive by auto

lemma full_semantics_receive_choice_input_is_equivalent_to_small_step_add_choice:
"((ChoiceId cn pt) \<turnstile> p1 \<rightarrow>\<^sup>\<mu> p2) \<longleftrightarrow> (full_semantics {(IChoice (ChoiceId cn pt) 0)} p1 (\<lambda>\<^sup>\<mu>) p2)"
  by (meson full_semantics_receive_choice_input_is_equivalent_to_small_step_add_choice_backward full_semantics_receive_choice_input_is_equivalent_to_small_step_add_choice_forward)

inductive experiment_relation :: "FConfig \<Rightarrow> FInput list \<Rightarrow> FConfig \<Rightarrow> bool" ("_ \<Midarrow>_\<Rightarrow> _" [55, 0, 55]) where
BaseExperiment: "\<lbrakk>c1 \<rightarrow>\<^sup>*\<^sup>\<T> c2 \<rbrakk> \<Longrightarrow>
  c1 \<Midarrow>[]\<Rightarrow> c2" |
StepExperiment: "\<lbrakk>c1 \<rightarrow>\<^sup>*\<^sup>\<T> c2;
  c2 \<rightarrow>\<^sup>i c3;
  c3 \<rightarrow>\<^sup>*\<^sup>\<T> c4;
  c4 \<Midarrow>rest\<Rightarrow> c5 \<rbrakk> \<Longrightarrow>
   c1 \<Midarrow>(i#rest)\<Rightarrow> c5"

inductive full_nd_experiment_relation :: "Restriction \<Rightarrow> FConfig \<Rightarrow> FullSymbol list \<Rightarrow> FConfig \<Rightarrow> bool" ("_ \<turnstile> _ \<Midarrow>\<Midarrow>_\<Rightarrow> _" [55, 0, 55]) where
BaseExperimentND: "\<lbrakk>c1 \<rightarrow>\<^sup>*\<^sup>\<T> c2 \<rbrakk> \<Longrightarrow>
  A \<turnstile> c1 \<Midarrow>\<Midarrow>[]\<Rightarrow> c2" |
StepExperimentND: "\<lbrakk>c1 \<rightarrow>\<^sup>*\<^sup>\<T> c2;
  A \<turnstile> c2 \<midarrow>\<midarrow>\<^sup>\<mu>\<rightarrow> c3;
  c3 \<rightarrow>\<^sup>*\<^sup>\<T> c4;
  A \<turnstile> c4 \<Midarrow>\<Midarrow>rest\<Rightarrow> c5 \<rbrakk> \<Longrightarrow>
   A \<turnstile> c1 \<Midarrow>\<Midarrow>(\<mu>#rest)\<Rightarrow> c5"

lemma single_full_nd_receive_in_experiment:
"full_nd_semantics A p1 \<mu> p2 \<Longrightarrow>
  A \<turnstile> p1 \<Midarrow>\<Midarrow>[\<mu>]\<Rightarrow> p2"
  using BaseExperimentND StepExperimentND by blast

inductive general_experiment_relation :: "('process \<Rightarrow> 'process \<Rightarrow> bool) \<Rightarrow>
  ('process \<Rightarrow> 'label \<Rightarrow> 'process \<Rightarrow> bool) \<Rightarrow>
  'process => 'label list \<Rightarrow> 'process \<Rightarrow> bool" ("_, _ \<Turnstile> _ \<Midarrow>_\<Rightarrow> _" [55, 55, 0, 0, 55]) where
GeneralBaseExperiment: "\<lbrakk>star (general_reaction) p1 p2\<rbrakk> \<Longrightarrow>
  general_reaction, general_receive \<Turnstile> p1 \<Midarrow>[]\<Rightarrow> p2" |
GeneralStepExperiment: "\<lbrakk>star (general_reaction) p1 p2;
  general_receive p2 inp p3;
  star general_reaction p3 p4;
  general_reaction, general_receive \<Turnstile> p4 \<Midarrow>rest\<Rightarrow> p5 \<rbrakk> \<Longrightarrow>
  general_reaction, general_receive \<Turnstile> p1 \<Midarrow>(inp#rest)\<Rightarrow>  p5"

lemma single_receive_in_experiment:
"receive p1 inp p2 \<Longrightarrow>
  reaction, receive \<Turnstile> p1 \<Midarrow>[inp]\<Rightarrow> p2"
  by (meson GeneralBaseExperiment GeneralStepExperiment star.refl)

lemma faustusExperimentInGeneral:
"experiment_relation c1 inp c2 = general_experiment_relation (internal_reaction) (small_step_receive_ccs) c1 inp c2"
  apply auto
   apply (induction  c1 inp c2 rule: experiment_relation.induct, auto simp add: GeneralBaseExperiment GeneralStepExperiment)
  by (induction internal_reaction small_step_receive_ccs c1 inp c2 rule: general_experiment_relation.induct, auto simp add: BaseExperiment StepExperiment)

inductive_cases EmptyExperimentE: "c1 \<Midarrow>[]\<Rightarrow> c2"
inductive_cases GeneralEmptyExperimentE: "general_experiment_relation reaction receive [] c1 c2"
thm EmptyExperimentE

lemma emptyExperimentComposition:
"reaction, receive \<Turnstile> c1 \<Midarrow>[]\<Rightarrow> c2 \<Longrightarrow>
  reaction, receive \<Turnstile> c2 \<Midarrow>[]\<Rightarrow> c3 \<Longrightarrow>
  reaction, receive \<Turnstile> c1 \<Midarrow>[]\<Rightarrow> c3"
  by (smt (verit, best) GeneralBaseExperiment general_experiment_relation.cases list.distinct(1) star_trans)

lemma emptyNDExperimentComposition:
"A \<turnstile> c1 \<Midarrow>\<Midarrow>[]\<Rightarrow> c2 \<Longrightarrow>
  A \<turnstile> c2 \<Midarrow>\<Midarrow>[]\<Rightarrow> c3 \<Longrightarrow>
  A \<turnstile> c1 \<Midarrow>\<Midarrow>[]\<Rightarrow> c3"
  by (smt (verit) full_nd_experiment_relation.simps list.discI star_trans)

lemma reactionsPrefixNDExperiment:
"A \<turnstile> c2 \<Midarrow>\<Midarrow>e\<Rightarrow> c3 \<Longrightarrow>
  star internal_reaction c1 c2 \<Longrightarrow>
  A \<turnstile> c1 \<Midarrow>\<Midarrow>e\<Rightarrow> c3"
proof (induction A c2 e c3 arbitrary: c1 rule: full_nd_experiment_relation.induct)
  case (BaseExperimentND c1 c2 A)
  then show ?case by (meson full_nd_experiment_relation.BaseExperimentND star_trans)
next
  case (StepExperimentND c1' c2 A \<mu> c3 c4 rest c5)
  moreover have "star internal_reaction c1 c2" using calculation star_trans by metis
  ultimately show ?case using full_nd_experiment_relation.StepExperimentND by blast
qed

lemma reactionsPrefixExperiment:
"reaction, receive \<Turnstile> c2 \<Midarrow>e\<Rightarrow> c3 \<Longrightarrow>
  star reaction c1 c2 \<Longrightarrow>
  reaction, receive \<Turnstile> c1 \<Midarrow>e\<Rightarrow> c3"
proof (induction reaction receive c2 e c3 arbitrary: c1 rule: general_experiment_relation.induct)
  case (GeneralBaseExperiment c2 c3)
  then show ?case using emptyExperimentComposition general_experiment_relation.GeneralBaseExperiment by metis
next
  case (GeneralStepExperiment reaction c2 c3 receive inp c4 c5 rest c6 c1)
  moreover have "star reaction c1 c3" using calculation star_trans by metis
  ultimately show ?case using general_experiment_relation.GeneralStepExperiment by metis
qed

lemma fullNDExperimentComposition:
"A \<turnstile> c1 \<Midarrow>\<Midarrow>e1\<Rightarrow> c2 \<Longrightarrow>
  A \<turnstile> c2 \<Midarrow>\<Midarrow>e2\<Rightarrow> c3 \<Longrightarrow>
  A \<turnstile> c1 \<Midarrow>\<Midarrow>(e1 @ e2)\<Rightarrow> c3"
proof (induction A c1 e1 c2 arbitrary: c3 rule: full_nd_experiment_relation.induct)
  case (BaseExperimentND c1 c2 A)
  then show ?case proof (induction c1 c2 rule: star.induct[of internal_reaction])
    case (refl c1)
    then show ?case by auto
  next
    case (step c1 c2 c2')
    moreover have "star internal_reaction c1 c2'" using calculation by (simp add: star.step)
    ultimately show ?case using reactionsPrefixNDExperiment by blast
  qed
next
  case (StepExperimentND c1 c2 A \<mu> innerC3 c4 rest c5)
  moreover have "A \<turnstile> c4 \<Midarrow>\<Midarrow>(rest @ e2)\<Rightarrow> c3" using calculation(5,6) by auto
  moreover have "A \<turnstile> innerC3 \<Midarrow>\<Midarrow>(rest @ e2)\<Rightarrow> c3" using calculation reactionsPrefixNDExperiment by blast
  moreover have "A \<turnstile> c2 \<Midarrow>\<Midarrow>(\<mu> # rest @ e2)\<Rightarrow> c3" using calculation full_nd_experiment_relation.StepExperimentND star.refl by (metis full_nd_experiment_relation.StepExperimentND)
  ultimately show ?case using reactionsPrefixExperiment by (simp add: full_nd_experiment_relation.StepExperimentND)
qed

lemma generalExperimentComposition:
"reaction, receive \<Turnstile> c1 \<Midarrow>e1\<Rightarrow> c2 \<Longrightarrow>
  reaction, receive \<Turnstile> c2 \<Midarrow>e2\<Rightarrow> c3 \<Longrightarrow>
  reaction, receive \<Turnstile> c1 \<Midarrow>(e1 @ e2)\<Rightarrow> c3"
proof (induction reaction receive c1 e1 c2 rule: general_experiment_relation.induct)
  case (GeneralBaseExperiment reaction c1 c2 receive)
  then show ?case proof (induction c1 c2 rule: star.induct[of reaction])
    case (refl c1)
    then show ?case by auto
  next
    case (step c1 c2 c2')
    moreover have "star reaction c1 c2'" using calculation by (simp add: star.step)
    ultimately show ?case by (metis star_step1 reactionsPrefixExperiment)
  qed
next
  case (GeneralStepExperiment reaction c1 c2 receive inp innerC3 c4 rest c5)
  moreover have "general_experiment_relation reaction receive c4 (rest @ e2) c3" using calculation by auto
  moreover have "general_experiment_relation reaction receive innerC3 (rest @ e2) c3" using calculation reactionsPrefixExperiment by auto
  moreover have "general_experiment_relation reaction receive c2 (inp # rest @ e2) c3" using calculation experiment_relation.StepExperiment star.refl by (metis general_experiment_relation.GeneralStepExperiment)
  ultimately show ?case using reactionsPrefixExperiment by auto
qed

lemma choice_reaction_same_as_basic_when_fresh:
"isFreshChoiceInProcess cn p \<Longrightarrow>
  (cn \<turnstile> p \<rightarrow>\<^sup>\<T> p') = p \<rightarrow>\<^sup>\<T> p'"
  apply auto
  subgoal premises ps proof -
    show ?thesis using ps(2) ps(1) proof (induction cn p p' rule: internal_reaction_add_choice.induct)
      case (ChoiceReaction newLower newUpper state newTimeEnv newTimeState realLower realUpper t freshChoiceName party i cases ctx warnings payments newC newCtx newState newEnv newWarnings newPayments cont env)
      then show ?case by (meson isFreshChoiceInCasesImpliesIsFreshChoiceInCasesGuards freshChoiceNoMatch_cases_step_aux isFreshChoiceInContract.simps(4) isFreshChoiceInProcess.simps)
    qed auto
  qed
  subgoal premises ps proof -
    show ?thesis using ps(2) ps(1) proof (induction p p' rule: internal_reaction.induct)
      case (SmallStepReduce c ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments)
      then show ?case by (simp add: internal_reaction_add_choice.BaseReaction internal_reaction.SmallStepReduce)
    qed
  qed
  done

lemma choice_receive_same_as_basic_when_fresh:
"isFreshChoiceInProcess cn p \<Longrightarrow>
  ((ChoiceId cn pt) \<turnstile> p \<rightarrow>\<^sup>i p') = p \<rightarrow>\<^sup>i p'"
apply auto
  subgoal premises ps proof -
    show ?thesis using ps(2) ps(1) proof (induction "(ChoiceId cn pt)" p i p' rule: small_step_receive_ccs_add_choice.induct)
      case (NoChoiceLambdaReceive f1 \<mu> f2 inp lower upper)
      then show ?case by auto
    next
      case (ChoiceLambdaReceive f1 \<mu> lower upper f2 f3 inp)
      then show ?case apply auto
        apply (elim ReceiveE, auto)
        by (meson freshChoiceNoMatch_cases_step_aux isFreshChoiceInCasesImpliesIsFreshChoiceInCasesGuards)
    next
      case (CCSTimeout \<mu> lower upper f1 f2)
      then show ?case by auto
    qed
  qed
  subgoal premises ps proof -
    show ?thesis using ps(2) ps(1) proof (induction p i p' rule: small_step_receive_ccs.induct)
      case (LambdaReceive lower upper state newTimeEnv newTimeState realLower realUpper t inp cases ctx warnings payments newC newCtx newState newEnv newWarnings newPayments i c env)
      then show ?case by (metis NoChoiceLambdaReceive freshChoiceNoMatch_cases_step_aux isFreshChoiceInCasesImpliesIsFreshChoiceInCasesGuards isFreshChoiceInContract.simps(4) isFreshChoiceInProcess.simps small_step_receive_ccs.LambdaReceive)
    next
      case (CCSTimeout lower upper state newTimeEnv newTimeState realLower realUpper t i cases c ctx env warnings payments)
      then show ?case by (meson small_step_receive_ccs.CCSTimeout small_step_receive_ccs_add_choice.CCSTimeout)
    qed
  qed
  done
    

lemma basic_reaction_preserves_fresh_choice:
"isFreshChoiceInProcess cn p \<Longrightarrow>
  p \<rightarrow>\<^sup>\<T> p' \<Longrightarrow>
  isFreshChoiceInProcess cn p'"
  using freshChoicePreserved_small_step_reduce internal_reaction.simps by auto

lemma basic_reaction_star_preserves_fresh_choice:
"p \<rightarrow>\<^sup>*\<^sup>\<T> p' \<Longrightarrow>
  isFreshChoiceInProcess cn p \<Longrightarrow>
  isFreshChoiceInProcess cn p'"
  by (induction p p' rule: star.induct[of internal_reaction], auto simp add: basic_reaction_preserves_fresh_choice)

lemma choice_reaction_preserves_fresh_choice:
"isFreshChoiceInProcess cn p \<Longrightarrow>
  (internal_reaction_add_choice cn) p p' \<Longrightarrow>
  isFreshChoiceInProcess cn p'"
  using choice_reaction_same_as_basic_when_fresh freshChoicePreserved_small_step_reduce internal_reaction.simps by auto

lemma choice_reaction_star_preserves_fresh_choice:
"star (internal_reaction_add_choice cn) p p' \<Longrightarrow>
  isFreshChoiceInProcess cn p \<Longrightarrow>
  isFreshChoiceInProcess cn p'"
  by (induction p p' rule: star.induct[of "internal_reaction_add_choice cn"], auto simp add: choice_reaction_preserves_fresh_choice)

lemma fix_time_preserves_choices:
"fixFInterval (lower, upper) state = (newTimeEnv, newTimeState) \<Longrightarrow>
  choices state = choices newTimeState"
  by (cases "lower < minTime state", auto)

lemma basic_receive_preserves_fresh_choice:
"p \<rightarrow>\<^sup>i p' \<Longrightarrow>
  isFreshChoiceInProcess cn p \<Longrightarrow>
  isFreshChoiceInProcess cn p'"
proof (induction p i p' rule: small_step_receive_ccs.induct)
  case (LambdaReceive lower upper state newTimeEnv newTimeState realLower realUpper t inp cases ctx warnings payments newC newCtx newState newEnv newWarnings newPayments c env)
  then show ?case by (metis freshChoicePreserved_cases_step isFreshChoiceInContract.simps(4) isFreshChoiceInProcess.simps)
next
  case (CCSTimeout lower upper state newTimeEnv newTimeState realLower realUpper t cases c ctx env warnings payments)
  then show ?case by (metis isFreshChoiceInContract.simps(4) isFreshChoiceInProcess.simps)
qed

lemma basic_receive_input_not_fresh_choice:
"p \<rightarrow>\<^sup>\<mu> p' \<Longrightarrow>
  isFreshChoiceInProcess cn p \<Longrightarrow>
  (\<nexists>p i lower upper. \<mu> = (Some ((IChoice (ChoiceId cn p) i)), lower, upper))"
proof (induction p \<mu> p' rule: small_step_receive_ccs.induct)
  case (LambdaReceive lower upper state newTimeEnv newTimeState realLower realUpper t inp cases ctx warnings payments newC newCtx newState newEnv newWarnings newPayments i c env)
  then show ?case apply auto
    using freshChoiceMatchNoChoiceInput_cases_step isFreshChoiceInCasesImpliesIsFreshChoiceInCasesGuards by blast
qed auto

lemma choice_receive_preserves_fresh_choice:
"isFreshChoiceInProcess cn p \<Longrightarrow>
  (ChoiceId cn pt) \<turnstile> p \<rightarrow>\<^sup>i p' \<Longrightarrow>
  isFreshChoiceInProcess cn p'"
  by (simp add: basic_receive_preserves_fresh_choice choice_receive_same_as_basic_when_fresh)

lemma choice_receive_input_not_fresh_choice:
"(ChoiceId cn pt) \<turnstile> p \<rightarrow>\<^sup>\<mu> p' \<Longrightarrow>
  isFreshChoiceInProcess cn p \<Longrightarrow>
  (\<nexists>p i lower upper. \<mu> = (Some ((IChoice (ChoiceId cn p) i)), lower, upper))"
  by (simp add: basic_receive_input_not_fresh_choice choice_receive_same_as_basic_when_fresh)

lemma choice_star_reaction_same_as_basic_when_fresh:
"isFreshChoiceInProcess cn p \<Longrightarrow>
  star (internal_reaction_add_choice cn) p p' = p \<rightarrow>\<^sup>*\<^sup>\<T> p'"
  apply auto
  subgoal premises ps proof -
    show ?thesis using ps(2) ps(1) proof (induction p p' rule: star.induct[of "(internal_reaction_add_choice cn)"])
      case (step x y z)
      then show ?case by (smt (verit, ccfv_SIG) choice_reaction_same_as_basic_when_fresh freshChoicePreserved_small_step_reduce isFreshChoiceInProcess.simps internal_reaction.simps star.step)
    qed auto
  qed
  subgoal premises ps proof -
    show ?thesis using ps(2) ps(1) proof (induction p p' rule: star.induct[of "internal_reaction"])
      case (step x y z)
      then show ?case by (smt (verit, best) freshChoicePreserved_small_step_reduce isFreshChoiceInProcess.simps internal_reaction_add_choice.BaseReaction internal_reaction.simps star.step)
    qed auto
  qed
  done

lemma reaction_choice_experiment_same_as_basic_when_fresh:
"isFreshChoiceInProcess cn p \<Longrightarrow>  
  general_experiment_relation (internal_reaction_add_choice cn) small_step_receive_ccs p inps p' = p \<Midarrow>inps\<Rightarrow> p'"
  apply auto
  subgoal premises ps proof -
    show ?thesis using ps(2) ps(1) proof (induction "(internal_reaction_add_choice cn)" small_step_receive_ccs p inps p' rule: general_experiment_relation.induct)
      case (GeneralBaseExperiment p1 p2)
      then show ?case using BaseExperiment choice_star_reaction_same_as_basic_when_fresh by blast
    next
      case (GeneralStepExperiment p1 p2 inp p3 p4 rest p5)
      then show ?case by (meson StepExperiment basic_receive_preserves_fresh_choice choice_reaction_star_preserves_fresh_choice choice_star_reaction_same_as_basic_when_fresh)
    qed
  qed
  subgoal premises ps proof -
    show ?thesis using ps(2) ps(1) proof (induction p inps p' rule: experiment_relation.induct)
      case (BaseExperiment c1 c2)
      then show ?case by (simp add: GeneralBaseExperiment choice_star_reaction_same_as_basic_when_fresh)
    next
      case (StepExperiment c1 c2 inp c3 c4 rest c5)
      then show ?case by (simp add: GeneralStepExperiment basic_reaction_star_preserves_fresh_choice basic_receive_preserves_fresh_choice choice_star_reaction_same_as_basic_when_fresh)
    qed
  qed
  done

lemma receive_choice_experiment_same_as_basic_when_fresh:
"isFreshChoiceInProcess cn p \<Longrightarrow>  
  internal_reaction, (small_step_receive_ccs_add_choice (ChoiceId cn pt)) \<Turnstile> p \<Midarrow>inps\<Rightarrow> p' = (p \<Midarrow>inps\<Rightarrow> p')"
  apply auto
  subgoal premises ps proof -
    show ?thesis using ps(2) ps(1) proof (induction "internal_reaction" "small_step_receive_ccs_add_choice (ChoiceId cn pt)" p inps p' rule: general_experiment_relation.induct)
      case (GeneralBaseExperiment p1 p2)
      then show ?case using BaseExperiment choice_star_reaction_same_as_basic_when_fresh by blast
    next
      case (GeneralStepExperiment p1 p2 inp p3 p4 rest p5)
      then show ?case by (meson StepExperiment basic_reaction_star_preserves_fresh_choice choice_receive_preserves_fresh_choice choice_receive_same_as_basic_when_fresh)
    qed
  qed
  subgoal premises ps proof -
    show ?thesis using ps(2) ps(1) proof (induction p inps p' rule: experiment_relation.induct)
      case (BaseExperiment c1 c2)
      then show ?case by (simp add: GeneralBaseExperiment choice_star_reaction_same_as_basic_when_fresh)
    next
      case (StepExperiment c1 c2 inp c3 c4 rest c5)
      then show ?case by (simp add: GeneralStepExperiment basic_reaction_star_preserves_fresh_choice basic_receive_preserves_fresh_choice choice_receive_same_as_basic_when_fresh)
    qed
  qed
  done

(*
  The weak_sim and weak_sim_simple definitions are given in
  Robin Milner's book "Communicating and Mobile Systems: The \<pi>-calculus".
  They are said to be equivalent, and we show that here.
*)

(* c1 is weakly simulated by c2 if there is a binary relation from c1 to c2 where this property holds. *)
definition weak_sim :: "FConfig rel \<Rightarrow> bool" where
"weak_sim s \<longleftrightarrow> (\<forall>c1 c2 . (c1, c2) \<in> s \<longrightarrow> (\<forall>e c1' . c1 \<Midarrow>e\<Rightarrow> c1' \<longrightarrow> (\<exists>c2' . c2 \<Midarrow>e\<Rightarrow> c2' \<and> (c1', c2') \<in> s)))"

definition weak_nd_sim :: "Proc rel \<Rightarrow> bool" where
"weak_nd_sim s \<longleftrightarrow> (\<forall>c1 A1 c2 A2 . ((c1, A1), (c2, A2)) \<in> s \<longrightarrow> (\<forall>e c1' . (A1 \<turnstile> c1 \<Midarrow>\<Midarrow>e\<Rightarrow> c1') \<longrightarrow> (\<exists>c2' . (A2 \<turnstile> c2 \<Midarrow>\<Midarrow>e\<Rightarrow> c2') \<and> ((c1', A1), (c2', A2)) \<in> s)))"

definition weak_nd_sim_simple :: "Proc rel \<Rightarrow> bool" where
"weak_nd_sim_simple s \<longleftrightarrow> (\<forall>c1 A1 c2 A2 . ((c1, A1), (c2, A2)) \<in> s \<longrightarrow>
  (\<forall>c1' . (A1 \<turnstile> c1 \<midarrow>\<midarrow>\<^sup>\<tau>\<rightarrow> c1') \<longrightarrow> (\<exists>c2' . c2 \<rightarrow>\<^sup>*\<^sup>\<T> c2' \<and> ((c1', A1), (c2', A2)) \<in> s)) \<and>
  (\<forall>\<mu> c1' . (A1 \<turnstile> c1 \<midarrow>\<midarrow>\<^sup>\<mu>\<rightarrow> c1') \<longrightarrow> (\<exists>c2' . (A2 \<turnstile> c2 \<Midarrow>\<Midarrow>[\<mu>]\<Rightarrow> c2') \<and> ((c1', A1), (c2', A2)) \<in> s)))"

definition observation_nd_bisimulation :: "Proc rel \<Rightarrow> bool" where
"observation_nd_bisimulation s \<longleftrightarrow> weak_nd_sim_simple s \<and> weak_nd_sim_simple (s\<inverse>)"

definition observable_equivalence :: "Proc \<Rightarrow> Proc \<Rightarrow> bool" (infix "\<approx>" 55) where
"observable_equivalence p1 p2 \<longleftrightarrow> (\<exists>r. observation_nd_bisimulation r \<and> (p1, p2) \<in> r)"

(* TODO elaborate on type signature, and what each argument means
  (tau relation, labeled transition, and simulation relation).
  Interesting case when first and third arguments are the same relation.
  Future state weakly simulates past states. *)
definition general_weak_sim :: "('process \<Rightarrow> 'process \<Rightarrow> bool) \<Rightarrow>
  ('process \<Rightarrow> 'label \<Rightarrow> 'process \<Rightarrow> bool) \<Rightarrow>
  'process rel \<Rightarrow> bool" where
"general_weak_sim reaction receive s \<longleftrightarrow> (\<forall>c1 c2 . (c1, c2) \<in> s \<longrightarrow>
  (\<forall>e c1' . reaction, receive \<Turnstile> c1 \<Midarrow>e\<Rightarrow> c1' \<longrightarrow>
    (\<exists>c2' . reaction, receive \<Turnstile> c2 \<Midarrow>e\<Rightarrow> c2' \<and> (c1', c2') \<in> s)))"

definition weak_sim_simple :: "FConfig rel \<Rightarrow> bool" where
"weak_sim_simple s \<longleftrightarrow> (\<forall>c1 c2 . (c1, c2) \<in> s \<longrightarrow>
  (\<forall>c1' . c1 \<rightarrow>\<^sup>\<T> c1' \<longrightarrow> (\<exists>c2' . c2 \<rightarrow>\<^sup>*\<^sup>\<T> c2' \<and> (c1', c2') \<in> s)) \<and>
  (\<forall>i c1' . c1 \<rightarrow>\<^sup>i c1' \<longrightarrow> (\<exists>c2' . c2 \<Midarrow>[i]\<Rightarrow> c2' \<and> (c1', c2') \<in> s)))"

definition general_weak_sim_simple :: "('process \<Rightarrow> 'process \<Rightarrow> bool) \<Rightarrow>
  ('process \<Rightarrow> 'label \<Rightarrow> 'process \<Rightarrow> bool) \<Rightarrow>
  'process rel \<Rightarrow> bool" where
"general_weak_sim_simple reaction receive s \<longleftrightarrow> (\<forall>c1 c2 . (c1, c2) \<in> s \<longrightarrow>
  (\<forall>c1' . reaction c1 c1' \<longrightarrow> (\<exists>c2' . star reaction c2 c2' \<and> (c1', c2') \<in> s)) \<and>
  (\<forall>i c1' . receive c1 i c1' \<longrightarrow> (\<exists>c2' . reaction, receive \<Turnstile> c2 \<Midarrow>[i]\<Rightarrow> c2' \<and> (c1', c2') \<in> s)))"

definition observation_bisimulation :: "('process \<Rightarrow> 'process \<Rightarrow> bool) \<Rightarrow>
  ('process \<Rightarrow> 'label \<Rightarrow> 'process \<Rightarrow> bool) \<Rightarrow>
  'process rel \<Rightarrow> bool" where
"observation_bisimulation reaction receive s \<longleftrightarrow> general_weak_sim_simple reaction receive s \<and> general_weak_sim_simple reaction receive (s\<inverse>)"

lemma concreteWeakSimDefInGeneralDef:
"weak_sim s = general_weak_sim internal_reaction small_step_receive_ccs s"
"weak_sim_simple s = general_weak_sim_simple internal_reaction small_step_receive_ccs s"
   apply (simp_all add: weak_sim_def general_weak_sim_def weak_sim_simple_def general_weak_sim_simple_def)
  using faustusExperimentInGeneral by presburger+

lemma initialReactionInEmptyExperimentGeneral:
"reaction c1 c1' \<Longrightarrow> (reaction, receive \<Turnstile> c1 \<Midarrow>[]\<Rightarrow> c1')"
  by (simp add: GeneralBaseExperiment)

lemma simpleWeakSimulationGeneralReaction:
"star reaction c1 c2 \<Longrightarrow>
  general_weak_sim_simple reaction receive s \<Longrightarrow>
  (c1, d1) \<in> s \<Longrightarrow>
  \<exists>d2 . star reaction d1 d2 \<and> (c2, d2) \<in> s"
  apply (induction c1 c2 arbitrary: d1 rule: star.induct[of reaction])
   apply blast
  by (smt (verit, best) star_trans general_weak_sim_simple_def)

lemma simpleWeakNDSimulationGeneralReaction:
"star internal_reaction c1 c2 \<Longrightarrow>
  weak_nd_sim_simple s \<Longrightarrow>
  ((c1, A1), (d1, A2)) \<in> s \<Longrightarrow>
  \<exists>d2 . star internal_reaction d1 d2 \<and> ((c2, A1), (d2, A2)) \<in> s"
proof (induction c1 c2 arbitrary: d1 rule: star.induct[of internal_reaction])
  case (refl x)
  then show ?case by blast
next
  case (step x y z)
  moreover obtain d2' where "((y, A1), d2', A2) \<in> s" using calculation by (metis full_nd_semantics_react_is_equivalent_to_internal_reaction weak_nd_sim_simple_def)
  ultimately show ?case by (smt (z3) BaseInternalFullND star_trans weak_nd_sim_simple_def)
qed

lemma simpleWeakSimulationImpliesWeakSimulation_aux:
"reaction, receive \<Turnstile> c1 \<Midarrow>e\<Rightarrow> c1' \<Longrightarrow>
  general_weak_sim_simple reaction receive s \<Longrightarrow>
  (c1, c2) \<in> s \<Longrightarrow>
  \<exists>c2'. reaction, receive \<Turnstile> c2 \<Midarrow>e\<Rightarrow> c2' \<and> (c1', c2') \<in> s"
proof (induction reaction receive "c1" e "c1'" arbitrary: c2 rule: general_experiment_relation.induct)
  case (GeneralBaseExperiment reaction c1 c1' receive)
  then show ?case proof (induction c1 c1' arbitrary: c2 rule: star.induct[of reaction])
    case (refl x)
    then show ?case by (metis star.refl general_experiment_relation.GeneralBaseExperiment)
  next
    case (step x y z)
    moreover obtain c2y where "star reaction c2 c2y \<and> (y, c2y) \<in> s" using general_weak_sim_simple_def calculation by metis
    moreover obtain c2' where "reaction, receive \<Turnstile> c2y \<Midarrow>[]\<Rightarrow> c2' \<and> (z, c2') \<in> s" using calculation by blast
    moreover have "reaction, receive \<Turnstile> c2 \<Midarrow>[]\<Rightarrow> c2y" using calculation general_experiment_relation.GeneralBaseExperiment by metis
    moreover have "reaction, receive \<Turnstile> c2 \<Midarrow>[]\<Rightarrow> c2'" using calculation emptyExperimentComposition by metis
    ultimately show ?case by blast
  qed
next
  case (GeneralStepExperiment reaction c1 c2 receive inp c3 c4 rest c5 d1)
  moreover obtain d2 where "star reaction d1 d2 \<and> (c2, d2) \<in> s" using calculation(1,6,7) simpleWeakSimulationGeneralReaction weak_sim_simple_def by metis
  moreover obtain d3 where "general_experiment_relation reaction receive d2 [inp]  d3 \<and> (c3, d3) \<in> s" using general_weak_sim_simple_def calculation by metis
  moreover obtain d4 where "star reaction d3 d4 \<and> (c4, d4) \<in> s" using calculation(3,6,9) simpleWeakSimulationGeneralReaction weak_sim_simple_def by metis
  moreover obtain d5 where "general_experiment_relation reaction receive d4 rest d5 \<and> (c5, d5) \<in> s" using calculation by blast
  ultimately show ?case by (metis (mono_tags, opaque_lifting) append_Cons append_Nil generalExperimentComposition reactionsPrefixExperiment)
qed

lemma simpleWeakNDSimulationImpliesWeakNDSimulation_aux:
"(A1 \<turnstile> c1 \<Midarrow>\<Midarrow>e\<Rightarrow> c1') \<Longrightarrow>
  weak_nd_sim_simple s \<Longrightarrow>
  ((c1, A1), (c2, A2)) \<in> s \<Longrightarrow>
  \<exists>c2'. (A2 \<turnstile> c2 \<Midarrow>\<Midarrow>e\<Rightarrow> c2') \<and> ((c1', A1), (c2', A2)) \<in> s"
proof (induction A1 "c1" e "c1'" arbitrary: c2 rule: full_nd_experiment_relation.induct)
  case (BaseExperimentND c1 c1' A)
  then show ?case proof (induction c1 c1' arbitrary: c2 rule: star.induct[])
    case (refl x)
    then show ?case using full_nd_experiment_relation.BaseExperimentND by blast
  next
    case (step x y z)
    moreover obtain c2y where "star internal_reaction c2 c2y \<and> ((y, A), (c2y, A2)) \<in> s" using weak_nd_sim_simple_def calculation BaseInternalFullND by presburger
    moreover obtain c2' where "(A2 \<turnstile> c2y \<Midarrow>\<Midarrow>[]\<Rightarrow> c2') \<and> ((z, A), (c2', A2)) \<in> s" using calculation by blast
    moreover have "A2 \<turnstile> c2 \<Midarrow>\<Midarrow>[]\<Rightarrow> c2y" using calculation full_nd_experiment_relation.BaseExperimentND by blast
    moreover have "A2 \<turnstile> c2 \<Midarrow>\<Midarrow>[]\<Rightarrow> c2'" using calculation emptyNDExperimentComposition by metis
    ultimately show ?case by blast
  qed
next
  case (StepExperimentND c1 c1' A \<mu> c1'' c1''' rest c1'''')
  moreover obtain c2' where "star internal_reaction c2 c2' \<and> ((c1', A), (c2', A2)) \<in> s" using calculation(1,6,7) weak_nd_sim_simple_def by (meson simpleWeakNDSimulationGeneralReaction)
  moreover obtain c2'' where "(A2 \<turnstile> c2' \<Midarrow>\<Midarrow>[\<mu>]\<Rightarrow> c2'') \<and> ((c1'', A), (c2'', A2)) \<in> s" using weak_nd_sim_simple_def calculation by metis
  moreover obtain c2''' where "star internal_reaction c2'' c2''' \<and> ((c1''', A), (c2''', A2)) \<in> s" using calculation(3,6,9) simpleWeakNDSimulationGeneralReaction weak_nd_sim_simple_def by metis
  moreover obtain c2'''' where "(A2 \<turnstile> c2''' \<Midarrow>\<Midarrow>rest\<Rightarrow> c2'''') \<and> ((c1'''', A), c2'''', A2) \<in> s" using calculation by blast
  moreover have "A2 \<turnstile> c2 \<Midarrow>\<Midarrow>\<mu>#rest\<Rightarrow> c2''''" by (metis (full_types) append_Cons append_Nil calculation(10,11,8,9) fullNDExperimentComposition reactionsPrefixNDExperiment)
  ultimately show ?case by blast
qed

lemma simpleWeakSimulationImpliesWeakSimulation:
"general_weak_sim_simple reaction receive s \<longrightarrow> general_weak_sim reaction receive s"
  by (smt (verit, ccfv_SIG) simpleWeakSimulationImpliesWeakSimulation_aux general_weak_sim_def general_weak_sim_simple_def)

lemma simpleWeakNDSimulationImpliesWeakNDSimulation:
"weak_nd_sim_simple s \<longrightarrow> weak_nd_sim s"
  using simpleWeakNDSimulationImpliesWeakNDSimulation_aux weak_nd_sim_def by presburger

lemma weakSimulationImpliesSimpleWeakSimulation:
"general_weak_sim reaction receive s \<longrightarrow> general_weak_sim_simple reaction receive s"
  apply (auto simp add: general_weak_sim_def general_weak_sim_simple_def)
   apply (smt (verit, best) general_experiment_relation.cases initialReactionInEmptyExperimentGeneral neq_Nil_conv)
  by (meson GeneralBaseExperiment GeneralStepExperiment star.refl)

lemma weakNDSimulationImpliesSimpleWeakNDSimulation:
"weak_nd_sim s \<longrightarrow> weak_nd_sim_simple s"
  apply (auto simp add: weak_nd_sim_def weak_nd_sim_simple_def)
   apply (metis BaseExperimentND full_nd_experiment_relation.cases full_nd_semantics_react_is_equivalent_to_internal_reaction list.distinct(1) star_step1)
  using single_full_nd_receive_in_experiment by presburger

lemma equivalenceIsWeakSimulation:
"general_weak_sim_simple reaction receive Id"
  by (simp add: general_weak_sim_def weakSimulationImpliesSimpleWeakSimulation)

(*
  split_cases is based on the transformation required for large contracts. Split the When into multiple pieces
  https://github.com/input-output-hk/marlowe-cardano/blob/bf9eb11b91b24ee4d0495688fdaae15fe7d48d50/marlowe/best-practices.md?plain=1#L136
  https://github.com/input-output-hk/marlowe-cardano/blob/2a0db652f01c6c08b3d90b6052c4b84785811d05/marlowe-cli/cookbook/stabilized-collective-loan-failure.ipynb
*)

function split_cases :: "TokenName \<Rightarrow> ChoiceName \<Rightarrow> nat \<Rightarrow> FContract \<Rightarrow> FContract" where
"split_cases role choiceName k (When cases t cont) = (
  let
    newCases = map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) cases;
    newCont = split_cases role choiceName k cont
  in
  if (k > 1 \<and> length newCases > k)
  then (When (take k newCases @ [Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) (split_cases role choiceName k (When (drop k cases) t cont))]) t newCont)
  else (When newCases t newCont))" |
"split_cases role choiceName k (Close) = Close" |
"split_cases role choiceName k (StatementCont s c) = (StatementCont s (split_cases role choiceName k c))" |
"split_cases role choiceName k (If e c1 c2) = (If e (split_cases role choiceName k c1) (split_cases role choiceName k c2))" |
"split_cases role choiceName k (Let i v c) = (Let i v (split_cases role choiceName k c))" |
"split_cases role choiceName k (LetObservation i v c) = (LetObservation i v (split_cases role choiceName k c))" |
"split_cases role choiceName k (LetPubKey i v c) = (LetPubKey i v (split_cases role choiceName k c))" |
"split_cases role choiceName k (LetC i p b c) = (LetC i p (split_cases role choiceName k b) (split_cases role choiceName k c))" |
"split_cases role choiceName k (UseC i a) = (UseC i a)"
  by pat_completeness auto

lemma split_cases_termination_aux_1:
"FaustusV2AST.FCase.Case x1 x2 \<in> set cases \<Longrightarrow>
  FaustusV2AST.fContractSize x2 < Suc (FaustusV2AST.fContractSize cont + FaustusV2AST.fCasesSize cases)"
  apply (induction cases, auto)
  by (smt (verit, ccfv_SIG) FaustusV2AST.fCasesSize.elims add_less_imp_less_left less_trans_Suc list.discI list.inject not_add_less2 not_less_eq)

lemma split_cases_termination_aux_2:
"cases \<noteq> [] \<Longrightarrow> k > 0 \<Longrightarrow> FaustusV2AST.fCasesSize (drop k cases) < FaustusV2AST.fCasesSize cases"
proof (induction cases arbitrary: k)
  case Nil
  then show ?case by auto
next
  case (Cons a cases)
  moreover have "cases = [] \<Longrightarrow> drop k (a # cases) = cases" using calculation by auto
  moreover have "fCasesSize (a # cases) > fCasesSize cases" by (cases a, auto)
  moreover have "k = 1 \<Longrightarrow> drop k (a # cases) = cases" by auto
  moreover have "k > 1 \<and> cases \<noteq> [] \<Longrightarrow> k-1 > 0 \<and> FaustusV2AST.fCasesSize (drop (k-1) cases) < FaustusV2AST.fCasesSize cases" using calculation by auto
  ultimately show ?case apply (cases "cases", auto)
    apply (cases "k = 1", auto)
    by (metis bot_nat_0.not_eq_extremum drop_Cons' dual_order.strict_trans)
qed

termination
  apply (relation "measures [(\<lambda>x::((TokenName \<times> ChoiceName \<times> nat \<times> FContract)) . case x of 
    (t, cn, n, contract) \<Rightarrow> (fContractSize contract))]")
             apply (auto)
  using split_cases_termination_aux_1 apply presburger
  by (metis Suc_lessD drop_Nil drop_eq_Nil2 leD split_cases_termination_aux_2)

definition advP :: FParty where "advP \<equiv> Role [0 :: 8 word]"
definition rcmdk :: nat where "rcmdk \<equiv> 5"

function sc :: "ChoiceName \<Rightarrow> FContract \<Rightarrow> FContract" where
"sc cn (When cases t cont) = (
  let
    newCases = map (\<lambda>(Case g c) \<Rightarrow> (Case g (sc cn c))) cases;
    newCont = sc cn cont
  in
  if (rcmdk > 1 \<and> length newCases > rcmdk)
  then (When (take rcmdk newCases @
    [Case (ActionGuard (Choice (FChoiceId cn advP) [(0, 0)]))
      (sc cn (When (drop rcmdk cases) t cont))])
    t newCont)
  else (When newCases t newCont))" |
"sc cn (Close) = Close" |
"sc cn (StatementCont s c) = (StatementCont s (sc cn c))" |
"sc cn (If e c1 c2) = (If e (sc cn c1) (sc cn c2))" |
"sc cn (Let i v c) = (Let i v (sc cn c))" |
"sc cn (LetObservation i v c) = (LetObservation i v (sc cn c))" |
"sc cn (LetPubKey i v c) = (LetPubKey i v (sc cn c))" |
"sc cn (LetC i p b c) = (LetC i p (sc cn b) (sc cn c))" |
"sc cn (UseC i a) = (UseC i a)"
  by pat_completeness auto

termination apply (relation "measures [(\<lambda>x::((ChoiceName \<times> FContract)) . case x of 
    (cn, contract) \<Rightarrow> (fContractSize contract))]")
             apply (auto)
  using split_cases_termination_aux_1 apply presburger
  by (metis Suc_lessD drop_Nil drop_eq_Nil2 leD split_cases_termination_aux_2)

lemma sc_is_split_cases:
"sc cn c = split_cases [0] cn rcmdk c"
proof (induction cn c rule: sc.induct)
  case (1 cn cases t cont)
  moreover have "map (FaustusV2AST.FCase.case_FCase (\<lambda>g c. FaustusV2AST.FCase.Case g (sc cn c))) cases = map (FaustusV2AST.FCase.case_FCase (\<lambda>g c. FaustusV2AST.FCase.Case g (split_cases [0] cn rcmdk c))) cases" using 1(1) apply (induction cases)
    by(auto split: FCase.split FCase.split_asm)
  moreover have "sc cn cont = split_cases [0] cn rcmdk cont" using 1 by auto
  ultimately show ?case by (cases "1 < rcmdk \<and> rcmdk < length cases", auto simp add: advP_def, presburger)
qed auto

fun split_cases_context :: "TokenName \<Rightarrow> ChoiceName \<Rightarrow> nat \<Rightarrow> FContext \<Rightarrow> FContext" where
"split_cases_context role choiceName k [] = []" |
"split_cases_context role choiceName k ((ContractAbstraction (i, p, body))#rest) = (ContractAbstraction (i, p, (split_cases role choiceName k body))#(split_cases_context role choiceName k rest))" |
"split_cases_context role choiceName k (h#rest) = (h#(split_cases_context role choiceName k rest))"

lemma lookup_split_cases_context_preserved_original:
"split_cases_context role choiceName k ctx1 = ctx2 \<Longrightarrow>
  lookupContractIdAbsInformation ctx1 cid = Some (params, body, restCtx) \<Longrightarrow>
  lookupContractIdAbsInformation ctx2 cid = Some (params, split_cases role choiceName k body, split_cases_context role choiceName k restCtx)"
proof (induction ctx1 arbitrary: ctx2)
  case Nil
  then show ?case by auto
next
  case (Cons a ctx1)
  then show ?case by (cases a, auto)
qed

lemma lookup_split_cases_context_preserved_split_context:
"split_cases_context role choiceName k ctx1 = ctx2 \<Longrightarrow>
  lookupContractIdAbsInformation ctx2 cid = Some (params, body, restCtx) \<Longrightarrow>
  (\<exists>body1 restCtx1 . lookupContractIdAbsInformation ctx1 cid = Some (params, body1, restCtx1) \<and>
    split_cases role choiceName k body1 = body \<and>
    split_cases_context role choiceName k restCtx1 = restCtx)"
proof (induction ctx1 arbitrary: ctx2)
  case Nil
  then show ?case by auto
next
  case (Cons a ctx1)
  then show ?case by (cases a, auto)
qed

lemma split_cases_context_append_distribute:
"split_cases_context role choiceName k (ctx1@ctx2) = (split_cases_context role choiceName k ctx1)@(split_cases_context role choiceName k ctx2)"
proof (induction ctx1)
  case Nil
  then show ?case by auto
next
  case (Cons a ctx1)
  then show ?case by (cases a, auto)
qed

lemma split_cases_context_append_params_preserved:
"split_cases_context role choiceName k ctx1 = ctx2 \<Longrightarrow>
  split_cases_context role choiceName k (paramsToFContext params @ ctx1) = (paramsToFContext params @ ctx2)"
proof (induction ctx1 arbitrary: ctx2)
  case Nil
  moreover have "split_cases_context role choiceName k (paramsToFContext params) = paramsToFContext params" using calculation by (induction params rule: paramsToFContext.induct, auto)
  ultimately show ?case by auto
next
  case (Cons a ctx1)
  then show ?case using split_cases_context_append_distribute by force
qed

fun state_minimal_eq :: "ChoiceName \<Rightarrow> FState \<Rightarrow> FState \<Rightarrow> bool" where
"state_minimal_eq choiceName s1 s2 = (accounts s2 = accounts s1 \<and>
    boundValues s2 = boundValues s1 \<and>
    boundPubKeys s2 = boundPubKeys s1 \<and>
    minTime s2 = minTime s1 \<and>
    (\<forall>choiceNameMade choicePartyMade choiceMade .
      choiceNameMade \<noteq> choiceName \<longrightarrow>
      MList.lookup (ChoiceId choiceNameMade choicePartyMade) (choices s1) = choiceMade \<longrightarrow> 
      MList.lookup (ChoiceId choiceNameMade choicePartyMade) (choices s2) = choiceMade))"

lemma state_minimal_eq_reflexive:
"state_minimal_eq choiceName s s" by auto

lemma state_minimal_eq_commutative:
"state_minimal_eq choiceName s1 s2 \<Longrightarrow>
  state_minimal_eq choiceName s2 s1" by auto

lemma state_minimal_eq_transitive:
"state_minimal_eq choiceName s1 s2 \<Longrightarrow>
  state_minimal_eq choiceName s2 s3 \<Longrightarrow>
  state_minimal_eq choiceName s1 s3" by auto

fun split_cases_relation :: "TokenName \<Rightarrow> ChoiceName \<Rightarrow> nat \<Rightarrow> FConfig \<Rightarrow> FConfig \<Rightarrow> bool" where
"split_cases_relation role choiceName k
  (c1, ctx1, s1, env1, warnings1, payments1)
  (c2, ctx2, s2, env2, warnings2, payments2) =
    (isFreshChoiceInProcess choiceName (c1, ctx1, s1, env1, warnings1, payments1) \<and>
    c2 = split_cases role choiceName k c1 \<and>
    ctx2 = split_cases_context role choiceName k ctx1 \<and>
    payments1 = payments2 \<and>
    warnings1 = warnings2 \<and>
    env2 = env1 \<and>
    state_minimal_eq choiceName s1 s2)"

fun split_cases_relation_set :: "TokenName \<Rightarrow> ChoiceName \<Rightarrow> nat \<Rightarrow> FConfig rel" where
"split_cases_relation_set role choiceName k = 
  {((c1, ctx1, s1, env1, warnings1, payments1), (c2, ctx2, s2, env2, warnings2, payments2)) |
   c1 ctx1 s1 env1 warnings1 payments1 c2 ctx2 s2 env2 warnings2 payments2 .  
    (isFreshChoiceInProcess choiceName (c1, ctx1, s1, env1, warnings1, payments1) \<and>
    c2 = split_cases role choiceName k c1 \<and>
    ctx2 = split_cases_context role choiceName k ctx1 \<and>
    payments1 = payments2 \<and>
    warnings1 = warnings2 \<and>
    env2 = env1 \<and>
    state_minimal_eq choiceName s1 s2)}"

lemma split_cases_relation_gives_set:
"split_cases_relation role cn k p1 p2 \<longleftrightarrow> (p1, p2) \<in> (split_cases_relation_set role cn k)"
  by (cases p1; cases p2; auto)

lemma fresh_choice_state_preserves_pubkey_evaluation:
"evalFParty s1 pk = s1Res \<Longrightarrow>
  boundPubKeys s1 = boundPubKeys s2 \<Longrightarrow>
  evalFParty s2 pk = s1Res"
  by (cases pk, auto)

lemma fresh_choice_state_preserves_payee_evaluation:
"evalFPayee s1 pk = s1Res \<Longrightarrow>
  boundPubKeys s1 = boundPubKeys s2 \<Longrightarrow>
  evalFPayee s2 pk = s1Res"
  apply (cases pk, auto)
  by (metis fresh_choice_state_preserves_pubkey_evaluation)+

lemma fresh_choice_state_preserves_choiceId_evaluation:
"evalFChoiceId s1 choiceId = s1Res \<Longrightarrow>
  boundPubKeys s1 = boundPubKeys s2 \<Longrightarrow>
  evalFChoiceId s2 choiceId = s1Res"
  by (metis evalFChoiceId.elims evalFChoiceId.simps fresh_choice_state_preserves_pubkey_evaluation)

lemma fresh_choice_state_preserves_value_evaluation:
"evalFValue env s1 val = s1Res \<Longrightarrow>
  isFreshChoiceInValue choiceName val \<Longrightarrow>
  state_minimal_eq choiceName s1 s2 \<Longrightarrow>
  evalFValue env s2 val = s1Res"
"evalFObservation env s1 obs = s1ResObs \<Longrightarrow>
  isFreshChoiceInObservation choiceName obs \<Longrightarrow>
  state_minimal_eq choiceName s1 s2 \<Longrightarrow>
  evalFObservation env s2 obs = s1ResObs"
proof (induction env s1 val and env s1 obs arbitrary: s1Res and s1ResObs rule: evalFValue_evalFObservation.induct)
  case (1 env state fPubKey token)
  then show ?case apply (simp only: evalFValue.simps state_minimal_eq.simps)
    by (metis fresh_choice_state_preserves_pubkey_evaluation)
next
  case (8 env state choId)
  moreover have "evalFChoiceId state choId = evalFChoiceId s2 choId" by (metis calculation(3) state_minimal_eq.simps fresh_choice_state_preserves_choiceId_evaluation)
  moreover obtain innerChoiceName innerParty where "choId = FChoiceId innerChoiceName innerParty" using calculation state_minimal_eq.simps FChoiceId.exhaust by blast
  moreover have "innerChoiceName \<noteq> choiceName" using calculation by auto
  moreover have "(\<forall>choiceParty . lookup (ChoiceId innerChoiceName choiceParty) (choices state) = lookup (ChoiceId innerChoiceName choiceParty) (choices s2))" using calculation(3) calculation(6) by force
  moreover have "(\<forall>choiceParty . findWithDefault 0 (ChoiceId innerChoiceName choiceParty) (choices state) = findWithDefault 0 (ChoiceId innerChoiceName choiceParty) (choices s2))" using calculation(7) by auto
  ultimately show ?case apply (simp only: evalFValue.simps)
    subgoal premises ps
      using ps(1) ps(8) by (cases "evalFParty s2 innerParty", auto)
    done
next
  case (16 env state choId)
  moreover have "evalFChoiceId state choId = evalFChoiceId s2 choId" by (metis calculation(3) state_minimal_eq.simps fresh_choice_state_preserves_choiceId_evaluation)
  moreover obtain innerChoiceName innerParty where "choId = FChoiceId innerChoiceName innerParty" using calculation FChoiceId.exhaust by blast
  moreover have "innerChoiceName \<noteq> choiceName" using calculation(2) calculation(5) by auto
  moreover have "(\<forall>choiceParty . lookup (ChoiceId innerChoiceName choiceParty) (choices state) = lookup (ChoiceId innerChoiceName choiceParty) (choices s2))" using calculation(3) calculation(6) by force
  moreover have "(\<forall>choiceParty . member (ChoiceId innerChoiceName choiceParty) (choices state) = member (ChoiceId innerChoiceName choiceParty) (choices s2))" by (simp add: calculation(7))
  ultimately show ?case apply (simp only: evalFObservation.simps)
    subgoal premises ps
      using ps(1) ps(8) by (cases "evalFParty s2 innerParty", auto)
    done
qed (auto split: option.split option.split_asm)

lemma fresh_choice_state_preserves_argument_evaluation:
"evalFArguments env s1 params args = Some newS1 \<Longrightarrow>
  isFreshChoiceInArguments choiceName args \<Longrightarrow>
  state_minimal_eq choiceName s1 s2 \<Longrightarrow>
  (\<exists>newS2 . evalFArguments env s2 params args = Some newS2 \<and>
    state_minimal_eq choiceName newS1 newS2)"
proof (induction env s1 params args arbitrary: newS1 s2 rule: evalFArguments.induct)
  case (2 e s vid restParams val restArgs)
  from 2 have val_same: "evalFValue e s val = evalFValue e s2 val" by (metis fresh_choice_state_preserves_value_evaluation(1) isFreshChoiceInArguments.simps(2))
  from 2 val_same show ?case apply auto
    by (cases "evalFValue e s2 val", auto)+
next
  case (3 e s obsId restParams obs restArgs)
  moreover from calculation have "evalFObservation e s obs = evalFObservation e s2 obs" by (metis fresh_choice_state_preserves_value_evaluation(2) isFreshChoiceInArguments.simps(3))
  moreover obtain res where "evalFObservation e s2 obs = Some res" using calculation by fastforce
  moreover obtain intermediateS2 where "intermediateS2 = s2\<lparr>FState.boundValues := MList.insert obsId (if res then 1 else 0) (FState.boundValues s2)\<rparr>" by auto
  moreover obtain intermediateS1 where "intermediateS1 = s\<lparr>FState.boundValues := MList.insert obsId (if res then 1 else 0) (FState.boundValues s)\<rparr>" by auto
  moreover have "evalFArguments e intermediateS1 restParams restArgs = Some newS1" using calculation apply meson
    apply (simp_all only: evalFArguments.simps)
    by (metis option.simps(5))
  moreover have "boundPubKeys intermediateS2 = boundPubKeys intermediateS1" using calculation by (metis state_minimal_eq.simps FState.select_convs(4) FState.surjective FState.update_convs(3))
  moreover have "boundValues intermediateS2 = boundValues intermediateS1" using calculation by (metis state_minimal_eq.simps FState.select_convs(3) FState.surjective FState.update_convs(3))
  moreover have "accounts intermediateS2 = accounts intermediateS1" using calculation by (metis state_minimal_eq.simps FState.select_convs(1) FState.surjective FState.update_convs(3))
  moreover have "\<forall>choiceNameMade choicePartyMade choiceMade. choiceNameMade \<noteq> choiceName \<longrightarrow>
       lookup (ChoiceId choiceNameMade choicePartyMade) (FState.choices intermediateS1) = choiceMade \<longrightarrow>
       lookup (ChoiceId choiceNameMade choicePartyMade) (FState.choices intermediateS2) = choiceMade" using calculation by (metis state_minimal_eq.simps FState.select_convs(2) FState.surjective FState.update_convs(3))
  moreover obtain newS2 where "(evalFArguments e intermediateS2 restParams restArgs) = Some newS2" using "3.IH"[of res newS1 intermediateS2] calculation(2-) by auto
  moreover have "boundPubKeys newS1 = boundPubKeys newS2" using "3.IH"[of res newS1 intermediateS2] calculation(2-) by auto
  moreover have "boundValues newS1 = boundValues newS2"  using "3.IH"[of res newS1 intermediateS2] calculation(2-) by auto
  moreover have "accounts newS1 = accounts newS2" using "3.IH"[of res newS1 intermediateS2] calculation(2-) by auto
  moreover have "minTime newS1 = minTime newS2" using "3.IH"[of res newS1 intermediateS2] calculation(2-) by auto
  moreover have "(\<forall>lookupChoiceName lookupChoiceParty . lookupChoiceName \<noteq> choiceName \<longrightarrow> lookup (ChoiceId lookupChoiceName lookupChoiceParty) (choices newS1) = lookup (ChoiceId lookupChoiceName lookupChoiceParty) (choices newS2))" using calculation(1)[of res newS1 intermediateS2] calculation(2-) by auto
  ultimately show ?case by force
next
  case (4 e s pkId restParams pk restArgs)
  then show ?case apply (simp only: state_minimal_eq.simps)
    by (smt (verit, ccfv_threshold) FState.select_convs(1-5) FState.surjective FState.update_convs(4) evalFArguments.simps(4) fresh_choice_state_preserves_pubkey_evaluation isFreshChoiceInArguments.simps(4) option.case_eq_if option.collapse option.discI)
qed auto

lemma small_step_preserves_split_cases_relation:
"(c1, ctx1, state1, env1, reduceWarnings1, payments1) \<rightarrow>\<^sub>f\<^sub>2 (c1', ctx1', state1', env1', reduceWarnings1', payments1') \<Longrightarrow>
  split_cases_relation role choiceName k (c1, ctx1, state1, env1, warnings1, payments1) (c2, ctx2, state2, env2, warnings2, payments2) \<Longrightarrow>
  reduceWarnings1 = reduceWarnings2 \<Longrightarrow>
  (\<exists>c2' ctx2' state2' env2' reduceWarnings2' payments2' . (c2, ctx2, state2, env2, reduceWarnings2, payments2) \<rightarrow>\<^sub>f\<^sub>2 (c2', ctx2', state2', env2', reduceWarnings2', payments2') \<and>
    split_cases_relation role choiceName k (c1', ctx1', state1', env1', convertReduceWarnings reduceWarnings1', payments1') (c2', ctx2', state2', env2', convertReduceWarnings reduceWarnings2', payments2'))"
proof (induction c1 ctx1 state1 env1 reduceWarnings1 payments1 c1' ctx1' state1' env1' reduceWarnings1' payments1'
    arbitrary: c2 ctx2 state2 env2 reduceWarnings2 payments2 warnings2 warnings1
    rule: small_step_reduce_induct)
  case (CloseRefund s party token money newAccount ctx env warns payments)
  moreover have "refundOne (FState.accounts state2) = Some ((party, token, money), newAccount)" using calculation by auto
  moreover have "(Close, split_cases_context role choiceName k ctx, state2, env2, reduceWarnings2, payments2) \<rightarrow>\<^sub>f\<^sub>2
    (Close, split_cases_context role choiceName k ctx, state2\<lparr>FState.accounts := newAccount\<rparr>, env2, reduceWarnings2 @ [ReduceNoWarning], payments2 @ [Payment party (Payee.Party party) token money])" by (simp add: calculation(4))
  moreover obtain p1 p2 where "p1 = (Close, ctx, s\<lparr>FState.accounts := newAccount\<rparr>, env, convertReduceWarnings (warns @ [ReduceNoWarning]), payments @ [Payment party (Payee.Party party) token money]) \<and>
    p2 = (Close, split_cases_context role choiceName k ctx, state2\<lparr>FState.accounts := newAccount\<rparr>, env2, convertReduceWarnings (reduceWarnings2 @ [ReduceNoWarning]), payments2 @ [Payment party (Payee.Party party) token money])" by auto
  moreover have "split_cases_relation role choiceName k p1 p2" using calculation by auto
  ultimately show ?case by (metis split_cases_relation.simps)
next
  case (PayNonPositive env s val res fromAcc accId toPayee payee token cont ctx warns payments)
  moreover have "evalFValue env s val = evalFValue env state2 val" using calculation(1) calculation(5) apply auto
    by (metis (no_types, opaque_lifting) calculation(5) fresh_choice_state_preserves_value_evaluation(1) split_cases_relation.simps)
  moreover have "evalFParty s accId = evalFParty state2 accId" using calculation(3) calculation(5) apply auto
    using fresh_choice_state_preserves_pubkey_evaluation by blast
  moreover have "evalFPayee s payee = evalFPayee state2 payee" using calculation(4) calculation(5) apply auto
    using fresh_choice_state_preserves_payee_evaluation by blast
  moreover have "(StatementCont (FStatement.Pay accId payee token val) (split_cases role choiceName k cont), split_cases_context role choiceName k ctx, state2, env2,
        reduceWarnings2, payments2) \<rightarrow>\<^sub>f\<^sub>2
       (split_cases role choiceName k cont, split_cases_context role choiceName k ctx, state2, env2, reduceWarnings2 @ [ReduceNonPositivePay fromAcc toPayee token res], payments2)" using calculation by auto
  ultimately show ?case by (auto, blast)
next
  case (PayPositivePartialWithPayment env s val res fromAcc accId toPayee payee token newAccs moneyToPay payment finalAccs somePayment cont ctx warns payments)
  moreover have "evalFValue env s val = evalFValue env state2 val" using calculation(1) calculation(10) apply auto
    by (metis (no_types, lifting) calculation(10) fresh_choice_state_preserves_value_evaluation(1) split_cases_relation.simps)
  moreover have "evalFParty s accId = evalFParty state2 accId" using calculation(3) calculation(10) apply auto
    using fresh_choice_state_preserves_pubkey_evaluation by blast
  moreover have "evalFPayee s payee = evalFPayee state2 payee" using calculation(4) calculation(10) apply auto
    using fresh_choice_state_preserves_payee_evaluation by blast
  moreover have "lookup (fromAcc, token) (FState.accounts s) = lookup (fromAcc, token) (FState.accounts state2)" using calculation by auto
  moreover have "(StatementCont (FStatement.Pay accId payee token val) (split_cases role choiceName k cont), split_cases_context role choiceName k ctx, state2, env2, reduceWarnings2, payments2) \<rightarrow>\<^sub>f\<^sub>2
    (split_cases role choiceName k cont, split_cases_context role choiceName k ctx, state2\<lparr>FState.accounts := finalAccs\<rparr>, env2, reduceWarnings2 @ [ReducePartialPay fromAcc toPayee token moneyToPay res], payments2 @ [somePayment])" using calculation by auto
  moreover have "split_cases_relation role choiceName k 
    (cont, ctx, s\<lparr>FState.accounts := finalAccs\<rparr>, env, convertReduceWarnings (warns @ [ReducePartialPay fromAcc toPayee token moneyToPay res]), payments @ [somePayment])
    (split_cases role choiceName k cont, split_cases_context role choiceName k ctx, state2\<lparr>FState.accounts := finalAccs\<rparr>, env2, convertReduceWarnings (reduceWarnings2 @ [ReducePartialPay fromAcc toPayee token moneyToPay res]), payments2 @ [somePayment])" using calculation by auto
  ultimately show ?case by (metis split_cases.simps(3) split_cases_relation.simps)
next
  case (PayPositivePartialWithoutPayment env s val res fromAcc accId toPayee payee token newAccs moneyToPay payment finalAccs cont ctx warns payments)
  moreover have "evalFValue env s val = evalFValue env state2 val" using calculation(1) calculation(10) apply auto
    by (metis (no_types, lifting) calculation(10) fresh_choice_state_preserves_value_evaluation(1) split_cases_relation.simps)
  moreover have "evalFParty s accId = evalFParty state2 accId" using calculation(3) calculation(10) apply auto
    using fresh_choice_state_preserves_pubkey_evaluation by blast
  moreover have "evalFPayee s payee = evalFPayee state2 payee" using calculation(4) calculation(10) apply auto
    using fresh_choice_state_preserves_payee_evaluation by blast
  moreover have "lookup (fromAcc, token) (FState.accounts s) = lookup (fromAcc, token) (FState.accounts state2)" using calculation by auto
  moreover have "(StatementCont (FStatement.Pay accId payee token val) (split_cases role choiceName k cont), split_cases_context role choiceName k ctx, state2, env2, reduceWarnings2, payments2) \<rightarrow>\<^sub>f\<^sub>2
    (split_cases role choiceName k cont, split_cases_context role choiceName k ctx, state2\<lparr>FState.accounts := finalAccs\<rparr>, env2, reduceWarnings2 @ [ReducePartialPay fromAcc toPayee token moneyToPay res], payments2)" using calculation by auto
  moreover have "split_cases_relation role choiceName k 
    (cont, ctx, s\<lparr>FState.accounts := finalAccs\<rparr>, env, convertReduceWarnings (warns @ [ReducePartialPay fromAcc toPayee token moneyToPay res]), payments)
    (split_cases role choiceName k cont, split_cases_context role choiceName k ctx, state2\<lparr>FState.accounts := finalAccs\<rparr>, env2, convertReduceWarnings (reduceWarnings2 @ [ReducePartialPay fromAcc toPayee token moneyToPay res]), payments2)" using calculation by auto
  ultimately show ?case by (metis split_cases.simps(3) split_cases_relation.simps)
next
  case (PayPositiveFullWithPayment env s val res fromAcc accId toPayee payee token moneyInAcc newBalance newAccs payment finalAccs somePayment cont ctx warns payments)
  moreover have "evalFValue env s val = evalFValue env state2 val" using calculation(1) calculation(11) apply auto
    by (metis (no_types, lifting) calculation(11) fresh_choice_state_preserves_value_evaluation(1) split_cases_relation.simps)
  moreover have "evalFParty s accId = evalFParty state2 accId" using calculation(3) calculation(11) apply auto
    using fresh_choice_state_preserves_pubkey_evaluation by blast
  moreover have "evalFPayee s payee = evalFPayee state2 payee" using calculation(4) calculation(11) apply auto
    using fresh_choice_state_preserves_payee_evaluation by blast
  moreover have "lookup (fromAcc, token) (FState.accounts s) = lookup (fromAcc, token) (FState.accounts state2)" using calculation by auto
  moreover have "(StatementCont (FStatement.Pay accId payee token val) (split_cases role choiceName k cont), split_cases_context role choiceName k ctx, state2, env2, reduceWarnings2, payments2) \<rightarrow>\<^sub>f\<^sub>2
    (split_cases role choiceName k cont, split_cases_context role choiceName k ctx, state2\<lparr>FState.accounts := finalAccs\<rparr>, env2, reduceWarnings2 @ [ReduceNoWarning], payments2 @ [somePayment])" using calculation by auto
  moreover have "split_cases_relation role choiceName k 
    (cont, ctx, s\<lparr>FState.accounts := finalAccs\<rparr>, env, convertReduceWarnings (warns @ [ReduceNoWarning]), payments @ [somePayment])
    (split_cases role choiceName k cont, split_cases_context role choiceName k ctx, state2\<lparr>FState.accounts := finalAccs\<rparr>, env2, convertReduceWarnings (reduceWarnings2 @ [ReduceNoWarning]), payments2 @ [somePayment])" using calculation by auto
  ultimately show ?case by (metis split_cases.simps(3) split_cases_relation.simps)
next
  case (PayPositiveFullWithoutPayment env s val res fromAcc accId toPayee payee token moneyInAcc newBalance newAccs payment finalAccs cont ctx warns payments)
  moreover have "evalFValue env s val = evalFValue env state2 val" using calculation(1) calculation(11) apply auto
    by (metis (no_types, lifting) calculation(11) fresh_choice_state_preserves_value_evaluation(1) split_cases_relation.simps)
  moreover have "evalFParty s accId = evalFParty state2 accId" using calculation(3) calculation(11) apply auto
    using fresh_choice_state_preserves_pubkey_evaluation by blast
  moreover have "evalFPayee s payee = evalFPayee state2 payee" using calculation(4) calculation(11) apply auto
    using fresh_choice_state_preserves_payee_evaluation by blast
  moreover have "lookup (fromAcc, token) (FState.accounts s) = lookup (fromAcc, token) (FState.accounts state2)" using calculation by auto
  moreover have "(StatementCont (FStatement.Pay accId payee token val) (split_cases role choiceName k cont), split_cases_context role choiceName k ctx, state2, env2, reduceWarnings2, payments2) \<rightarrow>\<^sub>f\<^sub>2
    (split_cases role choiceName k cont, split_cases_context role choiceName k ctx, state2\<lparr>FState.accounts := finalAccs\<rparr>, env2, reduceWarnings2 @ [ReduceNoWarning], payments2)" using calculation by auto
  moreover have "split_cases_relation role choiceName k 
    (cont, ctx, s\<lparr>FState.accounts := finalAccs\<rparr>, env, convertReduceWarnings (warns @ [ReduceNoWarning]), payments)
    (split_cases role choiceName k cont, split_cases_context role choiceName k ctx, state2\<lparr>FState.accounts := finalAccs\<rparr>, env2, convertReduceWarnings (reduceWarnings2 @ [ReduceNoWarning]), payments2)" using calculation by auto
  ultimately show ?case by (metis split_cases.simps(3) split_cases_relation.simps)
next
  case (IfTrue env s obs cont1 cont2 ctx warns payments)
  then show ?case apply auto
    by (smt (verit) FaustusV2Semantics.small_step_reduce.IfTrue IfTrue.prems(1) fresh_choice_state_preserves_value_evaluation(2) split_cases_relation.simps)
next
  case (IfFalse env s obs cont1 cont2 ctx warns payments)
  then show ?case apply auto
    by (smt (verit, del_insts) FaustusV2Semantics.small_step_reduce.IfFalse IfFalse.prems(1) fresh_choice_state_preserves_value_evaluation(2) split_cases_relation.simps)
next
  case (WhenTimeout env startSlot endSlot timeout cases cont ctx s warns payments)
  then show ?case apply auto
    by (smt (verit, best) FaustusV2Semantics.small_step_reduce.WhenTimeout)
next
  case (LetShadow valId s oldVal env val res cont ctx warns payments)
  from LetShadow have valEQ: "evalFValue env s val = evalFValue env state2 val" apply auto
    by (metis (no_types, lifting) LetShadow.prems(1) fresh_choice_state_preserves_value_evaluation(1) split_cases_relation.simps)
  from LetShadow have valExists: "lookup valId (boundValues state2) = Some oldVal" by auto
  from LetShadow valEQ valExists have step: "(Let valId val (split_cases role choiceName k cont), split_cases_context role choiceName k ctx, state2, env, reduceWarnings2, payments2) \<rightarrow>\<^sub>f\<^sub>2
       (split_cases role choiceName k cont, FaustusV2AST.AbstractionInformation.ValueAbstraction valId # split_cases_context role choiceName k ctx, state2\<lparr>FState.boundValues := MList.insert valId res (FState.boundValues state2)\<rparr>, env, reduceWarnings2 @ [ReduceShadowing valId oldVal res], payments2)" apply auto
    using small_step_reduce.LetShadow by metis
  from step LetShadow have rel: "split_cases_relation role choiceName k (cont, FaustusV2AST.AbstractionInformation.ValueAbstraction valId # ctx, s\<lparr>FState.boundValues := MList.insert valId res (FState.boundValues s)\<rparr>, env, convertReduceWarnings (warns @ [ReduceShadowing valId oldVal res]), payments)
    (split_cases role choiceName k cont, FaustusV2AST.AbstractionInformation.ValueAbstraction valId # split_cases_context role choiceName k ctx, state2\<lparr>FState.boundValues := MList.insert valId res (FState.boundValues state2)\<rparr>, env, convertReduceWarnings (reduceWarnings2 @ [ReduceShadowing valId oldVal res]), payments2)" by auto
  from LetShadow valEQ rel step show ?case by (metis split_cases.simps(5) split_cases_relation.simps)
next
  case (ReassignValShadow valId s oldVal env val res cont ctx warns payments)
  from ReassignValShadow have valEQ: "evalFValue env s val = evalFValue env state2 val" apply auto
    by (metis (no_types, lifting) ReassignValShadow.prems(1) fresh_choice_state_preserves_value_evaluation(1) split_cases_relation.simps)
  from ReassignValShadow have valExists: "lookup valId (boundValues state2) = Some oldVal" by auto
  from ReassignValShadow valEQ valExists have step: "(StatementCont (ReassignVal valId val) (split_cases role choiceName k cont), split_cases_context role choiceName k ctx, state2, env, reduceWarnings2, payments2) \<rightarrow>\<^sub>f\<^sub>2
       (split_cases role choiceName k cont, FaustusV2AST.AbstractionInformation.ValueAbstraction valId # split_cases_context role choiceName k ctx, state2\<lparr>FState.boundValues := MList.insert valId res (FState.boundValues state2)\<rparr>, env, reduceWarnings2 @ [ReduceShadowing valId oldVal res], payments2)" apply auto
    using small_step_reduce.ReassignValShadow by metis
  from step ReassignValShadow have rel: "split_cases_relation role choiceName k (cont, FaustusV2AST.AbstractionInformation.ValueAbstraction valId # ctx, s\<lparr>FState.boundValues := MList.insert valId res (FState.boundValues s)\<rparr>, env, convertReduceWarnings (warns @ [ReduceShadowing valId oldVal res]), payments)
    (split_cases role choiceName k cont, FaustusV2AST.AbstractionInformation.ValueAbstraction valId # split_cases_context role choiceName k ctx, state2\<lparr>FState.boundValues := MList.insert valId res (FState.boundValues state2)\<rparr>, env, convertReduceWarnings (reduceWarnings2 @ [ReduceShadowing valId oldVal res]), payments2)" by auto
  from ReassignValShadow valEQ rel step show ?case by (metis split_cases.simps(3) split_cases_relation.simps)
next
  case (LetNoShadow valId s env val res cont ctx warns payments)
  from LetNoShadow have valEQ: "evalFValue env s val = evalFValue env state2 val" apply auto
    by (metis (no_types, lifting) LetNoShadow.prems(1) fresh_choice_state_preserves_value_evaluation(1) split_cases_relation.simps)
  from LetNoShadow have valExists: "lookup valId (boundValues state2) = None" by auto
  from LetNoShadow valEQ valExists have step: "(Let valId val (split_cases role choiceName k cont), split_cases_context role choiceName k ctx, state2, env, reduceWarnings2, payments2) \<rightarrow>\<^sub>f\<^sub>2
       (split_cases role choiceName k cont, FaustusV2AST.AbstractionInformation.ValueAbstraction valId # split_cases_context role choiceName k ctx, state2\<lparr>FState.boundValues := MList.insert valId res (FState.boundValues state2)\<rparr>, env, reduceWarnings2 @ [ReduceNoWarning], payments2)" apply auto
    using small_step_reduce.LetNoShadow by metis
  from step LetNoShadow have rel: "split_cases_relation role choiceName k (cont, FaustusV2AST.AbstractionInformation.ValueAbstraction valId # ctx, s\<lparr>FState.boundValues := MList.insert valId res (FState.boundValues s)\<rparr>, env, convertReduceWarnings (warns @ [ReduceNoWarning]), payments)
    (split_cases role choiceName k cont, FaustusV2AST.AbstractionInformation.ValueAbstraction valId # split_cases_context role choiceName k ctx, state2\<lparr>FState.boundValues := MList.insert valId res (FState.boundValues state2)\<rparr>, env, convertReduceWarnings (reduceWarnings2 @ [ReduceNoWarning]), payments2)" by auto
  from LetNoShadow valEQ rel step show ?case by (metis split_cases.simps(5) split_cases_relation.simps)
next
  case (ReassignValNoShadow valId s env val res cont ctx warns payments)
  from ReassignValNoShadow have valEQ: "evalFValue env s val = evalFValue env state2 val" apply auto
    by (metis (no_types, lifting) ReassignValNoShadow.prems(1) fresh_choice_state_preserves_value_evaluation(1) split_cases_relation.simps)
  from ReassignValNoShadow have valExists: "lookup valId (boundValues state2) = None" by auto
  from ReassignValNoShadow valEQ valExists have step: "(StatementCont (ReassignVal valId val) (split_cases role choiceName k cont), split_cases_context role choiceName k ctx, state2, env, reduceWarnings2, payments2) \<rightarrow>\<^sub>f\<^sub>2
       (split_cases role choiceName k cont, FaustusV2AST.AbstractionInformation.ValueAbstraction valId # split_cases_context role choiceName k ctx, state2\<lparr>FState.boundValues := MList.insert valId res (FState.boundValues state2)\<rparr>, env, reduceWarnings2 @ [ReduceNoWarning], payments2)" apply auto
    using small_step_reduce.ReassignValNoShadow by metis
  from step ReassignValNoShadow have rel: "split_cases_relation role choiceName k (cont, FaustusV2AST.AbstractionInformation.ValueAbstraction valId # ctx, s\<lparr>FState.boundValues := MList.insert valId res (FState.boundValues s)\<rparr>, env, convertReduceWarnings (warns @ [ReduceNoWarning]), payments)
    (split_cases role choiceName k cont, FaustusV2AST.AbstractionInformation.ValueAbstraction valId # split_cases_context role choiceName k ctx, state2\<lparr>FState.boundValues := MList.insert valId res (FState.boundValues state2)\<rparr>, env, convertReduceWarnings (reduceWarnings2 @ [ReduceNoWarning]), payments2)" by auto
  from ReassignValNoShadow valEQ rel step show ?case by (metis split_cases.simps(3) split_cases_relation.simps)
next
  case (LetObservation e s obs res oId c cx ws ps)
  from LetObservation have valEQ: "evalFObservation e s obs = evalFObservation e state2 obs" apply auto
    by (metis (no_types, opaque_lifting) LetObservation.prems(1) fresh_choice_state_preserves_value_evaluation(2) split_cases_relation.simps)
  from LetObservation valEQ have step: "(LetObservation oId obs (split_cases role choiceName k c), split_cases_context role choiceName k cx, state2, e, reduceWarnings2, payments2) \<rightarrow>\<^sub>f\<^sub>2
       (split_cases role choiceName k c, ObservationAbstraction oId # split_cases_context role choiceName k cx, state2\<lparr>FState.boundValues := MList.insert oId (if res then 1 else 0) (FState.boundValues state2)\<rparr>, e, reduceWarnings2 @ [ReduceNoWarning], payments2)" using small_step_reduce.LetObservation by presburger
  from step LetObservation have rel: "split_cases_relation role choiceName k (c, ObservationAbstraction oId # cx, s\<lparr>FState.boundValues := MList.insert oId (if res then 1 else 0) (FState.boundValues s)\<rparr>, e, convertReduceWarnings (ws @ [ReduceNoWarning]), ps)
    (split_cases role choiceName k c, ObservationAbstraction oId # split_cases_context role choiceName k cx, state2\<lparr>FState.boundValues := MList.insert oId (if res then 1 else 0) (FState.boundValues state2)\<rparr>, e, convertReduceWarnings (reduceWarnings2 @ [ReduceNoWarning]), ps)" by auto
  from LetObservation valEQ rel step show ?case by (metis split_cases.simps(6) split_cases_relation.simps)
next
  case (ReassignObservation e s obs res oId c cx ws ps)
  from ReassignObservation have valEQ: "evalFObservation e s obs = evalFObservation e state2 obs" apply auto
    by (metis (no_types, lifting) ReassignObservation.prems(1) fresh_choice_state_preserves_value_evaluation(2) split_cases_relation.simps)
  from ReassignObservation valEQ have step: "(StatementCont (ReassignObservation oId obs) (split_cases role choiceName k c), split_cases_context role choiceName k cx, state2, e, reduceWarnings2, payments2) \<rightarrow>\<^sub>f\<^sub>2
       (split_cases role choiceName k c, ObservationAbstraction oId # split_cases_context role choiceName k cx, state2\<lparr>FState.boundValues := MList.insert oId (if res then 1 else 0) (FState.boundValues state2)\<rparr>, e, reduceWarnings2 @ [ReduceNoWarning], payments2)" using small_step_reduce.ReassignObservation by presburger
  from step ReassignObservation have rel: "split_cases_relation role choiceName k (c, ObservationAbstraction oId # cx, s\<lparr>FState.boundValues := MList.insert oId (if res then 1 else 0) (FState.boundValues s)\<rparr>, e, convertReduceWarnings (ws @ [ReduceNoWarning]), ps)
    (split_cases role choiceName k c, ObservationAbstraction oId # split_cases_context role choiceName k cx, state2\<lparr>FState.boundValues := MList.insert oId (if res then 1 else 0) (FState.boundValues state2)\<rparr>, e, convertReduceWarnings (reduceWarnings2 @ [ReduceNoWarning]), ps)" by auto
  from ReassignObservation valEQ rel step show ?case by (metis split_cases.simps(3) split_cases_relation.simps)
next
  case (LetPubKey s pk res pId c cx e ws ps)
  from LetPubKey have valEQ: "evalFParty s pk = evalFParty state2 pk" apply auto
    using fresh_choice_state_preserves_pubkey_evaluation by presburger
  from LetPubKey valEQ have step: "(LetPubKey pId pk (split_cases role choiceName k c), split_cases_context role choiceName k cx, state2, e, reduceWarnings2, payments2) \<rightarrow>\<^sub>f\<^sub>2
       (split_cases role choiceName k c, PubKeyAbstraction pId # split_cases_context role choiceName k cx, state2\<lparr>FState.boundPubKeys := MList.insert pId res (boundPubKeys state2)\<rparr>, e, reduceWarnings2 @ [ReduceNoWarning], payments2)" using small_step_reduce.LetPubKey by presburger
  from step LetPubKey have rel: "split_cases_relation role choiceName k (c, PubKeyAbstraction pId # cx, s\<lparr>FState.boundPubKeys := MList.insert pId res (FState.boundPubKeys s)\<rparr>, e, convertReduceWarnings (ws @ [ReduceNoWarning]), ps)
    (split_cases role choiceName k c, PubKeyAbstraction pId # split_cases_context role choiceName k cx, state2\<lparr>FState.boundPubKeys := MList.insert pId res (boundPubKeys state2)\<rparr>, e, convertReduceWarnings (reduceWarnings2 @ [ReduceNoWarning]), payments2)" by auto
  from LetPubKey valEQ rel step show ?case by (metis split_cases.simps(7) split_cases_relation.simps)
next
  case (ReassignPubKey s pk res pId c cx e ws ps)
  from ReassignPubKey have valEQ: "evalFParty s pk = evalFParty state2 pk" apply auto
    using fresh_choice_state_preserves_pubkey_evaluation by presburger
  from ReassignPubKey valEQ have step: "(StatementCont (ReassignPubKey pId pk) (split_cases role choiceName k c), split_cases_context role choiceName k cx, state2, e, reduceWarnings2, payments2) \<rightarrow>\<^sub>f\<^sub>2
       (split_cases role choiceName k c, PubKeyAbstraction pId # split_cases_context role choiceName k cx, state2\<lparr>FState.boundPubKeys := MList.insert pId res (boundPubKeys state2)\<rparr>, e, reduceWarnings2 @ [ReduceNoWarning], payments2)" using small_step_reduce.ReassignPubKey by presburger
  from step ReassignPubKey have rel: "split_cases_relation role choiceName k (c, FaustusV2AST.AbstractionInformation.PubKeyAbstraction pId # cx, s\<lparr>boundPubKeys := MList.insert pId res (boundPubKeys s)\<rparr>, e, convertReduceWarnings (ws @ [ReduceNoWarning]), ps)
    (split_cases role choiceName k c, PubKeyAbstraction pId # split_cases_context role choiceName k cx, state2\<lparr>FState.boundPubKeys := MList.insert pId res (boundPubKeys state2)\<rparr>, e, convertReduceWarnings (reduceWarnings2 @ [ReduceNoWarning]), payments2)" by auto
  from ReassignPubKey valEQ rel step show ?case by (metis split_cases.simps(3) split_cases_relation.simps)
next
  case (AssertTrue env s obs cont ctx warns payments)
  then show ?case apply auto
    by (smt (verit, del_insts) AssertTrue.prems(1) FaustusV2Semantics.small_step_reduce.AssertTrue fresh_choice_state_preserves_value_evaluation(2) split_cases_relation.simps)
next
  case (AssertFalse env s obs cont ctx warns payments)
  then show ?case apply auto
    by (smt (verit, best) AssertFalse.prems(1) FaustusV2Semantics.small_step_reduce.AssertFalse fresh_choice_state_preserves_value_evaluation(2) split_cases_relation.simps)
next
  case (UseCValueArg ctx cid params bodyCont innerCtx valId restParams args env s val valRes warns payments)
  moreover from UseCValueArg(1) have ctx_split: "lookupContractIdAbsInformation (split_cases_context role choiceName k ctx) cid = Some (params, split_cases role choiceName k bodyCont, split_cases_context role choiceName k innerCtx)" by (induction ctx cid rule: lookupContractIdAbsInformation.induct, auto)
  moreover have "evalFValue env state2 val = Some valRes" using calculation fresh_choice_state_preserves_value_evaluation(1)[of env s val "Some valRes" choiceName state2] by auto
  moreover have split_step: "(UseC cid (ValueArgument val # args), split_cases_context role choiceName k ctx, state2, env, reduceWarnings2, payments2) \<rightarrow>\<^sub>f\<^sub>2 (UseC cid args, split_cases_context role choiceName k ctx, state2\<lparr>boundValues := MList.insert valId valRes (boundValues state2)\<rparr>, env, warns @ [ReduceNoWarning], payments2)" using calculation(2,5,7) ctx_split by blast
  ultimately show ?case apply auto
    by (smt (verit, best) FState.select_convs(1,2,3,4,5) FState.surjective FState.update_convs(3) split_step)
next
  case (UseCObservationArg ctx cid params bodyCont innerCtx obsId restParams args env s obs obsRes warns payments)
  moreover from UseCObservationArg(1) have ctx_split: "lookupContractIdAbsInformation (split_cases_context role choiceName k ctx) cid = Some (params, split_cases role choiceName k bodyCont, split_cases_context role choiceName k innerCtx)" by (induction ctx cid rule: lookupContractIdAbsInformation.induct, auto)
  moreover have "evalFObservation env state2 obs = Some obsRes" using calculation fresh_choice_state_preserves_value_evaluation(2)[of env s obs "Some obsRes" choiceName state2] by auto
  moreover have split_step: "(UseC cid (ObservationArgument obs # args), split_cases_context role choiceName k ctx, state2, env, reduceWarnings2, payments2) \<rightarrow>\<^sub>f\<^sub>2 (UseC cid args, split_cases_context role choiceName k ctx, state2\<lparr>boundValues := MList.insert obsId (if obsRes then 1 else 0) (boundValues state2)\<rparr>, env, warns @ [ReduceNoWarning], payments2)" using calculation(2,5,7) ctx_split by blast
  ultimately show ?case apply auto
    by (smt (verit, best) FState.select_convs(1) FState.select_convs(2) FState.select_convs(3) FState.select_convs(4) FState.select_convs(5) FState.surjective FState.update_convs(3) split_step)+
next
  case (UseCPubKeyArg ctx cid params bodyCont innerCtx pkId restParams args s pk pkRes env warns payments)
  moreover from UseCPubKeyArg(1) have ctx_split: "lookupContractIdAbsInformation (split_cases_context role choiceName k ctx) cid = Some (params, split_cases role choiceName k bodyCont, split_cases_context role choiceName k innerCtx)" by (induction ctx cid rule: lookupContractIdAbsInformation.induct, auto)
  moreover have "evalFParty state2 pk = Some pkRes" using calculation fresh_choice_state_preserves_pubkey_evaluation[of s pk "Some pkRes" state2] by auto
  moreover have split_step: "(UseC cid (PubKeyArgument pk # args), split_cases_context role choiceName k ctx, state2, env, reduceWarnings2, payments2) \<rightarrow>\<^sub>f\<^sub>2 (UseC cid args, split_cases_context role choiceName k ctx, state2\<lparr>boundPubKeys := MList.insert pkId pkRes (boundPubKeys state2)\<rparr>, env, warns @ [ReduceNoWarning], payments2)" using calculation(2,5,7) ctx_split by blast
  ultimately show ?case apply auto
    by (smt (verit) FState.select_convs(1) FState.select_convs(2) FState.select_convs(3) FState.select_convs(4) FState.select_convs(5) FState.surjective FState.update_convs(4) split_step)
next
  case (UseCFound ctx cid params bodyCont innerCtx s env warns payments bodyResCont bodyResCtx bodyResState bodyResEnv bodyResWarns bodyResPayments)
  moreover from UseCFound(1) have ctx_split: "lookupContractIdAbsInformation (split_cases_context role choiceName k ctx) cid = Some (params, split_cases role choiceName k bodyCont, split_cases_context role choiceName k innerCtx)" by (induction ctx cid rule: lookupContractIdAbsInformation.induct, auto)
  moreover have "paramsToFContext params @ split_cases_context role choiceName k innerCtx = split_cases_context role choiceName k (paramsToFContext params @ innerCtx)" by (induction params rule: paramsToFContext.induct, auto)
  moreover have "split_cases_relation role choiceName k (bodyCont, paramsToFContext params @ innerCtx, s, env, convertReduceWarnings warns, payments) (split_cases role choiceName k bodyCont, split_cases_context role choiceName k (paramsToFContext params @ innerCtx), state2, env, convertReduceWarnings reduceWarnings2, payments2)" by (metis (no_types, opaque_lifting) calculation(1,4,5) freshChoicePreserved_appendParams freshChoicePreserved_lookupContractIdAbsInformation isFreshChoiceInProcess.simps split_cases_relation.simps)
  moreover from UseCFound(2,3) obtain bodySplitResState bodySplitResWarnings where  "(split_cases role choiceName k bodyCont, split_cases_context role choiceName k (paramsToFContext params @ innerCtx), state2, env, reduceWarnings2, payments2) \<rightarrow>\<^sub>f\<^sub>2 (split_cases role choiceName k bodyResCont, split_cases_context role choiceName k bodyResCtx, bodySplitResState, bodyResEnv, bodySplitResWarnings, bodyResPayments) \<and> state_minimal_eq choiceName bodyResState bodySplitResState \<and> convertReduceWarnings bodySplitResWarnings = convertReduceWarnings bodyResWarns" by (metis calculation(5) calculation(8) split_cases_relation.simps)
  moreover have step_split: "(split_cases role choiceName k bodyCont, paramsToFContext params @ (split_cases_context role choiceName k innerCtx), state2, env, reduceWarnings2, payments2) \<rightarrow>\<^sub>f\<^sub>2 (split_cases role choiceName k bodyResCont, split_cases_context role choiceName k bodyResCtx, bodySplitResState, bodyResEnv, bodySplitResWarnings, bodyResPayments) \<and> state_minimal_eq choiceName bodyResState bodySplitResState \<and> convertReduceWarnings bodySplitResWarnings = convertReduceWarnings bodyResWarns" using calculation(7,9) by presburger
  moreover from ctx_split step_split have full_split_step: "(UseC cid [], split_cases_context role choiceName k ctx, state2, env, reduceWarnings2, payments2) \<rightarrow>\<^sub>f\<^sub>2
       (split_cases role choiceName k bodyResCont, split_cases_context role choiceName k bodyResCtx, bodySplitResState, bodyResEnv, bodySplitResWarnings, bodyResPayments)" using small_step_reduce.UseCFound[of "split_cases_context role choiceName k ctx" cid params "split_cases role choiceName k bodyCont" "split_cases_context role choiceName k innerCtx" state2 env reduceWarnings2 payments2 "(split_cases role choiceName k bodyResCont, split_cases_context role choiceName k bodyResCtx, bodySplitResState, bodyResEnv, bodySplitResWarnings, bodyResPayments)"] by auto
  moreover have "split_cases_relation role choiceName k
        (bodyResCont, bodyResCtx, bodyResState, bodyResEnv, convertReduceWarnings bodyResWarns, bodyResPayments)
        (split_cases role choiceName k bodyResCont, split_cases_context role choiceName k bodyResCtx, bodySplitResState, bodyResEnv, convertReduceWarnings bodySplitResWarnings, bodyResPayments)" using calculation by auto
  ultimately show ?case by (metis split_cases.simps(9) split_cases_relation.simps)
qed auto

lemma small_step_preserves_inverse_split_cases_relation:
"(c2, ctx2, state2, env2, reduceWarnings2, payments2) \<rightarrow>\<^sub>f\<^sub>2 (c2', ctx2', state2', env2', reduceWarnings2', payments2') \<Longrightarrow>
  split_cases_relation role choiceName k (c1, ctx1, state1, env1, warnings1, payments1) (c2, ctx2, state2, env2, warnings2, payments2) \<Longrightarrow>
  reduceWarnings1 = reduceWarnings2 \<Longrightarrow>
  (\<exists>c1' ctx1' state1' env1' reduceWarnings1' payments1' . (c1, ctx1, state1, env1, reduceWarnings1, payments1) \<rightarrow>\<^sub>f\<^sub>2 (c1', ctx1', state1', env1', reduceWarnings1', payments1') \<and>
    split_cases_relation role choiceName k (c1', ctx1', state1', env1', convertReduceWarnings reduceWarnings1', payments1') (c2', ctx2', state2', env2', convertReduceWarnings reduceWarnings2', payments2'))"
proof (induction c2 ctx2 state2 env2 reduceWarnings2 payments2 c2' ctx2' state2' env2' reduceWarnings2' payments2'
    arbitrary: c1 ctx1 state1 env1 reduceWarnings1 payments1 warnings1 warnings2
    rule: small_step_reduce_induct)
  case (CloseRefund s party token money newAccount ctx env warns payments)
  moreover have "refundOne (FState.accounts state1) = Some ((party, token, money), newAccount)" using calculation by auto
  moreover have "c1 = Close" using calculation apply (induction c1, auto)
    by (smt (verit, best) FaustusV2AST.FContract.distinct(5))
  moreover have "(Close, ctx1, state1, env1, reduceWarnings1, payments1) \<rightarrow>\<^sub>f\<^sub>2 (Close, ctx1, state1\<lparr>accounts := newAccount\<rparr>, env1, reduceWarnings1 @ [ReduceNoWarning], payments1 @ [Payment party (Payee.Party party) token money])" using calculation by auto
  moreover have "isFreshChoiceInContract choiceName Close" by auto
  moreover have "state_minimal_eq choiceName (state1\<lparr>accounts := newAccount\<rparr>) (s\<lparr>FState.accounts := newAccount\<rparr>)" using calculation by auto
  ultimately show ?case by (metis isFreshChoiceInProcess.simps split_cases_relation.simps)
next
  case (PayNonPositive env s val res fromAcc accId toPayee payee token cont ctx warns payments)
  moreover obtain innerC1 where "c1 = (StatementCont (Pay accId payee token val) innerC1) \<and> split_cases role choiceName k innerC1 = cont" using calculation apply (cases c1, auto)
    by (smt (verit, best) FaustusV2AST.FContract.distinct(19))
  moreover have "isFreshChoiceInValue choiceName val" using calculation by auto
  moreover have "evalFValue env state1 val = Some res" using calculation by (metis (no_types, opaque_lifting) calculation(5) fresh_choice_state_preserves_value_evaluation(1) split_cases_relation.simps)
  moreover have forwardStep: "(StatementCont (Pay accId payee token val) innerC1, ctx1, state1, env1, warns, payments1) \<rightarrow>\<^sub>f\<^sub>2 (innerC1, ctx1, state1, env1, (warns @ [ReduceNonPositivePay fromAcc toPayee token res]), payments)" using calculation apply auto
    by (smt (verit, best) FaustusV2Semantics.small_step_reduce.PayNonPositive fresh_choice_state_preserves_payee_evaluation fresh_choice_state_preserves_pubkey_evaluation)
  ultimately show ?case using forwardStep by (auto, blast)
next
  case (PayPositivePartialWithPayment env s val res fromAcc accId toPayee payee token newAccs moneyToPay payment finalAccs somePayment cont ctx warns payments)
  moreover obtain innerC1 where "c1 = (StatementCont (Pay accId payee token val) innerC1) \<and> split_cases role choiceName k innerC1 = cont" using calculation apply (cases c1, auto)
    by (smt (verit, best) FaustusV2AST.FContract.distinct(19))
  moreover have "isFreshChoiceInValue choiceName val" using calculation by auto
  moreover have "evalFValue env state1 val = Some res" using calculation by (metis (no_types, opaque_lifting) fresh_choice_state_preserves_value_evaluation(1) split_cases_relation.simps)
  moreover have "Some fromAcc = evalFParty state1 accId \<and>
    Some toPayee = evalFPayee state1 payee \<and>
    moneyInAccount fromAcc token (FState.accounts state1) < res \<and>
    updateMoneyInAccount fromAcc token 0 (FState.accounts state1) = newAccs \<and>
    moneyInAccount fromAcc token (FState.accounts state1) = moneyToPay" using calculation apply (auto)
     apply (metis fresh_choice_state_preserves_pubkey_evaluation)
    by (metis fresh_choice_state_preserves_payee_evaluation)
  moreover have forwardStep: "(StatementCont (Pay accId payee token val) innerC1, ctx1, state1, env1, warns, payments1) \<rightarrow>\<^sub>f\<^sub>2 (innerC1, ctx1, state1\<lparr>FState.accounts := finalAccs\<rparr>, env1, (warns @ [ReducePartialPay fromAcc toPayee token moneyToPay res]), payments @ [somePayment])" using calculation by auto
  moreover have "state_minimal_eq choiceName (state1\<lparr>FState.accounts := finalAccs\<rparr>) (s\<lparr>FState.accounts := finalAccs\<rparr>)" using calculation by auto
  ultimately show ?case by (metis freshChoicePreserved_small_step_reduce isFreshChoiceInProcess.simps split_cases_relation.simps)
next
  case (PayPositivePartialWithoutPayment env s val res fromAcc accId toPayee payee token newAccs moneyToPay payment finalAccs cont ctx warns payments)
  moreover obtain innerC1 where "c1 = (StatementCont (Pay accId payee token val) innerC1) \<and> split_cases role choiceName k innerC1 = cont" using calculation by (cases c1, auto)
  moreover have "isFreshChoiceInValue choiceName val" using calculation by auto
  moreover have "evalFValue env state1 val = Some res" using calculation by (metis (no_types, opaque_lifting) fresh_choice_state_preserves_value_evaluation(1) split_cases_relation.simps)
  moreover have "Some fromAcc = evalFParty state1 accId \<and>
    Some toPayee = evalFPayee state1 payee \<and>
    moneyInAccount fromAcc token (FState.accounts state1) < res \<and>
    updateMoneyInAccount fromAcc token 0 (FState.accounts state1) = newAccs \<and>
    moneyInAccount fromAcc token (FState.accounts state1) = moneyToPay" using calculation by auto
  moreover have forwardStep: "(StatementCont (Pay accId payee token val) innerC1, ctx1, state1, env1, warns, payments1) \<rightarrow>\<^sub>f\<^sub>2 (innerC1, ctx1, state1\<lparr>FState.accounts := finalAccs\<rparr>, env1, (warns @ [ReducePartialPay fromAcc toPayee token moneyToPay res]), payments)" using calculation by auto
  moreover have "state_minimal_eq choiceName (state1\<lparr>FState.accounts := finalAccs\<rparr>) (s\<lparr>FState.accounts := finalAccs\<rparr>)" using calculation by auto
  ultimately show ?case by (metis freshChoicePreserved_small_step_reduce isFreshChoiceInProcess.simps split_cases_relation.simps)
next
  case (PayPositiveFullWithPayment env s val res fromAcc accId toPayee payee token moneyInAcc newBalance newAccs payment finalAccs somePayment cont ctx warns payments)
  moreover obtain innerC1 where "c1 = (StatementCont (Pay accId payee token val) innerC1) \<and> split_cases role choiceName k innerC1 = cont" using calculation apply (cases c1, auto)
    by (smt (verit, best) FaustusV2AST.FContract.distinct(19))
  moreover have "isFreshChoiceInValue choiceName val" using calculation by auto
  moreover have "evalFValue env state1 val = Some res" using calculation by (metis (no_types, opaque_lifting) fresh_choice_state_preserves_value_evaluation(1) split_cases_relation.simps)
  moreover have "Some fromAcc = evalFParty state1 accId \<and>
    Some toPayee = evalFPayee state1 payee \<and>
    moneyInAccount fromAcc token (FState.accounts state1) \<ge> res \<and>
    updateMoneyInAccount fromAcc token newBalance (FState.accounts state1) = newAccs \<and>
    moneyInAccount fromAcc token (FState.accounts state1) = moneyInAcc" using calculation apply (auto)
       using fresh_choice_state_preserves_pubkey_evaluation fresh_choice_state_preserves_payee_evaluation by metis+
  moreover have forwardStep: "(StatementCont (Pay accId payee token val) innerC1, ctx1, state1, env1, warns, payments1) \<rightarrow>\<^sub>f\<^sub>2 (innerC1, ctx1, state1\<lparr>FState.accounts := finalAccs\<rparr>, env1, (warns @ [ReduceNoWarning]), payments @ [somePayment])" using calculation by auto
  moreover have "state_minimal_eq choiceName (state1\<lparr>FState.accounts := finalAccs\<rparr>) (s\<lparr>FState.accounts := finalAccs\<rparr>)" using calculation by auto
  ultimately show ?case by (metis freshChoicePreserved_small_step_reduce isFreshChoiceInProcess.simps split_cases_relation.simps)
next
  case (PayPositiveFullWithoutPayment env s val res fromAcc accId toPayee payee token moneyInAcc newBalance newAccs payment finalAccs cont ctx warns payments)
  moreover obtain innerC1 where "c1 = (StatementCont (Pay accId payee token val) innerC1) \<and> split_cases role choiceName k innerC1 = cont" using calculation by (cases c1, auto)
  moreover have "isFreshChoiceInValue choiceName val" using calculation by auto
  moreover have "evalFValue env state1 val = Some res" using calculation by (metis (no_types, opaque_lifting) fresh_choice_state_preserves_value_evaluation(1) split_cases_relation.simps)
  moreover have "Some fromAcc = evalFParty state1 accId \<and>
    Some toPayee = evalFPayee state1 payee \<and>
    moneyInAccount fromAcc token (FState.accounts state1) \<ge> res \<and>
    updateMoneyInAccount fromAcc token newBalance (FState.accounts state1) = newAccs \<and>
    moneyInAccount fromAcc token (FState.accounts state1) = moneyInAcc" using calculation by auto
  moreover have forwardStep: "(StatementCont (Pay accId payee token val) innerC1, ctx1, state1, env1, warns, payments1) \<rightarrow>\<^sub>f\<^sub>2 (innerC1, ctx1, state1\<lparr>FState.accounts := finalAccs\<rparr>, env1, (warns @ [ReduceNoWarning]), payments)" using calculation by auto
  moreover have "state_minimal_eq choiceName (state1\<lparr>FState.accounts := finalAccs\<rparr>) (s\<lparr>FState.accounts := finalAccs\<rparr>)" using calculation by auto
  ultimately show ?case by (metis freshChoicePreserved_small_step_reduce isFreshChoiceInProcess.simps split_cases_relation.simps)
next
  case (IfTrue env s obs cont1 cont2 ctx warns payments)
  moreover obtain innerTrueCont innerFalseCont where "c1 = (If obs innerTrueCont innerFalseCont) \<and> split_cases role choiceName k innerTrueCont = cont1 \<and> split_cases role choiceName k innerFalseCont = cont2" using calculation(2) apply (cases c1, auto)
    by (smt (verit) FaustusV2AST.FContract.distinct(31))
  moreover have "isFreshChoiceInObservation choiceName obs" using calculation by auto
  moreover have "evalFObservation env state1 obs = Some True" using calculation by (metis (no_types, opaque_lifting) fresh_choice_state_preserves_value_evaluation(2) split_cases_relation.simps)
  moreover have forwardStep: "(If obs innerTrueCont innerFalseCont, ctx1, state1, env1, warns, payments1) \<rightarrow>\<^sub>f\<^sub>2 (innerTrueCont, ctx1, state1, env1, (warns @ [ReduceNoWarning]), payments)" using calculation by auto
  ultimately show ?case by (metis freshChoicePreserved_small_step_reduce isFreshChoiceInProcess.simps split_cases_relation.simps)
next
  case (IfFalse env s obs cont1 cont2 ctx warns payments)
  moreover obtain innerTrueCont innerFalseCont where "c1 = (If obs innerTrueCont innerFalseCont) \<and> split_cases role choiceName k innerTrueCont = cont1 \<and> split_cases role choiceName k innerFalseCont = cont2" using calculation(2) apply (cases c1, auto)
    by (smt (verit) FaustusV2AST.FContract.distinct(31))
  moreover have "isFreshChoiceInObservation choiceName obs" using calculation by auto
  moreover have "evalFObservation env state1 obs = Some False" using calculation by (metis (no_types, opaque_lifting) fresh_choice_state_preserves_value_evaluation(2) split_cases_relation.simps)
  moreover have forwardStep: "(If obs innerTrueCont innerFalseCont, ctx1, state1, env1, warns, payments1) \<rightarrow>\<^sub>f\<^sub>2 (innerFalseCont, ctx1, state1, env1, (warns @ [ReduceNoWarning]), payments)" using calculation by auto
  ultimately show ?case by (metis freshChoicePreserved_small_step_reduce isFreshChoiceInProcess.simps split_cases_relation.simps)
next
  case (WhenTimeout env startSlot endSlot timeout cases cont ctx s warns payments)
  moreover obtain innerCases innerTCont where "c1 = (When innerCases timeout innerTCont) \<and> split_cases role choiceName k innerTCont = cont" using calculation apply (cases c1, auto)
    by (smt (verit, best) FaustusV2AST.FContract.inject(3))
  moreover have "timeInterval env1 = (startSlot, endSlot)" using calculation by auto
  moreover have forwardStep: "(When innerCases timeout innerTCont, ctx1, state1, env1, warns, payments1) \<rightarrow>\<^sub>f\<^sub>2 (innerTCont, ctx1, state1, env1, (warns @ [ReduceNoWarning]), payments)" using calculation by auto
  ultimately show ?case by (metis freshChoicePreserved_small_step_reduce isFreshChoiceInProcess.simps split_cases_relation.simps)
next
  case (LetShadow valId s oldVal env val res cont ctx warns payments)
  moreover obtain innerCont where "c1 = (Let valId val innerCont) \<and> split_cases role choiceName k innerCont = cont" using calculation apply (cases c1, auto)
    by (smt (verit) FaustusV2AST.FContract.distinct)
  moreover have "isFreshChoiceInValue choiceName val" using calculation by auto
  moreover have "evalFValue env state1 val = Some res" using calculation by (metis (no_types, opaque_lifting) fresh_choice_state_preserves_value_evaluation(1) split_cases_relation.simps)
  moreover have "split_cases_context role choiceName k (ValueAbstraction valId#ctx1) = (ValueAbstraction valId#ctx)" using calculation by auto
  moreover have "state_minimal_eq choiceName (state1\<lparr>boundValues := MList.insert valId res (boundValues state1)\<rparr>) (s\<lparr>boundValues := MList.insert valId res (boundValues state1)\<rparr>)" using calculation by auto
  moreover have forwardStep: "(Let valId val innerCont, ctx1, state1, env1, warns, payments1) \<rightarrow>\<^sub>f\<^sub>2 (innerCont, ValueAbstraction valId#ctx1, state1\<lparr>boundValues := MList.insert valId res (boundValues state1)\<rparr>, env1, (warns @ [ReduceShadowing valId oldVal res]), payments)" using calculation by auto
  moreover have "split_cases_relation role choiceName k (innerCont, ValueAbstraction valId#ctx1, state1\<lparr>boundValues := MList.insert valId res (boundValues state1)\<rparr>, env1, convertReduceWarnings (warns @ [ReduceShadowing valId oldVal res]), payments) (cont, ValueAbstraction valId # ctx, s\<lparr>FState.boundValues := MList.insert valId res (FState.boundValues s)\<rparr>, env, convertReduceWarnings (warns @ [ReduceShadowing valId oldVal res]), payments)" using calculation by auto
  ultimately show ?case by meson
next
  case (ReassignValShadow valId s oldVal env val res cont ctx warns payments)
  moreover obtain innerCont where "c1 = (StatementCont (ReassignVal valId val) innerCont) \<and> split_cases role choiceName k innerCont = cont" using calculation apply (cases c1, auto)
    by (smt (verit) FaustusV2AST.FContract.distinct)
  moreover have "isFreshChoiceInValue choiceName val" using calculation by auto
  moreover have "evalFValue env state1 val = Some res" using calculation by (metis (no_types, opaque_lifting) fresh_choice_state_preserves_value_evaluation(1) split_cases_relation.simps)
  moreover have "split_cases_context role choiceName k (ValueAbstraction valId#ctx1) = (ValueAbstraction valId#ctx)" using calculation by auto
  moreover have "state_minimal_eq choiceName (state1\<lparr>boundValues := MList.insert valId res (boundValues state1)\<rparr>) (s\<lparr>boundValues := MList.insert valId res (boundValues state1)\<rparr>)" using calculation by auto
  moreover have forwardStep: "(StatementCont (ReassignVal valId val) innerCont, ctx1, state1, env1, warns, payments1) \<rightarrow>\<^sub>f\<^sub>2 (innerCont, ValueAbstraction valId#ctx1, state1\<lparr>boundValues := MList.insert valId res (boundValues state1)\<rparr>, env1, (warns @ [ReduceShadowing valId oldVal res]), payments)" using calculation by auto
  moreover have "split_cases_relation role choiceName k (innerCont, ValueAbstraction valId#ctx1, state1\<lparr>boundValues := MList.insert valId res (boundValues state1)\<rparr>, env1, convertReduceWarnings (warns @ [ReduceShadowing valId oldVal res]), payments) (cont, ValueAbstraction valId # ctx, s\<lparr>FState.boundValues := MList.insert valId res (FState.boundValues s)\<rparr>, env, convertReduceWarnings (warns @ [ReduceShadowing valId oldVal res]), payments)" using calculation by auto
  ultimately show ?case by meson
next
  case (LetNoShadow valId s env val res cont ctx warns payments)
  moreover obtain innerCont where "c1 = (Let valId val innerCont) \<and> split_cases role choiceName k innerCont = cont" using calculation apply (cases c1, auto)
    by (smt (verit) FaustusV2AST.FContract.distinct)
  moreover have "isFreshChoiceInValue choiceName val" using calculation by auto
  moreover have "evalFValue env state1 val = Some res" using calculation by (metis (no_types, opaque_lifting) fresh_choice_state_preserves_value_evaluation(1) split_cases_relation.simps)
  moreover have "split_cases_context role choiceName k (ValueAbstraction valId#ctx1) = (ValueAbstraction valId#ctx)" using calculation by auto
  moreover have "state_minimal_eq choiceName (state1\<lparr>boundValues := MList.insert valId res (boundValues state1)\<rparr>) (s\<lparr>boundValues := MList.insert valId res (boundValues state1)\<rparr>)" using calculation by auto
  moreover have forwardStep: "(Let valId val innerCont, ctx1, state1, env1, warns, payments1) \<rightarrow>\<^sub>f\<^sub>2 (innerCont, ValueAbstraction valId#ctx1, state1\<lparr>boundValues := MList.insert valId res (boundValues state1)\<rparr>, env1, (warns @ [ReduceNoWarning]), payments)" using calculation by auto
  moreover have "split_cases_relation role choiceName k (innerCont, ValueAbstraction valId#ctx1, state1\<lparr>boundValues := MList.insert valId res (boundValues state1)\<rparr>, env1, convertReduceWarnings (warns @ [ReduceNoWarning]), payments) (cont, ValueAbstraction valId # ctx, s\<lparr>FState.boundValues := MList.insert valId res (FState.boundValues s)\<rparr>, env, convertReduceWarnings (warns @ [ReduceNoWarning]), payments)" using calculation by auto
  ultimately show ?case by meson
next
  case (ReassignValNoShadow valId s env val res cont ctx warns payments)
  moreover obtain innerCont where "c1 = (StatementCont (ReassignVal valId val) innerCont) \<and> split_cases role choiceName k innerCont = cont" using calculation apply (cases c1, auto)
    by (smt (verit) FaustusV2AST.FContract.distinct)
  moreover have "isFreshChoiceInValue choiceName val" using calculation by auto
  moreover have "evalFValue env state1 val = Some res" using calculation by (metis (no_types, opaque_lifting) fresh_choice_state_preserves_value_evaluation(1) split_cases_relation.simps)
  moreover have "split_cases_context role choiceName k (ValueAbstraction valId#ctx1) = (ValueAbstraction valId#ctx)" using calculation by auto
  moreover have "state_minimal_eq choiceName (state1\<lparr>boundValues := MList.insert valId res (boundValues state1)\<rparr>) (s\<lparr>boundValues := MList.insert valId res (boundValues state1)\<rparr>)" using calculation by auto
  moreover have forwardStep: "(StatementCont (ReassignVal valId val) innerCont, ctx1, state1, env1, warns, payments1) \<rightarrow>\<^sub>f\<^sub>2 (innerCont, ValueAbstraction valId#ctx1, state1\<lparr>boundValues := MList.insert valId res (boundValues state1)\<rparr>, env1, (warns @ [ReduceNoWarning]), payments)" using calculation by auto
  moreover have "split_cases_relation role choiceName k (innerCont, ValueAbstraction valId#ctx1, state1\<lparr>boundValues := MList.insert valId res (boundValues state1)\<rparr>, env1, convertReduceWarnings (warns @ [ReduceNoWarning]), payments) (cont, ValueAbstraction valId # ctx, s\<lparr>FState.boundValues := MList.insert valId res (FState.boundValues s)\<rparr>, env, convertReduceWarnings (warns @ [ReduceNoWarning]), payments)" using calculation by auto
  ultimately show ?case by meson
next
  case (LetObservation e s obs res oId c cx ws ps)
  moreover obtain innerCont where "c1 = (LetObservation oId obs innerCont) \<and> split_cases role choiceName k innerCont = c" using calculation apply (cases c1, auto)
    by (smt (verit) FaustusV2AST.FContract.distinct)
  moreover have "isFreshChoiceInObservation choiceName obs" using calculation by auto
  moreover have "evalFObservation e state1 obs = Some res" using calculation by (metis (no_types, opaque_lifting) fresh_choice_state_preserves_value_evaluation(2) split_cases_relation.simps)
  moreover have "split_cases_context role choiceName k (ObservationAbstraction oId#ctx1) = (ObservationAbstraction oId#cx)" using calculation by auto
  moreover have "state_minimal_eq choiceName (state1\<lparr>boundValues := MList.insert oId (if res then 1 else 0) (boundValues state1)\<rparr>) (s\<lparr>boundValues := MList.insert oId (if res then 1 else 0) (boundValues state1)\<rparr>)" using calculation by auto
  moreover have forwardStep: "(LetObservation oId obs innerCont, ctx1, state1, env1, ws, payments1) \<rightarrow>\<^sub>f\<^sub>2 (innerCont, ObservationAbstraction oId#ctx1, state1\<lparr>boundValues := MList.insert oId (if res then 1 else 0) (boundValues state1)\<rparr>, env1, (ws @ [ReduceNoWarning]), ps)" using calculation by auto
  moreover have "split_cases_relation role choiceName k (innerCont, ObservationAbstraction oId#ctx1, state1\<lparr>boundValues := MList.insert oId (if res then 1 else 0) (boundValues state1)\<rparr>, env1, convertReduceWarnings (ws @ [ReduceNoWarning]), ps) (c, ObservationAbstraction oId # cx, s\<lparr>FState.boundValues := MList.insert oId (if res then 1 else 0) (FState.boundValues s)\<rparr>, e, convertReduceWarnings (ws @ [ReduceNoWarning]), ps)" using calculation by auto
  ultimately show ?case by meson
next
  case (ReassignObservation e s obs res oId c cx ws ps)
  moreover obtain innerCont where "c1 = (StatementCont (ReassignObservation oId obs) innerCont) \<and> split_cases role choiceName k innerCont = c" using calculation apply (cases c1, auto)
    by (smt (verit) FaustusV2AST.FContract.distinct)
  moreover have "isFreshChoiceInObservation choiceName obs" using calculation by auto
  moreover have "evalFObservation e state1 obs = Some res" using calculation by (metis (no_types, opaque_lifting) fresh_choice_state_preserves_value_evaluation(2) split_cases_relation.simps)
  moreover have "split_cases_context role choiceName k (ObservationAbstraction oId#ctx1) = (ObservationAbstraction oId#cx)" using calculation by auto
  moreover have "state_minimal_eq choiceName (state1\<lparr>boundValues := MList.insert oId (if res then 1 else 0) (boundValues state1)\<rparr>) (s\<lparr>boundValues := MList.insert oId (if res then 1 else 0) (boundValues state1)\<rparr>)" using calculation by auto
  moreover have forwardStep: "(StatementCont (ReassignObservation oId obs) innerCont, ctx1, state1, env1, ws, payments1) \<rightarrow>\<^sub>f\<^sub>2 (innerCont, ObservationAbstraction oId#ctx1, state1\<lparr>boundValues := MList.insert oId (if res then 1 else 0) (boundValues state1)\<rparr>, env1, (ws @ [ReduceNoWarning]), ps)" using calculation by auto
  moreover have "split_cases_relation role choiceName k (innerCont, ObservationAbstraction oId#ctx1, state1\<lparr>boundValues := MList.insert oId (if res then 1 else 0) (boundValues state1)\<rparr>, env1, convertReduceWarnings (ws @ [ReduceNoWarning]), ps) (c, ObservationAbstraction oId # cx, s\<lparr>FState.boundValues := MList.insert oId (if res then 1 else 0) (FState.boundValues s)\<rparr>, e, convertReduceWarnings (ws @ [ReduceNoWarning]), ps)" using calculation by auto
  ultimately show ?case by meson
next
  case (LetPubKey s pk res pId c cx e ws ps)
  moreover obtain innerCont where "c1 = (LetPubKey pId pk innerCont) \<and> split_cases role choiceName k innerCont = c" using calculation apply (cases c1, auto)
    by (smt (verit) FaustusV2AST.FContract.distinct)
  moreover have "evalFParty state1 pk = Some res" using calculation by (metis (no_types, lifting) fresh_choice_state_preserves_pubkey_evaluation split_cases_relation.simps state_minimal_eq.simps)
  moreover have "split_cases_context role choiceName k (PubKeyAbstraction pId#ctx1) = (PubKeyAbstraction pId#cx)" using calculation by auto
  moreover have "state_minimal_eq choiceName (state1\<lparr>boundPubKeys := MList.insert pId res (boundPubKeys state1)\<rparr>) (s\<lparr>boundPubKeys := MList.insert pId res (boundPubKeys s)\<rparr>)" using calculation by auto
  moreover have forwardStep: "(LetPubKey pId pk innerCont, ctx1, state1, env1, ws, payments1) \<rightarrow>\<^sub>f\<^sub>2 (innerCont, PubKeyAbstraction pId#ctx1, state1\<lparr>boundPubKeys := MList.insert pId res (boundPubKeys state1)\<rparr>, env1, (ws @ [ReduceNoWarning]), ps)" using calculation by auto
  moreover have "split_cases_relation role choiceName k (innerCont, PubKeyAbstraction pId#ctx1, state1\<lparr>boundPubKeys := MList.insert pId res (boundPubKeys state1)\<rparr>, env1, convertReduceWarnings (ws @ [ReduceNoWarning]), ps) (c, PubKeyAbstraction pId # cx, s\<lparr>boundPubKeys := MList.insert pId res (boundPubKeys s)\<rparr>, e, convertReduceWarnings (ws @ [ReduceNoWarning]), ps)" using calculation by auto
  ultimately show ?case by meson
next
  case (ReassignPubKey s pk res pId c cx e ws ps)
  moreover obtain innerCont where "c1 = (StatementCont (ReassignPubKey pId pk) innerCont) \<and> split_cases role choiceName k innerCont = c" using calculation apply (cases c1, auto)
    by (smt (verit) FaustusV2AST.FContract.distinct)
  moreover have "evalFParty state1 pk = Some res" using calculation by (metis (no_types, lifting) fresh_choice_state_preserves_pubkey_evaluation split_cases_relation.simps state_minimal_eq.simps)
  moreover have "split_cases_context role choiceName k (PubKeyAbstraction pId#ctx1) = (PubKeyAbstraction pId#cx)" using calculation by auto
  moreover have "state_minimal_eq choiceName (state1\<lparr>boundPubKeys := MList.insert pId res (boundPubKeys state1)\<rparr>) (s\<lparr>boundPubKeys := MList.insert pId res (boundPubKeys s)\<rparr>)" using calculation by auto
  moreover have forwardStep: "(StatementCont (ReassignPubKey pId pk) innerCont, ctx1, state1, env1, ws, payments1) \<rightarrow>\<^sub>f\<^sub>2 (innerCont, PubKeyAbstraction pId#ctx1, state1\<lparr>boundPubKeys := MList.insert pId res (boundPubKeys state1)\<rparr>, env1, (ws @ [ReduceNoWarning]), ps)" using calculation by auto
  moreover have "split_cases_relation role choiceName k (innerCont, PubKeyAbstraction pId#ctx1, state1\<lparr>boundPubKeys := MList.insert pId res (boundPubKeys state1)\<rparr>, env1, convertReduceWarnings (ws @ [ReduceNoWarning]), ps) (c, PubKeyAbstraction pId # cx, s\<lparr>boundPubKeys := MList.insert pId res (boundPubKeys s)\<rparr>, e, convertReduceWarnings (ws @ [ReduceNoWarning]), ps)" using calculation by auto
  ultimately show ?case by meson
next
  case (AssertTrue env s obs cont ctx warns payments)
  moreover obtain innerCont where "c1 = (StatementCont (Assert obs) innerCont) \<and> split_cases role choiceName k innerCont = cont" using calculation apply (cases c1, auto)
    by (smt (verit) FaustusV2AST.FContract.distinct)
  moreover have "isFreshChoiceInObservation choiceName obs" using calculation by auto
  moreover have "evalFObservation env1 state1 obs = Some True" using calculation by (metis (no_types, lifting) fresh_choice_state_preserves_value_evaluation(2) split_cases_relation.simps)
  moreover have forwardStep: "(StatementCont (Assert obs) innerCont, ctx1, state1, env1, warns, payments1) \<rightarrow>\<^sub>f\<^sub>2 (innerCont, ctx1, state1, env1, (warns @ [ReduceNoWarning]), payments)" using calculation by auto
  moreover have "split_cases_relation role choiceName k (innerCont, ctx1, state1, env1, convertReduceWarnings (warns @ [ReduceNoWarning]), payments) (cont, ctx, s, env, convertReduceWarnings (warns @ [ReduceNoWarning]), payments)" using calculation by auto
  ultimately show ?case by meson
next
  case (AssertFalse env s obs cont ctx warns payments)
  moreover obtain innerCont where "c1 = (StatementCont (Assert obs) innerCont) \<and> split_cases role choiceName k innerCont = cont" using calculation apply (cases c1, auto)
    by (smt (verit) FaustusV2AST.FContract.distinct)
  moreover have "isFreshChoiceInObservation choiceName obs" using calculation by auto
  moreover have "evalFObservation env1 state1 obs = Some False" using calculation by (metis (no_types, lifting) fresh_choice_state_preserves_value_evaluation(2) split_cases_relation.simps)
  moreover have forwardStep: "(StatementCont (Assert obs) innerCont, ctx1, state1, env1, warns, payments1) \<rightarrow>\<^sub>f\<^sub>2 (innerCont, ctx1, state1, env1, (warns @ [ReduceAssertionFailed]), payments)" using calculation by auto
  moreover have "split_cases_relation role choiceName k (innerCont, ctx1, state1, env1, convertReduceWarnings (warns @ [ReduceAssertionFailed]), payments) (cont, ctx, s, env, convertReduceWarnings (warns @ [ReduceAssertionFailed]), payments)" using calculation by auto
  ultimately show ?case by meson
next
  case (LetC cid params boundCon cont ctx s env warns payments)
  moreover obtain innerBodyCont innerCont where "c1 = (LetC cid params innerBodyCont innerCont) \<and> split_cases role choiceName k innerCont = cont \<and> split_cases role choiceName k innerBodyCont = boundCon" using calculation apply (cases c1, auto)
    by (smt (verit) FaustusV2AST.FContract.distinct)
  moreover have "split_cases_context role choiceName k (ContractAbstraction (cid, params, innerBodyCont)#ctx1) = (ContractAbstraction (cid, params, boundCon)#ctx)" using calculation by auto
  moreover have forwardStep: "(LetC cid params innerBodyCont innerCont, ctx1, state1, env1, warns, payments1) \<rightarrow>\<^sub>f\<^sub>2 (innerCont, ContractAbstraction (cid, params, innerBodyCont)#ctx1, state1, env1, (warns @ [ReduceNoWarning]), payments)" using calculation by auto
  ultimately show ?case by fastforce
next
  case (UseCValueArg ctx cid params bodyCont innerCtx valId restParams args env s val valRes warns payments)
  moreover have "c1 = UseC cid (ValueArgument val # args)" using calculation apply (cases c1, auto)
    by (smt (verit, ccfv_threshold) FaustusV2AST.FContract.distinct(51))
  moreover from UseCValueArg(1) obtain bodyCont1 innerCtx1 where ctx_split: "lookupContractIdAbsInformation ctx1 cid = Some (params, bodyCont1, innerCtx1) \<and> split_cases role choiceName k bodyCont1 = bodyCont \<and> split_cases_context role choiceName k innerCtx1 = innerCtx" using calculation lookup_split_cases_context_preserved_split_context by fastforce
  moreover have preserve_val: "evalFValue env state1 val = Some valRes" using calculation fresh_choice_state_preserves_value_evaluation(1)[of env s val "Some valRes" choiceName state1] by auto
  moreover have split_step: "(UseC cid (ValueArgument val # args), ctx1, state1, env, reduceWarnings1, payments1) \<rightarrow>\<^sub>f\<^sub>2 (UseC cid args, ctx1, state1\<lparr>boundValues := MList.insert valId valRes (boundValues state1)\<rparr>, env, reduceWarnings1 @ [ReduceNoWarning], payments1)" using preserve_val ctx_split UseCValueArg(2) by blast
  moreover have in_relation: "split_cases_relation role choiceName k (UseC cid args, ctx1, state1\<lparr>boundValues := MList.insert valId valRes (boundValues state1)\<rparr>, env, convertReduceWarnings (reduceWarnings1 @ [ReduceNoWarning]), payments1) (UseC cid args, ctx, s\<lparr>boundValues := MList.insert valId valRes (boundValues s)\<rparr>, env, convertReduceWarnings (warns @ [ReduceNoWarning]), payments)" using calculation by auto
  ultimately show ?case by (simp only: split_cases_relation.simps, metis)
next
  case (UseCObservationArg ctx cid params bodyCont innerCtx obsId restParams args env s obs obsRes warns payments)
  moreover have "c1 = UseC cid (ObservationArgument obs # args)" using calculation apply (cases c1, auto)
    by (smt (verit, ccfv_threshold) FaustusV2AST.FContract.distinct(51))
  moreover from UseCValueArg(1) obtain bodyCont1 innerCtx1 where ctx_split: "lookupContractIdAbsInformation ctx1 cid = Some (params, bodyCont1, innerCtx1) \<and> split_cases role choiceName k bodyCont1 = bodyCont \<and> split_cases_context role choiceName k innerCtx1 = innerCtx" using calculation lookup_split_cases_context_preserved_split_context by fastforce
  moreover have preserve_val: "evalFObservation env state1 obs = Some obsRes" using calculation fresh_choice_state_preserves_value_evaluation(2)[of env s obs "Some obsRes" choiceName state1] by auto
  moreover have split_step: "(UseC cid (ObservationArgument obs # args), ctx1, state1, env, reduceWarnings1, payments1) \<rightarrow>\<^sub>f\<^sub>2 (UseC cid args, ctx1, state1\<lparr>boundValues := MList.insert obsId (if obsRes then 1 else 0) (boundValues state1)\<rparr>, env, reduceWarnings1 @ [ReduceNoWarning], payments1)" using preserve_val ctx_split UseCObservationArg(2) by blast
  moreover have in_relation: "split_cases_relation role choiceName k (UseC cid args, ctx1, state1\<lparr>boundValues := MList.insert obsId (if obsRes then 1 else 0) (boundValues state1)\<rparr>, env, convertReduceWarnings (reduceWarnings1 @ [ReduceNoWarning]), payments1) (UseC cid args, ctx, s\<lparr>boundValues := MList.insert obsId (if obsRes then 1 else 0) (boundValues s)\<rparr>, env, convertReduceWarnings (warns @ [ReduceNoWarning]), payments)" using calculation by auto
  ultimately show ?case by (simp only: split_cases_relation.simps, metis)
next
  case (UseCPubKeyArg ctx cid params bodyCont innerCtx pkId restParams args s pk pkRes env warns payments)
  moreover have "c1 = UseC cid (PubKeyArgument pk # args)" using calculation apply (cases c1, auto)
    by (smt (verit, ccfv_threshold) FaustusV2AST.FContract.distinct(51))
  moreover from UseCValueArg(1) obtain bodyCont1 innerCtx1 where ctx_split: "lookupContractIdAbsInformation ctx1 cid = Some (params, bodyCont1, innerCtx1) \<and> split_cases role choiceName k bodyCont1 = bodyCont \<and> split_cases_context role choiceName k innerCtx1 = innerCtx" using calculation lookup_split_cases_context_preserved_split_context by fastforce
  moreover have preserve_val: "evalFParty state1 pk = Some pkRes" using calculation fresh_choice_state_preserves_pubkey_evaluation[of s pk "Some pkRes" state1] by auto
  moreover have split_step: "(UseC cid (PubKeyArgument pk # args), ctx1, state1, env, reduceWarnings1, payments1) \<rightarrow>\<^sub>f\<^sub>2 (UseC cid args, ctx1, state1\<lparr>boundPubKeys := MList.insert pkId pkRes (boundPubKeys state1)\<rparr>, env, reduceWarnings1 @ [ReduceNoWarning], payments1)" using preserve_val ctx_split UseCPubKeyArg(2) by blast
  moreover have in_relation: "split_cases_relation role choiceName k (UseC cid args, ctx1, state1\<lparr>boundPubKeys := MList.insert pkId pkRes (boundPubKeys state1)\<rparr>, env, convertReduceWarnings (reduceWarnings1 @ [ReduceNoWarning]), payments1) (UseC cid args, ctx, s\<lparr>boundPubKeys := MList.insert pkId pkRes (boundPubKeys s)\<rparr>, env, convertReduceWarnings (warns @ [ReduceNoWarning]), payments)" using calculation by auto
  ultimately show ?case by (simp only: split_cases_relation.simps, metis)
next
  case (UseCFound ctx cid params bodyCont innerCtx s env warns payments bodyResCont bodyResCtx bodyResState bodyResEnv bodyResWarns bodyResPayments)
  moreover have "c1 = (UseC cid [])" using calculation apply (cases c1, auto)
    by (smt (verit) FaustusV2AST.FContract.distinct)
  moreover obtain bodyCont1 innerCtx1 where "lookupContractIdAbsInformation ctx1 cid = Some (params, bodyCont1, innerCtx1) \<and> split_cases role choiceName k bodyCont1 = bodyCont \<and> split_cases_context role choiceName k innerCtx1 = innerCtx" using calculation lookup_split_cases_context_preserved_split_context by fastforce
  moreover have "split_cases_relation role choiceName k (bodyCont1, paramsToFContext params @ innerCtx1, state1, env, convertReduceWarnings reduceWarnings1, payments1)
     (bodyCont, paramsToFContext params @ innerCtx, s, env, convertReduceWarnings warns, payments)" using calculation apply auto
    using freshChoicePreserved_lookupContractIdAbsInformation split_cases_context_append_params_preserved freshChoicePreserved_appendParams by auto+
  moreover obtain bodyResCont1 bodyResCtx1 bodyResState1 bodyResWarns1 where bodyStep: "(bodyCont1, paramsToFContext params @ innerCtx1, state1, env1, warns, payments1) \<rightarrow>\<^sub>f\<^sub>2 (bodyResCont1, bodyResCtx1, bodyResState1, bodyResEnv, bodyResWarns1, bodyResPayments) \<and>
    split_cases_relation role choiceName k (bodyResCont1, bodyResCtx1, bodyResState1, bodyResEnv, convertReduceWarnings bodyResWarns1, bodyResPayments) (bodyResCont, bodyResCtx, bodyResState, bodyResEnv, convertReduceWarnings bodyResWarns, bodyResPayments)" using calculation by fastforce
  ultimately show ?case by (auto, blast)
qed

lemma react_step_add_choice_preserves_split_cases_relation:
"internal_reaction_add_choice choiceName p1 p1' \<Longrightarrow>
  split_cases_relation role choiceName k p1 p2 \<Longrightarrow>
  \<exists>p2'.
    star (internal_reaction_add_choice choiceName) p2 p2' \<and>
    split_cases_relation role choiceName k p1' p2'"
proof (induction choiceName p1 p1' arbitrary: p2 rule: internal_reaction_add_choice.induct)
  case (BaseReaction c1 c2 choiceName)
  moreover obtain cont1 ctx1 state1 env1 warnings1 payments1
    cont1' ctx1' state1' env1' warnings1' payments1'
    where "(cont1, ctx1, state1, env1, warnings1, payments1) \<rightarrow>\<^sub>f\<^sub>2 (cont1', ctx1', state1', env1', warnings1', payments1') \<and>
      c1 = (cont1, ctx1, state1, env1, convertReduceWarnings warnings1, payments1) \<and>
      c2 = (cont1', ctx1', state1', env1', convertReduceWarnings warnings1', payments1')" using calculation internal_reaction.simps by fastforce
  moreover obtain cont2 ctx2 state2 env2 payments2 where "p2 = (cont2, ctx2, state2, env2, convertReduceWarnings warnings1, payments2)" using calculation by (cases p2, auto)
  moreover obtain cont2 ctx2 state2 env2 warnings2 payments2
    cont2' ctx2' state2' env2' warnings2' payments2'
    where "(cont2, ctx2, state2, env2, warnings2, payments2) \<rightarrow>\<^sub>f\<^sub>2 (cont2', ctx2', state2', env2', warnings2', payments2') \<and>
      p2 = (cont2, ctx2, state2, env2, convertReduceWarnings warnings2, payments2) \<and>
      split_cases_relation role choiceName k (cont1', ctx1', state1', env1', convertReduceWarnings warnings1', payments1') (cont2', ctx2', state2', env2', convertReduceWarnings warnings2', payments2')" using calculation by (smt (verit, best) small_step_preserves_split_cases_relation)
  ultimately show ?case by (metis SmallStepReduce internal_reaction_add_choice.BaseReaction star_step1)
next
  case (ChoiceReaction newLower newUpper state newTimeEnv newTimeState realLower realUpper t freshChoiceName party i cases ctx warnings payments newC newCtx newState newEnv newWarnings newPayments cont env)
  then show ?case by (smt (z3) isFreshChoiceInCasesImpliesIsFreshChoiceInCasesGuards freshChoiceNoMatch_cases_step_aux isFreshChoiceInContract.simps(4) isFreshChoiceInProcess.simps split_cases_relation.elims(2))
qed

lemma react_step_strongly_preserves_split_cases_relation:
"internal_reaction p1 p1' \<Longrightarrow>
  split_cases_relation role choiceName k p1 p2 \<Longrightarrow>
  \<exists>p2'.
    internal_reaction p2 p2' \<and>
    split_cases_relation role choiceName k p1' p2'"
proof (induction p1 p1' arbitrary: p2 rule: internal_reaction.induct)
  case (SmallStepReduce c ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments)
  then show ?case by (smt (z3) internal_reaction.SmallStepReduce small_step_preserves_split_cases_relation split_cases_relation.elims(2) split_cases_relation.simps)
qed

lemma react_step_preserves_split_cases_relation:
"internal_reaction p1 p1' \<Longrightarrow>
  split_cases_relation role choiceName k p1 p2 \<Longrightarrow>
  \<exists>p2'.
    star internal_reaction p2 p2' \<and>
    split_cases_relation role choiceName k p1' p2'"
proof (induction p1 p1' arbitrary: p2 rule: internal_reaction.induct)
  case (SmallStepReduce c ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments)
  then show ?case by (meson internal_reaction.SmallStepReduce react_step_strongly_preserves_split_cases_relation star_step1)
qed

lemma react_step_preserves_inverse_split_cases_relation:
"internal_reaction p2 p2' \<Longrightarrow>
  split_cases_relation role choiceName k p1 p2 \<Longrightarrow>
  \<exists>p1'.
    star internal_reaction p1 p1' \<and>
    split_cases_relation role choiceName k p1' p2'"
proof (induction p2 p2' arbitrary: p1 rule: internal_reaction.induct)
  case (SmallStepReduce c ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments)
  then show ?case by (smt (z3) internal_reaction.SmallStepReduce small_step_preserves_inverse_split_cases_relation split_cases_relation.elims(2) split_cases_relation.simps star_step1)
qed

lemma split_cases_relation_preserves_halted_action:
"computeActionStep inp (a1, ctx1, state1, env1) = None \<Longrightarrow>
  isFreshChoiceInAction choiceName a1 \<Longrightarrow>
  state_minimal_eq choiceName state1 state2 \<Longrightarrow>
  computeActionStep inp (a1, split_cases_context role choiceName k ctx1, state2, env1) = None"
proof (induction inp a1 ctx1 state1 env1 rule: computeActionStep.induct[split_format(complete)])
  case (1 accId mParty mToken mMoney fAccId fParty fToken fValue ctx s env)
  then show ?case apply (auto split: option.split_asm option.split if_split_asm)
                       apply ((metis evalFValue_InsertDifferentValueStillExecutes(1) option.distinct(1))+)[3]
                    apply ((metis fresh_choice_state_preserves_pubkey_evaluation option.discI)+)[6]
              apply (metis option.distinct(1), metis option.distinct(1))
            apply ((metis fresh_choice_state_preserves_pubkey_evaluation option.inject)+)[6]
    by (metis 1(3) fresh_choice_state_preserves_value_evaluation(1) option.inject)+
next
  case (2 mChoiceId choice fChoiceId bounds ctx s env)
  then show ?case apply (auto split: option.split_asm option.split if_split_asm)
    using fresh_choice_state_preserves_choiceId_evaluation by blast
next
  case (3 obs ctx s env)
  then show ?case apply auto
    by (metis 3(3) fresh_choice_state_preserves_value_evaluation(2) option.distinct(1))
qed auto

lemma small_step_action_preserves_split_cases_relation:
"inp \<turnstile> (a, ctx1, state1, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (newS1, newWarning) \<Longrightarrow>
  isFreshChoiceInAction choiceName a \<Longrightarrow>
  state_minimal_eq choiceName state1 state2 \<Longrightarrow>
  (\<exists>newS2 . inp \<turnstile> (a, split_cases_context role choiceName k ctx1, state2, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (newS2, newWarning) \<and>
    state_minimal_eq choiceName newS1 newS2)"
proof (induction inp a ctx1 state1 env newS1 newWarning arbitrary: state2 rule: small_step_reduce_action.induct[split_format(complete)])
  case (DepositPositiveActionStep env s fValue valRes fAccId fAccIdRes fParty fPartyRes accId mParty fToken mToken mMoney moneyInAcc newBalance newAccs ctx)
  moreover have "evalFValue env state2 fValue = Some valRes" using fresh_choice_state_preserves_value_evaluation(1)[of env s fValue "Some valRes" choiceName state2] calculation by auto
  moreover have "evalFParty state2 fAccId = Some fAccIdRes" using fresh_choice_state_preserves_pubkey_evaluation[of s fAccId "Some fAccIdRes" state2] calculation by auto
  moreover have "evalFParty state2 fParty = Some fPartyRes" using fresh_choice_state_preserves_pubkey_evaluation[of s fParty "Some fPartyRes" state2] calculation by auto
  moreover have "moneyInAccount accId mToken (accounts state2) = moneyInAcc" using calculation by auto
  moreover have "updateMoneyInAccount accId mToken newBalance (accounts state2) = newAccs" using calculation by auto
  moreover have "(IDeposit accId mParty mToken mMoney) \<turnstile> (Deposit fAccId fParty fToken fValue, split_cases_context role choiceName k ctx, state2, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (state2\<lparr>FState.accounts := newAccs\<rparr>, ApplyNoWarning)" using calculation small_step_reduce_action.simps by fastforce
  moreover have "state_minimal_eq choiceName (s\<lparr>FState.accounts := newAccs\<rparr>) (state2\<lparr>FState.accounts := newAccs\<rparr>)" using calculation by auto
  ultimately show ?case by auto
next
  case (DepositNonPositiveActionStep env s fValue valRes fAccId fAccIdRes fParty fPartyRes accId mParty fToken mToken mMoney moneyInAcc newBalance newAccs ctx)
  moreover have "evalFValue env state2 fValue = Some valRes" using fresh_choice_state_preserves_value_evaluation(1)[of env s fValue "Some valRes" choiceName state2] calculation by auto
  moreover have "evalFParty state2 fAccId = Some fAccIdRes" using fresh_choice_state_preserves_pubkey_evaluation[of s fAccId "Some fAccIdRes" state2] calculation by auto
  moreover have "evalFParty state2 fParty = Some fPartyRes" using fresh_choice_state_preserves_pubkey_evaluation[of s fParty "Some fPartyRes" state2] calculation by auto
  moreover have "moneyInAccount accId mToken (accounts state2) = moneyInAcc" using calculation by auto
  moreover have "updateMoneyInAccount accId mToken newBalance (accounts state2) = newAccs" using calculation by auto
  moreover have "(IDeposit accId mParty mToken mMoney) \<turnstile> (Deposit fAccId fParty fToken fValue, split_cases_context role choiceName k ctx, state2, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (state2\<lparr>FState.accounts := newAccs\<rparr>, ApplyNonPositiveDeposit mParty accId mToken mMoney)" using calculation small_step_reduce_action.simps by fastforce
  moreover have "state_minimal_eq choiceName (s\<lparr>FState.accounts := newAccs\<rparr>) (state2\<lparr>FState.accounts := newAccs\<rparr>)" using calculation by auto
  ultimately show ?case by auto
next
  case (ChoiceActionStep choice bounds s fChoiceId fChoiceIdRes mChoiceId ctx env)
  then show ?case apply auto
    by (smt (verit, best) ChoiceActionStep.hyps(1) FState.select_convs(1-5) FState.surjective FState.update_convs(2) MList.insert_lookup_Some computeActionStep.simps(2) computeActionStepIsSmallStep fresh_choice_state_preserves_choiceId_evaluation insert_lookup_different)
next
  case (NotifyActionStep env s obs ctx)
  then show ?case using fresh_choice_state_preserves_value_evaluation(2) isFreshChoiceInAction.simps(2) small_step_reduce_action.NotifyActionStep by blast
qed

lemma split_cases_relation_preserves_halted_guard:
"computeGuardStep inp (g1, ctx1, state1, env1, reduceWarnings1, payments1) = None \<Longrightarrow>
  isFreshChoiceInGuard choiceName g1 \<Longrightarrow>
  state_minimal_eq choiceName state1 state2 \<Longrightarrow>
  computeGuardStep inp (g1, split_cases_context role choiceName k ctx1, state2, env1, reduceWarnings1, payments1) = None"
  apply (induction inp g1 ctx1 state1 env1 reduceWarnings1 payments1 rule: computeGuardStep.induct[split_format(complete)])
  by (auto split: option.split_asm option.split simp add: split_cases_relation_preserves_halted_action)

lemma small_step_guard_preserves_split_cases_relation:
"inp \<turnstile> (g1, ctx1, state1, env1, reduceWarnings1, payments1) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, g1'), ctx1', state1', env1', reduceWarnings1', payments1') \<Longrightarrow>
  isFreshChoiceInGuard choiceName g1 \<Longrightarrow>
  state_minimal_eq choiceName state1 state2 \<Longrightarrow>
  (\<exists> state2' . inp \<turnstile> (g1, split_cases_context role choiceName k ctx1, state2, env1, reduceWarnings1, payments1) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, g1'), split_cases_context role choiceName k ctx1', state2', env1', reduceWarnings1', payments1') \<and>
    state_minimal_eq choiceName state1' state2')"
proof (induction inp g1 ctx1 state1 env1 reduceWarnings1 payments1 stmts g1' ctx1' state1' env1' reduceWarnings1' payments1'
      arbitrary: state2 rule: small_step_reduce_guard.induct[split_format(complete)])
  case (ActionGuardStep g a ins ctx s env newS applyWarning warnings payments)
  moreover obtain newS2 where "ins \<turnstile> (a, split_cases_context role choiceName k ctx, state2, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (newS2, applyWarning) \<and> state_minimal_eq choiceName newS newS2" using calculation isFreshChoiceInGuard.simps(1) small_step_action_preserves_split_cases_relation by blast
  moreover have "ins \<turnstile> (ActionGuard a, split_cases_context role choiceName k ctx, state2, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 (([], None), split_cases_context role choiceName k ctx, newS2, env, warnings @ convertApplyWarning applyWarning, payments)" using calculation small_step_reduce_guard.ActionGuardStep by blast
  ultimately show ?case by auto
next
  case (GuardStmtsGuardCompleteGuardStep g g1 stmts ins ctx s env warnings payments newStmts newCtx newS newEnv newWarnings newPayments)
  then show ?case by (meson isFreshChoiceInGuard.simps(3) small_step_reduce_guard.GuardStmtsGuardCompleteGuardStep)
next
  case (GuardStmtsGuardIncompleteGuard g g1 stmts ins ctx s env warnings payments newStmts nextG1 newCtx newS newEnv newWarnings newPayments)
  then show ?case by (meson isFreshChoiceInGuard.simps(3) small_step_reduce_guard.GuardStmtsGuardIncompleteGuard)
next
  case (DisjointGuardMatchesG1 ins g1 ctx s env warnings payments a b a a a a b g2)
  then show ?case by (meson isFreshChoiceInGuard.simps(4) small_step_reduce_guard.DisjointGuardMatchesG1)
next
  case (DisjointGuardMatchesG2 ins g1 ctx s env warnings payments g2 a b a a a a b)
  then show ?case by (meson isFreshChoiceInGuard.simps(4) small_step_reduce_guard.DisjointGuardMatchesG2 split_cases_relation_preserves_halted_guard)
next
  case (InterleavedGuardMatchesG1Complete ins g1 ctx s env warnings payments stmts newCtx newS newEnv newWarnings newPayments g2)
  then show ?case by (meson isFreshChoiceInGuard.simps(5) small_step_reduce_guard.InterleavedGuardMatchesG1Complete)
next
  case (InterleavedGuardMatchesG1Incomplete ins g1 ctx s env warnings payments stmts innerG1 newCtx newS newEnv newWarnings newPayments g2)
  then show ?case by (meson isFreshChoiceInGuard.simps(5) small_step_reduce_guard.InterleavedGuardMatchesG1Incomplete)
next
  case (InterleavedGuardMatchesG2Complete ins g2 ctx s env warnings payments stmts newCtx newS newEnv newWarnings newPayments g1)
  then show ?case by (meson isFreshChoiceInGuard.simps(5) small_step_reduce_guard.InterleavedGuardMatchesG2Complete split_cases_relation_preserves_halted_guard)
next
  case (InterleavedGuardMatchesG2Incomplete ins g2 ctx s env warnings payments stmts innerG2 newCtx newS newEnv newWarnings newPayments g1)
  then show ?case by (meson isFreshChoiceInGuard.simps(5) small_step_reduce_guard.InterleavedGuardMatchesG2Incomplete split_cases_relation_preserves_halted_guard)
qed (auto; meson small_step_reduce_guard.GuardThenGuardCompleteFirstGuardStep GuardThenGuardIncompleteGuard)+

lemma split_cases_distributes_past_prepend_statements:
"prependStatements stmts (split_cases role choiceName k c) = split_cases role choiceName k (prependStatements stmts c)" by (induction stmts c rule: prependStatements.induct, auto)

lemma split_cases_relation_preserves_halted_case:
"computeCaseStep inp (c1, ctx1, state1, env1, reduceWarnings1, payments1) = None \<Longrightarrow>
  isFreshChoiceInCasesGuards choiceName [c1] \<Longrightarrow>
  state_minimal_eq choiceName state1 state2 \<Longrightarrow>
  computeCaseStep inp ((\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) c1, ctx2, state2, env1, reduceWarnings1, payments1) = None"
proof (induction inp c1 ctx1 state1 env1 reduceWarnings1 payments1 arbitrary: ctx2 state2 rule: computeCaseStep.induct[split_format(complete)])
  case (1 ins g c ctx s env warnings payments)
  then show ?case apply auto
    apply (cases "computeGuardStep ins (g, ctx, s, env, warnings, payments)", auto)
     apply (cases "computeGuardStep ins (g, ctx2, state2, env, warnings, payments)", auto)
     apply (metis 1(3) computeGuardStepInvariantsHalted option.distinct(1) split_cases_relation_preserves_halted_guard)
    apply (cases "computeGuardStep ins (g, ctx2, state2, env, warnings, payments)", auto)
    by (split option.split_asm option.split, auto)
qed

lemma small_step_case_preserves_split_cases_relation:
"inp \<turnstile> (c1, ctx1, state1, env1, reduceWarnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 (c1', ctx1', state1', env1', reduceWarnings1', payments1') \<Longrightarrow>
  isFreshChoiceInCasesGuards choiceName [c1] \<Longrightarrow>
  state_minimal_eq choiceName state1 state2 \<Longrightarrow>
  (\<exists>c2' state2' . inp \<turnstile> ((\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) c1, split_cases_context role choiceName k ctx1, state2, env1, reduceWarnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 (c2', split_cases_context role choiceName k ctx1', state2', env1', reduceWarnings1', payments1') \<and>
    state_minimal_eq choiceName state1' state2' \<and>
    (c2' t (split_cases role choiceName k tc1) = split_cases role choiceName k (c1' t tc1)))"
proof (induction inp c1 ctx1 state1 env1 reduceWarnings1 payments1 c1' ctx1' state1' env1' reduceWarnings1' payments1'
      arbitrary: state2 rule: small_step_reduce_case.induct[split_format(complete)])
  case (CaseGuardCompleteStep ins g ctx s env warnings payments stmts newCtx newS newEnv newWarnings newPayments c)
  moreover obtain state2' where "ins \<turnstile> (g, split_cases_context role choiceName k ctx, state2, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, None), split_cases_context role choiceName k newCtx, state2', newEnv, newWarnings, newPayments) \<and> state_minimal_eq choiceName newS state2'" using small_step_guard_preserves_split_cases_relation[of ins g ctx s env warnings payments stmts None newCtx newS newEnv newWarnings newPayments choiceName state2 role k] calculation by auto
  moreover have "ins \<turnstile> (Case g (split_cases role choiceName k c), split_cases_context role choiceName k ctx, state2, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 ((\<lambda>t tc . prependStatements stmts (split_cases role choiceName k c)), split_cases_context role choiceName k newCtx, state2', newEnv, newWarnings, newPayments)" by (simp add: calculation small_step_reduce_case.CaseGuardCompleteStep)
  moreover have "prependStatements stmts (split_cases role choiceName k c) = split_cases role choiceName k (prependStatements stmts c)" by (induction stmts c rule: prependStatements.induct, auto)
  ultimately show ?case by auto
next
  case (CaseGuardIncompleteStep ins g ctx s env warnings payments stmts innerG newCtx newS newEnv newWarnings newPayments c)
  moreover obtain state2' where "ins \<turnstile> (g, split_cases_context role choiceName k ctx, state2, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some innerG), split_cases_context role choiceName k newCtx, state2', newEnv, newWarnings, newPayments) \<and> state_minimal_eq choiceName newS state2'" using small_step_guard_preserves_split_cases_relation[of ins g ctx s env warnings payments stmts "Some innerG" newCtx newS newEnv newWarnings newPayments choiceName state2 role k] calculation by auto
  moreover have "ins \<turnstile> (Case g (split_cases role choiceName k c), split_cases_context role choiceName k ctx, state2, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 ((\<lambda>t tc . prependStatements stmts (When [Case innerG  (split_cases role choiceName k c)] t tc)), split_cases_context role choiceName k newCtx, state2', newEnv, newWarnings, newPayments)" by (simp add: calculation small_step_reduce_case.CaseGuardIncompleteStep)
  moreover have "When [Case innerG  (split_cases role choiceName k c)] t (split_cases role choiceName k tc1) = (split_cases role choiceName k (When [Case innerG c] t tc1))" by (induction k, auto)
  moreover have "(\<lambda>t tc . prependStatements stmts (When [Case innerG  (split_cases role choiceName k c)] t tc)) t (split_cases role choiceName k tc1) = prependStatements stmts (When [Case innerG  (split_cases role choiceName k c)] t (split_cases role choiceName k tc1))" by auto
  moreover have "(\<lambda>t tc . prependStatements stmts (When [Case innerG  (split_cases role choiceName k c)] t tc)) t (split_cases role choiceName k tc1) = split_cases role choiceName k (prependStatements stmts (When [Case innerG c] t tc1))" by (simp only: calculation split_cases_distributes_past_prepend_statements)
  ultimately show ?case by (metis FaustusV2AST.FCase.case)
qed

lemma minimal_state_eq_preserves_small_step_action:
"inp \<turnstile> (a, ctx1, state1, env1) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (newS, newWarning) \<Longrightarrow>
  isFreshChoiceInAction choiceName a \<Longrightarrow>
  state_minimal_eq choiceName state1 state2 \<Longrightarrow>
  (\<exists>newS2 . inp \<turnstile> (a, ctx1, state2, env1) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (newS2, newWarning) \<and>
    state_minimal_eq choiceName newS newS2)"
proof (induction inp a ctx1 state1 env1 newS newWarning rule: small_step_reduce_action.induct[split_format(complete)])
  case (DepositPositiveActionStep env s fValue valRes fAccId fAccIdRes fParty fPartyRes accId mParty fToken mToken mMoney moneyInAcc newBalance newAccs ctx)
  moreover have "evalFValue env state2 fValue = Some valRes" using fresh_choice_state_preserves_value_evaluation(1)[of env s fValue "Some valRes" choiceName state2] calculation(1,12,13) by auto
  moreover have "evalFParty state2 fAccId = Some fAccIdRes" using fresh_choice_state_preserves_pubkey_evaluation[of s fAccId "Some fAccIdRes" state2] calculation by auto
  moreover have "evalFParty s fParty = Some fPartyRes" using fresh_choice_state_preserves_pubkey_evaluation[of s fParty "Some fPartyRes" state2] calculation by auto
  moreover have "(IDeposit accId mParty mToken mMoney) \<turnstile> (Deposit fAccId fParty fToken fValue, ctx, state2, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (state2\<lparr>FState.accounts := newAccs\<rparr>, ApplyNoWarning)" using small_step_reduce_action.DepositPositiveActionStep[of env state2 fValue valRes fAccId fAccIdRes fParty fPartyRes accId mParty fToken mToken mMoney moneyInAcc newBalance newAccs ctx] calculation apply auto
    using fresh_choice_state_preserves_pubkey_evaluation by presburger+
  ultimately show ?case by auto
next
  case (DepositNonPositiveActionStep env s fValue valRes fAccId fAccIdRes fParty fPartyRes accId mParty fToken mToken mMoney moneyInAcc newBalance newAccs ctx)
  moreover have "evalFValue env state2 fValue = Some valRes" using fresh_choice_state_preserves_value_evaluation(1)[of env s fValue "Some valRes" choiceName state2] calculation(1,12,13) by auto
  moreover have "evalFParty state2 fAccId = Some fAccIdRes" using fresh_choice_state_preserves_pubkey_evaluation[of s fAccId "Some fAccIdRes" state2] calculation by auto
  moreover have "evalFParty s fParty = Some fPartyRes" using fresh_choice_state_preserves_pubkey_evaluation[of s fParty "Some fPartyRes" state2] calculation by auto
  moreover have "(IDeposit accId mParty mToken mMoney) \<turnstile> (Deposit fAccId fParty fToken fValue, ctx, state2, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (state2\<lparr>FState.accounts := newAccs\<rparr>, ApplyNonPositiveDeposit mParty accId mToken mMoney)" using small_step_reduce_action.DepositNonPositiveActionStep[of env state2 fValue valRes fAccId fAccIdRes fParty fPartyRes accId mParty fToken mToken mMoney moneyInAcc newBalance newAccs ctx] calculation apply auto
    using fresh_choice_state_preserves_pubkey_evaluation by presburger+
  ultimately show ?case by auto
next
  case (ChoiceActionStep choice bounds s fChoiceId fChoiceIdRes mChoiceId ctx env)
  moreover have "evalFChoiceId state2 fChoiceId = Some fChoiceIdRes" using fresh_choice_state_preserves_choiceId_evaluation[of s fChoiceId "Some fChoiceIdRes" state2] calculation by auto
  moreover have "(IChoice mChoiceId choice) \<turnstile> (FAction.Choice fChoiceId bounds, ctx, state2, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (state2\<lparr>FState.choices := MList.insert mChoiceId choice (FState.choices state2)\<rparr>, ApplyNoWarning)" using calculation small_step_reduce_action.ChoiceActionStep by auto
  moreover have "state_minimal_eq choiceName (s\<lparr>FState.choices := MList.insert mChoiceId choice (FState.choices s)\<rparr>) (state2\<lparr>FState.choices := MList.insert mChoiceId choice (FState.choices state2)\<rparr>)" using calculation apply auto
    by (metis MList.insert_lookup_Some insert_lookup_different)
  ultimately show ?case by auto
next
  case (NotifyActionStep env s obs ctx)
  moreover have "evalFObservation env state2 obs = Some True" using NotifyActionStep calculation(2,3) fresh_choice_state_preserves_value_evaluation(2) isFreshChoiceInAction.simps(2) by blast
  ultimately show ?case using small_step_reduce_action.NotifyActionStep by blast
qed

lemma minimal_state_eq_preserves_small_step_action_halted:
"computeActionStep inp (a, ctx1, state1, env1) = None \<Longrightarrow>
  isFreshChoiceInAction choiceName a \<Longrightarrow>
  state_minimal_eq choiceName state1 state2 \<Longrightarrow>
  computeActionStep inp (a, ctx1, state2, env1) = None"
  apply (induction inp a ctx1 state1 env1 rule: computeActionStep.induct[split_format(complete)])
           apply (auto split: option.split_asm option.split)
               apply ((metis evalFValue_InsertDifferentValueStillExecutes(1) option.distinct(1))+)[3]
            apply ((metis fresh_choice_state_preserves_pubkey_evaluation option.distinct(1))+)[6]
      apply (auto split: if_split if_split_asm)
              apply ((metis fresh_choice_state_preserves_pubkey_evaluation option.inject)+)[2]
            apply (smt (verit, ccfv_threshold) fresh_choice_state_preserves_value_evaluation(1) option.inject state_minimal_eq.simps)
           apply (metis option.distinct(1))
          apply ((metis fresh_choice_state_preserves_pubkey_evaluation option.inject)+)[2]
        apply (smt (verit, best) fresh_choice_state_preserves_value_evaluation(1) option.inject state_minimal_eq.simps)
       apply (metis option.distinct(1))
      apply ((metis fresh_choice_state_preserves_pubkey_evaluation option.inject)+)[2]
    apply (smt (verit, best) fresh_choice_state_preserves_value_evaluation(1) option.inject state_minimal_eq.elims(3))
   apply (metis fresh_choice_state_preserves_choiceId_evaluation)
  by (smt (z3) fresh_choice_state_preserves_value_evaluation(2) state_minimal_eq.elims(1))

lemma minimal_state_eq_preserves_small_step_guard_halted:
"computeGuardStep inp (g, ctx1, state1, env1, reduceWarnings1, payments1) = None \<Longrightarrow>
  isFreshChoiceInGuard choiceName g \<Longrightarrow>
  state_minimal_eq choiceName state1 state2 \<Longrightarrow>
  computeGuardStep inp (g, ctx1, state2, env1, reduceWarnings1, payments1) = None"
  apply (induction inp g ctx1 state1 env1 reduceWarnings1 payments1 arbitrary: state2 rule: computeGuardStep.induct[split_format(complete)])
  using minimal_state_eq_preserves_small_step_action_halted by (auto split: option.split option.split_asm)

lemma minimal_state_eq_preserves_small_step_guard:
"inp \<turnstile> (g, ctx1, state1, env1, reduceWarnings1, payments1) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, nextG), ctx1', state1', env1', reduceWarnings1', payments1') \<Longrightarrow>
  isFreshChoiceInGuard choiceName g \<Longrightarrow>
  state_minimal_eq choiceName state1 state2 \<Longrightarrow>
  (\<exists>state2' . inp \<turnstile> (g, ctx1, state2, env1, reduceWarnings1, payments1) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, nextG), ctx1', state2', env1', reduceWarnings1', payments1') \<and>
    state_minimal_eq choiceName state1' state2')"
proof (induction inp g ctx1 state1 env1 reduceWarnings1 payments1 stmts nextG ctx1' state1' env1' reduceWarnings1' payments1' arbitrary: state2 rule: small_step_reduce_guard.induct[split_format(complete)])
  case (ActionGuardStep g a ins ctx s env newS applyWarning warnings payments)
  moreover obtain newS2 where "ins \<turnstile> (a, ctx, state2, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (newS2, applyWarning) \<and> state_minimal_eq choiceName newS newS2" using minimal_state_eq_preserves_small_step_action[of ins a ctx s env newS applyWarning choiceName state2] calculation by auto
  ultimately show ?case using small_step_reduce_guard.ActionGuardStep by blast
qed (meson isFreshChoiceInGuard.simps minimal_state_eq_preserves_small_step_guard_halted
    GuardThenGuardCompleteFirstGuardStep GuardThenGuardIncompleteGuard GuardStmtsGuardCompleteGuardStep
    GuardStmtsGuardIncompleteGuard DisjointGuardMatchesG1 DisjointGuardMatchesG2 InterleavedGuardMatchesG1Complete
    InterleavedGuardMatchesG1Incomplete InterleavedGuardMatchesG2Complete InterleavedGuardMatchesG2Incomplete)+

lemma minimal_state_eq_preserves_small_step_case_halted:
"computeCaseStep inp (c1, ctx1, state1, env1, reduceWarnings1, payments1) = None \<Longrightarrow>
  isFreshChoiceInCasesGuards choiceName [c1] \<Longrightarrow>
  state_minimal_eq choiceName state1 state2 \<Longrightarrow>
  computeCaseStep inp (c1, ctx1, state2, env1, reduceWarnings1, payments1) = None"
  apply (induction inp c1 ctx1 state1 env1 reduceWarnings1 payments1 rule: computeCaseStep.induct[split_format(complete)])
  using minimal_state_eq_preserves_small_step_guard_halted by (auto split: option.split_asm option.split)

lemma minimal_state_eq_preserves_small_step_case:
"inp \<turnstile> (c1, ctx1, state1, env1, reduceWarnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 (c1', ctx1', state1', env1', reduceWarnings1', payments1') \<Longrightarrow>
  isFreshChoiceInCasesGuards choiceName [c1] \<Longrightarrow>
  state_minimal_eq choiceName state1 state2 \<Longrightarrow>
  (\<exists>state2' . inp \<turnstile> (c1, ctx1, state2, env1, reduceWarnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 (c1', ctx1', state2', env1', reduceWarnings1', payments1') \<and>
    state_minimal_eq choiceName state1' state2')"
proof (induction inp c1 ctx1 state1 env1 reduceWarnings1 payments1 c1' ctx1' state1' env1' reduceWarnings1' payments1'
      arbitrary: state2 rule: small_step_reduce_case.induct[split_format(complete)])
  case (CaseGuardCompleteStep ins g ctx s env warnings payments stmts newCtx newS newEnv newWarnings newPayments c)
  then show ?case by (metis isFreshChoiceInCasesGuards.simps(2) minimal_state_eq_preserves_small_step_guard reduceGuardExpressionsConstants small_step_reduce_case.CaseGuardCompleteStep)
next
  case (CaseGuardIncompleteStep ins g ctx s env warnings payments stmts innerG newCtx newS newEnv newWarnings newPayments c)
  then show ?case by (metis isFreshChoiceInCasesGuards.simps(2) minimal_state_eq_preserves_small_step_guard reduceGuardExpressionsConstants small_step_reduce_case.CaseGuardIncompleteStep)
qed

lemma minimal_state_eq_preserves_small_step_cases_halted:
"computeCasesStep inp (c1, ctx1, state1, env1, reduceWarnings1, payments1) = None \<Longrightarrow>
  isFreshChoiceInCasesGuards choiceName c1 \<Longrightarrow>
  state_minimal_eq choiceName state1 state2 \<Longrightarrow>
  computeCasesStep inp (c1, ctx1, state2, env1, reduceWarnings1, payments1) = None"
  apply (induction inp c1 ctx1 state1 env1 reduceWarnings1 payments1 rule: computeCasesStep.induct[split_format(complete)])
   apply (auto split: option.split_asm option.split)
  using isFreshChoiceInCasesGuards.elims(2) apply blast
  by (metis (no_types, lifting) isFreshChoiceInCasesGuards.elims(2) isFreshChoiceInCasesGuards.simps(1) isFreshChoiceInCasesGuards.simps(2) list.distinct(1) list.inject minimal_state_eq_preserves_small_step_case_halted option.distinct(1) state_minimal_eq.elims(3))

lemma minimal_state_eq_preserves_small_step_cases:
"inp \<turnstile> (c1, ctx1, state1, env1, reduceWarnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (c1', ctx1', state1', env1', reduceWarnings1', payments1') \<Longrightarrow>
  isFreshChoiceInCasesGuards choiceName c1 \<Longrightarrow>
  state_minimal_eq choiceName state1 state2 \<Longrightarrow>
  (\<exists>state2' . inp \<turnstile> (c1, ctx1, state2, env1, reduceWarnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (c1', ctx1', state2', env1', reduceWarnings1', payments1') \<and>
    state_minimal_eq choiceName state1' state2')"
proof (induction inp c1 ctx1 state1 env1 reduceWarnings1 payments1 c1' ctx1' state1' env1' reduceWarnings1' payments1'
      arbitrary: state2 rule: small_step_reduce_cases.induct[split_format(complete)])
  case (CasesHeadMatches ins c ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments rest)
  then show ?case by (metis FaustusV2AST.FCase.exhaust isFreshChoiceInCasesGuards.simps(1) isFreshChoiceInCasesGuards.simps(2) minimal_state_eq_preserves_small_step_case small_step_reduce_cases.CasesHeadMatches)
next
  case (CasesTailMatches ins rest ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments c)
  moreover have "isFreshChoiceInCasesGuards choiceName rest" using calculation by (cases c, auto)
  moreover obtain state2' where "ins \<turnstile> (rest, ctx, state2, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC, newCtx, state2', newEnv, newWarnings, newPayments)" using calculation(3)[of state2] calculation(1,2,4,5,6) by auto
  moreover have "computeCaseStep ins (c, ctx, s, env, warnings, payments) = None" by (meson calculation(2) computeCaseStepResImpliesReduceStep option.exhaust)
  moreover have "computeCaseStep ins (c, ctx, state2, env, warnings, payments) = None" by (metis (no_types, lifting) calculation(4) calculation(5) calculation(8) isFreshChoiceInCasesGuards.elims(2) isFreshChoiceInCasesGuards.simps(1) isFreshChoiceInCasesGuards.simps(2) list.distinct(1) list.inject minimal_state_eq_preserves_small_step_case_halted)
  ultimately show ?case by (metis (no_types, lifting) computeCasesStep.simps(2) computeCasesStepResImpliesReduceStep option.simps(4) reduceCasesStepImpliesComputeCaseStepRes)
qed

lemma small_step_cases_preserves_split_cases_relation:
"inp \<turnstile> (c1, ctx1, state1, env1, reduceWarnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (c1', ctx1', state1', env1', reduceWarnings1', payments1') \<Longrightarrow>
  isFreshChoiceInCasesGuards choiceName c1 \<Longrightarrow>
  state_minimal_eq choiceName state1 state2 \<Longrightarrow>
  (\<exists>c2' state2' . inp \<turnstile> (map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) c1, split_cases_context role choiceName k ctx1, state2, env1, reduceWarnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (c2', split_cases_context role choiceName k ctx1', state2', env1', reduceWarnings1', payments1') \<and>
    state_minimal_eq choiceName state1' state2' \<and>
    (c2' t (split_cases role choiceName k tc1) = split_cases role choiceName k (c1' t tc1)))"
proof (induction inp c1 ctx1 state1 env1 reduceWarnings1 payments1 c1' ctx1' state1' env1' reduceWarnings1' payments1'
      arbitrary: state2 rule: small_step_reduce_cases.induct[split_format(complete)])
  case (CasesHeadMatches ins c ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments rest)
  moreover have "isFreshChoiceInCasesGuards choiceName [c]" using calculation(2) by (cases c, auto)
  moreover obtain newC2 newS2 where "ins \<turnstile> ((\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) c, split_cases_context role choiceName k ctx, state2, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 (newC2, split_cases_context role choiceName k newCtx, newS2, newEnv, newWarnings, newPayments) \<and>
    state_minimal_eq choiceName newS newS2 \<and>
    (newC2 t (split_cases role choiceName k tc1) = split_cases role choiceName k (newC t tc1))"
    using small_step_case_preserves_split_cases_relation[of ins c ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments choiceName state2 role k t tc1] calculation by auto
  ultimately show ?case by (metis (mono_tags, lifting) list.simps(9) small_step_reduce_cases.CasesHeadMatches)
next
  case (CasesTailMatches ins rest ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments c)
  moreover have "computeCaseStep ins (c, ctx, s, env, warnings, payments) = None" by (meson calculation(2) computeCaseStepResImpliesReduceStep option.exhaust)
  moreover have "isFreshChoiceInCasesGuards choiceName [(\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) c]" using calculation by (cases c, auto)
  moreover have "computeCaseStep ins ((\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) c, split_cases_context role choiceName k ctx, state2, env, warnings, payments) = None" by (metis FaustusV2AST.FCase.exhaust calculation(4-7) isFreshChoiceInCasesGuards.simps(2)  split_cases_relation_preserves_halted_case)
  moreover have "\<forall>res . \<not> ins \<turnstile> ((\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) c, split_cases_context role choiceName k ctx, state2, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 res" using calculation reduceCaseStepImpliesComputeCaseStepRes by force
  ultimately show ?case by (smt (verit, best) computeCasesStep.simps(2) computeCasesStepResImpliesReduceStep isFreshChoiceInCasesGuards.elims(2) isFreshChoiceInCasesImpliesIsFreshChoiceInCasesGuards list.distinct(1) list.inject list.simps(9) option.simps(4) reduceCasesStepImpliesComputeCaseStepRes)
qed

lemma small_step_action_preserved_inverse_split_cases_relation:
"ins \<turnstile> (a, split_cases_context role choiceName k ctx1, s, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (newS, applyWarning) \<Longrightarrow>
  isFreshChoiceInAction choiceName a \<Longrightarrow>
  state_minimal_eq choiceName s1 s \<Longrightarrow>
  (\<exists>newS1 . ins \<turnstile> (a, ctx1, s1, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (newS1, applyWarning) \<and>
    state_minimal_eq choiceName newS1 newS)"
proof (induction ins a "split_cases_context role choiceName k ctx1" s env newS applyWarning 
    arbitrary: ctx1 s1 rule: small_step_reduce_action.induct[split_format(complete)])
  case (DepositPositiveActionStep env s fValue valRes fAccId fAccIdRes fParty fPartyRes accId mParty fToken mToken mMoney moneyInAcc newBalance newAccs)
  moreover have "evalFValue env s1 fValue = Some valRes \<and>
    evalFParty s1 fAccId = Some fAccIdRes \<and>
    evalFParty s1 fParty = Some fPartyRes" using calculation by (metis fresh_choice_state_preserves_pubkey_evaluation fresh_choice_state_preserves_value_evaluation(1) isFreshChoiceInAction.simps(3) state_minimal_eq.elims(1))
  moreover have "(IDeposit accId mParty mToken mMoney) \<turnstile> (Deposit fAccId fParty fToken fValue, ctx1, s1, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (s1\<lparr>FState.accounts := newAccs\<rparr>, ApplyNoWarning)" using calculation small_step_reduce_action.simps by auto
  ultimately show ?case by auto
next
  case (DepositNonPositiveActionStep env s fValue valRes fAccId fAccIdRes fParty fPartyRes accId mParty fToken mToken mMoney moneyInAcc newBalance newAccs)
  moreover have "evalFValue env s1 fValue = Some valRes \<and>
    evalFParty s1 fAccId = Some fAccIdRes \<and>
    evalFParty s1 fParty = Some fPartyRes" using calculation by (metis fresh_choice_state_preserves_pubkey_evaluation fresh_choice_state_preserves_value_evaluation(1) isFreshChoiceInAction.simps(3) state_minimal_eq.elims(1))
  moreover have "(IDeposit accId mParty mToken mMoney) \<turnstile> (Deposit fAccId fParty fToken fValue, ctx1, s1, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (s1\<lparr>FState.accounts := newAccs\<rparr>, ApplyNonPositiveDeposit mParty accId mToken mMoney)" using calculation small_step_reduce_action.simps by auto
  ultimately show ?case by auto
next
  case (ChoiceActionStep choice bounds s fChoiceId fChoiceIdRes mChoiceId env)
  moreover have "evalFChoiceId s1 fChoiceId = Some fChoiceIdRes" using calculation(2,5) fresh_choice_state_preserves_choiceId_evaluation state_minimal_eq.simps by blast
  moreover have "(IChoice mChoiceId choice) \<turnstile> (Choice fChoiceId bounds, ctx1, s1, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (s1\<lparr>FState.choices := MList.insert mChoiceId choice (FState.choices s1)\<rparr>, ApplyNoWarning)" using small_step_reduce_action.ChoiceActionStep[of choice bounds s1 fChoiceId fChoiceIdRes mChoiceId ctx1 env] calculation by simp
  ultimately show ?case by (smt (verit, ccfv_threshold) FState.select_convs(3) FState.surjective FState.update_convs(2) computeActionStep.simps(2) computeActionStepIsSmallStep minimal_state_eq_preserves_small_step_action option.inject state_minimal_eq_commutative)
next
  case (NotifyActionStep env s obs)
  moreover have "evalFObservation env s1 obs = Some True" by (metis NotifyActionStep.hyps calculation(2) calculation(3) fresh_choice_state_preserves_value_evaluation(2) isFreshChoiceInAction.simps(2))
  ultimately show ?case by (simp add: small_step_reduce_action.simps)
qed

lemma small_step_guard_preserved_inverse_split_cases_relation:
"ins \<turnstile> (g, split_cases_context role choiceName k ctx1, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, innerG), newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
  isFreshChoiceInGuard choiceName g \<Longrightarrow>
  state_minimal_eq choiceName s1 s \<Longrightarrow>
  (\<exists>newCtx1 newS1 . ins \<turnstile> (g, ctx1, s1, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, innerG), newCtx1, newS1, newEnv, newWarnings, newPayments) \<and>
    split_cases_context role choiceName k newCtx1 = newCtx \<and>
    state_minimal_eq choiceName newS1 newS)"
proof (induction ins g "split_cases_context role choiceName k ctx1" s env warnings payments stmts innerG newCtx newS newEnv newWarnings newPayments 
   arbitrary: ctx1 s1
   rule: small_step_reduce_guard.induct[split_format(complete)])
  case (ActionGuardStep g a ins s env newS applyWarning warnings payments)
  moreover obtain newS1 where "ins \<turnstile> (a, ctx1, s1, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (newS1, applyWarning) \<and> state_minimal_eq choiceName newS1 newS" using calculation small_step_action_preserved_inverse_split_cases_relation by fastforce
  moreover have "ins \<turnstile> (g, ctx1, s1, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 (([], None), ctx1, newS1, env, warnings @ convertApplyWarning applyWarning, payments)" using calculation small_step_reduce_guard.ActionGuardStep by auto
  ultimately show ?case by auto
next
  case (GuardThenGuardCompleteFirstGuardStep g g1 g2 ins s env warnings payments stmts newCtx newS newEnv newWarnings newPayments)
  then show ?case by (metis isFreshChoiceInGuard.simps(2) small_step_reduce_guard.GuardThenGuardCompleteFirstGuardStep)
next
  case (GuardThenGuardIncompleteGuard g g1 g2 ins s env warnings payments stmts nextG1 newCtx newS newEnv newWarnings newPayments)
  then show ?case by (metis isFreshChoiceInGuard.simps(2) small_step_reduce_guard.GuardThenGuardIncompleteGuard)
next
  case (GuardStmtsGuardCompleteGuardStep g g1 stmts ins s env warnings payments newStmts newCtx newS newEnv newWarnings newPayments)
  then show ?case by (metis isFreshChoiceInGuard.simps(3) small_step_reduce_guard.GuardStmtsGuardCompleteGuardStep)
next
  case (GuardStmtsGuardIncompleteGuard g g1 stmts ins s env warnings payments newStmts nextG1 newCtx newS newEnv newWarnings newPayments)
  then show ?case by (metis isFreshChoiceInGuard.simps(3) small_step_reduce_guard.GuardStmtsGuardIncompleteGuard)
next
  case (DisjointGuardMatchesG1 ins g1 s env warnings payments a b a a a a b g2)
  then show ?case by (metis isFreshChoiceInGuard.simps(4) small_step_reduce_guard.DisjointGuardMatchesG1)
next
  case (DisjointGuardMatchesG2 ins g1 s env warnings payments g2 a b a a a a b)
  then show ?case by (metis computeGuardStepInvariantsHalted isFreshChoiceInGuard.simps(4) minimal_state_eq_preserves_small_step_guard_halted small_step_reduce_guard.DisjointGuardMatchesG2 state_minimal_eq_commutative)
next
  case (InterleavedGuardMatchesG1Complete ins g1 s env warnings payments stmts newCtx newS newEnv newWarnings newPayments g2)
  then show ?case by (metis isFreshChoiceInGuard.simps(5) small_step_reduce_guard.InterleavedGuardMatchesG1Complete)
next
  case (InterleavedGuardMatchesG1Incomplete ins g1 s env warnings payments stmts innerG1 newCtx newS newEnv newWarnings newPayments g2)
  then show ?case by (metis isFreshChoiceInGuard.simps(5) small_step_reduce_guard.InterleavedGuardMatchesG1Incomplete)
next
  case (InterleavedGuardMatchesG2Complete ins g2 s env warnings payments stmts newCtx newS newEnv newWarnings newPayments g1)
  then show ?case by (metis (no_types, lifting) computeGuardStepInvariantsHalted isFreshChoiceInGuard.simps(5) minimal_state_eq_preserves_small_step_guard_halted small_step_reduce_guard.InterleavedGuardMatchesG2Complete state_minimal_eq_commutative)
next
  case (InterleavedGuardMatchesG2Incomplete ins g2 s env warnings payments stmts innerG2 newCtx newS newEnv newWarnings newPayments g1)
  then show ?case by (metis (no_types, lifting) computeGuardStepInvariantsHalted isFreshChoiceInGuard.simps(5) minimal_state_eq_preserves_small_step_guard_halted small_step_reduce_guard.InterleavedGuardMatchesG2Incomplete state_minimal_eq_commutative)
qed

lemma small_step_case_preserved_inverse_split_cases_relation:
"inp \<turnstile> ((\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) c1, split_cases_context role choiceName k ctx1, state2, env1, reduceWarnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 (c2', ctx2', state2', env2', reduceWarnings2', payments2') \<Longrightarrow>
  isFreshChoiceInCasesGuards choiceName [c1] \<Longrightarrow>
  state_minimal_eq choiceName state1 state2 \<Longrightarrow>
  (\<exists>c1' ctx1' state1' . inp \<turnstile> (c1, ctx1, state1, env1, reduceWarnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 (c1', ctx1', state1', env2', reduceWarnings2', payments2') \<and>
    state_minimal_eq choiceName state1' state2' \<and>
    split_cases_context role choiceName k ctx1' = ctx2' \<and>
    (c2' t (split_cases role choiceName k tc1) = split_cases role choiceName k (c1' t tc1)))"
proof (induction inp "(\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) c1" "split_cases_context role choiceName k ctx1" state2 env1 reduceWarnings1 payments1 c2' ctx2' state2' env2' reduceWarnings2' payments2'
      arbitrary: c1 ctx1 state1  rule: small_step_reduce_case.induct[split_format(complete)])
  case (CaseGuardCompleteStep ins g s env warnings payments stmts newCtx newS newEnv newWarnings newPayments c)
  moreover obtain innerCont1 where "c1 = Case g innerCont1 \<and> split_cases role choiceName k innerCont1 = c" using calculation by (cases c1, auto)
  moreover obtain newS1 where "ins \<turnstile> (g, ctx1, state1, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, None), ctx1, newS1, newEnv, newWarnings, newPayments) \<and>
    state_minimal_eq choiceName newS1 newS"  using small_step_guard_preserved_inverse_split_cases_relation calculation apply auto
    by (smt (verit, best) reduceGuardExpressionsConstants)
  moreover have "ins \<turnstile> (c1, ctx1, state1, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 ((\<lambda>t tcont . prependStatements stmts innerCont1), ctx1, newS1, newEnv, newWarnings, newPayments)" using calculation small_step_reduce_case.CaseGuardCompleteStep by presburger
  moreover have "newCtx = split_cases_context role choiceName k ctx1" using calculation reduceGuardExpressionsConstants by blast
  moreover have "split_cases role choiceName k ((\<lambda>t tcont . prependStatements stmts innerCont1) t tc1) = prependStatements stmts c" using calculation split_cases_distributes_past_prepend_statements by auto
  ultimately show ?case by metis
next
  case (CaseGuardIncompleteStep ins g s env warnings payments stmts innerG newCtx newS newEnv newWarnings newPayments c)
  moreover obtain innerCont1 where "c1 = Case g innerCont1 \<and> split_cases role choiceName k innerCont1 = c" using calculation by (cases c1, auto)
  moreover obtain newS1 where "ins \<turnstile> (g, ctx1, state1, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some innerG), ctx1, newS1, newEnv, newWarnings, newPayments) \<and>
    state_minimal_eq choiceName newS1 newS"  using small_step_guard_preserved_inverse_split_cases_relation calculation apply auto
    by (smt (verit, best) reduceGuardExpressionsConstants)
  moreover have "ins \<turnstile> (c1, ctx1, state1, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 ((\<lambda>t tcont . prependStatements stmts (FaustusV2AST.FContract.When [Case innerG innerCont1] t tcont)), ctx1, newS1, newEnv, newWarnings, newPayments)" using calculation small_step_reduce_case.CaseGuardIncompleteStep by auto
  moreover have "newCtx = split_cases_context role choiceName k ctx1" using calculation reduceGuardExpressionsConstants by blast
  moreover have "split_cases role choiceName k (When [FaustusV2AST.FCase.Case innerG innerCont1] t tc1) = (When [FaustusV2AST.FCase.Case innerG (split_cases role choiceName k innerCont1)] t (split_cases role choiceName k tc1))" by (cases "k = 0", auto)
  moreover have "split_cases role choiceName k ((\<lambda>t tcont . prependStatements stmts (When [Case innerG innerCont1] t tcont)) t tc1) = (prependStatements stmts (When [FaustusV2AST.FCase.Case innerG c] t (split_cases role choiceName k tc1)))" using calculation apply auto
    using split_cases_distributes_past_prepend_statements[of stmts role choiceName k "(FaustusV2AST.FContract.When [FaustusV2AST.FCase.Case innerG innerCont1] t tc1)"] by (metis calculation(9))
  ultimately show ?case by metis
qed

lemma small_step_cases_preserved_inverse_split_cases_relation:
"inp \<turnstile> (map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) c1, split_cases_context role choiceName k ctx1, state2, env1, reduceWarnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (c2', ctx2', state2', env2', reduceWarnings2', payments2') \<Longrightarrow>
  isFreshChoiceInCasesGuards choiceName c1 \<Longrightarrow>
  state_minimal_eq choiceName state1 state2 \<Longrightarrow>
  (\<exists>c1' ctx1' state1' . inp \<turnstile> (c1, ctx1, state1, env1, reduceWarnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (c1', ctx1', state1', env2', reduceWarnings2', payments2') \<and>
    state_minimal_eq choiceName state1' state2' \<and>
    split_cases_context role choiceName k ctx1' = ctx2' \<and>
    (c2' t (split_cases role choiceName k tc1) = split_cases role choiceName k (c1' t tc1)))"
proof (induction inp "map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) c1" "split_cases_context role choiceName k ctx1" state2 env1 reduceWarnings1 payments1 c2' ctx2' state2' env2' reduceWarnings2' payments2'
      arbitrary: c1 ctx1 state1  rule: small_step_reduce_cases.induct[split_format(complete)])
  case (CasesHeadMatches ins c s env warnings payments newC newCtx newS newEnv newWarnings newPayments rest)
  moreover obtain c1Head c1Rest where "c1 = c1Head#c1Rest \<and>
    isFreshChoiceInCasesGuards choiceName [c1Head] \<and>
    (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) c1Head = c" using calculation apply (cases c1, auto)
    by (metis FaustusV2AST.FCase.exhaust isFreshChoiceInCasesGuards.simps(1) isFreshChoiceInCasesGuards.simps(2))
  moreover obtain newC1 newCtx1 newS1 where "ins \<turnstile> (c1Head, ctx1, state1, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 (newC1, newCtx1, newS1, newEnv, newWarnings, newPayments) \<and>
    state_minimal_eq choiceName newS1 newS \<and>
    split_cases role choiceName k (newC1 t tc1) = newC t (split_cases role choiceName k tc1) \<and>
    split_cases_context role choiceName k newCtx1 = newCtx" using small_step_case_preserved_inverse_split_cases_relation[of ins role choiceName k c1Head ctx1 s env warnings payments newC newCtx newS newEnv newWarnings newPayments state1 t tc1] calculation by (cases c1Head, auto, blast)
  ultimately show ?case by (metis small_step_reduce_cases.CasesHeadMatches)
next
  case (CasesTailMatches ins rest s env warnings payments newC newCtx newS newEnv newWarnings newPayments c)
  moreover obtain c1Head c1Rest where "c1 = c1Head#c1Rest \<and>
    isFreshChoiceInCasesGuards choiceName [c1Head] \<and>
    isFreshChoiceInCasesGuards choiceName c1Rest \<and>
    (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) c1Head = c \<and>
    map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) c1Rest = rest" using calculation apply (cases c1, auto)
    by (metis FaustusV2AST.FCase.exhaust isFreshChoiceInCasesGuards.simps(1) isFreshChoiceInCasesGuards.simps(2))
  moreover obtain newC1 newCtx1 newS1 where "ins \<turnstile> (c1Rest, ctx1, state1, env, warnings,
               payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC1, newCtx1, newS1, newEnv, newWarnings, newPayments) \<and>
       state_minimal_eq choiceName newS1 newS \<and>
       split_cases_context role choiceName k newCtx1 = newCtx \<and>
       newC t (split_cases role choiceName k tc1) = split_cases role choiceName k (newC1 t tc1)" using calculation by blast
  moreover have c1CaseStepImpC2CaseStep: "\<forall> res . ins \<turnstile> (c1Head, ctx1, state1, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 res \<longrightarrow> (\<exists>res2 . ins \<turnstile> (c, split_cases_context role choiceName k ctx1, s, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 res2)" using small_step_case_preserves_split_cases_relation[of ins c1Head ctx1 state1 env warnings payments] calculation apply auto
    by meson
  ultimately show ?case by (metis (no_types, opaque_lifting) small_step_reduce_cases.CasesTailMatches)
qed

lemma small_step_cases_split_list_matches_one_side:
"inp \<turnstile> (c, ctx1, state1, env1, reduceWarnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (c', ctx1', state1', env1', reduceWarnings1', payments1') \<Longrightarrow>
  c = c1@c2 \<Longrightarrow>
  inp \<turnstile> (c1, ctx1, state1, env1, reduceWarnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (c', ctx1', state1', env1', reduceWarnings1', payments1') \<or> 
  (inp \<turnstile> (c2, ctx1, state1, env1, reduceWarnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (c', ctx1', state1', env1', reduceWarnings1', payments1') \<and> \<not> (\<forall>c' ctx1' state1' env1' reduceWarnings1' payments1'. inp \<turnstile> (c1, ctx1, state1, env1, reduceWarnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (c', ctx1', state1', env1', reduceWarnings1', payments1')))"
proof (induction inp c ctx1 state1 env1 reduceWarnings1 payments1 c' ctx1' state1' env1' reduceWarnings1' payments1' arbitrary: c1 c2 rule: small_step_reduce_cases.induct[split_format(complete)])
  case (CasesHeadMatches ins c ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments rest)
  then show ?case apply (induction c1, auto simp add: small_step_reduce_cases.CasesHeadMatches)
    by blast
next
  case (CasesTailMatches ins rest ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments c)
  then show ?case apply auto
     apply (induction c1, auto simp add: small_step_reduce_cases.CasesTailMatches)
     apply (metis CasesTailMatches.hyps(2) small_step_reduce_cases.CasesTailMatches)
    by blast
qed

lemma small_step_action_fresh_choice_state_eq:
"(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (Choice (FChoiceId choiceName (Role role)) [(0, 0)], ctx, state, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (state\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices state) \<rparr>, ApplyNoWarning) \<and>
  state_minimal_eq choiceName state (state\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices state) \<rparr>)"
proof -
  have "inBounds 0 [(0,0)]" by auto
  moreover have "evalFChoiceId state (FChoiceId choiceName (Role role)) = Some (ChoiceId choiceName (Party.Role role))" by auto
  moreover have "state_minimal_eq choiceName state (state\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices state) \<rparr>)" by (auto simp add: insert_lookup_different)
  ultimately show ?thesis by (auto simp add: ChoiceActionStep)
qed

lemma react_choice_maintains_split_cases:
"isFreshChoiceInCasesGuards choiceName cases \<Longrightarrow>
  (IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (cases @ [Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) innerC], ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (\<lambda>innerT innerTc . innerC, ctx, state\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices state) \<rparr>, env, warnings, payments)"
proof (induction cases)
  case Nil
  moreover have "(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (Choice (FChoiceId choiceName (Role role)) [(0, 0)], ctx, state, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (state\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices state) \<rparr>, ApplyNoWarning)" by (simp add: small_step_action_fresh_choice_state_eq)
  moreover have "(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)]), ctx, state, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 (([], None), ctx, state\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices state) \<rparr>, env, warnings @ (convertApplyWarning ApplyNoWarning), payments)" using ActionGuardStep calculation(2) by blast
  moreover have "(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) innerC, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 (\<lambda>innerT innerTc . (prependStatements [] innerC), ctx, state\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices state) \<rparr>, env, warnings @ (convertApplyWarning ApplyNoWarning), payments)" using CaseGuardCompleteStep calculation(3) by presburger
  moreover have "prependStatements [] innerC = innerC" by auto
  moreover have "[] @ [Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) innerC] = [Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) innerC]" by auto
  ultimately show ?case using CasesHeadMatches by force
next
  case (Cons a cases)
  moreover have "finalCases (IChoice (ChoiceId choiceName (Party.Role role)) 0) (a#cases, ctx, state, env, warnings, payments)" by (simp add: Cons.prems freshChoiceNoMatch_cases_step)
  moreover have "(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (Choice (FChoiceId choiceName (Role role)) [(0, 0)], ctx, state, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (state\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices state) \<rparr>, ApplyNoWarning)" by (simp add: small_step_action_fresh_choice_state_eq)
  moreover have "(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)]), ctx, state, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 (([], None), ctx, state\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices state) \<rparr>, env, warnings @ (convertApplyWarning ApplyNoWarning), payments)" using ActionGuardStep calculation by blast
  moreover have "(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) innerC, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 (\<lambda>innerT innerTc . (prependStatements [] innerC), ctx, state\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices state) \<rparr>, env, warnings @ (convertApplyWarning ApplyNoWarning), payments)" using CaseGuardCompleteStep calculation by presburger
  moreover have "(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> ([Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) innerC], ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (\<lambda>innerT innerTc . (prependStatements [] innerC), ctx, state\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices state) \<rparr>, env, warnings @ (convertApplyWarning ApplyNoWarning), payments)" using CasesHeadMatches calculation(6) by auto
  ultimately show ?case by (metis (no_types, lifting) casesStepAppendFinalCases freshChoiceNoMatch_cases_step_aux isFreshChoiceInCasesGuards.elims(2) list.distinct(1) list.inject small_step_cases_split_list_matches_one_side)
qed

lemma react_choice_map_split_maintains_split_cases:
"isFreshChoiceInCasesGuards choiceName cases \<Longrightarrow>
  (IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (take l (map (\<lambda>Case g c \<Rightarrow> Case g (split_cases role choiceName k c)) cases) @ [Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) innerC], ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (\<lambda>innerT innerTc . innerC, ctx, state\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices state) \<rparr>, env, warnings, payments)"
proof (induction cases arbitrary: l)
  case Nil
  moreover have "(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (Choice (FChoiceId choiceName (Role role)) [(0, 0)], ctx, state, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (state\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices state) \<rparr>, ApplyNoWarning)" by (simp add: small_step_action_fresh_choice_state_eq)
  moreover have "(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)]), ctx, state, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 (([], None), ctx, state\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices state) \<rparr>, env, warnings @ (convertApplyWarning ApplyNoWarning), payments)" using ActionGuardStep calculation(2) by blast
  moreover have "(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) innerC, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 (\<lambda>innerT innerTc . (prependStatements [] innerC), ctx, state\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices state) \<rparr>, env, warnings @ (convertApplyWarning ApplyNoWarning), payments)" using CaseGuardCompleteStep calculation(3) by presburger
  moreover have "prependStatements [] innerC = innerC" by auto
  moreover have "[] @ [Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) innerC] = [Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) innerC]" by auto
  ultimately show ?case using CasesHeadMatches by force
next
  case (Cons a cases)
  moreover have "finalCases (IChoice (ChoiceId choiceName (Party.Role role)) 0) (a#cases, ctx, state, env, warnings, payments)" by (simp add: Cons.prems freshChoiceNoMatch_cases_step)
  moreover have "(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (Choice (FChoiceId choiceName (Role role)) [(0, 0)], ctx, state, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (state\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices state) \<rparr>, ApplyNoWarning)" by (simp add: small_step_action_fresh_choice_state_eq)
  moreover have "(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)]), ctx, state, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 (([], None), ctx, state\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices state) \<rparr>, env, warnings @ (convertApplyWarning ApplyNoWarning), payments)" using ActionGuardStep calculation by blast
  moreover have "(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) innerC, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 (\<lambda>innerT innerTc . (prependStatements [] innerC), ctx, state\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices state) \<rparr>, env, warnings @ (convertApplyWarning ApplyNoWarning), payments)" using CaseGuardCompleteStep calculation by presburger
  moreover have "(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> ([Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) innerC], ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (\<lambda>innerT innerTc . (prependStatements [] innerC), ctx, state\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices state) \<rparr>, env, warnings @ (convertApplyWarning ApplyNoWarning), payments)" using CasesHeadMatches calculation(6) by auto
  moreover have "(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (take (l-1) (map (\<lambda>a. case a of Case g c \<Rightarrow> Case g (split_cases role choiceName k c)) cases) @ [FaustusV2AST.FCase.Case (ActionGuard (FAction.Choice (FChoiceId choiceName (FParty.Role role)) [(0, 0)])) innerC], ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (\<lambda>innerT innerTc. innerC, ctx, state \<lparr>FState.choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (FState.choices state)\<rparr>, env, warnings, payments)" using Cons.IH Cons.prems isFreshChoiceInCasesGuards.elims by (smt (verit, ccfv_SIG) isFreshChoiceInCasesImpliesIsFreshChoiceInCasesGuards list.inject neq_Nil_conv)
  moreover have "l > 0 \<longrightarrow> take l (map (\<lambda>a. case a of Case g c \<Rightarrow> Case g (split_cases role choiceName k c)) (a # cases)) = ((\<lambda>a. case a of Case g c \<Rightarrow> Case g (split_cases role choiceName k c)) a) # (take (l-1) (map (\<lambda>a. case a of Case g c \<Rightarrow> Case g (split_cases role choiceName k c)) (cases)))" by (cases a, auto simp add: take_Cons')
  moreover have "l \<le> 0 \<longrightarrow> take l (map (\<lambda>a. case a of Case g c \<Rightarrow> Case g (split_cases role choiceName k c)) (a # cases)) = []" by auto
  moreover have "\<forall> res . \<not> (IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> ((\<lambda>a. case a of Case g c \<Rightarrow> Case g (split_cases role choiceName k c)) a, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 res" using calculation apply (cases a, auto)
    by (metis (no_types, lifting) computeCaseStep.simps computeGuardStepHaltedImpliesSmallStepHalted freshChoiceNoMatch_guard_step option.distinct(1) option.simps(4) reduceCaseStepImpliesComputeCaseStepRes)+
  ultimately show ?case using CasesTailMatches by auto
qed

lemma fresh_choice_no_match_small_step_receive:
"isFreshChoiceInProcess choiceName p1 \<Longrightarrow>
  (\<nexists>p1' . p1 \<rightarrow>\<^sup>(Some (((IChoice ((ChoiceId choiceName (SemanticsTypes.Role role))) 0))), lower, upper) p1')"
  by (meson basic_receive_input_not_fresh_choice)

lemma fresh_choice_no_match_full_semantics:
"(A \<turnstile> p1 \<midarrow>\<^sup>\<lambda>\<^sup>(Some (((IChoice ((ChoiceId choiceName (SemanticsTypes.Role role))) 0))), lower, upper)\<rightarrow> p1') \<Longrightarrow>
  isFreshChoiceInProcess choiceName p1 \<Longrightarrow>
  False"
  apply (induction A p1 "\<lambda>\<^sup>(Some (((IChoice ((ChoiceId choiceName (SemanticsTypes.Role role))) 0))), lower, upper)" p1' rule: full_semantics.induct)
  by (meson fresh_choice_no_match_small_step_receive basic_receive_preserves_fresh_choice)+

lemma fresh_choice_no_match_full_nd_semantics:
"(A \<turnstile> p1 \<midarrow>\<midarrow>\<^sup>\<lambda>\<^sup>(Some (((IChoice ((ChoiceId choiceName (SemanticsTypes.Role role))) 0))), lower, upper)\<rightarrow> p1') \<Longrightarrow>
  isFreshChoiceInProcess choiceName p1 \<Longrightarrow>
  False"
  apply (induction A p1 "\<lambda>\<^sup>(Some (((IChoice ((ChoiceId choiceName (SemanticsTypes.Role role))) 0))), lower, upper)" p1' rule: full_nd_semantics.induct)
  by (meson fresh_choice_no_match_small_step_receive basic_receive_preserves_fresh_choice)+

inductive_cases FullSemanticsLambdaE[elim]: "A \<turnstile> (c1, ctx1, state1, env2, warnings2, payments2) \<midarrow>\<^sup>\<lambda>\<^sup>(a, aa, b)\<rightarrow> (c1', ctx1', state1', env1', warnings1', payments1')"
inductive_cases FullSemanticsTauE[elim]: "A \<turnstile> (c1, ctx1, state1, env2, warnings2, payments2) \<midarrow>\<^sup>\<tau>\<rightarrow> (c1', ctx1', state1', env1', warnings1', payments1')"
inductive_cases ReceiveWhenSomeElim: "(FaustusV2AST.FContract.When cases t cont, ctx1, state1, env1, warnings1, payments1) \<rightarrow>\<^sup>(Some inp, lower, upper) (c1', ctx1', state1', env1', warnings1', payments1')"

lemma receive_semantics_strongly_maintains_split_cases_NoChoiceLambdaReceive:
"split_cases role choiceName k c1 = c2 \<Longrightarrow>
  split_cases_context role choiceName k ctx1 = ctx2 \<Longrightarrow>
  state_minimal_eq choiceName state1 state2 \<Longrightarrow>
  env1 = env2 \<Longrightarrow>
  warnings1 = warnings2 \<Longrightarrow>
  payments1 = payments2 \<Longrightarrow>
  isFreshChoiceInContract choiceName c1 \<Longrightarrow>
  isFreshChoiceInContext choiceName ctx1 \<Longrightarrow>
  (c1, ctx1, state1, env1, warnings1, payments1) \<rightarrow>\<^sup>(Some inp, lower, upper) (c1', ctx1', state1', env1', warnings1', payments1') \<Longrightarrow>
  split_cases_relation role choiceName k (c1, ctx1, state1, env1, warnings1, payments1) (c2, ctx2, state2, env2, warnings2, payments2) \<Longrightarrow>
  \<exists>c2' ctx2' state2' env2' warnings2' payments2'.
    ({(IChoice (ChoiceId choiceName (Party.Role role)) 0)} \<turnstile> (c2, ctx2, state2, env2, warnings2, payments2) \<midarrow>\<^sup>\<lambda>\<^sup>(Some inp, lower, upper)\<rightarrow> (c2', ctx2', state2', env2', warnings2', payments2')) \<and>
    split_cases_relation role choiceName k (c1', ctx1', state1', env1', warnings1', payments1') (c2', ctx2', state2', env2', warnings2', payments2')"
proof (induction role choiceName k c1 arbitrary: c2 ctx1 ctx2 state1 state2 env1 env2 warnings1 warnings2 payments1 payments2 inp lower upper c1' ctx1' state1' env1' warnings1' payments1' rule: split_cases.induct)
  case (1 role choiceName k cases t cont)
  moreover obtain cases2 tCont2 where decompile_c2: "c2 = When cases2 t tCont2" using 1(4) by (cases "1 < k \<and> k < length cases", auto)
  moreover have small_k_cases2_is_take_k: "1 < k \<and> k < length cases \<longrightarrow> cases2 = (take k (map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) cases)) @ [Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) (split_cases role choiceName k (When (drop k cases) t cont))]" using 1(4) decompile_c2 apply (auto)
    by metis
  moreover have large_k_cases2_is_map: "\<not>(1 < k \<and> k < length cases) \<longrightarrow> cases2 = (map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) cases)" using 1(4) decompile_c2 by auto
  moreover obtain newTimeState1 newTimeState2 newTimeEnv1 newTimeEnv2 where new_time_states: "fixFInterval (lower, upper) state1 = (newTimeEnv1, newTimeState1) \<and>
    fixFInterval (lower, upper) state2 = (newTimeEnv2, newTimeState2) \<and>
    state_minimal_eq choiceName newTimeState1 newTimeState2 \<and>
    newTimeEnv1 = newTimeEnv2" using 1(6,7) apply (cases "lower < (FState.minTime state1)", auto)
    by (metis (no_types, lifting) FState.surjective FState.update_convs(5))
  moreover obtain realLower realUpper where real_lower_upper_obtained: "timeInterval newTimeEnv1 = (realLower, realUpper) \<and> realLower < t \<and> realUpper < t" using 1(12) new_time_states ReceiveWhenSomeElim by auto
  moreover have is_fresh_in_map_split: "isFreshChoiceInCasesGuards choiceName (map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) cases)" using 1(10) by (induction cases, auto split: FCase.split)
  moreover have is_fresh_in_take_k_map_split: "isFreshChoiceInCasesGuards choiceName (take k (map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) cases))" using is_fresh_in_map_split apply (induction cases arbitrary: k, auto split: FCase.split)
    by (metis append_take_drop_id isFreshChoiceInCasesGuards.simps(2) isFreshChoiceInCasesGuardsPreservedInSlice)
  moreover have "\<not>(\<forall>c2' . (IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> ((map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) cases), ctx2, newTimeState2, newTimeEnv2, warnings2, payments2) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 c2')" using is_fresh_in_map_split freshChoiceNoMatch_cases_step_aux by blast
  moreover have "\<not>(\<forall>c2' . (IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (take k (map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) cases), ctx2, newTimeState2, newTimeEnv2, warnings2, payments2) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 c2')" using is_fresh_in_take_k_map_split freshChoiceNoMatch_cases_step_aux by blast
  moreover have choice_step_take_k_map_cases:"(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> ((take k (map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) cases)) @ [Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) (split_cases role choiceName k (When (drop k cases) t cont))], ctx2, newTimeState2, newTimeEnv2, warnings2, payments2) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (\<lambda>passedT tCont. (split_cases role choiceName k (When (drop k cases) t cont)), ctx2, newTimeState2\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices newTimeState2) \<rparr>, newTimeEnv2, warnings2, payments2)"
    using is_fresh_in_take_k_map_split react_choice_maintains_split_cases by presburger
  moreover have small_k_cases_step_choice:"1 < k \<and> k < length cases \<longrightarrow> (IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (cases2, ctx2, newTimeState2, newTimeEnv2, warnings2, payments2) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (\<lambda>passedT passedTCont. (split_cases role choiceName k (When (drop k cases) t cont)), ctx2, newTimeState2\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices newTimeState2) \<rparr>, newTimeEnv2, warnings2, payments2)"
    using choice_step_take_k_map_cases small_k_cases2_is_take_k by auto
  moreover obtain newC1' where cases_step: "inp \<turnstile> (cases, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC1', ctx1', state1', env1', warnings1', payments1') \<and> newC1' t cont = c1'" using 1(12) new_time_states by (auto elim: ReceiveWhenSomeElim)
  moreover have cases_step_constants: "ctx1 = ctx1' \<and> newTimeEnv1 = env1' \<and> payments1 = payments1'" using cases_step by (auto simp add: reduceCasesConstants)
  moreover obtain newC2' newTimeState2' where no_split_cases2_step: "inp \<turnstile> ((map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) cases), ctx2, newTimeState2, newTimeEnv2, warnings2, payments2) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC2', split_cases_context role choiceName k ctx1', newTimeState2', env1', warnings1', payments1') \<and> (\<forall>t tCont. newC2' t (split_cases role choiceName k tCont) = split_cases role choiceName k (newC1' t tCont)) \<and> state_minimal_eq choiceName state1' newTimeState2'"
    using cases_step small_step_cases_preserves_split_cases_relation[of inp cases ctx1 newTimeState1 newTimeEnv1 warnings1 payments1 newC1' ctx1' state1' env1' warnings1' payments1' choiceName newTimeState2 role k] new_time_states 1(4-) isFreshChoiceInCasesImpliesIsFreshChoiceInCasesGuards apply auto
    by (metis deterministicCasesSemantics prod.inject)
  moreover have no_split_lemma_proved: "split_cases_relation role choiceName k (c1', ctx1', state1', env1', warnings1', payments1') (newC2' t (split_cases role choiceName k cont), split_cases_context role choiceName k ctx1', newTimeState2', env1', warnings1', payments1')" using no_split_cases2_step cases_step_constants 1(10,11) cases_step freshChoicePreserved_cases_step by auto
  moreover have drop_in_split_cases_relation: "split_cases_relation role choiceName k (When (drop k cases) t cont, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) (split_cases role choiceName k (When (drop k cases) t cont), ctx2, newTimeState2\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices newTimeState2) \<rparr>, newTimeEnv2, warnings2, payments2)" using 1(4-) new_time_states apply auto
    using 1(4-) apply (metis append_take_drop_id isFreshChoiceInCasesPreservedInSlice)
    by (metis ChoiceId.inject insert_lookup_different)
  moreover have is_fresh_choice_in_take_k_cases: "isFreshChoiceInCases choiceName (take k cases)" using 1(10) by (metis append_take_drop_id isFreshChoiceInCasesPreservedInSlice isFreshChoiceInContract.simps(4))
  moreover obtain takeKNewC2' takeKNewTimeState2' where take_k_cases_step_imp_step_in_relation: "inp \<turnstile> (take k cases, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC1', ctx1', state1', env1', warnings1', payments1') \<longrightarrow> inp \<turnstile> (take k (map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) cases), ctx2, newTimeState2, newTimeEnv2, warnings2, payments2) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (takeKNewC2', split_cases_context role choiceName k ctx1', takeKNewTimeState2', env1', warnings1', payments1') \<and> (\<forall>t tCont. takeKNewC2' t (split_cases role choiceName k tCont) = split_cases role choiceName k (newC1' t tCont)) \<and> state_minimal_eq choiceName state1' takeKNewTimeState2'"
    using cases_step small_step_cases_preserves_split_cases_relation[of inp "take k cases" ctx1 newTimeState1 newTimeEnv1 warnings1 payments1 newC1' ctx1' state1' env1' warnings1' payments1' choiceName newTimeState2 role k] new_time_states is_fresh_choice_in_take_k_cases 1(4-) isFreshChoiceInCasesImpliesIsFreshChoiceInCasesGuards take_map[of k "(\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c)))" cases] by (metis (no_types, lifting) Pair_inject deterministicCasesSemantics)
  moreover have no_match_take_k_match_drop_k: "finalCases inp (take k cases, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) \<longrightarrow> inp \<turnstile> (drop k cases, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC1', ctx1', state1', env1', warnings1', payments1')" by (metis append_take_drop_id cases_step finalCases_def small_step_cases_split_list_matches_one_side)
  moreover have finalCases_cases2: "finalCases inp (take k cases, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) \<longrightarrow> finalCases inp (take k (map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) cases), ctx2, newTimeState2, newTimeEnv2, warnings2, payments2)" using small_step_cases_preserved_inverse_split_cases_relation[of inp role choiceName k "take k cases" ctx1 newTimeState2 newTimeEnv2 warnings2 payments2 _ _ _ _ _ _ newTimeState1 t cont] new_time_states is_fresh_choice_in_take_k_cases 1(4-) isFreshChoiceInCasesImpliesIsFreshChoiceInCasesGuards take_map[of k "(\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c)))" cases] finalCases_def apply auto
    by presburger
  moreover have fixFInterval_fixed_point: "fixFInterval (lower, upper) newTimeState1 = (newTimeEnv1, newTimeState1)" using new_time_states apply (cases "lower < minTime state1", meson)
    by auto
  moreover have final_take_k_implies_drop_k_transition_to_final: "finalCases inp (take k cases, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) \<longrightarrow> (When (drop k cases) t cont, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) \<rightarrow>\<^sup>(Some inp, lower, upper) (c1', ctx1', state1', env1', warnings1', payments1')" by (metis LambdaReceive real_lower_upper_obtained fixFInterval_fixed_point cases_step no_match_take_k_match_drop_k)
  moreover have fixFInterval_fixed_point2: "fixFInterval (lower, upper) (newTimeState2\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices newTimeState2) \<rparr>) = (newTimeEnv2, newTimeState2\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices newTimeState2) \<rparr>)" using new_time_states apply auto
    apply (cases "lower < minTime state2"; auto)
     apply (metis (no_types, lifting) FState.surjective FState.update_convs(2) FState.update_convs(5))
    by (metis (no_types, lifting) FState.surjective FState.update_convs(2) FState.update_convs(5))
  moreover obtain newCases newTCont where newCases_newTCont_obtained: "newCases = map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) cases \<and> newTCont = split_cases role choiceName k cont" by auto
  moreover obtain dropState2' where dropState2'_obtained: "1 < k \<and> k < length cases \<and> finalCases inp (take k cases, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) \<longrightarrow> ({IChoice (ChoiceId choiceName (Party.Role role)) 0} \<turnstile> (split_cases role choiceName k (When (drop k cases) t cont), ctx2, newTimeState2\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices newTimeState2) \<rparr>, newTimeEnv2, warnings2, payments2) \<midarrow>\<^sup>\<lambda>\<^sup>(Some inp, lower, upper)\<rightarrow> (split_cases role choiceName k c1', split_cases_context role choiceName k ctx1', dropState2', env1', warnings1', payments1')) \<and> split_cases_relation role choiceName k (c1', ctx1', state1', env1', warnings1', payments1') (split_cases role choiceName k c1', split_cases_context role choiceName k ctx1', dropState2', env1', warnings1', payments1')"
    using 1(3)[of newCases newTCont "split_cases role choiceName k (When (drop k cases) t cont)" ctx1 ctx2 newTimeState1 "newTimeState2\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices newTimeState2) \<rparr>" newTimeEnv1 newTimeEnv2 warnings1 warnings2 payments1 payments2 inp lower upper c1' ctx1' state1' env1' warnings1' payments1'] drop_in_split_cases_relation final_take_k_implies_drop_k_transition_to_final newCases_newTCont_obtained by (auto simp del: state_minimal_eq.simps split_cases.simps)
  moreover have inp_not_choice: "inp \<noteq> (IChoice (ChoiceId choiceName (Party.Role role)) 0)" using freshChoiceMatchNoChoiceInput_cases_step is_fresh_in_map_split no_split_cases2_step by blast
  moreover have computeCasesStep_inp_choice_None: "computeCasesStep inp ([Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) (split_cases role choiceName k (When (drop k cases) t cont))], ctx2, newTimeState2, newTimeEnv2, warnings2, payments2) = None" using inp_not_choice apply (auto split: option.split)
    by (cases inp, auto split: if_split_asm)
  moreover have finalCases_inp_choice: "finalCases inp ([Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) (split_cases role choiceName k (When (drop k cases) t cont))], ctx2, newTimeState2, newTimeEnv2, warnings2, payments2)" by (metis (no_types, lifting) computeCasesStep_inp_choice_None finalCases_def option.distinct(1) reduceCasesStepImpliesComputeCaseStepRes)
  moreover have finalCases_take_append_choice: "finalCases inp (take k cases, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) \<longrightarrow> finalCases inp ((take k (map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) cases)) @ [Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) (split_cases role choiceName k (When (drop k cases) t cont))], ctx2, newTimeState2, newTimeEnv2, warnings2, payments2)" using finalCases_cases2 choice_step_take_k_map_cases small_k_cases2_is_take_k finalCases_inp_choice finalCasesAppend by auto
  moreover have finalCases2_no_match_take_split: "1 < k \<and> k < length cases \<and> finalCases inp (take k cases, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) \<longrightarrow> finalCases inp (cases2, ctx2, newTimeState2, newTimeEnv2, warnings2, payments2)" using small_k_cases2_is_take_k finalCases_take_append_choice by (auto simp del: split_cases.simps)
  moreover have split_halted_level_1: "1 < k \<and> k < length cases \<and> finalCases inp (take k cases, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) \<longrightarrow> (\<nexists>f . (c2, ctx2, state2, env2, warnings2, payments2) \<rightarrow>\<^sup>(Some inp, lower, upper) f)" using decompile_c2 finalCases_cases2 choice_step_take_k_map_cases small_k_cases2_is_take_k finalCases2_no_match_take_split new_time_states real_lower_upper_obtained by (auto simp only: small_step_receive_ccs.simps finalCases_def)
  moreover have split_cases_choice_step_lambda_receive: "1 < k \<and> k < length cases \<longrightarrow> (c2, ctx2, state2, env2, warnings2, payments2) \<rightarrow>\<^sup>(Some (IChoice (ChoiceId choiceName (Party.Role role)) 0), lower, upper) (split_cases role choiceName k (When (drop k cases) t cont), ctx2, newTimeState2\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices newTimeState2) \<rparr>, newTimeEnv2, warnings2, payments2)" using small_step_receive_ccs.LambdaReceive[of lower upper state2 newTimeEnv2 newTimeState2 realLower realUpper t "IChoice (ChoiceId choiceName (Party.Role role)) 0" cases2 ctx2 warnings2 payments2 "\<lambda>passedT passedTCont. split_cases role choiceName k (FaustusV2AST.FContract.When (drop k cases) t cont)" ctx2 "newTimeState2\<lparr>FState.choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (FState.choices newTimeState2)\<rparr>" newTimeEnv2 warnings2 payments2 "(Some (IChoice (ChoiceId choiceName (Party.Role role)) 0), lower, upper)" tCont2 env2] small_k_cases2_is_take_k decompile_c2 new_time_states small_k_cases_step_choice real_lower_upper_obtained newCases_newTCont_obtained by (auto simp del: split_cases.simps fixFInterval.simps state_minimal_eq.simps)
  moreover have full_hidden_step_lemma_proved: "1 < k \<and> k < length cases \<and> finalCases inp (take k cases, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) \<longrightarrow> ({IChoice (ChoiceId choiceName (Party.Role role)) 0} \<turnstile> (c2, ctx2, state2, env2, warnings2, payments2) \<midarrow>\<^sup>\<lambda>\<^sup>(Some inp, lower, upper)\<rightarrow> (split_cases role choiceName k c1', split_cases_context role choiceName k ctx1', dropState2', env1', warnings1', payments1'))"
    using full_semantics.RestrictedReceiveFull[of inp "{IChoice (ChoiceId choiceName (Party.Role role)) 0}" "(c2, ctx2, state2, env2, warnings2, payments2)" "(Some inp, lower, upper)" lower upper "(split_cases role choiceName k (When (drop k cases) t cont), ctx2, newTimeState2\<lparr> choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices newTimeState2) \<rparr>, newTimeEnv2, warnings2, payments2)" "(split_cases role choiceName k c1', split_cases_context role choiceName k ctx1', dropState2', env1', warnings1', payments1')"] split_halted_level_1 small_k_cases2_is_take_k decompile_c2 dropState2'_obtained new_time_states real_lower_upper_obtained singletonD singletonI small_k_cases_step_choice fixFInterval_fixed_point2 split_cases_choice_step_lambda_receive apply (auto simp del: split_cases.simps state_minimal_eq.simps fixFInterval.simps)
    by blast
  moreover have "inp \<turnstile> (take k cases, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC1', ctx1', state1', env1', warnings1', payments1') \<longrightarrow> (When ((take k (map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) cases)) @ [Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) (split_cases role choiceName k (When (drop k cases) t cont))]) t tCont2, ctx2, state2, env2, warnings2, payments2) \<rightarrow>\<^sup>(Some inp, lower, upper) (takeKNewC2' t tCont2, split_cases_context role choiceName k ctx1', takeKNewTimeState2', env1', warnings1', payments1')" using decompile_c2 new_time_states take_k_cases_step_imp_step_in_relation casesStepImpliesAppendCasesStep small_step_receive_ccs.LambdaReceive real_lower_upper_obtained by (auto)
  moreover have "\<not>finalCases inp (take k cases, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) \<longrightarrow> inp \<turnstile> (take k cases, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC1', ctx1', state1', env1', warnings1', payments1')" using cases_step append_take_drop_id[of k cases] deterministicCasesSemantics casesStepImpliesAppendCasesStep[of inp "take k cases" ctx1 newTimeState1 newTimeEnv1 warnings1 payments1 _ _ _ _ _ _ "drop k cases"] finalCases_def by fastforce
  moreover have "\<not>(1 < k \<and> k < length cases) \<longrightarrow> (When cases2 t tCont2, ctx2, state2, env2, warnings2, payments2) \<rightarrow>\<^sup>(Some inp, lower, upper) (newC2' t (split_cases role choiceName k cont), split_cases_context role choiceName k ctx1', newTimeState2', env1', warnings1', payments1')" using no_split_lemma_proved large_k_cases2_is_map no_split_cases2_step new_time_states real_lower_upper_obtained small_step_receive_ccs.LambdaReceive by (smt (z3) FaustusV2AST.FContract.inject(3) calculation(4) decompile_c2 split_cases.simps(1))
  moreover show ?case apply (cases "1 < k \<and> k < length cases")
     apply (cases "finalCases inp (take k cases, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1)")
      apply (meson full_hidden_step_lemma_proved dropState2'_obtained)
     apply (smt (z3) BaseReceiveFull FaustusV2AST.FContract.inject(3) calculation(4) calculation(47) calculation(48) cases_step decompile_c2 inp_not_choice no_split_lemma_proved singletonD small_k_cases2_is_take_k split_cases.simps(1) split_cases_relation.simps take_k_cases_step_imp_step_in_relation)
    by (metis NoChoiceLambdaReceive calculation(49) decompile_c2 full_semantics_receive_choice_input_is_equivalent_to_small_step_add_choice_forward inp_not_choice no_split_lemma_proved)
qed auto

lemma split_cases_relation_backward_simulation_small_step_receive_add_choice:
"(ChoiceId choiceName (SemanticsTypes.Role role)) \<turnstile> (c2, ctx2, state2, env2, warnings2, payments2) \<rightarrow>\<^sup>(inp, lower, upper) (c2', ctx2', state2', env2', warnings2', payments2') \<Longrightarrow>
  split_cases_relation role choiceName k (c1, ctx1, state1, env1, warnings1, payments1) (c2, ctx2, state2, env2, warnings2, payments2) \<Longrightarrow>
       \<exists>c1' ctx1' state1' env1' warnings1' payments1'.
          ((ChoiceId choiceName (SemanticsTypes.Role role)) \<turnstile> (c1, ctx1, state1, env1, warnings1, payments1) \<rightarrow>\<^sup>(inp, lower, upper) (c1', ctx1', state1', env1', warnings1', payments1')) \<and>
          split_cases_relation role choiceName k (c1', ctx1', state1', env1', warnings1', payments1') (c2', ctx2', state2', env2', warnings2', payments2')"
proof (induction "(ChoiceId choiceName (SemanticsTypes.Role role))" c2 ctx2 state2 env2 warnings2 payments2 inp lower upper c2' ctx2' state2' env2' warnings2' payments2'
    arbitrary: c1 ctx1 state1 env1 warnings1 payments1
    rule: small_step_receive_ccs_add_choice.induct[split_format(complete)])
  case (NoChoiceLambdaReceive c2 ctx2 state2 env2 warnings2 payments2 inp lower upper c2' ctx2' state2' env2' warnings2' payments2' innerInp innerLower innerUpper)
  moreover obtain cases2 t tCont2 where decompile_c2: "c2 = When cases2 t tCont2" using calculation by auto
  moreover obtain cases1 tCont1 where decompile: "c1 = When cases1 t tCont1 \<and>
    split_cases role choiceName k tCont1 = tCont2" using calculation apply (cases c1, auto)
    by (smt (verit) FaustusV2AST.FContract.inject(3))
  moreover have base_state_eq: "state_minimal_eq choiceName state2 state1" using calculation by auto
  moreover obtain newTimeEnv1 newTimeState1 newTimeEnv2 newTimeState2 where new_time_states: "fixFInterval (lower, upper) state1 = (newTimeEnv1, newTimeState1) \<and>
    fixFInterval (lower, upper) state2 = (newTimeEnv2, newTimeState2)" by (cases "lower < (minTime state1)"; cases "lower < (minTime state2)", auto)
  moreover have "newTimeEnv1 = newTimeEnv2 \<and> state_minimal_eq choiceName newTimeState2 newTimeState1" using new_time_states base_state_eq by (cases "lower < (minTime state1)"; cases "lower < (minTime state2)", auto; metis FState.surjective FState.update_convs(5) Pair_inject)
  moreover have "choices newTimeState1 = choices state1" using calculation by (cases "lower < (minTime state1)", auto)
  moreover have "choices newTimeState2 = choices state2" using new_time_states by (cases "lower < (minTime state2)", auto)
  moreover have "isFreshChoiceInCasesGuards choiceName cases1" using calculation isFreshChoiceInCasesImpliesIsFreshChoiceInCasesGuards by auto
  moreover have "split_cases_context role choiceName k ctx1 = ctx2" using calculation by auto
  moreover obtain newCases where "newCases = map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) cases1" using calculation by auto
  moreover obtain takeNewCases dropNewCases where "take k newCases = takeNewCases \<and> drop k newCases = dropNewCases" by auto
  moreover have "\<not>(k > 1 \<and> length cases1 > k) \<longrightarrow> cases2 = newCases" using calculation by auto
  moreover have small_k_cases2_split: "(k > 1 \<and> length cases1 > k) \<longrightarrow> cases2 = (takeNewCases @ [Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) (split_cases role choiceName k (When (drop k cases1) t tCont1))])" using calculation by auto
  moreover have "computeActionStep innerInp (Choice (FChoiceId choiceName (Role role)) [(0, 0)], ctx2, newTimeState2, newTimeEnv2) = None" using calculation by (cases innerInp, auto)
  moreover have "finalCases innerInp ([Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) (split_cases role choiceName k (When (drop k cases1) t tCont1))], ctx2, newTimeState2, newTimeEnv2, warnings2, payments2)" by (metis (no_types, lifting) ActionGuardE calculation computeActionStepIsSmallStep computeCaseStep.simps computeCasesStep.simps(1) computeCasesStep.simps(2) computeGuardStepHaltedImpliesSmallStepHalted finalCases_def finalGuard_def option.distinct(1) option.simps(4) reduceCasesStepImpliesComputeCaseStepRes)
  moreover obtain newC2 where newC2_obtained: "innerInp \<turnstile> (cases2, ctx2, newTimeState2, newTimeEnv2, warnings2, payments2) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC2, ctx2', state2', env2', warnings2', payments2')" using NoChoiceLambdaReceive new_time_states decompile_c2 by (induction c2 ctx2 state2 env2 warnings2 payments2 inp lower upper c2' ctx2' state2' env2' warnings2' payments2' rule: small_step_receive_ccs.induct[split_format(complete)], auto)
  moreover have "(k > 1 \<and> length cases1 > k) \<longrightarrow> innerInp \<turnstile> (takeNewCases, ctx2, newTimeState2, newTimeEnv2, warnings2, payments2) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC2, ctx2', state2', env2', warnings2', payments2')" using newC2_obtained casesStepDropFinalCases small_k_cases2_split calculation by meson
  moreover have "\<not>(k > 1 \<and> length cases1 > k) \<longrightarrow> innerInp \<turnstile> (newCases, ctx2, newTimeState2, newTimeEnv2, warnings2, payments2) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC2, ctx2', state2', env2', warnings2', payments2')" using calculation by auto
  moreover obtain takeCases1 dropCases1 where "take k cases1 = takeCases1 \<and> drop k cases1 = dropCases1" by auto
  moreover have "takeNewCases = map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) takeCases1" using calculation take_map by blast
  moreover have "isFreshChoiceInCasesGuards choiceName takeCases1" by (metis append_take_drop_id calculation isFreshChoiceInCasesGuardsPreservedInSlice)
  moreover obtain newC1 ctx1' state1' where takeCasesStep: "(k > 1 \<and> length cases1 > k) \<longrightarrow> innerInp \<turnstile> (takeCases1, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC1, ctx1', state1', newTimeEnv1, warnings2', payments2') \<and>
    split_cases role choiceName k (newC1 t tCont1) = newC2 t tCont2 \<and>
    split_cases_context role choiceName k ctx1' = ctx2' \<and>
    state_minimal_eq choiceName state1' state2'" using small_step_cases_preserved_inverse_split_cases_relation[of innerInp role choiceName k takeCases1 ctx1 newTimeState2 newTimeEnv1 warnings1 payments1 newC2 ctx2' state2' env2' warnings2' payments2' newTimeState1 t tCont1] by (metis NoChoiceLambdaReceive.prems calculation(21,24,25,9) decompile reduceCasesConstants split_cases_relation.simps state_minimal_eq_commutative)
  moreover have fullTakeCasesStep: "(k > 1 \<and> length cases1 > k) \<longrightarrow> innerInp \<turnstile> (cases1, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC1, ctx1', state1', newTimeEnv1, warnings2', payments2')" by (metis append_take_drop_id casesStepImpliesAppendCasesStep calculation)
  moreover obtain fullNewC1 fullNewCtx1 fullNewState1 where fullCasesStep: "\<not>(k > 1 \<and> length cases1 > k) \<longrightarrow> innerInp \<turnstile> (cases1, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (fullNewC1, fullNewCtx1, fullNewState1, newTimeEnv1, warnings2', payments2')  \<and>
    split_cases role choiceName k (fullNewC1 t tCont1) = newC2 t tCont2 \<and>
    split_cases_context role choiceName k fullNewCtx1 = ctx2' \<and>
    state_minimal_eq choiceName fullNewState1 state2'" using  small_step_cases_preserved_inverse_split_cases_relation[of innerInp role choiceName k cases1 ctx1 newTimeState2 newTimeEnv1 warnings1 payments1 newC2 ctx2' state2' env2' warnings2' payments2' newTimeState1 t tCont1]
    by (metis (no_types, lifting) NoChoiceLambdaReceive.prems calculation(12,14,22,9) decompile reduceCasesConstants split_cases_relation.simps state_minimal_eq_commutative)
  moreover obtain realLower realUpper where "timeInterval newTimeEnv2 = (realLower, realUpper) \<and> realLower < t \<and> realUpper < t" using NoChoiceLambdaReceive new_time_states decompile_c2 new_time_states by (induction c2 ctx2 state2 env2 warnings2 payments2 inp lower upper c2' ctx2' state2' env2' warnings2' payments2' rule: small_step_receive_ccs.induct[split_format(complete)], auto)
  moreover have "(k > 1 \<and> length cases1 > k) \<longrightarrow> (c1, ctx1, state1, env1, warnings1, payments1) \<rightarrow>\<^sup>(Some innerInp, lower, upper) (newC1 t tCont1, ctx1', state1', newTimeEnv1, warnings2', payments2')" using LambdaReceive[of lower upper state1 newTimeEnv1 newTimeState1 realLower realUpper t innerInp cases1 ctx1 warnings1 payments1 newC1 ctx1' state1' newTimeEnv1 warnings2' payments2' "(Some innerInp, lower, upper)" tCont1 env1] using calculation(29) calculation(9) decompile fullTakeCasesStep new_time_states by fastforce
  moreover have split_transition: "(k > 1 \<and> length cases1 > k) \<longrightarrow> (ChoiceId choiceName (SemanticsTypes.Role role)) \<turnstile> (c1, ctx1, state1, env1, warnings1, payments1) \<rightarrow>\<^sup>(Some innerInp, lower, upper) (newC1 t tCont1, ctx1', state1', newTimeEnv1, warnings2', payments2')" using calculation(2) calculation(30) small_step_receive_ccs_add_choice.NoChoiceLambdaReceive by auto
  moreover have no_split_transition: "\<not>(k > 1 \<and> length cases1 > k) \<longrightarrow> (ChoiceId choiceName (SemanticsTypes.Role role)) \<turnstile> (c1, ctx1, state1, env1, warnings1, payments1) \<rightarrow>\<^sup>(Some innerInp, lower, upper) (fullNewC1 t tCont1, fullNewCtx1, fullNewState1, newTimeEnv1, warnings2', payments2')" using LambdaReceive NoChoiceLambdaReceive.prems calculation choice_receive_same_as_basic_when_fresh decompile fullCasesStep by auto
  moreover have "newC2 t tCont2 = c2'" using NoChoiceLambdaReceive new_time_states decompile_c2 newC2_obtained apply (induction c2 ctx2 state2 env2 warnings2 payments2 inp lower upper c2' ctx2' state2' env2' warnings2' payments2' rule: small_step_receive_ccs.induct[split_format(complete)], auto)
    by (metis FaustusV2AST.FContract.inject(3) Pair_inject deterministicCasesSemantics)
  moreover have split_relation: "(k > 1 \<and> length cases1 > k) \<longrightarrow> split_cases_relation role choiceName k (newC1 t tCont1, ctx1', state1', newTimeEnv1, warnings2', payments2') (c2', ctx2', state2', env2', warnings2', payments2')" by (metis NoChoiceLambdaReceive.prems calculation(31) calculation(33) calculation(9) choice_receive_preserves_fresh_choice newC2_obtained reduceCasesConstants split_cases_relation.simps takeCasesStep)
  moreover have no_split_relation: "\<not>(k > 1 \<and> length cases1 > k) \<longrightarrow> split_cases_relation role choiceName k (fullNewC1 t tCont1, fullNewCtx1, fullNewState1, newTimeEnv1, warnings2', payments2') (c2', ctx2', state2', env2', warnings2', payments2')" by (metis NoChoiceLambdaReceive.prems calculation(13) calculation(32) calculation(33) calculation(9) choice_receive_preserves_fresh_choice fullCasesStep newC2_obtained reduceCasesConstants split_cases_relation.simps)
  show ?case apply (cases "(k > 1 \<and> length cases1 > k)")
    using NoChoiceLambdaReceive split_transition split_relation no_split_transition no_split_relation by blast+
next
  case (ChoiceLambdaReceive c2 ctx2 state2 env2 warnings2 payments2 inp1 lower1 upper1 lower2 upper2 innerC2 innerCtx2 innerState2 innerEnv2 innerWarnings2 innerPayments2 c2' ctx2' state2' env2' warnings2' payments2' innerInp)
  moreover have "lower1 = lower2 \<and> upper1 = upper2" using ChoiceLambdaReceive by auto
  moreover obtain tCont2 cases2 t where decompile_c2: "c2 = When cases2 t tCont2" using ChoiceLambdaReceive by auto
  moreover obtain cases1 tCont1 where decompile: "c1 = When cases1 t tCont1 \<and>
    split_cases role choiceName k tCont1 = tCont2" using calculation apply (cases c1, auto)
    by (smt (verit) FaustusV2AST.FContract.inject(3))
  moreover obtain newTimeEnv1 newTimeState1 newTimeState2 newTimeEnv2 where new_states: "fixFInterval (lower1, upper1) state1 = (newTimeEnv1, newTimeState1) \<and> fixFInterval (lower1, upper1) state2 = (newTimeEnv2, newTimeState2)" using calculation by (cases "lower1 < (minTime state1)"; cases "lower1 < (minTime state2)", auto)
  moreover have times_equal: "minTime state1 = minTime state2" using ChoiceLambdaReceive by auto
  moreover have new_times_equal: "newTimeEnv1 = newTimeEnv2 \<and> minTime newTimeState1 = minTime newTimeState2" using times_equal new_states fixFIntervalOnlyTimeDependent by metis
  moreover have time_state_1_only_time_changes: "choices newTimeState1 = choices state1 \<and>
    accounts newTimeState1 = accounts state1 \<and>
    boundValues newTimeState1 = boundValues state1 \<and>
    boundPubKeys newTimeState1 = boundPubKeys state1" using new_states by (cases "lower1 < (minTime state1)", auto)
  moreover have time_state_2_only_time_changes: "choices newTimeState2 = choices state2 \<and>
    accounts newTimeState2 = accounts state2 \<and>
    boundValues newTimeState2 = boundValues state2 \<and>
    boundPubKeys newTimeState2 = boundPubKeys state2" using new_states by (cases "lower1 < (minTime state2)", auto)
  moreover have newTimeStatesEq: "state_minimal_eq choiceName newTimeState2 newTimeState1" using ChoiceLambdaReceive(7) new_states times_equal new_times_equal time_state_1_only_time_changes time_state_2_only_time_changes by auto
  moreover have "isFreshChoiceInCasesGuards choiceName cases1" using calculation isFreshChoiceInCasesImpliesIsFreshChoiceInCasesGuards by auto
  moreover have "split_cases_context role choiceName k ctx1 = ctx2" using calculation by auto
  moreover obtain newCases where "newCases = map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) cases1" using calculation by auto
  moreover obtain takeNewCases dropNewCases where "take k newCases = takeNewCases \<and> drop k newCases = dropNewCases" by auto
  moreover have cases2_is_newCases_k_size: "\<not>(k > 1 \<and> length cases1 > k) \<longrightarrow> cases2 = newCases" using calculation by auto
  moreover have cases2_is_takeNewCases_k_size: "(k > 1 \<and> length cases1 > k) \<longrightarrow> cases2 = (takeNewCases @ [Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) (split_cases role choiceName k (When (drop k cases1) t tCont1))])" using calculation by auto
  moreover have "isFreshChoiceInCasesGuardsShallow choiceName (map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) cases1)" using calculation(17) by (induction cases1, auto split: FCase.split)
  moreover have "(\<forall> party i . finalCases (IChoice (ChoiceId choiceName party) i) (takeNewCases, ctx2, newTimeState2, newTimeEnv2, warnings2, payments2))" using calculation by (metis append_take_drop_id freshChoiceShallowNoMatch_cases_step isFreshChoiceInCasesGuardsShallowPreservedInSlice)
  moreover have choiceActionStep: "(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (Choice (FChoiceId choiceName (Role role)) [(0, 0)], ctx2, newTimeState2, newTimeEnv2) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (newTimeState2\<lparr>choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices newTimeState2) \<rparr>, ApplyNoWarning)" by (simp add: small_step_action_fresh_choice_state_eq)
  moreover have choiceActionStep: "computeActionStep (IChoice (ChoiceId choiceName party) i) (Choice (FChoiceId choiceName (Role role)) [(0, 0)], ctx2, newTimeState2, newTimeEnv2) = Some (newTimeState2\<lparr>choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices newTimeState2) \<rparr>, ApplyNoWarning) \<Longrightarrow> party = (Party.Role role) \<and> i = 0" by (auto split: if_split_asm)
  moreover obtain newChoiceState where new_choice_state: "newTimeState2\<lparr>choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices newTimeState2) \<rparr> = newChoiceState \<and> state_minimal_eq choiceName newTimeState2 newChoiceState" using small_step_action_fresh_choice_state_eq by auto
  moreover have choiceGuardStep: "(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)]), ctx2, newTimeState2, newTimeEnv2, warnings2, payments2) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 (([], None), ctx2, newChoiceState, newTimeEnv2, warnings2, payments2)"  by (metis ActionGuardStep append.right_neutral calculation(25,27) convertApplyWarning.simps(1))
  moreover have choiceCaseStep: "(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) (split_cases role choiceName k (When (drop k cases1) t tCont1)), ctx2, newTimeState2, newTimeEnv2, warnings2, payments2) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 (\<lambda> innerT innerTCont . prependStatements [] (split_cases role choiceName k (When (drop k cases1) t tCont1)), ctx2, newChoiceState, newTimeEnv2, warnings2, payments2)" using CaseGuardCompleteStep choiceGuardStep by meson
  moreover have choiceCasesStep: "(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> ([Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) (split_cases role choiceName k (When (drop k cases1) t tCont1))], ctx2, newTimeState2, newTimeEnv2, warnings2, payments2) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (\<lambda> innerT innerTCont . prependStatements [] (split_cases role choiceName k (When (drop k cases1) t tCont1)), ctx2, newChoiceState, newTimeEnv2, warnings2, payments2)" using CasesHeadMatches choiceCaseStep by force
  moreover have partyRoleIZero: "\<forall>res . computeCasesStep (IChoice (ChoiceId choiceName party) i) ([Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) (split_cases role choiceName k (When (drop k cases1) t tCont1))], ctx2, newTimeState2, newTimeEnv2, warnings2, payments2) = Some res \<longrightarrow> party = (Party.Role role) \<and> i = 0" by auto
  moreover have final_new_cases: "finalCases (IChoice (ChoiceId choiceName (Party.Role role)) 0) (newCases, ctx2, newTimeState2, newTimeEnv2, warnings2, payments2)" by (simp add: calculation(19) calculation(23) freshChoiceShallowNoMatch_cases_step)
  moreover have newCases_fails: "((When newCases t tCont2, ctx2, state2, env2, warnings2, payments2) \<rightarrow>\<^sup>(Some (IChoice (ChoiceId choiceName (Party.Role role)) 0), lower2, upper2) f) \<Longrightarrow> False" apply (induction "(When newCases t tCont2, ctx2, state2, env2, warnings2, payments2)" "(Some (IChoice (ChoiceId choiceName (Party.Role role)) 0), lower2, upper2)" "f" rule: small_step_receive_ccs.induct)
    using final_new_cases apply auto
    using calculation(19) calculation(23) freshChoiceShallowMatchNoChoiceInput_cases_step by blast
  moreover have cases2_not_newCases: "cases2 \<noteq> newCases" using final_new_cases newCases_fails[of ] decompile_c2 ChoiceLambdaReceive(2) small_step_receive_ccs.simps apply auto
    by (metis calculation(19) calculation(23) freshChoiceShallowNoMatch_cases_step_aux)
  moreover have cases2_is_takeNewCases: "cases2 = (takeNewCases @ [Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) (split_cases role choiceName k (When (drop k cases1) t tCont1))])" using cases2_is_newCases_k_size cases2_not_newCases cases2_is_takeNewCases_k_size by auto
  moreover have take_cases_cases_step: "(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (takeNewCases @ [Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) (split_cases role choiceName k (When (drop k cases1) t tCont1))], ctx2, newTimeState2, newTimeEnv2, warnings2, payments2) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (\<lambda> innerT innerTCont . prependStatements [] (split_cases role choiceName k (When (drop k cases1) t tCont1)), ctx2, newChoiceState, newTimeEnv2, warnings2, payments2)" using calculation(24,32) casesStepAppendFinalCases choiceCasesStep by presburger
  moreover have "(\<lambda> innerT innerTCont . prependStatements [] (split_cases role choiceName k (When (drop k cases1) t tCont1))) t tCont2 = (split_cases role choiceName k (When (drop k cases1) t tCont1))" by simp
  moreover have inner_choice_step: "(When cases2 t tCont2, ctx2, state2, env2, warnings2, payments2) \<rightarrow>\<^sup>(Some (IChoice (ChoiceId choiceName (Party.Role role)) 0), lower2, upper2) (innerC2, innerCtx2, innerState2, innerEnv2, innerWarnings2, innerPayments2)" using calculation(2) decompile_c2 by auto
  moreover have decompile_innerC2: "innerC2 = (split_cases role choiceName k (When (drop k cases1) t tCont1))" using inner_choice_step take_cases_cases_step cases2_is_takeNewCases  apply (elim ReceiveE)
    using new_states deterministicCasesSemantics calculation(8) decompile_c2 decompile by (auto, blast)
  moreover have decompile_innerCtx2: "innerCtx2 = ctx2" using inner_choice_step take_cases_cases_step cases2_is_takeNewCases decompile_c2 decompile  apply (elim ReceiveE)
    using new_states deterministicCasesSemantics calculation(8) decompile_c2 decompile by (auto)
  moreover have decompile_innerState2: "innerState2 = newChoiceState" using inner_choice_step take_cases_cases_step cases2_is_takeNewCases decompile_c2 decompile  apply (elim ReceiveE)
    using new_states deterministicCasesSemantics calculation(8) decompile_c2 decompile by (auto)
  moreover have decompile_innerEnv2: "innerEnv2 = newTimeEnv2" using inner_choice_step take_cases_cases_step cases2_is_takeNewCases decompile_c2 decompile  apply (elim ReceiveE)
    using new_states deterministicCasesSemantics calculation(8) decompile_c2 decompile by (auto)
  moreover have decompile_innerWarnings2: "innerWarnings2 = warnings2" using inner_choice_step take_cases_cases_step cases2_is_takeNewCases decompile_c2 decompile  apply (elim ReceiveE)
    using new_states deterministicCasesSemantics calculation(8) decompile_c2 decompile by (auto)
  moreover have decompile_innerPayments2: "innerPayments2 = payments2" using inner_choice_step take_cases_cases_step cases2_is_takeNewCases decompile_c2 decompile  apply (elim ReceiveE)
    using new_states deterministicCasesSemantics calculation(8) decompile_c2 decompile by (auto)
  moreover obtain dropCases1 where dropCasesEq: "dropCases1 = drop k cases1 \<and> map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) dropCases1 = dropNewCases" by (metis calculation(20) calculation(19) drop_map)
  moreover have fresh_choice_drop_cases: "isFreshChoiceInCasesGuards choiceName dropCases1" by (metis append_take_drop_id calculation(17) dropCasesEq isFreshChoiceInCasesGuardsPreservedInSlice)
  moreover have state1_eq_choice_state: "state_minimal_eq choiceName newTimeState1 newChoiceState" using calculation(27) newTimeStatesEq state_minimal_eq_commutative state_minimal_eq_transitive by blast
  moreover have dropCases1_in_relation_inner: "split_cases_relation role choiceName k (When dropCases1 t tCont1, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) (innerC2, innerCtx2, innerState2, innerEnv2, innerWarnings2, innerPayments2)" by (metis ChoiceLambdaReceive.prems append_take_drop_id decompile decompile_innerC2 decompile_innerCtx2 decompile_innerEnv2 decompile_innerPayments2 decompile_innerState2 decompile_innerWarnings2 dropCasesEq isFreshChoiceInCasesPreservedInSlice isFreshChoiceInContract.simps(4) isFreshChoiceInProcess.simps new_times_equal split_cases_relation.simps state1_eq_choice_state)
  moreover obtain c1' ctx1' state1' env1' warnings1' payments1' where dropCases1_step_in_relation: "(ChoiceId choiceName (Party.Role role) \<turnstile> (When dropCases1 t tCont1, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) \<rightarrow>\<^sup>(inp1, lower1, upper1) (c1', ctx1', state1', env1', warnings1', payments1')) \<and>
       split_cases_relation role choiceName k (c1', ctx1', state1', env1', warnings1', payments1') (c2', ctx2', state2', env2', warnings2', payments2')"
    using calculation(4) dropCases1_in_relation_inner by presburger
  moreover obtain takeCases1 where takeCasesEq: "takeCases1 = take k cases1 \<and> map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) takeCases1 = takeNewCases" by (metis calculation(19,20) take_map)
  moreover obtain realLower realUpper where real_time_bound: "timeInterval newTimeEnv2 = (realLower, realUpper) \<and> realLower < t \<and> realUpper < t" using ChoiceLambdaReceive new_states decompile_c2 by (auto simp add: small_step_receive_ccs.simps)
  moreover have finalCases_innerInp: "finalCases innerInp (cases2, ctx2, newTimeState2, newTimeEnv2, warnings2, payments2)" using ChoiceLambdaReceive(2) apply (simp add: small_step_receive_ccs.simps del: fixFInterval.simps)
    using decompile_c2 real_time_bound new_states calculation(8) apply (simp del: fixFInterval.simps)
    using small_step_receive_ccs.LambdaReceive[of lower2 upper2 state2 newTimeEnv2 newTimeState2 realLower realUpper t innerInp cases2 ctx2 warnings2 payments2] finalCases_def apply (simp del: fixFInterval.simps)
    apply (auto simp del: fixFInterval.simps)
    using ChoiceLambdaReceive(1,6) by blast
  moreover have finalCases_innerInp_takeNewCases: "finalCases innerInp (takeNewCases, ctx2, newTimeState2, newTimeEnv2, warnings2, payments2)" using cases2_is_takeNewCases finalCases_innerInp casesStepImpliesAppendCasesStep finalCases_def by fastforce
  moreover have fresh_choice_take_cases: "isFreshChoiceInCasesGuards choiceName takeCases1" by (metis append_take_drop_id calculation(17) calculation(50) isFreshChoiceInCasesGuardsPreservedInSlice)
  moreover have fresh_choice_drop_cases: "isFreshChoiceInCases choiceName dropCases1" using dropCases1_in_relation_inner by auto
  moreover have final_take_k_cases1: "finalCases innerInp (take k cases1, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1)" using small_step_cases_preserves_split_cases_relation[of innerInp takeCases1 ctx1 newTimeState1 newTimeEnv1 warnings1 payments1 _ _ _ _ _ _ choiceName newTimeState2 role k t tCont1] takeCasesEq ChoiceLambdaReceive(7) newTimeStatesEq new_choice_state state1_eq_choice_state state_minimal_eq_transitive fresh_choice_take_cases finalCases_def new_times_equal finalCases_innerInp_takeNewCases by (auto, presburger)
  moreover have fixFInterval_fixed_point: "fixFInterval (lower1, upper1) newTimeState1 = (newTimeEnv1, newTimeState1)" using new_states by (cases "lower1 < minTime state1", auto)
  moreover have When_dropCases1_basic_step: "(When dropCases1 t tCont1, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) \<rightarrow>\<^sup>(inp1, lower1, upper1) (c1', ctx1', state1', env1', warnings1', payments1')" by (meson dropCases1_step_in_relation choice_receive_same_as_basic_when_fresh dropCases1_in_relation_inner split_cases_relation.simps)
  moreover obtain newC1' where "innerInp \<turnstile> (dropCases1, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC1', ctx1', state1', env1', warnings1', payments1') \<and> newC1' t tCont1 = c1'" using When_dropCases1_basic_step deterministicCasesSemantics ChoiceLambdaReceive(6) fixFInterval_fixed_point by (auto simp add: small_step_receive_ccs.simps)
  moreover have "innerInp \<turnstile> (cases1, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC1', ctx1', state1', env1', warnings1', payments1')" by (metis append_take_drop_id calculation(59) casesStepAppendFinalCases dropCasesEq final_take_k_cases1)
  moreover have "(When cases1 t tCont1, ctx1, state1, env1, warnings1, payments1) \<rightarrow>\<^sup>(inp1, lower1, upper1) (c1', ctx1', state1', env1', warnings1', payments1')" by (metis LambdaReceive calculation(59) calculation(6) calculation(60) calculation(8) new_states new_times_equal real_time_bound)
  moreover have "(ChoiceId choiceName (Party.Role role) \<turnstile> (When cases1 t tCont1, ctx1, state1, env1, warnings1, payments1) \<rightarrow>\<^sup>(inp1, lower1, upper1) (c1', ctx1', state1', env1', warnings1', payments1'))" using NoChoiceLambdaReceive calculation(5) calculation(6) calculation(61) by presburger
  ultimately show ?case by meson
next
  case (CCSTimeout inp lower1 upper1 lower2 upper2 c2 ctx2 state2 env2 warnings2 payments2 c2' ctx2' state2' env2' warnings2' payments2')
   moreover have "lower1 = lower2 \<and> upper1 = upper2" using CCSTimeout by auto
   moreover obtain tCont2 cases2 t where decompile_c2: "c2 = When cases2 t tCont2" using CCSTimeout by auto
   moreover obtain cases1 tCont1 where decompile: "c1 = When cases1 t tCont1 \<and>
    split_cases role choiceName k tCont1 = tCont2" using calculation apply (cases c1, auto)
    by (smt (verit) FaustusV2AST.FContract.inject(3))
  moreover obtain newTimeEnv1 newTimeState1 where new_state_1: "fixFInterval (lower1, upper1) state1 = (newTimeEnv1, newTimeState1)" using calculation by (cases "lower1 < (minTime state1)", auto)
  moreover obtain newTimeEnv2 newTimeState2 where new_state_2: "fixFInterval (lower1, upper1) state2 = (newTimeEnv2, newTimeState2)" using calculation by (cases "lower1 < (minTime state1)", auto)
  moreover have "newTimeEnv1 = newTimeEnv2" using calculation by (cases "lower1 < (FState.minTime state1)", auto)
  moreover have times_equal: "minTime state1 = minTime state2" using CCSTimeout by auto
  moreover have new_times_equal: "newTimeEnv1 = newTimeEnv2 \<and> minTime newTimeState1 = minTime newTimeState2" using times_equal new_state_1 new_state_2 fixFIntervalOnlyTimeDependent by metis
  moreover have time_state_1_only_time_changes: "choices newTimeState1 = choices state1 \<and>
    accounts newTimeState1 = accounts state1 \<and>
    boundValues newTimeState1 = boundValues state1 \<and>
    boundPubKeys newTimeState1 = boundPubKeys state1" using new_state_1 by (cases "lower1 < (minTime state1)", auto)
  moreover have time_state_2_only_time_changes: "choices newTimeState2 = choices state2 \<and>
    accounts newTimeState2 = accounts state2 \<and>
    boundValues newTimeState2 = boundValues state2 \<and>
    boundPubKeys newTimeState2 = boundPubKeys state2" using new_state_2 by (cases "lower1 < (minTime state2)", auto)
  moreover have newTimeStatesEq: "state_minimal_eq choiceName newTimeState2 newTimeState1" using CCSTimeout new_state_1 new_state_2 times_equal new_times_equal time_state_1_only_time_changes time_state_2_only_time_changes by auto
  moreover have "choices newTimeState1 = choices state1" using new_state_1 by (cases "lower1 < (minTime state1)", auto)
  moreover have "choices newTimeState2 = choices state2" using new_state_2 by (cases "lower1 < (minTime state2)", auto)
  moreover have decompiled_step: "(When cases2 t tCont2, ctx2, state2, env2, warnings2, payments2) \<rightarrow>\<^sup>(inp, lower1, upper1) (tCont2, ctx2, newTimeState2, newTimeEnv2, warnings2, payments2)" using CCSTimeout(1,2) decompile_c2 small_step_receive_ccs.simps[of "(When cases2 t tCont2, ctx2, state2, env2, warnings2, payments2)" "(inp, lower1, upper1)" "(c2', ctx2', state2', env2', warnings2', payments2')"] apply (simp del: fixFInterval.simps)
    using CCSTimeout(2) by (metis fixFIntervalOnlyTimeDependent new_state_2 small_step_receive_ccs.CCSTimeout)
  moreover obtain realLower realUpper where real_time_bound: "timeInterval newTimeEnv2 = (realLower, realUpper) \<and> realLower \<ge> t \<and> realUpper \<ge> t" using CCSTimeout new_state_2 decompile_c2 by (auto simp add: small_step_receive_ccs.simps)
  moreover have "(c1, ctx1, state1, env1, warnings1, payments1) \<rightarrow>\<^sup>(None, lower1, upper1) (tCont1, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1)" using small_step_receive_ccs.CCSTimeout[of lower1 upper1 state1 newTimeEnv1 newTimeState1 realLower realUpper t _ cases1 tCont1 ctx1 env1 warnings1 payments1] calculation decompile by auto
  moreover have "(ChoiceId choiceName (Party.Role role)) \<turnstile> (c1, ctx1, state1, env1, warnings1, payments1) \<rightarrow>\<^sup>(None, lower1, upper1) (tCont1, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1)" using small_step_receive_ccs_add_choice.CCSTimeout calculation decompile by fastforce
  moreover have "split_cases_relation role choiceName k (tCont1, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) (tCont2, ctx2, newTimeState2, newTimeEnv2, warnings2, payments2)" using calculation by auto
  ultimately show ?case by (metis deterministicSmallStepReceive)
qed

lemma full_semantics_maintains_split_cases:
"A \<turnstile> (c1, ctx1, state1, env1, warnings1, payments1) \<midarrow>\<^sup>\<mu>\<rightarrow> (c1', ctx1', state1', env1', warnings1', payments1') \<Longrightarrow>
split_cases_relation role choiceName k (c1, ctx1, state1, env1, warnings1, payments1) (c2, ctx2, state2, env2, warnings2, payments2) \<Longrightarrow>
  \<exists>c2' ctx2' state2' env2' warnings2' payments2'.
    ((A \<union> {(IChoice (ChoiceId choiceName (Party.Role role)) 0)}) \<turnstile> (c2, ctx2, state2, env2, warnings2, payments2) \<midarrow>\<^sup>\<mu>\<rightarrow> (c2', ctx2', state2', env2', warnings2', payments2')) \<and>
    split_cases_relation role choiceName k (c1', ctx1', state1', env1', warnings1', payments1') (c2', ctx2', state2', env2', warnings2', payments2')"
proof (induction A "(c1, ctx1, state1, env1, warnings1, payments1)" \<mu> "(c1', ctx1', state1', env1', warnings1', payments1')"
    arbitrary: c1 ctx1 state1 env1 warnings1 payments1 c1' ctx1' state1' env1' warnings1' payments1' c2 ctx2 state2 env2 warnings2 payments2
    rule: full_semantics.induct)
  case (BaseInternalFull A)
  then show ?case using full_semantics_react_is_equivalent_to_internal_reaction react_step_strongly_preserves_split_cases_relation by force
next
  case (BaseReceiveFull i A lower upper)
  moreover obtain c2' ctx2' state2' env2' warnings2' payments2' where "({(IChoice (ChoiceId choiceName (Party.Role role)) 0)} \<turnstile> (c2, ctx2, state2, env2, warnings2, payments2) \<midarrow>\<^sup>\<lambda>\<^sup>(Some i, lower, upper)\<rightarrow> (c2', ctx2', state2', env2', warnings2', payments2')) \<and> split_cases_relation role choiceName k (c1', ctx1', state1', env1', warnings1', payments1') (c2', ctx2', state2', env2', warnings2', payments2')"
    using receive_semantics_strongly_maintains_split_cases_NoChoiceLambdaReceive[of role choiceName k c1 c2 ctx1 ctx2 state1 state2 env1 env2 warnings1 warnings2 payments1 payments2 i lower upper c1' ctx1' state1' env1' warnings1' payments1'] BaseReceiveFull by auto
  ultimately show ?case by (metis BaseReceiveFull(1) full_semantics_weakening inf_sup_aci(5))
next
  case (RestrictedReceiveFull i A \<mu> lower upper p3)
  moreover obtain j innerC1 innerCtx1 innerState1 innerEnv1 innerWarnings1 innerPayments1 where innerP3_obtained: "j \<in> A \<and> (c1, ctx1, state1, env1, warnings1, payments1) \<rightarrow>\<^sup>(Some j, lower, upper) p3 \<and> p3 = (innerC1, innerCtx1, innerState1, innerEnv1, innerWarnings1, innerPayments1)" using RestrictedReceiveFull by (cases p3, auto)
  moreover have j_not_choice: "j \<noteq> (IChoice (ChoiceId choiceName (Party.Role role)) 0)" by (metis basic_receive_input_not_fresh_choice innerP3_obtained local.RestrictedReceiveFull(7) split_cases_relation.simps)
  moreover obtain innerC2 innerCtx2 innerState2 innerEnv2 innerWarnings2 innerPayments2 where innerC2_obtained: "({IChoice (ChoiceId choiceName (Party.Role role)) 0} \<turnstile> (c2, ctx2, state2, env2, warnings2, payments2) \<midarrow>\<^sup>\<lambda>\<^sup>(Some j, lower, upper)\<rightarrow> (innerC2, innerCtx2, innerState2, innerEnv2, innerWarnings2, innerPayments2)) \<and> split_cases_relation role choiceName k p3 (innerC2, innerCtx2, innerState2, innerEnv2, innerWarnings2, innerPayments2)"
    using  receive_semantics_strongly_maintains_split_cases_NoChoiceLambdaReceive[of role choiceName k c1 c2 ctx1 ctx2 state1 state2 env1 env2 warnings1 warnings2 payments1 payments2 j lower upper innerC1 innerCtx1 innerState1 innerEnv1 innerWarnings1 innerPayments1] RestrictedReceiveFull j_not_choice innerP3_obtained by (auto simp del: state_minimal_eq.simps)
  moreover obtain c2' ctx2' state2' env2' warnings2' payments2' where final_c2_step: "((A \<union> {IChoice (ChoiceId choiceName (Party.Role role)) 0}) \<turnstile> (innerC2, innerCtx2, innerState2, innerEnv2, innerWarnings2, innerPayments2) \<midarrow>\<^sup>\<lambda>\<^sup>\<mu>\<rightarrow> (c2', ctx2', state2', env2', warnings2', payments2')) \<and>
       split_cases_relation role choiceName k (c1', ctx1', state1', env1', warnings1', payments1') (c2', ctx2', state2', env2', warnings2', payments2')"
    using RestrictedReceiveFull(6)[of innerC1 innerCtx1 innerState1 innerEnv1 innerWarnings1 innerPayments1 innerC2 innerCtx2 innerState2 innerEnv2 innerWarnings2 innerPayments2] innerC2_obtained innerP3_obtained by auto
  moreover have "i \<noteq> (IChoice (ChoiceId choiceName (Party.Role role)) 0)" by (metis fresh_choice_no_match_full_semantics innerC2_obtained innerP3_obtained local.RestrictedReceiveFull(3) local.RestrictedReceiveFull(5) split_cases_relation.simps)
  moreover have "\<nexists>f. (ChoiceId choiceName (Party.Role role)) \<turnstile> (c2, ctx2, state2, env2, warnings2, payments2) \<rightarrow>\<^sup>(Some i, lower, upper) f" using split_cases_relation_backward_simulation_small_step_receive_add_choice choice_receive_same_as_basic_when_fresh RestrictedReceiveFull apply auto
    by meson
  moreover have "(A \<union> {IChoice (ChoiceId choiceName (Party.Role role)) 0}) \<turnstile> (c2, ctx2, state2, env2, warnings2, payments2) \<midarrow>\<^sup>\<lambda>\<^sup>(Some i, lower, upper)\<rightarrow> (c2', ctx2', state2', env2', warnings2', payments2')"
      using full_semantics_union_composition_input[of "{IChoice (ChoiceId choiceName (Party.Role role)) 0}" "(c2, ctx2, state2, env2, warnings2, payments2)" j lower upper "(innerC2, innerCtx2, innerState2, innerEnv2, innerWarnings2, innerPayments2)" A "(Some i, lower, upper)" "(c2', ctx2', state2', env2', warnings2', payments2')" i]
      innerC2_obtained
      final_c2_step
      RestrictedReceiveFull(2)
      by (metis calculation(12) calculation(13) calculation(3) empty_iff full_semantics_receive_choice_input_is_equivalent_to_small_step_add_choice innerP3_obtained insert_iff)
  then show ?case using final_c2_step RestrictedReceiveFull by blast
next
  case (TimeoutFull lower upper A)
  moreover obtain cases1 t tCont1 where decompile_c1: "c1 = When cases1 t tCont1" using TimeoutFull by auto
  moreover obtain cases2 tCont2 where decompile_c2: "c2 = When cases2 t tCont2 \<and> tCont2 = split_cases role choiceName k tCont1" using decompile_c1 TimeoutFull(2) apply auto
    by (cases "1 < k \<and> k < length cases1", auto)
  moreover obtain newTimeState1 newTimeEnv1 newTimeState2 newTimeEnv2 where new_time_states: "fixFInterval (lower, upper) state1 = (newTimeEnv1, newTimeState1) \<and>
    fixFInterval (lower, upper) state2 = (newTimeEnv2, newTimeState2) \<and>
    state_minimal_eq choiceName newTimeState1 newTimeState2 \<and>
    newTimeEnv1 = newTimeEnv2" using TimeoutFull(2) apply auto
    apply (cases "lower < minTime state1"; cases "lower < minTime state2", auto)
    by (smt (verit, best) FState.select_convs(1) FState.select_convs(2) FState.select_convs(3) FState.select_convs(4) FState.select_convs(5) FState.surjective FState.update_convs(5))
  moreover obtain realLower realUpper where realLower_realUpper_obtained: "timeInterval newTimeEnv1 = (realLower, realUpper) \<and> realLower \<ge> t \<and> realUpper \<ge> t" using decompile_c1 new_time_states TimeoutFull(1) by (auto simp add: small_step_receive_ccs.simps)
  moreover have "(c2, ctx2, state2, env2, warnings2, payments2) \<rightarrow>\<^sup>(None, lower, upper) (tCont2, ctx2, newTimeState2, newTimeEnv2, warnings2, payments2)" using realLower_realUpper_obtained decompile_c2 new_time_states by (auto simp add: small_step_receive_ccs.simps)
  ultimately show ?case by (smt (z3) choice_receive_same_as_basic_when_fresh deterministicSmallStepReceive full_semantics.TimeoutFull small_step_receive_ccs_add_choice.CCSTimeout split_cases_relation.simps split_cases_relation_backward_simulation_small_step_receive_add_choice)
qed

lemma full_nd_semantics_maintains_split_cases:
"A \<turnstile> (c1, ctx1, state1, env1, warnings1, payments1) \<midarrow>\<midarrow>\<^sup>\<mu>\<rightarrow> (c1', ctx1', state1', env1', warnings1', payments1') \<Longrightarrow>
split_cases_relation role choiceName k (c1, ctx1, state1, env1, warnings1, payments1) (c2, ctx2, state2, env2, warnings2, payments2) \<Longrightarrow>
  \<exists>c2' ctx2' state2' env2' warnings2' payments2'.
    ((A \<union> {(IChoice (ChoiceId choiceName (Party.Role role)) 0)}) \<turnstile> (c2, ctx2, state2, env2, warnings2, payments2) \<midarrow>\<midarrow>\<^sup>\<mu>\<rightarrow> (c2', ctx2', state2', env2', warnings2', payments2')) \<and>
    split_cases_relation role choiceName k (c1', ctx1', state1', env1', warnings1', payments1') (c2', ctx2', state2', env2', warnings2', payments2')"
proof (induction A "(c1, ctx1, state1, env1, warnings1, payments1)" \<mu> "(c1', ctx1', state1', env1', warnings1', payments1')"
    arbitrary: c1 ctx1 state1 env1 warnings1 payments1 c1' ctx1' state1' env1' warnings1' payments1' c2 ctx2 state2 env2 warnings2 payments2
    rule: full_nd_semantics.induct)
  case (BaseInternalFullND A)
  then show ?case by (smt (verit, ccfv_SIG) full_nd_semantics.BaseInternalFullND internal_reaction.simps react_step_strongly_preserves_split_cases_relation)
next
  case (BaseReceiveFullND i A lower upper)
  moreover obtain c2' ctx2' state2' env2' warnings2' payments2' where "((ChoiceId choiceName (Party.Role role)) \<turnstile> (c2, ctx2, state2, env2, warnings2, payments2) \<rightarrow>\<^sup>(Some i, lower, upper) (c2', ctx2', state2', env2', warnings2', payments2')) \<and> 
    split_cases_relation role choiceName k (c1', ctx1', state1', env1', warnings1', payments1') (c2', ctx2', state2', env2', warnings2', payments2')"
    using BaseReceiveFullND receive_semantics_strongly_maintains_split_cases_NoChoiceLambdaReceive by (smt (verit, ccfv_SIG) full_semantics_maintains_split_cases full_semantics_receive_choice_input_is_equivalent_to_small_step_add_choice_backward full_semantics_receive_empty_set_is_equivalent_to_base_small_step sup_bot_left)
  ultimately show ?case by (metis full_nd_simulates_original full_semantics_receive_choice_input_is_equivalent_to_small_step_add_choice_forward full_semantics_weakening inf_sup_aci(5))
next
  case (RestrictedReceiveFullND i A \<mu> lower upper p3)
  moreover obtain j innerC1 innerCtx1 innerState1 innerEnv1 innerWarnings1 innerPayments1 where innerP3_obtained: "j \<in> A \<and> (c1, ctx1, state1, env1, warnings1, payments1) \<rightarrow>\<^sup>(Some j, lower, upper) p3 \<and> p3 = (innerC1, innerCtx1, innerState1, innerEnv1, innerWarnings1, innerPayments1)" using RestrictedReceiveFullND by (cases p3, auto)
  moreover have j_not_choice: "j \<noteq> (IChoice (ChoiceId choiceName (Party.Role role)) 0)" by (metis basic_receive_input_not_fresh_choice innerP3_obtained local.RestrictedReceiveFullND(6) split_cases_relation.simps)
  moreover obtain innerC2 innerCtx2 innerState2 innerEnv2 innerWarnings2 innerPayments2 where innerC2_obtained: "({IChoice (ChoiceId choiceName (Party.Role role)) 0} \<turnstile> (c2, ctx2, state2, env2, warnings2, payments2) \<midarrow>\<^sup>\<lambda>\<^sup>(Some j, lower, upper)\<rightarrow> (innerC2, innerCtx2, innerState2, innerEnv2, innerWarnings2, innerPayments2)) \<and> split_cases_relation role choiceName k p3 (innerC2, innerCtx2, innerState2, innerEnv2, innerWarnings2, innerPayments2)"
    using  receive_semantics_strongly_maintains_split_cases_NoChoiceLambdaReceive[of role choiceName k c1 c2 ctx1 ctx2 state1 state2 env1 env2 warnings1 warnings2 payments1 payments2 j lower upper innerC1 innerCtx1 innerState1 innerEnv1 innerWarnings1 innerPayments1] RestrictedReceiveFullND j_not_choice innerP3_obtained by (auto simp del: state_minimal_eq.simps)
  moreover obtain c2' ctx2' state2' env2' warnings2' payments2' where final_c2_step: "((A \<union> {IChoice (ChoiceId choiceName (Party.Role role)) 0}) \<turnstile> (innerC2, innerCtx2, innerState2, innerEnv2, innerWarnings2, innerPayments2) \<midarrow>\<midarrow>\<^sup>\<lambda>\<^sup>\<mu>\<rightarrow> (c2', ctx2', state2', env2', warnings2', payments2')) \<and>
       split_cases_relation role choiceName k (c1', ctx1', state1', env1', warnings1', payments1') (c2', ctx2', state2', env2', warnings2', payments2')"
    using RestrictedReceiveFullND(5)[of innerC1 innerCtx1 innerState1 innerEnv1 innerWarnings1 innerPayments1 innerC2 innerCtx2 innerState2 innerEnv2 innerWarnings2 innerPayments2] innerC2_obtained innerP3_obtained by auto
  moreover have "i \<noteq> (IChoice (ChoiceId choiceName (Party.Role role)) 0)" by (metis calculation(2) fresh_choice_no_match_full_nd_semantics innerC2_obtained innerP3_obtained local.RestrictedReceiveFullND(4) split_cases_relation.simps)
  moreover have "(A \<union> {IChoice (ChoiceId choiceName (Party.Role role)) 0}) \<turnstile> (c2, ctx2, state2, env2, warnings2, payments2) \<midarrow>\<midarrow>\<^sup>\<lambda>\<^sup>(Some i, lower, upper)\<rightarrow> (c2', ctx2', state2', env2', warnings2', payments2')"
      using full_nd_semantics_union_composition_input[of "{IChoice (ChoiceId choiceName (Party.Role role)) 0}" "(c2, ctx2, state2, env2, warnings2, payments2)" j lower upper "(innerC2, innerCtx2, innerState2, innerEnv2, innerWarnings2, innerPayments2)" A "(Some i, lower, upper)" "(c2', ctx2', state2', env2', warnings2', payments2')" i]
      innerC2_obtained
      final_c2_step
      RestrictedReceiveFullND(2)
      by (meson calculation(11) full_nd_simulates_original innerP3_obtained singletonD)
  then show ?case using final_c2_step RestrictedReceiveFullND by blast
next
  case (TimeoutFullND lower upper A)
  then show ?case by (smt (z3) TimeoutFull full_nd_simulates_original full_semantics_maintains_split_cases)
qed

(* == Backward simulation work == *)
definition disjoint_cases :: "FCase list \<Rightarrow> bool" where
"disjoint_cases cases \<longleftrightarrow> (\<forall>inp ctx state env warnings payments res .
  inp \<turnstile> (cases, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 res \<longrightarrow>
  (\<forall> cases1 cases2 . cases = cases1 @ cases2 \<longrightarrow>
    inp \<turnstile> (cases1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 res \<and> \<not>(\<exists>otherRes . inp \<turnstile> (cases2, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 otherRes) \<or>
    inp \<turnstile> (cases2, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 res \<and> \<not>(\<exists>otherRes . inp \<turnstile> (cases1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 otherRes)))"

lemma single_case_disjoint:
"disjoint_cases [c]"
  apply (auto simp add: disjoint_cases_def)
  by (metis (no_types, lifting) append_eq_Cons_conv append_is_Nil_conv computeCasesStep.simps(1) option.distinct(1) reduceCasesStepImpliesComputeCaseStepRes)+

lemma slice_cases_preserves_disjoint_cases:
"disjoint_cases (cases1 @ cases2) \<Longrightarrow>
  \<not>disjoint_cases cases2 \<Longrightarrow>
  False"
  subgoal premises ps proof -
    obtain cases21 cases22 ctx state env warnings payments res1 res2 inp where "cases2 = cases21 @ cases22 \<and>
      (inp \<turnstile> (cases21, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 res1 \<and> (inp \<turnstile> (cases22, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 res2))" using ps(2) apply (auto simp add: disjoint_cases_def)
        apply (meson small_step_cases_split_list_matches_one_side)
       apply (metis small_step_cases_split_list_matches_one_side)
      by (metis small_step_cases_split_list_matches_one_side)
    moreover obtain res3 where "inp \<turnstile> (cases1@cases21, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 res3" apply auto
      by (smt (verit, del_insts) calculation casesStepAppendFinalCases casesStepImpliesAppendCasesStep finalCases_def prod_cases6)
    moreover obtain cases121 where "cases121 = cases1@cases21" by auto
    moreover have new_slice: "cases1 @ cases2 = cases121 @ cases22" using calculation by auto
    moreover have early_step: "inp \<turnstile> (cases121, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 res3" using calculation by auto
    moreover have late_step: "(inp \<turnstile> (cases22, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 res2)" using calculation by auto
    moreover obtain cases where full_cases: "cases = cases121@cases22" using ps by auto
    moreover have full_cases_disjoint: "disjoint_cases cases" using new_slice ps(1) full_cases by auto
    moreover show ?thesis using full_cases_disjoint full_cases early_step late_step apply (auto simp add: disjoint_cases_def)
      by (metis casesStepImpliesAppendCasesStep prod_cases5)
  qed
  done

fun disjoint_cases_contract :: "FContract \<Rightarrow> bool" and disjoint_cases_in_cases :: "FCase list \<Rightarrow> bool" where
"disjoint_cases_contract (When cases t cont) = (disjoint_cases_in_cases cases \<and> disjoint_cases cases \<and> disjoint_cases_contract cont)" |
"disjoint_cases_contract Close = True" |
"disjoint_cases_contract (StatementCont s cont) = disjoint_cases_contract cont" |
"disjoint_cases_contract (If obs trueCont falseCont) = (disjoint_cases_contract trueCont \<and> disjoint_cases_contract falseCont)" |
"disjoint_cases_contract (Let vid val cont) = disjoint_cases_contract cont" |
"disjoint_cases_contract (LetObservation obsId obs cont) = disjoint_cases_contract cont" |
"disjoint_cases_contract (LetPubKey pkId pk cont) = disjoint_cases_contract cont" |
"disjoint_cases_contract (LetC cid params bodyCont nextCont) = (disjoint_cases_contract bodyCont \<and> disjoint_cases_contract nextCont)" |
"disjoint_cases_contract (UseC cid args) = True" |
"disjoint_cases_in_cases [] = True" |
"disjoint_cases_in_cases ((Case g c)#cases) = (disjoint_cases_contract c \<and> disjoint_cases_in_cases cases)"

fun disjoint_cases_context :: "FContext \<Rightarrow> bool" where
"disjoint_cases_context [] = True" |
"disjoint_cases_context ((ContractAbstraction (cid, params, bodyCont))#rest) = (disjoint_cases_contract bodyCont \<and> disjoint_cases_context rest)" |
"disjoint_cases_context ((ValueAbstraction vid)#rest) = disjoint_cases_context rest" |
"disjoint_cases_context ((ObservationAbstraction obsId)#rest) = disjoint_cases_context rest" |
"disjoint_cases_context ((PubKeyAbstraction pkId)#rest) = disjoint_cases_context rest"

fun disjoint_cases_process :: "FConfig \<Rightarrow> bool" where
"disjoint_cases_process (cont, ctx, state, env, warnings, payments) = (disjoint_cases_contract cont \<and> disjoint_cases_context ctx)"

fun split_cases_relation_disjoint_cases :: "TokenName \<Rightarrow> ChoiceName \<Rightarrow> nat \<Rightarrow> FConfig \<Rightarrow> FConfig \<Rightarrow> bool" where
"split_cases_relation_disjoint_cases role choiceName k
  (c1, ctx1, s1, env1, warnings1, payments1)
  (c2, ctx2, s2, env2, warnings2, payments2) =
    (disjoint_cases_process (c1, ctx1, s1, env1, warnings1, payments1) \<and>
    split_cases_relation role choiceName k (c1, ctx1, s1, env1, warnings1, payments1) (c2, ctx2, s2, env2, warnings2, payments2))"

fun split_cases_relation_disjoint_cases_set :: "TokenName \<Rightarrow> ChoiceName \<Rightarrow> nat \<Rightarrow> FConfig rel" where
"split_cases_relation_disjoint_cases_set role choiceName k =  {(p, q) | p q . disjoint_cases_process p} \<inter> (split_cases_relation_set role choiceName k)"

lemma split_cases_relation_disjoint_cases_gives_set:
"split_cases_relation_disjoint_cases role choiceName k p q \<longleftrightarrow> (p, q) \<in> (split_cases_relation_disjoint_cases_set role choiceName k)"
  by (cases p; cases q; auto)

lemma disjoint_cases_context_append:
"disjoint_cases_context ctx1 \<Longrightarrow>
  disjoint_cases_context ctx2 \<Longrightarrow>
  disjoint_cases_context (ctx1 @ ctx2)"
  by (induction ctx1 rule: disjoint_cases_context.induct, auto)

lemma small_step_reduce_preserves_disjoint_cases:
"(cont1, ctx1, state1, env1, warnings1, payments1) \<rightarrow>\<^sub>f\<^sub>2 (cont2, ctx2, state2, env2, warnings2, payments2) \<Longrightarrow>
  disjoint_cases_process (cont1, ctx1, state1, env1, convertReduceWarnings warnings1, payments1) \<Longrightarrow>
  disjoint_cases_process (cont2, ctx2, state2, env2, convertReduceWarnings warnings2, payments2)"
proof (induction cont1 ctx1 state1 env1 warnings1 payments1 cont2 ctx2 state2 env2 warnings2 payments2 rule: small_step_reduce.induct[split_format(complete)])
  case (UseCFound ctx cid params bodyCont innerCtx env s warns payments bodyResCont bodyResCtx bodyResState bodyResEnv bodyResWarnings bodyResPayments)
  moreover have "disjoint_cases_contract bodyCont \<and> disjoint_cases_context innerCtx" using calculation(1,4) by (induction ctx cid rule: lookupContractIdAbsInformation.induct, auto split: if_split if_split_asm)
  moreover have "disjoint_cases_context (paramsToFContext params)" by (induction params rule: paramsToFContext.induct, auto)
  ultimately show ?case using disjoint_cases_context_append by auto
qed auto

lemma prependStatements_preserves_disjoint_cases:
"disjoint_cases_contract c \<Longrightarrow>
  disjoint_cases_contract (prependStatements stmts c)"
  by (induction stmts c rule: prependStatements.induct, auto)

lemma small_step_case_preserves_disjoint_cases_relation:
"inp \<turnstile> (c1, ctx1, state1, env1, reduceWarnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 (c1', ctx1', state1', env1', reduceWarnings1', payments1') \<Longrightarrow>
  disjoint_cases_in_cases [c1] \<Longrightarrow>
  disjoint_cases_context ctx1 \<Longrightarrow>
  disjoint_cases_contract tc \<Longrightarrow>
  disjoint_cases_contract (c1' t tc) \<and> disjoint_cases_context ctx1'"
proof (induction inp c1 ctx1 state1 env1 reduceWarnings1 payments1 c1' ctx1' state1' env1' reduceWarnings1' payments1' rule: small_step_reduce_case.induct[split_format(complete)])
  case (CaseGuardCompleteStep ins g ctx s env warnings payments stmts newCtx newS newEnv newWarnings newPayments c)
  then show ?case using prependStatements_preserves_disjoint_cases reduceGuardExpressionsConstants by (auto, blast)
next
  case (CaseGuardIncompleteStep ins g ctx s env warnings payments stmts innerG newCtx newS newEnv newWarnings newPayments c)
  moreover have "disjoint_cases_in_cases [FaustusV2AST.FCase.Case innerG c]" using calculation by auto
  moreover have "disjoint_cases_contract (When [FaustusV2AST.FCase.Case innerG c] t tc)" using calculation single_case_disjoint by auto
  moreover have "disjoint_cases_contract (prependStatements stmts (When [FaustusV2AST.FCase.Case innerG c] t tc))" using calculation prependStatements_preserves_disjoint_cases by auto
  ultimately show ?case using reduceGuardExpressionsConstants by (auto, blast)
qed

lemma small_step_cases_preserves_disjoint_cases_relation:
"inp \<turnstile> (c1, ctx1, state1, env1, reduceWarnings1, payments1) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (c1', ctx1', state1', env1', reduceWarnings1', payments1') \<Longrightarrow>
  disjoint_cases_in_cases c1 \<Longrightarrow>
  disjoint_cases_context ctx1 \<Longrightarrow>
  disjoint_cases_contract tc \<Longrightarrow>
  disjoint_cases_contract (c1' t tc) \<and> disjoint_cases_context ctx1'"
proof (induction inp c1 ctx1 state1 env1 reduceWarnings1 payments1 c1' ctx1' state1' env1' reduceWarnings1' payments1' rule: small_step_reduce_cases.induct[split_format(complete)])
  case (CasesHeadMatches ins c ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments rest)
  then show ?case by (metis FaustusV2AST.FCase.exhaust disjoint_cases_in_cases.simps(1) disjoint_cases_in_cases.simps(2) small_step_case_preserves_disjoint_cases_relation)
next
  case (CasesTailMatches ins rest ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments c)
  then show ?case using disjoint_cases_in_cases.elims(2) by blast
qed

lemma receive_preserves_disjoint_cases_relation:
"(c1, ctx1, state1, env1, reduceWarnings1, payments1) \<rightarrow>\<^sup>(inp, lower, upper) (c1', ctx1', state1', env1', reduceWarnings1', payments1') \<Longrightarrow>
  disjoint_cases_contract c1 \<Longrightarrow>
  disjoint_cases_context ctx1 \<Longrightarrow>
  disjoint_cases_contract c1' \<and> disjoint_cases_context ctx1'"
proof (induction c1 ctx1 state1 env1 reduceWarnings1 payments1 inp lower upper c1' ctx1' state1' env1' reduceWarnings1' payments1' rule: small_step_receive_ccs.induct[split_format(complete)])
  case (LambdaReceive lower upper state newTimeEnv newTimeState realLower realUpper t freshChoiceName inp cases ctx warnings payments newC newCtx newState newEnv newWarnings newPayments c env)
  then show ?case using disjoint_cases_contract.simps(1) small_step_cases_preserves_disjoint_cases_relation by blast
next
  case (CCSTimeout lower upper state newTimeEnv newTimeState realLower realUpper t freshChoiceName cases c ctx env warnings payments)
  then show ?case by auto
qed

lemma receive_choice_preserves_disjoint_cases_relation:
"choiceName \<turnstile> (c1, ctx1, state1, env1, reduceWarnings1, payments1) \<rightarrow>\<^sup>(inp, lower, upper) (c1', ctx1', state1', env1', reduceWarnings1', payments1') \<Longrightarrow>
  disjoint_cases_contract c1 \<Longrightarrow>
  disjoint_cases_context ctx1 \<Longrightarrow>
  disjoint_cases_contract c1' \<and> disjoint_cases_context ctx1'"
proof (induction choiceName c1 ctx1 state1 env1 reduceWarnings1 payments1 inp lower upper c1' ctx1' state1' env1' reduceWarnings1' payments1' rule: small_step_receive_ccs_add_choice.induct[split_format(complete)])
  case (NoChoiceLambdaReceive )
  then show ?case using disjoint_cases_contract.simps(1) small_step_cases_preserves_disjoint_cases_relation by blast
next
  case (ChoiceLambdaReceive)
  then show ?case by (metis receive_preserves_disjoint_cases_relation)
next
  case (CCSTimeout lower upper state newTimeEnv newTimeState realLower realUpper t freshChoiceName cases c ctx env warnings payments)
  then show ?case by auto
qed

lemma receive_disjoint_cases_append:
"(When cases t cont, ctx1, state, env, warnings, payments) \<rightarrow>\<^sup>(i, lower, upper) (c1', ctx1', state1', env1', warnings1', payments1') \<Longrightarrow>
  disjoint_cases (cases2 @ cases) \<Longrightarrow>
  (When (cases2 @ cases) t cont, ctx1, state, env, warnings, payments) \<rightarrow>\<^sup>(i, lower, upper) (c1', ctx1', state1', env1', warnings1', payments1')"
proof (induction "When cases t cont" ctx1 state env warnings payments i lower upper c1' ctx1' state1' env1' warnings1' payments1' rule: small_step_receive_ccs.induct[split_format(complete)])
  case (LambdaReceive lower upper state newTimeEnv newTimeState realLower realUpper inp ctx warnings payments newC newCtx newState newEnv newWarnings newPayments i1 lower1 upper1 env)
  moreover have "finalCases inp (cases2, ctx, newTimeState, newTimeEnv, warnings, payments)" using LambdaReceive(5,7) finalCases_def disjoint_cases_def apply auto
    by (metis (no_types, opaque_lifting) casesStepImpliesAppendCasesStep)
  ultimately show ?case using casesStepAppendFinalCases small_step_receive_ccs.LambdaReceive by presburger
next
  case (CCSTimeout)
  then show ?case using small_step_receive_ccs.CCSTimeout by blast
qed

lemma full_nd_semantics_disjoint_cases_append:
"A \<turnstile> (When cases t cont, ctx1, state, env, warnings, payments) \<midarrow>\<midarrow>\<^sup>\<mu>\<rightarrow> (c1', ctx1', state1', env1', warnings1', payments1') \<Longrightarrow>
  disjoint_cases (cases2 @ cases) \<Longrightarrow>
  A \<turnstile> (When (cases2 @ cases) t cont, ctx1, state, env, warnings, payments) \<midarrow>\<midarrow>\<^sup>\<mu>\<rightarrow> (c1', ctx1', state1', env1', warnings1', payments1')"
proof (induction "When cases t cont" ctx1 state env warnings payments \<mu> c1' ctx1' state1' env1' warnings1' payments1' rule: full_nd_semantics.induct[split_format(complete)])
  case (BaseInternalFullND ctx1 state1 env1 warnings1 payments1 c1' ctx1' state1' env1' warnings1' payments1' A)
  moreover have "(FaustusV2AST.FContract.When (cases2 @ cases) t cont, ctx1, state1, env1, warnings1, payments1) \<rightarrow>\<^sup>\<T> (c1', ctx1', state1', env1', warnings1', payments1')" using BaseInternalFullND(1) internal_reaction.simps by auto
  ultimately show ?case using full_nd_semantics.BaseInternalFullND by blast
next
  case (BaseReceiveFullND i A ctx1 state1 env1 warnings1 payments1 lower upper c1' ctx1' state1' env1' warnings1' payments1')
  then show ?case using receive_disjoint_cases_append by (simp add: full_nd_semantics.BaseReceiveFullND)
next
  case (RestrictedReceiveFullND i A a a b lower upper a a a a b a a a a a b a a a a a b)
  then show ?case by (metis (no_types, lifting) full_nd_semantics.RestrictedReceiveFullND receive_disjoint_cases_append)
next
  case (TimeoutFullND a a a a b lower upper a a a a a b A)
  then show ?case by (simp add: full_nd_semantics.TimeoutFullND receive_disjoint_cases_append)
qed

lemma full_semantics_preserves_disjoint_cases_relation:
"A \<turnstile> (c1, ctx1, state1, env1, reduceWarnings1, payments1) \<midarrow>\<^sup>\<mu>\<rightarrow> (c1', ctx1', state1', env1', reduceWarnings1', payments1') \<Longrightarrow>
  disjoint_cases_contract c1 \<Longrightarrow>
  disjoint_cases_context ctx1 \<Longrightarrow>
  disjoint_cases_contract c1' \<and> disjoint_cases_context ctx1'"
proof (induction A c1 ctx1 state1 env1 reduceWarnings1 payments1 \<mu> c1' ctx1' state1' env1' reduceWarnings1' payments1' rule: full_semantics.induct[split_format(complete)])
  case (BaseInternalFull)
  then show ?case using internal_reaction.simps small_step_reduce_preserves_disjoint_cases by auto
next
  case (BaseReceiveFull)
  then show ?case using receive_preserves_disjoint_cases_relation by force
next
  case (RestrictedReceiveFull)
  then show ?case by (meson receive_preserves_disjoint_cases_relation)
next
  case (TimeoutFull)
  then show ?case by auto
qed

lemma full_nd_semantics_preserves_disjoint_cases_relation:
"A \<turnstile> (c1, ctx1, state1, env1, reduceWarnings1, payments1) \<midarrow>\<midarrow>\<^sup>\<mu>\<rightarrow> (c1', ctx1', state1', env1', reduceWarnings1', payments1') \<Longrightarrow>
  disjoint_cases_contract c1 \<Longrightarrow>
  disjoint_cases_context ctx1 \<Longrightarrow>
  disjoint_cases_contract c1' \<and> disjoint_cases_context ctx1'"
proof (induction A c1 ctx1 state1 env1 reduceWarnings1 payments1 \<mu> c1' ctx1' state1' env1' reduceWarnings1' payments1' rule: full_nd_semantics.induct[split_format(complete)])
  case (BaseInternalFullND)
  then show ?case using internal_reaction.simps small_step_reduce_preserves_disjoint_cases by auto
next
  case (BaseReceiveFullND)
  then show ?case using receive_preserves_disjoint_cases_relation by force
next
  case (RestrictedReceiveFullND)
  then show ?case by (meson receive_preserves_disjoint_cases_relation)
next
  case (TimeoutFullND)
  then show ?case by auto
qed

lemma small_step_receive_time_change_before_or_during:
"(When cases t cont, ctx1, newTimeState, newTimeEnv, warnings, payments) \<rightarrow>\<^sup>(i, lower, upper) (c1', ctx1', state1', env1', warnings1', payments1') \<Longrightarrow>
  fixFInterval (lower, upper) state = (newTimeEnv, newTimeState) \<Longrightarrow>
  (When cases t cont, ctx1, state, env, warnings, payments) \<rightarrow>\<^sup>(i, lower, upper) (c1', ctx1', state1', env1', warnings1', payments1')"
proof (induction "When cases t cont" ctx1 newTimeState newTimeEnv warnings payments i lower upper c1' ctx1' state1' env1' warnings1' payments1' arbitrary: state rule: small_step_receive_ccs.induct[split_format(complete)])
  case (LambdaReceive lower upper state1 newTimeEnv newTimeState realLower realUpper inp ctx warnings payments newC newCtx newState newEnv newWarnings newPayments i1 lower1 upper1 env)
  moreover have "lower1 = lower \<and> upper1 = upper" using calculation by auto
  moreover have "state1 = newTimeState" using calculation apply auto
    apply (cases "lower < (FState.minTime state1)", auto)
    by (cases "realLower < (FState.minTime state)", auto)
  moreover have "env = newTimeEnv" using calculation apply auto
    apply (cases "lower < (FState.minTime state1)", auto)
    by (metis FState.select_convs(5) FState.surjective FState.update_convs(5) LambdaReceive.prems fixFIntervalOnlyTimeDependent)+
  ultimately show ?case apply (auto simp del: fixFInterval.simps)
    using small_step_receive_ccs.LambdaReceive[of lower upper state newTimeEnv newTimeState realLower realUpper t inp cases ctx warnings payments newC newCtx newState newEnv newWarnings newPayments _ cont] by presburger
next
  case (CCSTimeout lower upper state1 newTimeEnv newTimeState realLower realUpper i1 lower1 upper1 ctx env warnings payments)
  moreover have "lower1 = lower \<and> upper1 = upper" using calculation by auto
  moreover have "state1 = newTimeState" using calculation apply auto
    apply (cases "lower < (FState.minTime state1)", auto)
    by (cases "realLower < (FState.minTime state)", auto)
  moreover have "env = newTimeEnv" using calculation apply auto
    apply (cases "lower < (FState.minTime state1)", auto)
    by (metis FState.select_convs(5) FState.surjective FState.update_convs(5) CCSTimeout.prems fixFIntervalOnlyTimeDependent)+
  ultimately show ?case using small_step_receive_ccs.CCSTimeout by auto
qed

lemma full_nd_semantics_time_change_before_or_during:
"A \<turnstile> (When cases t cont, ctx1, newTimeState, newTimeEnv, warnings, payments) \<midarrow>\<midarrow>\<^sup>\<lambda>\<^sup>(i, lower, upper)\<rightarrow> (c1', ctx1', state1', env1', warnings1', payments1') \<Longrightarrow>
  fixFInterval (lower, upper) state = (newTimeEnv, newTimeState) \<Longrightarrow>
  A \<turnstile> (When cases t cont, ctx1, state, env, warnings, payments) \<midarrow>\<midarrow>\<^sup>\<lambda>\<^sup>(i, lower, upper)\<rightarrow> (c1', ctx1', state1', env1', warnings1', payments1')"
proof (induction A "(When cases t cont, ctx1, newTimeState, newTimeEnv, warnings, payments)" "\<lambda>\<^sup>(i, lower, upper)" "(c1', ctx1', state1', env1', warnings1', payments1')" rule: full_nd_semantics.induct)
  case (BaseReceiveFullND i A)
  then show ?case by (meson full_nd_semantics.BaseReceiveFullND small_step_receive_time_change_before_or_during)
next
  case (RestrictedReceiveFullND i A lower upper p3)
  moreover obtain j where j_obtained: "j \<in> A \<and> (FaustusV2AST.FContract.When cases t cont, ctx1, newTimeState, newTimeEnv, warnings, payments) \<rightarrow>\<^sup>(Some j, lower, upper) p3" using calculation by auto
  moreover have "(FaustusV2AST.FContract.When cases t cont, ctx1, state, env, warnings, payments) \<rightarrow>\<^sup>(Some j, lower, upper) p3" using j_obtained RestrictedReceiveFullND(6) RestrictedReceiveFullND(2) small_step_receive_time_change_before_or_during[of cases t cont ctx1 newTimeState newTimeEnv warnings payments "Some j" lower upper ] apply (cases p3)
    by auto
  ultimately show ?case by (metis full_nd_semantics.RestrictedReceiveFullND)
next
  case (TimeoutFullND A)
  then show ?case by (meson full_nd_semantics.TimeoutFullND small_step_receive_time_change_before_or_during)
qed
 


lemma full_nd_semantics_preserves_split_cases_disjoint_backwards:
"((A \<union> {(IChoice (ChoiceId choiceName (Party.Role role)) 0)}) \<turnstile> (c2, ctx2, state2, env2, warnings2, payments2) \<midarrow>\<midarrow>\<^sup>\<mu>\<rightarrow> (c2', ctx2', state2', env2', warnings2', payments2')) \<Longrightarrow>
split_cases_relation_disjoint_cases role choiceName k (c1, ctx1, state1, env1, warnings1, payments1) (c2, ctx2, state2, env2, warnings2, payments2) \<Longrightarrow>
  \<exists>c1' ctx1' state1' env1' warnings1' payments1'.
    (A \<turnstile> (c1, ctx1, state1, env1, warnings1, payments1) \<midarrow>\<midarrow>\<^sup>\<mu>\<rightarrow> (c1', ctx1', state1', env1', warnings1', payments1')) \<and>
    split_cases_relation_disjoint_cases role choiceName k (c1', ctx1', state1', env1', warnings1', payments1') (c2', ctx2', state2', env2', warnings2', payments2')"
proof (induction "A \<union> {(IChoice (ChoiceId choiceName (Party.Role role)) 0)}" "(c2, ctx2, state2, env2, warnings2, payments2)" \<mu> "(c2', ctx2', state2', env2', warnings2', payments2')"
    arbitrary: c1 ctx1 state1 env1 warnings1 payments1 c2' ctx2' state2' env2' warnings2' payments2' c2 ctx2 state2 env2 warnings2 payments2
    rule: full_nd_semantics.induct)
  case BaseInternalFullND
  then show ?case by (smt (z3) full_nd_semantics.BaseInternalFullND internal_reaction.simps small_step_preserves_inverse_split_cases_relation small_step_reduce_preserves_disjoint_cases split_cases_relation.simps split_cases_relation_disjoint_cases.simps)
next
  case (BaseReceiveFullND i lower upper)
  moreover have "i \<noteq> IChoice (ChoiceId choiceName (Party.Role role)) 0" using calculation(1) by fastforce
  moreover have "(ChoiceId choiceName (Party.Role role)) \<turnstile> (c2, ctx2, state2, env2, warnings2, payments2) \<rightarrow>\<^sup>(Some i, lower, upper) (c2', ctx2', state2', env2', warnings2', payments2')" using NoChoiceLambdaReceive calculation(2) calculation(4) by presburger
  moreover obtain c1' ctx1' state1' env1' warnings1' payments1' where "((ChoiceId choiceName (Party.Role role)) \<turnstile> (c1, ctx1, state1, env1, warnings1, payments1) \<rightarrow>\<^sup>(Some i, lower, upper) (c1', ctx1', state1', env1', warnings1', payments1')) \<and>
    split_cases_relation_disjoint_cases role choiceName k (c1', ctx1', state1', env1', warnings1', payments1') (c2', ctx2', state2', env2', warnings2', payments2')" using split_cases_relation_backward_simulation_small_step_receive_add_choice BaseReceiveFull calculation by (metis disjoint_cases_process.simps receive_choice_preserves_disjoint_cases_relation split_cases_relation_disjoint_cases.simps)
  moreover have "(c1, ctx1, state1, env1, warnings1, payments1) \<rightarrow>\<^sup>(Some i, lower, upper) (c1', ctx1', state1', env1', warnings1', payments1')" by (meson BaseReceiveFullND.prems calculation(6) choice_receive_same_as_basic_when_fresh split_cases_relation.simps split_cases_relation_disjoint_cases.simps)
  ultimately show ?case by (meson UnI1 full_nd_semantics.BaseReceiveFullND)
next
  case (RestrictedReceiveFullND i \<mu> lower upper p3)
  moreover obtain j where j_obtained: "j \<in> A \<union> {IChoice (ChoiceId choiceName (Party.Role role)) 0} \<and> (c2, ctx2, state2, env2, warnings2, payments2) \<rightarrow>\<^sup>(Some j, lower, upper) p3" using RestrictedReceiveFullND(3) by auto
  moreover have j_not_choice_small_step_receive_add_choice_to_p3: "j \<noteq> IChoice (ChoiceId choiceName (Party.Role role)) 0 \<longrightarrow> ((ChoiceId choiceName (Party.Role role)) \<turnstile> (c2, ctx2, state2, env2, warnings2, payments2) \<rightarrow>\<^sup>(Some j, lower, upper) p3)" using NoChoiceLambdaReceive j_obtained by auto
  moreover obtain c1' ctx1' state1' env1' warnings1' payments1' where j_not_choice_c1_inner_step: "j \<noteq> IChoice (ChoiceId choiceName (Party.Role role)) 0 \<longrightarrow> ((ChoiceId choiceName (Party.Role role)) \<turnstile> (c1, ctx1, state1, env1, warnings1, payments1) \<rightarrow>\<^sup>(Some j, lower, upper) (c1', ctx1', state1', env1', warnings1', payments1')) \<and>
      split_cases_relation role choiceName k (c1', ctx1', state1', env1', warnings1', payments1') p3"
    using split_cases_relation_backward_simulation_small_step_receive_add_choice[of choiceName role c2 ctx2 state2 env2 warnings2 payments2 "Some j" lower upper _ _ _ _ _ _ k c1 ctx1 state1 env1 warnings1 payments1] j_obtained j_not_choice_small_step_receive_add_choice_to_p3 RestrictedReceiveFullND(6) by (smt (verit, del_insts) disjoint_cases_process.cases split_cases_relation_disjoint_cases.simps)
  moreover have j_not_choice_inner_step_disjoint: "j \<noteq> IChoice (ChoiceId choiceName (Party.Role role)) 0 \<longrightarrow> disjoint_cases_contract c1' \<and> disjoint_cases_context ctx1'" using receive_choice_preserves_disjoint_cases_relation[of "(ChoiceId choiceName (Party.Role role))" c1 ctx1 state1 env1 warnings1 payments1 "Some j" lower upper c1' ctx1' state1' env1' warnings1' payments1'] j_not_choice_c1_inner_step local.RestrictedReceiveFullND(6)
    by (auto simp only: split_cases_relation_disjoint_cases.simps disjoint_cases_process.simps)
  moreover obtain finalC1' finalCtx1' finalState1' finalEnv1' finalWarnings1' finalPayments1' where j_not_choice_c1_final_step: "j \<noteq> IChoice (ChoiceId choiceName (Party.Role role)) 0 \<longrightarrow> A \<turnstile> (c1', ctx1', state1', env1', warnings1', payments1') \<midarrow>\<midarrow>\<^sup>\<lambda>\<^sup>\<mu>\<rightarrow> (finalC1', finalCtx1', finalState1', finalEnv1', finalWarnings1', finalPayments1') \<and>
    split_cases_relation_disjoint_cases role choiceName k (finalC1', finalCtx1', finalState1', finalEnv1', finalWarnings1', finalPayments1') (c2', ctx2', state2', env2', warnings2', payments2')"
    using RestrictedReceiveFullND(5)[of _ _ _ _ _ _ c1' ctx1' state1' env1' warnings1' payments1'] j_not_choice_c1_inner_step j_not_choice_inner_step_disjoint apply (cases p3)
    by (metis j_not_choice_inner_step_disjoint disjoint_cases_process.simps split_cases_relation_disjoint_cases.simps)
  moreover have "j \<noteq> IChoice (ChoiceId choiceName (Party.Role role)) 0 \<longrightarrow> A \<turnstile> (c1, ctx1, state1, env1, warnings1, payments1) \<midarrow>\<midarrow>\<^sup>\<lambda>\<^sup>\<mu>\<rightarrow> (finalC1', finalCtx1', finalState1', finalEnv1', finalWarnings1', finalPayments1')" using j_not_choice_c1_final_step full_nd_semantics.RestrictedReceiveFullND RestrictedReceiveFullND by (metis (no_types, lifting) Un_insert_right choice_receive_same_as_basic_when_fresh insert_iff j_not_choice_c1_inner_step j_obtained split_cases_relation.simps split_cases_relation_disjoint_cases.simps sup_bot_right)
  moreover obtain tCont2 cases2 t where decompile_c2: "c2 = When cases2 t tCont2" using RestrictedReceiveFullND by auto
  moreover obtain cases1 tCont1 where decompile: "c1 = When cases1 t tCont1 \<and>
    split_cases role choiceName k tCont1 = tCont2" using RestrictedReceiveFullND(6) decompile_c2 apply (cases c1, auto)
    by (smt (verit) FaustusV2AST.FContract.inject(3))
  moreover obtain newTimeEnv1 newTimeState1 newTimeState2 newTimeEnv2 where new_states: "fixFInterval (lower, upper) state1 = (newTimeEnv1, newTimeState1) \<and> fixFInterval (lower, upper) state2 = (newTimeEnv2, newTimeState2)" using calculation by (cases "lower < (minTime state1)"; cases "lower < (minTime state2)", auto)
  moreover have times_equal: "minTime state1 = minTime state2" using RestrictedReceiveFullND by auto
  moreover have new_times_equal: "newTimeEnv1 = newTimeEnv2 \<and> minTime newTimeState1 = minTime newTimeState2" using times_equal new_states fixFIntervalOnlyTimeDependent by metis
  moreover have time_state_1_only_time_changes: "choices newTimeState1 = choices state1 \<and>
    accounts newTimeState1 = accounts state1 \<and>
    boundValues newTimeState1 = boundValues state1 \<and>
    boundPubKeys newTimeState1 = boundPubKeys state1" using new_states by (cases "lower < (minTime state1)", auto)
  moreover have time_state_2_only_time_changes: "choices newTimeState2 = choices state2 \<and>
    accounts newTimeState2 = accounts state2 \<and>
    boundValues newTimeState2 = boundValues state2 \<and>
    boundPubKeys newTimeState2 = boundPubKeys state2" using new_states by (cases "lower < (minTime state2)", auto)
  moreover have newTimeStatesEq: "state_minimal_eq choiceName newTimeState2 newTimeState1" using RestrictedReceiveFullND new_states times_equal new_times_equal time_state_1_only_time_changes time_state_2_only_time_changes by auto
  moreover have fresh_choice_in_cases1: "isFreshChoiceInCasesGuards choiceName cases1" using calculation isFreshChoiceInCasesImpliesIsFreshChoiceInCasesGuards by auto
  moreover have "split_cases_context role choiceName k ctx1 = ctx2" using calculation by auto
  moreover obtain newCases where newCases_obtained: "newCases = map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) cases1" using calculation by auto
  moreover obtain takeNewCases dropNewCases where split_newCases_obtained: "take k newCases = takeNewCases \<and> drop k newCases = dropNewCases" by auto
  moreover have cases2_is_takeNewCases_k_size: "(k > 1 \<and> length cases1 > k) \<longrightarrow> cases2 = (takeNewCases @ [Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) (split_cases role choiceName k (When (drop k cases1) t tCont1))])" using RestrictedReceiveFullND(6) decompile_c2 decompile split_newCases_obtained newCases_obtained apply auto
    by metis
  moreover have "isFreshChoiceInCasesGuardsShallow choiceName (map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) cases1)" using fresh_choice_in_cases1 by (induction cases1, auto split: FCase.split)
  moreover have "(\<forall> party i . finalCases (IChoice (ChoiceId choiceName party) i) (takeNewCases, ctx2, newTimeState2, newTimeEnv2, warnings2, payments2))" using calculation by (metis append_take_drop_id freshChoiceShallowNoMatch_cases_step isFreshChoiceInCasesGuardsShallowPreservedInSlice)
  moreover have choiceActionStep: "(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (Choice (FChoiceId choiceName (Role role)) [(0, 0)], ctx2, newTimeState2, newTimeEnv2) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (newTimeState2\<lparr>choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices newTimeState2) \<rparr>, ApplyNoWarning)" by (simp add: small_step_action_fresh_choice_state_eq)
  moreover obtain newChoiceState where new_choice_state: "newTimeState2\<lparr>choices := MList.insert (ChoiceId choiceName (Party.Role role)) 0 (choices newTimeState2) \<rparr> = newChoiceState \<and> state_minimal_eq choiceName newTimeState2 newChoiceState" using small_step_action_fresh_choice_state_eq by auto
  moreover have choiceGuardStep: "(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)]), ctx2, newTimeState2, newTimeEnv2, warnings2, payments2) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 (([], None), ctx2, newChoiceState, newTimeEnv2, warnings2, payments2)" by (metis ActionGuardStep append.right_neutral choiceActionStep convertApplyWarning.simps(1) new_choice_state)
  moreover have choiceCaseStep: "(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) (split_cases role choiceName k (When (drop k cases1) t tCont1)), ctx2, newTimeState2, newTimeEnv2, warnings2, payments2) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 (\<lambda> innerT innerTCont . prependStatements [] (split_cases role choiceName k (When (drop k cases1) t tCont1)), ctx2, newChoiceState, newTimeEnv2, warnings2, payments2)" using CaseGuardCompleteStep choiceGuardStep by meson
  moreover have choiceCasesStep: "(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> ([Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) (split_cases role choiceName k (When (drop k cases1) t tCont1))], ctx2, newTimeState2, newTimeEnv2, warnings2, payments2) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (\<lambda> innerT innerTCont . prependStatements [] (split_cases role choiceName k (When (drop k cases1) t tCont1)), ctx2, newChoiceState, newTimeEnv2, warnings2, payments2)" using CasesHeadMatches choiceCaseStep by force
  moreover have partyRoleIZero: "\<forall>res . computeCasesStep (IChoice (ChoiceId choiceName party) l) ([Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) (split_cases role choiceName k (When (drop k cases1) t tCont1))], ctx2, newTimeState2, newTimeEnv2, warnings2, payments2) = Some res \<longrightarrow> party = (Party.Role role) \<and> l = 0" by auto
  moreover have final_new_cases: "finalCases (IChoice (ChoiceId choiceName (Party.Role role)) 0) (newCases, ctx2, newTimeState2, newTimeEnv2, warnings2, payments2)" by (simp add: calculation freshChoiceShallowNoMatch_cases_step)
  moreover have newCases_fails: "((When newCases t tCont2, ctx2, state2, env2, warnings2, payments2) \<rightarrow>\<^sup>(Some (IChoice (ChoiceId choiceName (Party.Role role)) 0), lower, upper) f) \<Longrightarrow> False" apply (induction "(When newCases t tCont2, ctx2, state2, env2, warnings2, payments2)" "(Some (IChoice (ChoiceId choiceName (Party.Role role)) 0), lower, upper)" "f" rule: small_step_receive_ccs.induct)
    using final_new_cases apply auto
    using calculation(19) calculation(23) freshChoiceShallowMatchNoChoiceInput_cases_step calculation(26) by blast
  moreover have cases2_not_newCases: "j = IChoice (ChoiceId choiceName (Party.Role role)) 0 \<longrightarrow> cases2 \<noteq> newCases" using final_new_cases newCases_fails[of ] decompile_c2 RestrictedReceiveFullND(2) small_step_receive_ccs.simps apply auto
    by (metis ReceiveWhenSomeElim calculation(26) freshChoiceShallowNoMatch_cases_step_aux j_obtained newCases_obtained prod_cases6)
  moreover have "cases2 = newCases \<or> cases2 = (takeNewCases @ [Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) (split_cases role choiceName k (When (drop k cases1) t tCont1))])" using decompile_c2 decompile RestrictedReceiveFullND(6) newCases_obtained split_newCases_obtained apply (auto)
    apply (cases "1 < k \<and> k < length cases1")
     apply auto
    by metis
  moreover have cases2_is_takeNewCases: "j = IChoice (ChoiceId choiceName (Party.Role role)) 0 \<longrightarrow> cases2 = (takeNewCases @ [Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) (split_cases role choiceName k (When (drop k cases1) t tCont1))])" using calculation(37) cases2_not_newCases by linarith
  moreover have take_cases_cases_step: "(IChoice (ChoiceId choiceName (Party.Role role)) 0) \<turnstile> (takeNewCases @ [Case (ActionGuard (Choice (FChoiceId choiceName (Role role)) [(0, 0)])) (split_cases role choiceName k (When (drop k cases1) t tCont1))], ctx2, newTimeState2, newTimeEnv2, warnings2, payments2) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (\<lambda> innerT innerTCont . prependStatements [] (split_cases role choiceName k (When (drop k cases1) t tCont1)), ctx2, newChoiceState, newTimeEnv2, warnings2, payments2)" using calculation casesStepAppendFinalCases choiceCasesStep by presburger
  moreover have "(\<lambda> innerT innerTCont . prependStatements [] (split_cases role choiceName k (When (drop k cases1) t tCont1))) t tCont2 = (split_cases role choiceName k (When (drop k cases1) t tCont1))" by simp
  moreover obtain innerC2 innerCtx2 innerState2 innerEnv2 innerWarnings2 innerPayments2 where decompile_p3: "p3 = (innerC2, innerCtx2, innerState2, innerEnv2, innerWarnings2, innerPayments2)" by (cases p3, auto)
  moreover have inner_choice_step: "j = IChoice (ChoiceId choiceName (Party.Role role)) 0 \<longrightarrow> (When cases2 t tCont2, ctx2, state2, env2, warnings2, payments2) \<rightarrow>\<^sup>(Some (IChoice (ChoiceId choiceName (Party.Role role)) 0), lower, upper) (innerC2, innerCtx2, innerState2, innerEnv2, innerWarnings2, innerPayments2)" using calculation(2) j_obtained decompile_p3 decompile_c2 by auto
  moreover have decompile_innerC2: "j = IChoice (ChoiceId choiceName (Party.Role role)) 0 \<longrightarrow> innerC2 = (split_cases role choiceName k (When (drop k cases1) t tCont1))" using inner_choice_step take_cases_cases_step cases2_is_takeNewCases  apply (auto simp only:)
    apply (elim ReceiveE)
    using new_states deterministicCasesSemantics decompile_c2 decompile by (auto, blast)
  moreover have decompile_innerCtx2: "j = IChoice (ChoiceId choiceName (Party.Role role)) 0 \<longrightarrow> innerCtx2 = ctx2" using inner_choice_step take_cases_cases_step cases2_is_takeNewCases decompile_c2 decompile  apply (auto simp only:) apply(elim ReceiveE)
    apply (auto simp del: split_cases.simps)
    using new_states deterministicCasesSemantics decompile_c2 decompile reduceCasesConstants by blast
  moreover have decompile_innerState2: "j = IChoice (ChoiceId choiceName (Party.Role role)) 0 \<longrightarrow> innerState2 = newChoiceState" using inner_choice_step take_cases_cases_step cases2_is_takeNewCases decompile_c2 decompile apply (auto simp only:)
    apply (elim ReceiveE)
    using new_states deterministicCasesSemantics by (auto simp del: split_cases.simps)
  moreover have decompile_innerEnv2: "j = IChoice (ChoiceId choiceName (Party.Role role)) 0 \<longrightarrow> innerEnv2 = newTimeEnv2" using inner_choice_step take_cases_cases_step cases2_is_takeNewCases decompile_c2 decompile apply (auto simp only:)
    apply (elim ReceiveE)
    using new_states deterministicCasesSemantics by (auto simp del: split_cases.simps)
  moreover have decompile_innerWarnings2: "j = IChoice (ChoiceId choiceName (Party.Role role)) 0 \<longrightarrow> innerWarnings2 = warnings2" using inner_choice_step take_cases_cases_step cases2_is_takeNewCases decompile_c2 decompile apply (auto simp only:)
    apply (elim ReceiveE)
    using new_states deterministicCasesSemantics by (auto simp del: split_cases.simps)
  moreover have decompile_innerPayments2: "j = IChoice (ChoiceId choiceName (Party.Role role)) 0 \<longrightarrow> innerPayments2 = payments2" using inner_choice_step take_cases_cases_step cases2_is_takeNewCases decompile_c2 decompile apply (auto simp only:)
    apply (elim ReceiveE)
    using new_states deterministicCasesSemantics by (auto simp del: split_cases.simps)
  moreover obtain dropCases1 where dropCasesEq: "dropCases1 = drop k cases1 \<and> map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) dropCases1 = dropNewCases" by (metis drop_map newCases_obtained split_newCases_obtained)
  moreover have fresh_choice_drop_cases: "isFreshChoiceInCasesGuards choiceName dropCases1" by (metis append_take_drop_id dropCasesEq fresh_choice_in_cases1 isFreshChoiceInCasesGuardsPreservedInSlice)
  moreover have state1_eq_choice_state: "state_minimal_eq choiceName newTimeState1 newChoiceState" using calculation(27) newTimeStatesEq state_minimal_eq_commutative state_minimal_eq_transitive by (meson new_choice_state)
  moreover have dropCases1_in_relation_inner: "j = IChoice (ChoiceId choiceName (Party.Role role)) 0 \<longrightarrow> split_cases_relation role choiceName k (When dropCases1 t tCont1, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) (innerC2, innerCtx2, innerState2, innerEnv2, innerWarnings2, innerPayments2)" apply (auto simp only: split_cases_relation_disjoint_cases.simps)
    by (metis RestrictedReceiveFullND.prems append_take_drop_id decompile decompile_innerC2 decompile_innerCtx2 decompile_innerEnv2 decompile_innerPayments2 decompile_innerState2 decompile_innerWarnings2 dropCasesEq isFreshChoiceInCasesPreservedInSlice isFreshChoiceInContract.simps(4) isFreshChoiceInProcess.simps new_times_equal split_cases_relation.simps split_cases_relation_disjoint_cases.simps state1_eq_choice_state)
  moreover have "disjoint_cases (drop k cases1)" by (metis append_take_drop_id calculation(6) decompile disjoint_cases_contract.simps(1) disjoint_cases_process.simps slice_cases_preserves_disjoint_cases split_cases_relation_disjoint_cases.simps)
  moreover have disjoint_in_cases: "disjoint_cases_in_cases cases1" using RestrictedReceiveFullND.prems decompile by force 
  moreover have "disjoint_cases_in_cases (drop k cases1)" using disjoint_in_cases apply (induction cases1 arbitrary: k, auto)
    by (metis disjoint_cases_in_cases.elims(2) drop_Cons' list.distinct(1) list.inject)
  moreover have dropCases1_in_complete_relation_inner: "j = IChoice (ChoiceId choiceName (Party.Role role)) 0 \<longrightarrow> split_cases_relation_disjoint_cases role choiceName k (When dropCases1 t tCont1, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) (innerC2, innerCtx2, innerState2, innerEnv2, innerWarnings2, innerPayments2)" by (metis RestrictedReceiveFullND.prems calculation(53) calculation(55) decompile disjoint_cases_contract.simps(1) disjoint_cases_process.simps dropCases1_in_relation_inner dropCasesEq split_cases_relation_disjoint_cases.simps)
  moreover obtain c1' ctx1' state1' env1' warnings1' payments1' where dropCases1_step_in_relation: "j = IChoice (ChoiceId choiceName (Party.Role role)) 0 \<longrightarrow> (A \<turnstile> (When dropCases1 t tCont1, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) \<midarrow>\<midarrow>\<^sup>\<lambda>\<^sup>\<mu>\<rightarrow> (c1', ctx1', state1', env1', warnings1', payments1')) \<and>
       split_cases_relation_disjoint_cases role choiceName k (c1', ctx1', state1', env1', warnings1', payments1') (c2', ctx2', state2', env2', warnings2', payments2')"
    using RestrictedReceiveFullND(5) dropCases1_in_complete_relation_inner decompile_p3 by presburger
  moreover obtain takeCases1 where takeCasesEq: "takeCases1 = take k cases1 \<and> map (\<lambda>(Case g c) \<Rightarrow> (Case g (split_cases role choiceName k c))) takeCases1 = takeNewCases" by (metis newCases_obtained split_newCases_obtained take_map)
  moreover obtain realLower realUpper where real_time_bound: "timeInterval newTimeEnv2 = (realLower, realUpper) \<and> realLower < t \<and> realUpper < t" using RestrictedReceiveFullND new_states decompile_c2 by (auto simp add: small_step_receive_ccs.simps)
  moreover have j_is_choice_transition_time_before: "j = IChoice (ChoiceId choiceName (Party.Role role)) 0 \<longrightarrow> (A \<turnstile> (When cases1 t tCont1, ctx1, newTimeState1, newTimeEnv1, warnings1, payments1) \<midarrow>\<midarrow>\<^sup>\<lambda>\<^sup>\<mu>\<rightarrow> (c1', ctx1', state1', env1', warnings1', payments1')) \<and>
       split_cases_relation_disjoint_cases role choiceName k (c1', ctx1', state1', env1', warnings1', payments1') (c2', ctx2', state2', env2', warnings2', payments2')"
    using takeCasesEq dropCasesEq by (metis RestrictedReceiveFullND.prems append_take_drop_id decompile disjoint_cases_contract.simps(1) disjoint_cases_process.simps dropCases1_step_in_relation full_nd_semantics_disjoint_cases_append split_cases_relation_disjoint_cases.simps)
  moreover show ?case apply (cases "j = IChoice (ChoiceId choiceName (Party.Role role)) 0")
    using j_is_choice_transition_time_before decompile j_obtained apply (metis calculation(2) full_nd_semantics_time_change_before_or_during new_states)
    using j_not_choice_c1_inner_step j_not_choice_c1_final_step j_obtained by (meson calculation(12))
next
  case (TimeoutFullND lower upper)
  moreover obtain cases2 t tCont2 where decompile_c2: "c2 = When cases2 t tCont2" using TimeoutFullND by auto
  moreover obtain cases1 t tCont1 where "c1 = When cases1 t tCont1 \<and> tCont2 = split_cases role choiceName k tCont1" using TimeoutFullND(2) decompile_c2 apply (cases c1, auto)
    by (smt (verit, del_insts) FaustusV2AST.FContract.inject(3))
  moreover obtain newTimeEnv1 newTimeState1 newTimeState2 newTimeEnv2 where new_states: "fixFInterval (lower, upper) state1 = (newTimeEnv1, newTimeState1) \<and> fixFInterval (lower, upper) state2 = (newTimeEnv2, newTimeState2)" using calculation by (cases "lower < (minTime state1)"; cases "lower < (minTime state2)", auto)
  moreover have times_equal: "minTime state1 = minTime state2" using TimeoutFullND by auto
  moreover have new_times_equal: "newTimeEnv1 = newTimeEnv2 \<and> minTime newTimeState1 = minTime newTimeState2" using times_equal new_states fixFIntervalOnlyTimeDependent by metis
  ultimately show ?case by (smt (z3) choice_receive_same_as_basic_when_fresh disjoint_cases_process.simps full_nd_semantics.TimeoutFullND receive_choice_preserves_disjoint_cases_relation small_step_receive_ccs_add_choice.CCSTimeout split_cases_relation.simps split_cases_relation_backward_simulation_small_step_receive_add_choice split_cases_relation_disjoint_cases.simps)
qed

fun SC_set :: "ChoiceName \<Rightarrow> Proc rel" where
"SC_set choiceName =  {((p, A), (q, B)) | p q A B . disjoint_cases_process p \<and> B = (A \<union> {(IChoice (ChoiceId choiceName (Party.Role [0 :: 8 word])) 0)}) \<and> (p, q) \<in> split_cases_relation_set ([0 :: 8 word]) choiceName rcmdk}"

lemma split_cases_relation_disjoint_cases_is_weak_sim:
"weak_nd_sim_simple (SC_set choiceName)"
  apply (auto simp only: SC_set.simps weak_nd_sim_simple_def disjoint_cases_process.simps split_cases_relation_gives_set)
  using full_nd_semantics_maintains_split_cases full_nd_semantics_preserves_disjoint_cases_relation single_full_nd_receive_in_experiment split_cases_relation_gives_set full_nd_semantics_react_is_equivalent_to_internal_reaction mem_Collect_eq apply (smt (z3) disjoint_cases_process.simps react_step_preserves_split_cases_relation)
  using full_nd_semantics_maintains_split_cases full_nd_semantics_preserves_disjoint_cases_relation single_full_nd_receive_in_experiment split_cases_relation_gives_set mem_Collect_eq by (smt disjoint_cases_process.simps)

lemma split_cases_relation_disjoint_cases_inv_is_weak_sim_aux:
"((aj, ak, al, am, an, bb), ao, ap, aq, ar, as, bc) \<in> split_cases_relation_set [0] choiceName rcmdk \<Longrightarrow>
       A \<union> {IChoice (ChoiceId choiceName (Party.Role [0])) 0} \<turnstile> (ao, ap, aq, ar, as, bc) \<midarrow>\<midarrow>\<^sup>\<mu>\<rightarrow> (at, au, av, aw, ax, bd) \<Longrightarrow>
       disjoint_cases_contract aj \<Longrightarrow>
       disjoint_cases_context ak \<Longrightarrow>
       \<exists>c2'. A \<turnstile> (aj, ak, al, am, an, bb) \<midarrow>\<midarrow>\<^sup>\<mu>\<rightarrow> c2' \<and>
             (((at, au, av, aw, ax, bd), A \<union> {IChoice (ChoiceId choiceName (Party.Role [0])) 0}), c2', A) \<in> { ((p, A), q, B)| p A q B . (disjoint_cases_process p \<and> B = A \<union> {IChoice (ChoiceId choiceName (Party.Role [0])) 0} \<and> (p, q) \<in> split_cases_relation_set [0] choiceName rcmdk)}\<inverse>"
  subgoal premises ps proof -
    have "split_cases_relation_disjoint_cases [0] choiceName rcmdk (aj, ak, al, am, an, bb) (ao, ap, aq, ar, as, bc)" using disjoint_cases_process.simps ps(1) ps(3) ps(4) split_cases_relation_disjoint_cases.simps split_cases_relation_gives_set by presburger
    moreover obtain c2' where "A \<turnstile> (aj, ak, al, am, an, bb) \<midarrow>\<midarrow>\<^sup>\<mu>\<rightarrow> c2' \<and> split_cases_relation_disjoint_cases [0] choiceName rcmdk c2' (at, au, av, aw, ax, bd)" using calculation full_nd_semantics_preserves_split_cases_disjoint_backwards ps(2) by fastforce
    moreover have "(((at, au, av, aw, ax, bd), A \<union> {IChoice (ChoiceId choiceName (Party.Role [0])) 0}), c2', A) \<in> { ((p, A), q, B)| p A q B . (disjoint_cases_process p \<and> B = A \<union> {IChoice (ChoiceId choiceName (Party.Role [0])) 0} \<and> (p, q) \<in> split_cases_relation_set [0] choiceName rcmdk)}\<inverse>" using calculation(2) split_cases_relation_disjoint_cases_gives_set by auto
    ultimately show ?thesis by blast
  qed
  done

inductive_cases FullNDInternalE: "A \<turnstile> c1 \<midarrow>\<midarrow>\<^sup>\<tau>\<rightarrow> c2"

lemma split_cases_relation_disjoint_cases_inv_is_weak_sim_aux_2:
"A \<turnstile> c1 \<midarrow>\<midarrow>\<^sup>\<tau>\<rightarrow> c2 \<Longrightarrow>
  c1 \<rightarrow>\<^sup>*\<^sup>\<T> c2"
  apply (elim FullNDInternalE)
  using star_step1[of internal_reaction c1 c2] by force

lemma split_cases_relation_disjoint_cases_inv_is_weak_sim:
"weak_nd_sim_simple ((SC_set choiceName)\<inverse>)"
  apply (auto simp only: weak_nd_sim_simple_def SC_set.simps disjoint_cases_process.simps)
  using split_cases_relation_disjoint_cases_inv_is_weak_sim_aux split_cases_relation_disjoint_cases_inv_is_weak_sim_aux_2
   apply (smt (z3) converseD converseI mem_Collect_eq)
  using split_cases_relation_disjoint_cases_inv_is_weak_sim_aux single_full_nd_receive_in_experiment by (smt (z3) Collect_cong)

lemma split_cases_relation_disjoint_cases_is_bisimulation:
"observation_nd_bisimulation (SC_set choiceName)"
  using observation_nd_bisimulation_def split_cases_relation_disjoint_cases_inv_is_weak_sim split_cases_relation_disjoint_cases_is_weak_sim by presburger

lemma observational_equivalence_of_transformation:
"isFreshChoiceInProcess c (c1, ctx1, s1, e, w, ps) \<Longrightarrow>
  disjoint_cases_process (c1, ctx1, s1, e, w, ps) \<Longrightarrow>
  split_cases [0] c rcmdk c1 = c2 \<Longrightarrow>
  split_cases_context [0] c rcmdk ctx1 = ctx2 \<Longrightarrow>
  state_minimal_eq c s1 s2 \<Longrightarrow>
  ((c1, ctx1, s1, e, w, ps), A) \<approx> ((c2, ctx2, s2, e, w, ps), A \<union> {IChoice (ChoiceId c (Party.Role [0])) 0})"
  subgoal premises ps proof -
    have "(((c1, ctx1, s1, e, w, ps), A), ((c2, ctx2, s2, e, w, ps), A \<union> {IChoice (ChoiceId c (Party.Role [0])) 0})) \<in> (SC_set c)" using ps by auto
    then show ?thesis using observable_equivalence_def split_cases_relation_disjoint_cases_is_bisimulation by blast
  qed
  done

end


