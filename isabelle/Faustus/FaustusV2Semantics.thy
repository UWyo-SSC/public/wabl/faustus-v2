theory FaustusV2Semantics
  imports Main Util.MList Util.SList Core.ListTools Core.Semantics SmallStep "HOL-Library.Product_Lexorder" "HOL.Product_Type" "HOL-IMP.Star" FaustusV2AST FaustusV2TypeChecker
begin

type_synonym FConfiguration = "FContract * FContext * FState * Environment * (ReduceWarning list) * (Payment list)"

inductive small_step_reduce :: "FConfiguration \<Rightarrow> FConfiguration \<Rightarrow> bool" (infix "\<rightarrow>\<^sub>f\<^sub>2" 55) where
CloseRefund:  "refundOne (accounts s) = Some ((party, token, money), newAccount) \<Longrightarrow> 
  (Close, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2 
  (Close, ctx, (s\<lparr>accounts := newAccount\<rparr>), env, warns @ [ReduceNoWarning], payments @ [Payment party (SemanticsTypes.Party party) token money])" |
PayNonPositive: "\<lbrakk>evalFValue env s val = Some res; res \<le> 0; Some fromAcc = (evalFParty s accId); Some toPayee = (evalFPayee s payee)\<rbrakk> \<Longrightarrow>
  (StatementCont (Pay accId payee token val) cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2
  (cont, ctx, s, env, warns @ [ReduceNonPositivePay fromAcc toPayee token res], payments)" |
PayPositivePartialWithPayment: "\<lbrakk>evalFValue env s val = Some res;
  res > 0;
  Some fromAcc = (evalFParty s accId);
  Some toPayee = (evalFPayee s payee);
  res > moneyInAccount fromAcc token (accounts s);
  updateMoneyInAccount fromAcc token 0 (accounts s) = newAccs;
  moneyInAccount fromAcc token (accounts s) = moneyToPay;
  giveMoney fromAcc toPayee token (moneyToPay) newAccs = (payment, finalAccs);
  payment = ReduceWithPayment somePayment\<rbrakk> \<Longrightarrow>
  (StatementCont (Pay accId payee token val) cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2
  ((cont, ctx, s\<lparr>accounts := finalAccs\<rparr>, env, warns @ [ReducePartialPay fromAcc toPayee token moneyToPay res], payments @ [somePayment]))" |
PayPositivePartialWithoutPayment: "\<lbrakk>evalFValue env s val = Some res;
  res > 0;
  Some fromAcc = (evalFParty s accId);
  Some toPayee = (evalFPayee s payee);
  res > moneyInAccount fromAcc token (accounts s);
  updateMoneyInAccount fromAcc token 0 (accounts s) = newAccs;
  moneyInAccount fromAcc token (accounts s) = moneyToPay;
  giveMoney fromAcc toPayee token (moneyToPay) newAccs = (payment, finalAccs);
  payment = ReduceNoPayment\<rbrakk> \<Longrightarrow>
  (StatementCont (Pay accId payee token val) cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2
  ((cont, ctx, s\<lparr>accounts := finalAccs\<rparr>, env, warns @ [ReducePartialPay fromAcc toPayee token moneyToPay res], payments))" |
PayPositiveFullWithPayment: "\<lbrakk>evalFValue env s val = Some res;
  res > 0;
  Some fromAcc = (evalFParty s accId);
  Some toPayee = (evalFPayee s payee);
  res \<le> moneyInAccount fromAcc token (accounts s);
  moneyInAccount fromAcc token (accounts s) = moneyInAcc;
  moneyInAcc - res = newBalance;
  updateMoneyInAccount fromAcc token newBalance (accounts s) = newAccs;
  giveMoney fromAcc toPayee token res newAccs = (payment, finalAccs);
  payment = ReduceWithPayment somePayment\<rbrakk> \<Longrightarrow>
  (StatementCont (Pay accId payee token val) cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2
  (cont, ctx, s\<lparr>accounts := finalAccs\<rparr>, env, warns @ [ReduceNoWarning], payments @ [somePayment])" |
PayPositiveFullWithoutPayment: "\<lbrakk>evalFValue env s val = Some res;
  res > 0;
  Some fromAcc = (evalFParty s accId);
  Some toPayee = (evalFPayee s payee);
  res \<le> moneyInAccount fromAcc token (accounts s);
  moneyInAccount fromAcc token (accounts s) = moneyInAcc;
  moneyInAcc - res = newBalance;
  updateMoneyInAccount fromAcc token newBalance (accounts s) = newAccs;
  giveMoney fromAcc toPayee token res newAccs = (payment, finalAccs);
  payment = ReduceNoPayment\<rbrakk> \<Longrightarrow>
  (StatementCont (Pay accId payee token val) cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2
  (cont, ctx, s\<lparr>accounts := finalAccs\<rparr>, env, warns @ [ReduceNoWarning], payments)" |
IfTrue: "evalFObservation env s obs = Some True \<Longrightarrow> 
  (If obs cont1 cont2, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2
  (cont1, ctx, s, env, warns @ [ReduceNoWarning], payments)" |
IfFalse: "evalFObservation env s obs = Some False \<Longrightarrow> 
  (If obs cont1 cont2, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2
  (cont2, ctx, s, env, warns @ [ReduceNoWarning], payments)" |
WhenTimeout: "\<lbrakk>timeInterval env = (startSlot, endSlot);
  endSlot \<ge> timeout;
  startSlot \<ge> timeout\<rbrakk> \<Longrightarrow>
  (When cases timeout cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2
  (cont, ctx, s, env, warns @ [ReduceNoWarning], payments)" |
LetShadow: "\<lbrakk>lookup valId (boundValues s) = Some oldVal; evalFValue env s val = Some res\<rbrakk> \<Longrightarrow>
  (Let valId val cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2
  (cont, ValueAbstraction valId#ctx, s\<lparr> boundValues := MList.insert valId res (boundValues s)\<rparr>, env, warns @ [ReduceShadowing valId oldVal res], payments)" |
ReassignValShadow: "\<lbrakk>lookup valId (boundValues s) = Some oldVal; evalFValue env s val = Some res\<rbrakk> \<Longrightarrow>
  (StatementCont (ReassignVal valId val) cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2
  (cont, ValueAbstraction valId#ctx, s\<lparr> boundValues := MList.insert valId res (boundValues s)\<rparr>, env, warns @ [ReduceShadowing valId oldVal res], payments)" |
LetNoShadow: "\<lbrakk>lookup valId (boundValues s) = None; evalFValue env s val = Some res\<rbrakk> \<Longrightarrow>
  (Let valId val cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2
  (cont, ValueAbstraction valId#ctx, s\<lparr> boundValues := MList.insert valId res (boundValues s)\<rparr>, env, warns @ [ReduceNoWarning], payments)" |
ReassignValNoShadow: "\<lbrakk>lookup valId (boundValues s) = None; evalFValue env s val = Some res\<rbrakk> \<Longrightarrow>
  (StatementCont (ReassignVal valId val) cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2
  (cont, ValueAbstraction valId#ctx, s\<lparr> boundValues := MList.insert valId res (boundValues s)\<rparr>, env, warns @ [ReduceNoWarning], payments)" |
LetObservation: "\<lbrakk>evalFObservation e s obs = Some res\<rbrakk> \<Longrightarrow>
  (LetObservation oId obs c, cx, s, e, ws, ps) \<rightarrow>\<^sub>f\<^sub>2
  (c, ObservationAbstraction oId#cx, s\<lparr>boundValues := MList.insert oId (if res then 1 else 0) (boundValues s)\<rparr>, e, ws @ [ReduceNoWarning], ps)" |
ReassignObservation: "\<lbrakk>evalFObservation e s obs = Some res\<rbrakk> \<Longrightarrow>
  (StatementCont (ReassignObservation oId obs) c, cx, s, e, ws, ps) \<rightarrow>\<^sub>f\<^sub>2
  (c, ObservationAbstraction oId#cx, s\<lparr>boundValues := MList.insert oId (if res then 1 else 0) (boundValues s)\<rparr>, e, ws @ [ReduceNoWarning], ps)" |
LetPubKey: "\<lbrakk>evalFParty s pk = Some res\<rbrakk> \<Longrightarrow>
  (LetPubKey pId pk c, cx, s, e, ws, ps) \<rightarrow>\<^sub>f\<^sub>2
  (c, PubKeyAbstraction pId#cx, s\<lparr> boundPubKeys := MList.insert pId res (boundPubKeys s)\<rparr>, e, ws @ [ReduceNoWarning], ps)" |
ReassignPubKey: "\<lbrakk>evalFParty s pk = Some res\<rbrakk> \<Longrightarrow>
  (StatementCont (ReassignPubKey pId pk) c, cx, s, e, ws, ps) \<rightarrow>\<^sub>f\<^sub>2
  (c, PubKeyAbstraction pId#cx, s\<lparr> boundPubKeys := MList.insert pId res (boundPubKeys s)\<rparr>, e, ws @ [ReduceNoWarning], ps)" |
AssertTrue: "evalFObservation env s obs = Some True \<Longrightarrow>
  (StatementCont (Assert obs) cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2
  (cont, ctx, s, env, warns @ [ReduceNoWarning], payments)" |
AssertFalse: "evalFObservation env s obs = Some False \<Longrightarrow>
  (StatementCont (Assert obs) cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2
  (cont, ctx, s, env, warns @ [ReduceAssertionFailed], payments)" |
LetC: "(LetC cid params boundCon cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2
  (cont, ContractAbstraction (cid, params, boundCon)#ctx, s, env, warns @ [ReduceNoWarning], payments)" |
UseCValueArg: "\<lbrakk>lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, innerCtx);
  (ValueParameter valId#restParams) = drop (length params - length args) params;
  evalFValue env s val = Some valRes
  \<rbrakk> \<Longrightarrow>
  (UseC cid (ValueArgument val#args), ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2
  (UseC cid args, ctx, s\<lparr>boundValues := MList.insert valId valRes (boundValues s)\<rparr>, env, warns @ [ReduceNoWarning], payments)" |
UseCObservationArg: "\<lbrakk>lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, innerCtx);
  (ObservationParameter obsId#restParams) = drop (length params - length args) params;
  evalFObservation env s obs = Some obsRes
  \<rbrakk> \<Longrightarrow>
  (UseC cid (ObservationArgument obs#args), ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2
  (UseC cid args, ctx, s\<lparr>boundValues := MList.insert obsId (if obsRes then 1 else 0) (boundValues s)\<rparr>, env, warns @ [ReduceNoWarning], payments)" |
UseCPubKeyArg: "\<lbrakk>lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, innerCtx);
  (PubKeyParameter pkId#restParams) = drop (length params - length args) params;
  evalFParty s pk = Some pkRes
  \<rbrakk> \<Longrightarrow>
  (UseC cid (PubKeyArgument pk#args), ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2
  (UseC cid args, ctx, s\<lparr>boundPubKeys := MList.insert pkId pkRes (boundPubKeys s)\<rparr>, env, warns @ [ReduceNoWarning], payments)" |
UseCFound: "\<lbrakk>lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, innerCtx);
  (bodyCont, paramsToFContext params @ innerCtx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2 res\<rbrakk> \<Longrightarrow>
  (UseC cid [], ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2 res"

abbreviation
  small_step_reduces :: "FConfiguration \<Rightarrow> FConfiguration \<Rightarrow> bool" (infix "\<rightarrow>\<^sub>f\<^sub>2*" 55)
where "x \<rightarrow>\<^sub>f\<^sub>2* y == star small_step_reduce x y"

thm small_step_reduce.induct
lemmas small_step_reduce_induct = small_step_reduce.induct[split_format(complete)]
thm small_step_reduce_induct
declare small_step_reduce.intros[simp,intro]

inductive_cases CloseE[elim!]: "(Close, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2 ct"
thm CloseE
inductive_cases PayE[elim!]: "(StatementCont (Pay accId payee token val) cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2 ct"
thm PayE
inductive_cases IfE[elim!]: "(If obs cont1 cont2, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2 ct"
thm IfE
inductive_cases WhenE[elim!]: "(When cases timeout cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2 ct"
thm WhenE
inductive_cases LetE[elim!]: "(Let valId val cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2 ct"
thm LetE
inductive_cases LetObservationE[elim!]: "(LetObservation obsId obs cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2 ct"
thm LetObservationE
inductive_cases LetPubKeyE[elim!]: "(LetPubKey pkId pk cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2 ct"
thm LetPubKeyE
inductive_cases AssertE[elim!]: "(StatementCont (Assert obs) cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2 ct"
thm AssertE
inductive_cases LetCE[elim!]: "(LetC cid boundCon params cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2 ct"
thm LetCE
inductive_cases UseCE[elim!]: "(UseC cid args, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2 ct"
thm UseCE
inductive_cases UseCEmptyE: "(UseC cid [], ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2 ct"
thm UseCEmptyE
inductive_cases ReassignValE[elim!]: "(StatementCont (ReassignVal vId val) cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2 ct"
inductive_cases ReassignObsE[elim!]: "(StatementCont (ReassignObservation oId obs) cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2 ct"
inductive_cases ReassignPkE[elim!]: "(StatementCont (ReassignPubKey pkId pk) cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2 ct"

lemma deterministic:
  "cs \<rightarrow>\<^sub>f\<^sub>2 cs' \<Longrightarrow> cs \<rightarrow>\<^sub>f\<^sub>2 cs'' \<Longrightarrow> cs'' = cs'"
proof(induction arbitrary: cs'' rule: small_step_reduce.induct)
  case (PayNonPositive env s val res fromAcc accId toPayee payee token cont ctx warns payments)
  then show ?case apply auto
     apply (cases "evalFParty s accId", auto)
    by (cases "evalFPayee s payee", auto)
next
  case (PayPositivePartialWithPayment env s val res fromAcc accId toPayee payee token newAccs moneyToPay payment finalAccs somePayment cont ctx warns payments)
  then show ?case apply (auto)
    by (cases "evalFParty s accId"; cases "evalFPayee s payee", auto)+
next
  case (PayPositiveFullWithPayment env s val res fromAcc accId toPayee payee token moneyInAcc newBalance newAccs payment finalAccs somePayment cont ctx warns payments)
  then show ?case apply (auto)
    by (cases "evalFParty s accId"; cases "evalFPayee s payee", auto)+
next
  case (UseCValueArg ctx cid params bodyCont innerCtx valId restParams args env s val valRes warns payments)
  then show ?case by (cases "drop (length params - length args) params", auto)
next
  case (UseCObservationArg ctx cid params bodyCont innerCtx obsId restParams args env s obs obsRes warns payments)
  then show ?case by (cases "drop (length params - length args) params", auto)
next
  case (UseCPubKeyArg ctx cid params bodyCont innerCtx pkId restParams args s pk pkRes env warns payments)
  then show ?case by (cases "drop (length params - length args) params", auto)
next
  case (UseCFound ctx cid params bodyCont innerCtx s env warns payments res)
  moreover have "lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, innerCtx) \<Longrightarrow>
        (bodyCont, paramsToFContext params @ innerCtx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2 cs'' \<Longrightarrow> (bodyCont, paramsToFContext params @ innerCtx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2 cs''" using calculation by auto
  moreover have "(bodyCont, paramsToFContext params @ innerCtx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2 cs''" using calculation by auto
  moreover have "cs'' = res" using calculation by auto
  moreover show ?case using calculation(7) by auto
qed auto

lemma smallStepEnvConstant:
"(c, bc, s, e, w, p) \<rightarrow>\<^sub>f\<^sub>2 (c', bc', s', e', w', p') \<Longrightarrow> e = e'"
  apply (induction c bc s e w p c' bc' s' e' w' p' rule: small_step_reduce_induct)
  by auto

lemma smallStepWarningsAreArbitrary:
"(c, bc, s, e, w, p) \<rightarrow>\<^sub>f\<^sub>2 (c', bc', s', e', w', p') \<Longrightarrow>
  (\<forall>w'' . \<exists>w''' . (c, bc, s, e, w'', p) \<rightarrow>\<^sub>f\<^sub>2 (c', bc', s', e', w''', p'))"
  apply (induction c bc s e w p c' bc' s' e' w' p' rule: small_step_reduce_induct)
  by fastforce+

lemma smallStepExistingWarningsPaymentsConstant:
"(c, bc, s, e, w, p) \<rightarrow>\<^sub>f\<^sub>2 (c', bc', s', e', w', p') \<Longrightarrow>
  (\<exists>newW newP . p' = p @ newP \<and> w' = w @ newW)"
  apply (induction c bc s e w p c' bc' s' e' w' p' rule: small_step_reduce_induct)
  by auto

lemma smallStepPaymentsAreArbitrary:
"(c, bc, s, e, w, p) \<rightarrow>\<^sub>f\<^sub>2 (c', bc', s', e', w', p') \<Longrightarrow>
  (\<forall>p'' . \<exists>p''' . (c, bc, s, e, w, p'') \<rightarrow>\<^sub>f\<^sub>2 (c', bc', s', e', w', p'''))"
  apply (induction c bc s e w p c' bc' s' e' w' p' rule: small_step_reduce_induct)
  by (blast | fastforce)+

lemma smallStepStarWarningsAreArbitrary:
  "(c, bc, s, e, w, p) \<rightarrow>\<^sub>f\<^sub>2* (c', bc', s', e', w'', p') \<Longrightarrow>
    (\<forall>w'' . \<exists>w''' . (c, bc, s, e, w'', p) \<rightarrow>\<^sub>f\<^sub>2* (c', bc', s', e', w''', p'))"
  apply (induction rule: star.induct[of "small_step_reduce", split_format(complete)], auto)
  by (meson smallStepWarningsAreArbitrary star.step)

lemma smallStepStarExistingWarningsPaymentsConstant:
"(c, bc, s, e, w, p) \<rightarrow>\<^sub>f\<^sub>2* (c', bc', s', e', w', p') \<Longrightarrow>
  (\<exists>newW newP . p' = p @ newP \<and> w' = w @ newW)"
  apply (induction rule: star.induct[of "small_step_reduce", split_format(complete)], auto)
  by (metis append.assoc smallStepExistingWarningsPaymentsConstant)+

definition "finalV2 cs \<longleftrightarrow> \<not>(\<exists>cs'. cs \<rightarrow>\<^sub>f\<^sub>2 cs')"

fun validTraceFromConfiguration :: "FConfiguration \<Rightarrow> FConfiguration list \<Rightarrow> bool" where
"validTraceFromConfiguration f1 [] = finalV2 f1" |
"validTraceFromConfiguration f1 (f2#rest) = (f1 \<rightarrow>\<^sub>f\<^sub>2 f2 \<and> validTraceFromConfiguration f2 rest)"

fun validTrace :: "FConfiguration list \<Rightarrow> bool" where
"validTrace [] = False" |
"validTrace (f1#rest) = validTraceFromConfiguration f1 rest"

lemma finalWarningsAndPaymentsAreArbitrary:
"finalV2 (c, bc, s, e, w, p) \<Longrightarrow>
  (\<forall>w' p' . finalV2 (c, bc, s, e, w', p'))"
  using finalV2_def smallStepWarningsAreArbitrary smallStepPaymentsAreArbitrary apply simp
  by meson

inductive small_step_reduce_trans :: "FConfiguration \<Rightarrow> FConfiguration \<Rightarrow> bool" (infix  "\<longrightarrow>\<^sub>f\<^sub>2" 55) where
TransSingleStep: "\<lbrakk>f1 \<rightarrow>\<^sub>f\<^sub>2 f2\<rbrakk> \<Longrightarrow> f1 \<longrightarrow>\<^sub>f\<^sub>2 f2" |
TransMultipleStep: "\<lbrakk>f1 \<rightarrow>\<^sub>f\<^sub>2 f2; f2 \<longrightarrow>\<^sub>f\<^sub>2 f3\<rbrakk> \<Longrightarrow> f1 \<longrightarrow>\<^sub>f\<^sub>2 f3"

lemma smallStepTransImpliesNotFinal:
"f1 \<longrightarrow>\<^sub>f\<^sub>2 f2 \<Longrightarrow> \<not> finalV2 f1"
  apply (induction f1 f2 rule: small_step_reduce_trans.induct, auto simp add: finalV2_def)
  by fastforce+

lemma finalImpliesNoSmallStepTrans:
"finalV2 f1 \<Longrightarrow> \<not> f1 \<longrightarrow>\<^sub>f\<^sub>2 f2"
  using smallStepTransImpliesNotFinal by blast

lemma notFinalImpliesSmallStepTrans:
"\<not> finalV2 f1 \<Longrightarrow> \<exists>f2 . f1 \<longrightarrow>\<^sub>f\<^sub>2 f2"
  by (meson TransSingleStep finalV2_def)

inductive small_step_reduce_all :: "FConfiguration \<Rightarrow> FConfiguration \<Rightarrow> bool" (infix  "\<Down>\<^sub>f\<^sub>2" 55) where
FinalStep: "\<lbrakk>f1 \<rightarrow>\<^sub>f\<^sub>2 f2; finalV2 f2\<rbrakk> \<Longrightarrow> f1 \<Down>\<^sub>f\<^sub>2 f2" |
AddStep: "\<lbrakk>f1 \<rightarrow>\<^sub>f\<^sub>2 f2; f2 \<Down>\<^sub>f\<^sub>2 f3\<rbrakk> \<Longrightarrow> f1 \<Down>\<^sub>f\<^sub>2 f3"

lemmas small_step_reduce_all_induct = small_step_reduce_all.induct[split_format(complete)]
declare small_step_reduce_all.intros[simp,intro]

definition "finalAllV2 cs \<longleftrightarrow> \<not>(\<exists>cs'. cs \<Down>\<^sub>f\<^sub>2 cs')"

lemma freshChoiceInParamsContext:
"isFreshChoiceInContext cn (paramsToFContext params)"
  by (induction params rule: paramsToFContext.induct, auto)

lemma freshChoicePreserved_append:
"isFreshChoiceInContext cn ctx1 \<Longrightarrow>
  isFreshChoiceInContext cn ctx2 \<Longrightarrow>
  isFreshChoiceInContext cn (ctx1 @ ctx2)"
  by (induction ctx1 rule: isFreshChoiceInContext.induct, auto)

lemma freshChoicePreserved_evalArguments:
"evalFArguments env s params args = Some newState \<Longrightarrow>
  isFreshChoiceInState cn s \<Longrightarrow>
  isFreshChoiceInState cn newState"
  by (induction env s params args rule: evalFArguments.induct, auto split: option.split_asm)

lemma freshChoicePreserved_lookupContractIdAbsInformation:
"isFreshChoiceInContext cn ctx \<Longrightarrow>
  lookupContractIdAbsInformation ctx cid = Some (params, body, innerCtx) \<Longrightarrow>
  isFreshChoiceInContract cn body \<and> isFreshChoiceInContext cn innerCtx"
  by (induction cn ctx rule: isFreshChoiceInContext.induct, auto split: if_split_asm)

lemma freshChoicePreserved_appendParams:
"isFreshChoiceInContext cn ctx \<Longrightarrow>
  isFreshChoiceInContext cn (paramsToFContext params @ ctx)"
  by (induction cn ctx rule: isFreshChoiceInContext.induct; induction params rule: paramsToFContext.induct, auto)

lemma freshChoicePreserved_small_step_reduce_weak:
"(c1, ctx1, state1, env1, warnings1, payments1) \<rightarrow>\<^sub>f\<^sub>2 (c2, ctx2, state2, env2, warnings2, payments2) \<Longrightarrow>
  isFreshChoiceInContract cn c1 \<Longrightarrow>
  isFreshChoiceInContext cn ctx1 \<Longrightarrow>
  isFreshChoiceInState cn state1 \<Longrightarrow>
  (isFreshChoiceInContract cn c2 \<and> isFreshChoiceInContext cn ctx2 \<and> isFreshChoiceInState cn state2)"
proof (induction c1 ctx1 state1 env1 warnings1 payments1 c2 ctx2 state2 env2 warnings2 payments2 rule: small_step_reduce.induct[split_format(complete)])
  case (UseCFound ctx cid params bodyCont innerCtx s env warns payments a a a a a b)
  then show ?case using freshChoicePreserved_appendParams freshChoicePreserved_lookupContractIdAbsInformation by blast
qed auto

lemma freshChoicePreserved_small_step_reduce:
"(c1, ctx1, state1, env1, warnings1, payments1) \<rightarrow>\<^sub>f\<^sub>2 (c2, ctx2, state2, env2, warnings2, payments2) \<Longrightarrow>
  isFreshChoiceInContract cn c1 \<Longrightarrow>
  isFreshChoiceInContext cn ctx1 \<Longrightarrow>
  (isFreshChoiceInContract cn c2 \<and> isFreshChoiceInContext cn ctx2)"
proof (induction c1 ctx1 state1 env1 warnings1 payments1 c2 ctx2 state2 env2 warnings2 payments2 rule: small_step_reduce.induct[split_format(complete)])
  case (UseCFound ctx cid params bodyCont innerCtx env s args newState warns payments)
  then show ?case proof (induction ctx cid rule: lookupContractIdAbsInformation.induct)
    case (2 boundCid innerParams innerBodyCont restAbs cid)
    then show ?case using freshChoicePreserved_appendParams freshChoicePreserved_lookupContractIdAbsInformation by blast
  qed (auto split: if_split_asm)
qed auto

type_synonym FActionConfig = "FAction * FContext * FState * Environment"

type_synonym FActionRes = "(FState * ApplyWarning)"

inductive small_step_reduce_action :: "Input \<Rightarrow> FActionConfig \<Rightarrow> FActionRes \<Rightarrow> bool" ("_ \<turnstile> _ \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 _" [55, 0, 55]) where
DepositPositiveActionStep:  "\<lbrakk>evalFValue env s fValue = Some valRes;
  evalFParty s fAccId = Some fAccIdRes;
  evalFParty s fParty = Some fPartyRes;
  fAccIdRes = accId;
  fPartyRes = mParty;
  fToken = mToken;
  valRes = mMoney;
  moneyInAccount accId mToken (accounts s) = moneyInAcc;
  moneyInAcc + mMoney = newBalance;
  updateMoneyInAccount accId mToken newBalance (accounts s) = newAccs;
  mMoney > 0\<rbrakk> \<Longrightarrow> 
  (IDeposit accId mParty mToken mMoney) \<turnstile> (Deposit fAccId fParty fToken fValue, ctx, s, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 
  (s\<lparr>accounts := newAccs\<rparr>, ApplyNoWarning)" |
DepositNonPositiveActionStep:  "\<lbrakk>evalFValue env s fValue = Some valRes;
  evalFParty s fAccId = Some fAccIdRes;
  evalFParty s fParty = Some fPartyRes;
  fAccIdRes = accId;
  fPartyRes = mParty;
  fToken = mToken;
  valRes = mMoney;
  moneyInAccount accId mToken (accounts s) = moneyInAcc;
  moneyInAcc + mMoney = newBalance;
  updateMoneyInAccount accId mToken newBalance (accounts s) = newAccs;
  mMoney \<le> 0\<rbrakk> \<Longrightarrow> 
  (IDeposit accId mParty mToken mMoney) \<turnstile> (Deposit fAccId fParty fToken fValue, ctx, s, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 
  (s\<lparr>accounts := newAccs\<rparr>, ApplyNonPositiveDeposit mParty accId mToken mMoney)" |
ChoiceActionStep:  "\<lbrakk>inBounds choice bounds;
  evalFChoiceId s fChoiceId = Some fChoiceIdRes;
  fChoiceIdRes = mChoiceId\<rbrakk> \<Longrightarrow> 
  (IChoice mChoiceId choice) \<turnstile> (Choice fChoiceId bounds, ctx, s, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 
  (s\<lparr> choices := MList.insert mChoiceId choice (choices s) \<rparr>, ApplyNoWarning)" |
NotifyActionStep:  "\<lbrakk>evalFObservation env s obs = Some True\<rbrakk> \<Longrightarrow> 
  INotify \<turnstile> (Notify obs, ctx, s, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (s, ApplyNoWarning)"

inductive_cases DepositActionE[elim]: "ins \<turnstile> (Deposit fAccId fParty fToken fValue, ctx, s, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 aRes"
inductive_cases ChoiceActionE[elim]: "ins \<turnstile> (Choice fChoiceId bounds, ctx, s, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 aRes"
inductive_cases NotifyActionE[elim]: "ins \<turnstile> (Notify obs, ctx, s, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 aRes"

lemma deterministicActionSemantics:
"ins \<turnstile> a \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 a' \<Longrightarrow>
ins \<turnstile> a \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 a'' \<Longrightarrow>
a' = a''"
  by (cases ins a a' rule: small_step_reduce_action.cases;
      cases ins a a'' rule: small_step_reduce_action.cases;
      auto split: option.split_asm)

fun computeActionStep :: "Input \<Rightarrow> FActionConfig \<Rightarrow> FActionRes option" where
"computeActionStep (IDeposit accId mParty mToken mMoney) (Deposit fAccId fParty fToken fValue, ctx, s, env) = (
  case (evalFValue env s fValue, evalFParty s fAccId, evalFParty s fParty) of
    (Some valRes, Some fAccIdRes, Some fPartyRes) \<Rightarrow> (if (fAccIdRes = accId \<and> fPartyRes = mParty \<and> fToken = mToken \<and> valRes = mMoney)
      then (let moneyInAcc = moneyInAccount accId mToken (accounts s) in
        let newBalance = moneyInAcc + mMoney in
        let newAccs = updateMoneyInAccount accId mToken newBalance (accounts s) in
        Some (s\<lparr>accounts := newAccs\<rparr>, if mMoney > 0 then ApplyNoWarning else ApplyNonPositiveDeposit mParty accId mToken mMoney))
      else None)
    | _ \<Rightarrow> None)" |
"computeActionStep (IChoice mChoiceId choice) (Choice fChoiceId bounds, ctx, s, env) = (
  if (inBounds choice bounds \<and> evalFChoiceId s fChoiceId = Some mChoiceId) 
  then Some (s\<lparr> choices := MList.insert mChoiceId choice (choices s) \<rparr>, ApplyNoWarning)
  else None)" |
"computeActionStep (INotify) (Notify obs, ctx, s, env) = (
  if evalFObservation env s obs = Some True
  then Some (s, ApplyNoWarning)
  else None)" |
"computeActionStep INotify _ = None" |
"computeActionStep _ _ = None"

lemma computeActionStepIsSmallStep:
"computeActionStep ins a = Some res \<longleftrightarrow> ins \<turnstile> a \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 res"
  apply auto
  apply (cases "(ins, a)" rule: computeActionStep.cases)
  using DepositPositiveActionStep DepositNonPositiveActionStep NotifyActionStep ChoiceActionStep apply (auto split: option.split_asm if_split_asm simp del: inBounds.simps moneyInAccount.simps updateMoneyInAccount.simps)
  by (cases ins a res rule: small_step_reduce_action.cases, auto)

lemma reduceActionInvariants:
"ins \<turnstile> (a, ctx, s, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (newS, newWarning) \<Longrightarrow>
  (\<forall>ctx' .
    \<exists> newCtx' .
    ins \<turnstile> (a, ctx', s, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (newS, newWarning))"
  apply (induction ins a ctx s env newS newWarning rule: small_step_reduce_action.induct[split_format(complete)])
  by (auto simp add: small_step_reduce_action.simps)

lemma freshChoicePreserved_insert:
"isFreshChoiceInChoices cn cs \<Longrightarrow>
  cn \<noteq> newCn \<Longrightarrow>
  isFreshChoiceInChoices cn (MList.insert (ChoiceId newCn p) choice cs)"
proof (induction "(ChoiceId newCn p)" choice cs arbitrary: cn newCn p rule: MList.insert.induct)
  case (2 b x y z)
  then show ?case by (cases x, auto)
qed auto

lemma freshChoicePreserved_action_step:
"ins \<turnstile> (a, ctx, s, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (newS, newWarning) \<Longrightarrow>
  isFreshChoiceInAction cn a \<Longrightarrow>
  isFreshChoiceInContext cn ctx \<Longrightarrow>
  isFreshChoiceInState cn s \<Longrightarrow>
  (isFreshChoiceInState cn newS)"
proof (induction ins a ctx s env newS newWarning rule: small_step_reduce_action.induct[split_format(complete)])
  case (ChoiceActionStep choice bounds s fChoiceId fChoiceIdRes mChoiceId ctx env)
  then show ?case by (cases fChoiceId, auto split: option.split_asm simp add: freshChoicePreserved_insert)
qed auto

lemma freshChoiceNoMatch_action_step:
"isFreshChoiceInAction cn a \<Longrightarrow> \<not> (IChoice (ChoiceId cn p) i) \<turnstile> (a, ctx, s, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (newS, newWarning)"
  apply (induction cn a rule: isFreshChoiceInAction.induct, auto)
  by (elim ChoiceActionE, auto split: option.split_asm)

lemma freshChoiceMatchNoChoiceInput_action_step:
"isFreshChoiceInAction cn a \<Longrightarrow> inp \<turnstile> (a, ctx, s, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (newS, newWarning) \<Longrightarrow> (\<nexists>p i . inp = (IChoice (ChoiceId cn p) i))"
  using freshChoiceNoMatch_action_step by auto

fun bound_intersect :: "Bound \<Rightarrow> Bound \<Rightarrow> Bound" where
"bound_intersect (l1, h1) (l2, h2) = (max l1 l2, min h1 h2)"

fun left_prioritize_bound_single :: "Bound \<Rightarrow> Bound \<Rightarrow> Bound list" where
"left_prioritize_bound_single (l1, h1) (l2, h2) = [(l2, min (l1-1) h2), (max l2 (h1+1), h2)]"
(*(1,3) (2,4) = [(2,1), (4,4)]*)
(* (2,4) (1,3) = [(1, 1), (5, 3)] *)
(* (2,3) (1, 5) = [(1,1), (4, 5)]*)

lemma left_prioritize_bound_single_correctness_disjoint:
"left_prioritize_bound_single b1 b2 = bs \<Longrightarrow>
  (\<forall>n . (inBounds n [b1] \<longrightarrow> \<not>inBounds n bs) \<and>
  (inBounds n bs \<longrightarrow> \<not>inBounds n [b1]) \<and>
  (inBounds n [b1] \<and> inBounds n [b2] \<longrightarrow> \<not>inBounds n bs))"
  apply (induction b1 b2 arbitrary: bs rule: left_prioritize_bound_single.induct)
  by auto

lemma left_prioritize_bound_single_correctness_preservation:
"left_prioritize_bound_single b1 b2 = bs \<Longrightarrow>
  (\<forall>n . inBounds n bs \<longrightarrow> inBounds n [b2])"
  by (cases b1, auto)

fun left_prioritize_bound :: "Bound \<Rightarrow> Bound list \<Rightarrow> Bound list" where
"left_prioritize_bound b [] = []" |
"left_prioritize_bound b1 (b2#bs) = ((left_prioritize_bound_single b1 b2) @ (left_prioritize_bound b1 bs))"

lemma left_prioritize_bound_correctness_disjoint:
"left_prioritize_bound b1 bs2 = bs \<Longrightarrow>
  (\<forall>n . (inBounds n [b1] \<longrightarrow> \<not>inBounds n bs) \<and>
  (inBounds n bs \<longrightarrow> \<not>inBounds n [b1]) \<and>
  (inBounds n bs2 \<and> inBounds n [b1] \<longrightarrow> \<not>inBounds n bs))"
proof (induction b1 bs2 arbitrary: bs rule: left_prioritize_bound.induct)
  case (1 b)
  then show ?case by auto
next
  case (2 b1 b2 bsRest)
  moreover obtain bsRestRes where "left_prioritize_bound b1 bsRest = bsRestRes" using 2(2) by simp
  moreover obtain b2Res1 b2Res2 where "left_prioritize_bound_single b1 b2 = [b2Res1, b2Res2]" by (cases b1; cases b2, auto)
  moreover have "bs = (b2Res1#b2Res2#bsRestRes)" using calculation by auto
  moreover have "\<forall>n arbRest . inBounds n [b2Res1,b2Res2] \<longrightarrow> inBounds n (b2Res1#b2Res2#arbRest)" by auto
  moreover have "\<forall>n arbRest . \<not>inBounds n [b2Res1,b2Res2] \<longrightarrow> inBounds n (b2Res1#b2Res2#arbRest) = inBounds n arbRest" by auto
  ultimately show ?case by (metis left_prioritize_bound_single_correctness_disjoint)+
qed

lemma left_prioritize_bound_correctness_preservation:
"left_prioritize_bound b1 bs2 = bs \<Longrightarrow>
  (\<forall>n . inBounds n bs \<longrightarrow> inBounds n bs2)"
  apply (induction b1 bs2 arbitrary: bs rule: left_prioritize_bound.induct)
  by (cases b1, auto)

fun left_prioritize_bounds :: "Bound list \<Rightarrow> Bound list \<Rightarrow> Bound list" where
"left_prioritize_bounds [] bounds2 = bounds2" |
"left_prioritize_bounds (b#bs) bounds2 = left_prioritize_bounds bs (left_prioritize_bound b bounds2)"

lemma left_prioritize_bounds_correctness_disjoint:
"left_prioritize_bounds bs1 bs2 = bs \<Longrightarrow>
  (\<forall>n . (inBounds n bs1 \<longrightarrow> \<not>inBounds n bs) \<and>
  (inBounds n bs \<longrightarrow> \<not>inBounds n bs1) \<and>
  (inBounds n bs2 \<and> inBounds n bs1 \<longrightarrow> \<not>inBounds n bs))"
proof (induction bs1 bs2 arbitrary: bs rule: left_prioritize_bounds.induct)
  case (1 bounds2)
  then show ?case by auto
next
  case (2 b1 b1Rest bounds2)
  moreover have "bs = left_prioritize_bounds b1Rest (left_prioritize_bound b1 bounds2)" using 2(2) by simp
  moreover have "\<forall>n . inBounds n (left_prioritize_bound b1 bounds2) \<longrightarrow> \<not>inBounds n [b1]" using left_prioritize_bound_correctness_disjoint by blast
  moreover have "\<forall>n arbRest . inBounds n [b1] \<longrightarrow> inBounds n (b1#arbRest)" by auto
  moreover have "\<forall>n arbRest . \<not>inBounds n [b1] \<longrightarrow> inBounds n (b1#arbRest) = inBounds n arbRest" by auto
  moreover have "\<forall>n arbFirst . inBounds n b1Rest \<longrightarrow> inBounds n (arbFirst#b1Rest)" by auto
  moreover have "\<forall>n arbFirst . \<not>inBounds n b1Rest \<longrightarrow> inBounds n (arbFirst#b1Rest) = inBounds n [arbFirst]" by auto
  moreover have "inBounds n (b1 # b1Rest) \<longrightarrow> \<not> inBounds n bs" apply (cases "inBounds n [b1]")
    using calculation(4,5)
    sorry
  ultimately show ?case
    sorry
qed

fun left_prioritize_action :: "FState \<Rightarrow> FAction \<Rightarrow> FAction \<Rightarrow> FAction" where
"left_prioritize_action \<sigma> (Deposit p11 p12 t1 v1) (Deposit p21 p22 t2 v2) = (if (evalFParty \<sigma> p11 = evalFParty \<sigma> p21) \<and> (evalFParty \<sigma> p12 = evalFParty \<sigma> p22) \<and> t1 = t2
  then Deposit p21 p22 t2 (Cond (ValueEQ v1 v2) (Constant (-1000)) v2)
  else Deposit p21 p22 t2 v2)" |
"left_prioritize_action \<sigma> (Notify obs1) (Notify obs2) = (Notify (AndObs (NotObs obs1) (obs2)))" |
"left_prioritize_action \<sigma> (Choice (FChoiceId cn1 p1) bounds1) (Choice (FChoiceId cn2 p2) bounds2) = (if (evalFParty \<sigma> p1 = evalFParty \<sigma> p2) \<and> cn1 = cn2
  then Choice (FChoiceId cn2 p2) (left_prioritize_bounds bounds1 bounds2)
  else Choice (FChoiceId cn2 p2) bounds2)"

type_synonym FGuardConfig = "FGuardExpression * FContext * FState * Environment * TransactionWarning list * Payment list"
type_synonym FGuardConfigRes = "((FStatement list) * FGuardExpression option) * FContext * FState * Environment * TransactionWarning list * Payment list"

fun computeGuardStep :: "Input \<Rightarrow> FGuardConfig \<Rightarrow> FGuardConfigRes option" where
"computeGuardStep ins (ActionGuard a, ctx, s, env, warnings, payments) = (case computeActionStep ins (a, ctx, s, env) of
  Some (newS, applyWarning) \<Rightarrow> Some (([], None), ctx, newS, env, warnings @ convertApplyWarning applyWarning, payments)
  | None \<Rightarrow> None)" |
"computeGuardStep ins (GuardThenGuard g1 g2, ctx, s, env, warnings, payments) = (case computeGuardStep ins (g1, ctx, s, env, warnings, payments) of
  Some ((stmts, None), newCtx, newS, newEnv, newWarnings) \<Rightarrow> Some ((stmts, Some g2), newCtx, newS, newEnv, newWarnings)
  | Some ((stmts, Some innerG1), newCtx, newS, newEnv, newWarnings) \<Rightarrow> Some ((stmts, Some (GuardThenGuard innerG1 g2)), newCtx, newS, newEnv, newWarnings)
  | None \<Rightarrow> None)" |
"computeGuardStep ins (GuardStmtsGuard g1 stmts, ctx, s, env, warnings, payments) = (case computeGuardStep ins (g1, ctx, s, env, warnings, payments) of
  Some ((newStmts, None), newCtx, newS, newEnv, newWarnings) \<Rightarrow> Some ((newStmts @ stmts, None), newCtx, newS, newEnv, newWarnings)
  | Some ((newStmts, Some innerG1), newCtx, newS, newEnv, newWarnings) \<Rightarrow> Some ((newStmts, Some (GuardStmtsGuard innerG1 stmts)), newCtx, newS, newEnv, newWarnings)
  | None \<Rightarrow> None)" |
"computeGuardStep ins (DisjointGuard g1 g2, ctx, s, env, warnings, payments) = (case computeGuardStep ins (g1, ctx, s, env, warnings, payments) of
  Some res \<Rightarrow> Some res
  | None \<Rightarrow> (case computeGuardStep ins (g2, ctx, s, env, warnings, payments) of
    Some res \<Rightarrow> Some res
    | None \<Rightarrow> None))" |
"computeGuardStep ins (InterleavedGuard g1 g2, ctx, s, env, warnings, payments) = (case computeGuardStep ins (g1, ctx, s, env, warnings, payments) of
  Some ((stmts, None), newCtx, newS, newEnv, newWarnings) \<Rightarrow> Some ((stmts, Some g2), newCtx, newS, newEnv, newWarnings)
  | Some ((stmts, Some innerG1), newCtx, newS, newEnv, newWarnings) \<Rightarrow> Some ((stmts, Some (InterleavedGuard innerG1 g2)), newCtx, newS, newEnv, newWarnings)
  | None \<Rightarrow> (case computeGuardStep ins (g2, ctx, s, env, warnings, payments) of
    Some ((stmts, None), newCtx, newS, newEnv, newWarnings) \<Rightarrow> Some ((stmts, Some g1), newCtx, newS, newEnv, newWarnings)
    | Some ((stmts, Some innerG2), newCtx, newS, newEnv, newWarnings) \<Rightarrow> Some ((stmts, Some (InterleavedGuard g1 innerG2)), newCtx, newS, newEnv, newWarnings)
    | None \<Rightarrow> None))"

inductive small_step_reduce_guard :: "Input \<Rightarrow> FGuardConfig \<Rightarrow> FGuardConfigRes \<Rightarrow> bool" ("_ \<turnstile> _ \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 _" [55, 0, 55]) where
ActionGuardStep:  "\<lbrakk>g = ActionGuard a;
  ins \<turnstile> (a, ctx, s, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (newS, applyWarning)\<rbrakk> \<Longrightarrow> 
  ins \<turnstile> (g, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 (([], None), ctx, newS, env, warnings @ convertApplyWarning applyWarning, payments)" |
GuardThenGuardCompleteFirstGuardStep:  "\<lbrakk>g = GuardThenGuard g1 g2;
  ins \<turnstile> (g1, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, None), newCtx, newS, newEnv, newWarnings, newPayments)\<rbrakk> \<Longrightarrow> 
  ins \<turnstile> (g, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some g2), newCtx, newS, newEnv, newWarnings, newPayments)" |
GuardThenGuardIncompleteGuard:  "\<lbrakk>g = GuardThenGuard g1 g2;
  ins \<turnstile> (g1, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some nextG1), newCtx, newS, newEnv, newWarnings, newPayments)\<rbrakk> \<Longrightarrow> 
  ins \<turnstile> (g, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some (GuardThenGuard nextG1 g2)), newCtx, newS, newEnv, newWarnings, newPayments)" |
GuardStmtsGuardCompleteGuardStep: "\<lbrakk>g = GuardStmtsGuard g1 stmts;
  ins \<turnstile> (g1, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((newStmts, None), newCtx, newS, newEnv, newWarnings, newPayments)\<rbrakk> \<Longrightarrow> 
  ins \<turnstile> (g, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((newStmts @ stmts, None), newCtx, newS, newEnv, newWarnings, newPayments)" |
GuardStmtsGuardIncompleteGuard: "\<lbrakk>g = GuardStmtsGuard g1 stmts;
  ins \<turnstile> (g1, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((newStmts, Some nextG1), newCtx, newS, newEnv, newWarnings, newPayments)\<rbrakk> \<Longrightarrow> 
  ins \<turnstile> (g, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((newStmts, Some (GuardStmtsGuard nextG1 stmts)), newCtx, newS, newEnv, newWarnings, newPayments)" |
DisjointGuardMatchesG1:  "\<lbrakk>(ins \<turnstile> (g1, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 g1Res)\<rbrakk> \<Longrightarrow> 
  ins \<turnstile> (DisjointGuard g1 g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 g1Res" |
DisjointGuardMatchesG2:  "\<lbrakk>computeGuardStep ins (g1, ctx, s, env, warnings, payments) = None;
  (ins \<turnstile> (g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 g2Res)\<rbrakk> \<Longrightarrow> 
  ins \<turnstile> (DisjointGuard g1 g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 g2Res" |
InterleavedGuardMatchesG1Complete:  "\<lbrakk>(ins \<turnstile> (g1, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, None), newCtx, newS, newEnv, newWarnings, newPayments))\<rbrakk> \<Longrightarrow> 
  ins \<turnstile> (InterleavedGuard g1 g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some g2), newCtx, newS, newEnv, newWarnings, newPayments)" |
InterleavedGuardMatchesG1Incomplete:  "\<lbrakk>(ins \<turnstile> (g1, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some innerG1), newCtx, newS, newEnv, newWarnings, newPayments))\<rbrakk> \<Longrightarrow> 
  ins \<turnstile> (InterleavedGuard g1 g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some (InterleavedGuard innerG1 g2)), newCtx, newS, newEnv, newWarnings, newPayments)" |
InterleavedGuardMatchesG2Complete: "\<lbrakk>(ins \<turnstile> (g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, None), newCtx, newS, newEnv, newWarnings, newPayments));
  computeGuardStep ins (g1, ctx, s, env, warnings, payments) = None\<rbrakk> \<Longrightarrow> 
  ins \<turnstile> (InterleavedGuard g1 g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some g1), newCtx, newS, newEnv, newWarnings, newPayments)" |
InterleavedGuardMatchesG2Incomplete:  "\<lbrakk>(ins \<turnstile> (g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some innerG2), newCtx, newS, newEnv, newWarnings, newPayments));
  computeGuardStep ins (g1, ctx, s, env, warnings, payments) = None\<rbrakk> \<Longrightarrow> 
  ins \<turnstile> (InterleavedGuard g1 g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some (InterleavedGuard g1 innerG2)), newCtx, newS, newEnv, newWarnings, newPayments)"

inductive_cases ActionGuardE[elim]: "ins \<turnstile> (ActionGuard a, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 gRes"
inductive_cases GuardThenGuardE[elim!]: "ins \<turnstile> (GuardThenGuard g1 g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 gRes"
inductive_cases GuardStmtsGuardE[elim]: "ins \<turnstile> (GuardStmtsGuard g1 stmts, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 gRes"
inductive_cases DisjointGuardE_compute: "ins \<turnstile> (DisjointGuard g1 g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 gRes"
inductive_cases InterleavedGuardE_compute: "ins \<turnstile> (InterleavedGuard g1 g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 gRes"

definition "finalGuard ins gConfig \<longleftrightarrow> (\<forall>res . \<not>(ins \<turnstile> gConfig \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 res))"

lemma computeGuardStepIsSmallStep:
"computeGuardStep ins (g, ctx, s, env, warnings, payments) = Some res \<Longrightarrow>
  ins \<turnstile> (g, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 res"
  apply (induction arbitrary: res rule: computeGuardStep.induct[split_format(complete)])
  using ActionGuardStep apply (auto split: option.split_asm simp add: computeActionStepIsSmallStep)[1]
  using GuardThenGuardIncompleteGuard GuardThenGuardCompleteFirstGuardStep apply (auto split: option.split_asm)[1]
  using GuardStmtsGuardCompleteGuardStep GuardStmtsGuardIncompleteGuard apply (auto split: option.split_asm)[1]
  using DisjointGuardMatchesG1 DisjointGuardMatchesG2 apply (auto split: option.split_asm)[1]
  using InterleavedGuardMatchesG1Complete
    InterleavedGuardMatchesG1Incomplete
    InterleavedGuardMatchesG2Complete
    InterleavedGuardMatchesG2Incomplete
  by (auto split: option.split_asm)

lemma smallStepGuardIsComputed:
"ins \<turnstile> (g, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 res \<Longrightarrow>
  computeGuardStep ins (g, ctx, s, env, warnings, payments) = Some res"
proof (induction ins "(g, ctx, s, env, warnings, payments)" res arbitrary: g rule: small_step_reduce_guard.induct)
  case (ActionGuardStep g a ins newS applyWarning)
  then show ?case apply auto
    apply (cases "computeActionStep ins (a, ctx, s, env)", auto)
      apply (simp_all add: computeActionStepIsSmallStep option.discI)
    using deterministicActionSemantics by force+
qed auto

lemma computeGuardStepHaltedImpliesSmallStepHalted:
"computeGuardStep ins (g, ctx, s, env, warnings, payments) = None \<longleftrightarrow>
  finalGuard ins (g, ctx, s, env, warnings, payments)"
  by (metis finalGuard_def computeGuardStepIsSmallStep smallStepGuardIsComputed not_Some_eq)

lemma DisjointGuardMatchesG2_no_computation:
"\<lbrakk>finalGuard ins (g1, ctx, s, env, warnings, payments);
  (ins \<turnstile> (g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 g2Res)\<rbrakk> \<Longrightarrow> 
  ins \<turnstile> (DisjointGuard g1 g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 g2Res"
  by (simp add: computeGuardStepHaltedImpliesSmallStepHalted DisjointGuardMatchesG2)

lemma InterleavedGuardMatchesG2Complete_no_computation:
"\<lbrakk>(ins \<turnstile> (g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, None), newCtx, newS, newEnv, newWarnings, newPayments));
  finalGuard ins (g1, ctx, s, env, warnings, payments)\<rbrakk> \<Longrightarrow> 
  ins \<turnstile> (InterleavedGuard g1 g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some g1), newCtx, newS, newEnv, newWarnings, newPayments)"
  by (simp add: computeGuardStepHaltedImpliesSmallStepHalted InterleavedGuardMatchesG2Complete)

lemma InterleavedGuardMatchesG2Incomplete_no_computation:
"\<lbrakk>(ins \<turnstile> (g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some innerG2), newCtx, newS, newEnv, newWarnings, newPayments));
  finalGuard ins (g1, ctx, s, env, warnings, payments)\<rbrakk> \<Longrightarrow> 
  ins \<turnstile> (InterleavedGuard g1 g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some (InterleavedGuard g1 innerG2)), newCtx, newS, newEnv, newWarnings, newPayments)"
  by (simp add: computeGuardStepHaltedImpliesSmallStepHalted InterleavedGuardMatchesG2Incomplete)

lemma DisjointGuardE[elim!]:
"ins \<turnstile> (DisjointGuard g1 g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 gRes \<Longrightarrow>
(ins \<turnstile> (g1, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 gRes \<Longrightarrow> P) \<Longrightarrow>
(finalGuard ins (g1, ctx, s, env, warnings, payments) \<Longrightarrow>
 ins \<turnstile> (g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 gRes \<Longrightarrow> P) \<Longrightarrow>
P"
  by (meson DisjointGuardE_compute computeGuardStepHaltedImpliesSmallStepHalted)

thm InterleavedGuardE_compute

lemma InterleavedGuardE [elim!]:
"ins \<turnstile> (InterleavedGuard g1 g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 gRes \<Longrightarrow>
(\<forall>stmts newCtx newS newEnv newWarnings newPayments.
    gRes = ((stmts, Some g2), newCtx, newS, newEnv, newWarnings, newPayments) \<longrightarrow>
    ins \<turnstile> (g1, ctx, s, env, warnings,
             payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, None), newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
    P) \<Longrightarrow>
(\<forall>stmts innerG1 newCtx newS newEnv newWarnings newPayments.
    gRes = ((stmts, Some (InterleavedGuard innerG1 g2)), newCtx, newS, newEnv, newWarnings, newPayments) \<longrightarrow>
    ins \<turnstile> (g1, ctx, s, env, warnings,
             payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some innerG1), newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
    P) \<Longrightarrow>
(\<forall>stmts newCtx newS newEnv newWarnings newPayments.
    gRes = ((stmts, Some g1), newCtx, newS, newEnv, newWarnings, newPayments) \<longrightarrow>
    ins \<turnstile> (g2, ctx, s, env, warnings,
             payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, None), newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
    finalGuard ins (g1, ctx, s, env, warnings, payments) \<Longrightarrow> P) \<Longrightarrow>
(\<forall>stmts innerG2 newCtx newS newEnv newWarnings newPayments.
    gRes = ((stmts, Some (InterleavedGuard g1 innerG2)), newCtx, newS, newEnv, newWarnings, newPayments) \<longrightarrow>
    ins \<turnstile> (g2, ctx, s, env, warnings,
             payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some innerG2), newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
    finalGuard ins (g1, ctx, s, env, warnings, payments) \<Longrightarrow> P) \<Longrightarrow>
P"
  using InterleavedGuardE_compute computeGuardStepHaltedImpliesSmallStepHalted by fastforce

lemma deterministicGuardExpressionsSemantics:
"ins \<turnstile> g \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 g' \<Longrightarrow>
ins \<turnstile> g \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 g'' \<Longrightarrow>
g' = g''"
  by (metis old.prod.exhaust option.inject smallStepGuardIsComputed)

lemma reduceGuardExpressionsConstants:
"ins \<turnstile> (g, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, nextG), newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
  ctx = newCtx \<and> payments = newPayments \<and> env = newEnv"
  apply (induction ins g ctx s env warnings payments stmts nextG newCtx newS newEnv newWarnings newPayments rule: small_step_reduce_guard.induct[split_format(complete)])
  by auto

lemma computeGuardStepInvariantsHalted:
"computeGuardStep ins (g, ctx, s, env, warnings, payments) = None \<Longrightarrow>
  (\<forall>ctx' warnings' payments' . computeGuardStep ins (g, ctx', s, env, warnings', payments') = None)"
proof (induction "ins" "(g, ctx, s, env, warnings, payments)" arbitrary: g rule: computeGuardStep.induct)
  case (1 ins a)
  then show ?case by (cases a; cases ins, auto split: option.split_asm)
next
  case (2 ins g1 g2)
  then show ?case by (auto split: option.split_asm option.split)
next
  case (3 ins g1 stmts)
  then show ?case by (auto split: option.split_asm option.split)
next
  case (4 ins g1 g2)
  then show ?case by (auto split: option.split_asm option.split)
next
  case (5 ins g1 g2)
  then show ?case by (auto split: option.split_asm option.split)
qed

lemma reduceGuardExpressionsInvaraintsHalted:
"finalGuard ins (g, ctx, s, env, warnings, payments) \<Longrightarrow>
  (\<forall>ctx' warnings' payments' . finalGuard ins (g, ctx', s, env, warnings', payments'))"
  by (meson computeGuardStepHaltedImpliesSmallStepHalted computeGuardStepInvariantsHalted)

lemma reduceGuardExpressionsInvariants:
"ins \<turnstile> (g, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, nextG), newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
  (\<forall>ctx' warnings' payments' .
    \<exists> newCtx' newWarnings' newPayments' .
    ins \<turnstile> (g, ctx', s, env, warnings', payments') \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, nextG), newCtx', newS, newEnv, newWarnings', newPayments') \<and>
    (warnings' = warnings \<longrightarrow> newWarnings' = newWarnings) \<and>
    (payments' = payments \<longrightarrow> newPayments' = newPayments) \<and>
    (ctx' = ctx \<longrightarrow> newCtx' = newCtx))"
proof (induction ins g ctx s env warnings payments stmts nextG newCtx newS newEnv newWarnings newPayments rule: small_step_reduce_guard.induct[split_format(complete)])
  case (ActionGuardStep g a ins ctx s env newS applyWarning warnings payments)
  then show ?case apply auto
    by (metis ActionGuardStep.hyps(2) reduceActionInvariants small_step_reduce_guard.ActionGuardStep)
qed (auto; meson computeGuardStepInvariantsHalted
    small_step_reduce_guard.GuardThenGuardCompleteFirstGuardStep
    small_step_reduce_guard.GuardThenGuardIncompleteGuard
    small_step_reduce_guard.GuardStmtsGuardCompleteGuardStep
    small_step_reduce_guard.GuardStmtsGuardIncompleteGuard
    small_step_reduce_guard.DisjointGuardMatchesG1
    small_step_reduce_guard.DisjointGuardMatchesG2
    small_step_reduce_guard.InterleavedGuardMatchesG1Complete
    small_step_reduce_guard.InterleavedGuardMatchesG1Incomplete
    small_step_reduce_guard.InterleavedGuardMatchesG2Complete
    small_step_reduce_guard.InterleavedGuardMatchesG2Incomplete)+

lemma freshChoicePreserved_statements_append:
"isFreshChoiceInStatements cn stmts1 \<Longrightarrow>
  isFreshChoiceInStatements cn stmts2 \<Longrightarrow>
  isFreshChoiceInStatements cn (stmts1 @ stmts2)"
  by (induction stmts1, auto)

lemma freshChoicePreserved_guard_step_weak:
"ins \<turnstile> (g, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, nextG), newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
  isFreshChoiceInGuard cn g \<Longrightarrow>
  isFreshChoiceInContext cn ctx \<Longrightarrow>
  isFreshChoiceInState cn s \<Longrightarrow>
  (isFreshChoiceInStatements cn stmts \<and> isFreshChoiceInContext cn newCtx \<and> isFreshChoiceInState cn newS \<and> (nextG = Some newG \<longrightarrow> isFreshChoiceInGuard cn newG))"
proof (induction ins g ctx s env warnings payments stmts nextG newCtx newS newEnv newWarnings newPayments arbitrary: newG rule: small_step_reduce_guard.induct[split_format(complete)])
  case (ActionGuardStep g a ins ctx s env newS applyWarning warnings payments)
  then show ?case using freshChoicePreserved_action_step by auto
next
  case (GuardStmtsGuardCompleteGuardStep g innerG stmts ins ctx s env warnings payments newStmts newCtx newS newEnv newWarnings newPayments)
  then show ?case by (auto simp add: freshChoicePreserved_statements_append)
qed auto

lemma freshChoicePreserved_guard_step:
"ins \<turnstile> (g, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, nextG), newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
  isFreshChoiceInGuard cn g \<Longrightarrow>
  isFreshChoiceInContext cn ctx \<Longrightarrow>
  (isFreshChoiceInStatements cn stmts \<and> isFreshChoiceInContext cn newCtx \<and> (nextG = Some newG \<longrightarrow> isFreshChoiceInGuard cn newG))"
proof (induction ins g ctx s env warnings payments stmts nextG newCtx newS newEnv newWarnings newPayments arbitrary: newG rule: small_step_reduce_guard.induct[split_format(complete)])
  case (ActionGuardStep g a ins ctx s env newS applyWarning warnings payments)
  then show ?case using freshChoicePreserved_action_step by auto
next
  case (GuardStmtsGuardCompleteGuardStep g innerG stmts ins ctx s env warnings payments newStmts newCtx newS newEnv newWarnings newPayments)
  then show ?case by (auto simp add: freshChoicePreserved_statements_append)
qed auto

lemma freshChoiceNoMatch_guard_step:
"isFreshChoiceInGuard cn g \<Longrightarrow> finalGuard (IChoice (ChoiceId cn p) i) (g, ctx, s, env, warnings, payments)"
  apply (induction cn g rule: isFreshChoiceInGuard.induct, auto)
      apply (meson ActionGuardE finalGuard_def freshChoiceNoMatch_action_step)
  using finalGuard_def apply force[1]
  using finalGuard_def apply auto[1]
   apply (meson DisjointGuardE finalGuard_def)
  by (metis (no_types, lifting) computeGuardStep.simps(5) computeGuardStepHaltedImpliesSmallStepHalted option.case_eq_if)

lemma freshChoiceMatchNoChoiceInput_guard_step:
"isFreshChoiceInGuard cn g \<Longrightarrow>
  inp \<turnstile> (g, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, nextG), newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
  (\<nexists>p i . inp = IChoice (ChoiceId cn p) i)"
  using finalGuard_def freshChoiceNoMatch_guard_step by blast

fun prependStatements :: "FStatement list \<Rightarrow> FContract \<Rightarrow> FContract" where
"prependStatements [] cont = cont" |
"prependStatements (stmt#stmts) cont = (StatementCont stmt (prependStatements stmts cont))"

fun linearSize :: "LinearGuardExpression \<Rightarrow> nat" where
"linearSize (LinearGuardNode a) = 1" |
"linearSize (LinearGuardThenGuard firstNode gCont) = 1 + linearSize gCont" |
"linearSize (LinearDisjointGuard g1 g2) = 1 + linearSize g1 + linearSize g2"

fun wellTypedLinearGuardNode :: "TypeContext \<Rightarrow> LinearGuardNode \<Rightarrow> bool" where
"wellTypedLinearGuardNode tyCtx (a, stmts) = (wellTypedAction tyCtx a \<and> wellTypedStatements tyCtx stmts)"

fun wellTypedLinearGuardExpression :: "TypeContext \<Rightarrow> LinearGuardExpression \<Rightarrow> bool" where
"wellTypedLinearGuardExpression tyCtx (LinearGuardNode node) = wellTypedLinearGuardNode tyCtx node" |
"wellTypedLinearGuardExpression tyCtx (LinearGuardThenGuard node g2) = (wellTypedLinearGuardNode tyCtx node \<and> wellTypedLinearGuardExpression tyCtx g2)" |
"wellTypedLinearGuardExpression tyCtx (LinearDisjointGuard g1 g2) = (wellTypedLinearGuardExpression tyCtx g1 \<and> wellTypedLinearGuardExpression tyCtx g2)"

function interleaveLinearTrees :: "LinearGuardExpression \<Rightarrow> LinearGuardExpression \<Rightarrow> bool \<Rightarrow> LinearGuardExpression" and
  interleaveLinearTreesRightFirst :: "LinearGuardExpression \<Rightarrow> LinearGuardExpression \<Rightarrow> LinearGuardExpression" where
"interleaveLinearTrees (LinearGuardNode node) g2 f = (let firstGuard = LinearGuardThenGuard node g2 in
  if f then LinearDisjointGuard firstGuard (interleaveLinearTreesRightFirst g2 (LinearGuardNode node)) else firstGuard)" |
"interleaveLinearTrees (LinearGuardThenGuard node gCont) g2 f = (let firstGuard = LinearGuardThenGuard node (interleaveLinearTrees gCont g2 True) in
  if f then (LinearDisjointGuard firstGuard (interleaveLinearTreesRightFirst g2 (LinearGuardThenGuard node gCont))) else firstGuard)" |
"interleaveLinearTrees (LinearDisjointGuard innerG1 innerG2) g2 f = (let firstGuard = interleaveLinearTrees innerG1 g2 False in
  let secondGuard = interleaveLinearTrees innerG2 g2 False in
  LinearDisjointGuard firstGuard (if f then (LinearDisjointGuard secondGuard (interleaveLinearTreesRightFirst g2 (LinearDisjointGuard innerG1 innerG2))) else secondGuard))" |
"interleaveLinearTreesRightFirst (LinearGuardNode node) g2 = (LinearGuardThenGuard node g2)" |
"interleaveLinearTreesRightFirst (LinearGuardThenGuard node gCont) g2 = (LinearGuardThenGuard node (interleaveLinearTrees g2 gCont True))" |
"interleaveLinearTreesRightFirst (LinearDisjointGuard innerG1 innerG2) g2 = (let firstGuard = interleaveLinearTreesRightFirst innerG1 g2 in
  let secondGuard = interleaveLinearTreesRightFirst innerG2 g2 in
  LinearDisjointGuard firstGuard secondGuard)"
  apply pat_completeness
  by fastforce+

termination
  apply (relation "measures [\<lambda>x. case x of Inl (ge1, ge2, f) \<Rightarrow> linearSize ge1 + linearSize ge2 | Inr (ge1, ge2) \<Rightarrow>  linearSize ge1 + linearSize ge2, \<lambda>x. case x of Inl (ge1, ge2, f) \<Rightarrow> if f then 1 else 0 | Inr (ge1, ge2) \<Rightarrow> 0]")
  by auto

fun addStatementsContinuation :: "FStatement list \<Rightarrow> LinearGuardExpression \<Rightarrow> LinearGuardExpression" where
"addStatementsContinuation stmts (LinearGuardNode (a, innerStmts)) = (LinearGuardNode (a, innerStmts @ stmts))" |
"addStatementsContinuation stmts (LinearGuardThenGuard node gCont) = (LinearGuardThenGuard node (addStatementsContinuation stmts gCont))" |
"addStatementsContinuation stmts (LinearDisjointGuard g1 g2) = (LinearDisjointGuard (addStatementsContinuation stmts g1) (addStatementsContinuation stmts g2))"

fun addLinearGuardContinuation :: "LinearGuardExpression \<Rightarrow> LinearGuardExpression \<Rightarrow> LinearGuardExpression" where
"addLinearGuardContinuation gCont (LinearGuardNode (a, stmts)) = (LinearGuardThenGuard (a, stmts) gCont)" |
"addLinearGuardContinuation gCont (LinearGuardThenGuard node innerG) = (LinearGuardThenGuard node (addLinearGuardContinuation gCont innerG))" |
"addLinearGuardContinuation gCont (LinearDisjointGuard g1 g2) = (LinearDisjointGuard (addLinearGuardContinuation gCont g1) (addLinearGuardContinuation gCont g2))"

fun linearizeGuardExpression :: "FGuardExpression \<Rightarrow> LinearGuardExpression" where
"linearizeGuardExpression (ActionGuard a) = (LinearGuardNode (a, []))" |
"linearizeGuardExpression (GuardThenGuard g1 g2) = (addLinearGuardContinuation (linearizeGuardExpression g2) (linearizeGuardExpression g1))" |
"linearizeGuardExpression (GuardStmtsGuard g innerStmts) = addStatementsContinuation innerStmts (linearizeGuardExpression g)" |
"linearizeGuardExpression (DisjointGuard g1 g2) = (LinearDisjointGuard (linearizeGuardExpression g1) (linearizeGuardExpression g2))" |
"linearizeGuardExpression (InterleavedGuard g1 g2) = (let linearTree1 = linearizeGuardExpression g1 in
  let linearTree2 = linearizeGuardExpression g2 in
  interleaveLinearTrees linearTree1 linearTree2 True)"

fun linearGuardExpressionToCases :: "LinearGuardExpression \<Rightarrow> FContract \<Rightarrow> int \<Rightarrow> FContract \<Rightarrow> FCase list" where
"linearGuardExpressionToCases (LinearGuardNode (a, stmts)) cont t tcont = [Case (ActionGuard a) (prependStatements stmts cont)]" |
"linearGuardExpressionToCases (LinearGuardThenGuard (a, stmts) innerG) cont t tcont = [Case (ActionGuard a) (prependStatements stmts (When (linearGuardExpressionToCases innerG cont t tcont) t tcont))]" |
"linearGuardExpressionToCases (LinearDisjointGuard g1 g2) cont t tcont = (linearGuardExpressionToCases g1 cont t tcont) @ (linearGuardExpressionToCases g2 cont t tcont)"

lemma all_append:
"all f l1 \<Longrightarrow> all f l2 \<Longrightarrow> all f (l1 @ l2)"
  by (induction l1, auto)

lemma linearGuardExpressionToCasesAlwaysActionGuards:
"linearGuardExpressionToCases g cont t tcont = cases \<Longrightarrow>
  all (\<lambda>c . \<exists>a innerCont . c = Case (ActionGuard a) innerCont) cases"
  apply auto
  apply (induction g cont t tcont arbitrary: cases rule: linearGuardExpressionToCases.induct)
  using all_append by auto

type_synonym FLinearGuardConfig = "LinearGuardExpression * FContext * FState * Environment * TransactionWarning list * Payment list"
type_synonym FLinearGuardConfigRes = "((FStatement list) * LinearGuardExpression option) * FContext * FState * Environment * TransactionWarning list * Payment list"

fun computeLinearGuardStep :: "Input \<Rightarrow> FLinearGuardConfig \<Rightarrow> FLinearGuardConfigRes option" where
"computeLinearGuardStep ins (LinearGuardNode (a, stmts), ctx, s, env, warnings, payments) = (case computeActionStep ins (a, ctx, s, env) of
  Some (newS, applyWarning) \<Rightarrow> Some ((stmts, None), ctx, newS, env, warnings @ convertApplyWarning applyWarning, payments)
  | None \<Rightarrow> None)" |
"computeLinearGuardStep ins (LinearGuardThenGuard (a, stmts) g2, ctx, s, env, warnings, payments) = (case computeActionStep ins (a, ctx, s, env) of
  Some (newS, applyWarning) \<Rightarrow> Some ((stmts, Some g2), ctx, newS, env, warnings @ convertApplyWarning applyWarning, payments)
  | None \<Rightarrow> None)" |
"computeLinearGuardStep ins (LinearDisjointGuard g1 g2, ctx, s, env, warnings, payments) = (case computeLinearGuardStep ins (g1, ctx, s, env, warnings, payments) of
  Some res \<Rightarrow> Some res
  | None \<Rightarrow> (case computeLinearGuardStep ins (g2, ctx, s, env, warnings, payments) of
    Some res \<Rightarrow> Some res
    | None \<Rightarrow> None))"

inductive small_step_reduce_linear_guard :: "Input \<Rightarrow> FLinearGuardConfig \<Rightarrow> FLinearGuardConfigRes \<Rightarrow> bool" ("_ \<turnstile> _ \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 _" [55, 0, 55]) where
LinearGuardNodeStep:  "\<lbrakk>g = LinearGuardNode (a, stmts);
  ins \<turnstile> (a, ctx, s, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (newS, applyWarning)\<rbrakk> \<Longrightarrow> 
  ins \<turnstile> (g, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, None), ctx, newS, env, warnings @ convertApplyWarning applyWarning, payments)" |
LinearGuardThenGuardStep:  "\<lbrakk>g = LinearGuardThenGuard (a, stmts) g2;
  ins \<turnstile> (a, ctx, s, env) \<rightarrow>\<^sup>a\<^sub>f\<^sub>2 (newS, applyWarning)\<rbrakk> \<Longrightarrow> 
  ins \<turnstile> (g, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some g2), ctx, newS, env, warnings @ convertApplyWarning applyWarning, payments)" |
LinearDisjointGuardMatchesG1:  "\<lbrakk>(ins \<turnstile> (g1, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 g1Res)\<rbrakk> \<Longrightarrow> 
  ins \<turnstile> (LinearDisjointGuard g1 g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 g1Res" |
LinearDisjointGuardMatchesG2:  "\<lbrakk>computeLinearGuardStep ins (g1, ctx, s, env, warnings, payments) = None;
  (ins \<turnstile> (g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 g2Res)\<rbrakk> \<Longrightarrow> 
  ins \<turnstile> (LinearDisjointGuard g1 g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 g2Res"

inductive_cases LinearGuardNodeE[elim]: "ins \<turnstile> (LinearGuardNode (a, stmts), ctx, s, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 gRes"
inductive_cases LinearGuardThenGuardE[elim!]: "ins \<turnstile> (LinearGuardThenGuard (a, stmts) g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 gRes"
inductive_cases LinearDisjointGuardE_compute: "ins \<turnstile> (LinearDisjointGuard g1 g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 gRes"

definition "finalLinearGuard ins gConfig \<longleftrightarrow> (\<forall>res . \<not>(ins \<turnstile> gConfig \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 res))"

lemma computeLinearGuardStepIsSmallStep:
"computeLinearGuardStep ins (g, ctx, s, env, warnings, payments) = Some res \<Longrightarrow>
  ins \<turnstile> (g, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 res"
  apply (induction arbitrary: res rule: computeLinearGuardStep.induct[split_format(complete)])
  using LinearGuardNodeStep LinearGuardThenGuardStep LinearDisjointGuardMatchesG1 LinearDisjointGuardMatchesG2 by (auto split: option.split_asm simp add: computeActionStepIsSmallStep)

lemma smallStepLinearGuardIsComputed:
"ins \<turnstile> (g, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 res \<Longrightarrow>
  computeLinearGuardStep ins (g, ctx, s, env, warnings, payments) = Some res"
proof (induction ins "(g, ctx, s, env, warnings, payments)" res arbitrary: g rule: small_step_reduce_linear_guard.induct)
  case (LinearGuardNodeStep g a stmts ins newS applyWarning)
  then show ?case apply auto
    apply (cases "computeActionStep ins (a, ctx, s, env)", auto)
      apply (simp_all add: computeActionStepIsSmallStep option.discI)
    using deterministicActionSemantics by force+
next
  case (LinearGuardThenGuardStep g a stmts g2 ins newS applyWarning)
  moreover have "computeActionStep ins (a, ctx, s, env) = Some (newS, applyWarning)" using calculation computeActionStepIsSmallStep by auto
  ultimately show ?case by auto
qed auto

lemma computeLinearGuardStepHaltedImpliesSmallStepHalted:
"computeLinearGuardStep ins (g, ctx, s, env, warnings, payments) = None \<longleftrightarrow>
  finalLinearGuard ins (g, ctx, s, env, warnings, payments)"
  by (metis finalLinearGuard_def computeLinearGuardStepIsSmallStep smallStepLinearGuardIsComputed not_Some_eq)

lemma LinearDisjointGuardMatchesG2_no_computation:
"\<lbrakk>finalLinearGuard ins (g1, ctx, s, env, warnings, payments);
  (ins \<turnstile> (g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 g2Res)\<rbrakk> \<Longrightarrow> 
  ins \<turnstile> (LinearDisjointGuard g1 g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 g2Res"
  by (simp add: computeLinearGuardStepHaltedImpliesSmallStepHalted LinearDisjointGuardMatchesG2)

lemma LinearDisjointGuardE[elim!]:
"ins \<turnstile> (LinearDisjointGuard g1 g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 gRes \<Longrightarrow>
(ins \<turnstile> (g1, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 gRes \<Longrightarrow> P) \<Longrightarrow>
(finalLinearGuard ins (g1, ctx, s, env, warnings, payments) \<Longrightarrow>
 ins \<turnstile> (g2, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 gRes \<Longrightarrow> P) \<Longrightarrow> P"
  by (meson LinearDisjointGuardE_compute computeLinearGuardStepHaltedImpliesSmallStepHalted)

lemma deterministicLinearGuardExpressionsSemantics:
"ins \<turnstile> g \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 g' \<Longrightarrow>
ins \<turnstile> g \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 g'' \<Longrightarrow>
g' = g''"
  by (metis old.prod.exhaust option.inject smallStepLinearGuardIsComputed)

type_synonym FCaseConfig = "FCase * FContext * FState * Environment * TransactionWarning list * Payment list"
type_synonym FCaseConfigRes = "(int \<Rightarrow> FContract \<Rightarrow> FContract) * FContext * FState * Environment * TransactionWarning list * Payment list"

inductive small_step_reduce_case :: "Input \<Rightarrow> FCaseConfig \<Rightarrow> FCaseConfigRes \<Rightarrow> bool" ("_ \<turnstile> _ \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 _" [55, 0, 55]) where
CaseGuardCompleteStep:  "\<lbrakk>ins \<turnstile> (g, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, None), newCtx, newS, newEnv, newWarnings, newPayments)\<rbrakk> \<Longrightarrow> 
  ins \<turnstile> (Case g c, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 ((\<lambda>t tc . prependStatements stmts c), newCtx, newS, newEnv, newWarnings, newPayments)" |
CaseGuardIncompleteStep:  "\<lbrakk>ins \<turnstile> (g, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some innerG), newCtx, newS, newEnv, newWarnings, newPayments)\<rbrakk> \<Longrightarrow> 
  ins \<turnstile> (Case g c, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 ((\<lambda>t tc . prependStatements stmts (When [Case innerG c] t tc)), newCtx, newS, newEnv, newWarnings, newPayments)"

inductive_cases CaseStepE: "ins \<turnstile> (Case g c, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 res"

fun computeCaseStep :: "Input \<Rightarrow> FCaseConfig \<Rightarrow> FCaseConfigRes option" where
"computeCaseStep ins (Case g c, ctx, s, env, warnings, payments) = (case computeGuardStep ins (g, ctx, s, env, warnings, payments) of
  Some ((stmts, None), newCtx, newS, newEnv, newWarnings, newPayments) \<Rightarrow> Some ((\<lambda>t tc . prependStatements stmts c), newCtx, newS, newEnv, newWarnings, newPayments)
  | Some ((stmts, Some innerG), newCtx, newS, newEnv, newWarnings, newPayments) \<Rightarrow> Some ((\<lambda>t tc . prependStatements stmts (When [Case innerG c] t tc)), newCtx, newS, newEnv, newWarnings, newPayments)
  | None \<Rightarrow> None)"

lemma computeCaseStepResImpliesReduceStep:
"computeCaseStep ins c = Some res \<Longrightarrow>
  ins \<turnstile> c \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 res"
  apply (cases "(ins, c)" rule: computeCaseStep.cases)
  by (auto split: option.split_asm simp add: CaseGuardCompleteStep CaseGuardIncompleteStep computeGuardStepIsSmallStep)

lemma reduceCaseStepImpliesComputeCaseStepRes:
"ins \<turnstile> c \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 res \<Longrightarrow>
  computeCaseStep ins c = Some res"
  apply (cases ins c res rule: small_step_reduce_case.cases)
  by (auto split: option.split simp add: smallStepGuardIsComputed)

lemma reduceCaseStepConstants:
"ins \<turnstile> (c, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 (newC, newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
  ctx = newCtx \<and> payments = newPayments \<and> env = newEnv"
  apply (induction ins c ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments rule: small_step_reduce_case.induct[split_format(complete)])
  by (auto simp add: reduceGuardExpressionsConstants)

lemma computeCaseStepInvariantsHalted:
"computeCaseStep ins (c, ctx, s, env, warnings, payments) = None \<Longrightarrow>
  (\<forall>ctx' warnings' payments' . computeCaseStep ins (c, ctx', s, env, warnings', payments') = None)"
proof (induction "ins" "(c, ctx, s, env, warnings, payments)" arbitrary: c rule: computeCaseStep.induct)
  case (1 ins g c)
  then show ?case by (auto split: option.split_asm option.split; metis computeGuardStepInvariantsHalted option.discI)
qed

lemma reduceCaseInvariants:
"ins \<turnstile> (c, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 (newC, newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
  (\<forall>ctx' warnings' payments' .
    \<exists> newCtx' newWarnings' newPayments' .
    ins \<turnstile> (c, ctx', s, env, warnings', payments') \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 (newC, newCtx', newS, newEnv, newWarnings', newPayments') \<and>
    (ctx' = ctx \<longrightarrow> newCtx' = newCtx) \<and>
    (warnings' = warnings \<longrightarrow> newWarnings' = newWarnings) \<and>
    (payments' = payments \<longrightarrow> newPayments' = newPayments))"
proof (induction ins c ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments rule: small_step_reduce_case.induct[split_format(complete)])
  case (CaseGuardCompleteStep ins g ctx s env warnings payments stmts newCtx newS newEnv newWarnings newPayments c)
  moreover have "(\<forall>ctx' warnings' payments'. \<exists>newCtx' newWarnings' newPayments' . ins \<turnstile> (g, ctx', s, env, warnings', payments') \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, None), newCtx', newS, newEnv, newWarnings', newPayments') \<and>
    (ctx' = ctx \<longrightarrow> newCtx' = newCtx) \<and>
    (warnings' = warnings \<longrightarrow> newWarnings' = newWarnings) \<and>
    (payments' = payments \<longrightarrow> newPayments' = newPayments))"
    by (smt (verit, best) calculation reduceGuardExpressionsInvariants)
  ultimately show ?case apply auto
    by (metis small_step_reduce_case.CaseGuardCompleteStep)
next
  case (CaseGuardIncompleteStep ins g ctx s env warnings payments stmts innerG newCtx newS newEnv newWarnings newPayments c)
  moreover have "(\<forall>ctx' warnings' payments'. \<exists>newCtx' newWarnings' newPayments' . ins \<turnstile> (g, ctx', s, env, warnings', payments') \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some innerG), newCtx', newS, newEnv, newWarnings', newPayments') \<and>
    (ctx' = ctx \<longrightarrow> newCtx' = newCtx) \<and>
    (warnings' = warnings \<longrightarrow> newWarnings' = newWarnings) \<and>
    (payments' = payments \<longrightarrow> newPayments' = newPayments))"
    by (smt (verit, best) calculation reduceGuardExpressionsInvariants)
  ultimately show ?case apply auto
    by (metis small_step_reduce_case.CaseGuardIncompleteStep)
qed

lemma freshChoicePreserved_prependStatements:
"isFreshChoiceInContract cn cont \<Longrightarrow>
  isFreshChoiceInStatements cn stmts \<Longrightarrow>
  isFreshChoiceInContract cn (prependStatements stmts cont)"
  by (induction stmts cont rule: prependStatements.induct, auto)

lemma freshChoicePreserved_case_step_weak:
"ins \<turnstile> (c, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 (newC, newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
  isFreshChoiceInCases cn [c] \<Longrightarrow>
  isFreshChoiceInContext cn ctx \<Longrightarrow>
  isFreshChoiceInState cn s \<Longrightarrow>
  (isFreshChoiceInContext cn newCtx \<and> isFreshChoiceInState cn newS \<and> (isFreshChoiceInContract cn tc \<longrightarrow> isFreshChoiceInContract cn (newC t tc)))"
proof (induction ins c ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments rule: small_step_reduce_case.induct[split_format(complete)])
  case (CaseGuardCompleteStep ins g ctx s env warnings payments stmts newCtx newS newEnv newWarnings newPayments c)
  then show ?case apply (auto simp add: freshChoicePreserved_prependStatements freshChoicePreserved_guard_step)
    using freshChoicePreserved_guard_step_weak isFreshChoiceInState.simps by blast
next
  case (CaseGuardIncompleteStep ins g ctx s env warnings payments stmts innerG newCtx newS newEnv newWarnings newPayments c)
  then show ?case apply (auto simp add: freshChoicePreserved_guard_step_weak freshChoicePreserved_prependStatements)
    using freshChoicePreserved_guard_step_weak isFreshChoiceInState.simps by blast
qed

lemma freshChoicePreserved_case_step:
"ins \<turnstile> (c, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 (newC, newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
  isFreshChoiceInCases cn [c] \<Longrightarrow>
  isFreshChoiceInContext cn ctx \<Longrightarrow>
  (isFreshChoiceInContext cn newCtx \<and> (isFreshChoiceInContract cn tc \<longrightarrow> isFreshChoiceInContract cn (newC t tc)))"
proof (induction ins c ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments rule: small_step_reduce_case.induct[split_format(complete)])
  case (CaseGuardCompleteStep ins g ctx s env warnings payments stmts newCtx newS newEnv newWarnings newPayments c)
  then show ?case by (auto simp add: freshChoicePreserved_prependStatements freshChoicePreserved_guard_step)
next
  case (CaseGuardIncompleteStep ins g ctx s env warnings payments stmts innerG newCtx newS newEnv newWarnings newPayments c)
  then show ?case by (auto simp add: freshChoicePreserved_guard_step freshChoicePreserved_prependStatements)
qed

lemma freshChoiceNoMatch_case_step:
"\<not> ((IChoice (ChoiceId cn p) i) \<turnstile> (c, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 (newC, newCtx, newS, newEnv, newWarnings, newPayments) \<and> isFreshChoiceInCasesGuards cn [c])"
  apply (auto)
  apply (induction "(IChoice (ChoiceId cn p) i)" c ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments rule: small_step_reduce_case.induct[split_format(complete)])
  using finalGuard_def freshChoiceNoMatch_guard_step isFreshChoiceInCasesGuards.simps(2) by blast+

lemma freshChoiceMatchNoChoiceInput_case_step:
"isFreshChoiceInCases cn [c] \<Longrightarrow>
  ins \<turnstile> (c, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 (newC, newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
  (\<nexists>p i . (IChoice (ChoiceId cn p) i) = ins)"
  using freshChoiceNoMatch_case_step isFreshChoiceInCasesImpliesIsFreshChoiceInCasesGuards by blast

lemma freshChoiceShallowNoMatch_case_step:
"\<not> ((IChoice (ChoiceId cn p) i) \<turnstile> (c, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 (newC, newCtx, newS, newEnv, newWarnings, newPayments) \<and> isFreshChoiceInCasesGuardsShallow cn [c])"
  apply (auto)
  apply (induction "(IChoice (ChoiceId cn p) i)" c ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments rule: small_step_reduce_case.induct[split_format(complete)])
  using finalGuard_def freshChoiceNoMatch_guard_step isFreshChoiceInCasesGuardsShallow.simps(2) by blast+

lemma freshChoiceShallowMatchNoChoiceInput_case_step:
"isFreshChoiceInCasesGuardsShallow cn [c] \<Longrightarrow>
  ins \<turnstile> (c, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 (newC, newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
  (\<nexists>p i . (IChoice (ChoiceId cn p) i) = ins)"
  using freshChoiceShallowNoMatch_case_step isFreshChoiceInCasesImpliesIsFreshChoiceInCasesGuards by blast

type_synonym FCasesConfig = "FCase list * FContext * FState * Environment * TransactionWarning list * Payment list"
type_synonym FCasesConfigRes = "(int \<Rightarrow> FContract \<Rightarrow> FContract) * FContext * FState * Environment * TransactionWarning list * Payment list"

inductive small_step_reduce_cases :: "Input \<Rightarrow> FCasesConfig \<Rightarrow> FCasesConfigRes \<Rightarrow> bool" ("_ \<turnstile> _ \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 _" [55, 0, 55]) where
CasesHeadMatches: "ins \<turnstile> (c, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 (newC, newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
  ins \<turnstile> (c#rest, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC, newCtx, newS, newEnv, newWarnings, newPayments)" |
CasesTailMatches: "\<lbrakk>ins \<turnstile> (rest, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC, newCtx, newS, newEnv, newWarnings, newPayments);
  (\<forall>res . \<not>(ins \<turnstile> (c, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 res))\<rbrakk> \<Longrightarrow>
  ins \<turnstile> (c#rest, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC, newCtx, newS, newEnv, newWarnings, newPayments)"

inductive_cases SingleCaseCasesStepE: " ins \<turnstile> ([c], ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 res"
inductive_cases ConsCasesStepE: "ins \<turnstile> (c#cs, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 res"

definition "finalCases ins cConfig \<longleftrightarrow> (\<forall>res . \<not>(ins \<turnstile> cConfig \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 res))"

fun computeCasesStep :: "Input \<Rightarrow> FCasesConfig \<Rightarrow> FCasesConfigRes option" where
"computeCasesStep ins ([], _) = None" |
"computeCasesStep ins (c#cs, ctx, s, env, warnings, payments) = (case computeCaseStep ins (c, ctx, s, env, warnings, payments) of
  Some res \<Rightarrow> Some res
  | None \<Rightarrow> computeCasesStep ins (cs, ctx, s, env, warnings, payments))"

lemma computeCasesStepResImpliesReduceStep:
"computeCasesStep ins c = Some res \<Longrightarrow>
  ins \<turnstile> c \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 res"
  apply (induction ins c arbitrary: res rule: computeCasesStep.induct)
   apply (auto split: option.split_asm simp add: reduceCaseStepImpliesComputeCaseStepRes CasesTailMatches)
   apply (smt (verit, best) CasesTailMatches option.distinct(1) reduceCaseStepImpliesComputeCaseStepRes)
  by (simp add: CasesHeadMatches computeCaseStepResImpliesReduceStep)

lemma reduceCasesStepImpliesComputeCaseStepRes:
"ins \<turnstile> c \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 res \<Longrightarrow>
  computeCasesStep ins c = Some res"
  apply (induction ins c res rule: small_step_reduce_cases.induct)
   apply (simp add: reduceCaseStepImpliesComputeCaseStepRes)
  by (metis computeCaseStepResImpliesReduceStep computeCasesStep.simps(2) option.exhaust option.simps(4))

lemma deterministicCaseSemantics:
"ins \<turnstile> c \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 c' \<Longrightarrow>
  ins \<turnstile> c \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 c'' \<Longrightarrow>
  c' = c''"
  by (metis option.inject reduceCaseStepImpliesComputeCaseStepRes)

lemma deterministicCasesSemantics:
"ins \<turnstile> c \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 c' \<Longrightarrow>
  ins \<turnstile> c \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 c'' \<Longrightarrow>
  c' = c''"
  by (metis option.inject reduceCasesStepImpliesComputeCaseStepRes)

lemma reduceCasesConstants:
"ins \<turnstile> (c, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC, newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
  ctx = newCtx \<and> payments = newPayments \<and> env = newEnv"
  apply (induction ins c ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments rule: small_step_reduce_cases.induct[split_format(complete)])
  by (auto simp add: reduceCaseStepConstants)

lemma computeCasesStepInvariantsHalted:
"computeCasesStep ins (c, ctx, s, env, warnings, payments) = None \<Longrightarrow>
  (\<forall>ctx' warnings' payments' . computeCasesStep ins (c, ctx', s, env, warnings', payments') = None)"
  apply (induction "ins" "(c, ctx, s, env, warnings, payments)" arbitrary: c rule: computeCasesStep.induct, auto split: option.split_asm option.split)
  by (metis computeCaseStepInvariantsHalted option.distinct(1))

lemma reduceCasesInvaraintsHalted:
"finalCases ins (c, ctx, s, env, warnings, payments) \<Longrightarrow>
  (\<forall>ctx' warnings' payments' . finalCases ins (c, ctx', s, env, warnings', payments'))"
  by (metis (no_types, opaque_lifting) computeCasesStepInvariantsHalted computeCasesStepResImpliesReduceStep finalCases_def not_None_eq reduceCasesStepImpliesComputeCaseStepRes)

lemma reduceCasesInvariants:
"ins \<turnstile> (c, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC, newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
  (\<forall>ctx' warnings' payments' .
    \<exists> newCtx' newWarnings' newPayments' .
    ins \<turnstile> (c, ctx', s, env, warnings', payments') \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC, newCtx', newS, newEnv, newWarnings', newPayments') \<and>
    (ctx' = ctx \<longrightarrow> newCtx' = newCtx) \<and>
    (warnings' = warnings \<longrightarrow> newWarnings' = newWarnings) \<and>
    (payments' = payments \<longrightarrow> newPayments' = newPayments))"
proof (induction ins c ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments rule: small_step_reduce_cases.induct[split_format(complete)])
  case (CasesHeadMatches ins c ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments rest)
  then show ?case by (smt reduceCaseInvariants small_step_reduce_cases.CasesHeadMatches)
next
  case (CasesTailMatches ins rest ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments c)
  then show ?case by (smt (z3) computeCaseStepInvariantsHalted computeCaseStepResImpliesReduceStep option.collapse reduceCaseStepImpliesComputeCaseStepRes small_step_reduce_cases.CasesTailMatches)
qed

lemma freshChoicePreserved_cases_step_weak:
"ins \<turnstile> (c, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC, newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
  isFreshChoiceInCases cn c \<Longrightarrow>
  isFreshChoiceInContext cn ctx \<Longrightarrow>
  isFreshChoiceInState cn s \<Longrightarrow>
  (isFreshChoiceInContext cn newCtx \<and> isFreshChoiceInState cn newS \<and> (isFreshChoiceInContract cn tc \<longrightarrow> isFreshChoiceInContract cn (newC t tc)))"
proof (induction ins c ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments rule: small_step_reduce_cases.induct[split_format(complete)])
  case (CasesHeadMatches ins c ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments rest)
  then show ?case apply auto
    using reduceCaseStepConstants apply blast
     apply (metis FaustusV2AST.FCase.exhaust freshChoicePreserved_case_step_weak isFreshChoiceInCases.simps(1) isFreshChoiceInCases.simps(2) isFreshChoiceInState.simps)
    by (metis CasesHeadMatches.prems(3) FaustusV2AST.FCase.exhaust freshChoicePreserved_case_step_weak isFreshChoiceInCases.simps(1) isFreshChoiceInCases.simps(2))
next
  case (CasesTailMatches ins rest ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments c)
  then show ?case apply auto
    using reduceCasesConstants apply blast
    using isFreshChoiceInCases.elims(2) by blast+
qed

lemma freshChoicePreserved_cases_step:
"ins \<turnstile> (c, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC, newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
  isFreshChoiceInCases cn c \<Longrightarrow>
  isFreshChoiceInContext cn ctx \<Longrightarrow>
  (isFreshChoiceInContext cn newCtx \<and> (isFreshChoiceInContract cn tc \<longrightarrow> isFreshChoiceInContract cn (newC t tc)))"
proof (induction ins c ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments rule: small_step_reduce_cases.induct[split_format(complete)])
  case (CasesHeadMatches ins c ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments rest)
  then show ?case apply auto
    using reduceCaseStepConstants apply blast
    by (metis FaustusV2AST.FCase.exhaust freshChoicePreserved_case_step isFreshChoiceInCases.simps(1) isFreshChoiceInCases.simps(2))
next
  case (CasesTailMatches ins rest ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments c)
  then show ?case apply auto
    using reduceCasesConstants apply blast
    using isFreshChoiceInCases.elims(2) by blast+
qed

lemma freshChoiceNoMatch_cases_step_aux:
"(IChoice (ChoiceId cn p) i) \<turnstile> (c, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC, newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
  isFreshChoiceInCasesGuards cn c \<Longrightarrow>
  False"
  apply (induction "(IChoice (ChoiceId cn p) i)" c ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments rule: small_step_reduce_cases.induct[split_format(complete)])
   apply (metis FaustusV2AST.FCase.exhaust freshChoiceNoMatch_case_step isFreshChoiceInCasesGuards.simps(1) isFreshChoiceInCasesGuards.simps(2))
  by (metis FaustusV2AST.FCase.exhaust isFreshChoiceInCasesGuards.simps(2))

lemma freshChoiceShallowNoMatch_cases_step_aux:
"(IChoice (ChoiceId cn p) i) \<turnstile> (c, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC, newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
  isFreshChoiceInCasesGuardsShallow cn c \<Longrightarrow>
  False"
  apply (induction "(IChoice (ChoiceId cn p) i)" c ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments rule: small_step_reduce_cases.induct[split_format(complete)])
   apply (metis FaustusV2AST.FCase.exhaust freshChoiceShallowNoMatch_case_step isFreshChoiceInCasesGuardsShallow.simps)
  by (metis FaustusV2AST.FCase.exhaust isFreshChoiceInCasesGuardsShallow.simps(2))

lemma freshChoiceNoMatch_cases_step:
"isFreshChoiceInCasesGuards cn c \<Longrightarrow> finalCases (IChoice (ChoiceId cn p) i) (c, ctx, s, env, warnings, payments)"
  using finalCases_def freshChoiceNoMatch_cases_step_aux by fastforce

lemma freshChoiceMatchNoChoiceInput_cases_step:
"isFreshChoiceInCasesGuards cn c \<Longrightarrow>
  inp \<turnstile> (c, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC, newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
  (\<nexists>p i . (IChoice (ChoiceId cn p) i) = inp)"
  using finalCases_def freshChoiceNoMatch_cases_step by blast

lemma freshChoiceShallowNoMatch_cases_step:
"isFreshChoiceInCasesGuardsShallow cn c \<Longrightarrow> finalCases (IChoice (ChoiceId cn p) i) (c, ctx, s, env, warnings, payments)"
  using finalCases_def freshChoiceShallowNoMatch_cases_step_aux by fastforce

lemma freshChoiceShallowMatchNoChoiceInput_cases_step:
"isFreshChoiceInCasesGuardsShallow cn c \<Longrightarrow>
  inp \<turnstile> (c, ctx, s, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC, newCtx, newS, newEnv, newWarnings, newPayments) \<Longrightarrow>
  (\<nexists>p i . (IChoice (ChoiceId cn p) i) = inp)"
  using finalCases_def freshChoiceShallowNoMatch_cases_step by blast

lemma linearGuardFinalImpliesAddContinuationFinal:
"finalLinearGuard ins (g1, ctx, state, env, warnings, payments) \<Longrightarrow> finalLinearGuard ins ((addLinearGuardContinuation g2 g1), ctx, state, env, warnings, payments)"
proof (induction g2 g1 rule: addLinearGuardContinuation.induct)
  case (1 gCont a stmts)
  then show ?case using LinearGuardNodeStep by (fastforce simp add: finalLinearGuard_def)
next
  case (2 gCont node innerG)
  then show ?case apply (auto simp add: finalLinearGuard_def)
    apply (cases node, auto)
    using LinearGuardThenGuardStep by metis
next
  case (3 gCont g1 g2)
  then show ?case apply (auto simp add: finalLinearGuard_def)
     apply (meson LinearDisjointGuardMatchesG1)
    by (meson "3.prems" LinearDisjointGuardMatchesG1 LinearDisjointGuardMatchesG2_no_computation finalLinearGuard_def)
qed

lemma linearGuardFinalImpliesAddStatementsContinuationFinal:
"finalLinearGuard ins (g1, ctx, state, env, warnings, payments) \<Longrightarrow> finalLinearGuard ins (addStatementsContinuation stmts g1, ctx, state, env, warnings, payments)"
proof (induction stmts g1 rule: addStatementsContinuation.induct)
  case (1 gCont a stmts)
  then show ?case using LinearGuardNodeStep by (fastforce simp add: finalLinearGuard_def)
next
  case (2 gCont node innerG)
  then show ?case apply (auto simp add: finalLinearGuard_def)
    apply (cases node, auto)
    using LinearGuardThenGuardStep by metis
next
  case (3 gCont g1 g2)
  then show ?case apply (auto simp add: finalLinearGuard_def)
     apply (meson LinearDisjointGuardMatchesG1)
    by (meson "3.prems" LinearDisjointGuardMatchesG1 LinearDisjointGuardMatchesG2_no_computation finalLinearGuard_def)
qed

lemma linearGuardFinalImpliesInterleavedLinearGuardFinal:
"finalLinearGuard ins (g1, ctx, state, env, warnings, payments) \<Longrightarrow>
  finalLinearGuard ins (g2, ctx, state, env, warnings, payments) \<Longrightarrow>
  finalLinearGuard ins ((interleaveLinearTrees g1 g2 f), ctx, state, env, warnings, payments)"
"finalLinearGuard ins (g1, ctx, state, env, warnings, payments) \<Longrightarrow>
  finalLinearGuard ins (g2, ctx, state, env, warnings, payments) \<Longrightarrow>
  finalLinearGuard ins ((interleaveLinearTreesRightFirst g1 g2), ctx, state, env, warnings, payments)"
proof (induction g1 g2 f and g1 g2 rule: interleaveLinearTrees_interleaveLinearTreesRightFirst.induct)
  case (1 node g2 f)
  then show ?case by (cases f; auto simp add: finalLinearGuard_def; cases node; auto; metis LinearGuardNodeStep)
next
  case (2 node gCont g2 f)
  then show ?case by (cases f; auto simp add: finalLinearGuard_def; cases node; auto; meson LinearGuardThenGuardStep)
next
  case (3 innerG1 innerG2 g2 f)
  then show ?case by (cases f; auto simp add: finalLinearGuard_def; auto; meson LinearDisjointGuardMatchesG1 LinearDisjointGuardMatchesG2_no_computation finalLinearGuard_def)
next
  case (4 node g2)
  then show ?case by (cases node; auto simp add: finalLinearGuard_def; meson LinearGuardNodeStep)
next
  case (5 node gCont g2)
  then show ?case by (cases node; auto simp add: finalLinearGuard_def; meson LinearGuardThenGuardStep)
next
  case (6 innerG1 innerG2 g2)
  then show ?case by (auto simp add: finalLinearGuard_def; meson "6.prems"(1) LinearDisjointGuardMatchesG1 LinearDisjointGuardMatchesG2_no_computation finalLinearGuard_def)
qed

lemma finalGuardImpliesFinalLinearGuard:
"finalGuard ins (g, ctx, state, env, warnings, payments) \<Longrightarrow> finalLinearGuard ins (linearizeGuardExpression g, ctx, state, env, warnings, payments)"
proof (induction g rule: linearizeGuardExpression.induct)
  case (1 a)
  then show ?case apply (auto simp add: finalGuard_def finalLinearGuard_def)
    apply (cases rule: small_step_reduce_linear_guard.cases, auto)
    using ActionGuardStep by fastforce
next
  case (2 g1 g2)
  moreover have "finalGuard ins (g1, ctx, state, env, warnings, payments)" using calculation apply (auto simp add: finalGuard_def)
    using GuardThenGuardCompleteFirstGuardStep GuardThenGuardIncompleteGuard option.exhaust by metis
  ultimately show ?case by (auto simp add: finalGuard_def linearGuardFinalImpliesAddContinuationFinal)
next
  case (3 g innerStmts)
  moreover have "finalGuard ins (g, ctx, state, env, warnings, payments)" using calculation apply (auto simp add: finalGuard_def)
    using GuardStmtsGuardCompleteGuardStep GuardStmtsGuardIncompleteGuard option.exhaust by metis
  ultimately show ?case by (auto simp add: finalGuard_def linearGuardFinalImpliesAddStatementsContinuationFinal)
next
  case (4 g1 g2)
  moreover have "finalGuard ins (g1, ctx, state, env, warnings, payments)" using calculation apply (auto simp add: finalGuard_def)
    using DisjointGuardMatchesG1 by metis
  moreover have "finalGuard ins (g2, ctx, state, env, warnings, payments)" using calculation apply (auto simp add: finalGuard_def)
    using DisjointGuardMatchesG2_no_computation by (meson calculation(4))
  ultimately show ?case by (auto simp add: finalGuard_def finalLinearGuard_def)
next
  case (5 g1 g2)
  moreover have "finalGuard ins (g1, ctx, state, env, warnings, payments)" using calculation apply (auto simp add: finalGuard_def)
    using InterleavedGuardMatchesG1Complete InterleavedGuardMatchesG1Incomplete option.exhaust by metis
  moreover have "finalGuard ins (g2, ctx, state, env, warnings, payments)" using calculation apply (auto simp add: finalGuard_def)
    using InterleavedGuardMatchesG2Complete_no_computation InterleavedGuardMatchesG2Incomplete_no_computation option.exhaust by (metis calculation(4))
  ultimately show ?case using linearGuardFinalImpliesInterleavedLinearGuardFinal(1) linearizeGuardExpression.simps(5) by presburger
qed

lemma linearGuardStepImpliesAddStatementsContinuationStep:
"ins \<turnstile> (g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, None), ctx', state', env', warnings', payments') \<Longrightarrow>
  ins \<turnstile> ((addStatementsContinuation stmts2 g1), ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts @ stmts2, None), ctx', state', env', warnings', payments')"
proof (induction stmts2 g1 rule: addStatementsContinuation.induct)
  case (1 gCont a stmts)
  then show ?case by (cases rule: small_step_reduce_linear_guard.cases, auto simp add: LinearGuardNodeStep)
next
  case (2 gCont node innerG)
  then show ?case apply auto
    by (cases rule: small_step_reduce_linear_guard.cases, auto)
next
  case (3 gCont g1 g2)
  then show ?case apply auto
     apply (cases rule: small_step_reduce_linear_guard.cases, auto)
    using LinearDisjointGuardMatchesG1 "3.IH"(1) LinearDisjointGuardMatchesG2_no_computation linearGuardFinalImpliesAddStatementsContinuationFinal by presburger+
qed

lemma linearGuardStepImpliesAddStatementsContinuationStepCalculated:
"ins \<turnstile> (g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, gCont), ctx', state', env', warnings', payments') \<Longrightarrow>
  ins \<turnstile> ((addStatementsContinuation stmts2 g1), ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((if gCont = None then stmts @ stmts2 else stmts, if gCont = None then None else Some (addStatementsContinuation stmts2 (the gCont))), ctx', state', env', warnings', payments')"
proof (induction stmts2 g1 arbitrary: stmts gCont rule: addStatementsContinuation.induct)
  case (1 gCont a stmts)
  then show ?case by (cases rule: small_step_reduce_linear_guard.cases, auto simp add: LinearGuardNodeStep)
next
  case (2 boundStmts node innerG)
  then show ?case apply auto
     apply (cases rule: small_step_reduce_linear_guard.cases, auto)
    by (cases node, auto simp add: LinearGuardThenGuardStep)
next
  case (3 gCont g1 g2)
  then show ?case apply auto
     apply (cases rule: small_step_reduce_linear_guard.cases, auto)
    using LinearDisjointGuardMatchesG1 "3.IH"(1) LinearDisjointGuardMatchesG2_no_computation linearGuardFinalImpliesAddStatementsContinuationFinal apply (presburger)+
    using LinearDisjointGuardMatchesG1 apply fastforce
    using LinearDisjointGuardMatchesG2_no_computation linearGuardFinalImpliesAddStatementsContinuationFinal apply presburger
    by (smt (verit) LinearDisjointGuardMatchesG2_no_computation linearGuardFinalImpliesAddStatementsContinuationFinal option.discI option.sel)
qed

lemma linearGuardStepImpliesAddStatementsContinuationStepGeneral:
"ins \<turnstile> (g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, gCont), ctx', state', env', warnings', payments') \<Longrightarrow>
  (\<exists>newStmts newGCont . ins \<turnstile> ((addStatementsContinuation stmts2 g1), ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((newStmts, newGCont), ctx', state', env', warnings', payments') \<and>
  (gCont = None \<longrightarrow> newStmts = stmts @ stmts2 \<and> newGCont = None) \<and>
  (gCont \<noteq> None \<longrightarrow> newStmts = stmts \<and> newGCont = Some (addStatementsContinuation stmts2 (the gCont))))"
  using linearGuardStepImpliesAddStatementsContinuationStepCalculated by fastforce

lemma addStatementsContinuationFinalImpliesFinalLinearGuard:
"finalLinearGuard ins (addStatementsContinuation stmts2 g1, ctx, state, env, warnings, payments) \<Longrightarrow>
  finalLinearGuard ins (g1, ctx, state, env, warnings, payments)"
proof (induction stmts2 g1 rule: addStatementsContinuation.induct)
  case (1 stmts a innerStmts)
  then show ?case apply (auto simp add: finalLinearGuard_def)
    apply (elim LinearGuardNodeE, auto simp add: LinearGuardNodeStep)
    using LinearGuardNodeStep by blast
next
  case (2 stmts node gCont)
  then show ?case apply (cases node, auto simp add: finalLinearGuard_def)
    using LinearGuardThenGuardStep by metis
next
  case (3 stmts g1 g2)
  then show ?case apply (auto simp add: finalLinearGuard_def)
     apply (meson LinearDisjointGuardMatchesG1)
    by (metis "3.prems" LinearDisjointGuardMatchesG1 LinearDisjointGuardMatchesG2_no_computation addStatementsContinuation.simps(3) finalLinearGuard_def)
qed

lemma addLinearGuardContinuationFinalImpliesFinalLinearGuard:
"finalLinearGuard ins (addLinearGuardContinuation g2 g1, ctx, state, env, warnings, payments) \<Longrightarrow>
  finalLinearGuard ins (g1, ctx, state, env, warnings, payments)"
proof (induction g2 g1 rule: addLinearGuardContinuation.induct)
  case (1 stmts a innerStmts)
  then show ?case apply (auto simp add: finalLinearGuard_def)
    apply (elim LinearGuardNodeE, auto simp add: LinearGuardThenGuardStep)
    using LinearGuardThenGuardStep by blast
next
  case (2 stmts node gCont)
  then show ?case apply (cases node, auto simp add: finalLinearGuard_def)
    using LinearGuardThenGuardStep by metis
next
  case (3 stmts g1 g2)
  then show ?case apply (auto simp add: finalLinearGuard_def)
     apply (meson LinearDisjointGuardMatchesG1)
    by (metis "3.prems" LinearDisjointGuardMatchesG1 LinearDisjointGuardMatchesG2_no_computation addLinearGuardContinuation.simps(3) finalLinearGuard_def)
qed

lemma addStatementsContinuationStepImpliesLinearGuardStep:
"ins \<turnstile> ((addStatementsContinuation stmts2 g1), ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, gCont), ctx', state', env', warnings', payments') \<Longrightarrow>
  (\<exists>stmts1 gCont1. ins \<turnstile> (g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts1, gCont1), ctx', state', env', warnings', payments'))"
proof (induction stmts2 g1 arbitrary: stmts gCont rule: addStatementsContinuation.induct)
  case (1 boundStmts a innerStmts)
  then show ?case apply (cases rule: small_step_reduce_linear_guard.cases, auto simp add: LinearGuardNodeStep)
    by (meson LinearGuardNodeStep)
next
  case (2 boundStmts node boundGCont)
  then show ?case apply (cases node, auto)
    by (meson LinearGuardThenGuardStep)
next
  case (3 stmts g1 g2)
  then show ?case by (auto; meson LinearDisjointGuardMatchesG1 LinearDisjointGuardMatchesG2_no_computation addStatementsContinuationFinalImpliesFinalLinearGuard)
qed

lemma linearGuardStepImpliesAddContinuationStep:
"ins \<turnstile> (g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, None), ctx', state', env', warnings', payments') \<Longrightarrow>
  ins \<turnstile> (addLinearGuardContinuation g2 g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some g2), ctx', state', env', warnings', payments')"
proof (induction g2 g1 rule: addLinearGuardContinuation.induct)
  case (1 gCont a stmts)
  then show ?case by (cases rule: small_step_reduce_linear_guard.cases, auto simp add: LinearGuardThenGuardStep)
next
  case (2 gCont node innerG)
  then show ?case by (cases node, auto)
next
  case (3 gCont g1 g2)
  then show ?case apply auto
     apply (cases rule: small_step_reduce_linear_guard.cases, auto)
    using "3.IH"(1) LinearDisjointGuardMatchesG1 LinearDisjointGuardMatchesG2_no_computation linearGuardFinalImpliesAddContinuationFinal by presburger+
qed

lemma addLinearGuardContinuationStepImpliesLinearGuardStep:
"ins \<turnstile> (addLinearGuardContinuation g2 g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, gCont), ctx', state', env', warnings', payments') \<Longrightarrow>
  (\<exists>gCont' . ins \<turnstile> (g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, gCont'), ctx', state', env', warnings', payments'))"
proof (induction g2 g1 rule: addLinearGuardContinuation.induct)
  case (1 gCont a stmts)
  then show ?case apply (cases rule: small_step_reduce_linear_guard.cases, auto simp add: LinearGuardThenGuardStep)
    by (meson LinearGuardNodeStep)
next
  case (2 gCont node innerG)
  then show ?case apply (cases node, auto)
    by (meson LinearGuardThenGuardStep)
next
  case (3 gCont g1 g2)
  then show ?case apply auto
     apply (cases rule: small_step_reduce_linear_guard.cases, auto)
    by (meson LinearDisjointGuardMatchesG1 LinearDisjointGuardMatchesG2_no_computation addLinearGuardContinuationFinalImpliesFinalLinearGuard)+
qed

lemma linearGuardStepWithContinuationImpliesAddContinuationStepWithContinuation:
"ins \<turnstile> (g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some innerGCont), ctx', state', env', warnings', payments') \<Longrightarrow>
  ins \<turnstile> (addLinearGuardContinuation g2 g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some (addLinearGuardContinuation g2 innerGCont)), ctx', state', env', warnings', payments')"
proof (induction g2 g1 rule: addLinearGuardContinuation.induct)
  case (1 gCont a stmts)
  then show ?case by (cases rule: small_step_reduce_linear_guard.cases, auto)
next
  case (2 gCont node innerG)
  then show ?case apply auto
    apply (cases rule: small_step_reduce_linear_guard.cases, auto)
    using LinearGuardThenGuardStep by presburger
next
  case (3 gCont g1 g2)
  then show ?case apply auto
     apply (cases rule: small_step_reduce_linear_guard.cases, auto)
    using "3.IH"(1) LinearDisjointGuardMatchesG1 LinearDisjointGuardMatchesG2_no_computation linearGuardFinalImpliesAddContinuationFinal by presburger+
qed

lemma linearGuardStepWithContinuationImpliesAddStatementsContinuationStepWithContinuation:
"ins \<turnstile> (g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some innerGCont), ctx', state', env', warnings', payments') \<Longrightarrow>
  ins \<turnstile> ((addStatementsContinuation stmts2 g1), ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, (Some (addStatementsContinuation stmts2 innerGCont))), ctx', state', env', warnings', payments')"
proof (induction stmts g1 rule: addStatementsContinuation.induct)
  case (1 gCont a stmts)
  then show ?case by (cases rule: small_step_reduce_linear_guard.cases, auto)
next
  case (2 gCont node innerG)
  then show ?case by (cases node; auto simp add: LinearGuardThenGuardStep)
next
  case (3 gCont g1 g2)
  then show ?case apply (auto simp add: LinearDisjointGuardMatchesG1)
    using LinearDisjointGuardMatchesG2_no_computation linearGuardFinalImpliesAddStatementsContinuationFinal by presburger
qed

lemma finalLinearGuardLeftImpliesInterleavedNoFlipFinalLinearGuard:
"finalLinearGuard ins (g1, ctx, state, env, warnings, payments) \<Longrightarrow>
  f=False \<Longrightarrow>
  finalLinearGuard ins ((interleaveLinearTrees g1 g2 f), ctx, state, env, warnings, payments)"
"finalLinearGuard ins (g1, ctx, state, env, warnings, payments) \<Longrightarrow>
  finalLinearGuard ins ((interleaveLinearTreesRightFirst g1 g2), ctx, state, env, warnings, payments)"
proof (induction g1 g2 f and g1 g2 rule: interleaveLinearTrees_interleaveLinearTreesRightFirst.induct)
  case (1 node g2 f)
  then show ?case using linearGuardFinalImpliesAddContinuationFinal by (cases f; cases node; auto; fastforce)
next
  case (2 node gCont g2 f)
  then show ?case apply (cases f, auto simp add: finalLinearGuard_def)
    apply (cases rule: small_step_reduce_linear_guard.cases, auto)
    using LinearGuardThenGuardStep by blast
next
  case (3 innerG1 innerG2 g2 f)
  then show ?case apply (cases f, auto simp add: finalLinearGuard_def)
     apply (cases rule: small_step_reduce_linear_guard.cases, auto simp add: smallStepLinearGuardIsComputed)
    by (meson LinearDisjointGuardMatchesG1 LinearGuardThenGuardStep finalLinearGuard_def LinearDisjointGuardMatchesG2 "3.prems"(1) LinearDisjointGuardMatchesG2_no_computation)+
next
  case (4 node g2)
  then show ?case apply (cases node, auto simp add: finalLinearGuard_def)
    using LinearGuardNodeStep by blast
next
  case (5 node gCont g2)
  then show ?case apply (auto simp add: finalLinearGuard_def)
    apply (cases rule: small_step_reduce_linear_guard.cases, auto)
    using LinearGuardThenGuardStep by blast
next
  case (6 innerG1 innerG2 g2)
  then show ?case apply (auto simp add: finalLinearGuard_def)
     apply (cases rule: small_step_reduce_linear_guard.cases, auto simp add: smallStepLinearGuardIsComputed)
    by (meson LinearDisjointGuardMatchesG1 LinearGuardThenGuardStep finalLinearGuard_def LinearDisjointGuardMatchesG2 "6.prems"(1) LinearDisjointGuardMatchesG2_no_computation)+
qed

lemma leftLinearGuardStepNoContinueImpliesInterleavedLinearGuardStepWithRightContinue:
"ins \<turnstile> (g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, None), ctx', state', env', warnings', payments') \<longrightarrow>
  ins \<turnstile> (interleaveLinearTrees g1 g2 f, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some g2), ctx', state', env', warnings', payments')"
"ins \<turnstile> (g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, None), ctx', state', env', warnings', payments') \<longrightarrow>
  ins \<turnstile> (interleaveLinearTreesRightFirst g1 g2, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some g2), ctx', state', env', warnings', payments')"
proof (induction g1 g2 f and g1 g2 rule: interleaveLinearTrees_interleaveLinearTreesRightFirst.induct)
  case (1 node g2 f)
  then show ?case apply (cases f; cases node; auto)
      apply (metis LinearDisjointGuardMatchesG1 addLinearGuardContinuation.simps(1) linearGuardStepImpliesAddContinuationStep)
     apply (metis LinearDisjointGuardMatchesG1 addLinearGuardContinuation.simps(1) linearGuardStepImpliesAddContinuationStep)
    by (metis addLinearGuardContinuation.simps(1) linearGuardStepImpliesAddContinuationStep)
next
  case (2 node gCont g2 f)
  then show ?case by (cases f; cases node; auto)
next
  case (3 innerG1 innerG2 g2 f)
  then show ?case apply (cases f, auto)
    by (cases rule: small_step_reduce_linear_guard.cases, auto simp add: LinearGuardThenGuardStep LinearDisjointGuardMatchesG1 LinearDisjointGuardMatchesG2 computeLinearGuardStepHaltedImpliesSmallStepHalted finalLinearGuardLeftImpliesInterleavedNoFlipFinalLinearGuard)
next
  case (4 node g2)
  then show ?case apply (cases node; auto)
    by (metis addLinearGuardContinuation.simps(1) linearGuardStepImpliesAddContinuationStep)
next
  case (5 node gCont g2)
  then show ?case by (cases node; auto)
next
  case (6 innerG1 innerG2 g2)
  then show ?case apply auto
    by (cases rule: small_step_reduce_linear_guard.cases, auto simp add: LinearGuardThenGuardStep LinearDisjointGuardMatchesG1 LinearDisjointGuardMatchesG2 computeLinearGuardStepHaltedImpliesSmallStepHalted finalLinearGuardLeftImpliesInterleavedNoFlipFinalLinearGuard)
qed

lemma leftLinearGuardStepWithContinueImpliesInterleavedLinearGuardStepWithRightInterleavedContinue:
"ins \<turnstile> (g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some innerG1), ctx', state', env', warnings', payments') \<longrightarrow>
  ins \<turnstile> ((interleaveLinearTrees g1 g2 f), ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some (interleaveLinearTrees innerG1 g2 True)), ctx', state', env', warnings', payments')"
"ins \<turnstile> (g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some innerG1), ctx', state', env', warnings', payments') \<longrightarrow>
  ins \<turnstile> ((interleaveLinearTreesRightFirst g1 g2), ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some (interleaveLinearTrees g2 innerG1 True)), ctx', state', env', warnings', payments')"
proof (induction g1 g2 f and g1 g2 rule: interleaveLinearTrees_interleaveLinearTreesRightFirst.induct)
  case (1 node g2 f)
  then show ?case by (cases f; cases node; auto)
next
  case (2 node gCont g2 f)
  then show ?case apply (cases f; cases node; auto)
    by (meson LinearDisjointGuardMatchesG1 LinearGuardThenGuardStep)+
next
  case (3 innerG1 innerG2 g2 f)
  then show ?case by (cases f, auto simp add: LinearDisjointGuardMatchesG1 LinearDisjointGuardMatchesG2_no_computation finalLinearGuardLeftImpliesInterleavedNoFlipFinalLinearGuard(1))
next
  case (4 node g2)
  then show ?case by (cases node, auto)
next
  case (5 node gCont g2)
  then show ?case apply (cases node)
    apply (auto simp del: interleaveLinearTrees.simps)
    by (meson LinearDisjointGuardMatchesG1 LinearGuardThenGuardStep)+
next
  case (6 innerG1a innerG2 g2)
  then show ?case by (auto simp add: LinearDisjointGuardMatchesG1 LinearDisjointGuardMatchesG2_no_computation finalLinearGuardLeftImpliesInterleavedNoFlipFinalLinearGuard(1) finalLinearGuardLeftImpliesInterleavedNoFlipFinalLinearGuard(2))
qed

lemma rightLinearGuardStepNoContinueImpliesInterleavedLinearGuardStepWithLeftContinue:
"finalLinearGuard ins (g1, ctx, state, env, warnings, payments) \<longrightarrow>
  ins \<turnstile> (g2, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, None), ctx', state', env', warnings', payments') \<longrightarrow>
  f = True \<longrightarrow>
  ins \<turnstile> (interleaveLinearTrees g1 g2 True, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some g1), ctx', state', env', warnings', payments')"
"finalLinearGuard ins (g1, ctx, state, env, warnings, payments) \<longrightarrow>
  ins \<turnstile> (g2, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, None), ctx', state', env', warnings', payments') \<longrightarrow>
  f = True \<longrightarrow>
  ins \<turnstile> (interleaveLinearTreesRightFirst g2 g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some g1), ctx', state', env', warnings', payments')"
proof (induction g1 g2 f and g1 g2 rule: interleaveLinearTrees_interleaveLinearTreesRightFirst.induct)
  case (1 node g2 f)
  then show ?case apply (cases node; cases f; auto)
    by (metis LinearDisjointGuardMatchesG2_no_computation finalLinearGuardLeftImpliesInterleavedNoFlipFinalLinearGuard(2) interleaveLinearTreesRightFirst.simps(1) leftLinearGuardStepNoContinueImpliesInterleavedLinearGuardStepWithRightContinue(2))+
next
  case (2 node gCont g2 f)
  then show ?case apply (cases f; cases node; auto)
    by (meson LinearDisjointGuardMatchesG2_no_computation LinearGuardThenGuardE LinearGuardThenGuardStep finalLinearGuard_def leftLinearGuardStepNoContinueImpliesInterleavedLinearGuardStepWithRightContinue(2))+
next
  case (3 innerG1 innerG2 g2 f)
  then show ?case apply (cases f, auto)
    by (metis LinearDisjointGuardMatchesG1 LinearDisjointGuardMatchesG2_no_computation finalLinearGuardLeftImpliesInterleavedNoFlipFinalLinearGuard(1) finalLinearGuard_def leftLinearGuardStepNoContinueImpliesInterleavedLinearGuardStepWithRightContinue(2))
next
  case (4 node g2)
  then show ?case apply (cases node, auto)
    using leftLinearGuardStepNoContinueImpliesInterleavedLinearGuardStepWithRightContinue(2) by auto
next
  case (5 node gCont g2)
  then show ?case apply (cases node; auto)
    by (meson LinearDisjointGuardMatchesG2_no_computation LinearGuardThenGuardE LinearGuardThenGuardStep finalLinearGuard_def leftLinearGuardStepNoContinueImpliesInterleavedLinearGuardStepWithRightContinue(2))+
next
  case (6 innerG1 innerG2 g2)
  then show ?case by (auto simp add: leftLinearGuardStepNoContinueImpliesInterleavedLinearGuardStepWithRightContinue(2))
qed

lemma finalLinearDisjointImpliesBothFinal:
"finalLinearGuard ins2 (LinearDisjointGuard innerG12 innerG22, ctx2, state2, env2, warnings2, payments2) \<longrightarrow>
  finalLinearGuard ins2 (innerG12, ctx2, state2, env2, warnings2, payments2) \<and> finalLinearGuard ins2 (innerG22, ctx2, state2, env2, warnings2, payments2)"
  by (metis LinearDisjointGuardMatchesG1 LinearDisjointGuardMatchesG2_no_computation finalLinearGuard_def)

lemma finalInterleaveTreesImpliesBothFinal:
"finalLinearGuard ins2 (interleaveLinearTrees g1 g2 f, ctx2, state2, env2, warnings2, payments2) \<longrightarrow>
  finalLinearGuard ins2 (g1, ctx2, state2, env2, warnings2, payments2) \<and> (f \<longrightarrow> finalLinearGuard ins2 (g2, ctx2, state2, env2, warnings2, payments2))"
"finalLinearGuard ins2 (interleaveLinearTreesRightFirst g1 g2, ctx2, state2, env2, warnings2, payments2) \<longrightarrow>
  finalLinearGuard ins2 (g1, ctx2, state2, env2, warnings2, payments2)"
proof (induction g1 g2 f and g1 g2 rule: interleaveLinearTrees_interleaveLinearTreesRightFirst.induct)
  case (1 node g2 f)
  then show ?case apply (cases f; cases node, auto simp add: finalLinearGuard_def)
       apply (meson LinearDisjointGuardMatchesG1 LinearGuardNodeE LinearGuardThenGuardStep)
      apply (metis LinearDisjointGuardMatchesG1 LinearDisjointGuardMatchesG2_no_computation LinearGuardNodeE LinearGuardThenGuardStep addLinearGuardContinuation.simps(1) finalLinearGuard_def linearGuardFinalImpliesAddContinuationFinal)
    by (meson LinearDisjointGuardMatchesG1 LinearGuardNodeE LinearGuardThenGuardStep)+
next
  case (2 node gCont g2 f)
  then show ?case apply (cases f; cases node, auto simp add: finalLinearGuard_def)
        apply (meson LinearDisjointGuardMatchesG1 LinearGuardThenGuardStep)
       apply (metis LinearDisjointGuardE_compute LinearDisjointGuardMatchesG2 LinearGuardThenGuardE finalLinearDisjointImpliesBothFinal finalLinearGuard_def)
    by (meson LinearDisjointGuardMatchesG1 LinearGuardThenGuardStep)+
next
  case (3 innerG1 innerG2 g2 f)
  then show ?case apply (cases f, auto)
    by (meson finalLinearDisjointImpliesBothFinal LinearDisjointGuardE finalLinearGuard_def)+
next
  case (4 node g2)
  then show ?case apply (cases node, auto simp add: finalLinearGuard_def)
    by (meson LinearDisjointGuardMatchesG1 LinearGuardNodeE LinearGuardThenGuardStep)
next
  case (5 node gCont g2)
  then show ?case apply (cases node, auto simp add: finalLinearGuard_def)
    by (meson LinearGuardThenGuardStep)+
next
  case (6 innerG1 innerG2 g2)
  then show ?case apply auto
    using finalLinearDisjointImpliesBothFinal by (blast | meson LinearDisjointGuardE finalLinearGuard_def)+
qed

lemma linearGuardStepNoContinuationGuardImpliesNotInterleaved:
"ins \<turnstile> (interleaveLinearTrees g1 g2 f, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, None::LinearGuardExpression option), ctx', state', env', warnings', payments') \<longrightarrow>
  False"
"ins \<turnstile> (interleaveLinearTreesRightFirst g1 g2, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, None::LinearGuardExpression option), ctx', state', env', warnings', payments') \<longrightarrow>
  False"
proof (induction g1 g2 f and g1 g2 arbitrary: g rule: interleaveLinearTrees_interleaveLinearTreesRightFirst.induct)
  case (1 node g2 f)
  then show ?case by (cases f; cases node; auto)
next
  case (2 node gCont g2 f)
  then show ?case by (cases f; cases node; auto)
next
  case (3 innerG1 innerG2 g2 f)
  then show ?case by (cases f, auto)
next
  case (4 node g2)
  then show ?case by (cases node, auto)
next
  case (5 node gCont g2)
  then show ?case by (cases node, auto)
next
  case (6 innerG1 innerG2 g2)
  then show ?case by auto
qed

lemma rightLinearGuardStepWithContinueImpliesInterleavedLinearGuardStepWithLeftContinueInterleaved:
"finalLinearGuard ins (g1, ctx, state, env, warnings, payments) \<longrightarrow>
  ins \<turnstile> (g2, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some innerGCont), ctx', state', env', warnings', payments') \<longrightarrow>
  f = True \<longrightarrow>
  ins \<turnstile> (interleaveLinearTrees g1 g2 True, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some (interleaveLinearTrees g1 innerGCont True)), ctx', state', env', warnings', payments')"
"finalLinearGuard ins (g1, ctx, state, env, warnings, payments) \<longrightarrow>
  ins \<turnstile> (g2, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some innerGCont), ctx', state', env', warnings', payments') \<longrightarrow>
  f = True \<longrightarrow>
  ins \<turnstile> (interleaveLinearTreesRightFirst g2 g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some (interleaveLinearTrees g1 innerGCont True)), ctx', state', env', warnings', payments')"
proof (induction g1 g2 f and g1 g2 arbitrary: innerGCont and innerGCont rule: interleaveLinearTrees_interleaveLinearTreesRightFirst.induct)
  case (1 node g2 f)
  then show ?case apply (cases f; cases node; auto)
    by (smt (verit, best) LinearDisjointGuardMatchesG2_no_computation finalLinearGuardLeftImpliesInterleavedNoFlipFinalLinearGuard(2) interleaveLinearTrees.simps(1) interleaveLinearTreesRightFirst.simps(1) leftLinearGuardStepWithContinueImpliesInterleavedLinearGuardStepWithRightInterleavedContinue(2))+
next
  case (2 node gCont g2 f)
  then show ?case apply (cases f; cases node; auto)
    by (smt (verit, best) LinearDisjointGuardMatchesG2_no_computation finalLinearGuardLeftImpliesInterleavedNoFlipFinalLinearGuard(1) interleaveLinearTrees.simps(2) leftLinearGuardStepWithContinueImpliesInterleavedLinearGuardStepWithRightInterleavedContinue(2))+
next
  case (3 innerG1 innerG2 g2 f)
  then show ?case apply auto
    apply (cases f; auto)
    apply (cases g2, auto)
      apply (metis LinearDisjointGuardMatchesG2_no_computation LinearGuardThenGuardStep finalLinearDisjointImpliesBothFinal finalLinearGuardLeftImpliesInterleavedNoFlipFinalLinearGuard(1))
     apply (smt (verit, ccfv_SIG) LinearDisjointGuardMatchesG1 LinearDisjointGuardMatchesG2_no_computation finalLinearDisjointImpliesBothFinal finalLinearGuardLeftImpliesInterleavedNoFlipFinalLinearGuard(1) interleaveLinearTrees.simps(3) leftLinearGuardStepWithContinueImpliesInterleavedLinearGuardStepWithRightInterleavedContinue(2))
  proof -
    fix x31 :: LinearGuardExpression and x32 :: LinearGuardExpression
    assume a1: "ins \<turnstile> (x32, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some innerGCont), ctx', state', env', warnings', payments')"
    assume a2: "finalLinearGuard ins (x31, ctx, state, env, warnings, payments)"
    assume a3: "finalLinearGuard ins (LinearDisjointGuard innerG1 innerG2, ctx, state, env, warnings, payments)"
    have f4: "LinearDisjointGuard (interleaveLinearTrees innerG1 innerGCont False) (LinearDisjointGuard (interleaveLinearTrees innerG2 innerGCont False) (interleaveLinearTreesRightFirst innerGCont (LinearDisjointGuard innerG1 innerG2))) = interleaveLinearTrees (LinearDisjointGuard innerG1 innerG2) innerGCont True"
      by simp
    then have f6: "None = computeLinearGuardStep ins (interleaveLinearTrees innerG1 (LinearDisjointGuard x31 x32) False, ctx, state, env, warnings, payments)"
      using a3 by (metis (no_types) computeLinearGuardStepHaltedImpliesSmallStepHalted finalLinearDisjointImpliesBothFinal finalLinearGuardLeftImpliesInterleavedNoFlipFinalLinearGuard(1))
    have "None = computeLinearGuardStep ins (interleaveLinearTrees innerG2 (LinearDisjointGuard x31 x32) False, ctx, state, env, warnings, payments)"
      using computeLinearGuardStepHaltedImpliesSmallStepHalted a3 by (metis (no_types) finalLinearDisjointImpliesBothFinal finalLinearGuardLeftImpliesInterleavedNoFlipFinalLinearGuard(1))
    then show "ins \<turnstile> (LinearDisjointGuard (interleaveLinearTrees innerG1 (LinearDisjointGuard x31 x32) False) (LinearDisjointGuard (interleaveLinearTrees innerG2 (LinearDisjointGuard x31 x32) False) (LinearDisjointGuard (interleaveLinearTreesRightFirst x31 (LinearDisjointGuard innerG1 innerG2)) (interleaveLinearTreesRightFirst x32 (LinearDisjointGuard innerG1 innerG2)))), ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some (LinearDisjointGuard (interleaveLinearTrees innerG1 innerGCont False) (LinearDisjointGuard (interleaveLinearTrees innerG2 innerGCont False) (interleaveLinearTreesRightFirst innerGCont (LinearDisjointGuard innerG1 innerG2))))), ctx', state', env', warnings', payments')"
      using f6 computeLinearGuardStepHaltedImpliesSmallStepHalted f4 a3 a2 a1 by (metis (no_types) LinearDisjointGuardMatchesG2 leftLinearGuardStepWithContinueImpliesInterleavedLinearGuardStepWithRightInterleavedContinue(2) linearGuardFinalImpliesInterleavedLinearGuardFinal(2))
  qed
next
  case (4 node g2)
  then show ?case apply (cases node; auto)
    by (smt (verit, best) LinearDisjointGuardMatchesG2_no_computation finalLinearGuardLeftImpliesInterleavedNoFlipFinalLinearGuard(2) interleaveLinearTrees.simps(1) interleaveLinearTreesRightFirst.simps(1) leftLinearGuardStepWithContinueImpliesInterleavedLinearGuardStepWithRightInterleavedContinue(2))+
next
  case (5 node gCont g2)
  then show ?case apply (cases node; auto)
    by (smt (verit, best) LinearDisjointGuardMatchesG2_no_computation finalLinearGuardLeftImpliesInterleavedNoFlipFinalLinearGuard(1) interleaveLinearTrees.simps(2) leftLinearGuardStepWithContinueImpliesInterleavedLinearGuardStepWithRightInterleavedContinue(2))+
next
  case (6 innerG1 innerG2 g2)
  then show ?case apply auto
    apply (cases g2, auto)
      apply (metis LinearGuardThenGuardStep)
     apply (smt (verit, ccfv_SIG) LinearDisjointGuardMatchesG1 LinearDisjointGuardMatchesG2_no_computation finalLinearDisjointImpliesBothFinal finalLinearGuardLeftImpliesInterleavedNoFlipFinalLinearGuard(1) interleaveLinearTrees.simps(3) leftLinearGuardStepWithContinueImpliesInterleavedLinearGuardStepWithRightInterleavedContinue(2))
    by (smt (verit, best) LinearDisjointGuardMatchesG2_no_computation interleaveLinearTrees.simps(3) interleaveLinearTreesRightFirst.simps(3) leftLinearGuardStepWithContinueImpliesInterleavedLinearGuardStepWithRightInterleavedContinue(2))
qed

lemma addLinearGuardContinuationStepNonemptyNextGuard:
"(ins \<turnstile> (addLinearGuardContinuation g2 g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, gCont), ctx', state', env', warnings', payments') \<Longrightarrow>
    gCont \<noteq> None)"
proof (induction g2 g1 rule: addLinearGuardContinuation.induct)
  case (1 gCont a stmts)
  then show ?case by auto
next
  case (2 gCont node innerG)
  then show ?case by (cases node, auto)
next
  case (3 gCont g1 g2)
  then show ?case by auto
qed

lemma interleaveLinearTreesStepImpliesLeftOrRightLinearGuardStep:
"ins \<turnstile> (interleaveLinearTrees g1 g2 f, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, gCont), ctx', state', env', warnings', payments') \<longrightarrow>
  (\<exists>innerGCont . ins \<turnstile> (g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, innerGCont), ctx', state', env', warnings', payments') \<or>
    ins \<turnstile> (g2, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, innerGCont), ctx', state', env', warnings', payments'))"
"ins \<turnstile> (interleaveLinearTreesRightFirst g1 g2, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, gCont), ctx', state', env', warnings', payments') \<longrightarrow>
  (\<exists> innerGCont . ins \<turnstile> (g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, innerGCont), ctx', state', env', warnings', payments') \<or>
    ins \<turnstile> (g2, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, innerGCont), ctx', state', env', warnings', payments'))"
proof (induction g1 g2 f and g1 g2 rule: interleaveLinearTrees_interleaveLinearTreesRightFirst.induct)
  case (1 node g2 f)
  then show ?case apply (cases f; cases node; auto)
    using LinearGuardNodeStep by fastforce+
next
  case (2 node gCont g2 f)
  then show ?case apply (cases f; cases node; auto)
    by (meson LinearGuardThenGuardStep)+
next
  case (3 innerG1 innerG2 g2 f)
  then show ?case apply (cases f; auto)
    by (meson LinearDisjointGuardMatchesG1 LinearDisjointGuardMatchesG2_no_computation finalInterleaveTreesImpliesBothFinal(1))+
next
  case (4 node g2)
  then show ?case apply (cases node; auto)
    using LinearGuardNodeStep by fastforce+
next
  case (5 node gCont g2)
  then show ?case apply (cases node; auto)
    by (meson LinearGuardThenGuardStep)+
next
  case (6 innerG1 innerG2 g2)
  then show ?case apply (auto)
    by (meson LinearDisjointGuardMatchesG1 LinearDisjointGuardMatchesG2_no_computation finalInterleaveTreesImpliesBothFinal(2))+
qed

lemma addStatementsContinuationNilNoEffect:
"addStatementsContinuation [] g = g"
  by (induction g, auto)

lemma guardStepImpliesLinearGuardStep:
"ins \<turnstile> (g, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, None), ctx', state', env', warnings', payments') \<Longrightarrow>
   ins \<turnstile> ((linearizeGuardExpression g), ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, None), ctx', state', env', warnings', payments')"
proof (induction g arbitrary: stmts rule: linearizeGuardExpression.induct)
  case (1 a)
  then show ?case by (cases rule: small_step_reduce_guard.cases, auto simp add: LinearGuardNodeStep)
next
  case (2 g1 g2)
  then show ?case by auto
next
  case (3 g1 innerStmts)
  then show ?case apply auto
    by (cases rule: small_step_reduce_guard.cases, auto simp add: linearGuardStepImpliesAddStatementsContinuationStep)
next
  case (4 g1 g2)
  then show ?case apply auto
     apply (cases rule: small_step_reduce_guard.cases, auto)
    using LinearDisjointGuardMatchesG1 DisjointGuardMatchesG1 DisjointGuardMatchesG2 LinearDisjointGuardMatchesG2_no_computation finalGuardImpliesFinalLinearGuard by presburger+
next
  case (5 g1 g2)
  then show ?case apply simp
    apply (cases "ins" "(InterleavedGuard g1 g2, ctx, state, env, warnings, payments)" "((stmts, None::FGuardExpression option), ctx', state', env', warnings', payments')" rule: small_step_reduce_guard.cases)
    by auto
qed

lemma finalLinearGuardImpliesFinalGuard:
"finalLinearGuard ins (linearizeGuardExpression g, ctx, state, env, warnings, payments) \<Longrightarrow> finalGuard ins (g, ctx, state, env, warnings, payments)"
proof (induction g rule: linearizeGuardExpression.induct)
  case (1 a)
  then show ?case apply (auto simp add: finalGuard_def finalLinearGuard_def)
    apply (elim ActionGuardE, auto)
    using LinearGuardNodeStep by metis
next
  case (2 g1 g2)
  then show ?case apply auto
    by (metis (no_types, lifting) addLinearGuardContinuationFinalImpliesFinalLinearGuard computeGuardStep.simps(2) computeGuardStepHaltedImpliesSmallStepHalted option.simps(4))
next
  case (3 g innerStmts)
  then show ?case apply auto
    by (metis (no_types, lifting) addStatementsContinuationFinalImpliesFinalLinearGuard computeGuardStep.simps(3) computeGuardStepHaltedImpliesSmallStepHalted option.simps(4))
next
  case (4 g1 g2)
  then show ?case apply auto
    by (meson DisjointGuardE finalGuard_def finalLinearDisjointImpliesBothFinal)
next
  case (5 g1 g2)
  then show ?case apply auto
    by (metis (no_types, lifting) computeGuardStep.simps(5) computeGuardStepHaltedImpliesSmallStepHalted finalInterleaveTreesImpliesBothFinal(1) option.case_eq_if)
qed

lemma linearGuardStepImpliesGuardStep:
"ins \<turnstile> (linearizeGuardExpression g, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, None), ctx', state', env', warnings', payments') \<Longrightarrow>
  ins \<turnstile> (g, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, None), ctx', state', env', warnings', payments')"
proof (induction g arbitrary: stmts rule: linearizeGuardExpression.induct)
  case (1 a)
  then show ?case apply auto
    by (auto simp add: ActionGuardStep)
next
  case (2 g1 g2)
  then show ?case using addLinearGuardContinuationStepNonemptyNextGuard by fastforce
next
  case (3 g innerStmts)
  moreover obtain baseStmts baseGCont where "ins \<turnstile> (linearizeGuardExpression g, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((baseStmts, baseGCont), ctx', state', env', warnings', payments')" using calculation apply auto
    using addStatementsContinuationStepImpliesLinearGuardStep by blast
  moreover have "baseGCont = None" using deterministicLinearGuardExpressionsSemantics linearGuardStepImpliesAddStatementsContinuationStepGeneral calculation apply auto
    by (metis not_Some_eq)
  moreover have "ins \<turnstile> (g, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((baseStmts, None), ctx', state', env', warnings', payments')" using calculation by auto
  moreover have "stmts = baseStmts @ innerStmts" using calculation linearGuardStepImpliesAddStatementsContinuationStepGeneral deterministicLinearGuardExpressionsSemantics apply auto
    by metis
  ultimately show ?case by (metis GuardStmtsGuardCompleteGuardStep)
next
  case (4 g1 g2)
  then show ?case apply auto
    using DisjointGuardMatchesG1 apply presburger
    using DisjointGuardMatchesG2_no_computation finalLinearGuardImpliesFinalGuard by presburger
next
  case (5 g1 g2)
  then show ?case using linearGuardStepNoContinuationGuardImpliesNotInterleaved(1) by auto
qed

lemma guardStepWithContinuationImpliesLinearGuardStepWithContinuation:
"ins \<turnstile> (g, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some gCont), ctx', state', env', warnings', payments') \<Longrightarrow>
   ins \<turnstile> ((linearizeGuardExpression g), ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some (linearizeGuardExpression gCont)), ctx', state', env', warnings', payments')"
proof (induction g arbitrary: gCont stmts rule: linearizeGuardExpression.induct)
  case (1 a)
  then show ?case by (cases rule: small_step_reduce_guard.cases, auto)
next
  case (2 g1 g2)
  then show ?case apply auto
    by (cases rule: small_step_reduce_guard.cases, auto simp add: LinearDisjointGuardMatchesG1 guardStepImpliesLinearGuardStep linearGuardStepImpliesAddContinuationStep linearGuardStepWithContinuationImpliesAddContinuationStepWithContinuation)
next
  case (3 g1 innerStmts)
  then show ?case apply auto
    by (cases rule: small_step_reduce_guard.cases, auto simp add: linearGuardStepImpliesAddStatementsContinuationStep linearGuardStepWithContinuationImpliesAddStatementsContinuationStepWithContinuation)
next
  case (4 g1 g2)
  then show ?case apply auto
    by (cases rule: small_step_reduce_guard.cases, auto simp add: finalGuardImpliesFinalLinearGuard linearGuardStepImpliesAddStatementsContinuationStep linearGuardStepWithContinuationImpliesAddStatementsContinuationStepWithContinuation LinearDisjointGuardMatchesG1 guardStepImpliesLinearGuardStep linearGuardStepImpliesAddContinuationStep LinearDisjointGuardMatchesG2_no_computation)
next
  case (5 g1 g2)
  then show ?case apply simp
    apply (cases ins "(InterleavedGuard g1 g2, ctx, state, env, warnings, payments)" "((stmts, Some gCont), ctx', state', env', warnings', payments')" rule: small_step_reduce_guard.cases)
    by (auto simp add: rightLinearGuardStepWithContinueImpliesInterleavedLinearGuardStepWithLeftContinueInterleaved guardStepImpliesLinearGuardStep leftLinearGuardStepNoContinueImpliesInterleavedLinearGuardStepWithRightContinue(1) leftLinearGuardStepWithContinueImpliesInterleavedLinearGuardStepWithRightInterleavedContinue(1) computeGuardStepHaltedImpliesSmallStepHalted finalGuardImpliesFinalLinearGuard rightLinearGuardStepNoContinueImpliesInterleavedLinearGuardStepWithLeftContinue(1))
qed

lemma linearGuardStepWithContinuationImpliesGuardStepWithContinuation:
"ins \<turnstile> (linearizeGuardExpression g, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some lgCont), ctx', state', env', warnings', payments') \<Longrightarrow>
  (\<exists> gCont . ins \<turnstile> (g, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, Some gCont), ctx', state', env', warnings', payments'))"
proof (induction g arbitrary: stmts lgCont rule: linearizeGuardExpression.induct)
  case (1 a)
  then show ?case by (auto simp add: ActionGuardStep)
next
  case (2 g1 g2)
  then show ?case apply auto
    by (metis GuardThenGuardCompleteFirstGuardStep GuardThenGuardIncompleteGuard addLinearGuardContinuationStepImpliesLinearGuardStep linearGuardStepImpliesGuardStep option.exhaust)
next
  case (3 g innerStmts)
  moreover obtain innerBoundStmts innerLGOpt where "ins \<turnstile> (linearizeGuardExpression g, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((innerBoundStmts, innerLGOpt), ctx', state', env', warnings', payments')" using calculation apply auto
    using addStatementsContinuationStepImpliesLinearGuardStep by metis
  moreover have "innerBoundStmts = stmts" using calculation linearGuardStepImpliesAddStatementsContinuationStepGeneral deterministicLinearGuardExpressionsSemantics apply auto
    by (metis linearGuardStepImpliesAddStatementsContinuationStepGeneral)
  moreover obtain innerLG where "innerLGOpt = Some innerLG \<and> lgCont = addStatementsContinuation innerStmts innerLG" using calculation linearGuardStepImpliesAddStatementsContinuationStepGeneral deterministicLinearGuardExpressionsSemantics linearGuardStepImpliesAddStatementsContinuationStepGeneral apply auto
    by (metis (no_types, lifting) option.exhaust_sel option.sel)
  moreover obtain arbStmts arbGCont where "ins \<turnstile> (g, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((arbStmts, Some arbGCont), ctx', state', env', warnings', payments')" using calculation by blast
  moreover have "stmts = arbStmts" using guardStepWithContinuationImpliesLinearGuardStepWithContinuation deterministicGuardExpressionsSemantics deterministicLinearGuardExpressionsSemantics apply auto
    by (metis calculation(3) calculation(4) calculation(6))
  ultimately show ?case apply auto
    by (meson GuardStmtsGuardIncompleteGuard)
next
  case (4 g1 g2)
  then show ?case apply auto
    by (meson DisjointGuardMatchesG1 DisjointGuardMatchesG2_no_computation finalLinearGuardImpliesFinalGuard)+
next
  case (5 g1 g2)
  moreover obtain innerLGCont where "(ins \<turnstile> (linearizeGuardExpression g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, innerLGCont), ctx', state', env', warnings', payments')) \<or>
    (ins \<turnstile> (linearizeGuardExpression g2, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, innerLGCont), ctx', state', env', warnings', payments'))"
    using interleaveLinearTreesStepImpliesLeftOrRightLinearGuardStep(1)[of ins "linearizeGuardExpression g1" "linearizeGuardExpression g2" True ctx state env warnings payments stmts "Some lgCont" ctx' state' env' warnings' payments'] calculation(3) by auto
  moreover obtain innerGCont where "(ins \<turnstile> (g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, innerGCont), ctx', state', env', warnings', payments')) \<or>
    (ins \<turnstile> (g2, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, innerGCont), ctx', state', env', warnings', payments'))" using calculation apply (cases innerLGCont, auto)
    by (meson linearGuardStepImpliesGuardStep)+
  ultimately show ?case apply (cases innerGCont, auto)
                   apply (meson finalGuard_def finalGuardImpliesFinalLinearGuard finalLinearGuard_def InterleavedGuardMatchesG2Complete_no_computation InterleavedGuardMatchesG1Complete)+
         apply (metis InterleavedGuardMatchesG1Complete InterleavedGuardMatchesG1Incomplete linearGuardStepImpliesGuardStep option.collapse)
        apply (cases "finalGuard ins (g1, ctx, state, env, warnings, payments)")
         apply (meson InterleavedGuardMatchesG2Complete_no_computation) defer
        apply (metis InterleavedGuardMatchesG1Incomplete)
       apply (meson InterleavedGuardMatchesG1Incomplete)
      apply (metis InterleavedGuardMatchesG1Complete InterleavedGuardMatchesG1Incomplete linearGuardStepImpliesGuardStep not_Some_eq)
     apply (cases "finalGuard ins (g1, ctx, state, env, warnings, payments)")
      apply (meson InterleavedGuardMatchesG2Incomplete_no_computation)
     apply (auto simp add: finalGuard_def)
    subgoal premises ps proof -
      obtain g1Stmts g1Cont g1Ctx g1State g1Env g1Warnings g1Payments where "ins \<turnstile> (g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((g1Stmts, g1Cont), g1Ctx, g1State, g1Env, g1Warnings, g1Payments)" using ps by auto
      moreover obtain lg1Cont where "ins \<turnstile> (linearizeGuardExpression g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((g1Stmts, lg1Cont), g1Ctx, g1State, g1Env, g1Warnings, g1Payments)" using calculation guardStepImpliesLinearGuardStep guardStepWithContinuationImpliesLinearGuardStepWithContinuation option.exhaust by metis
      moreover obtain interleavedLG1Cont where "ins \<turnstile> (interleaveLinearTrees (linearizeGuardExpression g1) (linearizeGuardExpression g2) True, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((g1Stmts, interleavedLG1Cont), g1Ctx, g1State, g1Env, g1Warnings, g1Payments)" using calculation(2) apply (cases lg1Cont, auto)
         apply (metis InterleavedGuardMatchesG1Complete guardStepWithContinuationImpliesLinearGuardStepWithContinuation linearGuardStepImpliesGuardStep linearizeGuardExpression.simps(5))
        by (meson leftLinearGuardStepWithContinueImpliesInterleavedLinearGuardStepWithRightInterleavedContinue(1))
      moreover have "g1Stmts = stmts \<and> g1Ctx = ctx' \<and> g1State = state' \<and> g1Env = env' \<and> g1Warnings = warnings' \<and> g1Payments = payments'" using ps calculation deterministicLinearGuardExpressionsSemantics by auto
      ultimately show ?thesis apply (cases g1Cont)
        by (meson InterleavedGuardMatchesG1Complete InterleavedGuardMatchesG1Incomplete)+
    qed
    subgoal premises ps proof -
      obtain g1Stmts g1Cont g1Ctx g1State g1Env g1Warnings g1Payments where "ins \<turnstile> (g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((g1Stmts, g1Cont), g1Ctx, g1State, g1Env, g1Warnings, g1Payments)" using ps by auto
      moreover obtain lg1Cont where "ins \<turnstile> (linearizeGuardExpression g1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((g1Stmts, lg1Cont), g1Ctx, g1State, g1Env, g1Warnings, g1Payments)" using calculation guardStepImpliesLinearGuardStep guardStepWithContinuationImpliesLinearGuardStepWithContinuation option.exhaust by metis
      moreover obtain interleavedLG1Cont where "ins \<turnstile> (interleaveLinearTrees (linearizeGuardExpression g1) (linearizeGuardExpression g2) True, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((g1Stmts, interleavedLG1Cont), g1Ctx, g1State, g1Env, g1Warnings, g1Payments)" using calculation(2) apply (cases lg1Cont, auto)
         apply (metis InterleavedGuardMatchesG1Complete guardStepWithContinuationImpliesLinearGuardStepWithContinuation linearGuardStepImpliesGuardStep linearizeGuardExpression.simps(5))
        by (meson leftLinearGuardStepWithContinueImpliesInterleavedLinearGuardStepWithRightInterleavedContinue(1))
      moreover have "g1Stmts = stmts \<and> g1Ctx = ctx' \<and> g1State = state' \<and> g1Env = env' \<and> g1Warnings = warnings' \<and> g1Payments = payments'" using ps calculation deterministicLinearGuardExpressionsSemantics by auto
      ultimately show ?thesis apply (cases g1Cont)
        by (meson InterleavedGuardMatchesG1Complete InterleavedGuardMatchesG1Incomplete)+
    qed
    done
qed

lemma linearGuardStepImpliesGuardStepGeneral:
"ins \<turnstile> (linearizeGuardExpression g, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, lgCont), ctx', state', env', warnings', payments') \<Longrightarrow>
  (\<exists> gCont . ins \<turnstile> (g, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, gCont), ctx', state', env', warnings', payments') \<and>
    (lgCont = None \<longrightarrow> gCont = None) \<and>
    (lgCont = Some innerLGCont \<longrightarrow> linearizeGuardExpression (the gCont) = innerLGCont))"
subgoal premises ps proof (cases lgCont)
  case None
  moreover obtain gCont where "ins \<turnstile> (g, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, gCont), ctx', state', env', warnings', payments')" using calculation linearGuardStepImpliesGuardStep ps by blast
  moreover have "gCont = None" using ps calculation linearGuardStepImpliesGuardStep apply auto
    by (metis (no_types, opaque_lifting) InterleavedGuardMatchesG1Complete deterministicLinearGuardExpressionsSemantics guardStepWithContinuationImpliesLinearGuardStepWithContinuation linearGuardStepNoContinuationGuardImpliesNotInterleaved(1) linearizeGuardExpression.simps(5) not_Some_eq)
  ultimately show ?thesis using ps by auto
next
  case (Some innerLGCont)
  moreover obtain gCont where "ins \<turnstile> (g, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, gCont), ctx', state', env', warnings', payments')" using calculation linearGuardStepWithContinuationImpliesGuardStepWithContinuation ps by blast
  moreover obtain innerGCont where "gCont = Some innerGCont" by (metis (no_types, opaque_lifting) Some calculation(2) deterministicLinearGuardExpressionsSemantics guardStepImpliesLinearGuardStep leftLinearGuardStepNoContinueImpliesInterleavedLinearGuardStepWithRightContinue(1) linearGuardStepNoContinuationGuardImpliesNotInterleaved(1) option.collapse ps)
  moreover have "innerLGCont = linearizeGuardExpression innerGCont" using calculation ps deterministicGuardExpressionsSemantics deterministicLinearGuardExpressionsSemantics guardStepWithContinuationImpliesLinearGuardStepWithContinuation by (auto, meson option.inject)
  ultimately show ?thesis by auto
qed done

lemma guardStepImpliesLinearGuardStepGeneral:
"ins \<turnstile> (g, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, gCont), ctx', state', env', warnings', payments') \<Longrightarrow>
   (\<exists>lgCont . ins \<turnstile> ((linearizeGuardExpression g), ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, lgCont), ctx', state', env', warnings', payments') \<and>
    (gCont = None \<longrightarrow> lgCont = None) \<and>
    (gCont = Some innerGCont \<longrightarrow> lgCont = Some (linearizeGuardExpression innerGCont)))"
  apply (cases gCont, auto simp add: guardStepImpliesLinearGuardStep)
  using guardStepWithContinuationImpliesLinearGuardStepWithContinuation by blast

lemma casesStepImpliesAppendCasesStep:
"ins \<turnstile> (c1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (casesRes, ctx', state', env', warnings', payments') \<Longrightarrow>
  ins \<turnstile> (c1 @ c2, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (casesRes, ctx', state', env', warnings', payments')"
  apply (induction ins c1 ctx state env warnings payments casesRes ctx' state' env' warnings' payments' rule: small_step_reduce_cases.induct[split_format(complete)])
  by (simp_all add: CasesHeadMatches CasesTailMatches)

lemma finalCasesTailNoDef:
"\<not> (\<exists>res . ins \<turnstile> (c#c1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 res) \<Longrightarrow>
  (\<exists>innerRes . ins \<turnstile> (c1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 innerRes) \<Longrightarrow>
  False"
  apply (cases "(\<exists>innerRes2 . ins \<turnstile> (c, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 innerRes2)")
   apply (simp add: computeCasesStepResImpliesReduceStep reduceCaseStepImpliesComputeCaseStepRes)
  subgoal premises ps proof -
    obtain innerRes2 where "ins \<turnstile> (c, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 innerRes2" using ps by blast
    then have "ins \<turnstile> (c # c1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 innerRes2" using CasesHeadMatches ps deterministicCaseSemantics by blast
    then show ?thesis using ps \<open>ins \<turnstile> (c, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sub>f\<^sub>2 innerRes2\<close> deterministicCaseSemantics by blast
  qed
  subgoal premises ps proof -
    obtain innerRes where "ins \<turnstile> (c1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 innerRes" using ps by blast
    then have "ins \<turnstile> (c # c1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 innerRes" using ps CasesTailMatches by (auto, meson)
    then show ?thesis using ps by blast
  qed
  done

lemma finalCasesTail:
"finalCases ins (c#c1, ctx, state, env, warnings, payments) \<Longrightarrow>
  finalCases ins (c1, ctx, state, env, warnings, payments)"
  by (meson finalCasesTailNoDef finalCases_def)

lemma finalCasesAppend:
"finalCases ins (c1, ctx, state, env, warnings, payments) \<Longrightarrow>
  finalCases ins (c2, ctx, state, env, warnings, payments) \<Longrightarrow>
  finalCases ins (c1 @ c2, ctx, state, env, warnings, payments)"
  apply (auto simp add: finalCases_def)
  apply (induction c1 arbitrary:c2, auto)
  subgoal premises ps proof -
    obtain a c1 c2 aa aaa ab ac ad b where
      "ins \<turnstile> (a # c1 @ c2, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (aa, aaa, ab, ac, ad, b) \<and>
      (\<forall>aa ab ac ad ae b. \<not> ins \<turnstile> (a # c1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (aa, ab, ac, ad, ae, b)) \<and>
      (\<forall>a aa ab ac ad b. \<not> ins \<turnstile> (c2, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (a, aa, ab, ac, ad, b)) \<and>
      (\<forall>a aa ab ac ad b c2.
           (\<forall>a aa ab ac ad b. \<not> ins \<turnstile> (c1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (a, aa, ab, ac, ad, b)) \<longrightarrow>
           (\<forall>a aa ab ac ad b. \<not> ins \<turnstile> (c2, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (a, aa, ab, ac, ad, b)) \<longrightarrow>
           ins \<turnstile> (c1 @ c2, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (a, aa, ab, ac, ad, b) \<longrightarrow> False)" using ps by (auto, meson)
    moreover have "finalCases ins (a#c1, ctx, state, env, warnings, payments)" using ps calculation finalCases_def by auto
    moreover have "finalCases ins (c1, ctx, state, env, warnings, payments)" using ps calculation finalCasesTail by auto
    moreover have "finalCases ins (c2, ctx, state, env, warnings, payments)" using ps calculation finalCases_def by auto
    moreover have "finalCases ins (c1 @ c2, ctx, state, env, warnings, payments)" using calculation finalCases_def by auto
    ultimately show ?thesis using ps apply auto
       apply (meson finalCases_def)
      by (metis (no_types, opaque_lifting) computeCasesStep.simps(2) computeCasesStepResImpliesReduceStep not_Some_eq option.simps(4) option.simps(5) reduceCasesStepImpliesComputeCaseStepRes)
  qed
  done

lemma casesStepAppendFinalCases:
"finalCases ins (c2, ctx, state, env, warnings, payments) \<Longrightarrow>
  ins \<turnstile> (c1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 res \<Longrightarrow>
  ins \<turnstile> (c2 @ c1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 res"
proof (induction c2 arbitrary: c1)
  case Nil
  then show ?case by auto
next
  case (Cons a c2)
  moreover have "finalCases ins (c2, ctx, state, env, warnings, payments)" using calculation finalCasesTail by auto
  moreover have "ins \<turnstile> (c2 @ c1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 res" using calculation by auto
  ultimately show ?case apply (auto simp add: finalCases_def)
    by (smt (verit, ccfv_threshold) CasesHeadMatches CasesTailMatches prod_cases6)
qed

thm small_step_reduce_cases.induct[split_format(complete)]

lemma casesStepDropFinalCases:
"ins \<turnstile> (c1 @ c2, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC, newCtx, newState, newEnv, newWarnings, newPayments) \<Longrightarrow>
  finalCases ins (c2, ctx, state, env, warnings, payments) \<Longrightarrow>
  ins \<turnstile> (c1, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (newC, newCtx, newState, newEnv, newWarnings, newPayments)"
proof (induction ins "c1@c2" ctx state env warnings payments newC newCtx newState newEnv newWarnings newPayments arbitrary: c1 c2 rule: small_step_reduce_cases.induct[split_format(complete)])
  case (CasesHeadMatches ins c ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments rest)
  then show ?case by (smt (verit, ccfv_threshold) append_eq_Cons_conv finalCases_def small_step_reduce_cases.CasesHeadMatches)
next
  case (CasesTailMatches ins rest ctx s env warnings payments newC newCtx newS newEnv newWarnings newPayments c)
  then show ?case by (smt (verit, ccfv_SIG) append_eq_Cons_conv append_self_conv2 finalCasesTail small_step_reduce_cases.CasesTailMatches)
qed

lemma finalLinearGuardExpressionImpliesFinalCases:
"finalLinearGuard ins (g, ctx, state, env, warnings, payments) \<Longrightarrow>
  finalCases ins (linearGuardExpressionToCases g cont t tcont, ctx, state, env, warnings, payments)"
proof (induction g cont t tcont rule: linearGuardExpressionToCases.induct)
  case (1 a stmts cont t tcont)
  then show ?case apply (auto simp add: finalLinearGuard_def finalCases_def)
    apply (elim SingleCaseCasesStepE, auto)
    using LinearGuardNodeStep apply (elim CaseStepE; elim ActionGuardE, auto; metis)
    using reduceCasesStepImpliesComputeCaseStepRes by fastforce
next
  case (2 a stmts innerG cont t tcont)
  then show ?case apply (auto simp add: finalLinearGuard_def finalCases_def)
    apply (elim SingleCaseCasesStepE, auto)+
     apply (elim CaseStepE, auto)+
     apply (elim ActionGuardE, auto)+
     apply (meson LinearGuardThenGuardStep)
    using reduceCasesStepImpliesComputeCaseStepRes by fastforce
next
  case (3 g1 g2 cont t tcont)
  then show ?case apply (auto simp add: finalLinearGuard_def finalCases_def)
    by (meson "3.IH"(1) "3.IH"(2) "3.prems" finalCasesAppend finalCases_def finalLinearDisjointImpliesBothFinal)
qed

lemma linearGuardExpressionsStepImpliesCasesStep:
"ins \<turnstile> (g, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, nextGuard), ctx', state', env', warnings', payments') \<Longrightarrow>
  (\<exists>casesRes . ins \<turnstile> (linearGuardExpressionToCases g cont t tcont, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (casesRes, ctx', state', env', warnings', payments'))"
proof (induction g cont t tcont rule: linearGuardExpressionToCases.induct)
  case (1 a stmts cont t tcont)
  then show ?case apply auto
    apply (elim LinearGuardNodeE, auto)
    by (meson ActionGuardStep CaseGuardCompleteStep CasesHeadMatches)
next
  case (2 a stmts innerG cont t tcont)
  then show ?case apply auto
    by (meson ActionGuardStep CaseGuardCompleteStep CasesHeadMatches)
next
  case (3 g1 g2 cont t tcont)
  then show ?case apply auto
     apply (meson casesStepImpliesAppendCasesStep)
    by (meson casesStepAppendFinalCases finalLinearGuardExpressionImpliesFinalCases)
qed

lemma linearGuardExpressionsStepImpliesCasesStepContinuationCorrect:
"ins \<turnstile> (g, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, nextGuard), ctx', state', env', warnings', payments') \<Longrightarrow>
  ins \<turnstile> (linearGuardExpressionToCases g cont t tcont, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (casesRes, ctx', state', env', warnings', payments') \<Longrightarrow>
  (nextGuard = None \<longrightarrow> casesRes t tcont = prependStatements stmts cont) \<and>
  (nextGuard = Some innerNextGuard \<longrightarrow> casesRes t tcont = prependStatements stmts (When (linearGuardExpressionToCases innerNextGuard cont t tcont) t tcont))"
proof (induction g cont t tcont rule: linearGuardExpressionToCases.induct)
  case (1 a stmts cont t tcont)
  then show ?case apply auto
    apply (elim SingleCaseCasesStepE, auto)
     apply (elim CaseStepE, auto)
     apply (elim ActionGuardE, auto)
    using reduceCasesStepImpliesComputeCaseStepRes by fastforce
next
  case (2 a stmts innerG cont t tcont)
  then show ?case apply auto
    apply (elim SingleCaseCasesStepE, auto)
     apply (elim CaseStepE, auto)
    using reduceCasesStepImpliesComputeCaseStepRes by fastforce
next
  case (3 g1 g2 cont t tcont)
  then show ?case apply auto
       apply (metis casesStepImpliesAppendCasesStep deterministicCasesSemantics linearGuardExpressionsStepImpliesCasesStep)
      apply (metis casesStepImpliesAppendCasesStep deterministicCasesSemantics linearGuardExpressionsStepImpliesCasesStep)
    by (metis casesStepAppendFinalCases deterministicCasesSemantics finalLinearGuardExpressionImpliesFinalCases linearGuardExpressionsStepImpliesCasesStep)+
qed

lemma casesStepImpliesLinearGuardExpressionsStep:
"(ins \<turnstile> (linearGuardExpressionToCases g cont t tcont, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (casesRes, ctx', state', env', warnings', payments')) \<Longrightarrow>
  (\<exists>stmts nextGuard . ins \<turnstile> (g, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, nextGuard), ctx', state', env', warnings', payments'))"
  subgoal premises ps proof -
    from ps have "\<not>finalLinearGuard ins (g, ctx, state, env, warnings, payments)" by (meson finalCases_def finalLinearGuardExpressionImpliesFinalCases)
    then obtain stmts nextGuard lCtx lState lEnv lWarnings lPayments where "ins \<turnstile> (g, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>l\<^sup>g\<^sub>f\<^sub>2 ((stmts, nextGuard), lCtx, lState, lEnv, lWarnings, lPayments)" using finalLinearGuard_def by auto
    then show ?thesis by (metis ps deterministicCasesSemantics linearGuardExpressionsStepImpliesCasesStep old.prod.inject)
  qed
  done

lemma guardExpressionsStepImpliesCasesStep:
"ins \<turnstile> (g, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, nextGuard), ctx', state', env', warnings', payments') \<Longrightarrow>
  (\<exists>casesRes . ins \<turnstile> (linearGuardExpressionToCases (linearizeGuardExpression g) cont t tcont, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (casesRes, ctx', state', env', warnings', payments'))"
  apply (induction g rule: linearizeGuardExpression.induct)
  by (meson guardStepImpliesLinearGuardStepGeneral linearGuardExpressionsStepImpliesCasesStep)+

lemma finalGuardExpressionImpliesFinalCases:
"finalGuard ins (g, ctx, state, env, warnings, payments) \<Longrightarrow>
  finalCases ins (linearGuardExpressionToCases (linearizeGuardExpression g) cont t tcont, ctx, state, env, warnings, payments)"
  apply (induction g rule: linearizeGuardExpression.induct)
  by (meson finalGuardImpliesFinalLinearGuard finalLinearGuardExpressionImpliesFinalCases)+

lemma guardExpressionsStepImpliesCasesStepContinuationCorrect:
"ins \<turnstile> (g, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, nextGuard), ctx', state', env', warnings', payments') \<Longrightarrow>
  (\<exists>casesRes . ins \<turnstile> (linearGuardExpressionToCases (linearizeGuardExpression g) cont t tcont, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (casesRes, ctx', state', env', warnings', payments') \<and>
  (nextGuard = None \<longrightarrow> casesRes t tcont = prependStatements stmts cont) \<and>
  (nextGuard = Some innerNextGuard \<longrightarrow> casesRes t tcont = prependStatements stmts (When (linearGuardExpressionToCases (linearizeGuardExpression (the nextGuard)) cont t tcont) t tcont)))"
  apply (induction g rule: linearizeGuardExpression.induct)
      apply (metis guardExpressionsStepImpliesCasesStep guardStepImpliesLinearGuardStepGeneral linearGuardExpressionsStepImpliesCasesStepContinuationCorrect option.sel)
     apply (metis guardExpressionsStepImpliesCasesStep guardStepImpliesLinearGuardStepGeneral linearGuardExpressionsStepImpliesCasesStepContinuationCorrect option.sel)
    apply (metis guardExpressionsStepImpliesCasesStep guardStepImpliesLinearGuardStep guardStepWithContinuationImpliesLinearGuardStepWithContinuation linearGuardExpressionsStepImpliesCasesStepContinuationCorrect option.sel)
   apply (metis DisjointGuardE casesStepAppendFinalCases casesStepImpliesAppendCasesStep finalGuardImpliesFinalLinearGuard finalLinearGuardExpressionImpliesFinalCases linearGuardExpressionToCases.simps(3) linearizeGuardExpression.simps(4))
  by (metis guardExpressionsStepImpliesCasesStep guardStepImpliesLinearGuardStep guardStepWithContinuationImpliesLinearGuardStepWithContinuation linearGuardExpressionsStepImpliesCasesStepContinuationCorrect option.sel)

lemma casesStepImpliesGuardExpressionsStep:
"ins \<turnstile> (linearGuardExpressionToCases (linearizeGuardExpression g) cont t tcont, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>c\<^sup>a\<^sup>s\<^sup>e\<^sup>s\<^sub>f\<^sub>2 (casesRes, ctx', state', env', warnings', payments') \<Longrightarrow>
  (\<exists>stmts nextGuard . ins \<turnstile> (g, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, nextGuard), ctx', state', env', warnings', payments'))"
  subgoal premises ps proof -
    from ps have "\<not>finalGuard ins (g, ctx, state, env, warnings, payments)" by (meson finalCases_def finalGuardExpressionImpliesFinalCases)
    then obtain stmts nextGuard lCtx lState lEnv lWarnings lPayments where "ins \<turnstile> (g, ctx, state, env, warnings, payments) \<rightarrow>\<^sup>g\<^sub>f\<^sub>2 ((stmts, nextGuard), lCtx, lState, lEnv, lWarnings, lPayments)" using finalGuard_def by auto
    then show ?thesis by (metis ps deterministicCasesSemantics guardExpressionsStepImpliesCasesStep old.prod.inject)
  qed
  done

end