theory FaustusV2Compiler
  imports Main Util.MList Util.SList Core.ListTools Core.Semantics SmallStep "HOL-Library.Product_Lexorder" "HOL.Product_Type" "HOL-IMP.Star" FaustusTypes FaustusSemantics FaustusV2AST FaustusV2TypeChecker FaustusV2Semantics
begin


lemma reduceAllImpliesNotFinal:
"c1 \<Down>\<^sub>f\<^sub>2 c2 \<Longrightarrow> \<not> finalV2 c1"
  by (meson finalV2_def small_step_reduce_all.cases)

lemma finalImpliesNoReduceAll:
"finalV2 c1 \<Longrightarrow> \<not> (c1 \<Down>\<^sub>f\<^sub>2 c2)"
  using reduceAllImpliesNotFinal by metis

lemma reduceAllImpliesReduceTrans:
"f1 \<Down>\<^sub>f\<^sub>2 f2 \<Longrightarrow> f1 \<longrightarrow>\<^sub>f\<^sub>2 f2"
  apply (induction rule: small_step_reduce_all.induct)
  using small_step_reduce_trans.TransSingleStep small_step_reduce_trans.TransMultipleStep by auto

lemma smallStepReduceNotReflexive:
"f1 \<rightarrow>\<^sub>f\<^sub>2 f2 \<Longrightarrow> f1 \<noteq> f2"
  apply  (induction f1 f2 rule: small_step_reduce.induct)
  by auto

lemma smallStepReduceAddsSingleWarning:
"(contract, ctx, state, env, warnings, payments) \<rightarrow>\<^sub>f\<^sub>2 (newContract, newCtx, newState, newEnv, newWarnings, newPayments) \<Longrightarrow>
  length newWarnings = length warnings + 1"
  apply (induction rule: small_step_reduce_induct)
  by auto

lemma smallStepReduceStarIncreasesWarnings:
"(contract, ctx, state, env, warnings, payments) \<rightarrow>\<^sub>f\<^sub>2* (newContract, newCtx, newState, newEnv, newWarnings, newPayments) \<Longrightarrow>
  length newWarnings \<ge> length warnings"
proof (induction contract ctx state env warnings payments newContract newCtx newState newEnv newWarnings newPayments rule: star.induct[of small_step_reduce, split_format(complete)])
  case (refl contract ctx state env warnings payments)
  then show ?case by auto
next
  case (step contract ctx state env warnings payments newContract newCtx newState newEnv newWarnings newPayments finalContract finalCtx finalState finalEnv finalWarnings finalPayments)
  then show ?case using smallStepReduceAddsSingleWarning by auto
qed

lemma smallStepReduceNoBacktrack:
"f2 \<rightarrow>\<^sub>f\<^sub>2* f3 \<Longrightarrow> (\<forall>f1 . f1 \<rightarrow>\<^sub>f\<^sub>2 f2 \<longrightarrow> f3 \<noteq> f1)"
proof(induction f2 f3  rule: star.induct[of small_step_reduce])
  case (refl x)
  then show ?case apply auto
    using smallStepReduceNotReflexive by blast
next
  case (step x y z)
  then show ?case using smallStepReduceStarIncreasesWarnings smallStepReduceAddsSingleWarning apply auto
    by (smt (verit, ccfv_threshold) Suc_leD Suc_n_not_le_n prod_cases6)
qed

lemma smallStepReflexiveTransitiveIsFinite:
"\<exists>f2Set . (f1 \<rightarrow>\<^sub>f\<^sub>2* f2 \<longrightarrow> f2 \<in> f2Set) \<longrightarrow> finite f2Set"
  by auto

lemma smallStepTransitiveIsFinite:
"\<exists>f2Set . (f1 \<longrightarrow>\<^sub>f\<^sub>2 f2 \<longrightarrow> f2 \<in> f2Set) \<longrightarrow> finite f2Set"
  by auto

lemma smallStepReduceAllIncreasesWarnings:
"(contract, ctx, state, env, warnings, payments) \<Down>\<^sub>f\<^sub>2 (newContract, newCtx, newState, newEnv, newWarnings, newPayments) \<Longrightarrow>
  length newWarnings > length warnings"
proof (induction contract ctx state env warnings payments newContract newCtx newState newEnv newWarnings newPayments rule: small_step_reduce_all_induct)
  case (FinalStep contract ctx state env warnings payments)
  then show ?case using smallStepReduceAddsSingleWarning by auto
next
  case (AddStep contract ctx state env warnings payments newContract newCtx newState newEnv newWarnings newPayments finalContract finalCtx finalState finalEnv finalWarnings finalPayments)
  then show ?case using smallStepReduceAddsSingleWarning by auto
qed

lemma smallStepReduceAppendsWarning:
"(contract, ctx, state, env, [], payments) \<rightarrow>\<^sub>f\<^sub>2 (newContract, newCtx, newState, newEnv, newWarning, newPayments) \<Longrightarrow>
  (contract, ctx, state, env, warnings, payments) \<rightarrow>\<^sub>f\<^sub>2 (newContract, newCtx, newState, newEnv, warnings @ newWarning, newPayments)"
  apply (induction contract ctx state env "Nil :: ReduceWarning list" payments newContract newCtx newState newEnv newWarning newPayments rule: small_step_reduce_induct)
  by auto

lemma smallStepReduceAppendsPayment:
"(contract, ctx, state, env, warnings, []) \<rightarrow>\<^sub>f\<^sub>2 (newContract, newCtx, newState, newEnv, newWarnings, newPayments) \<Longrightarrow>
  (contract, ctx, state, env, warnings, payments) \<rightarrow>\<^sub>f\<^sub>2 (newContract, newCtx, newState, newEnv, newWarnings, payments @ newPayments)"
  apply (induction contract ctx state env warnings "Nil :: Payment list" newContract newCtx newState newEnv newWarnings newPayments rule: small_step_reduce_induct)
  by auto

lemma smallStepReduceAllWarningsAreArbitrary:
  "(c, bc, s, e, w, p) \<Down>\<^sub>f\<^sub>2 (c', bc', s', e', w'', p') \<Longrightarrow>
    (\<forall>w'' . \<exists>w''' . (c, bc, s, e, w'', p) \<Down>\<^sub>f\<^sub>2 (c', bc', s', e', w''', p'))"
  apply (induction rule: small_step_reduce_all_induct, auto)
  by (meson smallStepWarningsAreArbitrary finalWarningsAndPaymentsAreArbitrary small_step_reduce_all.simps)+

lemma smallStepReduceAllPaymentsAreArbitrary:
"(c, bc, s, e, w, p) \<Down>\<^sub>f\<^sub>2 (c', bc', s', e', w', p') \<Longrightarrow>
  (\<forall>p'' . \<exists>p''' . (c, bc, s, e, w, p'') \<Down>\<^sub>f\<^sub>2 (c', bc', s', e', w', p'''))"
  apply (induction rule: small_step_reduce_all_induct, auto)
  by (meson smallStepPaymentsAreArbitrary finalWarningsAndPaymentsAreArbitrary small_step_reduce_all.simps)+

lemma smallStepReduceAllExistingWarningsPaymentsConstant:
"(c, bc, s, e, w, p) \<Down>\<^sub>f\<^sub>2 (c', bc', s', e', w', p') \<Longrightarrow>
  (\<exists>newP . p' = p @ newP) \<and> (\<exists>newW . w' = w @ newW)"
  apply (induction rule: small_step_reduce_all_induct, auto)
     apply (metis smallStepExistingWarningsPaymentsConstant)
    apply (metis smallStepExistingWarningsPaymentsConstant)
  by (metis append.assoc smallStepExistingWarningsPaymentsConstant)+

lemma smallStepReduceAllAppendedWarningsConstant:
"(contract, ctx, state, env, warnings, payments) \<Down>\<^sub>f\<^sub>2 (newContract, newCtx, newState, newEnv, newWarnings, newPayments) \<Longrightarrow>
  (\<exists>addedWarnings . newWarnings = warnings @ addedWarnings \<longrightarrow>
  (\<forall>existingWarnings . (contract, ctx, state, env, existingWarnings, payments) \<Down>\<^sub>f\<^sub>2 (newContract, newCtx, newState, newEnv, existingWarnings @ addedWarnings, newPayments)))"
proof (induction contract ctx state env warnings payments newContract newCtx newState newEnv newWarnings newPayments rule: small_step_reduce_all_induct)
  case (FinalStep a a a a b a a a a a b)
  then show ?case by (metis (full_types) list.distinct(1) self_append_conv2)
next
  case (AddStep a a a a b a a a a a b a a a a a b)
  then show ?case by (metis (full_types) list.distinct(1) self_append_conv2)
qed

lemma smallStepReduceAllAppendedPaymentsConstant:
"(contract, ctx, state, env, warnings, payments) \<Down>\<^sub>f\<^sub>2 (newContract, newCtx, newState, newEnv, newWarnings, newPayments) \<Longrightarrow>
  (\<exists>addedPayments . newPayments = payments @ addedPayments \<longrightarrow>
  (\<forall>existingPayments . (contract, ctx, state, env, warnings, existingPayments) \<Down>\<^sub>f\<^sub>2 (newContract, newCtx, newState, newEnv, newWarnings, existingPayments @ addedPayments)))"
  apply (induction contract ctx state env warnings payments newContract newCtx newState newEnv newWarnings newPayments rule: small_step_reduce_all_induct)
  by (metis (full_types) list.distinct(1) self_append_conv2)+

thm star.induct[of small_step_reduce, split_format(complete)]

lemma smallStepReduceAppendedWarningsConstantGeneral:
"((contract, ctx, state, env, warnings, payments) \<rightarrow>\<^sub>f\<^sub>2 (newContract, newCtx, newState, newEnv, newWarnings, newPayments) \<longrightarrow>
  (\<exists>addedWarnings . (newWarnings = warnings @ addedWarnings \<and>
  (\<forall>existingWarnings . (contract, ctx, state, env, existingWarnings, payments) \<rightarrow>\<^sub>f\<^sub>2 (newContract, newCtx, newState, newEnv, existingWarnings @ addedWarnings, newPayments)))))"
  apply auto 
  apply (induction contract ctx state env warnings payments newContract newCtx newState newEnv newWarnings newPayments rule: small_step_reduce_induct)
  by auto

lemma smallStepReduceAppendedPaymentsConstantGeneral:
"((contract, ctx, state, env, warnings, payments) \<rightarrow>\<^sub>f\<^sub>2 (newContract, newCtx, newState, newEnv, newWarnings, newPayments) \<longrightarrow>
  (\<exists>addedPayments . (newPayments = payments @ addedPayments \<and>
  (\<forall>existingPayments . (contract, ctx, state, env, warnings, existingPayments) \<rightarrow>\<^sub>f\<^sub>2 (newContract, newCtx, newState, newEnv, newWarnings, existingPayments @ addedPayments)))))"
  apply auto 
  apply (induction contract ctx state env warnings payments newContract newCtx newState newEnv newWarnings newPayments rule: small_step_reduce_induct)
  by auto

lemma smallStepReduceAllAppendedWarningsConstantGeneral:
"((contract, ctx, state, env, warnings, payments) \<Down>\<^sub>f\<^sub>2 (newContract, newCtx, newState, newEnv, newWarnings, newPayments) \<longrightarrow>
  (\<exists>addedWarnings . (newWarnings = warnings @ addedWarnings \<and>
  (\<forall>existingWarnings . (contract, ctx, state, env, existingWarnings, payments) \<Down>\<^sub>f\<^sub>2 (newContract, newCtx, newState, newEnv, existingWarnings @ addedWarnings, newPayments)))))"
  apply auto
  apply (induction contract ctx state env warnings payments newContract newCtx newState newEnv newWarnings newPayments rule: small_step_reduce_all_induct)
   apply (meson FinalStep finalWarningsAndPaymentsAreArbitrary smallStepReduceAppendedWarningsConstantGeneral)
  by (smt (verit) AddStep append.assoc smallStepReduceAppendedWarningsConstantGeneral)

lemma smallStepReduceAllAppendedPaymentsConstantGeneral:
"((contract, ctx, state, env, warnings, payments) \<Down>\<^sub>f\<^sub>2 (newContract, newCtx, newState, newEnv, newWarnings, newPayments) \<longrightarrow>
  (\<exists>addedPayments . (newPayments = payments @ addedPayments \<and>
  (\<forall>existingPayments . (contract, ctx, state, env, warnings, existingPayments) \<Down>\<^sub>f\<^sub>2 (newContract, newCtx, newState, newEnv, newWarnings, existingPayments @ addedPayments)))))"
  apply auto
  apply (induction contract ctx state env warnings payments newContract newCtx newState newEnv newWarnings newPayments rule: small_step_reduce_all_induct)
   apply (meson FinalStep finalWarningsAndPaymentsAreArbitrary smallStepReduceAppendedPaymentsConstantGeneral)
  by (smt (verit) AddStep append.assoc smallStepReduceAppendedPaymentsConstantGeneral)

lemma smallStepReduceAllNotReflexive:
"f1 \<Down>\<^sub>f\<^sub>2 f2 \<Longrightarrow> f1 \<noteq> f2"
  using smallStepReduceAllIncreasesWarnings apply (cases f1; cases f2; auto)
  by blast

lemma smallStepReduceAllIsInTransitiveClosure:
"c1 \<Down>\<^sub>f\<^sub>2 c2 \<Longrightarrow> c1 \<rightarrow>\<^sub>f\<^sub>2* c2"
proof (induction c1 c2 rule: small_step_reduce_all.induct)
  case (FinalStep f1 f2)
  then show ?case by auto
next
  case (AddStep f1 f2 f3)
  then show ?case using star.step[of small_step_reduce] by auto
qed

lemma smallStepReduceAllDeterministic:
"f1 \<Down>\<^sub>f\<^sub>2 f2 \<Longrightarrow> f1 \<Down>\<^sub>f\<^sub>2 f3 \<Longrightarrow> f2 = f3"
  apply (induction f1 f2 arbitrary: f3 rule: small_step_reduce_all.induct)
   apply (metis deterministic reduceAllImpliesNotFinal small_step_reduce_all.cases)
  by (metis deterministic finalImpliesNoReduceAll small_step_reduce_all.simps)

lemma smallStepReduceAllNoBacktrack:
"f2 \<Down>\<^sub>f\<^sub>2 f3 \<Longrightarrow> (\<forall>f1 . f1 \<rightarrow>\<^sub>f\<^sub>2 f2 \<longrightarrow> f3 \<noteq> f1)"
proof(induction f2 f3  rule: small_step_reduce_all.induct)
  case (FinalStep f1 f2)
  then show ?case apply auto
    using finalV2_def by blast
next
  case (AddStep f1 f2 f3)
  then show ?case apply auto
    using smallStepReduceAllIsInTransitiveClosure smallStepReduceNoBacktrack by blast
qed

lemma smallStepReduceAllGivesTrace:
"f1 \<Down>\<^sub>f\<^sub>2 f2 \<Longrightarrow> (\<exists>insideStates . validTrace (f1 # insideStates @ [f2]))"
  apply (induction rule: small_step_reduce_all.induct, auto)
   apply (metis append_Nil validTraceFromConfiguration.simps(1) validTraceFromConfiguration.simps(2))
  by (metis append_Cons validTraceFromConfiguration.simps(2))

lemma traceGivesSmallStepReduceAll:
"validTrace fs \<Longrightarrow> (\<exists> f1 f2 f3 . fs = f1 # f2 @ [f3] \<longrightarrow> f1 \<Down>\<^sub>f\<^sub>2 f3)"
proof (induction fs rule: validTrace.induct)
  case 1
  then show ?case by auto
next
  case (2 f1 rest)
  then show ?case apply auto
    by blast
qed

lemma smallStepTraceIsUnique:
"(validTraceFromConfiguration f1 rest1 \<and> validTraceFromConfiguration f1 rest2 \<longrightarrow> rest1 = rest2)"
proof (induction f1 rest1 arbitrary: rest2 rule: validTraceFromConfiguration.induct)
  case (1 f1)
  then show ?case apply auto
    by (metis finalV2_def validTraceFromConfiguration.elims(2))
next
  case (2 f1 f2 rest)
  then show ?case apply simp
    by (metis deterministic TransSingleStep finalImpliesNoSmallStepTrans validTraceFromConfiguration.elims(2))
qed

lemma wellTypedPayContinues:
  assumes "wellTypedContract tyCtx (StatementCont (Pay accId payee token amt) cont) \<and>
  wellTypedContext tyCtx ctx \<and>
  wellTypedState tyCtx s"
  shows "(\<exists> newS newW newP .(StatementCont (Pay accId payee token amt) cont, ctx, s, e, w, p) \<rightarrow>\<^sub>f\<^sub>2 (cont, ctx, newS, e, newW, newP))"
proof -
  obtain res where "evalFValue e s amt = Some res" using assms apply auto
    by (meson option.exhaust wellTypedValueObservationEvaluates(1))
  show ?thesis proof (cases "res \<le> 0")
    case True
    then show ?thesis using assms apply auto
      using \<open>evalFValue e s amt = Some res\<close> wellTypedPayeeEvaluates wellTypedPartyEvaluates by fastforce
  next
    case False
    then show ?thesis proof -
      obtain fromAcc where "evalFParty s accId = Some fromAcc" using assms apply auto
        by (meson option.exhaust_sel wellTypedPartyEvaluates)
      moreover obtain toPayee where "evalFPayee s payee = Some toPayee" using assms apply auto
        by (meson option.exhaust wellTypedPayeeEvaluates)
      ultimately show ?thesis proof (cases "res > moneyInAccount fromAcc token (accounts s)")
        case True
        then show ?thesis using assms apply auto
          by (metis small_step_reduce.PayNonPositive small_step_reduce.PayPositiveFullWithPayment small_step_reduce.PayPositivePartialWithPayment  \<open>\<And>thesis. (\<And>res. evalFValue e s amt = Some res \<Longrightarrow> thesis) \<Longrightarrow> thesis\<close> \<open>evalFPayee s payee = Some toPayee\<close> \<open>evalFParty s accId = Some fromAcc\<close> giveMoney.elims not_less)
      next
        case False
        then show ?thesis using assms apply auto
          by (metis False small_step_reduce.PayNonPositive small_step_reduce.PayPositiveFullWithPayment \<open>evalFPayee s payee = Some toPayee\<close> \<open>evalFParty s accId = Some fromAcc\<close> \<open>evalFValue e s amt = Some res\<close> giveMoney.elims not_less)
      qed
    qed
  qed
qed

lemma wellTypedAssertContinues:
  assumes "wellTypedContract tyCtx (StatementCont (Assert obs) cont) \<and>
  wellTypedContext tyCtx ctx \<and>
  wellTypedState tyCtx s"
  shows "(\<exists> newW newP .(StatementCont (Assert obs) cont, ctx, s, e, w, p) \<rightarrow>\<^sub>f\<^sub>2 (cont, ctx, s, e, newW, newP))"
proof -
  obtain res where "evalFObservation e s obs = Some res" using assms apply auto
    by (meson option.exhaust wellTypedValueObservationEvaluates(2))
  show ?thesis proof (cases res)
    case True
    then show ?thesis using assms \<open>evalFObservation e s obs = Some res\<close> by force
  next
    case False
    then show ?thesis using assms \<open>evalFObservation e s obs = Some res\<close> by force
  qed
qed

lemma wellTypedReassignValContinues:
  assumes "wellTypedContract tyCtx (StatementCont (ReassignVal vId val) cont) \<and>
  wellTypedContext tyCtx ctx \<and>
  wellTypedState tyCtx s"
  shows "(\<exists> newS newW newP .(StatementCont (ReassignVal vId val) cont, ctx, s, e, w, p) \<rightarrow>\<^sub>f\<^sub>2 (cont, (ValueAbstraction vId)#ctx, newS, e, newW, newP))"
proof -
  obtain res where "evalFValue e s val = Some res" using assms apply auto
    using wellTypedValueObservationEvaluates(1) by fastforce
  show ?thesis using assms \<open>evalFValue e s val = Some res\<close>
    by (metis small_step_reduce.ReassignValNoShadow small_step_reduce.ReassignValShadow not_Some_eq)
qed

lemma wellTypedReassignObsContinues:
  assumes "wellTypedContract tyCtx (StatementCont (ReassignObservation oId obs) cont) \<and>
  wellTypedContext tyCtx ctx \<and>
  wellTypedState tyCtx s"
  shows "(\<exists> newS newW newP .(StatementCont (ReassignObservation oId obs) cont, ctx, s, e, w, p) \<rightarrow>\<^sub>f\<^sub>2 (cont, (ObservationAbstraction oId)#ctx, newS, e, newW, newP))"
proof -
  obtain res where "evalFObservation e s obs = Some res" using assms apply auto
    using wellTypedValueObservationEvaluates(2) by fastforce
  show ?thesis using assms \<open>evalFObservation e s obs = Some res\<close>
    by (metis small_step_reduce.ReassignObservation)
qed

lemma wellTypedReassignPkContinues:
  assumes "wellTypedContract tyCtx (StatementCont (ReassignPubKey pkId pk) cont) \<and>
  wellTypedContext tyCtx ctx \<and>
  wellTypedState tyCtx s"
  shows "(\<exists> newS newW newP .(StatementCont (ReassignPubKey pkId pk) cont, ctx, s, e, w, p) \<rightarrow>\<^sub>f\<^sub>2 (cont, (PubKeyAbstraction pkId)#ctx, newS, e, newW, newP))"
proof -
  obtain res where "evalFParty s pk = Some res" using assms apply auto
    using wellTypedPartyEvaluates by fastforce
  show ?thesis using assms \<open>evalFParty s pk = Some res\<close>
    by (metis small_step_reduce.ReassignPubKey)
qed

lemma wellTypedUseCContinues:
  assumes "wellTypedContract tyCtx (UseC cid args) \<and>
  wellTypedContext tyCtx ctx \<and>
  wellTypedState tyCtx s"
  shows "(\<exists> newCont newCtx newS newW .(UseC cid args, ctx, s, e, w, p) \<rightarrow>\<^sub>f\<^sub>2 (newCont, newCtx, newS, e, newW, p))"
  using assms apply (cases "lookupContractIdParamTypeInformation tyCtx cid", auto)
  by (metis UseCFound lookupContractAbstractionWellTypedContext option.exhaust_sel wellTypedArgumentsExecute)

(* A well typed final configuration only has Close or When contracts. *)
lemma finalV2D: "(finalV2 (c1, cx, s, e, w, p) \<and> (wellTypedContract tcx c1) \<and> wellTypedContext tcx cx \<and> wellTypedState tcx s) \<Longrightarrow> c1 = Close \<or> (\<exists> cs t c2 . c1 = When cs t c2)"
  apply (auto simp add: finalV2_def)
  apply (cases c1, auto)
        apply (smt FStatement.exhaust wellTypedStatement.simps wellTypedAssertContinues wellTypedReassignValContinues wellTypedReassignObsContinues wellTypedReassignPkContinues wellTypedContract.simps(2) wellTypedPayContinues)
        apply (smt small_step_reduce.IfFalse small_step_reduce.intros option.exhaust_sel wellTypedValueObservationEvaluates(2))
       apply (metis small_step_reduce.LetNoShadow small_step_reduce.LetShadow not_Some_eq wellTypedValueObservationEvaluates(1))
      apply (meson small_step_reduce.intros(14) option.exhaust_sel wellTypedValueObservationEvaluates(2))
     apply (meson small_step_reduce.intros(16) option.exhaust_sel wellTypedPartyEvaluates)
   apply blast
  by (metis wellTypedContract.simps(9) wellTypedUseCContinues)

lemma insertGivesSameDomainAsConsCompared:
"set (map fst m) \<subseteq> set c \<Longrightarrow> set (map fst (MList.insert newKey (newVal) (m))) \<subseteq> set ((newKey)#c)"
  by (induction m rule: MList.insert.induct, auto)

lemma typePreservationUseC:
  assumes"(UseC cid args, ContractAbstraction (cid, params, newContract) # innerCtx, s, e, w, p) \<rightarrow>\<^sub>f\<^sub>2 (newContract, newCtx, newS, newE, newW, newP) \<and>
    wellTypedContract ((cid, ContractType params) # restTyCtx) (UseC cid args) \<and>
    wellTypedContext ((cid, ContractType params) # restTyCtx) (ContractAbstraction (cid, params, newContract) # innerCtx) \<and>
    wellTypedState ((cid, ContractType params) # restTyCtx) s"
  shows "wellTypedContract (paramsToTypeContext params@restTyCtx) newContract \<and>
    wellTypedContext (paramsToTypeContext params@restTyCtx) newCtx \<and>
    wellTypedState (paramsToTypeContext params@restTyCtx) newS"
  using assms apply auto
  using concatWellTypedContexts paramsWellTypedContext apply blast
  by (metis wellTypedArgumentsAddParamsToState wellTypedArgumentsOnlyAddIdsToState wellTypedState.simps(5) wellTypedStateCombineTypeContexts)

(* A single step of reduction maintains type correctness. *)
lemma typePreservation:
assumes
  "(c1, cx1, s1, e1, w1, p1) \<rightarrow>\<^sub>f\<^sub>2 (c2, cx2, s2, e2, w2, p2) \<and>
  wellTypedContract tcx1 c1 \<and>
  wellTypedContext tcx1 cx1 \<and>
  wellTypedState tcx1 s1"
shows
  "(\<exists>tcx2 . wellTypedContract tcx2 c2 \<and>
  wellTypedContext tcx2 cx2 \<and>
  wellTypedState tcx2 s2)"
  using assms apply (cases c1, auto)
  using wellTypedStateAccountsArbitrary apply blast
  subgoal premises ps proof-
    from ps assms obtain stmt cont where "(StatementCont stmt cont) = c1" by auto
    moreover from calculation ps assms have "cont = c2" apply (cases stmt)
      by auto
    ultimately show ?thesis using ps assms wellTypedStateAccountsArbitrary apply (cases stmt, auto)
      using consValueWellTypedContext
        consObservationWellTypedContext
        consPubKeyWellTypedContext
        insertAppendValueWellTypedStateIsWellTyped
        insertAppendObservationWellTypedStateIsWellTyped
        insertAppendPubKeyWellTypedStateIsWellTyped by metis+
  qed
  subgoal premises ps proof -
    obtain vid val where "c1 = Let vid val c2" using ps by auto
    moreover have "wellTypedContext ((vid, ValueType)#tcx1) cx2" using calculation ps assms by auto
    moreover have "wellTypedState ((vid, ValueType)#tcx1) s2" using calculation ps assms apply (auto simp add: MList.insert_lookup_Some)
      using insertValueWellTypedStateIsWellTyped by blast
    ultimately show ?thesis using ps by blast
  qed
   subgoal premises ps proof -
    obtain vid val where "c1 = Let vid val c2" using ps by auto
    moreover have "wellTypedContext ((vid, ValueType)#tcx1) cx2" using calculation ps assms by auto
    moreover have "wellTypedState ((vid, ValueType)#tcx1) s2" using calculation ps assms apply (auto simp add: MList.insert_lookup_Some)
      using insertValueWellTypedStateIsWellTyped by blast
    ultimately show ?thesis using ps by blast
  qed
  subgoal premises ps proof -
    obtain vid obs where "c1 = LetObservation vid obs c2" using ps by auto
    moreover have "wellTypedContext ((vid, ObservationType)#tcx1) cx2" using calculation ps assms by auto
    moreover have "wellTypedState ((vid, ObservationType)#tcx1) s2" using calculation ps assms apply simp
      by (metis MList.insert_lookup_Some insertValueWellTypedStateIsWellTyped)
    ultimately show ?thesis using ps by blast
  qed
  subgoal premises ps proof -
    obtain obsId obs where "c1 = LetObservation obsId obs c2" using ps by auto
    moreover have "wellTypedContext ((obsId, ObservationType)#tcx1) cx2" using calculation ps assms by auto
    moreover have "wellTypedState ((obsId, ObservationType)#tcx1) s2" using calculation ps assms apply simp
      apply (meson calculation ps assms insertValueWellTypedStateIsWellTyped MList.insert_lookup_Some)
      using insertValueWellTypedStateIsWellTyped by blast
    ultimately show ?thesis using ps by blast
  qed
  subgoal premises ps proof -
    obtain pkId pk where "c1 = LetPubKey pkId pk c2" using ps by auto
    moreover have "wellTypedContext ((pkId, PubKeyType)#tcx1) cx2" using calculation ps assms by auto
    moreover have "wellTypedState ((pkId, PubKeyType)#tcx1) s2" using calculation ps assms apply (auto simp add: MList.insert_lookup_Some)
      using insertPubKeyWellTypedStateIsWellTyped by blast
    ultimately show ?thesis using ps by blast
  qed
  subgoal premises ps proof -
    obtain cid args where "c1 = UseC cid args" using ps by auto
    moreover obtain params restTy where "lookupContractIdParamTypeInformation tcx1 cid = Some (params, restTy)" using ps assms calculation by (cases "lookupContractIdParamTypeInformation tcx1 cid", auto)
    moreover obtain  innerCtx where "lookupContractIdAbsInformation cx1 cid = Some (params, c2, innerCtx)" using ps assms calculation by (metis FContract.inject(8) Pair_inject lookupContractTypeWellTypedContext option.inject)
    moreover have "wellTypedContext restTy innerCtx" using calculation(2) calculation(3) lookupContractRestIsWellTyped ps(5) by blast
    moreover have "wellTypedContext (paramsToTypeContext params @ restTy) ((paramsToFContext params) @ innerCtx)" by (simp add: calculation(4) concatWellTypedContexts paramsWellTypedContext)
    ultimately show ?thesis using ps assms apply auto
      by (metis assms lookupContractFoundContractIsWellTyped lookupContractStateStaysWellTyped wellTypedArgumentsAddParamsToState wellTypedArgumentsOnlyAddIdsToState wellTypedStateCombineTypeContexts)
  qed
  done

(* Many steps of reduction maintains type correctness. *)
lemma typePreservationManySteps:
assumes"(contract, ctx, s, e, w, p) \<rightarrow>\<^sub>f\<^sub>2* (newContract, newCtx, newS, newE, newW, newP) \<and>
    wellTypedContract tyCtx contract \<and>
    wellTypedContext tyCtx ctx \<and>
    wellTypedState tyCtx s"
shows "(\<exists>newTyCtx . wellTypedContract newTyCtx newContract \<and>
    wellTypedContext newTyCtx newCtx \<and>
    wellTypedState newTyCtx newS)"
  using assms apply auto
  subgoal proof (induction arbitrary: tyCtx rule:star.induct[of small_step_reduce, split_format(complete)])
    case (refl)
    then show ?case by auto
  next
    case (step)
    then show ?case using typePreservation by blast
  qed
  done

lemma typePreservationAllSteps:
assumes"(contract, ctx, s, e, w, p) \<Down>\<^sub>f\<^sub>2 (newContract, newCtx, newS, newE, newW, newP) \<and>
    wellTypedContract tyCtx contract \<and>
    wellTypedContext tyCtx ctx \<and>
    wellTypedState tyCtx s"
shows "(\<exists>newTyCtx . wellTypedContract newTyCtx newContract \<and>
    wellTypedContext newTyCtx newCtx \<and>
    wellTypedState newTyCtx newS)"
  using assms smallStepReduceAllIsInTransitiveClosure typePreservationManySteps by meson

(* A well typed contract will be able to continue unless in a final state. *)
lemma contractProgress:
  assumes "wellTypedContract tyCtx contract \<and>
    wellTypedContext tyCtx ctx \<and>
    wellTypedState tyCtx s \<and>
    \<not>(finalV2 (contract, ctx, s, e, w, p))"
  shows "\<exists>cs'. (contract, ctx, s, e, w, p) \<rightarrow>\<^sub>f\<^sub>2 cs'"
  using assms by (auto simp add: finalV2_def)

(* A well typed program can get to a final state. *)
lemma typeSoundness:
  assumes "(contract, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f\<^sub>2* (newContract, newCtx, newS, newEnv, newWarns, newPayments) \<and>
            \<not>(finalV2 (newContract, newCtx, newS, newEnv, newWarns, newPayments)) \<and>
            wellTypedContract tyCtx contract \<and>
            wellTypedContext tyCtx ctx \<and>
            wellTypedState tyCtx s"
  shows "\<exists>cs''. (newContract, newCtx, newS, newEnv, newWarns, newPayments) \<rightarrow>\<^sub>f\<^sub>2 cs''"
  using assms by (auto simp add: finalV2_def)

fun compileStmt :: "FStatement \<Rightarrow> FaustusTypes.FContract \<Rightarrow> FaustusTypes.FContract" where
"compileStmt (Pay accId payee t v) = (FaustusTypes.Pay accId payee t v)" |
"compileStmt (Assert obs) = (FaustusTypes.Assert obs)" |
"compileStmt (ReassignVal vId val) = (FaustusTypes.Let vId val)" |
"compileStmt (ReassignObservation obsId obs) = (FaustusTypes.LetObservation obsId obs)" |
"compileStmt (ReassignPubKey pkId pk) = (FaustusTypes.LetPubKey pkId pk)"

fun compileStmts :: "FStatement list \<Rightarrow> FaustusTypes.FContract \<Rightarrow> FaustusTypes.FContract" where
"compileStmts [] = id" |
"compileStmts (stmt#stmts) = (let compiledStmt = compileStmt stmt in
  let compiledStmts = compileStmts stmts in
  (\<lambda>c \<Rightarrow> compiledStmt (compiledStmts c)))"

fun compileLinearGuardNode :: "LinearGuardNode \<Rightarrow> FaustusTypes.FContract \<Rightarrow> FaustusTypes.FCase" where
"compileLinearGuardNode (a, stmts) cont = FaustusTypes.Case a (compileStmts stmts cont)"

fun compileLinearGuardExpression :: "LinearGuardExpression \<Rightarrow> FaustusTypes.FContract \<Rightarrow> int \<Rightarrow> FaustusTypes.FContract \<Rightarrow> FaustusTypes.FCase list" where
"compileLinearGuardExpression (LinearGuardNode node) cont t tcont = [compileLinearGuardNode node cont]" |
"compileLinearGuardExpression (LinearGuardThenGuard node g2) cont t tcont = [compileLinearGuardNode node (FaustusTypes.When (compileLinearGuardExpression g2 cont t tcont) t tcont)]" |
"compileLinearGuardExpression (LinearDisjointGuard g1 g2) cont t tcont = ((compileLinearGuardExpression g1 cont t tcont) @ (compileLinearGuardExpression g2 cont t tcont))"

fun compileGuardExpression :: "FGuardExpression \<Rightarrow> FaustusTypes.FContract \<Rightarrow> int \<Rightarrow> FaustusTypes.FContract \<Rightarrow> FaustusTypes.FCase list" where
"compileGuardExpression g cont t tcont = (compileLinearGuardExpression (linearizeGuardExpression g) cont t tcont)"

fun compile :: "FContract \<Rightarrow> FaustusTypes.FContract"
  and compileCases :: "FCase list \<Rightarrow> int \<Rightarrow> FaustusTypes.FContract \<Rightarrow> FaustusTypes.FCase list" where
"compile Close = FaustusTypes.Close"|
"compile (StatementCont stmt cont) = (compileStmt stmt (compile cont))"|
"compile (If obs trueCont falseCont) = (FaustusTypes.If obs (compile trueCont) (compile falseCont))"|
"compile (When cases t tCont) = (let compiledTCont = (compile tCont) in
      (FaustusTypes.When (compileCases cases t compiledTCont) t compiledTCont))"|
"compile (Let valId val cont) = (FaustusTypes.Let valId val (compile cont))"|
"compile (LetObservation obsId obs cont) = (FaustusTypes.LetObservation obsId obs (compile cont))"|
"compile (LetPubKey pkId pk cont) = (FaustusTypes.LetPubKey pkId pk (compile cont))"|
"compile (LetC cid params boundCon cont) = (FaustusTypes.LetC cid params (compile boundCon) (compile cont))" |
"compile (UseC cid args) = (FaustusTypes.UseC cid args)" |
"compileCases ((Case g c) # rest) t tcont = (((compileGuardExpression g (compile c) t tcont) @ (compileCases rest t tcont)))" |
"compileCases [] t tcont = []"

lemma compileCorrectness_Close:
"compile Close = (FaustusTypes.Close)"
  by auto

lemma compileCorrectness_Pay:
"\<forall>compiledCont . (compileStmt (Pay accId payee tok val)) compiledCont = (FaustusTypes.Pay accId payee tok val compiledCont)"
  by auto

lemma compileCorrectness_Assert:
"\<forall>compiledCont . (compileStmt (Assert obs)) compiledCont = (FaustusTypes.Assert obs compiledCont)"
  by auto

lemma compileCorrectness_If:
"compile (If obs trueCont falseCont) = (mCont) \<Longrightarrow>
  \<exists>trueMCont falseMCont. compile trueCont = (trueMCont) \<and>
  compile falseCont = (falseMCont) \<and>
  (mCont = FaustusTypes.If obs trueMCont falseMCont)"
  by auto

lemma compileCorrectness_When:
"compile (When cases t tCont) = (mCont) \<Longrightarrow>
\<exists>mcs mTCont. compile tCont = (mTCont) \<and>
  mCont = (FaustusTypes.When mcs t mTCont) \<and>
  compileCases cases t mTCont = (mcs)"
  apply auto
  by metis

lemma compileCorrectness_Let:
"compile (Let vid val cont) = (mCont) \<Longrightarrow>
  \<exists>innerMCont . compile cont = (innerMCont) \<and>
  mCont = (FaustusTypes.Let vid val innerMCont)"
  by (auto)

lemma compileCorrectness_LetObservation:
"compile (LetObservation obsId obs cont) = (mCont) \<Longrightarrow>
  \<exists>innerMCont . compile cont = (innerMCont) \<and>
  mCont = (FaustusTypes.LetObservation obsId obs innerMCont)"
  by (auto)

lemma compileCorrectness_LetPubKey:
"compile (LetPubKey pkId pk cont) = (mCont) \<Longrightarrow>
  \<exists>innerMCont . compile cont = (innerMCont) \<and>
  mCont = (FaustusTypes.LetPubKey pkId pk innerMCont)"
  by (auto)

lemma compileCorrectness_LetC:
"compile (LetC cid params boundCon cont) = (mCont) \<Longrightarrow>
  \<exists>innerMCont compiledBoundCon . compile cont = (innerMCont) \<and>
  compile boundCon = compiledBoundCon \<and>
  mCont = (FaustusTypes.LetC cid params compiledBoundCon innerMCont)"
  by auto

lemma compileCorrectness_UseC:
"compile (UseC cid args) = (mCont) \<Longrightarrow>
  mCont = FaustusTypes.UseC cid args"
  by (auto)

lemma lookupTypeConsResMaintainsPartyV1TypeCorrectness:
"lookupType tyCtx2 i = Some res \<Longrightarrow>
  FaustusSemantics.wellTypedParty (tyCtx1 @ tyCtx2) p \<Longrightarrow>
  FaustusSemantics.wellTypedParty (tyCtx1 @ (i, res)#tyCtx2) p"
  apply (induction tyCtx2, auto)
  by (induction tyCtx1; cases p; auto)

lemma shadowedTypeInfoIsArbitraryForWellTypedPartyV1:
"FaustusSemantics.wellTypedParty ((i, someType)#tyCtx) p \<Longrightarrow>
  FaustusSemantics.wellTypedParty ((i, someType)#(i, someOtherType)#tyCtx) p"
  by (induction tyCtx; cases p, auto)

lemma lookupTypeConsResMaintainsPayeeV1TypeCorrectness:
"lookupType tyCtx2 i = Some res \<Longrightarrow>
  FaustusSemantics.wellTypedPayee (tyCtx1 @ tyCtx2) p \<Longrightarrow>
  FaustusSemantics.wellTypedPayee (tyCtx1 @ (i, res)#tyCtx2) p"
  apply (induction tyCtx2 i rule: lookupType.induct)
  by (cases p, auto simp add: lookupTypeConsResMaintainsPartyV1TypeCorrectness)+

lemma shadowedTypeInfoIsArbitraryForWellTypedPayeeV1:
"FaustusSemantics.wellTypedPayee ((i, someType)#tyCtx) p \<Longrightarrow>
  FaustusSemantics.wellTypedPayee ((i, someType)#(i, someOtherType)#tyCtx) p"
  by (induction tyCtx; cases p, auto simp add:shadowedTypeInfoIsArbitraryForWellTypedPartyV1)

lemma lookupTypeConsResMaintainsChoiceIdV1TypeCorrectness:
"lookupType tyCtx2 i = Some res \<Longrightarrow>
  FaustusSemantics.wellTypedChoiceId (tyCtx1 @ tyCtx2) c \<Longrightarrow>
  FaustusSemantics.wellTypedChoiceId (tyCtx1 @ (i, res)#tyCtx2) c"
  by (induction tyCtx2 i rule: lookupType.induct;
      cases c;
      auto simp add: lookupTypeConsResMaintainsPartyV1TypeCorrectness)

lemma shadowedTypeInfoIsArbitraryForWellTypedChoiceIdV1:
"FaustusSemantics.wellTypedChoiceId ((i, someType)#tyCtx) c \<Longrightarrow>
  FaustusSemantics.wellTypedChoiceId ((i, someType)#(i, someOtherType)#tyCtx) c"
  by (induction tyCtx; cases c, auto simp add:shadowedTypeInfoIsArbitraryForWellTypedPartyV1)

lemma lookupTypeConsResMaintainsValueV1TypeCorrectness_UseValue:
"lookupType newTyCtx2 newI = Some newRes \<Longrightarrow>
        wellTypedValue (newTyCtx1 @ newTyCtx2) (UseValue newValId) \<Longrightarrow>
        wellTypedValue (newTyCtx1 @ (newI, newRes)#newTyCtx2) (UseValue newValId)"
  apply (induction newTyCtx2, auto)
  by (induction newTyCtx1, auto)

lemma lookupTypeConsResMaintainsValueV1TypeCorrectness_UseObservation:
"lookupType newTyCtx2 newI = Some newRes \<Longrightarrow>
        wellTypedObservation (newTyCtx1 @ newTyCtx2) (UseObservation newValId) \<Longrightarrow>
        wellTypedObservation (newTyCtx1 @ (newI, newRes)#newTyCtx2) (UseObservation newValId)"
  apply (induction newTyCtx2, auto)
  by (induction newTyCtx1, auto)

lemma lookupTypeConsResMaintainsValueV1TypeCorrectness:
"\<forall>tyCtx1 tyCtx2 i res . lookupType tyCtx2 i = Some res \<longrightarrow>
  FaustusSemantics.wellTypedValue (tyCtx1 @ tyCtx2) v \<longrightarrow>
  FaustusSemantics.wellTypedValue (tyCtx1 @ (i, res)#tyCtx2) v"
"(\<forall>tyCtx1 tyCtx2 i res . lookupType tyCtx2 i = Some res \<longrightarrow>
  FaustusSemantics.wellTypedObservation (tyCtx1 @ tyCtx2) obs \<longrightarrow>
  FaustusSemantics.wellTypedObservation (tyCtx1 @ (i, res)#tyCtx2) obs)"
  apply (induction rule: wellTypedValue_wellTypedObservation.induct)
  by (auto simp add: lookupTypeConsResMaintainsChoiceIdV1TypeCorrectness lookupTypeConsResMaintainsPartyV1TypeCorrectness lookupTypeConsResMaintainsValueV1TypeCorrectness_UseValue lookupTypeConsResMaintainsValueV1TypeCorrectness_UseObservation)

lemma shadowedTypeInfoIsArbitraryForWellTypedValueV1:
"(\<forall>i someType tyCtx someOtherType . FaustusSemantics.wellTypedValue ((i, someType)#tyCtx) val \<longrightarrow>
  FaustusSemantics.wellTypedValue ((i, someType)#(i, someOtherType)#tyCtx) val)"
"(\<forall>i someType tyCtx someOtherType . FaustusSemantics.wellTypedObservation ((i, someType)#tyCtx) obs \<longrightarrow>
  FaustusSemantics.wellTypedObservation ((i, someType)#(i, someOtherType)#tyCtx) obs)"
   apply (induction rule: wellTypedValue_wellTypedObservation.induct)
  by (auto simp add:shadowedTypeInfoIsArbitraryForWellTypedPartyV1 shadowedTypeInfoIsArbitraryForWellTypedChoiceIdV1)

lemma lookupTypeConsResMaintainsArgumentTypeCorrectness:
"lookupType tyCtx2 i = Some res \<Longrightarrow>
  FaustusSemantics.wellTypedArgument (tyCtx1@tyCtx2) p a \<Longrightarrow>
  FaustusSemantics.wellTypedArgument (tyCtx1@(i, res)#tyCtx2) p a"
  apply (induction tyCtx2 i rule: lookupType.induct, auto split: if_split_asm)
  by (cases p; cases a; auto simp add: lookupTypeConsResMaintainsValueV1TypeCorrectness lookupTypeConsResMaintainsPartyV1TypeCorrectness)+

lemma shadowedTypeInfoIsArbitraryForWellTypedArgumentV1:
"FaustusSemantics.wellTypedArgument ((i, someType)#tyCtx) p a \<Longrightarrow>
  FaustusSemantics.wellTypedArgument ((i, someType)#(i, someOtherType)#tyCtx) p a"
  by (induction tyCtx; cases p; cases a; auto simp add:shadowedTypeInfoIsArbitraryForWellTypedPartyV1 shadowedTypeInfoIsArbitraryForWellTypedValueV1)

lemma lookupTypeConsResMaintainsArgumentsV1TypeCorrectness:
"FaustusSemantics.wellTypedArguments (tyCtx1 @ tyCtx2) p a \<Longrightarrow>
  lookupType tyCtx2 i = Some res \<Longrightarrow>
  FaustusSemantics.wellTypedArguments (tyCtx1 @ (i, res)#tyCtx2) p a"
  by (induction tyCtx2 p a arbitrary: i res rule: wellTypedArguments.induct, auto simp add: lookupTypeConsResMaintainsArgumentTypeCorrectness)

lemma shadowedTypeInfoIsArbitraryForWellTypedArgumentsV1:
"FaustusSemantics.wellTypedArguments ((i, someType)#tyCtx) p a \<Longrightarrow>
  FaustusSemantics.wellTypedArguments ((i, someType)#(i, someOtherType)#tyCtx) p a"
  by (induction tyCtx p a arbitrary: i someType someOtherType rule: wellTypedArguments.induct; auto simp add: shadowedTypeInfoIsArbitraryForWellTypedArgumentV1 shadowedTypeInfoIsArbitraryForWellTypedPartyV1 shadowedTypeInfoIsArbitraryForWellTypedValueV1)

lemma lookupTypeConsResMaintainsLookupParamV1TypeCorrectness:
"FaustusSemantics.lookupContractIdParamTypeInformation (tyCtx1 @ tyCtx2) cid = Some (a, b) \<Longrightarrow>
  lookupType tyCtx2 i = Some res \<Longrightarrow>
  (res = ValueType \<or> res = ObservationType \<or> res = PubKeyType) \<Longrightarrow>
  (\<exists>newB . FaustusSemantics.lookupContractIdParamTypeInformation (tyCtx1 @ (i, res)#tyCtx2) cid = Some (a, newB))"
  by (induction tyCtx1; induction tyCtx2, auto split: if_split_asm AbstractionTypeInformation.split_asm)

lemma lookupTypeWeakenSameResult:
"lookupType (tyCtx1@(i, res)#tyCtx2) lookupI = Some lookupRes \<Longrightarrow>
  lookupType (tyCtx1@(i, res)#(i, res)#tyCtx2) lookupI = Some lookupRes"
  by (induction tyCtx1, auto)

lemma lookupTypeConsResMaintainsLookupType:
"lookupType (tyCtx1 @ tyCtx2) i1 = Some res1 \<Longrightarrow>
  lookupType tyCtx2 i2 = Some res2 \<Longrightarrow> lookupType (tyCtx1 @ (i2, res2) # tyCtx2) i1 = Some res1"
  by (induction tyCtx1, auto)

lemma lookupTypeConsResMaintainsStatementTypeCorrectness:
"(\<forall>tyCtx2 i res . wellTypedStatement (tyCtx1@tyCtx2) stmt \<longrightarrow>
  lookupType tyCtx2 i = Some res \<longrightarrow>
  wellTypedStatement (tyCtx1 @ (i, res)#tyCtx2) stmt)"
  apply (induction tyCtx1 stmt rule: wellTypedStatement.induct)
  by (auto simp add: lookupTypeConsResMaintainsPartyV1TypeCorrectness lookupTypeConsResMaintainsPayeeV1TypeCorrectness lookupTypeConsResMaintainsValueV1TypeCorrectness lookupTypeWeakenSameResult lookupTypeConsResMaintainsLookupType)

lemma lookupTypeConsResMaintainsStatementsTypeCorrectness:
"(\<forall> tyCtx1 tyCtx2 i res . wellTypedStatements (tyCtx1@tyCtx2) stmts \<longrightarrow>
  lookupType tyCtx2 i = Some res \<longrightarrow>
  (res = ValueType \<or> res = ObservationType \<or> res = PubKeyType) \<longrightarrow>
  wellTypedStatements (tyCtx1 @ (i, res)#tyCtx2) stmts)"
proof (induction stmts)
  case Nil
  then show ?case by auto
next
  case (Cons s stmts)
  then show ?case apply (simp add: lookupTypeConsResMaintainsStatementTypeCorrectness)
    by (cases s, auto simp add: lookupTypeConsResMaintainsStatementTypeCorrectness lookupTypeConsResMaintainsPartyV1TypeCorrectness lookupTypeConsResMaintainsPayeeV1TypeCorrectness; metis Cons_eq_appendI)+
qed

lemma lookupTypeConsResMaintainsContractTypeCorrectness:
"(\<forall> tyCtx1 tyCtx2 i res . FaustusSemantics.wellTypedContract (tyCtx1@tyCtx2) c \<longrightarrow>
  lookupType tyCtx2 i = Some res \<longrightarrow>
  (res = ValueType \<or> res = ObservationType \<or> res = PubKeyType) \<longrightarrow>
  FaustusSemantics.wellTypedContract (tyCtx1 @ (i, res)#tyCtx2) c)"
"(\<forall> tyCtx1 tyCtx2 i res . FaustusSemantics.wellTypedCases (tyCtx1 @ tyCtx2) ca \<longrightarrow>
  lookupType tyCtx2 i = Some res \<longrightarrow>
  (res = ValueType \<or> res = ObservationType \<or> res = PubKeyType) \<longrightarrow>
  FaustusSemantics.wellTypedCases (tyCtx1 @ (i, res)#tyCtx2) ca)"
proof (induction arbitrary: rule: FaustusSemantics.wellTypedContract_wellTypedCases.induct)
  case (6 tyCtx valId val cont)
  then show ?case apply (auto simp add: lookupTypeConsResMaintainsValueV1TypeCorrectness)
    by (metis Cons_eq_appendI)+
next
  case (7 tyCtx obsId obs cont)
  then show ?case apply (auto simp add: lookupTypeConsResMaintainsValueV1TypeCorrectness)
    by (metis Cons_eq_appendI)+
next
  case (8 tyCtx pkId pk cont)
  then show ?case apply (auto simp add: lookupTypeConsResMaintainsPartyV1TypeCorrectness)
    by (metis Cons_eq_appendI)+
next
  case (9 tyCtx cid params body cont)
  then show ?case apply auto
    by (metis append.assoc, metis Cons_eq_appendI)+
next
  case (10 tyCtx cid args)
  then show ?case apply (auto split: option.split)
    using lookupTypeConsResMaintainsArgumentsV1TypeCorrectness lookupTypeConsResMaintainsLookupParamV1TypeCorrectness by fastforce+
qed (auto simp add: lookupTypeConsResMaintainsValueV1TypeCorrectness lookupTypeConsResMaintainsPartyV1TypeCorrectness lookupTypeConsResMaintainsPayeeV1TypeCorrectness lookupTypeConsResMaintainsChoiceIdV1TypeCorrectness)

(* A well typed contract/cases will compile
  to a Marlowe contract/cases *)
lemma wellTypedStatementCompilesToWellTypedContract:
"(\<forall>tyCtx . (wellTypedStatement tyCtx stmt \<longrightarrow>
  (\<forall>cont . FaustusSemantics.wellTypedContract tyCtx cont \<longrightarrow> FaustusSemantics.wellTypedContract tyCtx (compileStmt stmt cont))))"
proof (induction rule: compileStmt.induct)
  case (1 accId payee t v)
  then show ?case by auto
next
  case (2 obs)
  then show ?case by auto
next
  case (3 a b)
  then show ?case apply auto
    by (metis append_Nil lookupTypeConsResMaintainsContractTypeCorrectness(1))
next
  case (4 a b)
  then show ?case apply auto
    by (metis append_Nil lookupTypeConsResMaintainsContractTypeCorrectness(1))
next
  case (5 a b)
  then show ?case apply auto
    by (metis append_Nil lookupTypeConsResMaintainsContractTypeCorrectness(1))
qed

lemma wellTypedStatementsCompileToWellTypedContract:
"(\<forall>tyCtx . (wellTypedStatements tyCtx stmts \<longrightarrow>
  (\<forall>cont . FaustusSemantics.wellTypedContract tyCtx cont \<longrightarrow> FaustusSemantics.wellTypedContract tyCtx (compileStmts stmts cont))))"
proof (induction rule: compileStmts.induct)
  case 1
  then show ?case by auto
next
  case (2 stmt stmts)
  then show ?case by (cases stmt; auto; metis eq_Nil_appendI lookupTypeConsResMaintainsContractTypeCorrectness(1))
qed

lemma wellTypedCasesAppend:
"(\<forall>tyCtx . FaustusSemantics.wellTypedCases tyCtx c1 \<and> FaustusSemantics.wellTypedCases tyCtx c2 \<longrightarrow>
  FaustusSemantics.wellTypedCases tyCtx (c1 @ c2))"
proof (induction c1)
  case Nil
  then show ?case by auto
next
  case (Cons a c1)
  then show ?case proof (cases a)
    case (Case x1 x2)
    then show ?thesis using local.Cons by (cases x1, auto)
  qed
qed

lemma interleavingPreservesWellTyped:
"\<forall>tyCtx . wellTypedLinearGuardExpression tyCtx g1 \<longrightarrow>
  wellTypedLinearGuardExpression tyCtx g2 \<longrightarrow>
  wellTypedLinearGuardExpression tyCtx (interleaveLinearTrees g1 g2 f)"
"\<forall>tyCtx . wellTypedLinearGuardExpression tyCtx g1 \<longrightarrow>
  wellTypedLinearGuardExpression tyCtx g2 \<longrightarrow>
  wellTypedLinearGuardExpression tyCtx (interleaveLinearTreesRightFirst g1 g2)"
proof (induction g1 g2 f and g1 g2 rule: interleaveLinearTrees_interleaveLinearTreesRightFirst.induct)
    case (1 a g2 f)
  then show ?case by (cases f, auto)
next
  case (2 innerG1 innerG2 g2 f)
  then show ?case by (cases f, auto)
next
  case (3 innerG1 stmts g2 f)
  then show ?case by (smt (verit, best) interleaveLinearTrees.simps(3) wellTypedLinearGuardExpression.simps(2) wellTypedLinearGuardExpression.simps)
next
  case (4 node g2)
  then show ?case by auto
next
  case (5 node gCont g2)
  then show ?case by auto
next
  case (6 innerG1 innerG2 g2)
  then show ?case by auto
qed

lemma addContinuationPreservesWellTyped:
"\<forall>tyCtx . wellTypedLinearGuardExpression tyCtx g1 \<longrightarrow>
  wellTypedLinearGuardExpression tyCtx g2 \<longrightarrow>
  wellTypedLinearGuardExpression tyCtx (addLinearGuardContinuation g2 g1)"
proof (induction g2 g1 rule: addLinearGuardContinuation.induct)
  case (1 gCont a stmts)
  then show ?case by auto
next
  case (2 gCont node innerG)
  then show ?case by auto
next
  case (3 gCont g1 g2)
  then show ?case by auto
qed

lemma wellTypedStatementsAppend:
"\<forall>tyCtx . wellTypedStatements tyCtx stmts1 \<longrightarrow>
  wellTypedStatements tyCtx stmts2 \<longrightarrow>
  wellTypedStatements tyCtx (stmts1 @ stmts2)"
proof (induction stmts1)
  case Nil
  then show ?case by auto
next
  case (Cons s stmts1)
  then show ?case apply (cases s, auto)
    by (metis eq_Nil_appendI lookupTypeConsResMaintainsStatementsTypeCorrectness)+
qed

lemma addStatementsContinuationPreservesWellTyped:
"\<forall>tyCtx . wellTypedLinearGuardExpression tyCtx g1 \<longrightarrow>
  wellTypedStatements tyCtx stmts \<longrightarrow>
  wellTypedLinearGuardExpression tyCtx (addStatementsContinuation stmts g1)"
proof (induction stmts g1 rule: addStatementsContinuation.induct)
  case (1 stmts a innerStmts)
  then show ?case by (auto simp add: wellTypedStatementsAppend)
next
  case (2 stmts node gCont)
  then show ?case by auto
next
  case (3 stmts g1 g2)
  then show ?case by auto
qed

lemma linearizingPreservesWellTyped:
"\<forall>tyCtx . wellTypedGuardExpression tyCtx g \<longrightarrow> wellTypedLinearGuardExpression tyCtx (linearizeGuardExpression g)"
proof (induction rule: linearizeGuardExpression.induct)
  case (1 a)
  then show ?case by auto
next
  case (2 g1 g2)
  then show ?case by (auto simp add: addContinuationPreservesWellTyped)
next
  case (3 g stmts)
  then show ?case by (auto simp add: addStatementsContinuationPreservesWellTyped)
next
  case (4 g1 g2)
  then show ?case by auto
next
  case (5 g1 g2)
  then show ?case by (simp add: interleavingPreservesWellTyped)
qed

lemma wellTypedLinearGuardNodeCompilesToWellTypedCase:
"(\<forall>tyCtx . (wellTypedLinearGuardNode tyCtx node \<longrightarrow>
  (\<forall>cont t tcont. FaustusSemantics.wellTypedContract tyCtx cont \<longrightarrow> FaustusSemantics.wellTypedCases tyCtx [compileLinearGuardNode node cont])))"
proof (induction rule: compileLinearGuardNode.induct)
  case (1 a stmts cont)
  then show ?case by (cases a, auto simp add: wellTypedStatementsCompileToWellTypedContract)
qed

lemma wellTypedLinearGuardCompilesToWellTypedCases:
"(\<forall>tyCtx . (wellTypedLinearGuardExpression tyCtx ge \<longrightarrow>
  (\<forall>cont t tcont. FaustusSemantics.wellTypedContract tyCtx cont \<longrightarrow> FaustusSemantics.wellTypedContract tyCtx tcont \<longrightarrow> FaustusSemantics.wellTypedCases tyCtx (compileLinearGuardExpression ge cont t tcont))))"
proof (induction rule: compileLinearGuardExpression.induct)
  case (1 a cont t tcont)
  then show ?case by (auto simp add: wellTypedLinearGuardNodeCompilesToWellTypedCase)
next
  case (2 g1 g2 cont t tcont)
  then show ?case apply auto
    using wellTypedLinearGuardNodeCompilesToWellTypedCase FaustusSemantics.wellTypedContract.simps(4) by presburger
next
  case (3 g1 stmts cont t tcont)
  then show ?case apply auto
    using wellTypedCasesAppend by presburger
qed

lemma wellTypedGuardCompilesToWellTypedCases:
"(\<forall>tyCtx . (wellTypedGuardExpression tyCtx ge \<longrightarrow>
  (\<forall>cont t tcont. FaustusSemantics.wellTypedContract tyCtx cont \<longrightarrow> FaustusSemantics.wellTypedContract tyCtx tcont \<longrightarrow> FaustusSemantics.wellTypedCases tyCtx (compileGuardExpression ge cont t tcont))))"
proof (induction rule: wellTypedGuardExpression.induct)
  case (1 tyCtx a)
  then show ?case by (cases a, auto)
next
  case (2 tyCtx g1 g2)
  then show ?case apply auto
    using addContinuationPreservesWellTyped linearizingPreservesWellTyped wellTypedLinearGuardCompilesToWellTypedCases by presburger
next
  case (3 tyCtx g stmts)
  then show ?case by (auto simp add: addStatementsContinuationPreservesWellTyped linearizingPreservesWellTyped wellTypedLinearGuardCompilesToWellTypedCases)
next
  case (4 tyCtx g1 g2)
  then show ?case by (simp add: wellTypedCasesAppend)
next
  case (5 tyCtx g1 g2)
  then show ?case by (simp add: interleavingPreservesWellTyped linearizingPreservesWellTyped wellTypedLinearGuardCompilesToWellTypedCases)
qed

lemma wellTypedCompilesToWellTyped:
"(\<forall>tyCtx . (wellTypedContract tyCtx contract \<longrightarrow>
  (FaustusSemantics.wellTypedContract tyCtx (compile contract))))"
"(\<forall>tyCtx .(wellTypedCases tyCtx cases \<longrightarrow>
  (\<forall>t tcont . \<exists>mCases. FaustusSemantics.wellTypedContract tyCtx tcont \<longrightarrow> FaustusSemantics.wellTypedCases tyCtx (compileCases cases t tcont))))"
proof (induction rule: compile_compileCases.induct)
  case (1)
  then show ?case by auto
next
  case (2 s cont)
  then show ?case by (cases s, auto simp add: wellTypedStatementCompilesToWellTypedContract)
next
  case (3 obs trueCont falseCont)
  then show ?case by auto
next
  case (4 cases t tCont)
  then show ?case apply auto
    by (meson FaustusSemantics.wellTypedContract.simps(4))
next
  case (5 vid val cont)
  then show ?case by auto
next
  case (6 obsId obs cont)
  then show ?case by auto
next
  case (7 pkId pk cont)
  then show ?case by auto
next
  case (8 cid params bodyCon cont)
  then show ?case by auto
next
  case (9 cid args)
  then show ?case by auto
next
  case (10 g cont rest t tcont)
  then show ?case by (simp add: linearizingPreservesWellTyped wellTypedCasesAppend wellTypedLinearGuardCompilesToWellTypedCases)
next
  case (11 t tcont)
  then show ?case by auto
qed

fun compileCtx :: "FContext \<Rightarrow> FaustusTypes.FContext" where
"compileCtx [] = []" |
"compileCtx ((PubKeyAbstraction i)#rest) = (FaustusTypes.PubKeyAbstraction i)#(compileCtx rest)" |
"compileCtx ((ValueAbstraction i)#rest) = (FaustusTypes.ValueAbstraction i)#(compileCtx rest)" |
"compileCtx ((ObservationAbstraction i)#rest) = (FaustusTypes.ObservationAbstraction i)#(compileCtx rest)" |
"compileCtx ((ContractAbstraction (i, params, body))#rest) = (FaustusTypes.ContractAbstraction (i, params, (compile body)))#(compileCtx rest)"

lemma compileCtxPreservesWellTyped:
"(\<forall>tyCtx . (wellTypedContext tyCtx ctx \<longrightarrow>
  (FaustusSemantics.wellTypedContext tyCtx (compileCtx ctx))))"
proof (induction rule: compileCtx.induct)
  case 1
  then show ?case by (metis FaustusSemantics.wellTypedContext.simps(1) wellTypedContext.simps(2) compileCtx.simps(1) neq_Nil_conv)
next
  case (2 i rest)
  then show ?case by (smt (verit) FaustusSemantics.wellTypedContext.elims(1) FaustusTypes.AbstractionInformation.distinct(11) FaustusTypes.AbstractionInformation.distinct(3) FaustusTypes.AbstractionInformation.distinct(7) FaustusTypes.AbstractionInformation.inject(3) wellTypedContext.simps(20) wellTypedContext.simps(21) wellTypedContext.simps(22) wellTypedContext.simps(3) wellTypedContext.simps(5) compileCtx.simps(2) list.discI list.inject)
next
  case (3 i rest)
  then show ?case using wellTypedContext.elims(2) by force
next
  case (4 i rest)
  then show ?case using wellTypedContext.elims(2) by force
next
  case (5 i params body rest)
  then show ?case by (smt (verit, best) wellTypedContext.simps wellTypedCompilesToWellTyped FaustusSemantics.wellTypedContext.simps(7) wellTypedContext.elims(2) compileCtx.simps(5))
qed

lemma compileCtxPreservesLookup:
"lookupContractIdAbsInformation ctx i = Some (params, body, boundCtx) \<longrightarrow>
  FaustusSemantics.lookupContractIdAbsInformation (compileCtx ctx) i = Some (params, compile body, compileCtx boundCtx)"
proof (induction ctx)
  case Nil
  then show ?case by auto
next
  case (Cons head ctx)
  then show ?case by (cases head, auto)
qed

lemma compileCtxDistributesIntoParamsToCtx:
"compileCtx (paramsToFContext params @ innerCtx) = FaustusTypes.paramsToFContext params @ (compileCtx innerCtx)"
proof (induction params)
  case Nil
  then show ?case by auto
next
  case (Cons a params)
  then show ?case by (cases a, auto)
qed

fun translateV2ContractToV1ContextChange :: "FContract \<Rightarrow> FaustusTypes.FContext \<Rightarrow> FaustusTypes.FContext" where
"translateV2ContractToV1ContextChange (StatementCont (ReassignVal vid val) _) ctx = (FaustusTypes.ValueAbstraction vid)#ctx" |
"translateV2ContractToV1ContextChange (StatementCont (ReassignObservation obsId obs) _) ctx = (FaustusTypes.ObservationAbstraction obsId)#ctx" |
"translateV2ContractToV1ContextChange (StatementCont (ReassignPubKey pkId pk) _) ctx = (FaustusTypes.PubKeyAbstraction pkId)#ctx" |
"translateV2ContractToV1ContextChange _ ctx = ctx"

(* Zero or more well typed Faustus steps correspond
  to zero or more Marlowe steps. *)
lemma preservationOfSemantics_FaustusV2StepFaustusV1Step:
"(fContract, ctx, state, env, warnings, payments) \<rightarrow>\<^sub>f\<^sub>2 (newFContract, newCtx, newState, newEnv, newWarnings, newPayments) \<Longrightarrow>
  compile fContract = fV1Contract \<Longrightarrow>
  compile newFContract = newFV1Contract \<Longrightarrow>
  ((fV1Contract, compileCtx ctx, state, env, warnings, payments) \<rightarrow>\<^sub>f (newFV1Contract, (compileCtx newCtx), newState, newEnv, newWarnings, newPayments))"
proof (induction "(fContract, ctx, state, env, warnings, payments)" "(newFContract, newCtx, newState, newEnv, newWarnings, newPayments)" rule: small_step_reduce.induct)
  case (WhenTimeout startSlot endSlot timeout cases)
  then show ?case apply auto
    by (meson FaustusSemantics.small_step_reduce.WhenTimeout)
next
  case (UseCFound cid params innerCtx args)
  then show ?case using compileCtxPreservesLookup FaustusSemantics.small_step_reduce.UseCFound compileCtxDistributesIntoParamsToCtx by auto
qed auto

(* If a well typed Faustus contract executes to a certain state in any number of steps,
  then the compiled Marlowe contract will execute to the same state.*)
lemma preservationOfSemantics_FaustusV2StepsImpFaustusV1Steps:
"(fContract, ctx, state, env, warnings, payments) \<rightarrow>\<^sub>f\<^sub>2* (newFContract, newCtx, newState, newEnv, newWarnings, newPayments) \<Longrightarrow>
  wellTypedContract tyCtx fContract \<Longrightarrow>
  wellTypedContext tyCtx ctx \<Longrightarrow>
  wellTypedState tyCtx state \<Longrightarrow>
  ((compile fContract), compileCtx ctx, state, env, warnings, payments) \<rightarrow>\<^sub>f*
    (compile newFContract, compileCtx newCtx, newState, newEnv, newWarnings, newPayments)"
proof (induction arbitrary: tyCtx rule: star.induct[of small_step_reduce, split_format(complete)])
  case (refl)
  then show ?case by auto
next
  case (step)
  then show ?case by (metis (no_types, lifting) typePreservation preservationOfSemantics_FaustusV2StepFaustusV1Step star.simps)
qed

lemma preservationOfSemantics_HaltedFaustusV2ImpliesHaltedFaustusV1:
"\<forall>ctx state env warnings payments tyCtx . finalV2 (fContract, ctx, state, env, warnings, payments) \<longrightarrow>
  wellTypedContract tyCtx fContract \<longrightarrow>
  wellTypedContext tyCtx ctx \<longrightarrow>
  wellTypedState tyCtx state \<longrightarrow>
  final (compile fContract, compileCtx ctx, state, env, warnings, payments)"
proof (cases fContract)
  case Close
  then show ?thesis apply(auto simp add: finalV2_def final_def)
    by blast
next
  case (StatementCont s c)
  then show ?thesis using finalV2D by blast
next
  case (If x31 x32 x33)
  then show ?thesis using finalV2D by blast
next
  case (When x41 timeout x43)
  then show ?thesis apply (auto simp add: finalV2D final_def)
    by (meson FaustusSemantics.WhenE small_step_reduce.WhenTimeout finalV2_def)
next
  case (Let x51 x52 x53)
  then show ?thesis using finalV2D by blast
next
  case (LetObservation x61 x62 x63)
  then show ?thesis using finalV2D by blast
next
  case (LetPubKey x71 x72 x73)
  then show ?thesis using finalV2D by blast
next
  case (LetC x91 x92 x93 x94)
  then show ?thesis using finalV2D by blast
next
  case (UseC x101 x102)
  then show ?thesis using finalV2D by blast
qed

datatype FaustusReduceStatementResult = StatementReduced ReduceWarning ReduceEffect FState
                          | StatementUnboundIdentifierError

fun reduceStatement :: "Environment \<Rightarrow> FState \<Rightarrow> FStatement \<Rightarrow> FaustusReduceStatementResult" where
"reduceStatement env state (Pay faustusAccId faustusPayee token val) =
  (case (evalFParty state faustusAccId, evalFPayee state faustusPayee, evalFValue env state val) of 
    (None, _, _) \<Rightarrow> StatementUnboundIdentifierError
    | (_, None, _) \<Rightarrow> StatementUnboundIdentifierError
    | (_, _, None) \<Rightarrow> StatementUnboundIdentifierError
    | (Some accId, Some payee, Some moneyToPay) \<Rightarrow>
     (if moneyToPay \<le> 0
       then (let warning = ReduceNonPositivePay accId payee token moneyToPay in
             StatementReduced warning ReduceNoPayment state)
       else (let balance = moneyInAccount accId token (accounts state) in
            (let paidMoney = min balance moneyToPay in
             let newBalance = balance - paidMoney in
             let newAccs = updateMoneyInAccount accId token newBalance (accounts state) in
             let warning = (if paidMoney < moneyToPay
                            then ReducePartialPay accId payee token paidMoney moneyToPay
                            else ReduceNoWarning) in
             let (payment, finalAccs) = giveMoney accId payee token paidMoney newAccs in
             StatementReduced warning payment (state \<lparr> accounts := finalAccs \<rparr>)))))" |
"reduceStatement env state (Assert obs) =
  (case evalFObservation env state obs of
    None \<Rightarrow> StatementUnboundIdentifierError
    | Some evaluatedObservation \<Rightarrow> (let warning = if evaluatedObservation
                 then ReduceNoWarning
                 else ReduceAssertionFailed
   in StatementReduced warning ReduceNoPayment state))" |
"reduceStatement env state (ReassignVal valId val) = 
  (case (evalFValue env state val, lookup valId (boundValues state)) of
    (None, _) \<Rightarrow> StatementUnboundIdentifierError
    | (Some evaluatedVal, None) \<Rightarrow> StatementReduced ReduceNoWarning ReduceNoPayment (state\<lparr>boundValues := MList.insert valId evaluatedVal (boundValues state)\<rparr>)
    | (Some evaluatedVal, Some oldVal) \<Rightarrow> StatementReduced (ReduceShadowing valId oldVal evaluatedVal) ReduceNoPayment (state\<lparr>boundValues := MList.insert valId evaluatedVal (boundValues state)\<rparr>))" |
"reduceStatement env state (ReassignObservation obsId obs) = 
  (case (evalFObservation env state obs) of
    (None) \<Rightarrow> StatementUnboundIdentifierError
    | (Some evaluatedObs) \<Rightarrow> StatementReduced ReduceNoWarning ReduceNoPayment (state\<lparr>boundValues := MList.insert obsId (if evaluatedObs then 1 else 0) (boundValues state)\<rparr>))" |
"reduceStatement env state (ReassignPubKey pkId pk) = 
  (case (evalFParty state pk) of
    (None) \<Rightarrow> StatementUnboundIdentifierError
    | (Some evaluatedPk) \<Rightarrow> StatementReduced ReduceNoWarning ReduceNoPayment (state\<lparr>boundPubKeys := MList.insert pkId evaluatedPk (boundPubKeys state)\<rparr>))"

fun updateCtxFromStmt :: "FContext \<Rightarrow> FStatement \<Rightarrow> FContext" where
"updateCtxFromStmt ctx (ReassignVal valId val) = (ValueAbstraction valId#ctx)" |
"updateCtxFromStmt ctx (ReassignObservation obsId obs) = (ObservationAbstraction obsId#ctx)" |
"updateCtxFromStmt ctx (ReassignPubKey pkId pk) = ((PubKeyAbstraction pkId)#ctx)" |
"updateCtxFromStmt ctx _ = ctx"

datatype FaustusReduceStepResult = Reduced FContext ReduceWarning ReduceEffect FState FContract
                          | NotReduced
                          | AmbiguousSlotIntervalReductionError
                          | UnboundIdentifierError
                          | ArgumentEvaluationError

fun reduceContractStep :: "FContext \<Rightarrow> Environment \<Rightarrow> FState \<Rightarrow> FContract \<Rightarrow> FaustusReduceStepResult" where
"reduceContractStep ctx _ state Close =
  (case refundOne (accounts state) of
     Some ((party, token, money), newAccount) \<Rightarrow>
       let newState = state \<lparr> accounts := newAccount \<rparr> in
       Reduced ctx ReduceNoWarning (ReduceWithPayment (Payment party (SemanticsTypes.Party party) token money)) newState Close
   | None \<Rightarrow> NotReduced)" |
"reduceContractStep ctx env state (StatementCont stmt cont) =
  (case reduceStatement env state stmt of
    StatementReduced warning payment newState \<Rightarrow> Reduced (updateCtxFromStmt ctx stmt) warning payment newState cont
    | StatementUnboundIdentifierError \<Rightarrow> UnboundIdentifierError)" |
"reduceContractStep ctx env state (If obs cont1 cont2) =
  (case evalFObservation env state obs of
    None \<Rightarrow> UnboundIdentifierError
    | Some evaluatedObservation \<Rightarrow> (let cont = (if evaluatedObservation then cont1 else cont2) in
       Reduced ctx ReduceNoWarning ReduceNoPayment state cont))" |
"reduceContractStep ctx env state (When _ timeout cont) =
  (let (startSlot, endSlot) = timeInterval env in
   if endSlot < timeout
   then NotReduced
   else (if timeout \<le> startSlot
         then Reduced ctx ReduceNoWarning ReduceNoPayment state cont
         else AmbiguousSlotIntervalReductionError))" |
"reduceContractStep ctx env state (Let valId val cont) =
  (case evalFValue env state val of
    None \<Rightarrow> UnboundIdentifierError
    | Some evaluatedValue \<Rightarrow> (let boundVals = boundValues state in
      let newState = state \<lparr> boundValues := MList.insert valId evaluatedValue boundVals\<rparr> in
      let warn = case lookup valId boundVals of
                  Some oldVal \<Rightarrow> ReduceShadowing valId oldVal evaluatedValue
                | None \<Rightarrow> ReduceNoWarning in
      Reduced (ValueAbstraction valId#ctx) warn ReduceNoPayment newState cont))" |
"reduceContractStep ctx env state (LetObservation obsId obs cont) =
  (case evalFObservation env state obs of
    None \<Rightarrow> UnboundIdentifierError
    | Some evaluatedObservation \<Rightarrow> (let boundVals = boundValues state in
      let newState = state \<lparr> boundValues := MList.insert obsId (if evaluatedObservation then 1 else 0) boundVals \<rparr> in
      Reduced (ObservationAbstraction obsId#ctx) ReduceNoWarning ReduceNoPayment newState cont))" |
"reduceContractStep ctx env state (LetPubKey pkId pk cont) =
  (case evalFParty state pk of
    None \<Rightarrow> UnboundIdentifierError
    | Some evaluatedPubKey \<Rightarrow> (let boundPubKeys = boundPubKeys state in
      let newState = state \<lparr> boundPubKeys := MList.insert pkId evaluatedPubKey boundPubKeys \<rparr> in
      Reduced (PubKeyAbstraction pkId#ctx) ReduceNoWarning ReduceNoPayment newState cont))" |
"reduceContractStep ctx env state (LetC cid params boundCon cont) = Reduced (ContractAbstraction (cid, params, boundCon)#ctx) ReduceNoWarning ReduceNoPayment state cont" |
"reduceContractStep ctx env state (UseC cid args) = (
  case lookupContractIdAbsInformation ctx cid of
    None \<Rightarrow> UnboundIdentifierError
    | Some (params, bodyCont, innerCtx) \<Rightarrow> (case evalFArguments env state params args of
      None \<Rightarrow> ArgumentEvaluationError
      | Some newState \<Rightarrow> Reduced ((paramsToFContext params)@innerCtx) ReduceNoWarning ReduceNoPayment newState bodyCont))"

(* If reduce step function reduces a contract, then a small step reduction is possible. 
datatype ReduceEffect = ReduceNoPayment
                      | ReduceWithPayment Payment*)
fun convertReduceEffect :: "ReduceEffect \<Rightarrow> Payment list" where
"convertReduceEffect ReduceNoPayment = []" |
"convertReduceEffect (ReduceWithPayment p) = [p]"

lemma reduceStepIsSmallStepReduction:
assumes "reduceContractStep ctx env state contract = Reduced newCtx warning paymentReduceResult newState continueContract"
shows "(contract, ctx, state, env, initialWarnings, initialPayments) \<rightarrow>\<^sub>f\<^sub>2 (continueContract, newCtx, newState, env, initialWarnings @ [warning], initialPayments @ (convertReduceEffect paymentReduceResult))"
proof (cases contract paymentReduceResult rule: FContract.exhaust[case_product ReduceEffect.exhaust])
  case Close_ReduceNoPayment
  then show ?thesis using assms apply auto
    by (cases "refundOne (accounts state)", auto)
next
  case (Close_ReduceWithPayment reducePayment)
  then show ?thesis using assms apply auto
    by (cases "refundOne (accounts state)", auto)
next
  case (StatementCont_ReduceNoPayment s cont)
  then show ?thesis using assms apply auto
    subgoal premises ps proof (cases s)
      case (Pay accId payee token val)
      then show ?thesis using ps apply (cases "evalFParty state accId" "evalFPayee state payee" "evalFValue env state val" rule: option.exhaust[case_product option.exhaust[case_product option.exhaust]], auto)
    apply (split if_split_asm, auto)
    subgoal premises ps proof -
      obtain evaluatedPubKey where "evalFParty state accId = Some evaluatedPubKey" using ps by auto
      moreover obtain evaluatedPayee where "evalFPayee state payee = Some evaluatedPayee" using ps by auto
      moreover obtain evaluatedValue where "evalFValue env state val = Some evaluatedValue" using ps by auto
      moreover obtain finalAccs where "giveMoney evaluatedPubKey evaluatedPayee token 0 (MList.delete (evaluatedPubKey, token) (FState.accounts state)) = (ReduceNoPayment, finalAccs)" using ps calculation apply auto
        by (metis FaustusReduceStatementResult.simps(4) FaustusReduceStepResult.inject ReduceEffect.distinct(1))
      moreover have "(FContract.StatementCont (FStatement.Pay accId payee token val) continueContract, newCtx, state, env, initialWarnings, initialPayments) \<rightarrow>\<^sub>f\<^sub>2 (continueContract, newCtx, state\<lparr>FState.accounts := finalAccs\<rparr>, env, initialWarnings @ [ReducePartialPay evaluatedPubKey evaluatedPayee token 0 evaluatedValue], initialPayments)" using calculation ps by auto
      ultimately show ?thesis using ps by auto
    qed
    done
    next
      case (Assert x2)
      then show ?thesis using ps by (auto split: option.split_asm)
    next
      case (ReassignVal)
      then show ?thesis using ps by (auto split: FaustusReduceStatementResult.split_asm option.split_asm)
    next
      case (ReassignObservation)
      then show ?thesis using ps by (auto split: FaustusReduceStatementResult.split_asm option.split_asm)
    next
      case (ReassignPubKey)
      then show ?thesis using ps by (auto split: FaustusReduceStatementResult.split_asm option.split_asm)
    qed
    done
next
  case (StatementCont_ReduceWithPayment s cont payment)
  then show ?thesis using assms apply auto
    subgoal premises ps proof (cases s)
      case (Pay accId payee token val)
      then show ?thesis using ps apply (cases "evalFParty state accId" "evalFPayee state payee" "evalFValue env state val" rule: option.exhaust[case_product option.exhaust[case_product option.exhaust]], auto)
    apply (split if_split_asm, auto)
    apply (split option.split_asm, auto simp add: min_def)
    subgoal premises ps proof -
      obtain evaluatedPubKey where "evalFParty state accId = Some evaluatedPubKey" using ps by auto
      moreover obtain evaluatedPayee where "evalFPayee state payee = Some evaluatedPayee" using ps by auto
      moreover obtain evaluatedValue where "evalFValue env state val = Some evaluatedValue" using ps by auto
      moreover obtain finalAccs where "giveMoney evaluatedPubKey evaluatedPayee token 0 (MList.delete (evaluatedPubKey, token) (FState.accounts state)) = (ReduceWithPayment payment, finalAccs)" using ps calculation apply auto
        by (metis FaustusReduceStatementResult.simps(4) FaustusReduceStepResult.inject ReduceEffect.inject)
      moreover have "(StatementCont (Pay accId payee token val) continueContract, newCtx, state, env, initialWarnings, initialPayments) \<rightarrow>\<^sub>f\<^sub>2 (continueContract, newCtx, state\<lparr>FState.accounts := finalAccs\<rparr>, env, initialWarnings @ [ReducePartialPay evaluatedPubKey evaluatedPayee token 0 evaluatedValue], initialPayments @ [payment])" using calculation ps PayPositivePartialWithPayment by auto
      ultimately show ?thesis using ps by auto
    qed
    apply (auto split: if_split_asm)
    subgoal premises ps proof -
      obtain evaluatedPubKey where "evalFParty state accId = Some evaluatedPubKey" using ps by auto
      moreover obtain evaluatedPayee where "evalFPayee state payee = Some evaluatedPayee" using ps by auto
      moreover obtain evaluatedValue where "evalFValue env state val = Some evaluatedValue" using ps by auto
      moreover have "evaluatedValue > 0" using ps calculation by auto
      moreover obtain moneyToPay where "moneyInAccount evaluatedPubKey token (FState.accounts state) = moneyToPay" using ps calculation by auto
      moreover obtain availableMoney where "lookup (evaluatedPubKey, token) (FState.accounts state) = Some availableMoney" using ps calculation by auto
      moreover have "moneyToPay = availableMoney" using calculation ps by auto
      moreover obtain finalAccs where "giveMoney evaluatedPubKey evaluatedPayee token availableMoney (MList.delete (evaluatedPubKey, token) (FState.accounts state)) = (ReduceWithPayment payment, finalAccs)" using ps calculation apply auto
        by (metis FaustusReduceStatementResult.simps(4) FaustusReduceStepResult.inject ReduceEffect.inject)
      moreover have "newCtx = ctx" using ps calculation apply auto
        by (metis FaustusReduceStatementResult.simps(4) FaustusReduceStepResult.inject updateCtxFromStmt.simps(4))
      moreover have "evaluatedValue > moneyToPay" using ps calculation by auto
      moreover have "(StatementCont (Pay accId payee token val) continueContract, newCtx, state, env, initialWarnings, initialPayments) \<rightarrow>\<^sub>f\<^sub>2 (continueContract, newCtx, state\<lparr>FState.accounts := finalAccs\<rparr>, env, initialWarnings @ [ReducePartialPay evaluatedPubKey evaluatedPayee token availableMoney evaluatedValue], initialPayments @ [payment])" using calculation by auto
      ultimately show ?thesis using ps apply auto
        by (metis FaustusReduceStatementResult.simps(4) FaustusReduceStepResult.inject \<open>(StatementCont (FStatement.Pay accId payee token val) continueContract, newCtx, state, env, initialWarnings, initialPayments) \<rightarrow>\<^sub>f\<^sub>2 (continueContract, newCtx, state \<lparr>FState.accounts := finalAccs\<rparr>, env, initialWarnings @ [ReducePartialPay evaluatedPubKey evaluatedPayee token availableMoney evaluatedValue], initialPayments @ [payment])\<close>)
    qed
    subgoal premises ps proof -
      obtain evaluatedPubKey where "evalFParty state accId = Some evaluatedPubKey" using ps by auto
      moreover obtain evaluatedPayee where "evalFPayee state payee = Some evaluatedPayee" using ps by auto
      moreover obtain evaluatedValue where "evalFValue env state val = Some evaluatedValue" using ps by auto
      moreover obtain availableMoney where "lookup (evaluatedPubKey, token) (FState.accounts state) = Some availableMoney" using ps calculation by auto
      moreover obtain finalAccs where "giveMoney evaluatedPubKey evaluatedPayee token availableMoney (MList.delete (evaluatedPubKey, token) (FState.accounts state)) = (ReduceWithPayment payment, finalAccs)" using ps calculation apply auto
        by (metis FaustusReduceStatementResult.simps(4) FaustusReduceStepResult.inject ReduceEffect.inject)
      moreover have "(StatementCont (Pay accId payee token val) continueContract, newCtx, state, env, initialWarnings, initialPayments) \<rightarrow>\<^sub>f\<^sub>2 (continueContract, newCtx, state\<lparr>FState.accounts := finalAccs\<rparr>, env, initialWarnings @ [ReduceNoWarning], initialPayments @ [payment])" using calculation ps PayPositivePartialWithPayment by auto
      ultimately show ?thesis using ps apply auto
        by (metis FaustusReduceStatementResult.simps(4) updateCtxFromStmt.simps(4) FaustusReduceStepResult.inject \<open>(StatementCont (FStatement.Pay accId payee token val) continueContract, newCtx, state, env, initialWarnings, initialPayments) \<rightarrow>\<^sub>f\<^sub>2 (continueContract, newCtx, state \<lparr>FState.accounts := finalAccs\<rparr>, env, initialWarnings @ [ReduceNoWarning], initialPayments @ [payment])\<close> ps(9))
    qed
    subgoal premises ps proof -
      obtain evaluatedPubKey where "evalFParty state accId = Some evaluatedPubKey" using ps by auto
      moreover obtain evaluatedPayee where "evalFPayee state payee = Some evaluatedPayee" using ps by auto
      moreover obtain evaluatedValue where "evalFValue env state val = Some evaluatedValue" using ps by auto
      moreover obtain availableMoney where "lookup (evaluatedPubKey, token) (FState.accounts state) = Some availableMoney" using ps calculation by auto
      moreover obtain finalAccs where "giveMoney evaluatedPubKey evaluatedPayee token evaluatedValue (MList.insert (evaluatedPubKey, token) (availableMoney - evaluatedValue) (FState.accounts state)) = (ReduceWithPayment payment, finalAccs)" using ps calculation apply auto
        by (metis FaustusReduceStatementResult.simps(4) FaustusReduceStepResult.inject ReduceEffect.inject)
      moreover have "(StatementCont (Pay accId payee token val) continueContract, newCtx, state, env, initialWarnings, initialPayments) \<rightarrow>\<^sub>f\<^sub>2 (continueContract, newCtx, state\<lparr>FState.accounts := finalAccs\<rparr>, env, initialWarnings @ [ReduceNoWarning], initialPayments @ [payment])" using calculation ps by auto
      ultimately show ?thesis using ps by auto
    qed
    done
    next
      case (Assert x2)
      then show ?thesis using ps by (auto split: option.split_asm)
    next
      case (ReassignVal)
      then show ?thesis using ps by (auto split: FaustusReduceStatementResult.split_asm option.split_asm)
    next
      case (ReassignObservation)
      then show ?thesis using ps by (auto split: FaustusReduceStatementResult.split_asm option.split_asm)
    next
      case (ReassignPubKey)
      then show ?thesis using ps by (auto split: FaustusReduceStatementResult.split_asm option.split_asm)
    qed
    done
next
  case (If_ReduceNoPayment obs x32 x33)
  then show ?thesis using assms by (auto split: option.split_asm)
next
  case (If_ReduceWithPayment x31 x32 x33 x2)
  then show ?thesis using assms by (auto split: option.split_asm)
next
  case (When_ReduceNoPayment x41 timeout x43)
  then show ?thesis using assms apply auto
    subgoal premises ps proof (cases "timeInterval env")
      case (Pair ss es)
      then show ?thesis using ps apply auto
        apply (cases "es < timeout", auto)
        by (cases "timeout \<le> ss", auto)
    qed
    done
next
  case (When_ReduceWithPayment x41 x42 x43 x2)
  then show ?thesis using assms by (auto split: if_split_asm prod.split_asm)
next
  case (Let_ReduceNoPayment vid x52 x53)
  then show ?thesis using assms apply (auto split: option.split_asm)
    by (cases "lookup vid (boundValues state)", auto)
next
  case (Let_ReduceWithPayment vid x52 x53 x2)
  then show ?thesis using assms apply (auto split: option.split_asm)
    by (cases "lookup vid (boundValues state)", auto)
next
  case (LetObservation_ReduceNoPayment obsId x52 x53)
  then show ?thesis using assms apply (simp split: option.split_asm)
    by (metis (no_types, lifting) LetObservation)
next
  case (LetObservation_ReduceWithPayment vid x52 x53 x2)
  then show ?thesis using assms by (auto split: option.split_asm)
next
  case (LetPubKey_ReduceNoPayment pkId x52 x53)
  then show ?thesis using assms apply (simp split: option.split_asm)
    by (metis (no_types, lifting) LetPubKey)
next
  case (LetPubKey_ReduceWithPayment vid x52 x53 x2)
  then show ?thesis using assms by (auto split: option.split_asm)
next
  case (LetC_ReduceNoPayment cid x72 x73)
  then show ?thesis using assms by auto
next
  case (LetC_ReduceWithPayment cid x72 x73 x2)
  then show ?thesis using assms by auto
next
  case (UseC_ReduceNoPayment cid)
  then show ?thesis using assms by (auto split: option.split_asm)
next
  case (UseC_ReduceWithPayment cid x2)
  then show ?thesis using assms by (auto split: option.split_asm)
qed

(* If a small step reduction is possible, then the reduce step function will reduce the contract *)
lemma smallStepReductionImpReduceStep:
assumes "cs = (contract, ctx, state, env, initialWarnings, initialPayments) \<and>
    cs' = (continueContract, newCtx, newState, newEnv, newWarnings, newPayments) \<and>
    cs \<rightarrow>\<^sub>f\<^sub>2 cs'"
shows "\<exists>warning paymentReduceResult . reduceContractStep ctx env state contract = Reduced newCtx warning paymentReduceResult newState continueContract \<and>
  newPayments = initialPayments @ (convertReduceEffect paymentReduceResult) \<and>
  newWarnings = initialWarnings @ [warning]"
proof (cases contract)
  case Close
  then show ?thesis using assms by auto
next
  case (StatementCont s cont)
  then show ?thesis using assms apply auto
    apply (cases s, auto)
    apply (split option.split option.split_asm, auto)[4]
       apply (auto split: option.split_asm)
    using FaustusReduceStatementResult.simps(4) convertReduceEffect.simps(2) apply (auto split: option.split)
            apply (metis)
    by (metis, metis updateCtxFromStmt.simps(4) FaustusReduceStatementResult.simps(4) convertReduceEffect.simps(2))+
next
  case (If x31 x32 x33)
  then show ?thesis using assms by auto
next
  case (When cases timeout cont)
  then show ?thesis using assms by auto
next
  case (Let x51 x52 x53)
  then show ?thesis using assms by auto
next
  case (LetObservation x51 x52 x53)
  then show ?thesis using assms by auto
next
  case (LetPubKey x51 x52 x53)
  then show ?thesis using assms by auto
next
  case (LetC x71 x72 x73)
  then show ?thesis using assms by auto
next
  case (UseC x8)
  then show ?thesis using assms by (auto split: option.split)
qed

lemma wellTypedStatementNeverGetsUnboundIdentifierError:
"wellTypedStatement tyCtx stmt \<Longrightarrow>
  wellTypedContext tyCtx ctx \<Longrightarrow>
  wellTypedState tyCtx state \<Longrightarrow>
  reduceStatement env state stmt \<noteq> StatementUnboundIdentifierError"
  apply (cases stmt)
   apply (auto split: option.split_asm if_split_asm prod.split_asm FaustusReduceStatementResult.split FaustusReduceStatementResult.split_asm simp add: wellTypedPartyEvaluates wellTypedValueObservationEvaluates wellTypedPayeeEvaluates min_def)
  by (metis FaustusReduceStatementResult.distinct(1))+


lemma wellTypedContractNeverGetsUnboundIdentifierError:
"wellTypedContract tyCtx contract \<Longrightarrow>
  wellTypedContext tyCtx ctx \<Longrightarrow>
  wellTypedState tyCtx state \<Longrightarrow>
  reduceContractStep ctx env state contract \<noteq> UnboundIdentifierError"
  apply (induction contract rule: reduceContractStep.induct, auto)
         apply (auto split: option.split_asm if_split_asm prod.split_asm FaustusReduceStatementResult.split FaustusReduceStatementResult.split_asm simp add: wellTypedPartyEvaluates wellTypedValueObservationEvaluates wellTypedPayeeEvaluates wellTypedStatementNeverGetsUnboundIdentifierError min_def)
   apply (meson FaustusReduceStatementResult.distinct FaustusReduceStepResult.distinct(5))+
  using lookupContractAbstractionWellTypedContext by fastforce

lemma wellTypedContractNeverGetsArgumentEvaluationError:
"wellTypedContract tyCtx contract \<Longrightarrow>
  wellTypedContext tyCtx ctx \<Longrightarrow>
  wellTypedState tyCtx state \<Longrightarrow>
  reduceContractStep ctx env state contract \<noteq> ArgumentEvaluationError"
  apply (induction contract rule: reduceContractStep.induct, auto)
         apply (auto split: option.split_asm if_split_asm prod.split_asm simp add: wellTypedPartyEvaluates wellTypedValueObservationEvaluates wellTypedPayeeEvaluates wellTypedStatementNeverGetsUnboundIdentifierError min_def)
    apply (metis FaustusReduceStatementResult.exhaust FaustusReduceStatementResult.simps(4) FaustusReduceStepResult.distinct(7) wellTypedStatementNeverGetsUnboundIdentifierError)
   apply (meson FaustusReduceStepResult.distinct wellTypedStatementNeverGetsUnboundIdentifierError)+
  by (metis lookupContractTypeWellTypedContext old.prod.inject option.inject wellTypedArgumentsExecute)

datatype FaustusReduceResult = ContractQuiescent "ReduceWarning list" "Payment list" FContext FState FContract
                                          | RRAmbiguousSlotIntervalError
                                          | RRUnboundIdentifierError
                                          | RRArgumentEvaluationError

function (sequential) reductionLoop :: "FContext \<Rightarrow> Environment \<Rightarrow> FState \<Rightarrow> FContract \<Rightarrow> ReduceWarning list \<Rightarrow>
                                        Payment list \<Rightarrow> FaustusReduceResult" where
"reductionLoop ctx env state contract warnings payments =
  (case reduceContractStep ctx env state contract of
     Reduced newCtx warning effect newState ncontract \<Rightarrow>
       let newWarnings = (if warning = ReduceNoWarning
                          then warnings
                          else warning # warnings) in
       let newPayments = (case effect of
                            ReduceWithPayment payment \<Rightarrow> payment # payments
                          | ReduceNoPayment \<Rightarrow> payments) in
       reductionLoop newCtx env newState ncontract newWarnings newPayments
   | AmbiguousSlotIntervalReductionError \<Rightarrow> RRAmbiguousSlotIntervalError
   | NotReduced \<Rightarrow> ContractQuiescent (rev warnings) (rev payments) ctx state contract
   | UnboundIdentifierError \<Rightarrow> RRUnboundIdentifierError
   | ArgumentEvaluationError \<Rightarrow> RRArgumentEvaluationError)"
  by pat_completeness auto

lemma reductionLoop_termination_aux1:
"reduceContractStep ctx env state contract =
       FaustusReduceStepResult.Reduced newCtx x12 x13 x14 newContract \<Longrightarrow>
       \<not> fContractSize newContract + (fold (+) (map (abstractionInformationSize) newCtx) 0)
          < fContractSize contract + (fold (+) (map (abstractionInformationSize) ctx) 0) \<Longrightarrow>
       fContractSize newContract + (fold (+) (map (abstractionInformationSize) newCtx) 0) =
       fContractSize contract + (fold (+) (map (abstractionInformationSize) ctx) 0)"
  subgoal premises ps proof (cases contract)
    case Close
    then show ?thesis using ps by (cases "refundOne (accounts state)", auto)
  next
    case (StatementCont s c)
    then show ?thesis using ps apply (cases s, auto split: option.split_asm if_split_asm prod.split_asm simp add: min_def)
      by (metis FaustusReduceStatementResult.simps(4) updateCtxFromStmt.simps(4) FaustusReduceStepResult.inject lessI)+
  next
    case (If x31 x32 x33)
    then show ?thesis using ps by (auto split: option.split_asm simp add: le_imp_less_Suc)
  next
  case (When x41 x42 x43)
    then show ?thesis using ps by (cases "timeInterval env" "snd (timeInterval env) < x42" "x42 \<le> fst (timeInterval env)" rule: prod.exhaust[case_product bool.exhaust[case_product bool.exhaust]], auto)
  next
    case (Let x51 x52 x53)
    then show ?thesis using ps by (cases "lookup x51 (boundValues state)", auto split: option.split_asm)
  next
    case (LetObservation obsId obs cont)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
    case (LetPubKey pkId pk cont)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
    case (LetC x71 x72 x73)
    then show ?thesis using ps by (auto simp add: fold_plus_sum_list_rev)
  next
    case (UseC x8)
    then show ?thesis using ps apply (auto split: option.split_asm)
      using paramsToFContextNoSize lookupContractCompilerContextSmaller by fastforce
  qed
  done

lemma reductionLoop_termination_aux2:
"reduceContractStep ctx env state contract =
       FaustusReduceStepResult.Reduced newCtx x12 x13 x14 x15 \<Longrightarrow>
       \<not> fContractSize x15 + (fold (+) (map (abstractionInformationSize) newCtx) 0)
          < fContractSize contract + (fold (+) (map (abstractionInformationSize) ctx) 0) \<Longrightarrow>
       \<not> length newCtx < length ctx \<Longrightarrow> length newCtx = length ctx"
  subgoal premises ps proof (cases contract)
    case Close
    then show ?thesis using ps by (cases "refundOne (accounts state)", auto)
  next
    case (StatementCont s x25)
    then show ?thesis using ps apply (cases s, auto split: option.split_asm if_split_asm prod.split_asm simp add: min_def)
         by (metis FaustusReduceStatementResult.simps(4) updateCtxFromStmt.simps(4) FaustusReduceStepResult.inject)+
  next
    case (If x31 x32 x33)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
  case (When x41 x42 x43)
    then show ?thesis using ps by (cases "timeInterval env" "snd (timeInterval env) < x42" "x42 \<le> fst (timeInterval env)" rule: prod.exhaust[case_product bool.exhaust[case_product bool.exhaust]], auto)
  next
    case (Let x51 x52 x53)
    then show ?thesis using ps apply (auto split: option.split_asm)
      by (cases "lookup x51 (boundValues state)", auto)
  next
    case (LetObservation obsId obs cont)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
    case (LetPubKey pkId pk cont)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
    case (LetC x71 x72 x73)
    then show ?thesis using ps by (auto simp add: fold_plus_sum_list_rev)
  next
    case (UseC x8)
    then show ?thesis using ps apply (auto split: option.split_asm)
      using paramsToFContextNoSize lookupContractCompilerContextSmaller by fastforce
  qed
  done

lemma reductionLoop_termination_aux3:
"reduceContractStep ctx env state contract =
       FaustusReduceStepResult.Reduced newCtx x12 x13 x14 x15 \<Longrightarrow>
       \<not> fContractSize x15 + (fold (+) (map (abstractionInformationSize) newCtx) 0)
          < fContractSize contract + (fold (+) (map (abstractionInformationSize) ctx) 0) \<Longrightarrow>
       \<not> length newCtx < length ctx \<Longrightarrow>
       (\<And>ctx env state contract newCtx x12 x13 x14 newContract.
           reduceContractStep ctx env state contract =
           FaustusReduceStepResult.Reduced newCtx x12 x13 x14 newContract \<Longrightarrow>
           \<not> fContractSize newContract + (fold (+) (map (abstractionInformationSize) newCtx) 0)
              < fContractSize contract + (fold (+) (map (abstractionInformationSize) ctx) 0) \<Longrightarrow>
           fContractSize newContract + (fold (+) (map (abstractionInformationSize) newCtx) 0) =
           fContractSize contract + (fold (+) (map (abstractionInformationSize) ctx) 0)) \<Longrightarrow>
       (\<And>ctx env state contract newCtx x12 x13 x14 x15.
           reduceContractStep ctx env state contract =
           FaustusReduceStepResult.Reduced newCtx x12 x13 x14 x15 \<Longrightarrow>
           \<not> fContractSize x15 + (fold (+) (map (abstractionInformationSize) newCtx) 0)
              < fContractSize contract + (fold (+) (map (abstractionInformationSize) ctx) 0) \<Longrightarrow>
           \<not> length newCtx < length ctx \<Longrightarrow> length newCtx = length ctx) \<Longrightarrow>
       length (accounts x14) < length (accounts state)"
  subgoal premises ps proof (cases contract)
    case Close
    then show ?thesis using ps apply auto
      apply (induction "accounts state", auto)
      by (cases "refundOne (accounts state)", auto simp add: refundOneShortens)
  next
    case (StatementCont s x25)
    then show ?thesis using ps apply (cases s, auto split: option.split_asm if_split_asm prod.split_asm simp add: min_def)
      by (metis FaustusReduceStatementResult.simps(4) updateCtxFromStmt.simps(4) FaustusReduceStepResult.inject lessI)+
  next
    case (If x31 x32 x33)
    then show ?thesis using ps by (auto split: option.split_asm simp add: le_imp_less_Suc)
  next
    case (When x41 x42 x43)
    then show ?thesis using ps by (cases "timeInterval env" "snd (timeInterval env) < x42" "x42 \<le> fst (timeInterval env)" rule: prod.exhaust[case_product bool.exhaust[case_product bool.exhaust]], auto)
  next
    case (Let x51 x52 x53)
    then show ?thesis using ps apply (auto split: option.split_asm)
      by (cases "lookup x51 (boundValues state)", auto)
  next
    case (LetObservation obsId obs cont)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
    case (LetPubKey pkId pk cont)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
    case (LetC x71 x72 x73)
    then show ?thesis using ps by (auto simp add: fold_plus_sum_list_rev)
  next
    case (UseC x8)
    then show ?thesis using ps apply (auto split: option.split_asm)
      using paramsToFContextNoSize lookupContractCompilerContextSmaller by fastforce
  qed
  done

termination reductionLoop
  apply (relation "measures [(\<lambda>(ctx, env, state, contract, _) . ((fContractSize contract)) + (fold (+) (map (abstractionInformationSize) ctx) 0)), 
    (\<lambda>(ctx, _, state, contract, _) . (length ctx)),
    (\<lambda>(ctx, _, state, contract, _) . (length (accounts state)))]", auto)
  using reductionLoop_termination_aux1
    reductionLoop_termination_aux2 
    reductionLoop_termination_aux3 by auto

thm reductionLoop.induct[split_format(complete)]
thm reductionLoop.induct
thm small_step_reduce_all_induct

lemma finalImpliesNoReductionLoopChange:
"\<forall>tyCtx . wellTypedContext tyCtx ctx \<longrightarrow>
  wellTypedContract tyCtx contract \<longrightarrow>
  wellTypedState tyCtx state \<longrightarrow>
  finalV2 (contract, ctx, state, env, warnings, payments) \<longrightarrow>
  (reductionLoop ctx env state contract initialWarnings initialPayments = ContractQuiescent (rev initialWarnings) (rev initialPayments) ctx state contract \<or>
    reductionLoop ctx env state contract initialWarnings initialPayments = RRAmbiguousSlotIntervalError)"
proof (induction ctx env state contract initialWarnings initialPayments rule: reductionLoop.induct)
  case (1 ctx env state contract warnings payments)
  then show ?case apply (cases "reduceContractStep ctx env state contract", auto simp add: wellTypedContractNeverGetsUnboundIdentifierError wellTypedContractNeverGetsArgumentEvaluationError)
    by (meson reduceStepIsSmallStepReduction finalV2_def)+
qed

lemma reduceAllImpliesReductionLoopChange:
"(contract, ctx, state, env, initialWarnings, initialPayments) \<Down>\<^sub>f\<^sub>2 (continueContract, newCtx, newState, newEnv, newWarnings, newPayments) \<Longrightarrow>
  wellTypedContext tyCtx ctx \<Longrightarrow>
  wellTypedContract tyCtx contract \<Longrightarrow>
  wellTypedState tyCtx state \<Longrightarrow>
  (\<exists>newWarningsEval newPaymentsEval . reductionLoop ctx env state contract initialWarnings initialPayments = ContractQuiescent newWarningsEval newPaymentsEval newCtx newState continueContract \<or>
    reductionLoop ctx env state contract initialWarnings initialPayments = RRAmbiguousSlotIntervalError)"
proof (induction contract ctx state env initialWarnings initialPayments continueContract newCtx newState newEnv newWarnings newPayments arbitrary: tyCtx initialWarnings initialPayments rule: small_step_reduce_all_induct)
  case (FinalStep contract ctx state env initialWarnings initialPayments finalContract finalCtx finalState finalEnv finalWarnings finalPayments)
  then show ?case apply auto
    subgoal premises ps proof (cases "reduceContractStep ctx env state contract")                                      
      case (Reduced nextCtx nextWarning nextEffect nextState nextContract)
      moreover have "nextCtx = finalCtx \<and> nextState = finalState \<and> nextContract = finalContract" using ps calculation apply auto
        apply (simp_all add: finalV2_def)
        using reduceStepIsSmallStepReduction smallStepReductionImpReduceStep FaustusReduceResult.distinct by fastforce+
      ultimately show ?thesis using Reduced ps apply (cases "reduceContractStep finalCtx env finalState finalContract", auto)
             apply (metis (no_types, opaque_lifting) deterministic reduceStepIsSmallStepReduction finalV2_def)
            apply (metis (mono_tags, opaque_lifting) deterministic reduceStepIsSmallStepReduction finalV2_def)
           apply (metis (no_types, lifting) deterministic reduceStepIsSmallStepReduction finalV2_def)
          apply (metis (no_types, lifting) deterministic reduceStepIsSmallStepReduction finalV2_def)
        using typePreservation wellTypedContractNeverGetsUnboundIdentifierError apply blast
        using typePreservation wellTypedContractNeverGetsArgumentEvaluationError by blast
    next
      case NotReduced
      then show ?thesis using ps reduceStepIsSmallStepReduction smallStepReductionImpReduceStep by fastforce
    next
      case AmbiguousSlotIntervalReductionError
      then show ?thesis using ps by auto
    next
      case UnboundIdentifierError
      then show ?thesis using ps wellTypedContractNeverGetsUnboundIdentifierError by auto
    next
      case ArgumentEvaluationError
      then show ?thesis using ps wellTypedContractNeverGetsArgumentEvaluationError by auto
    qed
    done
next
  case (AddStep contract ctx state env initialWarnings initialPayments nextContract nextCtx nextState nextEnv nextWarnings nextPayments finalContract finalCtx finalState finalEnv finalWarnings finalPayments)
  moreover obtain newTyCtx where "wellTypedContract newTyCtx nextContract \<and> wellTypedContext newTyCtx nextCtx \<and> wellTypedState newTyCtx nextState" using typePreservation calculation by blast
  ultimately show ?case apply (auto simp del: reductionLoop.simps)
    apply (simp (no_asm) only: reductionLoop.simps)
    subgoal premises ps proof (cases "reduceContractStep ctx env state contract")
      case (Reduced innerNextCtx innerNextWarning innerNextEffect innerNextState innerNextContract)
      moreover have "nextCtx = innerNextCtx \<and> nextState = innerNextState \<and> nextContract = innerNextContract" using ps calculation apply auto 
        using small_step_reduce.simps reduceStepIsSmallStepReduction smallStepReductionImpReduceStep finalV2_def FaustusReduceResult.distinct by (smt (verit, best) FaustusReduceStepResult.inject)+
      moreover have "env = nextEnv" using ps calculation smallStepEnvConstant by auto
      ultimately show ?thesis using ps Reduced apply (auto simp del: reductionLoop.simps)
        apply (cases innerNextEffect, auto)
          apply (metis reductionLoop.simps ReduceEffect.simps(4))
         apply (metis reductionLoop.simps ReduceEffect.simps(5))
        apply (cases innerNextEffect, auto)
         apply meson
        by meson
    next
      case NotReduced
      then show ?thesis using ps reduceStepIsSmallStepReduction smallStepReductionImpReduceStep by fastforce
    next
      case AmbiguousSlotIntervalReductionError
      then show ?thesis using ps by auto
    next
      case UnboundIdentifierError
      then show ?thesis using ps wellTypedContractNeverGetsUnboundIdentifierError by auto
    next
      case ArgumentEvaluationError
      then show ?thesis using ps wellTypedContractNeverGetsArgumentEvaluationError by auto
    qed
    done
qed

fun convertSemanticsWarningsToEvalWarnings :: "ReduceWarning list \<Rightarrow> ReduceWarning list" where
"convertSemanticsWarningsToEvalWarnings [] = []" |
"convertSemanticsWarningsToEvalWarnings (ReduceNoWarning # rest) = convertSemanticsWarningsToEvalWarnings rest" |
"convertSemanticsWarningsToEvalWarnings (w#rest) = (w # convertSemanticsWarningsToEvalWarnings rest)"

lemma reduceAllWarningsPaymentsMatchReductionLoop:
"(contract, ctx, state, env, initialWarnings, initialPayments) \<Down>\<^sub>f\<^sub>2 (continueContract, newCtx, newState, newEnv, newWarnings, newPayments) \<Longrightarrow>
  wellTypedContext tyCtx ctx \<Longrightarrow>
  wellTypedContract tyCtx contract \<Longrightarrow>
  wellTypedState tyCtx state \<Longrightarrow>
  reductionLoop ctx env state contract initialWarningsEval initialPaymentsEval = ContractQuiescent newWarningsEval newPaymentsEval newCtx newState continueContract \<Longrightarrow>
  (\<exists>addedWarnings addedPayments . newWarnings = initialWarnings @ addedWarnings \<and>
    newPayments = initialPayments @ addedPayments \<and>
    newWarningsEval = (rev initialWarningsEval) @ convertSemanticsWarningsToEvalWarnings addedWarnings \<and>
    newPaymentsEval = (rev initialPaymentsEval) @ addedPayments)"
proof (induction contract ctx state env initialWarnings initialPayments continueContract newCtx newState newEnv newWarnings newPayments arbitrary: tyCtx initialWarningsEval initialPaymentsEval rule: small_step_reduce_all_induct)
  case (FinalStep contract ctx state env initialWarnings initialPayments finalContract finalCtx finalState finalEnv finalWarnings finalPayments)
  moreover obtain addedWarnings where "finalWarnings = initialWarnings @ addedWarnings" using calculation smallStepExistingWarningsPaymentsConstant by blast
  moreover obtain addedPayments where "finalPayments = initialPayments @ addedPayments" using calculation smallStepExistingWarningsPaymentsConstant by blast
  moreover obtain addedWarning effect where "reduceContractStep ctx env state contract = Reduced finalCtx addedWarning effect finalState finalContract" using smallStepReductionImpReduceStep calculation by blast
  moreover have "addedWarnings = [addedWarning]" using calculation smallStepReductionImpReduceStep by fastforce
  moreover have "finalEnv = env" using calculation smallStepEnvConstant by blast
  moreover have "reduceContractStep finalCtx env finalState finalContract = NotReduced" using calculation apply auto
    apply (cases "reduceContractStep finalCtx env finalState finalContract", auto)
    using finalV2_def reduceStepIsSmallStepReduction ReduceResult.distinct smallStepEnvConstant by blast
  ultimately show ?case apply auto
     apply (cases addedWarning, auto)
         apply (cases effect, auto split: if_split_asm)
    using smallStepReductionImpReduceStep by fastforce+
next
  case (AddStep contract1 ctx1 state1 env1 warnings1 payments1 contract2 ctx2 state2 env2 warnings2 payments2 contract3 ctx3 state3 env3 warnings3 payments3)
  moreover obtain addedWarnings1 where "warnings2 = warnings1 @ addedWarnings1" using calculation smallStepExistingWarningsPaymentsConstant by metis
  moreover obtain addedPayments1 where "payments2 = payments1 @ addedPayments1" using calculation smallStepExistingWarningsPaymentsConstant by metis
  moreover obtain addedWarnings2 where "warnings3 = warnings2 @ addedWarnings2" by (meson calculation smallStepReduceAllExistingWarningsPaymentsConstant)
  moreover obtain addedPayments2 where "payments3 = payments2 @ addedPayments2" by (meson calculation smallStepReduceAllExistingWarningsPaymentsConstant)
  moreover obtain addedWarning1 effect1 where "reduceContractStep ctx1 env1 state1 contract1 = Reduced ctx2 addedWarning1 effect1 state2 contract2" using smallStepReductionImpReduceStep calculation by metis
  moreover have "addedWarnings1 = [addedWarning1]" using calculation smallStepReductionImpReduceStep by fastforce
  moreover have "env2 = env1" using calculation smallStepEnvConstant by metis
  moreover have "env2 = env3" using calculation(2) smallStepEnvConstant by (induction contract2 ctx2 state2 env2 warnings2 payments2 contract3 ctx3 state3 env3 warnings3 payments3 rule: small_step_reduce_all_induct, simp_all)
  moreover obtain tyCtx2 where "wellTypedContract tyCtx2 contract2 \<and> wellTypedContext tyCtx2 ctx2 \<and> wellTypedState tyCtx2 state2" using typePreservation calculation by metis
  moreover have "reductionLoop ctx2 env1 state2 contract2 
        (if addedWarning1 = ReduceNoWarning then initialWarningsEval else (addedWarning1 # initialWarningsEval))
        ((case effect1 of
                            ReduceWithPayment payment \<Rightarrow> payment # initialPaymentsEval
                          | ReduceNoPayment \<Rightarrow> initialPaymentsEval)) =
        FaustusReduceResult.ContractQuiescent newWarningsEval newPaymentsEval ctx3 state3 contract3" using calculation(7) calculation(12) apply (auto simp del: reductionLoop.simps)
     apply (cases effect1, auto simp del: reductionLoop.simps)
      apply (smt (verit, del_insts) reductionLoop.simps FaustusReduceResult.exhaust FaustusReduceStepResult.simps(22) ReduceEffect.simps(4))
    using reductionLoop.simps FaustusReduceResult.exhaust FaustusReduceStepResult.simps ReduceEffect.simps apply (smt (verit))
    apply (cases effect1, auto simp del: reductionLoop.simps)
    using reductionLoop.simps FaustusReduceResult.exhaust FaustusReduceStepResult.simps ReduceEffect.simps by (smt (verit))+
  ultimately show ?case apply (auto simp del: reductionLoop.simps)
     apply (cases addedWarning1, auto simp del: reductionLoop.simps)
        apply (cases effect1, auto simp del: reductionLoop.simps; metis Cons_eq_appendI append_assoc rev_append rev_singleton_conv same_append_eq self_append_conv)
       apply (cases effect1, auto simp del: reductionLoop.simps; metis Cons_eq_appendI append_assoc rev_append rev_singleton_conv same_append_eq self_append_conv)
      apply (cases effect1, auto simp del: reductionLoop.simps; metis Cons_eq_appendI append_assoc rev_append rev_singleton_conv same_append_eq self_append_conv)
     apply (metis Cons_eq_appendI append_assoc rev_append rev_singleton_conv same_append_eq self_append_conv)
    apply (cases effect1, auto simp del: reductionLoop.simps split: if_split_asm)
    using smallStepReductionImpReduceStep by fastforce+
qed

lemma reductionLoopIsAllSmallStepReductions:
"reductionLoop ctx env state contract initialWarningsEval initialPaymentsEval = ContractQuiescent newWarningsEval newPaymentsEval newCtx newState continueContract \<longrightarrow>
  (\<exists>newWarnings newPayments . (contract, ctx, state, env, warnings, payments) \<Down>\<^sub>f\<^sub>2 (continueContract, newCtx, newState, env, newWarnings, newPayments) \<or> finalV2 (contract, ctx, state, env, warnings, payments))"
proof (induction ctx env state contract initialWarningsEval initialPaymentsEval rule: reductionLoop.induct)
  case (1 ctx env state contract warnings payments)
  then show ?case apply (auto simp del: reductionLoop.simps)
    subgoal premises ps proof (cases "reduceContractStep ctx env state contract")
      case (Reduced reduceCtx reduceStepWarning reduceEffect reduceState reduceContract)
      moreover have "(contract, ctx, state, env, warnings, payments) \<rightarrow>\<^sub>f\<^sub>2 (reduceContract, reduceCtx, reduceState, env, warnings @ [reduceStepWarning], payments @ convertReduceEffect reduceEffect)" using calculation ps reduceStepIsSmallStepReduction by presburger
      moreover have "reductionLoop reduceCtx env reduceState reduceContract 
        (if reduceStepWarning = ReduceNoWarning then warnings else reduceStepWarning # warnings) 
        (case reduceEffect of ReduceWithPayment p \<Rightarrow> p # payments | _ \<Rightarrow> payments) = ContractQuiescent newWarningsEval newPaymentsEval newCtx newState continueContract" using Reduced ps(2) apply auto
        by meson
      moreover have "(\<exists>newWarnings newPayments. (reduceContract, reduceCtx, reduceState, env, warnings, payments) \<Down>\<^sub>f\<^sub>2 (continueContract, newCtx, newState, env, newWarnings, newPayments)) \<or> finalV2 (reduceContract, reduceCtx, reduceState, env, warnings, payments)" using calculation ps apply auto
        by (metis (no_types, lifting) finalWarningsAndPaymentsAreArbitrary smallStepReduceAllAppendedPaymentsConstantGeneral smallStepReduceAllAppendedWarningsConstantGeneral)
      ultimately show ?thesis using ps apply (auto simp del: reductionLoop.simps)
         apply (cases reduceStepWarning;
            cases reduceEffect;
            auto simp del: reductionLoop.simps;
            meson smallStepReduceAllAppendedWarningsConstantGeneral smallStepReduceAllPaymentsAreArbitrary small_step_reduce_all.simps)
        apply (cases "reduceContractStep reduceCtx env reduceState reduceContract", auto)
         apply (meson reduceStepIsSmallStepReduction finalV2_def)
        by (meson FinalStep finalWarningsAndPaymentsAreArbitrary smallStepReduceAllAppendedPaymentsConstantGeneral smallStepReduceAllAppendedWarningsConstantGeneral)+
    next
      case NotReduced
      then show ?thesis using ps finalV2_def apply auto
        by (metis FaustusReduceStepResult.distinct(1) smallStepReductionImpReduceStep)
    next
      case AmbiguousSlotIntervalReductionError
      then show ?thesis using ps by auto
    next
      case UnboundIdentifierError
      then show ?thesis using ps by auto
    next
      case ArgumentEvaluationError
      then show ?thesis using ps by auto
    qed
    done
qed

fun reduceContractUntilQuiescent :: "FContext \<Rightarrow> Environment \<Rightarrow> FState \<Rightarrow> FContract \<Rightarrow> FaustusReduceResult" where
"reduceContractUntilQuiescent ctx env state contract = reductionLoop ctx env state contract [] []"

datatype FaustusReduceStatementsResult = StatementsReduced "ReduceWarning list" "Payment list" FState
  | StatementsUnboundIdentifierError

fun reduceStatements :: "Environment \<Rightarrow> FState \<Rightarrow> FStatement list \<Rightarrow> ReduceWarning list \<Rightarrow> Payment list \<Rightarrow> FaustusReduceStatementsResult" where
"reduceStatements env state [] warnings payments = StatementsReduced (rev warnings) (rev payments) state" |
"reduceStatements env state (s#stmts) warnings payments = (case reduceStatement env state s of
  StatementReduced warning effect newState \<Rightarrow> (let newWarnings = (if warning = ReduceNoWarning
                          then warnings
                          else warning # warnings) in
       let newPayments = (case effect of
                            ReduceWithPayment payment \<Rightarrow> payment # payments
                          | ReduceNoPayment \<Rightarrow> payments) in
    reduceStatements env newState stmts newWarnings newPayments)
  | StatementUnboundIdentifierError \<Rightarrow> StatementsUnboundIdentifierError)"

datatype ApplyGuardResult = AppliedGuard ApplyWarning FState "FGuardExpression option" "FStatement list"
  | ApplyGuardNoMatch

fun applyGuard :: "Environment \<Rightarrow> FState \<Rightarrow> Input \<Rightarrow> FGuardExpression \<Rightarrow> ApplyGuardResult" where
"applyGuard env state (IDeposit accId1 party1 tok1 amount) (ActionGuard (Deposit accId2 party2 tok2 val)) = (if (Some accId1 = evalFParty state accId2 \<and> Some party1 = evalFParty state party2 \<and> tok1 = tok2
        \<and> Some amount = evalFValue env state val)
   then let warning = if amount > 0
                      then ApplyNoWarning
                      else ApplyNonPositiveDeposit party1 accId1 tok2 amount in
        let newState = state \<lparr> accounts := addMoneyToAccount accId1 tok1 amount (accounts state) \<rparr> in
        AppliedGuard warning newState None []
   else ApplyGuardNoMatch)" |
"applyGuard env state (IDeposit accId1 party1 tok1 amount) (ActionGuard _) = ApplyGuardNoMatch" |
"applyGuard env state (IChoice choId1 choice) (ActionGuard (Choice choId2 bounds)) = (if (Some choId1 = evalFChoiceId state choId2 \<and> inBounds choice bounds)
   then let newState = state \<lparr> choices := MList.insert choId1 choice (choices state) \<rparr> in
        AppliedGuard ApplyNoWarning newState None []
   else ApplyGuardNoMatch)" |
"applyGuard env state (IChoice choId1 choice) (ActionGuard _) = ApplyGuardNoMatch" |
"applyGuard env state INotify (ActionGuard (Notify obs)) = (if evalFObservation env state obs = Some True
   then AppliedGuard ApplyNoWarning state None []
   else ApplyGuardNoMatch)" |
"applyGuard env state INotify (ActionGuard _) = ApplyGuardNoMatch" |
"applyGuard env state i (GuardThenGuard g1 g2) = (case applyGuard env state i g1 of
  AppliedGuard w s (Some g) stmts \<Rightarrow> AppliedGuard w s (Some (GuardThenGuard g g2)) stmts
  | AppliedGuard w s None stmts \<Rightarrow> AppliedGuard w s (Some g2) stmts
  | _ \<Rightarrow> ApplyGuardNoMatch)" |
"applyGuard env state i (GuardStmtsGuard g stmts) = (case applyGuard env state i g of
  AppliedGuard w newState (Some newG) newStmts \<Rightarrow> AppliedGuard w newState (Some (GuardStmtsGuard newG stmts)) newStmts
  | AppliedGuard w newState None newStmts \<Rightarrow> AppliedGuard w newState None (newStmts @ stmts)
  | _ \<Rightarrow> ApplyGuardNoMatch)" |
"applyGuard env state i (DisjointGuard g1 g2) = (case applyGuard env state i g1 of
  AppliedGuard w newState innerG newStmts \<Rightarrow> AppliedGuard w newState innerG newStmts
  | ApplyGuardNoMatch \<Rightarrow> applyGuard env state i g2)" |
"applyGuard env state i (InterleavedGuard g1 g2) = (case applyGuard env state i g1 of
  AppliedGuard w newState (Some innerG) newStmts \<Rightarrow> AppliedGuard w newState (Some (InterleavedGuard innerG g2)) newStmts
  | AppliedGuard w newState None newStmts \<Rightarrow> AppliedGuard w newState (Some g2) newStmts
  | ApplyGuardNoMatch \<Rightarrow> (case applyGuard env state i g2 of
    AppliedGuard w newState (Some innerG) newStmts \<Rightarrow> AppliedGuard w newState (Some (InterleavedGuard g1 innerG)) newStmts
    | AppliedGuard w newState None newStmts \<Rightarrow> AppliedGuard w newState (Some g1) newStmts
    | ApplyGuardNoMatch \<Rightarrow> ApplyGuardNoMatch))"

datatype ApplyLinearGuardNodeResult = AppliedLinearGuardNode ApplyWarning FState "FStatement list"
  | ApplyLinearGuardNodeNoMatch

fun applyLinearGuardNode :: "Environment \<Rightarrow> FState \<Rightarrow> Input \<Rightarrow> LinearGuardNode \<Rightarrow> ApplyLinearGuardNodeResult" where
"applyLinearGuardNode env state (IDeposit accId1 party1 tok1 amount) (Deposit accId2 party2 tok2 val, stmts) = (if (Some accId1 = evalFParty state accId2 \<and> Some party1 = evalFParty state party2 \<and> tok1 = tok2
        \<and> Some amount = evalFValue env state val)
   then let warning = if amount > 0
                      then ApplyNoWarning
                      else ApplyNonPositiveDeposit party1 accId1 tok2 amount in
        let newState = state \<lparr> accounts := addMoneyToAccount accId1 tok1 amount (accounts state) \<rparr> in
        AppliedLinearGuardNode warning newState stmts
   else ApplyLinearGuardNodeNoMatch)" |
"applyLinearGuardNode env state (IDeposit accId1 party1 tok1 amount) _ = ApplyLinearGuardNodeNoMatch" |
"applyLinearGuardNode env state (IChoice choId1 choice) (Choice choId2 bounds, stmts) = (if (Some choId1 = evalFChoiceId state choId2 \<and> inBounds choice bounds)
   then let newState = state \<lparr> choices := MList.insert choId1 choice (choices state) \<rparr> in
        AppliedLinearGuardNode ApplyNoWarning newState stmts
   else ApplyLinearGuardNodeNoMatch)" |
"applyLinearGuardNode env state (IChoice choId1 choice) _ = ApplyLinearGuardNodeNoMatch" |
"applyLinearGuardNode env state INotify (Notify obs, stmts) = (if evalFObservation env state obs = Some True
   then AppliedLinearGuardNode ApplyNoWarning state stmts
   else ApplyLinearGuardNodeNoMatch)" |
"applyLinearGuardNode env state INotify _ = ApplyLinearGuardNodeNoMatch"

datatype ApplyLinearGuardResult = AppliedLinearGuard ApplyWarning FState "LinearGuardExpression option" "FStatement list"
  | ApplyLinearGuardNoMatch

fun applyLinearGuard :: "Environment \<Rightarrow> FState \<Rightarrow> Input \<Rightarrow> LinearGuardExpression \<Rightarrow> ApplyLinearGuardResult" where
"applyLinearGuard env state input (LinearGuardNode node) = (case applyLinearGuardNode env state input node of
  AppliedLinearGuardNode warning newState stmts \<Rightarrow> AppliedLinearGuard warning newState None stmts
  | ApplyLinearGuardNodeNoMatch \<Rightarrow> ApplyLinearGuardNoMatch)" |
"applyLinearGuard env state input (LinearGuardThenGuard node g2) = (case applyLinearGuardNode env state input node of
  AppliedLinearGuardNode warning newState stmts \<Rightarrow> AppliedLinearGuard warning newState (Some g2) stmts
  | ApplyLinearGuardNodeNoMatch \<Rightarrow> ApplyLinearGuardNoMatch)" |
"applyLinearGuard env state i (LinearDisjointGuard g1 g2) = (case applyLinearGuard env state i g1 of
  AppliedLinearGuard w newState innerG newStmts \<Rightarrow> AppliedLinearGuard w newState innerG newStmts
  | ApplyLinearGuardNoMatch \<Rightarrow> applyLinearGuard env state i g2)"

lemma preservationOfApplyGuard_LinearGuardNoMatchImpliesInterleavedLinearGuardNoMatch:
"applyLinearGuard env state i g1 = ApplyLinearGuardNoMatch \<Longrightarrow>
  applyLinearGuard env state i g2 = ApplyLinearGuardNoMatch \<Longrightarrow>
  applyLinearGuard env state i (interleaveLinearTrees g1 g2 f) = ApplyLinearGuardNoMatch"
"applyLinearGuard env state i g1 = ApplyLinearGuardNoMatch \<Longrightarrow>
  applyLinearGuard env state i g2 = ApplyLinearGuardNoMatch \<Longrightarrow>
  applyLinearGuard env state i (interleaveLinearTreesRightFirst g1 g2) = ApplyLinearGuardNoMatch"
proof (induction g1 g2 f and g1 g2 rule: interleaveLinearTrees_interleaveLinearTreesRightFirst.induct)
  case (1 node g2 f)
  then show ?case by (cases f, auto split: ApplyLinearGuardNodeResult.split_asm)
next
  case (2 node gCont g2 f)
  then show ?case by (cases f, auto split: ApplyLinearGuardNodeResult.split_asm ApplyLinearGuardResult.split_asm option.split_asm)
next
  case (3 innerG1 innerG2 g2 f)
  then show ?case by (cases f, auto split: ApplyLinearGuardResult.split_asm option.split_asm)
next
  case (4 node g2)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm)
next
  case (5 node gCont g2)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm)
next
  case (6 innerG1 innerG2 g2)
  then show ?case by (auto split: ApplyLinearGuardResult.split_asm)
qed

lemma preservationOfApplyGuard_LinearGuardNoMatchLeftImpliesInterleavedNoFlipLinearGuardNoMatch:
"applyLinearGuard env state i g1 = ApplyLinearGuardNoMatch \<Longrightarrow>
  f=False \<Longrightarrow>
  applyLinearGuard env state i (interleaveLinearTrees g1 g2 f) = ApplyLinearGuardNoMatch"
"applyLinearGuard env state i g1 = ApplyLinearGuardNoMatch \<Longrightarrow>
  applyLinearGuard env state i (interleaveLinearTreesRightFirst g1 g2) = ApplyLinearGuardNoMatch"
proof (induction g1 g2 f and g1 g2 rule: interleaveLinearTrees_interleaveLinearTreesRightFirst.induct)
  case (1 node g2 f)
  then show ?case by (cases f, auto split: ApplyLinearGuardNodeResult.split_asm)
next
  case (2 node gCont g2 f)
  then show ?case by (cases f, auto split: ApplyLinearGuardNodeResult.split_asm ApplyLinearGuardResult.split_asm option.split_asm)
next
  case (3 innerG1 innerG2 g2 f)
  then show ?case by (cases f, auto split: ApplyLinearGuardResult.split_asm option.split_asm)
next
  case (4 node g2)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm)
next
  case (5 node gCont g2)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm)
next
  case (6 innerG1 innerG2 g2)
  then show ?case by (auto split: ApplyLinearGuardResult.split_asm)
qed

lemma preservationOfApplyGuard_LeftLinearGuardMatchNoContinueImpliesInterleavedLinearGuardMatchWithRightContinue:
"(applyLinearGuard env state i g1 = AppliedLinearGuard w newState None stmts \<longrightarrow>
  applyLinearGuard env state i (interleaveLinearTrees g1 g2 f) = AppliedLinearGuard w newState (Some g2) stmts)"
"(applyLinearGuard env state i g1 = AppliedLinearGuard w newState None stmts \<longrightarrow>
  applyLinearGuard env state i (interleaveLinearTreesRightFirst g1 g2) = AppliedLinearGuard w newState (Some g2) stmts)"
proof (induction g1 g2 f and g1 g2 rule: interleaveLinearTrees_interleaveLinearTreesRightFirst.induct)
  case (1 node g2 f)
  then show ?case by (cases f, auto split: ApplyLinearGuardNodeResult.split_asm)
next
  case (2 node gCont g2 f)
  then show ?case by (cases f, auto split: ApplyLinearGuardNodeResult.split_asm)
next
  case (3 innerG1 innerG2 g2 f)
  then show ?case by (cases f, auto split: ApplyLinearGuardResult.split_asm option.split_asm simp add: preservationOfApplyGuard_LinearGuardNoMatchLeftImpliesInterleavedNoFlipLinearGuardNoMatch)
next
  case (4 node g2)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm)
next
  case (5 node gCont g2)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm)
next
  case (6 innerG1 innerG2 g2)
  then show ?case by (auto split: ApplyLinearGuardResult.split_asm option.split_asm simp add: preservationOfApplyGuard_LinearGuardNoMatchLeftImpliesInterleavedNoFlipLinearGuardNoMatch)
qed

lemma preservationOfApplyGuard_LeftLinearGuardMatchWithContinueImpliesInterleavedLinearGuardMatchWithRightInterleavedContinue:
"(applyLinearGuard env state i g1 = AppliedLinearGuard w newState (Some innerG1) stmts \<longrightarrow>
  applyLinearGuard env state i (interleaveLinearTrees g1 g2 f) = AppliedLinearGuard w newState (Some (interleaveLinearTrees innerG1 g2 True)) stmts)"
"(applyLinearGuard env state i g1 = AppliedLinearGuard w newState (Some innerG1) stmts \<longrightarrow>
  applyLinearGuard env state i (interleaveLinearTreesRightFirst g1 g2) = AppliedLinearGuard w newState (Some (interleaveLinearTrees g2 innerG1 True)) stmts)"
proof (induction g1 g2 f and g1 g2 rule: interleaveLinearTrees_interleaveLinearTreesRightFirst.induct)
  case (1 node g2 f)
  then show ?case by (cases f, auto split: ApplyLinearGuardNodeResult.split_asm)
next
  case (2 node gCont g2 f)
  then show ?case by (cases f, auto split: ApplyLinearGuardNodeResult.split_asm)
next
  case (3 innerG1 innerG2 g2 f)
  then show ?case by (cases f, auto split: ApplyLinearGuardResult.split_asm option.split_asm simp add: preservationOfApplyGuard_LinearGuardNoMatchLeftImpliesInterleavedNoFlipLinearGuardNoMatch)
next
  case (4 node g2)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm)
next
  case (5 node gCont g2)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm)
next
  case (6 innerG1a innerG2 g2)
  then show ?case by (auto split: ApplyLinearGuardResult.split_asm option.split_asm simp add: preservationOfApplyGuard_LinearGuardNoMatchLeftImpliesInterleavedNoFlipLinearGuardNoMatch)
qed

lemma preservationOfApplyGuard_RightLinearGuardMatchNoContinueImpliesInterleavedLinearGuardMatchWithLeftContinue:
"(applyLinearGuard env state i g1 = ApplyLinearGuardNoMatch \<longrightarrow>
  applyLinearGuard env state i g2 = AppliedLinearGuard w newState None stmts \<longrightarrow>
  f = True \<longrightarrow>
  applyLinearGuard env state i (interleaveLinearTrees g1 g2 True) = AppliedLinearGuard w newState (Some g1) stmts)"
"(applyLinearGuard env state i g1 = ApplyLinearGuardNoMatch \<longrightarrow>
  applyLinearGuard env state i g2 = AppliedLinearGuard w newState None stmts \<longrightarrow>
  applyLinearGuard env state i (interleaveLinearTreesRightFirst g2 g1) = AppliedLinearGuard w newState (Some g1) stmts)"
proof (induction g1 g2 f and g1 g2 rule: interleaveLinearTrees_interleaveLinearTreesRightFirst.induct)
  case (1 node g2 f)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm simp add: preservationOfApplyGuard_LinearGuardNoMatchLeftImpliesInterleavedNoFlipLinearGuardNoMatch preservationOfApplyGuard_LeftLinearGuardMatchNoContinueImpliesInterleavedLinearGuardMatchWithRightContinue)
next
  case (2 node gCont g2 f)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm simp add: preservationOfApplyGuard_LeftLinearGuardMatchNoContinueImpliesInterleavedLinearGuardMatchWithRightContinue)
next
  case (3 innerG1 innerG2 g2 f)
  then show ?case by (auto split: ApplyLinearGuardResult.split_asm ApplyLinearGuardResult.split ApplyLinearGuardNodeResult.split_asm simp add: preservationOfApplyGuard_LinearGuardNoMatchLeftImpliesInterleavedNoFlipLinearGuardNoMatch preservationOfApplyGuard_LeftLinearGuardMatchNoContinueImpliesInterleavedLinearGuardMatchWithRightContinue)
next
  case (4 node g2)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm simp add: preservationOfApplyGuard_LinearGuardNoMatchLeftImpliesInterleavedNoFlipLinearGuardNoMatch preservationOfApplyGuard_LeftLinearGuardMatchNoContinueImpliesInterleavedLinearGuardMatchWithRightContinue)
next
  case (5 node gCont g2)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm simp add: preservationOfApplyGuard_LeftLinearGuardMatchNoContinueImpliesInterleavedLinearGuardMatchWithRightContinue)
next
  case (6 innerG1 innerG2 g2)
  then show ?case by (auto split: ApplyLinearGuardResult.split_asm ApplyLinearGuardResult.split ApplyLinearGuardNodeResult.split_asm simp add: preservationOfApplyGuard_LinearGuardNoMatchLeftImpliesInterleavedNoFlipLinearGuardNoMatch preservationOfApplyGuard_LeftLinearGuardMatchNoContinueImpliesInterleavedLinearGuardMatchWithRightContinue)
qed

lemma preservationOfApplyGuard_RightLinearGuardMatchWithContinueImpliesInterleavedLinearGuardMatchWithLeftContinueInterleaved:
"(applyLinearGuard env state i g1 = ApplyLinearGuardNoMatch \<longrightarrow>
  applyLinearGuard env state i g2 = AppliedLinearGuard w newState (Some innerGCont) stmts \<longrightarrow>
  f = True \<longrightarrow>
  applyLinearGuard env state i (interleaveLinearTrees g1 g2 True) = AppliedLinearGuard w newState (Some (interleaveLinearTrees g1 innerGCont True)) stmts)"
"(applyLinearGuard env state i g1 = ApplyLinearGuardNoMatch \<longrightarrow>
  applyLinearGuard env state i g2 = AppliedLinearGuard w newState (Some innerGCont) stmts \<longrightarrow>
  applyLinearGuard env state i (interleaveLinearTreesRightFirst g2 g1) = AppliedLinearGuard w newState (Some (interleaveLinearTrees g1 innerGCont True)) stmts)"
proof (induction g1 g2 f and g1 g2 rule: interleaveLinearTrees_interleaveLinearTreesRightFirst.induct)
  case (1 node g2 f)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm simp add: preservationOfApplyGuard_LeftLinearGuardMatchWithContinueImpliesInterleavedLinearGuardMatchWithRightInterleavedContinue preservationOfApplyGuard_LinearGuardNoMatchLeftImpliesInterleavedNoFlipLinearGuardNoMatch preservationOfApplyGuard_LeftLinearGuardMatchNoContinueImpliesInterleavedLinearGuardMatchWithRightContinue)
next
  case (2 node gCont g2 f)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm simp add: preservationOfApplyGuard_LeftLinearGuardMatchWithContinueImpliesInterleavedLinearGuardMatchWithRightInterleavedContinue preservationOfApplyGuard_LeftLinearGuardMatchNoContinueImpliesInterleavedLinearGuardMatchWithRightContinue)
next
  case (3 innerG1 innerG2 g2 f)
  then show ?case by (auto split:  ApplyLinearGuardResult.split_asm ApplyLinearGuardResult.split ApplyLinearGuardNodeResult.split_asm simp add: preservationOfApplyGuard_LeftLinearGuardMatchWithContinueImpliesInterleavedLinearGuardMatchWithRightInterleavedContinue preservationOfApplyGuard_LinearGuardNoMatchLeftImpliesInterleavedNoFlipLinearGuardNoMatch preservationOfApplyGuard_LeftLinearGuardMatchNoContinueImpliesInterleavedLinearGuardMatchWithRightContinue)
next
  case (4 node g2)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm simp add: preservationOfApplyGuard_LeftLinearGuardMatchWithContinueImpliesInterleavedLinearGuardMatchWithRightInterleavedContinue preservationOfApplyGuard_LinearGuardNoMatchLeftImpliesInterleavedNoFlipLinearGuardNoMatch preservationOfApplyGuard_LeftLinearGuardMatchNoContinueImpliesInterleavedLinearGuardMatchWithRightContinue)
next
  case (5 node gCont g2)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm simp add: preservationOfApplyGuard_LeftLinearGuardMatchWithContinueImpliesInterleavedLinearGuardMatchWithRightInterleavedContinue preservationOfApplyGuard_LeftLinearGuardMatchNoContinueImpliesInterleavedLinearGuardMatchWithRightContinue)
next
  case (6 innerG1 innerG2 g2)
  then show ?case by (auto split: ApplyLinearGuardResult.split_asm ApplyLinearGuardResult.split ApplyLinearGuardNodeResult.split_asm simp add: preservationOfApplyGuard_LeftLinearGuardMatchWithContinueImpliesInterleavedLinearGuardMatchWithRightInterleavedContinue preservationOfApplyGuard_LinearGuardNoMatchLeftImpliesInterleavedNoFlipLinearGuardNoMatch preservationOfApplyGuard_LeftLinearGuardMatchNoContinueImpliesInterleavedLinearGuardMatchWithRightContinue)
qed

lemma applyLinearGuardNoMatchImpliesAddContinuationNoMatch:
"applyLinearGuard env state i g1 = ApplyLinearGuardNoMatch \<Longrightarrow> applyLinearGuard env state i (addLinearGuardContinuation g2 g1) = ApplyLinearGuardNoMatch"
proof (induction g2 g1 rule: addLinearGuardContinuation.induct)
  case (1 gCont a stmts)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm)
next
  case (2 gCont node innerG)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm)
next
  case (3 gCont g1 g2)
  then show ?case by (auto split: ApplyLinearGuardResult.split_asm)
qed

lemma applyLinearGuardNoMatchImpliesAddStatementsContinuationNoMatch:
"applyLinearGuard env state i g1 = ApplyLinearGuardNoMatch \<Longrightarrow> applyLinearGuard env state i (addStatementsContinuation stmts g1) = ApplyLinearGuardNoMatch"
proof (induction stmts g1 rule: addStatementsContinuation.induct)
  case (1 gCont a stmts)
  then show ?case by (cases a; cases i; auto split: ApplyLinearGuardNodeResult.split_asm ApplyLinearGuardNodeResult.split)
next
  case (2 gCont node innerG)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm)
next
  case (3 gCont g1 g2)
  then show ?case by (auto split: ApplyLinearGuardResult.split_asm)
qed

lemma applyLinearGuardMatchImpliesAddContinuationMatch:
"applyLinearGuard env state i g1 = AppliedLinearGuard newWarning newState None stmts \<Longrightarrow> applyLinearGuard env state i (addLinearGuardContinuation g2 g1) = AppliedLinearGuard newWarning newState (Some g2) stmts"
proof (induction g2 g1 rule: addLinearGuardContinuation.induct)
  case (1 gCont a stmts)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm)
next
  case (2 gCont node innerG)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm)
next
  case (3 gCont g1 g2)
  then show ?case by (auto split: ApplyLinearGuardResult.split_asm ApplyLinearGuardResult.split simp add: applyLinearGuardNoMatchImpliesAddContinuationNoMatch)
qed

lemma applyLinearGuardMatchImpliesAddStatementsContinuationMatch:
"applyLinearGuard env state i g1 = AppliedLinearGuard newWarning newState None stmts \<Longrightarrow> applyLinearGuard env state i (addStatementsContinuation stmts2 g1) = AppliedLinearGuard newWarning newState None (stmts @ stmts2)"
proof (induction stmts2 g1 rule: addStatementsContinuation.induct)
  case (1 gCont a stmts)
  then show ?case apply (cases a; cases i; auto split: ApplyLinearGuardNodeResult.split_asm ApplyLinearGuardNodeResult.split)
    by (metis ApplyLinearGuardNodeResult.distinct(1))+
next
  case (2 gCont node innerG)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm)
next
  case (3 gCont g1 g2)
  then show ?case by (auto split: ApplyLinearGuardResult.split_asm ApplyLinearGuardResult.split simp add: applyLinearGuardNoMatchImpliesAddStatementsContinuationNoMatch)
qed

lemma applyLinearGuardMatchWithContinuationImpliesAddContinuationMatchWithContinuation:
"applyLinearGuard env state i g1 = AppliedLinearGuard newWarning newState (Some innerGCont) stmts \<Longrightarrow> applyLinearGuard env state i (addLinearGuardContinuation g2 g1) = AppliedLinearGuard newWarning newState (Some (addLinearGuardContinuation g2 innerGCont)) stmts"
proof (induction g2 g1 rule: addLinearGuardContinuation.induct)
  case (1 gCont a stmts)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm)
next
  case (2 gCont node innerG)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm)
next
  case (3 gCont g1 g2)
  then show ?case by (auto split: ApplyLinearGuardResult.split_asm ApplyLinearGuardResult.split simp add: applyLinearGuardNoMatchImpliesAddContinuationNoMatch)
qed

lemma applyLinearGuardMatchWithContinuationImpliesAddStatementsContinuationMatchWithContinuation:
"applyLinearGuard env state i g1 = AppliedLinearGuard newWarning newState (Some innerGCont) stmts \<Longrightarrow> applyLinearGuard env state i (addStatementsContinuation stmts2 g1) = AppliedLinearGuard newWarning newState (Some (addStatementsContinuation stmts2 innerGCont)) stmts"
proof (induction stmts g1 rule: addStatementsContinuation.induct)
  case (1 gCont a stmts)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm)
next
  case (2 gCont node innerG)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm)
next
  case (3 gCont g1 g2)
  then show ?case by (auto split: ApplyLinearGuardResult.split_asm ApplyLinearGuardResult.split simp add: applyLinearGuardNoMatchImpliesAddStatementsContinuationNoMatch)
qed

lemma preservationOfApplyGuard_GuardNoMatchImpliesLinearGuardNoMatch:
"applyGuard env state i g = ApplyGuardNoMatch \<Longrightarrow> applyLinearGuard env state i (linearizeGuardExpression g) = ApplyLinearGuardNoMatch"
proof (induction g rule: linearizeGuardExpression.induct)
  case (1 a)
  then show ?case by (cases a; cases i, auto)
next
  case (2 g1 g2)
  then show ?case by (auto split: ApplyGuardResult.split_asm option.split_asm simp add: applyLinearGuardNoMatchImpliesAddContinuationNoMatch)
next
  case (3 g stmts)
  then show ?case by (auto split: ApplyGuardResult.split_asm option.split_asm simp add: applyLinearGuardNoMatchImpliesAddStatementsContinuationNoMatch)
next
  case (4 g1 g2)
  then show ?case by (auto split: ApplyGuardResult.split_asm option.split_asm)
next
  case (5 g1 g2)
  then show ?case by (auto split: ApplyGuardResult.split_asm option.split_asm simp add: preservationOfApplyGuard_LinearGuardNoMatchImpliesInterleavedLinearGuardNoMatch)
qed

lemma applyGuardMatchNoContinuationGuardImpliesNotInterleaved:
"applyGuard env state i g = AppliedGuard warn newState None stmts \<Longrightarrow> \<not>(\<exists>g1 g2. g = InterleavedGuard g1 g2)"
  by (induction g arbitrary: env state i warn newState stmts rule: applyGuard.induct, auto split: ApplyGuardResult.split_asm option.split_asm)

lemma preservationOfApplyGuard_GuardMatchImpliesLinearGuardMatch:
"applyGuard env state i g = AppliedGuard warn newState None stmts \<Longrightarrow> applyLinearGuard env state i (linearizeGuardExpression g) = AppliedLinearGuard warn newState None stmts"
proof (induction g arbitrary: stmts rule: linearizeGuardExpression.induct)
  case (1 a)
  then show ?case by (cases a; cases i, auto split: if_split_asm)
next
  case (2 g1 g2)
  then show ?case by (auto split: ApplyGuardResult.split_asm option.split_asm simp add: applyLinearGuardNoMatchImpliesAddContinuationNoMatch)
next
  case (3 g1 innerStmts)
  then show ?case by (auto split: ApplyGuardResult.split_asm option.split_asm simp add: applyLinearGuardMatchImpliesAddStatementsContinuationMatch)
next
  case (4 g1 g2)
  then show ?case by (auto split: ApplyLinearGuardResult.split ApplyGuardResult.split_asm option.split_asm simp add: preservationOfApplyGuard_GuardNoMatchImpliesLinearGuardNoMatch)
next
  case (5 g1 g2)
  then show ?case by (auto split: ApplyGuardResult.split_asm option.split_asm simp add: preservationOfApplyGuard_LinearGuardNoMatchImpliesInterleavedLinearGuardNoMatch)
qed

lemma preservationOfApplyGuard_GuardMatchWithContinuationImpliesLinearGuardMatchWithContinuation:
"applyGuard env state i g = AppliedGuard warn newState (Some gCont) stmts \<Longrightarrow> applyLinearGuard env state i (linearizeGuardExpression g) = AppliedLinearGuard warn newState (Some (linearizeGuardExpression gCont)) stmts"
proof (induction g arbitrary: gCont stmts rule: linearizeGuardExpression.induct)
  case (1 a)
  then show ?case by (cases a; cases i, auto split: if_split_asm)
next
  case (2 g1 g2)
  then show ?case by (auto split: ApplyGuardResult.split_asm option.split_asm simp add: applyLinearGuardMatchImpliesAddContinuationMatch preservationOfApplyGuard_GuardMatchImpliesLinearGuardMatch applyLinearGuardMatchWithContinuationImpliesAddContinuationMatchWithContinuation)
next
  case (3 g1 innerStmts)
  then show ?case by (auto split: ApplyGuardResult.split_asm option.split_asm simp add: applyLinearGuardMatchImpliesAddStatementsContinuationMatch simp add: applyLinearGuardMatchWithContinuationImpliesAddStatementsContinuationMatchWithContinuation)
next
  case (4 g1 g2)
  then show ?case by (auto split: ApplyLinearGuardResult.split ApplyGuardResult.split_asm option.split_asm simp add: preservationOfApplyGuard_GuardNoMatchImpliesLinearGuardNoMatch)
next
  case (5 g1 g2)
  then show ?case apply (auto split: ApplyGuardResult.split_asm option.split_asm)
       apply (simp add: preservationOfApplyGuard_GuardMatchImpliesLinearGuardMatch preservationOfApplyGuard_LeftLinearGuardMatchNoContinueImpliesInterleavedLinearGuardMatchWithRightContinue(1))
      apply (simp add: preservationOfApplyGuard_LeftLinearGuardMatchWithContinueImpliesInterleavedLinearGuardMatchWithRightInterleavedContinue(1))
     apply (simp add: preservationOfApplyGuard_GuardMatchImpliesLinearGuardMatch preservationOfApplyGuard_GuardNoMatchImpliesLinearGuardNoMatch preservationOfApplyGuard_RightLinearGuardMatchNoContinueImpliesInterleavedLinearGuardMatchWithLeftContinue(1))
    by (simp add: preservationOfApplyGuard_GuardNoMatchImpliesLinearGuardNoMatch preservationOfApplyGuard_RightLinearGuardMatchWithContinueImpliesInterleavedLinearGuardMatchWithLeftContinueInterleaved(1))
qed

lemma preservationOfApplyGuard_ApplyCasesNoMatchImpliesApplyCasesRest:
"FaustusSemantics.applyCases env state i cases1 = FaustusSemantics.ApplyNoMatchError \<Longrightarrow> FaustusSemantics.applyCases env state i (cases1 @ cases2) = FaustusSemantics.applyCases env state i cases2"
proof (induction cases1)
  case Nil
  then show ?case by auto
next
  case (Cons a cases1)
  then show ?case apply auto
    subgoal premises ps proof (cases a)
      case (Case x1 x2)
      then show ?thesis using ps by (cases x1; cases i; auto)
    qed
    done
qed

lemma preservationOfApplyGuard_ApplyCasesMatchImpliesDropRest:
"FaustusSemantics.applyCases env state i cases1 = FaustusSemantics.Applied warn newState newCont \<Longrightarrow> FaustusSemantics.applyCases env state i (cases1 @ cases2) = FaustusSemantics.Applied warn newState newCont"
proof (induction cases1)
  case Nil
  then show ?case by auto
next
  case (Cons a cases1)
  then show ?case apply auto
    subgoal premises ps proof (cases a)
      case (Case x1 x2)
      then show ?thesis using ps by (cases x1; cases i; auto)
    qed
    done
qed

lemma preservationOfApplyGuard_LinearGuardNodeNoMatchImpliesCompiledLinearGuardNodeNoMatch:
"applyLinearGuardNode env state i node = ApplyLinearGuardNodeNoMatch \<Longrightarrow>
  FaustusSemantics.applyCases env state i ([compileLinearGuardNode node cont]) = FaustusSemantics.ApplyNoMatchError"
proof (induction node cont rule: compileLinearGuardNode.induct)
  case (1 a stmts cont)
  then show ?case by (cases a; cases i; auto)
qed

lemma preservationOfApplyGuard_LinearGuardNodeMatchImpliesCompiledLinearGuardNodeMatch:
"applyLinearGuardNode env state i node = AppliedLinearGuardNode warn newState stmts \<Longrightarrow>
  FaustusSemantics.applyCases env state i ([compileLinearGuardNode node cont]) = FaustusSemantics.Applied warn newState (compileStmts stmts cont)"
proof (induction node cont rule: compileLinearGuardNode.induct)
  case (1 a stmts cont)
  then show ?case by (cases a; cases i; auto split: if_split_asm)
qed

lemma preservationOfApplyGuard_LinearGuardNoMatchImpliesCompiledLinearGuardNoMatch:
"applyLinearGuard env state i g = ApplyLinearGuardNoMatch \<Longrightarrow>
  FaustusSemantics.applyCases env state i (compileLinearGuardExpression g cont t tcont) = FaustusSemantics.ApplyNoMatchError"
proof (induction g cont t tcont arbitrary: cont t tcont rule: compileLinearGuardExpression.induct)
  case (1 a cont t tcont)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm simp add: preservationOfApplyGuard_LinearGuardNodeNoMatchImpliesCompiledLinearGuardNodeNoMatch)
next
  case (2 g1 g2 cont t tcont)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm simp add: preservationOfApplyGuard_LinearGuardNodeNoMatchImpliesCompiledLinearGuardNodeNoMatch)
next
  case (3 g1 stmts cont t tcont)
  then show ?case by (auto split: ApplyLinearGuardResult.split_asm ApplyLinearGuardNodeResult.split_asm option.split_asm simp add: preservationOfApplyGuard_LinearGuardNodeNoMatchImpliesCompiledLinearGuardNodeNoMatch preservationOfApplyGuard_ApplyCasesNoMatchImpliesApplyCasesRest)
qed

lemma preservationOfApplyGuard_LinearGuardMatchImpliesCompiledLinearGuardMatch:
"applyLinearGuard env state i g = AppliedLinearGuard warn newState None stmts \<Longrightarrow>
  FaustusSemantics.applyCases env state i (compileLinearGuardExpression g cont t tcont) = FaustusSemantics.Applied warn newState (compileStmts stmts cont)"
proof (induction g cont t tcont arbitrary: stmts cont t tcont rule: compileLinearGuardExpression.induct)
  case (1 a cont t tcont)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm simp add: preservationOfApplyGuard_LinearGuardNodeMatchImpliesCompiledLinearGuardNodeMatch)
next
  case (2 g1 g2 cont t tcont)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm simp add: preservationOfApplyGuard_LinearGuardNodeMatchImpliesCompiledLinearGuardNodeMatch)
next
  case (3 a b c d e f g h)
  then show ?case by (auto split: ApplyLinearGuardResult.split_asm ApplyLinearGuardNodeResult.split_asm option.split_asm simp add: preservationOfApplyGuard_LinearGuardNodeMatchImpliesCompiledLinearGuardNodeMatch preservationOfApplyGuard_ApplyCasesNoMatchImpliesApplyCasesRest preservationOfApplyGuard_ApplyCasesMatchImpliesDropRest preservationOfApplyGuard_LinearGuardNoMatchImpliesCompiledLinearGuardNoMatch)
qed

lemma preservationOfApplyGuard_LinearGuardWithContinuationMatchImpliesCompiledLinearGuardMatch:
"applyLinearGuard env state i g = AppliedLinearGuard warn newState (Some gCont) stmts \<Longrightarrow>
  FaustusSemantics.applyCases env state i (compileLinearGuardExpression g cont t tcont) = FaustusSemantics.Applied warn newState (compileStmts stmts (FaustusTypes.When (compileLinearGuardExpression gCont cont t tcont) t tcont))"
proof (induction g cont t tcont arbitrary: stmts cont t tcont rule: compileLinearGuardExpression.induct)
  case (1 a cont t tcont)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm simp add: preservationOfApplyGuard_LinearGuardNodeMatchImpliesCompiledLinearGuardNodeMatch)
next
  case (2 g1 g2 cont t tcont)
  then show ?case by (auto split: ApplyLinearGuardNodeResult.split_asm simp add: preservationOfApplyGuard_LinearGuardNodeMatchImpliesCompiledLinearGuardNodeMatch)
next
  case (3 a b c d e f g h)
  then show ?case by (auto split: ApplyLinearGuardResult.split_asm ApplyLinearGuardNodeResult.split_asm option.split_asm simp add: preservationOfApplyGuard_LinearGuardNodeMatchImpliesCompiledLinearGuardNodeMatch preservationOfApplyGuard_ApplyCasesNoMatchImpliesApplyCasesRest preservationOfApplyGuard_ApplyCasesMatchImpliesDropRest preservationOfApplyGuard_LinearGuardNoMatchImpliesCompiledLinearGuardNoMatch)
qed

datatype ApplyResult = Applied ApplyWarning FState FContract
                     | ApplyNoMatchError

lemma preservationOfSemantics_prependStatementsIsSameAsCompiling:
"(\<forall>cont . compile (prependStatements stmts cont) = compileStmts stmts (compile cont))"
proof (induction stmts rule: compileStmts.induct)
  case 1
  then show ?case by auto 
next
  case (2 stmt stmts)
  then show ?case by auto
qed

fun applyCases :: "int \<Rightarrow> FContract \<Rightarrow> Environment \<Rightarrow> FState \<Rightarrow> Input \<Rightarrow> FCase list \<Rightarrow> ApplyResult" where
"applyCases t tcont env state input [] = ApplyNoMatchError" |
"applyCases t tcont env state input (Case g cont # rest) = (case applyGuard env state input g of
  ApplyGuardNoMatch \<Rightarrow> applyCases t tcont env state input rest
  | AppliedGuard w newState innerG stmts \<Rightarrow> (case innerG of
    None \<Rightarrow> Applied w newState (prependStatements stmts cont)
    | Some innerGReal \<Rightarrow> Applied w newState (prependStatements stmts (When [Case innerGReal cont] t tcont))))"

lemma preservationOfApplyGuard_FaustusV2MatchImpliesFaustusV1Match:
"applyGuard env state input g = AppliedGuard warn newState None stmts \<Longrightarrow>
  FaustusSemantics.applyCases env state input
    (compileGuardExpression g cont t tcont) = FaustusSemantics.Applied warn newState (compileStmts stmts cont)"
  by (simp add: preservationOfApplyGuard_GuardMatchImpliesLinearGuardMatch preservationOfApplyGuard_LinearGuardMatchImpliesCompiledLinearGuardMatch)


lemma preservationOfApplyGuard_FaustusV2MatchWithContinuationImpliesFaustusV1Match:
"applyGuard env state input g = AppliedGuard warn newState (Some gCont) stmts \<Longrightarrow>
  FaustusSemantics.applyCases env state input
    (compileGuardExpression g cont t tcont) = FaustusSemantics.Applied warn newState (compileStmts stmts (FaustusTypes.When (compileGuardExpression gCont cont t tcont) t tcont))"
  by (simp add: preservationOfApplyGuard_GuardMatchWithContinuationImpliesLinearGuardMatchWithContinuation preservationOfApplyGuard_LinearGuardWithContinuationMatchImpliesCompiledLinearGuardMatch)

lemma preservationOfApplyGuard_FaustusV2NoMatchImpliesFaustusV1NoMatch:
"applyGuard env state input g = ApplyGuardNoMatch \<Longrightarrow>
  FaustusSemantics.applyCases env state input
    (compileGuardExpression g cont t tcont @ compileCases rest t tcont) = FaustusSemantics.applyCases env state input (compileCases rest t tcont)"
  by (auto simp add: preservationOfApplyGuard_ApplyCasesNoMatchImpliesApplyCasesRest preservationOfApplyGuard_GuardNoMatchImpliesLinearGuardNoMatch preservationOfApplyGuard_LinearGuardNoMatchImpliesCompiledLinearGuardNoMatch)

lemma preservationOfApplyCases_FaustusV2ErrorImpliesFaustusV1Error:
"applyCases t tcont env state inp cases = ApplyNoMatchError \<Longrightarrow>
  wellTypedCases tyCtx cases \<Longrightarrow>
  wellTypedState tyCtx state \<Longrightarrow>
  compileCases cases t (compile tcont) = fV1Cases \<Longrightarrow>
  FaustusSemantics.applyCases env state inp fV1Cases = FaustusSemantics.ApplyNoMatchError"
proof (induction t tcont env state inp cases arbitrary: fV1Cases rule: applyCases.induct)
  case (1 t tcont env state input)
  then show ?case by auto
next
  case (2 t tcont env state input g cont rest)
  then show ?case using preservationOfApplyGuard_FaustusV2NoMatchImpliesFaustusV1NoMatch by (cases "applyGuard env state input g", auto split: option.split_asm)
qed

lemma preservationOfApplyCases_FaustusV2AppliedImpliesFaustusV1Applied:
"applyCases t tcont env state inp cases = Applied warn newState newCont \<Longrightarrow>
  wellTypedCases tyCtx cases \<Longrightarrow>
  wellTypedState tyCtx state \<Longrightarrow>
  compileCases cases t (compile tcont) = faustusV1Cases \<Longrightarrow>
  FaustusSemantics.applyCases env state inp faustusV1Cases = FaustusSemantics.Applied warn newState (compile newCont)"
proof (induction t tcont env state inp cases arbitrary: faustusV1Cases warn newState newCont rule: applyCases.induct)
  case (1 t tcont env state input)
  then show ?case by auto
next
  case (2 t tcont env state input g cont rest)
  then show ?case using preservationOfApplyGuard_ApplyCasesMatchImpliesDropRest preservationOfApplyGuard_FaustusV2MatchImpliesFaustusV1Match preservationOfSemantics_prependStatementsIsSameAsCompiling preservationOfApplyGuard_FaustusV2NoMatchImpliesFaustusV1NoMatch apply (cases "applyGuard env state input g", auto split: option.split_asm)
    by (smt (verit, ccfv_SIG) compileCases.simps(1) compileCases.simps(2) compileCorrectness_When append.right_neutral compileGuardExpression.elims preservationOfApplyGuard_ApplyCasesMatchImpliesDropRest preservationOfApplyGuard_FaustusV2MatchWithContinuationImpliesFaustusV1Match preservationOfSemantics_prependStatementsIsSameAsCompiling)
qed

lemma preservationOfApplyCases_FaustusV1ErrorImpliesFaustusV2Error:
"wellTypedCases tyCtx cases \<Longrightarrow>
  wellTypedState tyCtx state \<Longrightarrow>
  wellTypedContext tyCtx ctx \<Longrightarrow>
  FaustusSemantics.applyCases env state inp (compileCases cases t (compile tcont)) = FaustusSemantics.ApplyNoMatchError \<Longrightarrow>
  applyCases t tcont env state inp cases = ApplyNoMatchError"
  by (metis FaustusSemantics.ApplyResult.distinct(1) ApplyResult.exhaust preservationOfApplyCases_FaustusV2AppliedImpliesFaustusV1Applied)

lemma preservationOfApplyCases_FaustusV1AppliedImpliesFaustusV2Applied:
"(\<exists>newCont. wellTypedCases tyCtx cases \<longrightarrow>
  wellTypedState tyCtx state \<longrightarrow>
  wellTypedContext tyCtx ctx \<longrightarrow>
  FaustusSemantics.applyCases env state inp (compileCases cases t (compile tcont)) = FaustusSemantics.Applied warn newState newFaustusV1Cont \<longrightarrow>
  (applyCases t tcont env state inp cases = Applied warn newState newCont \<and> 
  (compile newCont) = newFaustusV1Cont))"
  by (smt (verit, ccfv_threshold) FaustusSemantics.ApplyResult.distinct(1) FaustusSemantics.ApplyResult.inject ApplyResult.exhaust preservationOfApplyCases_FaustusV2AppliedImpliesFaustusV1Applied preservationOfApplyCases_FaustusV2ErrorImpliesFaustusV1Error)

end