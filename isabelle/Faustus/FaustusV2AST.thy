theory FaustusV2AST
  imports Main FaustusTypes
begin

datatype FStatement = Pay FAccountId FPayee Token FValue
  | Assert FObservation
  | ReassignVal Identifier FValue
  | ReassignObservation Identifier FObservation
  | ReassignPubKey Identifier FParty

datatype FGuardExpression = ActionGuard FAction
  | GuardThenGuard FGuardExpression FGuardExpression
  | GuardStmtsGuard FGuardExpression "FStatement list"
  | DisjointGuard FGuardExpression FGuardExpression
  | InterleavedGuard FGuardExpression FGuardExpression

datatype FContract = Close
             | StatementCont FStatement FContract
             | If FObservation FContract FContract
             | When "FCase list" Timeout FContract
             | Let Identifier FValue FContract
             | LetObservation Identifier FObservation FContract
             | LetPubKey Identifier FParty FContract
             | LetC Identifier "FParameter list" FContract FContract
             | UseC Identifier "FArgument list"
and FCase = Case FGuardExpression FContract

fun fContractSize :: "FContract \<Rightarrow> nat" and
  fCasesSize :: "FCase list \<Rightarrow> nat" where
"fContractSize Close = 1"|
"fContractSize (StatementCont stmt cont) = 1 + fContractSize cont"|
"fContractSize (If obs trueCont falseCont) = 1 + fContractSize trueCont + fContractSize falseCont"|
"fContractSize (When cases t tCont) = 1 + fContractSize tCont + fCasesSize cases"|
"fContractSize (Let vid val cont) = 1 + fContractSize cont"|
"fContractSize (LetObservation obsId obs cont) = 1 + fContractSize cont"|
"fContractSize (LetC cid params boundCon cont) = 1 + size params + fContractSize boundCon + fContractSize cont"|
"fContractSize (UseC cid args) = 1 + size args"|
"fContractSize (LetPubKey pkId pk cont) = 1 + fContractSize cont"|
"fCasesSize Nil = 0"|
"fCasesSize ((Case act cont) # cs) = (fContractSize cont + 1) + fCasesSize cs"

lemma fContractSize_GT0:
"fContractSize con > 0"
  apply (cases "con")
  by auto

(* Helper datatype to show we remove interleaving by linearizing the expressions. *)
type_synonym LinearGuardNode = "(FAction \<times> FStatement list)"

(* TODO rename to normalized *)
datatype LinearGuardExpression =
  LinearGuardNode LinearGuardNode
  | LinearGuardThenGuard LinearGuardNode LinearGuardExpression
  | LinearDisjointGuard LinearGuardExpression LinearGuardExpression

datatype AbstractionInformation = ValueAbstraction Identifier
  | ObservationAbstraction Identifier
  | PubKeyAbstraction Identifier
  | ContractAbstraction "Identifier \<times> FParameter list \<times> FContract"

(* Stack of abstraction information. Will allow static scoping in execution and compilation. *)
type_synonym FContext = "AbstractionInformation list"

fun abstractionInformationSize :: "AbstractionInformation \<Rightarrow> nat" where
"abstractionInformationSize (PubKeyAbstraction pkId) = 0" |
"abstractionInformationSize (ValueAbstraction valId) = 0" |
"abstractionInformationSize (ObservationAbstraction obsId) = 0" |
"abstractionInformationSize (ContractAbstraction (cid, params, bodyCont)) = size params + fContractSize bodyCont"

fun paramsToFContext :: "FParameter list \<Rightarrow> FContext" where
"paramsToFContext [] = []" |
"paramsToFContext (ValueParameter vid#rest) = (ValueAbstraction vid)#(paramsToFContext rest)" |
"paramsToFContext (ObservationParameter obsId#rest) = (ObservationAbstraction obsId)#(paramsToFContext rest)" |
"paramsToFContext (PubKeyParameter pkId#rest) = (PubKeyAbstraction pkId)#(paramsToFContext rest)"

lemma paramsToFContextNoSize:
"paramsToFContext params = newCtx \<Longrightarrow>
  fold (+) (map (abstractionInformationSize) newCtx) 0 = 0"
  by (induction arbitrary: newCtx rule: paramsToFContext.induct, auto)

fun isFreshChoiceInChoiceId :: "ChoiceName \<Rightarrow> FChoiceId \<Rightarrow> bool" where
"isFreshChoiceInChoiceId cn (FChoiceId innerChoiceName p) = (cn \<noteq> innerChoiceName)"

fun isFreshChoiceInValue :: "ChoiceName \<Rightarrow> FValue \<Rightarrow> bool" and
  isFreshChoiceInObservation :: "ChoiceName \<Rightarrow> FObservation \<Rightarrow> bool" where
"isFreshChoiceInValue cn (Constant i) = True" |
"isFreshChoiceInValue cn (AvailableMoney accId token) = True" |
"isFreshChoiceInValue cn TimeIntervalStart = True" |
"isFreshChoiceInValue cn TimeIntervalEnd = True" |
"isFreshChoiceInValue cn (UseValue vId) = True" |
"isFreshChoiceInValue cn (NegValue v) = isFreshChoiceInValue cn v" |
"isFreshChoiceInValue cn (AddValue v1 v2) = (isFreshChoiceInValue cn v1 \<and> isFreshChoiceInValue cn v2)" |
"isFreshChoiceInValue cn (SubValue v1 v2) = (isFreshChoiceInValue cn v1 \<and> isFreshChoiceInValue cn v2)" |
"isFreshChoiceInValue cn (MulValue v1 v2) = (isFreshChoiceInValue cn v1 \<and> isFreshChoiceInValue cn v2)" |
"isFreshChoiceInValue cn (DivValue v1 v2) = (isFreshChoiceInValue cn v1 \<and> isFreshChoiceInValue cn v2)" |
"isFreshChoiceInValue cn (ChoiceValue choiceId) = isFreshChoiceInChoiceId cn choiceId" |
"isFreshChoiceInValue cn (Cond obs v1 v2) = (isFreshChoiceInObservation cn obs \<and> isFreshChoiceInValue cn v1 \<and> isFreshChoiceInValue cn v2)" |
"isFreshChoiceInObservation cn TrueObs = True" |
"isFreshChoiceInObservation cn FalseObs = True" |
"isFreshChoiceInObservation cn (UseObservation oId) = True" |
"isFreshChoiceInObservation cn (AndObs obs1 obs2) = (isFreshChoiceInObservation cn obs1 \<and> isFreshChoiceInObservation cn obs2)" |
"isFreshChoiceInObservation cn (OrObs obs1 obs2) = (isFreshChoiceInObservation cn obs1 \<and> isFreshChoiceInObservation cn obs2)" |
"isFreshChoiceInObservation cn (NotObs obs) = isFreshChoiceInObservation cn obs" |
"isFreshChoiceInObservation cn (ChoseSomething choiceId) = isFreshChoiceInChoiceId cn choiceId" |
"isFreshChoiceInObservation cn (ValueGE v1 v2) = (isFreshChoiceInValue cn v1 \<and> isFreshChoiceInValue cn v2)" |
"isFreshChoiceInObservation cn (ValueGT v1 v2) = (isFreshChoiceInValue cn v1 \<and> isFreshChoiceInValue cn v2)" |
"isFreshChoiceInObservation cn (ValueLT v1 v2) = (isFreshChoiceInValue cn v1 \<and> isFreshChoiceInValue cn v2)" |
"isFreshChoiceInObservation cn (ValueLE v1 v2) = (isFreshChoiceInValue cn v1 \<and> isFreshChoiceInValue cn v2)" |
"isFreshChoiceInObservation cn (ValueEQ v1 v2) = (isFreshChoiceInValue cn v1 \<and> isFreshChoiceInValue cn v2)"

fun isFreshChoiceInAction :: "ChoiceName \<Rightarrow> FAction \<Rightarrow> bool" where
"isFreshChoiceInAction cn (Choice (FChoiceId innerCn p) bounds) = (cn \<noteq> innerCn)" |
"isFreshChoiceInAction cn (Notify obs) = isFreshChoiceInObservation cn obs" |
"isFreshChoiceInAction cn (Deposit p1 p2 t v) = isFreshChoiceInValue cn v"

fun isFreshChoiceInStatement :: "ChoiceName \<Rightarrow> FStatement \<Rightarrow> bool" where
"isFreshChoiceInStatement cn (Pay accId payee token v) = isFreshChoiceInValue cn v" |
"isFreshChoiceInStatement cn (Assert obs) = isFreshChoiceInObservation cn obs" |
"isFreshChoiceInStatement cn (ReassignVal vId val) = isFreshChoiceInValue cn val" |
"isFreshChoiceInStatement cn (ReassignObservation oId obs) = isFreshChoiceInObservation cn obs" |
"isFreshChoiceInStatement cn (ReassignPubKey pkId pk) = True"

fun isFreshChoiceInStatements :: "ChoiceName \<Rightarrow> FStatement list \<Rightarrow> bool" where
"isFreshChoiceInStatements cn [] = True" |
"isFreshChoiceInStatements cn (s#stmts) = (isFreshChoiceInStatement cn s \<and> isFreshChoiceInStatements cn stmts)"

fun isFreshChoiceInGuard :: "ChoiceName \<Rightarrow> FGuardExpression \<Rightarrow> bool" where
"isFreshChoiceInGuard cn (ActionGuard a) = isFreshChoiceInAction cn a" |
"isFreshChoiceInGuard cn (GuardThenGuard g1 g2) = (isFreshChoiceInGuard cn g1 \<and> isFreshChoiceInGuard cn g2)" |
"isFreshChoiceInGuard cn (GuardStmtsGuard g stmts) = (isFreshChoiceInGuard cn g \<and> isFreshChoiceInStatements cn stmts)" |
"isFreshChoiceInGuard cn (DisjointGuard g1 g2) = (isFreshChoiceInGuard cn g1 \<and> isFreshChoiceInGuard cn g2)" |
"isFreshChoiceInGuard cn (InterleavedGuard g1 g2) = (isFreshChoiceInGuard cn g1 \<and> isFreshChoiceInGuard cn g2)"

fun isFreshChoiceInArguments :: "ChoiceName \<Rightarrow> FArgument list \<Rightarrow> bool" where
"isFreshChoiceInArguments cn [] = True" |
"isFreshChoiceInArguments cn ((ValueArgument val)#args) = (isFreshChoiceInValue cn val \<and> isFreshChoiceInArguments cn args)" |
"isFreshChoiceInArguments cn ((ObservationArgument obs)#args) = (isFreshChoiceInObservation cn obs \<and> isFreshChoiceInArguments cn args)" |
"isFreshChoiceInArguments cn ((PubKeyArgument pk)#args) = (isFreshChoiceInArguments cn args)"

fun isFreshChoiceInContract :: "ChoiceName \<Rightarrow> FContract \<Rightarrow> bool" and isFreshChoiceInCases :: "ChoiceName \<Rightarrow> FCase list \<Rightarrow> bool" where
"isFreshChoiceInContract cn Close = True" |
"isFreshChoiceInContract cn (StatementCont s cont) = (isFreshChoiceInStatement cn s \<and> isFreshChoiceInContract cn cont)" |
"isFreshChoiceInContract cn (If obs cont1 cont2) = (isFreshChoiceInObservation cn obs \<and> isFreshChoiceInContract cn cont1 \<and> isFreshChoiceInContract cn cont2)" |
"isFreshChoiceInContract cn (When cases t tcont) = (isFreshChoiceInContract cn tcont \<and> isFreshChoiceInCases cn cases)" |
"isFreshChoiceInContract cn (Let valId val cont) = (isFreshChoiceInValue cn val \<and> isFreshChoiceInContract cn cont)" |
"isFreshChoiceInContract cn (LetObservation valId obs cont) = (isFreshChoiceInObservation cn obs \<and> isFreshChoiceInContract cn cont)" |
"isFreshChoiceInContract cn (LetPubKey pkId pk cont) = (isFreshChoiceInContract cn cont)" |
"isFreshChoiceInContract cn (LetC cId params bodyCont cont) = (isFreshChoiceInContract cn bodyCont \<and> isFreshChoiceInContract cn cont)" |
"isFreshChoiceInContract cn (UseC cId args) = (isFreshChoiceInArguments cn args)" |
"isFreshChoiceInCases cn [] = True" |
"isFreshChoiceInCases cn (Case g cont#cases) = (isFreshChoiceInGuard cn g \<and> isFreshChoiceInContract cn cont \<and> isFreshChoiceInCases cn cases)"

fun isFreshChoiceInCasesGuardsShallow :: "ChoiceName \<Rightarrow> FCase list \<Rightarrow> bool" where
"isFreshChoiceInCasesGuardsShallow cn [] = True" |
"isFreshChoiceInCasesGuardsShallow cn ((Case g cont)#cases) = (isFreshChoiceInGuard cn g \<and> isFreshChoiceInCasesGuardsShallow cn cases)"

fun isFreshChoiceInCasesGuards :: "ChoiceName \<Rightarrow> FCase list \<Rightarrow> bool" where
"isFreshChoiceInCasesGuards cn [] = True" |
"isFreshChoiceInCasesGuards cn ((Case g cont)#cases) = (isFreshChoiceInGuard cn g \<and> isFreshChoiceInCasesGuards cn cases)"

lemma isFreshChoiceInCasesImpliesIsFreshChoiceInCasesGuards:
"isFreshChoiceInCases cn cases \<Longrightarrow>
  isFreshChoiceInCasesGuards cn cases"
  apply (induction cases, auto)
  by (metis isFreshChoiceInCases.simps(2) isFreshChoiceInCasesGuards.elims(3) list.inject)

lemma isFreshChoiceInCasesGuardsImpliesIsFreshChoiceInCasesGuardsShallow:
"isFreshChoiceInCasesGuards cn cases \<Longrightarrow>
  isFreshChoiceInCasesGuardsShallow cn cases"
  apply (induction cases, auto)
  by (metis FaustusV2AST.FCase.exhaust isFreshChoiceInCasesGuards.simps(2) isFreshChoiceInCasesGuardsShallow.simps(2))

lemma isFreshChoiceInCasesPreservedInSlice:
"isFreshChoiceInCases cn cases \<Longrightarrow>
  cases = c1@c2 \<Longrightarrow>
  isFreshChoiceInCases cn c1 \<and> isFreshChoiceInCases cn c2"
proof (induction cases arbitrary: c1 c2)
  case Nil
  then show ?case by auto
next
  case (Cons a cases)
  then show ?case apply (cases a, meson)
     apply (smt (verit, best) Cons_eq_append_conv isFreshChoiceInCases.simps(1) isFreshChoiceInCases.simps(2))
    by (smt (verit) append_eq_Cons_conv append_eq_append_conv2 isFreshChoiceInCases.elims(1) isFreshChoiceInCases.simps(1) isFreshChoiceInCases.simps(2) list.inject)
qed

lemma isFreshChoiceInCasesGuardsPreservedInSlice:
"isFreshChoiceInCasesGuards cn cases \<Longrightarrow>
  cases = c1@c2 \<Longrightarrow>
  isFreshChoiceInCasesGuards cn c1 \<and> isFreshChoiceInCasesGuards cn c2"
proof (induction cases arbitrary: c1 c2)
  case Nil
  then show ?case by auto
next
  case (Cons a cases)
  then show ?case apply (cases a, meson)
     apply (smt (verit, best) Cons_eq_append_conv isFreshChoiceInCasesGuards.simps(1) isFreshChoiceInCasesGuards.simps(2))
    by (smt (verit) append_eq_Cons_conv append_eq_append_conv2 isFreshChoiceInCasesGuards.elims(1) isFreshChoiceInCasesGuards.simps list.inject)
qed

lemma isFreshChoiceInCasesGuardsShallowPreservedInSlice:
"isFreshChoiceInCasesGuardsShallow cn cases \<Longrightarrow>
  cases = c1@c2 \<Longrightarrow>
  isFreshChoiceInCasesGuardsShallow cn c1 \<and> isFreshChoiceInCasesGuardsShallow cn c2"
proof (induction cn cases arbitrary: c1 c2 rule: isFreshChoiceInCasesGuardsShallow.induct)
  case (1 cn)
  then show ?case by auto
next
  case (2 cn g cont cases)
  then show ?case apply meson
     apply (metis Cons_eq_append_conv isFreshChoiceInCasesGuardsShallow.simps(1) isFreshChoiceInCasesGuardsShallow.simps(2))
    by (metis Cons_eq_append_conv isFreshChoiceInCasesGuardsShallow.simps(2))
qed

fun isFreshChoiceInContext :: "ChoiceName \<Rightarrow> FContext \<Rightarrow> bool" where
"isFreshChoiceInContext cn [] = True" |
"isFreshChoiceInContext cn (PubKeyAbstraction i#ctx) = isFreshChoiceInContext cn ctx" |
"isFreshChoiceInContext cn (ValueAbstraction i#ctx) = isFreshChoiceInContext cn ctx" |
"isFreshChoiceInContext cn (ObservationAbstraction i#ctx) = isFreshChoiceInContext cn ctx" |
"isFreshChoiceInContext cn (ContractAbstraction (i, params, body)#ctx) = (isFreshChoiceInContract cn body \<and> isFreshChoiceInContext cn ctx)"

fun isFreshChoiceInChoices :: "ChoiceName \<Rightarrow> (ChoiceId \<times> ChosenNum) list \<Rightarrow> bool" where
"isFreshChoiceInChoices cn [] = True" |
"isFreshChoiceInChoices cn ((ChoiceId innerCn p, n)#cs) = (cn \<noteq> innerCn \<and> isFreshChoiceInChoices cn cs)"

fun isFreshChoiceInState :: "ChoiceName \<Rightarrow> FState \<Rightarrow> bool" where
"isFreshChoiceInState cn s = isFreshChoiceInChoices cn (choices s)"

end