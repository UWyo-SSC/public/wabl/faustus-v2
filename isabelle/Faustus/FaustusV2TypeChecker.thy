theory FaustusV2TypeChecker
  imports Main Util.MList Util.SList Core.ListTools Core.Semantics SmallStep "HOL-Library.Product_Lexorder" "HOL.Product_Type" "HOL-IMP.Star" FaustusTypes FaustusSemantics FaustusV2AST
begin

(* Type rules require the identifier is most recently associated with the correct type in the stack.*)
fun lookupType :: "TypeContext \<Rightarrow> Identifier \<Rightarrow> AbstractionTypeInformation option" where
"lookupType [] i = None" |
"lookupType ((boundId, tyInfo)#rest) i = (if i = boundId then Some tyInfo else lookupType rest i)"

fun wellTypedStatement :: "TypeContext \<Rightarrow> FStatement \<Rightarrow> bool" where
"wellTypedStatement tyCtx (Assert obs) = (wellTypedObservation tyCtx obs)" |
"wellTypedStatement tyCtx (Pay accId payee token val) =(wellTypedParty tyCtx accId \<and> wellTypedPayee tyCtx payee \<and> wellTypedValue tyCtx val)" |
"wellTypedStatement tyCtx (ReassignVal valId val) = (lookupType tyCtx valId = (Some ValueType) \<and> wellTypedValue tyCtx val)" |
"wellTypedStatement tyCtx (ReassignObservation obsId obs) = (lookupType tyCtx obsId = (Some ObservationType) \<and> wellTypedObservation tyCtx obs)" |
"wellTypedStatement tyCtx (ReassignPubKey pkId pk) = (lookupType tyCtx pkId = (Some PubKeyType) \<and> wellTypedParty tyCtx pk)"

fun updateTyCtxFromStmt :: "TypeContext \<Rightarrow> FStatement \<Rightarrow> TypeContext" where
"updateTyCtxFromStmt tyCtx (ReassignVal valId val) = ((valId, ValueType)#tyCtx)" |
"updateTyCtxFromStmt tyCtx (ReassignObservation obsId obs) = ((obsId, ObservationType)#tyCtx)" |
"updateTyCtxFromStmt tyCtx (ReassignPubKey pkId pk) = ((pkId, PubKeyType)#tyCtx)" |
"updateTyCtxFromStmt tyCtx _ = tyCtx"

fun wellTypedStatements :: "TypeContext \<Rightarrow> FStatement list \<Rightarrow> bool" where
"wellTypedStatements tyCtx [] = True" |
"wellTypedStatements tyCtx (s#rest) = ((wellTypedStatement tyCtx s) \<and> (wellTypedStatements (updateTyCtxFromStmt tyCtx s) rest))"

fun updateTyCtxFromStmts :: "TypeContext \<Rightarrow> FStatement list \<Rightarrow> TypeContext" where
"updateTyCtxFromStmts tyCtx [] = tyCtx" |
"updateTyCtxFromStmts tyCtx (s#rest) = (updateTyCtxFromStmts (updateTyCtxFromStmt tyCtx s) rest)"

fun wellTypedAction :: "TypeContext \<Rightarrow> FAction \<Rightarrow> bool" where
"wellTypedAction tyCtx (Deposit fromPk toPk token value) = ((wellTypedParty tyCtx fromPk) \<and> (wellTypedParty tyCtx toPk) \<and> (wellTypedValue tyCtx value))" |
"wellTypedAction tyCtx (Choice choiceId bounds) = (wellTypedChoiceId tyCtx choiceId)" |
"wellTypedAction tyCtx (Notify observation) = (wellTypedObservation tyCtx observation)"

fun wellTypedGuardExpression :: "TypeContext \<Rightarrow> FGuardExpression \<Rightarrow> bool" where
"wellTypedGuardExpression tyCtx (ActionGuard a) = (wellTypedAction tyCtx a)" |
"wellTypedGuardExpression tyCtx (GuardThenGuard g1 g2) = ((wellTypedGuardExpression tyCtx g1) \<and> (wellTypedGuardExpression tyCtx g2))" |
"wellTypedGuardExpression tyCtx (GuardStmtsGuard g stmts) = ((wellTypedGuardExpression tyCtx g) \<and> (wellTypedStatements tyCtx stmts))" |
"wellTypedGuardExpression tyCtx (DisjointGuard g1 g2) = ((wellTypedGuardExpression tyCtx g1) \<and> (wellTypedGuardExpression tyCtx g2))" |
"wellTypedGuardExpression tyCtx (InterleavedGuard g1 g2) = ((wellTypedGuardExpression tyCtx g1) \<and> (wellTypedGuardExpression tyCtx g2))"

fun wellTypedContract :: "TypeContext \<Rightarrow> FContract \<Rightarrow> bool" and
  wellTypedCases :: "TypeContext \<Rightarrow> FCase list \<Rightarrow> bool" where
"wellTypedContract tyCtx Close = True" |
"wellTypedContract tyCtx (StatementCont stmt cont) = (wellTypedStatement tyCtx stmt \<and> (wellTypedContract (updateTyCtxFromStmt tyCtx stmt) cont))" |
"wellTypedContract tyCtx (If obs cont1 cont2) =
  (wellTypedObservation tyCtx obs \<and> wellTypedContract tyCtx cont1 \<and> wellTypedContract tyCtx cont2)" |
"wellTypedContract tyCtx (When cases t cont) = (wellTypedCases tyCtx cases \<and> wellTypedContract tyCtx cont)" |
"wellTypedContract tyCtx (Let valId val cont) = (wellTypedValue tyCtx val \<and> wellTypedContract ((valId, ValueType)#tyCtx) cont)" |
"wellTypedContract tyCtx (LetObservation obsId obs cont) = (wellTypedObservation tyCtx obs \<and> wellTypedContract ((obsId, ObservationType)#tyCtx) cont)" |
"wellTypedContract tyCtx (LetPubKey pkId pk cont) = (wellTypedParty tyCtx pk \<and> wellTypedContract ((pkId, PubKeyType)#tyCtx) cont)" |
"wellTypedContract tyCtx (LetC cid params body cont) = (wellTypedContract ((paramsToTypeContext params)@tyCtx) body \<and> wellTypedContract ((cid, ContractType params)#tyCtx) cont)" |
"wellTypedContract tyCtx (UseC cid args) = (case lookupContractIdParamTypeInformation tyCtx cid of
  Some (params, innerTyCtx) \<Rightarrow> wellTypedArguments tyCtx params args
  | _ \<Rightarrow> False)" |
"wellTypedCases tyCtx [] = True" |
"wellTypedCases tyCtx ((Case g c)#rest) = ((wellTypedGuardExpression tyCtx g) \<and> (wellTypedContract tyCtx c) \<and> (wellTypedCases tyCtx rest))"

fun wellTypedContext :: "TypeContext \<Rightarrow> FContext \<Rightarrow> bool" where
"wellTypedContext [] [] = True" |
"wellTypedContext tyCtx [] = False" |
"wellTypedContext [] ctx = False" |
"wellTypedContext ((tyValId, ValueType)#restTypes) ((ValueAbstraction absValId)#restAbstractions) = 
  (tyValId = absValId \<and> wellTypedContext restTypes restAbstractions)" |
"wellTypedContext ((tyPkId, PubKeyType)#restTypes) ((PubKeyAbstraction absPkId)#restAbstractions) = 
  (tyPkId = absPkId \<and> wellTypedContext restTypes restAbstractions)" |
"wellTypedContext ((tyObsId, ObservationType)#restTypes) ((ObservationAbstraction absObsId)#restAbstractions) = 
  (tyObsId = absObsId \<and> wellTypedContext restTypes restAbstractions)" |
"wellTypedContext ((tyCid, ContractType tyParams)#restTypes) ((ContractAbstraction (absCid, absParams, absBody))#restAbstractions) = 
  (tyCid = absCid \<and> tyParams = absParams \<and> wellTypedContract ((paramsToTypeContext absParams)@restTypes) absBody \<and> wellTypedContext restTypes restAbstractions)" |
"wellTypedContext (someType#restTypes) (someAbs#restAbs) = False"

lemma consValueWellTypedContext:
"wellTypedContext tyCtx ctx \<longleftrightarrow>
  wellTypedContext ((valId, ValueType)#tyCtx) (ValueAbstraction valId#ctx)"
  by simp

lemma consObservationWellTypedContext:
"wellTypedContext tyCtx ctx \<longleftrightarrow>
  wellTypedContext ((obsId, ObservationType)#tyCtx) (ObservationAbstraction obsId#ctx)"
  by simp

lemma consPubKeyWellTypedContext:
"wellTypedContext tyCtx ctx \<longleftrightarrow>
  wellTypedContext ((pkId, PubKeyType)#tyCtx) (PubKeyAbstraction pkId#ctx)"
  by simp

lemma consContractWellTypedContext:
"wellTypedContext tyCtx ctx \<and> wellTypedContract ((paramsToTypeContext params)@tyCtx) bodyCont \<longleftrightarrow>
  wellTypedContext ((cid, ContractType params)#tyCtx) (ContractAbstraction (cid, params, bodyCont)#ctx)"
  by auto

lemma paramsWellTypedContext:
"wellTypedContext (paramsToTypeContext params) (paramsToFContext params)"
proof (induction params)
  case Nil
  then show ?case by auto
next
  case (Cons firstParam params)
  then show ?case by (cases firstParam, auto)
qed

lemma wellTypedActionAppendTypes [simp]:
"(\<forall> tyCtx2 . wellTypedAction tyCtx a \<longrightarrow>
  wellTypedAction (tyCtx @ tyCtx2) a)"
proof (induction rule: wellTypedAction.induct)
  case (1 tyCtx fromPk toPk token "value")
  then show ?case by (auto simp add: wellTypedPartyAppendTypes wellTypedValueAppendTypes)
next
  case (2 tyCtx choiceId bounds)
  then show ?case by (auto simp add: wellTypedChoiceIdAppendTypes)
next
  case (3 tyCtx observation)
  then show ?case by (auto simp add: wellTypedValueAppendTypes)
qed

lemma lookupTypeAppendTypes:
"(\<forall> tyCtx2 . lookupType tyCtx i = Some res \<longrightarrow>
  lookupType (tyCtx @ tyCtx2) i = Some res)"
  by (induction tyCtx, auto)

lemma wellTypedStatementAppendTypes:
"(\<forall> tyCtx2 . wellTypedStatement tyCtx s \<longrightarrow>
  wellTypedStatement (tyCtx @ tyCtx2) s)"
proof (induction rule: wellTypedStatement.induct)
  case (1 tyCtx obs)
  then show ?case by (auto simp add: wellTypedValueAppendTypes)
next
  case (2 tyCtx accId payee token val)
  then show ?case by (auto simp add: wellTypedValueAppendTypes wellTypedPartyAppendTypes wellTypedPayeeAppendTypes)
next
  case (3 tyCtx valId val)
  then show ?case by (auto simp add: lookupTypeAppendTypes wellTypedValueAppendTypes)
next
  case (4 tyCtx obsId obs)
  then show ?case by (auto simp add: lookupTypeAppendTypes wellTypedValueAppendTypes)
next
  case (5 tyCtx pkId pk)
  then show ?case by (auto simp add: lookupTypeAppendTypes wellTypedPartyAppendTypes)
qed

lemma wellTypedStatementsAppendTypes:
"(\<forall> tyCtx2 . wellTypedStatements tyCtx stmts \<longrightarrow>
  wellTypedStatements (tyCtx @ tyCtx2) stmts)"
proof (induction rule: wellTypedStatements.induct)
  case (1 tyCtx)
  then show ?case by auto
next
  case (2 tyCtx s rest)
  then show ?case by (cases s, auto simp add: lookupTypeAppendTypes wellTypedStatementAppendTypes wellTypedPayeeAppendTypes wellTypedValueAppendTypes wellTypedPartyAppendTypes)
qed

lemma wellTypedGuardExpressionAppendTypes:
"(\<forall> tyCtx2 . wellTypedGuardExpression tyCtx g \<longrightarrow>
  wellTypedGuardExpression (tyCtx @ tyCtx2) g)"
proof (induction rule: wellTypedGuardExpression.induct)
  case (1 tyCtx a)
  then show ?case by auto
next
  case (2 tyCtx g1 g2)
  then show ?case by auto
next
  case (3 tyCtx g stmts)
  then show ?case by (auto simp add: wellTypedStatementsAppendTypes)
next
  case (4 tyCtx g1 g2)
  then show ?case by auto
next
  case (5 tyCtx g1 g2)
  then show ?case by auto
qed


lemma wellTypedContractAppendTypes:
"(\<forall> tyCtx2 . wellTypedContract tyCtx cont \<longrightarrow>
  wellTypedContract (tyCtx @ tyCtx2) cont)"
"(\<forall> tyCtx2 . wellTypedCases tyCtx cases \<longrightarrow>
  wellTypedCases (tyCtx @ tyCtx2) cases)"
proof (induction rule: wellTypedContract_wellTypedCases.induct)
  case (1 tyCtx)
  then show ?case by auto
next
  case (2 tyCtx s cont)
  then show ?case by (cases s, auto simp add: lookupTypeAppendTypes wellTypedStatementAppendTypes wellTypedPayeeAppendTypes wellTypedValueAppendTypes wellTypedPartyAppendTypes)
next
  case (3 tyCtx obs cont1 cont2)
  then show ?case by (simp add: wellTypedValueAppendTypes(2))
next
  case (4 tyCtx cases t cont)
  then show ?case by simp
next
  case (5 tyCtx valId val cont)
  then show ?case by (simp add: wellTypedValueAppendTypes(1))
next
  case (6 tyCtx obsId obs cont)
  then show ?case by (simp add: wellTypedValueAppendTypes(2))
next
  case (7 tyCtx pkId pk cont)
  then show ?case by (simp add: wellTypedPartyAppendTypes)
next
  case (8 tyCtx cid params body cont)
  then show ?case by auto
next
  case (9 tyCtx cid args)
  then show ?case by (cases "lookupContractIdParamTypeInformation tyCtx cid", auto simp add: lookupCidAppendTypes wellTypedArgumentsAppendTypes)
next
  case (10 tyCtx)
  then show ?case by simp
next
  case (11 tyCtx g c rest)
  then show ?case by (simp add: wellTypedGuardExpressionAppendTypes)
qed

lemma concatWellTypedContexts:
"wellTypedContext tyCtx1 ctx1 \<Longrightarrow>
  wellTypedContext tyCtx2 ctx2 \<Longrightarrow>
  wellTypedContext (tyCtx2 @ tyCtx1) (ctx2 @ ctx1)"
  apply (induction tyCtx2 ctx2 rule: wellTypedContext.induct, auto)
  using wellTypedContractAppendTypes(1) by fastforce

fun lookupContractIdAbsInformation :: "FContext \<Rightarrow> Identifier \<Rightarrow> (FParameter list \<times> FContract \<times> FContext) option" where
"lookupContractIdAbsInformation [] cid = None" |
"lookupContractIdAbsInformation (ContractAbstraction (boundCid, params, bodyCont)#restAbs) cid = (if boundCid = cid
  then Some (params, bodyCont, restAbs)
  else lookupContractIdAbsInformation restAbs cid)" |
"lookupContractIdAbsInformation (ValueAbstraction i#restAbs) cid = (if i = cid then None else lookupContractIdAbsInformation restAbs cid)" |
"lookupContractIdAbsInformation (ObservationAbstraction i#restAbs) cid = (if i = cid then None else lookupContractIdAbsInformation restAbs cid)" |
"lookupContractIdAbsInformation (PubKeyAbstraction i#restAbs) cid = (if i = cid then None else lookupContractIdAbsInformation restAbs cid)"

lemma lookupContractTypeWellTypedContext:
"(\<forall>cid params bodyCont restCtx. \<exists>restTy . wellTypedContext tyCtx ctx \<longrightarrow>
  lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, restCtx) \<longrightarrow>
  lookupContractIdParamTypeInformation tyCtx cid = Some (params, restTy))"
  by (induction tyCtx ctx rule: wellTypedContext.induct, auto)

lemma lookupContractAbstractionWellTypedContext:
"(\<forall>cid params restTy. \<exists>bodyCont restCtx . wellTypedContext tyCtx ctx \<longrightarrow>
  lookupContractIdParamTypeInformation tyCtx cid = Some (params, restTy) \<longrightarrow>
  lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, restCtx))"
  by (induction tyCtx ctx rule: wellTypedContext.induct, auto)

lemma lookupContractRestIsWellTyped:
"(\<forall>cid params restTy bodyCont restCtx . wellTypedContext tyCtx ctx \<longrightarrow>
  lookupContractIdParamTypeInformation tyCtx cid = Some (params, restTy) \<longrightarrow>
  lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, restCtx) \<longrightarrow>
  wellTypedContext restTy restCtx)"
  by (induction tyCtx ctx rule: wellTypedContext.induct, auto)

lemma lookupContractFoundContractIsWellTyped:
"(\<forall>cid params restTy bodyCont restCtx . wellTypedContext tyCtx ctx \<longrightarrow>
  lookupContractIdParamTypeInformation tyCtx cid = Some (params, restTy) \<longrightarrow>
  lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, restCtx) \<longrightarrow>
  wellTypedContract (paramsToTypeContext params @ restTy) bodyCont)"
  by (induction tyCtx ctx rule: wellTypedContext.induct, auto)

lemma lookupContractStateStaysWellTyped:
"(\<forall>cid params restTy restCtx .
  wellTypedState tyCtx s \<longrightarrow>
  lookupContractIdParamTypeInformation tyCtx cid = Some (params, restTy) \<longrightarrow>
  wellTypedState restTy s)"
  by (induction tyCtx s rule: wellTypedState.induct, auto)

lemma lookupContractCompilerContextSmaller:
"lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, innerCtx) \<Longrightarrow>
  1 + fold (+) (map (abstractionInformationSize) ctx) 0 > fContractSize bodyCont + fold (+) (map (abstractionInformationSize) (innerCtx)) 0"
  apply (induction rule: lookupContractIdAbsInformation.induct, auto)
     apply (smt add.commute add.right_neutral fold_plus_sum_list_rev less_add_Suc1 less_trans nat_add_left_cancel_less not_less_eq not_less_iff_gr_or_eq option.inject prod.inject)
    apply (meson option.distinct(1))
   apply (meson option.distinct(1))
  by (meson option.distinct(1))


end