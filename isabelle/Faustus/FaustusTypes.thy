theory FaustusTypes
  imports Main Util.MList Util.SList Core.ListTools Core.Semantics SmallStep "HOL-Library.Product_Lexorder" "HOL.Product_Type" "HOL-IMP.Star"
begin

type_synonym Identifier = ValueId

datatype FParty = ConstantPubKey ByteString
  | UseParty Identifier
  | Role TokenName

fun names_party :: "FParty \<Rightarrow> Identifier set" where
"names_party (UseParty pId) = {pId}" |
"names_party (ConstantPubKey pk) = {}" |
"names_party (Role role) = {}"

type_synonym FAccountId = FParty

fun less_eq_FParty :: "FParty \<Rightarrow> FParty \<Rightarrow> bool" where
"less_eq_FParty (ConstantPubKey a) (UseParty b) = True" |
"less_eq_FParty (ConstantPubKey a) (Role b) = True" |
"less_eq_FParty (Role a) (UseParty b) = True" |
"less_eq_FParty (UseParty a) (ConstantPubKey b) = False" |
"less_eq_FParty (UseParty a) (Role b) = False" |
"less_eq_FParty (Role a) (ConstantPubKey b) = False" |
"less_eq_FParty (ConstantPubKey a) (ConstantPubKey b) = (less_eq_BS a b)" |
"less_eq_FParty (Role a) (Role b) = (less_eq_BS a b)" |
"less_eq_FParty (UseParty a) (UseParty b) = (a \<le> b)"

fun less_FParty :: "FParty \<Rightarrow> FParty \<Rightarrow> bool" where
"less_FParty a b = (\<not> (less_eq_FParty b a))"

instantiation "FParty" :: "ord"
begin
definition "a \<le> b = less_eq_FParty a b"
definition "a < b = less_FParty a b"
instance
proof
qed
end

lemma linearFParty : "x \<le> y \<or> y \<le> (x::FParty)"
  by (cases x y rule: FParty.exhaust[case_product FParty.exhaust], auto simp add: less_eq_FParty_def lineaBS)

instantiation "FParty" :: linorder
begin
instance
proof
  fix x y
  have "(x < y) = (x \<le> y \<and> \<not> y \<le> (x :: FParty))"
    by (meson less_FParty.elims less_FParty.elims less_FParty_def less_eq_FParty_def linearFParty)
  thus "(x < y) = (x \<le> y \<and> \<not> y \<le> x)" by simp
next
  fix x
  have "x \<le> (x :: FParty)" by (meson linearFParty)
  thus "x \<le> x" by simp
next
  fix x y z
  have "x \<le> y \<Longrightarrow> y \<le> z \<Longrightarrow> x \<le> (z :: FParty)"
    by (cases x y z rule: FParty.exhaust[case_product FParty.exhaust[case_product FParty.exhaust]], auto simp add: less_eq_FParty_def lineaBS byteStringOrder)
  thus "x \<le> y \<Longrightarrow> y \<le> z \<Longrightarrow> x \<le> z" by simp
next
  fix x y z
  have "x \<le> y \<Longrightarrow> y \<le> x \<Longrightarrow> x = (y :: FParty)"
    by (cases x y rule: FParty.exhaust[case_product FParty.exhaust], auto simp add: less_eq_FParty_def lineaBS byteStringOrder byteStringLessEqTwiceEq)
  thus "x \<le> y \<Longrightarrow> y \<le> x \<Longrightarrow> x = (y :: FParty)" by simp
next
  fix x y
  from linearFParty have "x \<le> y \<or> y \<le> (x :: FParty)" by simp
  thus "x \<le> y \<or> y \<le> x" by simp
qed
end

datatype FPayee = Account FParty
  | Party FParty

fun names_payee :: "FPayee \<Rightarrow> Identifier set" where
"names_payee (Account p) = names_party p" |
"names_payee (Party p) = names_party p"

type_synonym FaustusParty = FParty

datatype FChoiceId = FChoiceId ChoiceName FaustusParty

fun names_choiceId :: "FChoiceId \<Rightarrow> Identifier set" where
"names_choiceId (FChoiceId cname p) = names_party p"

(* BEGIN Proof of linorder for FChoiceId *)
fun less_eq_FaustusChoId :: "FChoiceId \<Rightarrow> FChoiceId \<Rightarrow> bool" where
"less_eq_FaustusChoId (FChoiceId a b) (FChoiceId c d) =
   (if less_BS a c then True
    else (if (less_BS c a) then False else b \<le> d))"

fun less_FaustusChoId :: "FChoiceId \<Rightarrow> FChoiceId \<Rightarrow> bool" where
"less_FaustusChoId a b = (\<not> (less_eq_FaustusChoId b a))"

instantiation "FChoiceId" :: "ord"
begin
definition "a \<le> b = less_eq_FaustusChoId a b"
definition "a < b = less_FaustusChoId a b"
instance
proof
qed
end

lemma linearFChoiceId : "x \<le> y \<or> y \<le> (x::FChoiceId)"
  by (cases x y rule: FChoiceId.exhaust[case_product FChoiceId.exhaust], auto simp add: less_eq_FChoiceId_def)

instantiation "FChoiceId" :: linorder
begin
instance
proof
  fix x y
  have "(x < y) = (x \<le> y \<and> \<not> y \<le> (x :: FChoiceId))"
    by (meson less_FaustusChoId.elims less_FaustusChoId.elims less_FChoiceId_def less_eq_FChoiceId_def linearFChoiceId)
  thus "(x < y) = (x \<le> y \<and> \<not> y \<le> x)" by simp
next
  fix x
  have "x \<le> (x :: FChoiceId)" by (meson linearFChoiceId)
  thus "x \<le> x" by simp
next
  fix x y z
  have "x \<le> y \<Longrightarrow> y \<le> z \<Longrightarrow> x \<le> (z :: FChoiceId)"
    apply (cases x y z rule: FChoiceId.exhaust[case_product FChoiceId.exhaust[case_product FChoiceId.exhaust]], auto simp add: less_eq_FChoiceId_def split: if_split_asm)
    using byteStringOrder lineaBS apply blast
    using byteStringLessEqTwiceEq lineaBS by auto
  thus "x \<le> y \<Longrightarrow> y \<le> z \<Longrightarrow> x \<le> z" by simp
next
  fix x y z
  have "x \<le> y \<Longrightarrow> y \<le> x \<Longrightarrow> x = (y :: FChoiceId)"
    using lineaBS by (cases x y rule: FChoiceId.exhaust[case_product FChoiceId.exhaust], auto simp add: less_eq_FChoiceId_def byteStringLessEqTwiceEq split: if_split_asm)
  thus "x \<le> y \<Longrightarrow> y \<le> x \<Longrightarrow> x = (y :: FChoiceId)" by simp
next
  fix x y
  from linearFChoiceId have "x \<le> y \<or> y \<le> (x :: FChoiceId)" by simp
  thus "x \<le> y \<or> y \<le> x" by simp
qed
end
(* END Proof of linorder for FChoiceId *)


datatype FValue = AvailableMoney FAccountId Token
               | Constant int
               | NegValue FValue
               | AddValue FValue FValue
               | SubValue FValue FValue
               | MulValue FValue FValue
               | DivValue FValue FValue
               | ChoiceValue FChoiceId
               | TimeIntervalStart
               | TimeIntervalEnd
               | UseValue Identifier
               | Cond FObservation FValue FValue
and FObservation = AndObs FObservation FObservation
                     | OrObs FObservation FObservation
                     | NotObs FObservation
                     | ChoseSomething FChoiceId
                     | ValueGE FValue FValue
                     | ValueGT FValue FValue
                     | ValueLT FValue FValue
                     | ValueLE FValue FValue
                     | ValueEQ FValue FValue
                     | UseObservation Identifier
                     | TrueObs
                     | FalseObs

fun names_value :: "FValue \<Rightarrow> Identifier set" and
  names_observation :: "FObservation \<Rightarrow> Identifier set" where
"names_value (AvailableMoney accId token) = names_party accId" |
"names_value (Constant i) = {}" |
"names_value (NegValue v) = names_value v" |
"names_value (AddValue vl vr) = names_value vl \<union> names_value vr" |
"names_value (SubValue vl vr) = names_value vl \<union> names_value vr" |
"names_value (MulValue vl vr) = names_value vl \<union> names_value vr" |
"names_value (DivValue vl vr) = names_value vl \<union> names_value vr" |
"names_value (ChoiceValue cName) = {}" |
"names_value TimeIntervalStart = {}" |
"names_value TimeIntervalEnd = {}" |
"names_value (UseValue vId) = {vId}" |
"names_value (Cond obs vTrue vFalse) = names_observation obs \<union> names_value vTrue \<union> names_value vFalse" |
"names_observation TrueObs = {}" |
"names_observation FalseObs = {}" |
"names_observation (AndObs obsL obsR) = names_observation obsL \<union> names_observation obsR" |
"names_observation (OrObs obsL obsR) = names_observation obsL \<union> names_observation obsR" |
"names_observation (NotObs obs) = names_observation obs" |
"names_observation (ChoseSomething cName) = {}" |
"names_observation (ValueGE vL vR) = names_value vL \<union> names_value vR" |
"names_observation (ValueGT vL vR) = names_value vL \<union> names_value vR" |
"names_observation (ValueLT vL vR) = names_value vL \<union> names_value vR" |
"names_observation (ValueLE vL vR) = names_value vL \<union> names_value vR" |
"names_observation (ValueEQ vL vR) = names_value vL \<union> names_value vR" |
"names_observation (UseObservation obsId) = {obsId}"

datatype FParameter = ValueParameter Identifier
  | ObservationParameter Identifier
  | PubKeyParameter Identifier

fun names_parameter :: "FParameter \<Rightarrow> Identifier set" where
"names_parameter (ValueParameter vId) = {vId}" |
"names_parameter (ObservationParameter obsId) = {obsId}" |
"names_parameter (PubKeyParameter pkId) = {pkId}"

fun names_parameters :: "FParameter list \<Rightarrow> Identifier set" where
"names_parameters [] = {}" |
"names_parameters (p1#ps) = names_parameter p1 \<union> names_parameters ps"

datatype FArgument = ValueArgument FValue
  | ObservationArgument FObservation
  | PubKeyArgument FParty

fun names_argument :: "FArgument \<Rightarrow> Identifier set" where
"names_argument (ValueArgument val) = names_value val" |
"names_argument (ObservationArgument obs) = names_observation obs" |
"names_argument (PubKeyArgument pk) = names_party pk"

fun names_arguments :: "FArgument list \<Rightarrow> Identifier set" where
"names_arguments [] = {}" |
"names_arguments (a1#as) = names_argument a1 \<union> names_arguments as"

datatype FAction = Deposit FParty FParty Token FValue
                | Choice FChoiceId "Bound list"
                | Notify FObservation

fun names_action :: "FAction \<Rightarrow> Identifier set" where
"names_action (Deposit p1 p2 t v) = names_party p1 \<union> names_party p2 \<union> names_value v" |
"names_action (Choice cId bs) = names_choiceId cId" |
"names_action (Notify obs) = names_observation obs"

datatype FContract = Close
             | Pay FAccountId FPayee Token FValue FContract
             | If FObservation FContract FContract
             | When "FCase list" Timeout FContract
             | Let Identifier FValue FContract
             | LetObservation Identifier FObservation FContract
             | LetPubKey Identifier FParty FContract
             | Assert FObservation FContract
             | LetC Identifier "FParameter list" FContract FContract
             | UseC Identifier "FArgument list"
and FCase = Case FAction FContract

fun names_contract :: "FContract \<Rightarrow> Identifier set" and
  names_cases :: "FCase list \<Rightarrow> Identifier set" where
"names_contract Close = {}" |
"names_contract (Pay accId payee token val cont) = names_party accId \<union> names_payee payee \<union> names_value val \<union> names_contract cont" |
"names_contract (If obs trueCont falseCont) = names_observation obs \<union> names_contract trueCont \<union> names_contract falseCont" |
"names_contract (When cases t tCont) = names_cases cases \<union> names_contract tCont" |
"names_contract (Let valId val cont) = {valId} \<union> names_value val \<union> names_contract cont" |
"names_contract (LetObservation obsId obs cont) = {obsId} \<union> names_observation obs \<union> names_contract cont" |
"names_contract (LetPubKey pkId pk cont) = {pkId} \<union> names_party pk \<union> names_contract cont" |
"names_contract (Assert obs cont) = names_observation obs \<union> names_contract cont" |
"names_contract (LetC cId params body cont) = {cId} \<union>  names_parameters params \<union> names_contract body \<union> names_contract cont" |
"names_contract (UseC cId args) = {cId} \<union>  names_arguments args" |
"names_cases [] = {}" |
"names_cases (Case a cont#cases) = names_action a \<union> names_contract cont \<union> names_cases cases"

fun caseToAction :: "FCase \<Rightarrow> FAction" where
"caseToAction (Case act cont) = act"

fun caseToContract :: "FCase \<Rightarrow> FContract" where
"caseToContract (Case act cont) = cont"

lemma caseToContract_Size_LT:
"\<And>cases tCont x.
       x \<in> set cases \<Longrightarrow>
       size (caseToContract x) < size_list size cases"
  apply auto
  by (metis FCase.exhaust FCase.size(2) add.commute add.left_neutral add_Suc caseToContract.simps lessI size_list_estimation)

fun fContractSize :: "FContract \<Rightarrow> nat" and
  fCasesSize :: "FCase list \<Rightarrow> nat" where
"fContractSize Close = 1"|
"fContractSize (Pay accId payee tok val cont) = 1 + fContractSize cont"|
"fContractSize (If obs trueCont falseCont) = 1 + fContractSize trueCont + fContractSize falseCont"|
"fContractSize (When cases t tCont) = 1 + fContractSize tCont + fCasesSize cases"|
"fContractSize (Let vid val cont) = 1 + fContractSize cont"|
"fContractSize (LetObservation obsId obs cont) = 1 + fContractSize cont"|
"fContractSize (Assert obs cont) = 1 + fContractSize cont"|
"fContractSize (LetC cid params boundCon cont) = 1 + size params + fContractSize boundCon + fContractSize cont"|
"fContractSize (UseC cid args) = 1 + size args"|
"fContractSize (LetPubKey pkId pk cont) = 1 + fContractSize cont"|
"fCasesSize Nil = 0"|
"fCasesSize ((Case act cont) # cs) = (fContractSize cont + 1) + fCasesSize cs"

lemma fContractSize_GT0:
"fContractSize con > 0"
  apply (cases "con")
  by auto

datatype AbstractionTypeInformation = ValueType
  | ObservationType
  | PubKeyType
  | ContractType "FParameter list"

(* Stack of type information. Will allow static scoping type checking. *)
type_synonym TypeContext = "(Identifier \<times> AbstractionTypeInformation) list"

fun paramsToTypeContext :: "FParameter list \<Rightarrow> TypeContext" where
"paramsToTypeContext [] = []" |
"paramsToTypeContext (ValueParameter i#rest) = (i, ValueType)#(paramsToTypeContext rest)" |
"paramsToTypeContext (ObservationParameter i#rest) = (i, ObservationType)#(paramsToTypeContext rest)" |
"paramsToTypeContext (PubKeyParameter i#rest) = (i, PubKeyType)#(paramsToTypeContext rest)"

datatype AbstractionInformation = ValueAbstraction Identifier
  | ObservationAbstraction Identifier
  | PubKeyAbstraction Identifier
  | ContractAbstraction "Identifier \<times> FParameter list \<times> FContract"

(* Stack of abstraction information. Will allow static scoping in execution and compilation. *)
type_synonym FContext = "AbstractionInformation list"

fun paramsToFContext :: "FParameter list \<Rightarrow> FContext" where
"paramsToFContext [] = []" |
"paramsToFContext (ValueParameter vid#rest) = (ValueAbstraction vid)#(paramsToFContext rest)" |
"paramsToFContext (ObservationParameter obsId#rest) = (ObservationAbstraction obsId)#(paramsToFContext rest)" |
"paramsToFContext (PubKeyParameter pkId#rest) = (PubKeyAbstraction pkId)#(paramsToFContext rest)"

record FState = accounts :: Accounts
  choices :: "(ChoiceId \<times> ChosenNum) list"
  boundValues :: "(Identifier \<times> int) list"
  boundPubKeys :: "(Identifier \<times> SemanticsTypes.Party) list"
  minTime :: POSIXTime

fun valid_faustus_state :: "FState \<Rightarrow> bool" where
"valid_faustus_state state = (valid_map (accounts state)
                     \<and> valid_map (choices state)
                     \<and> valid_map (boundValues state)
                     \<and> valid_map (boundPubKeys state))"
end