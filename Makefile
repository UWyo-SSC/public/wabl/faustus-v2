.PHONY: bnfc parse

src/Language/Faustus/Syntax/%.hs src/Language/Faustus/Syntax/%.x src/Language/Faustus/Syntax/%.y: src/Faustus.cf
	bnfc -o src -p Language.Faustus.Syntax $^
	alex --ghc src/Language/Faustus/Syntax/LexFaustus.x
	happy --array --info --ghc --coerce src/Language/Faustus/Syntax/ParFaustus.y

bnfc: src/Language/Faustus/Syntax/*.hs src/Language/Faustus/Syntax/*.x src/Language/Faustus/Syntax/*.y

src/Language/Faustus/Syntax/TestFaustus: src/Language/Faustus/Syntax/AbsFaustus.hs src/Language/Faustus/Syntax/LexFaustus.hs src/Language/Faustus/Syntax/ParFaustus.hs src/Language/Faustus/Syntax/PrintFaustus.hs src/Language/Faustus/Syntax/SkelFaustus.hs src/Language/Faustus/Syntax/TestFaustus.hs
	stack ghc src/Language/Faustus/Syntax/AbsFaustus.hs src/Language/Faustus/Syntax/LexFaustus.hs src/Language/Faustus/Syntax/ParFaustus.hs src/Language/Faustus/Syntax/PrintFaustus.hs src/Language/Faustus/Syntax/SkelFaustus.hs src/Language/Faustus/Syntax/TestFaustus.hs

parse: src/Language/Faustus/Syntax/TestFaustus
	@src/Language/Faustus/Syntax/TestFaustus
