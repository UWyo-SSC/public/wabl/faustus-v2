# Faustus

Faustus Version 2 is in heavy development, and at the moment the Isabelle proofs contain the verification of Marlowe. See the [Faustus Version 1 repository](https://gitlab.com/UWyo-SSC/public/wabl/faustus) for the verification and implementation of the previous version of Faustus.

To develop Faustus it is recommended to use Docker and the Dev Containers extensions. With those tools, open the repository folder in the development container to get access to ```stack build``` and ```stack run``` to run the Faustus compiler.

The parser, type checker, compiler, and evaluator can be found in their respective files in the src/Language/Faustus folder [here](https://gitlab.com/UWyo-SSC/public/wabl/faustus-v2/-/tree/master/src/Language/Faustus)
