{-# LANGUAGE DefaultSignatures  #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies  #-}
{-# LANGUAGE GADTs #-}
{-# OPTIONS_GHC -Wno-name-shadowing -Wno-orphans  #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators #-}

module Language.Experimental.Named (Named(..), type (<==), name, The(..)) where

import Data.Coerce
import Unsafe.Coerce

--- * Some type wizardy from Noonan's paper "Ghosts of Departed Proofs."
--   <https://iohk.io/en/research/library/papers/ghosts-of-departed-proofs-functional-pearls>
--  

-- |Noonan's type constructor of Named types where the name is a phantom type.
newtype Named name a = Named a

--  |read `a <== name` as "@ name @ names expressions of type @ a @."
type a <== name = Named name a 
-- Noonan writes : Morally the type of `name` is
--     'a -> (exists name . (a <== name))'. Note that the second argument is 
--     a function that must be polymorphic for *ALL* names.
name :: a -> (forall name . (a <== name) -> t) -> t
name x k = k (coerce x) 


-- |For newtypes with a single phantom type parameter, making them an instance of the 
--    type class @ The @, the function @ the @ strips off the phantom argument from 
--    the type. 
class The d a | d -> a where
    the :: d -> a
    default the :: Coercible d a => d -> a
    the = coerce -- <<< default implementation of @the@.
