
{-# OPTIONS_GHC -Wno-name-shadowing -Wno-orphans  #-}
{-# LANGUAGE TypeFamilyDependencies #-}


module Language.Experimental.Target where

import Data.Proxy
import Data.Coerce


-- | This Type level function computes the final range (the Target) of a type.
type family Target a = b  where
  Target (b -> c) = Target c
  Target a = a


-- ghci> :k! Target Int
-- Target Int :: *
-- = Int
-- ghci> :k! Target (Int -> Int)
-- Target (Int -> Int) :: *
-- = Int
-- ghci> :k! Target (Int -> Int -> Int)
-- Target (Int -> Int -> Int) :: *
-- = Int
-- ghci> :k! Target (Int -> Int -> Bool)
-- Target (Int -> Int -> Bool) :: *
-- = Bool