module Language.Faustus
    ( module Language.Faustus.Syntax
    , module Language.Faustus.Compiler
    , module Language.Faustus.TypeChecker
    , module Language.Faustus.Semantics
    )
where

import           Language.Faustus.Semantics hiding (newLoc)
import           Language.Faustus.Semantics.Types
import           Language.Faustus.Compiler hiding (newLoc)
import           Language.Faustus.Syntax
import           Language.Faustus.TypeChecker
