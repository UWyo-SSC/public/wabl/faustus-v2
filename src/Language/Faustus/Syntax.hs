{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# OPTIONS_GHC -Wno-name-shadowing -Wno-orphans #-}
module Language.Faustus.Syntax where

import qualified Language.Faustus.Semantics.Types as Dsl
import qualified Language.Marlowe.Semantics.Types as MDsl
import qualified Data.ByteString as BS
import qualified Data.Text as T
import qualified Data.List as L
import Data.Map
import Data.Text.Encoding (encodeUtf8)
import Text.ParserCombinators.Parsec
--import qualified Debug.Trace as Dbg

type AbsDotIdentifier = [String]
type AbsIdentifier = String

symbol :: String -> GenParser Char st String
symbol s = do
    spaces
    res <- string s
    spaces
    return res

keyword :: String -> GenParser Char st String
keyword s = do
    spaces
    res <- string s
    space
    spaces
    return res

chaseAbs = Dsl.chase

data AbsContract = DeclCont AbsDecl AbsContract
    | StmtCont AbsStmt AbsContract
    | IfElseCont AbsExpr AbsContract AbsContract
    | WhenCont [AbsCase] AbsExpr AbsContract
    | IdentCont AbsDotIdentifier
    | CallCont AbsDotIdentifier [AbsArg]
    | CloseCont
    deriving (Show,Read,Eq,Ord)

data AbsStmt = PayStmt AbsParty AbsPayee AbsExpr AbsToken
    | AssertStmt AbsExpr
    | AssignStmt AbsDotIdentifier AbsExpr
    | IdentStmt AbsDotIdentifier
    | CallStmt AbsDotIdentifier [AbsArg]
    deriving (Show,Read,Eq,Ord)

data AbsDecl = PartyDecl AbsIdentifier AbsParty
    | TokenDecl AbsIdentifier AbsToken
    | BoundDecl AbsIdentifier AbsBound
    | ChoiceDecl AbsIdentifier AbsChoice
    | VarDecl AbsIdentifier AbsExpr
    | FunDecl AbsIdentifier [AbsParam] AbsExpr
    | ActionDecl AbsIdentifier AbsAction
    | ActionParamDecl AbsIdentifier [AbsParam] AbsAction
    | ContDecl AbsIdentifier AbsContract
    | ContParamDecl AbsIdentifier [AbsParam] AbsContract
    | StmtDecl AbsIdentifier [AbsStmt]
    | StmtParamDecl AbsIdentifier [AbsParam] [AbsStmt]
    | CaseDecl AbsIdentifier AbsCase
    | CaseParamDecl AbsIdentifier [AbsParam] AbsCase
    | ModuleDecl AbsIdentifier Dsl.Context [AbsDecl]
    | ModuleParamDecl AbsIdentifier Dsl.Context [AbsParam] [AbsDecl]
    | ImportDecl AbsDotIdentifier (Maybe AbsIdentifier)
    | ImportArgsDecl AbsDotIdentifier [AbsArg] (Maybe AbsIdentifier)
    | OpenDecl AbsDotIdentifier
    | OpenArgsDecl AbsDotIdentifier [AbsArg]
    deriving (Show,Read,Eq,Ord)

data AbsCase = GuardedContractExpression AbsGuardExpression AbsContract
    | IdentCase AbsDotIdentifier
    | CallCase AbsDotIdentifier [AbsArg]
    deriving (Show,Read,Eq,Ord)

data AbsGuardExpression = ActionGuard AbsAction
    | GuardThenGuard AbsGuardExpression AbsGuardExpression
    | GuardStmtsGuard AbsGuardExpression [AbsStmt]
    | DisjointGuard AbsGuardExpression AbsGuardExpression
    | InterleavedGuard AbsGuardExpression AbsGuardExpression
    deriving (Show,Read,Eq,Ord)

data AbsExpr = TrueExpr
    | FalseExpr
    | IntExpr Integer
    | PosixIntExpr Integer
    | NotExpr AbsExpr
    | NegExpr AbsExpr
    | AndExpr AbsExpr AbsExpr
    | OrExpr AbsExpr AbsExpr
    | CondExpr AbsExpr AbsExpr AbsExpr
    | EqExpr AbsExpr AbsExpr
    | NEqExpr AbsExpr AbsExpr
    | LEExpr AbsExpr AbsExpr
    | LTExpr AbsExpr AbsExpr
    | GEExpr AbsExpr AbsExpr
    | GTExpr AbsExpr AbsExpr
    | AddExpr AbsExpr AbsExpr
    | SubExpr AbsExpr AbsExpr
    | MulExpr AbsExpr AbsExpr
    | DivExpr AbsExpr AbsExpr
    | AvailMoneyExpr AbsParty AbsToken
    | StartExpr
    | EndExpr
    | ChoiceExpr AbsParty AbsChoice
    | ChosenExpr AbsParty AbsChoice
    | IdentExpr AbsDotIdentifier
    | CallExpr AbsDotIdentifier [AbsArg]
    deriving (Show,Read,Eq,Ord)

data AbsAction = DepAct AbsParty AbsExpr AbsToken AbsParty
    | ChoosesAct AbsParty AbsChoice
    | ChoosesNotAct AbsParty AbsChoice
    | ChoosesBoundsAct AbsParty AbsChoice [AbsBound]
    | NotifyAct AbsExpr
    | IdentAct AbsDotIdentifier
    | CallAct AbsDotIdentifier [AbsArg]
    deriving (Show,Read,Eq,Ord)

data AbsBound = IdentBound AbsDotIdentifier
    | DoubleBound AbsExpr AbsExpr
    | SingleBound AbsExpr
    deriving (Show,Read,Eq,Ord)

data AbsParty = PubKeyParty String
    | RoleParty String
    | IdentParty AbsDotIdentifier
    deriving (Show,Read,Eq,Ord)

data AbsPayee = Internal AbsParty
    | External AbsParty
    deriving (Show,Read,Eq,Ord)

data AbsChoice = ConstantChoice String
    | IdentChoice AbsDotIdentifier
    deriving (Show,Read,Eq,Ord)

data AbsToken = ConstantToken String String
    | SingleToken String
    | IdentToken AbsDotIdentifier
    deriving (Show,Read,Eq,Ord)

data AbsParam = Param Dsl.FaustusFullType AbsIdentifier
    deriving (Show,Read,Eq,Ord)

absParamToContext :: AbsParam -> Dsl.Context
absParamToContext (Param ty i) = singleton i ty

absParamsToContext :: [AbsParam] -> Dsl.Context
absParamsToContext ps = Prelude.foldr (\p -> \c -> union (absParamToContext p) c) empty ps

data AbsArg = ContArg AbsContract
    | StmtArg [AbsStmt]
    | ModuleArg [AbsDecl]
    | CaseArg AbsCase
    | ExprArg AbsExpr
    | ActionArg AbsAction
    | BoundArg AbsBound
    | PartyArg AbsParty
    | ChoiceArg AbsChoice
    | TokenArg AbsToken
    | ArrowArg AbsDotIdentifier
    deriving (Show,Read,Eq,Ord)

parseToAbstractSyntax :: String -> Either ParseError AbsContract
parseToAbstractSyntax input = case runParser commentStrip empty "(unknown)" input of
    Left err -> Left err
    Right res -> runParser contractP empty "(unknown)" res


comment = do
    try (do
        string "//"
        manyTill anyChar (try newline)
        return "")
    <|> try (do
        eof
        return "")

commentStrip :: GenParser Char Dsl.Context String
commentStrip = do
    try (do
        eof
        return "")
    <|> try (do
    beforeComment <- manyTill anyChar (try comment)
    afterComment <- commentStrip
    return $ beforeComment ++ "\n" ++ afterComment)


contractP :: GenParser Char Dsl.Context AbsContract
contractP = do
    try (do
        d <- declaration
        symbol ";"
        DeclCont d <$> contractP)
    <|> try (do
        s <- statement
        symbol ";"
        StmtCont s <$> contractP)
    <|> try (do
        symbol "if"
        (eTy, e) <- expr
        symbol "then"
        c1 <- contractP
        symbol "else"
        c2 <- contractP
        case eTy of
            Dsl.TyBool _ -> return $ IfElseCont e c1 c2
            _ -> fail "Expected boolean expression in if statement!")
    <|> try (do
        outerCtx <- getState
        symbol "when"
        symbol "{"
        cs <- cases
        symbol "}"
        symbol "after"
        (eTy, e) <- expr
        symbol "->"
        symbol "{"
        updateState $ const outerCtx
        tCont <- contractP
        symbol "}"
        case eTy of
            Dsl.TyInt Dsl.TyPure -> return $ WhenCont cs e tCont
            _ -> fail "Did not receive pure integer expression!")
    <|> try (do
        (ids, args) <- callTypedMatches Dsl.TyContract
        return $ CallCont ids args)
    <|> try (do
        ids <- dotIdTyped (Dsl.Primitive Dsl.TyContract)
        return $ IdentCont ids)
    <|> try (do
        symbol "close"
        return CloseCont)

caseP :: GenParser Char Dsl.Context AbsCase
caseP = do
    try (do
        ge <- guardExpression
        symbol "->"
        outerCtx <- getState
        symbol "{"
        c <- contractP
        spaces
        symbol "}"
        updateState $ const outerCtx
        return $ GuardedContractExpression ge c)
    <|> try (do
        (ids, args) <- callTypedMatches Dsl.TyCase
        return $ CallCase ids args)
    <|> try (do
        ids <- dotIdTyped (Dsl.Primitive Dsl.TyCase)
        return $ IdentCase ids)

-- Deposit -> Deposit <*> Deposit -> Deposit
guardExpression :: GenParser Char Dsl.Context AbsGuardExpression
guardExpression = do
    try (do
        left <- guardExpression1
        continued <- guardExpression'
        continued left)

guardExpression' :: GenParser Char Dsl.Context (AbsGuardExpression -> GenParser Char Dsl.Context AbsGuardExpression)
guardExpression' = do
    try (do
        symbol "<+>"
        rest <- guardExpression1
        continued <- guardExpression'
        return (\left -> continued (DisjointGuard left rest)))
    <|> return pure

guardExpression1 :: GenParser Char Dsl.Context AbsGuardExpression
guardExpression1 = do
    try (do
        left <- guardExpression2
        continued <- guardExpression1'
        continued left)

guardExpression1' :: GenParser Char Dsl.Context (AbsGuardExpression -> GenParser Char Dsl.Context AbsGuardExpression)
guardExpression1' = do
    try (do
        symbol "<*>"
        rest <- guardExpression2
        continued <- guardExpression1'
        return (\left -> continued (InterleavedGuard left rest)))
    <|> return pure

guardExpression2 :: GenParser Char Dsl.Context AbsGuardExpression
guardExpression2 = do
    try (do
        left <- guardExpression3
        continued <- guardExpression2'
        continued left)

guardExpression2' :: GenParser Char Dsl.Context (AbsGuardExpression -> GenParser Char Dsl.Context AbsGuardExpression)
guardExpression2' = do
    try (do
        symbol "->"
        rest <- guardExpression3
        continued <- guardExpression2'
        return (\left -> continued (GuardThenGuard left rest)))
    <|> try (do
        symbol "->"
        rest <- statements1
        continued <- guardExpression2'
        return (\left -> continued (GuardStmtsGuard left rest)))
    <|> return pure

guardExpression3 :: GenParser Char Dsl.Context AbsGuardExpression
guardExpression3 = do
    try (ActionGuard <$> action)
    <|> try (do
        symbol "("
        ge <- guardExpression
        symbol ")"
        return ge)

cases :: GenParser Char Dsl.Context [AbsCase]
cases = do
    try (do
        spaces
        cs <- try (do
            cs' <- many (do
                try (do
                    c' <- caseP
                    symbol ","
                    return c'))
            c'' <- caseP
            spaces
            return $ cs' ++ [c''])
        return cs)
    <|> try (do
        spaces
        c <- caseP
        spaces
        return [c])
    <|> return []


statement :: GenParser Char Dsl.Context AbsStmt
statement = do
    try (do
        symbol "@"
        p1 <- party
        symbol "pays"
        p2 <- payee
        symbol "("
        spaces
        (eTy, e) <- expr
        spaces
        symbol ","
        t <- tokenP
        spaces
        symbol ")"
        case eTy of
            Dsl.TyInt _ -> return $ PayStmt p1 p2 e t
            _ -> fail "Expected integer expression in pay statement!")
    <|> try (do
        symbol "assert"
        (eTy, e) <- expr
        case eTy of
            Dsl.TyBool _ -> return $ AssertStmt e
            _ -> fail "Expected boolean expression in assert statement!")
    <|> try (do
        x <- dotIdentifier
        s <- getState
        case chaseAbs x s of
            Nothing -> fail "Identifier assigned without being declared!"
            Just (Dsl.Primitive ty) -> do
                symbol ":="
                (eTy, e) <- expr
                if eTy == ty then return $ AssignStmt x e else fail "Incorrect type of expression in assignment!"
            _ -> fail "Identifier does not have an assignable type!")
    <|> try (do
        (ids, args) <- callTypedMatches (Dsl.TyStatement)
        return $ CallStmt ids args)
    <|> try (do
        ids <- dotIdTyped (Dsl.Primitive Dsl.TyStatement)
        return $ IdentStmt ids)

-- | Parses semicolon separated statements. Requires at least one statement.
statements1 :: GenParser Char Dsl.Context [AbsStmt]
statements1 = do
    try (do
        stmt <- statement
        symbol ";"
        stmts <- statements1
        return (stmt:stmts)
        )
    <|> try (do
        stmt <- statement
        return [stmt])

declaration :: GenParser Char Dsl.Context AbsDecl
declaration = do
    try (do
        outerCtx <- getState
        symbol "party"
        i <- identifier
        symbol "="
        p <- party
        updateState (\s -> outerCtx)
        updateState (\s -> insert i (Dsl.Primitive Dsl.TyParty) s)
        return $ PartyDecl i p)
    <|> try (do
        outerCtx <- getState
        symbol "token"
        i <- identifier
        symbol "="
        t <- tokenP
        updateState (\s -> outerCtx)
        updateState (\s -> insert i (Dsl.Primitive Dsl.TyToken) s)
        return $ TokenDecl i t)
    <|> try (do
        outerCtx <- getState
        symbol "bound"
        i <- identifier
        symbol "="
        b <- bound
        updateState (\s -> outerCtx)
        updateState (\s -> insert i (Dsl.Primitive Dsl.TyBound) s)
        return $ BoundDecl i b)
    <|> try (do
        outerCtx <- getState
        symbol "choice"
        i <- identifier
        symbol "="
        c <- choiceP
        updateState (\s -> outerCtx)
        updateState (\s -> insert i (Dsl.Primitive Dsl.TyChoice) s)
        return $ ChoiceDecl i c)
    <|> try (do
        outerCtx <- getState
        symbol "var"
        i <- identifier
        symbol "="
        (eTy, e) <- expr
        updateState (\s -> outerCtx)
        updateState (\s -> insert i (Dsl.Primitive eTy) s)
        return $ VarDecl i e)
    <|> try (do
        outerCtx <- getState
        symbol "fun"
        i <- identifier
        symbol "("
        ps <- params
        symbol ")"
        symbol "{"
        (eTy, e) <- expr
        symbol "}"
        let paramTys = (\(Param t pi) -> t) <$> ps
        updateState (\s -> outerCtx)
        updateState (\s -> insert i (Dsl.Arrow (Dsl.TyFun paramTys (eTy))) s)
        return $ FunDecl i ps e)
    <|> try (do
        outerCtx <- getState
        symbol "action"
        i <- identifier
        symbol "="
        symbol "{"
        a <- action
        symbol "}"
        updateState (\s -> outerCtx)
        updateState (\s -> insert i (Dsl.Primitive Dsl.TyAction) s)
        return $ ActionDecl i a)
    <|> try (do
        outerCtx <- getState
        symbol "action"
        i <- identifier
        symbol "("
        ps <- params
        symbol ")"
        symbol "{"
        a <- action
        symbol "}"
        let paramTys = (\(Param t pi) -> t) <$> ps
        updateState (\s -> outerCtx) -- Reset context outside of function.
        updateState (\s -> insert i (Dsl.Arrow (Dsl.TyFun paramTys Dsl.TyAction)) s)
        return $ ActionParamDecl i ps a)
    <|> try (do
        outerCtx <- getState
        symbol "contract"
        i <- identifier
        symbol "="
        symbol "{"
        c <- contractP
        symbol "}"
        updateState (\s -> outerCtx)
        updateState (\s -> insert i (Dsl.Primitive Dsl.TyContract) s)
        return $ ContDecl i c)
    <|> try (do
        symbol "contract"
        outerCtx <- getState
        i <- identifier
        symbol "("
        ps <- params
        symbol ")"
        symbol "{"
        c <- contractP
        symbol "}"
        let paramTys = (\(Param t pi) -> t) <$> ps
        updateState (\s -> outerCtx)
        updateState (\s -> insert i (Dsl.Arrow (Dsl.TyFun paramTys Dsl.TyContract)) s)
        return $ ContParamDecl i ps c)
    <|> try (do
        outerCtx <- getState
        symbol "statement"
        i <- identifier
        symbol "="
        symbol "{"
        stmts <- statements1
        symbol "}"
        updateState (\s -> outerCtx)
        updateState (\s -> insert i (Dsl.Primitive Dsl.TyStatement) s)
        return $ StmtDecl i stmts)
    <|> try (do
        outerCtx <- getState
        symbol "statement"
        i <- identifier
        symbol "("
        ps <- params
        symbol ")"
        symbol "{"
        stmts <- statements1
        symbol "}"
        let paramTys = (\(Param t pi) -> t) <$> ps
        updateState (\s -> outerCtx)
        updateState (\s -> insert i (Dsl.Arrow (Dsl.TyFun paramTys Dsl.TyStatement)) s)
        return $ StmtParamDecl i ps stmts)
    <|> try (do
        outerCtx <- getState
        symbol "case"
        i <- identifier
        symbol "="
        symbol "{"
        c <- caseP
        symbol "}"
        updateState (\s -> outerCtx)
        updateState (\s -> insert i (Dsl.Primitive Dsl.TyCase) s)
        return $ CaseDecl i c)
    <|> try (do
        outerCtx <- getState
        symbol "case"
        i <- identifier
        symbol "("
        ps <- params
        symbol ")"
        symbol "{"
        c <- caseP
        symbol "}"
        let paramTys = (\(Param t pi) -> t) <$> ps
        updateState (\s -> outerCtx)
        updateState (\s -> insert i (Dsl.Arrow (Dsl.TyFun paramTys Dsl.TyCase)) s)
        return $ CaseParamDecl i ps c)
    <|> try (do
        outerCtx <- getState
        symbol "module"
        i <- identifier
        symbol "(|"
        declaredCtx <- absParamsToContext <$> params
        symbol "|)"
        symbol "="
        symbol "{"
        d <- declarations1
        symbol "}"
        innerCtx <- getState
        updateState (\s -> outerCtx)
        updateState (\s -> insert i (Dsl.Primitive (Dsl.TyModule innerCtx)) empty)
        return $ ModuleDecl i declaredCtx d)
    <|> try (do
        outerCtx <- getState
        symbol "module"
        i <- identifier
        symbol "(|"
        declaredCtx <- absParamsToContext <$> params
        symbol "|)"
        symbol "("
        ps <- params
        symbol ")"
        symbol "{"
        ds <- declarations1
        symbol "}"
        innerCtx <- getState
        let paramTys = (\(Param t pi) -> t) <$> ps
        updateState (\s -> outerCtx)
        updateState (\s -> insert i (Dsl.Arrow (Dsl.TyFun paramTys (Dsl.TyModule innerCtx))) s)
        return $ ModuleParamDecl i declaredCtx ps ds)
    <|> try (do
        outerCtx <- getState
        symbol "open"
        ids <- lookAhead (dotIdentifier) -- Look ahead to get inner context
        let (Just (Dsl.Arrow (Dsl.TyFun pTys (Dsl.TyModule innerCtx)))) = chaseAbs ids outerCtx
        (dotId, args) <- callTypedMatches $ Dsl.TyModule innerCtx
        updateState (\s -> innerCtx `union` s)
        return $ OpenArgsDecl dotId args)
    <|> try (do
        outerCtx <- getState
        symbol "open"
        ids <- lookAhead (dotIdentifier) -- Look ahead to get inner context
        let (Just (Dsl.Arrow (Dsl.TyFun pTys (Dsl.TyModule innerCtx)))) = chaseAbs ids outerCtx
        dotId <- dotIdTyped $ Dsl.Primitive (Dsl.TyModule innerCtx)
        updateState (\s -> innerCtx `union` s)
        return $ OpenDecl dotId)

declarations1 :: GenParser Char Dsl.Context [AbsDecl]
declarations1 = do
    try (do
        decl <- declaration
        symbol ";"
        decls <- declarations1
        return (decl:decls)
        )
    <|> try (do
        decl <- declaration
        return [decl])

identifier :: GenParser Char st AbsIdentifier
identifier = do
    startChar <- lower
    rest <- many (alphaNum <|> char '_')
    return (startChar:rest)

dotIdentifier :: GenParser Char st AbsDotIdentifier
dotIdentifier = do
    firstId <- identifier
    try (do
        char '.'
        restId <- dotIdentifier
        return (firstId:restId)) <|> return [firstId]

dotIdTyped :: Dsl.FaustusFullType -> GenParser Char Dsl.Context AbsDotIdentifier
dotIdTyped t = do
    try (do
        ids <- dotIdentifier
        c <- getState
        case chaseAbs ids c of
            Just t' -> if t == t' then return ids else fail $ (L.intercalate "." ids) ++ " has the incorrect type!"
            Nothing -> fail $ (L.intercalate "." ids) ++ " has not been declared!")

arg :: Dsl.FaustusFullType -> GenParser Char Dsl.Context AbsArg
arg ty = case ty of
    Dsl.Arrow arrowTy -> try (do
        spaces
        ids <- dotIdTyped ty
        spaces
        return $ ArrowArg ids)
    Dsl.Primitive primTy -> do
        spaces
        res <- do
            case primTy of
                Dsl.TyParty -> do
                    p <- party
                    return $ PartyArg p
                Dsl.TyToken -> do
                    t <- tokenP
                    return $ TokenArg t
                Dsl.TyContract -> do
                    c <- contractP
                    return $ ContArg c
                Dsl.TyCase -> do
                    c <- caseP
                    return $ CaseArg c
                Dsl.TyChoice -> do
                    c <- choiceP
                    return $ ChoiceArg c
                Dsl.TyBound -> do
                    b <- bound
                    return $ BoundArg b
                Dsl.TyAction -> do
                    a <- action
                    return $ ActionArg a
                Dsl.TyBool _ -> do
                    (eTy, e) <- expr
                    case eTy of
                        Dsl.TyBool _ -> return $ ExprArg e
                        _ -> fail "Incorrect expression type passed as an argument. Expected boolean!"
                Dsl.TyInt p -> do
                    (eTy, e) <- expr
                    case eTy of
                        Dsl.TyInt _ -> return $ ExprArg e
                        _ -> fail "Incorrect expression type passed as an argument. Expected integer!"
                Dsl.TyPosix p -> do
                    (eTy, e) <- expr
                    case eTy of
                        Dsl.TyPosix _ -> return $ ExprArg e
                        _ -> fail "Incorrect expression type passed as an argument. Expected posix!"
                Dsl.TyModule _ -> do
                    d <- declarations1
                    return $ ModuleArg d
                Dsl.TyStatement -> do
                    d <- statements1
                    return $ StmtArg d
        return res

callTypedMatches :: Dsl.FaustusPrimType -> GenParser Char Dsl.Context (AbsDotIdentifier, [AbsArg])
callTypedMatches resTy = do
    try (do
        ids <- dotIdentifier
        c <- getState
        case chaseAbs ids c of
            Just (Dsl.Arrow (Dsl.TyFun pTys resTy')) -> if (resTy' /= resTy)
                then fail $ (L.intercalate "." ids) ++ " has the incorrect type!"
                else (do
                    spaces
                    char '('
                    spaces
                    let argParsers = arg <$> pTys
                        argParsers' = (if length pTys > 0 then ((head argParsers):(leadingCommas <$> tail argParsers))
                            else argParsers)
                    args <- mapM id argParsers'
                    spaces
                    char ')'
                    return (ids, args))
            _ -> fail $ (L.intercalate "." ids) ++ " has the incorrect type!")
    where
        leadingCommas p = (try (do
                            spaces
                            char ','
                            spaces
                            res <- p
                            spaces
                            return res))

callTyped :: GenParser Char Dsl.Context (Dsl.FaustusPrimType, AbsDotIdentifier, [AbsArg])
callTyped = do
    try (do
        ids <- dotIdentifier
        c <- getState
        case chaseAbs ids c of
            Just (Dsl.Arrow (Dsl.TyFun pTys resTy')) -> (do
                    spaces
                    char '('
                    spaces
                    let argParsers = arg <$> pTys
                        argParsers' = (if length pTys > 0 then ((head argParsers):(leadingCommas <$> tail argParsers))
                            else argParsers)
                    args <- mapM id argParsers'
                    spaces
                    char ')'
                    return (resTy', ids, args))
            _ -> fail $ (L.intercalate "." ids) ++ " has the incorrect type!")
    where
        leadingCommas p = (try (do
                            spaces
                            char ','
                            spaces
                            res <- p
                            spaces
                            return res))

faustusChar :: GenParser Char st [Char]
faustusChar = (many $ noneOf "\".\'")

-- | Returns what is inside a string.
faustusString :: GenParser Char st String
faustusString = do
    insideString <- between (char '\"') (char '\"') faustusChar -- TODO make reasonable set of characters
    return insideString

party :: GenParser Char Dsl.Context AbsParty
party = do
    try $ (do
        ids <- dotIdTyped (Dsl.Primitive Dsl.TyParty)
        return $ IdentParty ids)
    <|> try (do
        insideString <- faustusString
        return $ RoleParty insideString)
    <|> try (do
        insideString <- between (char '\'') (char '\'') (many $ noneOf "'.")
        return $ PubKeyParty insideString)

payee :: GenParser Char Dsl.Context AbsPayee
payee = do
    try (do
        v <- choice [char '!', char '@']
        p <- party
        return $ if v == '!' then External p else Internal p)

tokenP :: GenParser Char Dsl.Context AbsToken
tokenP = do
    try $ (do
        ids <- dotIdTyped (Dsl.Primitive Dsl.TyToken)
        return $ IdentToken ids)
    <|> try (do
        insideString1 <- faustusString
        spaces
        insideString2 <- faustusString
        return $ ConstantToken insideString1 insideString2)
    <|> (do
        insideString1 <- faustusString
        return $ SingleToken insideString1)

choiceP :: GenParser Char Dsl.Context AbsChoice
choiceP = do
    try (do
        ids <- dotIdTyped (Dsl.Primitive Dsl.TyChoice)
        return $ IdentChoice ids)
    <|> try (do
        spaces
        insideString <- faustusString
        return $ ConstantChoice insideString)

bound :: GenParser Char Dsl.Context AbsBound
bound = do
    try $ (do
        ids <- dotIdTyped (Dsl.Primitive Dsl.TyBound)
        return $ IdentBound ids)
    <|> try (do
        char '['
        spaces
        (eTy, e) <- expr
        spaces
        char ']'
        case eTy of
            Dsl.TyInt Dsl.TyPure -> return $ SingleBound e
            _ -> fail "Expression in bound is a not pure integer!")
    <|> try (do
        char '['
        spaces
        (eTy1, e1) <- expr
        spaces
        char ','
        spaces
        (eTy2, e2) <- expr
        spaces
        char ']'
        case (eTy1, eTy2) of
            (Dsl.TyInt Dsl.TyPure, Dsl.TyInt Dsl.TyPure) -> return $ DoubleBound e1 e2
            _ -> fail "Expressions in bound are not pure integers!")

coercePurity :: Dsl.FaustusPrimType -> Dsl.FaustusPrimType -> GenParser c s Dsl.FaustusPurityType
coercePurity ty1 ty2 = do
    p1 <- getTyPurity ty1
    p2 <- getTyPurity ty2
    return (max p1 p2)
    where
        getTyPurity t = do
            case t of
                Dsl.TyBool p -> return p
                Dsl.TyInt p -> return p
                Dsl.TyPosix p -> return p
                _ -> fail $ "Invalid type: " ++ (show t)

coercePurity3 :: Dsl.FaustusPrimType -> Dsl.FaustusPrimType -> Dsl.FaustusPrimType -> GenParser c s Dsl.FaustusPurityType
coercePurity3 ty1 ty2 ty3 = do
    p1 <- coercePurity ty1 ty2
    p2 <- coercePurity ty2 ty3
    return (max p1 p2)

updatePurity :: Dsl.FaustusPrimType -> Dsl.FaustusPurityType -> Dsl.FaustusPrimType
updatePurity ty p = case ty of
    Dsl.TyBool _ -> Dsl.TyBool p
    Dsl.TyInt _ -> Dsl.TyInt p
    Dsl.TyPosix _ -> Dsl.TyPosix p
    _ -> ty

checkTy :: Dsl.FaustusPrimType -> Dsl.FaustusPrimType -> GenParser c s ()
checkTy tyE ty = do
    case (tyE, ty) of
        (Dsl.TyBool p1, Dsl.TyBool p2) -> return ()
        (Dsl.TyInt p1, Dsl.TyInt p2) -> return ()
        (Dsl.TyPosix p1, Dsl.TyPosix p2) -> return ()
        _ -> if tyE == ty then return () else fail "Unexpected type!"

expr :: GenParser Char Dsl.Context (Dsl.FaustusPrimType, AbsExpr)
expr = do
    try (do
        (tyE1, e1) <- expr1
        spaces
        char '?'
        spaces
        (tyE2, e2) <- expr
        spaces
        char ':'
        spaces
        (tyE3, e3) <- expr
        p <- coercePurity3 tyE1 tyE2 tyE3
        checkTy tyE1 (Dsl.TyBool p)
        checkTy tyE2 tyE3
        return $ (Dsl.TyBool p, CondExpr e1 e2 e3))
    <|> try expr1

infixSymbolParser :: (String, (AbsExpr -> AbsExpr -> AbsExpr)) -> GenParser Char Dsl.Context (String, (AbsExpr -> AbsExpr -> AbsExpr))
infixSymbolParser (s, builder) = do
    res <- symbol s
    return (res, builder)

innerInfixParser :: [(String, (AbsExpr -> AbsExpr -> AbsExpr))] -> GenParser Char Dsl.Context (Dsl.FaustusPrimType, AbsExpr) -> Dsl.FaustusPrimType -> Dsl.FaustusPrimType -> Dsl.FaustusPrimType -> GenParser Char Dsl.Context ((Dsl.FaustusPrimType, AbsExpr) -> GenParser Char Dsl.Context (Dsl.FaustusPrimType, AbsExpr))
innerInfixParser symbols nextHigherPrecedenceParser leftExpectedType rightExpectedType resultType = try (do
    (symb, builder) <- choice (try . infixSymbolParser <$> symbols)
    (tyE2, e2) <- nextHigherPrecedenceParser
    e'' <- innerInfixParser symbols nextHigherPrecedenceParser leftExpectedType rightExpectedType resultType
    return (\(leftETy, leftExp) -> do
        p <- coercePurity leftETy tyE2
        let resultType' = updatePurity resultType p
            leftExpectedType' = updatePurity leftExpectedType p
            rightExpectedType' = updatePurity rightExpectedType p
        checkTy leftETy leftExpectedType'
        checkTy tyE2 rightExpectedType'
        e'' (resultType', builder leftExp e2)))
    <|> return (return)

expr1 :: GenParser Char Dsl.Context (Dsl.FaustusPrimType, AbsExpr)
expr1 = do
    try (do
        (tyE1, e1) <- expr2
        spaces
        e' <- innerInfixParser [("||", OrExpr)] expr2 (Dsl.TyBool Dsl.TyImpure) (Dsl.TyBool Dsl.TyImpure) (Dsl.TyBool Dsl.TyImpure)
        e' (tyE1, e1))
    <|> expr2

expr2 :: GenParser Char Dsl.Context (Dsl.FaustusPrimType, AbsExpr)
expr2 = do
    try (do
        e1 <- expr3
        spaces
        e' <- innerInfixParser [("&&", AndExpr)] expr3 (Dsl.TyBool Dsl.TyImpure) (Dsl.TyBool Dsl.TyImpure) (Dsl.TyBool Dsl.TyImpure)
        e' e1)
    <|> expr3

expr3 :: GenParser Char Dsl.Context (Dsl.FaustusPrimType, AbsExpr)
expr3 = do
    try (do
        e1 <- expr4
        spaces
        e' <- innerInfixParser [("==", EqExpr), ("!=", NEqExpr)] expr4 (Dsl.TyInt Dsl.TyImpure) (Dsl.TyInt Dsl.TyImpure) (Dsl.TyBool Dsl.TyImpure)
        e' e1)
    <|> expr4

expr4 :: GenParser Char Dsl.Context (Dsl.FaustusPrimType, AbsExpr)
expr4 = do
    try (do
        e1 <- expr5
        spaces
        e' <- innerInfixParser [("<=", LEExpr), ("<", LTExpr), (">=", GEExpr), (">", GTExpr)] expr5 (Dsl.TyInt Dsl.TyImpure) (Dsl.TyInt Dsl.TyImpure) (Dsl.TyBool Dsl.TyImpure)
        e' e1)
    <|> expr5

expr5 :: GenParser Char Dsl.Context (Dsl.FaustusPrimType, AbsExpr)
expr5 = do
    try (do
        e1 <- expr6
        spaces
        e' <- innerInfixParser [("+", AddExpr), ("-", SubExpr)] expr6 (Dsl.TyInt Dsl.TyImpure) (Dsl.TyInt Dsl.TyImpure) (Dsl.TyInt Dsl.TyImpure)
        e' e1)
    <|> expr6

expr6 :: GenParser Char Dsl.Context (Dsl.FaustusPrimType, AbsExpr)
expr6 = do
    try (do
        e1 <- expr7
        spaces
        e' <- innerInfixParser [("*", MulExpr), ("/", DivExpr)] expr7 (Dsl.TyInt Dsl.TyImpure) (Dsl.TyInt Dsl.TyImpure) (Dsl.TyInt Dsl.TyImpure)
        e' e1)
    <|> expr7

expr7 :: GenParser Char Dsl.Context (Dsl.FaustusPrimType, AbsExpr)
expr7 = do
    try (do
        char '-'
        (tyE1, e1) <- expr8
        checkTy tyE1 (Dsl.TyInt Dsl.TyImpure)
        return $ (tyE1, NegExpr e1))
    <|> try (do
        char '!'
        (tyE1, e1) <- expr8
        checkTy tyE1 (Dsl.TyBool Dsl.TyImpure)
        return $ (tyE1, NotExpr e1))
    <|> expr8

expr8 :: GenParser Char Dsl.Context (Dsl.FaustusPrimType, AbsExpr)
expr8 = do
    try (do
        p <- party
        spaces
        char '$'
        spaces
        t <- tokenP
        return $ (Dsl.TyInt Dsl.TyImpure, AvailMoneyExpr p t))
    <|> expr9

expr9 :: GenParser Char Dsl.Context (Dsl.FaustusPrimType, AbsExpr)
expr9 = do
    try (do
        symbol "("
        p <- party
        symbol ","
        cho <- choiceP
        spaces
        symbol ")"
        q <- choice [char '#', char '?']
        return $ if q == '#' then (Dsl.TyInt Dsl.TyImpure, ChoiceExpr p cho) else (Dsl.TyBool Dsl.TyImpure, ChosenExpr p cho))
    <|> try (do
        (resTy, ids, args) <- callTyped
        return $ (resTy, CallExpr ids args))
    <|> try (do
        ids <- dotIdentifier
        c <- getState
        case chaseAbs ids c of
            Just (Dsl.Primitive (Dsl.TyInt p)) -> return $ (Dsl.TyInt p, IdentExpr ids)
            Just (Dsl.Primitive (Dsl.TyBool p)) -> return $ (Dsl.TyBool p, IdentExpr ids)
            Just (Dsl.Primitive (Dsl.TyPosix p)) -> return $ (Dsl.TyPosix p, IdentExpr ids)
            _ -> fail $ (L.intercalate "." ids) ++ " is not an integer, boolean, or posix value!")
    <|> try (do
        ds <- many1 digit
        return $ (Dsl.TyInt Dsl.TyPure, IntExpr (read ds)))
    <|> try (do
        symbol "true"
        return (Dsl.TyBool Dsl.TyPure, TrueExpr))
    <|> try (do
        symbol "false"
        return (Dsl.TyBool Dsl.TyPure, FalseExpr))
    <|> try (do
        symbol "start"
        return (Dsl.TyInt Dsl.TyImpure, StartExpr))
    <|> try (do
        symbol "end"
        return (Dsl.TyInt Dsl.TyImpure, EndExpr))
    <|> try (do
        symbol "("
        e <- expr
        symbol ")"
        return e)

action :: GenParser Char Dsl.Context AbsAction
action = do
    try (do
        p1 <- party
        symbol "deposits"
        (eTy, v) <- expr
        spaces
        t <- tokenP
        symbol "into"
        symbol "@"
        p2 <- party
        case eTy of
            Dsl.TyInt _ -> return $ DepAct p1 v t p2
            _ -> fail "Action does not have expression of type integer!")
    <|> try (do
        p <- party
        symbol "chooses"
        ch <- choiceP
        symbol "within"
        bs <- many1 bound
        return $ ChoosesBoundsAct p ch bs)
    <|> try (do
        p <- party
        symbol "chooses"
        ch <- choiceP
        return $ ChoosesAct p ch)
    <|> try (do
        p <- party
        symbol "chooses"
        symbol "not"
        ch <- choiceP
        return $ ChoosesNotAct p ch)
    <|> try (do
        symbol "notify"
        (eTy, e) <- expr
        case eTy of
            Dsl.TyBool _ -> return $ NotifyAct e
            _ -> fail "Notify does not have boolean expression!")
    <|> try (do
        (ids, args) <- callTypedMatches (Dsl.TyAction)
        return $ CallAct ids args)
    <|> try (do
        ids <- dotIdTyped (Dsl.Primitive Dsl.TyAction)
        return $ IdentAct ids)

primitiveType :: GenParser Char Dsl.Context Dsl.FaustusPrimType
primitiveType = do
    try (do
        symbol "contract"
        return Dsl.TyContract)
    <|> try (do
        symbol "case"
        return Dsl.TyCase)
    <|> try (do
        symbol "action"
        return Dsl.TyAction)
    <|> try (do
        symbol "choice"
        return Dsl.TyChoice)
    <|> try (do
        symbol "bool"
        return $ Dsl.TyBool Dsl.TyImpure)
    <|> try (do
        symbol "int"
        return $ Dsl.TyInt Dsl.TyImpure)
    <|> try (do
        symbol "posix"
        return $ Dsl.TyPosix Dsl.TyImpure)
    <|> try (do
        symbol "module"
        symbol "(|"
        ctx <- absParamsToContext <$> params
        symbol "|)"
        return $ Dsl.TyModule ctx)
    <|> try (do
        symbol "party"
        return Dsl.TyParty)
    <|> try (do
        symbol "token"
        return Dsl.TyToken)
    <|> try (do
        symbol "bound"
        return Dsl.TyBound)

arrowType :: GenParser Char Dsl.Context Dsl.FaustusArrowType
arrowType = do
    char '('
    spaces
    primTys <- fullType `sepBy` (do
        spaces
        char ','
        spaces)
    char ')'
    symbol "->"
    resTy <- primitiveType
    return $ Dsl.TyFun primTys resTy

fullType :: GenParser Char Dsl.Context Dsl.FaustusFullType
fullType = do
    try (do
        ty <- arrowType
        return $ Dsl.Arrow ty)
    <|> try (do
        ty <- primitiveType
        return $ Dsl.Primitive ty)

-- >>> Text.ParserCombinators.Parsec.runParser param Data.Map.empty "(unknown)" "module (|int y|) x"
-- Right (Param (Primitive (TyModule (fromList [("y",Primitive (TyInt TyImpure))]))) "x")
--
-- >>> Text.ParserCombinators.Parsec.runParser param Data.Map.empty "(unknown)" "(int) -> module (|int y|) x"
-- Right (Param (Arrow (TyFun [Primitive (TyInt TyImpure)] (TyModule (fromList [("y",Primitive (TyInt TyImpure))])))) "x")
--
-- >>> Text.ParserCombinators.Parsec.runParser param Data.Map.empty "(unknown)" "(module (|int x|),int) -> module (|int y|) x "
-- Right (Param (Arrow (TyFun [Primitive (TyModule (fromList [("x",Primitive (TyInt TyImpure))])),Primitive (TyInt TyImpure)] (TyModule (fromList [("y",Primitive (TyInt TyImpure))])))) "x")
--
param :: GenParser Char Dsl.Context AbsParam
param = do
    ty <- fullType
    spaces
    i <- identifier
    return $ Param ty i

params :: GenParser Char Dsl.Context [AbsParam]
params = do
    try (do
        spaces
        p1 <- param
        ps <- many (do
            symbol ","
            p <- param
            return p)
        updateState (\s -> Prelude.foldr (\(Param ty i) -> \acc -> insert i ty acc) s (p1:ps))
        return $ p1:ps)
    <|> try (do
        spaces
        return $ [])

-- >>> Text.ParserCombinators.Parsec.runParser party Data.Map.empty "(unknown)" "someId"
-- Left "(unknown)" (line 1, column 7):
-- unexpected end of input
-- expecting letter or digit, "_" or "."
-- someId has not been declared!
--
-- >>> Text.ParserCombinators.Parsec.runParser Language.Faustus.Syntax.party Data.Map.empty "(unknown)" "someId"
-- Left "(unknown)" (line 1, column 7):
-- unexpected end of input
-- expecting letter or digit, "_" or "."
-- someId has not been declared!
--
-- >>> Text.ParserCombinators.Parsec.runParser Language.Faustus.Syntax.party (Data.Map.fromList [(Dsl.Identifier . Data.Text.pack $ "someId", Dsl.Primitive Dsl.TyParty)]) "(unknown)" "someId "
-- Right (IdentParty ["someId"])
--
-- >>> Text.ParserCombinators.Parsec.runParser Language.Faustus.Syntax.party Data.Map.empty "(unknown)" "\"a role\""
-- Right (RoleParty "a role")
--
-- >>> Text.ParserCombinators.Parsec.runParser Language.Faustus.Syntax.party Data.Map.empty "(unknown)" "'a pubkey'"
-- Right (PubKeyParty "a pubkey")
--
-- >>> Text.ParserCombinators.Parsec.runParser Language.Faustus.Syntax.tokenP (Data.Map.fromList [(Dsl.Identifier . Data.Text.pack $ "someId", Dsl.Primitive Dsl.TyToken)]) "(unknown)" "someId"
-- Right (IdentToken ["someId"])
--
-- >>> Text.ParserCombinators.Parsec.runParser Language.Faustus.Syntax.payee (Data.Map.fromList [(Dsl.Identifier . Data.Text.pack $ "someId", Dsl.Primitive Dsl.TyToken)]) "(unknown)" "someId"
-- Left "(unknown)" (line 1, column 1):
-- unexpected "s"
-- expecting "!" or "@"
--
-- >>> Text.ParserCombinators.Parsec.runParser Language.Faustus.Syntax.payee (Data.Map.fromList [(Dsl.Identifier . Data.Text.pack $ "someId", Primitive TyParty)]) "(unknown)" "@someId"
-- Right (Internal (IdentParty ["someId"]))
--
-- >>> Text.ParserCombinators.Parsec.runParser Language.Faustus.Syntax.payee (Data.Map.fromList [(Dsl.Identifier . Data.Text.pack $ "someId", Primitive TyParty)]) "(unknown)" "!someId"
-- Right (External (IdentParty ["someId"]))
--
-- >>>Text.ParserCombinators.Parsec.runParser Language.Faustus.Syntax.expr (Data.Map.fromList [(Language.Faustus.Semantics.Types.Identifier . Data.Text.pack $ "someId", Language.Faustus.Semantics.Types.Primitive Language.Faustus.Semantics.Types.TyParty)]) "(unknown)" "(2+2)*2+2"
-- Right (AddExpr (MulExpr (AddExpr (IntExpr 2) (IntExpr 2)) (IntExpr 2)) (IntExpr 2))
--
-- >>>Text.ParserCombinators.Parsec.runParser Language.Faustus.Syntax.expr (Data.Map.fromList [(Language.Faustus.Semantics.Types.Identifier . Data.Text.pack $ "someChoice", Language.Faustus.Semantics.Types.Primitive Language.Faustus.Semantics.Types.TyChoice)]) "(unknown)" "2 + someId? 1 : 0"
-- Right (IntExpr 2)
--
-- TODO THIS ONE WILL FAIL FOR SOME REASON
-- >>>Text.ParserCombinators.Parsec.runParser (Language.Faustus.Syntax.callTypedMatches Dsl.TyAction) (Data.Map.fromList [(Dsl.Identifier . Data.Text.pack $ "someFun", Dsl.Arrow (Dsl.TyFun [Dsl.Primitive  Dsl.TyBound] Dsl.TyAction))]) "someFun([2])"
-- <interactive>:54920:1-243: error:
--     * No instance for (Show
--                          ([Char] -> Either ParseError (AbsDotIdentifier, [AbsArg])))
--         arising from a use of `print'
--         (maybe you haven't applied a function to enough arguments?)
--     * In a stmt of an interactive GHCi command: print it
--
-- >>>Text.ParserCombinators.Parsec.runParser Language.Faustus.Syntax.action (Data.Map.fromList [(Dsl.Identifier . Data.Text.pack $ "someId", Dsl.Arrow (Dsl.TyFun [Dsl.Primitive Dsl.TyAction, Dsl.Primitive Dsl.TyAction] Dsl.TyAction))]) "(unknown)" "someId(notify 2, notify 3)"
-- Left "(unknown)" (line 1, column 16):
-- unexpected ","
-- expecting digit, white space, "*", "/", "+", "-", "<=", "<", ">=", ">", "==", "!=", "&&" or "||"
-- Notify does not have boolean expression!
--
-- >>>Text.ParserCombinators.Parsec.runParser expr (Data.Map.fromList [(Dsl.Identifier . Data.Text.pack $ "someId", Dsl.Arrow (Dsl.TyFun [Dsl.Primitive Dsl.TyAction, Dsl.Primitive Dsl.TyAction] (Dsl.TyInt Dsl.TyImpure)))]) "(unknown)" "2+someId(notify true, notify false)"
-- Right (TyInt TyImpure,AddExpr (IntExpr 2) (CallExpr ["someId"] [ActionArg (NotifyAct TrueExpr),ActionArg (NotifyAct FalseExpr)]))
--
-- >>>Text.ParserCombinators.Parsec.runParser contractP (Data.Map.fromList []) "(unknown)" "close"
-- Right CloseCont
--
-- >>>Text.ParserCombinators.Parsec.runParser contractP (Data.Map.fromList []) "(unknown)" "if true then close else close"
-- Right (IfElseCont TrueExpr CloseCont CloseCont)
--
-- >>>Text.ParserCombinators.Parsec.runParser contractP (Data.Map.fromList []) "(unknown)" "@\"alice\" pays @'bob' (100, \"\") ; close"
-- Right (StmtCont (PayStmt (RoleParty "alice") (Internal (PubKeyParty "bob")) (IntExpr 100) (SingleToken "")) CloseCont)
--
-- >>>Text.ParserCombinators.Parsec.runParser contractP (Data.Map.fromList []) "(unknown)" "token ada = \"\"; party alice = \"alice\"; party bob = \"bob\"; @alice pays !bob (100, ada) ; close"
-- Right (DeclCont (SeqDecl (SeqDecl (TokenDecl "ada" (SingleToken "")) (PartyDecl "alice" (RoleParty "alice"))) (PartyDecl "bob" (RoleParty "bob"))) (StmtCont (PayStmt (IdentParty ["alice"]) (External (IdentParty ["bob"])) (IntExpr 100) (IdentToken ["ada"])) CloseCont))
--