{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -Wno-name-shadowing -Wno-orphans #-}
module Language.Faustus.TypeChecker where

import Language.Faustus.Syntax
import qualified Language.Faustus.Semantics.Types as Dsl
import qualified Language.Marlowe.Semantics.Types as MDsl
import qualified Data.Text as T
import qualified Data.List as L
import Data.Map
import Data.Text.Encoding (encodeUtf8)
import Control.Monad.State

class TypeDerivation a where
    getConcl :: a -> (Dsl.Context, String, Dsl.FaustusFullType)

data TyRuleIdentifier = IdentifierRule Dsl.Context AbsIdentifier Dsl.FaustusFullType

data TyRuleDotIdentifier = DotIdentifierRule Dsl.Context AbsDotIdentifier Dsl.FaustusFullType

data TyRuleTuple = TupleRule Dsl.Context [AbsArg] [TyRuleArg]
    deriving (Eq, Show, Ord, Read)

getTupleConcl (TupleRule c argBodies argTys) =
    let
        concls = getConcl <$> argTys
        bodies = (\(a,b,c)->b) <$> concls
        tys = (\(a,b,c)->c) <$> concls
    in
        (c, "(" ++ (L.intercalate ", " bodies) ++ ")", tys)

data TyRuleArg = ContArgRule Dsl.Context AbsArg TyRuleCont
    | StmtArgRule Dsl.Context AbsArg [TyRuleStmt]
    | ModuleArgRule Dsl.Context AbsArg Dsl.Context [TyRuleDecl]
    | CaseArgRule Dsl.Context AbsArg TyRuleCase
    | ExprArgRule Dsl.Context AbsArg TyRuleExpr
    | ActionArgRule Dsl.Context AbsArg TyRuleAction
    | BoundArgRule Dsl.Context AbsArg TyRuleBound
    | PartyArgRule Dsl.Context AbsArg TyRuleParty
    | ChoiceArgRule Dsl.Context AbsArg TyRuleChoice
    | TokenArgRule Dsl.Context AbsArg TyRuleToken
    | ArrowArgRule Dsl.Context AbsArg AbsDotIdentifier
    deriving (Eq, Show, Ord, Read)

data TyRuleCont = CloseRule Dsl.Context AbsContract
    | DeclRule Dsl.Context AbsContract TyRuleDecl TyRuleCont -- DeclCont d 
    | StmtRule Dsl.Context AbsContract TyRuleStmt TyRuleCont
    | WhenRule Dsl.Context AbsContract [TyRuleCase] TyRuleExpr TyRuleCont
    | IfRule Dsl.Context AbsContract TyRuleExpr TyRuleCont TyRuleCont
    | ContCallRule Dsl.Context AbsContract AbsDotIdentifier TyRuleTuple -- X(a1, .. an)
    | ContUseRule Dsl.Context AbsContract AbsDotIdentifier
    deriving (Eq, Show, Ord, Read)

data TyRuleStmt = PayInternalRule Dsl.Context AbsStmt TyRuleParty TyRuleParty TyRuleExpr TyRuleToken
    | PayExternalRule Dsl.Context AbsStmt TyRuleParty TyRuleParty TyRuleExpr TyRuleToken
    | AssertRule Dsl.Context AbsStmt TyRuleExpr
    | ReassignRule Dsl.Context AbsStmt AbsIdentifier TyRuleExpr
    | StmtCallRule Dsl.Context AbsStmt AbsDotIdentifier TyRuleTuple
    | StmtUseRule Dsl.Context AbsStmt AbsDotIdentifier
    deriving (Eq, Show, Ord, Read)

data TyRuleDecl = PartyDeclRule Dsl.Context AbsDecl AbsIdentifier TyRuleParty
    | TokenDeclRule Dsl.Context AbsDecl AbsIdentifier TyRuleToken
    | BoundDeclRule Dsl.Context AbsDecl AbsIdentifier TyRuleBound
    | ChoiceDeclRule Dsl.Context AbsDecl AbsIdentifier TyRuleChoice
    | VarDeclRule Dsl.Context AbsDecl AbsIdentifier TyRuleExpr
    | FunDeclRule Dsl.Context AbsDecl AbsIdentifier [AbsParam] TyRuleExpr
    | ActionDeclRule Dsl.Context AbsDecl AbsIdentifier TyRuleAction
    | ActionParamDeclRule Dsl.Context AbsDecl AbsIdentifier [AbsParam] TyRuleAction
    | ContractDeclRule Dsl.Context AbsDecl AbsIdentifier TyRuleCont
    | ContractParamDeclRule Dsl.Context AbsDecl AbsIdentifier [AbsParam] TyRuleCont
    | CaseDeclRule Dsl.Context AbsDecl AbsIdentifier TyRuleCase
    | CaseParamDeclRule Dsl.Context AbsDecl AbsIdentifier [AbsParam] TyRuleCase
    | StatementDeclRule Dsl.Context AbsDecl AbsIdentifier [TyRuleStmt]
    | StatementParamDeclRule Dsl.Context AbsDecl AbsIdentifier [AbsParam] [TyRuleStmt]
    | ModuleRule Dsl.Context AbsDecl AbsIdentifier Dsl.Context [TyRuleDecl]
    | ModuleParamRule Dsl.Context AbsDecl AbsIdentifier Dsl.Context [AbsParam] [TyRuleDecl]
    | ModuleOpenCallRule Dsl.Context AbsDecl AbsDotIdentifier TyRuleTuple
    | ModuleOpenRule Dsl.Context AbsDecl AbsDotIdentifier
    deriving (Eq, Show, Ord, Read)

data TyRuleCase = GuardedContractExpressionCaseRule Dsl.Context AbsCase TyRuleGuardedContractExpression TyRuleCont
    | CaseCallRule Dsl.Context AbsCase AbsDotIdentifier TyRuleTuple
    | CaseUseRule Dsl.Context AbsCase AbsDotIdentifier
    deriving (Eq, Show, Ord, Read)

data TyRuleGuardedContractExpression = ActionGuardRule Dsl.Context AbsGuardExpression TyRuleAction
    | GuardThenGuardRule Dsl.Context AbsGuardExpression TyRuleGuardedContractExpression TyRuleGuardedContractExpression
    | GuardThenStmtsRule Dsl.Context AbsGuardExpression TyRuleGuardedContractExpression [TyRuleStmt]
    | GuardOpRule Dsl.Context AbsGuardExpression TyRuleGuardedContractExpression TyRuleGuardedContractExpression
    deriving (Eq, Show, Ord, Read)

data TyRuleAction = DepositRule Dsl.Context AbsAction TyRuleParty TyRuleExpr TyRuleToken TyRuleParty
    | ChoosesRule Dsl.Context AbsAction TyRuleParty TyRuleChoice
    | ChoosesNotRule Dsl.Context AbsAction TyRuleParty TyRuleChoice
    | ChoosesWithinRule Dsl.Context AbsAction TyRuleParty TyRuleChoice [TyRuleBound]
    | NotifyRule Dsl.Context AbsAction TyRuleExpr
    | ActionCallRule Dsl.Context AbsAction AbsDotIdentifier TyRuleTuple
    | ActionUseRule Dsl.Context AbsAction AbsDotIdentifier
    deriving (Eq, Show, Ord, Read)

data TyRuleExpr = TrueRule Dsl.Context AbsExpr
    | FalseRule Dsl.Context AbsExpr
    | StartRule Dsl.Context AbsExpr
    | EndRule Dsl.Context AbsExpr
    | IntRule Dsl.Context AbsExpr Integer
    | PosixRule Dsl.Context AbsExpr Integer
    | CondRule Dsl.Context AbsExpr TyRuleExpr TyRuleExpr TyRuleExpr
    | AccountBalRule Dsl.Context AbsExpr TyRuleParty TyRuleToken
    | NumOpRule Dsl.Context AbsExpr TyRuleExpr TyRuleExpr
    | NegRule Dsl.Context AbsExpr TyRuleExpr
    | BoolOpRule Dsl.Context AbsExpr TyRuleExpr TyRuleExpr
    | CompOpRule Dsl.Context AbsExpr TyRuleExpr TyRuleExpr
    | NotRule Dsl.Context AbsExpr TyRuleExpr
    | FunRule Dsl.Context AbsExpr AbsDotIdentifier TyRuleTuple
    | ExprUseRule Dsl.Context AbsExpr AbsDotIdentifier
    | ChoiceExprRule Dsl.Context AbsExpr TyRuleParty TyRuleChoice
    | ChosenExprRule Dsl.Context AbsExpr TyRuleParty TyRuleChoice
    deriving (Eq, Show, Ord, Read)

data TyRuleChoice = ConstantChoiceRule Dsl.Context AbsChoice String
    | ChoiceUseRule Dsl.Context AbsChoice AbsDotIdentifier
    deriving (Eq, Show, Ord, Read)

data TyRuleBound = RangeRule Dsl.Context AbsBound TyRuleExpr TyRuleExpr
    | SingleValueRule Dsl.Context AbsBound TyRuleExpr
    | BoundUseRule Dsl.Context AbsBound AbsDotIdentifier
    deriving (Eq, Show, Ord, Read)

data TyRuleToken = ConstantTokenRule Dsl.Context AbsToken String String
    | SingleTokenRule Dsl.Context AbsToken String
    | TokenUseRule Dsl.Context AbsToken AbsDotIdentifier
    deriving (Eq, Show, Ord, Read)

data TyRuleParty = RoleRule Dsl.Context AbsParty String
    | PubKeyRule Dsl.Context AbsParty String
    | PartyUseRule Dsl.Context AbsParty AbsDotIdentifier
    deriving (Eq, Show, Ord, Read)

instance TypeDerivation TyRuleArg where
    getConcl (ContArgRule c body cont) =
        let
            (c', contSyntax, tau) = getConcl cont
        in
            (c, contSyntax, tau)
    getConcl (StmtArgRule c body stmt) =
        let
            (c', stmtSyntax, tau) = getConcl stmt
        in
            (c, stmtSyntax, tau)
    getConcl (ModuleArgRule c body mCtx m) =
        let
            (c', mSyntax, tau) = getConcl m
        in
            (c, mSyntax, Dsl.Primitive . Dsl.TyModule $ mCtx)
    getConcl (CaseArgRule c body cs) =
        let
            (c', caseSyntax, tau) = getConcl cs
        in
            (c, caseSyntax, tau)
    getConcl (ExprArgRule c body e) =
        let
            (c', exprSyntax, tau) = getConcl e
        in
            (c, exprSyntax, tau)
    getConcl (ActionArgRule c body a) =
        let
            (c', actionSyntax, tau) = getConcl a
        in
            (c, actionSyntax, tau)
    getConcl (BoundArgRule c body b) =
        let
            (c', boundSyntax, tau) = getConcl b
        in
            (c, boundSyntax, tau)
    getConcl (PartyArgRule c body p) =
        let
            (c', partySyntax, tau) = getConcl p
        in
            (c, partySyntax, tau)
    getConcl (ChoiceArgRule c body ch) =
        let
            (c', choiceSyntax, tau) = getConcl ch
        in
            (c, choiceSyntax, tau)
    getConcl (TokenArgRule c body t) =
        let
            (c', tokenSyntax, tau) = getConcl t
        in
            (c, tokenSyntax, tau)
    getConcl (ArrowArgRule c body ids) =
        let
            Just res = chaseAbs ids c
        in
            (c, L.intercalate "." ids, res)

instance TypeDerivation TyRuleCont where
    getConcl (CloseRule c body) = (c, "close", Dsl.Primitive Dsl.TyContract)
    getConcl (DeclRule c body decl cont) =
        let
            (c', declSyntax, tauDecl) = getConcl decl
            (c'', contSyntax, tauCont) = getConcl cont
        in
            (c, declSyntax ++ " ;\n" ++ contSyntax, Dsl.Primitive Dsl.TyContract)
    getConcl (StmtRule c body stmt cont) =
        let
            (c', stmtSyntax, tauStmt) = getConcl stmt
            (c'', contSyntax, tauCont) = getConcl cont
        in
            (c, stmtSyntax ++ " ;\n" ++ contSyntax, Dsl.Primitive Dsl.TyContract)
    getConcl (WhenRule c body caseDerivs timeoutDeriv contDeriv) =
        let
            caseConcls = getConcl <$> caseDerivs
            caseBodies = L.intercalate ",\n" $ ((\(a, b, c) -> b) <$> caseConcls)
            (c', exprSyntax, tauExpr) = getConcl timeoutDeriv
            (c'', contSyntax, tauExpr') = getConcl contDeriv
        in
            (c, "when {\n" ++ caseBodies ++ "\n} after " ++ exprSyntax ++ " -> {\n" ++ contSyntax ++ "\n}", Dsl.Primitive Dsl.TyContract)
    getConcl (IfRule c body e c1 c2) =
        let
            (c', exprSyntax, tau) = getConcl e
            (c'', trueContSyntax, tau') = getConcl c1
            (c''', falseContSyntax, tau'') = getConcl c2
        in
            (c, "if " ++ exprSyntax ++ " then\n" ++ trueContSyntax ++ "\nelse\n" ++ falseContSyntax, Dsl.Primitive Dsl.TyContract)
    getConcl (ContCallRule c body dotId args) =
        let
            (c', tupleSyntax, tau) = getTupleConcl args
        in
            (c, L.intercalate "." dotId ++ tupleSyntax, Dsl.Primitive Dsl.TyContract)
    getConcl (ContUseRule c body dotId) = (c, L.intercalate "." dotId, Dsl.Primitive Dsl.TyContract)

instance TypeDerivation TyRuleStmt where
    getConcl (PayInternalRule c body p1 p2 e t) =
        let
            (_, party1Syntax, _) = getConcl p1
            (_, party2Syntax, _) = getConcl p2
            (_, exprSyntax, _) = getConcl e
            (_, tokenSyntax, _) = getConcl t
        in
            (c, party1Syntax ++ " pays @" ++ party2Syntax ++ " (" ++ exprSyntax ++ ", " ++ tokenSyntax ++ ")", Dsl.Primitive Dsl.TyStatement)
    getConcl (PayExternalRule c body p1 p2 e t) =
        let
            (_, party1Syntax, _) = getConcl p1
            (_, party2Syntax, _) = getConcl p2
            (_, exprSyntax, _) = getConcl e
            (_, tokenSyntax, _) = getConcl t
        in
            (c, party1Syntax ++ " pays !" ++ party2Syntax ++ " (" ++ exprSyntax ++ ", " ++ tokenSyntax ++ ")", Dsl.Primitive Dsl.TyStatement)
    getConcl (AssertRule c body e) =
        let
            (_, exprSyntax, _) = getConcl e
        in
            (c, "assert " ++ exprSyntax, Dsl.Primitive Dsl.TyStatement)
    getConcl (ReassignRule c body i e) =
        let
            (_, exprSyntax, _) = getConcl e
        in
            (c, i ++ " := " ++ exprSyntax, Dsl.Primitive Dsl.TyStatement)
    getConcl (StmtCallRule c body ids args) =
        let
            (_, tupleSyntax, _) = getTupleConcl args
        in
            (c, L.intercalate "." ids ++ tupleSyntax, Dsl.Primitive Dsl.TyStatement)
    getConcl (StmtUseRule c body ids) = (c, L.intercalate "." ids, Dsl.Primitive Dsl.TyStatement)

instance TypeDerivation [TyRuleStmt] where
    getConcl [] = error "Empty statements not allowed."
    getConcl [s] = getConcl s
    getConcl (s:ss) =
        let
            (ctx1, sSyntax, sTy) = getConcl s
            (ctx2, ssSyntax, ssTy) = getConcl ss
        in
            (ctx1, sSyntax ++ ";\n" ++ ssSyntax, Dsl.Primitive Dsl.TyStatement)

instance TypeDerivation TyRuleDecl where
    getConcl (PartyDeclRule c body i p) =
        let
            (_, partySyntax, _) = getConcl p
        in
            (c, "party " ++ i ++ " = " ++ partySyntax, Dsl.Primitive . Dsl.TyModule $ singleton i (Dsl.Primitive Dsl.TyParty))
    getConcl (TokenDeclRule c body i t) =
        let
            (_, tokenSyntax, _) = getConcl t
        in
            (c, "token " ++ i ++ " = " ++ tokenSyntax, Dsl.Primitive . Dsl.TyModule $ singleton i (Dsl.Primitive Dsl.TyToken))
    getConcl (BoundDeclRule c body i b) =
        let
            (_, boundSyntax, _) = getConcl b
        in
            (c, "bound " ++ i ++ " = " ++ boundSyntax, Dsl.Primitive . Dsl.TyModule $ singleton i (Dsl.Primitive Dsl.TyBound))
    getConcl (ChoiceDeclRule c body i ch) =
        let
            (_, choiceSyntax, _) = getConcl ch
        in
            (c, "choice " ++ i ++ " = " ++ choiceSyntax, Dsl.Primitive . Dsl.TyModule $ singleton i (Dsl.Primitive Dsl.TyChoice))
    getConcl (VarDeclRule c body i e) =
        let
            (_, exprSyntax, tau) = getConcl e
        in
            (c, "var " ++ i ++ " = " ++ exprSyntax, Dsl.Primitive . Dsl.TyModule $ singleton i (tau))
    getConcl (FunDeclRule c body i ps e) =
        let
            (_, exprSyntax, Dsl.Primitive theta) = getConcl e
            paramTys = (\(Param tau ident) -> tau) <$> ps
        in
            (c, "fun " ++ i ++ " (" ++ L.intercalate ", " (printParam <$> ps) ++ ")\n{\n " ++ exprSyntax ++ "\n}", Dsl.Primitive . Dsl.TyModule $ singleton i (Dsl.Arrow (Dsl.TyFun paramTys theta)))
    getConcl (ActionDeclRule c body i a) =
        let
            (_, actionSyntax, _) = getConcl a
        in
            (c, "action " ++ i ++ " = {\n" ++ actionSyntax ++ "\n}", Dsl.Primitive . Dsl.TyModule $ singleton i (Dsl.Primitive Dsl.TyAction))
    getConcl (ActionParamDeclRule c body i ps a) =
        let
            (_, actionSyntax, _) = getConcl a
            paramTys = (\(Param tau ident) -> tau) <$> ps
        in
            (c, "action " ++ i ++ " (" ++ L.intercalate ", " (printParam <$> ps) ++ ")\n{\n" ++ actionSyntax ++ "\n}", Dsl.Primitive . Dsl.TyModule $ singleton i (Dsl.Arrow (Dsl.TyFun paramTys Dsl.TyAction)))
    getConcl (ContractDeclRule c body i cont) =
        let
            (_, contSyntax, _) = getConcl cont
        in
            (c, "contract " ++ i ++ " = {\n" ++ contSyntax ++ "\n}", Dsl.Primitive . Dsl.TyModule $ singleton i (Dsl.Primitive Dsl.TyContract))
    getConcl (ContractParamDeclRule c body i ps cont) =
        let
            (_, contSyntax, _) = getConcl cont
            paramTys = (\(Param tau ident) -> tau) <$> ps
        in
            (c, "contract " ++ i ++ " (" ++ L.intercalate ", " (printParam <$> ps) ++ ")\n{\n" ++ contSyntax ++ "\n}", Dsl.Primitive . Dsl.TyModule $ singleton i (Dsl.Arrow (Dsl.TyFun paramTys Dsl.TyContract)))
    getConcl (CaseDeclRule c body i ca) =
        let
            (_, caseSyntax, _) = getConcl ca
        in
            (c, "case " ++ i ++ " = {\n" ++ caseSyntax ++ "\n}", Dsl.Primitive . Dsl.TyModule $ singleton i (Dsl.Primitive Dsl.TyCase))
    getConcl (CaseParamDeclRule c body i ps ca) =
        let
            (_, caseSyntax, _) = getConcl ca
            paramTys = (\(Param tau ident) -> tau) <$> ps
        in
            (c, "case " ++ i ++ " (" ++ L.intercalate ", " (printParam <$> ps) ++ ")\n{\n" ++ caseSyntax ++ "\n}", Dsl.Primitive . Dsl.TyModule $ singleton i (Dsl.Arrow (Dsl.TyFun paramTys Dsl.TyCase)))
    getConcl (StatementDeclRule c body i s) =
        let
            (_, statementSyntax, _) = getConcl s
        in
            (c, "statement " ++ i ++ " = {\n" ++ statementSyntax ++ "\n}", Dsl.Primitive . Dsl.TyModule $ singleton i (Dsl.Primitive Dsl.TyStatement))
    getConcl (StatementParamDeclRule c body i ps s) =
        let
            (_, statementSyntax, _) = getConcl s
            paramTys = (\(Param tau ident) -> tau) <$> ps
        in
            (c, "statement " ++ i ++ " (" ++ L.intercalate ", " (printParam <$> ps) ++ ")\n{\n" ++ statementSyntax ++ "\n}", Dsl.Primitive . Dsl.TyModule $ singleton i (Dsl.Arrow (Dsl.TyFun paramTys Dsl.TyStatement)))
    getConcl (ModuleRule c body i exportedCtx m) =
        let
            (_, moduleSyntax, tau) = getConcl m
        in
            (c, "module " ++ i ++ " = {\n" ++ moduleSyntax ++ "\n}", Dsl.Primitive . Dsl.TyModule $ singleton i (Dsl.Primitive (Dsl.TyModule exportedCtx)))
    getConcl (ModuleParamRule c body i exportedCtx ps m) =
        let
            (_, moduleSyntax, Dsl.Primitive theta) = getConcl m
            paramTys = (\(Param tau ident) -> tau) <$> ps
        in
            (c, "module " ++ i ++ " (" ++ L.intercalate ", " (printParam <$> ps) ++ ")\n{\n " ++ moduleSyntax ++ "\n}", Dsl.Primitive . Dsl.TyModule $ singleton i (Dsl.Arrow (Dsl.TyFun paramTys (Dsl.TyModule exportedCtx))))
    getConcl (ModuleOpenCallRule c body ids args) =
        let
            Just (Dsl.Arrow (Dsl.TyFun pTys (Dsl.TyModule moduleCtx))) = chaseAbs ids c
            (_, tupleSyntax, _) = getTupleConcl args
        in
            (c, "open " ++ L.intercalate "." ids ++ " " ++ tupleSyntax, Dsl.Primitive . Dsl.TyModule $ moduleCtx)
    getConcl (ModuleOpenRule c body ids) =
        let
            Just (Dsl.Primitive (Dsl.TyModule moduleCtx)) = chaseAbs ids c
        in
            (c, "open " ++ L.intercalate "." ids, Dsl.Primitive . Dsl.TyModule $ moduleCtx)

instance TypeDerivation [TyRuleDecl] where
    getConcl [] = (Data.Map.empty, "", Dsl.Primitive . Dsl.TyModule $ Data.Map.empty)
    getConcl (d:ds) =
        let
            (ctx1, dSyntax, Dsl.Primitive (Dsl.TyModule dCtx)) = getConcl d
            (ctx2, dsSyntax, Dsl.Primitive (Dsl.TyModule dsCtx)) = getConcl ds
        in
            (ctx1, dSyntax ++ ";\n" ++ dsSyntax, Dsl.Primitive (Dsl.TyModule (Data.Map.union dsCtx dCtx)))

instance TypeDerivation TyRuleCase where
    getConcl (GuardedContractExpressionCaseRule c body ge cont) =
        let
            (c', geSyntax, tau1) = getConcl ge
            (c'', contSyntax, tau2) = getConcl cont
        in
            (c, geSyntax ++ " -> \n{\n" ++ contSyntax ++ "\n}\n", Dsl.Primitive Dsl.TyCase)
    getConcl (CaseCallRule c body ids args) =
        let
            (c', tupleSyntax, tau) = getTupleConcl args
        in
            (c, L.intercalate "." ids ++ tupleSyntax, Dsl.Primitive Dsl.TyCase)
    getConcl (CaseUseRule c body ids) = (c, L.intercalate "." ids, Dsl.Primitive Dsl.TyCase)

instance TypeDerivation TyRuleGuardedContractExpression where
    getConcl (ActionGuardRule c body a) =
        let
            (c', actionSyntax, tau1) = getConcl a
        in
            (c, actionSyntax, Dsl.Primitive Dsl.TyGuardedContractExpression)
    getConcl (GuardThenStmtsRule c body g stmts) =
        let
            (c', guardSyntax, tau1) = getConcl g
            (c'', stmtsSyntax, tau2) = getConcl stmts
        in
            (c, guardSyntax ++ " -> " ++ stmtsSyntax, Dsl.Primitive Dsl.TyGuardedContractExpression)
    getConcl (GuardThenGuardRule c body g1 g2) =
        let
            (c', g1Syntax, tau1) = getConcl g1
            (c'', g2Syntax, tau2) = getConcl g2
        in
            (c, g1Syntax ++ " -> " ++ g2Syntax, Dsl.Primitive Dsl.TyGuardedContractExpression)
    getConcl (GuardOpRule c body e1 e2) = 
        let
            (c1, e1Syntax, e1Ty) = getConcl e1
            (c2, e2Syntax, e2Ty) = getConcl e2
            op = case body of
                InterleavedGuard _ _ -> "|||"
                DisjointGuard _ _ -> "<+>"
                _ -> error "No operator in Guarded Contract Expression syntax for operator type rule."
        in
            (c, "( " ++ e1Syntax ++ " ) " ++ op ++ " ( " ++ e2Syntax ++ " )", Dsl.Primitive Dsl.TyGuardedContractExpression)

instance TypeDerivation TyRuleAction where
    getConcl (DepositRule c body p1 e t p2) =
        let
            (c1, p1Syntax, tau1) = getConcl p1
            (c2, eSyntax, tau2) = getConcl e
            (c3, tSyntax, tau3) = getConcl t
            (c4, p2Syntax, tau4) = getConcl p2
        in
            (c, p1Syntax ++ " deposits " ++ eSyntax ++ " " ++ tSyntax ++ " @" ++ p2Syntax, Dsl.Primitive Dsl.TyAction)
    getConcl (ChoosesRule c body p ch) =
        let
            (c1, pSyntax, tau1) = getConcl p
            (c2, chSyntax, tau2) = getConcl ch
        in
            (c, pSyntax ++ " chooses " ++ chSyntax, Dsl.Primitive Dsl.TyAction)
    getConcl (ChoosesNotRule c body p ch) =
        let
            (c1, pSyntax, tau1) = getConcl p
            (c2, chSyntax, tau2) = getConcl ch
        in
            (c, pSyntax ++ " chooses not " ++ chSyntax, Dsl.Primitive Dsl.TyAction)
    getConcl (ChoosesWithinRule c body p ch bs) =
        let
            (c1, pSyntax, tau1) = getConcl p
            (c2, chSyntax, tau2) = getConcl ch
            bConcls = getConcl <$> bs
            bBodies = (\(a,b,c)->b) <$> bConcls
        in
            (c, pSyntax ++ " chooses " ++ chSyntax ++ " within " ++ (L.intercalate "" bBodies) ++ "", Dsl.Primitive Dsl.TyAction)
    getConcl (NotifyRule c body e) =
        let
            (c1, eSyntax, tau) = getConcl e
        in
            (c, "notify " ++ eSyntax, Dsl.Primitive Dsl.TyAction)
    getConcl (ActionCallRule c body ids args) =
        let
            (c1, tupleSyntax, tau) = getTupleConcl args
        in
            (c, L.intercalate "." ids ++ tupleSyntax, Dsl.Primitive Dsl.TyAction)
    getConcl (ActionUseRule c body ids) = (c, L.intercalate "." ids, Dsl.Primitive Dsl.TyAction)


getTyPurity :: Dsl.FaustusFullType -> Dsl.FaustusPurityType
getTyPurity (Dsl.Primitive (Dsl.TyBool p)) = p
getTyPurity (Dsl.Primitive (Dsl.TyInt p)) = p
getTyPurity (Dsl.Primitive (Dsl.TyPosix p)) = p
getTyPurity ty = error $ "Type does not have a purity: " ++ show ty

extractOuterType :: Dsl.FaustusFullType -> (Dsl.FaustusPurityType -> Dsl.FaustusFullType)
extractOuterType (Dsl.Primitive (Dsl.TyBool _)) = (\p -> Dsl.Primitive (Dsl.TyBool p))
extractOuterType (Dsl.Primitive (Dsl.TyInt _)) = (\p -> Dsl.Primitive (Dsl.TyInt p))
extractOuterType (Dsl.Primitive (Dsl.TyPosix _)) = (\p -> Dsl.Primitive (Dsl.TyPosix p))
extractOuterType ty = (\p -> ty)

instance TypeDerivation TyRuleExpr where
    getConcl (TrueRule c body) = (c, "true", Dsl.Primitive . Dsl.TyBool $ Dsl.TyPure)
    getConcl (FalseRule c body) = (c, "false", Dsl.Primitive . Dsl.TyBool $ Dsl.TyPure)
    getConcl (StartRule c body) = (c, "start", Dsl.Primitive . Dsl.TyPosix $ Dsl.TyImpure)
    getConcl (EndRule c body) = (c, "end", Dsl.Primitive . Dsl.TyPosix $ Dsl.TyImpure)
    getConcl (IntRule c body i) = (c, show i, Dsl.Primitive . Dsl.TyInt $ Dsl.TyPure)
    getConcl (PosixRule c body p) = (c, show p, Dsl.Primitive . Dsl.TyPosix $ Dsl.TyPure)
    getConcl (CondRule c body eCond eTrue eFalse) =
        let
            (c1, condSyntax, condTy) = getConcl eCond
            (c2, eTrueSyntax, eTrueTy) = getConcl eTrue
            (c3, eFalseSyntax, eFalseTy) = getConcl eFalse
            p1 = getTyPurity condTy
            p2 = getTyPurity eTrueTy
            p3 = getTyPurity eFalseTy
            maxPurity = max p1 (max p2 p3)
            resTy = (extractOuterType eTrueTy) maxPurity
        in
            (c, condSyntax ++ " ? " ++ eTrueSyntax ++ " : " ++ eFalseSyntax, resTy)
    getConcl (AccountBalRule c body p t) =
        let
            (c1, partySyntax, tau1) = getConcl p
            (c2, tokenSyntax, tau2) = getConcl t
        in
            (c, "@" ++ partySyntax ++ "$" ++ tokenSyntax, Dsl.Primitive . Dsl.TyInt $ Dsl.TyImpure)
    getConcl (NumOpRule c body e1 e2) =
        let
            (c1, e1Syntax, e1Ty) = getConcl e1
            (c2, e2Syntax, e2Ty) = getConcl e2
            p1 = getTyPurity e1Ty
            p2 = getTyPurity e2Ty
            op = case body of
                AddExpr _ _ -> "+"
                SubExpr _ _ -> "-"
                MulExpr _ _ -> "*"
                DivExpr _ _ -> "/"
        in
            (c, "( "  ++ e1Syntax ++ " ) " ++ op ++ " ( " ++ e2Syntax ++ " )", (extractOuterType e1Ty) (max p1 p2))
    getConcl (NegRule c body e) =
        let
            (c1, eSyntax, eTy) = getConcl e
        in
            (c, "-( " ++ eSyntax ++ " )", extractOuterType eTy $ getTyPurity eTy)
    getConcl (BoolOpRule c body e1 e2) =
        let
            (c1, e1Syntax, e1Ty) = getConcl e1
            (c2, e2Syntax, e2Ty) = getConcl e2
            p1 = getTyPurity e1Ty
            p2 = getTyPurity e2Ty
            op = case body of
                AndExpr _ _ -> "&&"
                OrExpr _ _ -> "||"
        in
            (c, "( "  ++ e1Syntax ++ " ) " ++ op ++ " ( " ++ e2Syntax ++ " )", Dsl.Primitive . Dsl.TyBool $ max p1 p2)
    getConcl (CompOpRule c body e1 e2) =
        let
            (c1, e1Syntax, e1Ty) = getConcl e1
            (c2, e2Syntax, e2Ty) = getConcl e2
            p1 = getTyPurity e1Ty
            p2 = getTyPurity e2Ty
            op = case body of
                GEExpr _ _ -> ">="
                GTExpr _ _ -> ">"
                LEExpr _ _ -> "<="
                LTExpr _ _ -> "<"
                EqExpr _ _ -> "=="
                NEqExpr _ _ -> "!="
        in
            (c, "( "  ++ e1Syntax ++ " ) " ++ op ++ " ( " ++ e2Syntax ++ " )", Dsl.Primitive . Dsl.TyBool $ max p1 p2)
    getConcl (NotRule c body e) =
        let
            (c1, eSyntax, eTy) = getConcl e
        in
            (c, "!( " ++ eSyntax ++ " )", Dsl.Primitive . Dsl.TyBool $ getTyPurity eTy)
    getConcl (FunRule c body ids args) =
        let
            (c1, tupleSyntax, tau) = getTupleConcl args
            Just (Dsl.Arrow (Dsl.TyFun pTys resTy)) = chaseAbs ids c
        in
            (c, L.intercalate "." ids ++ tupleSyntax, Dsl.Primitive resTy)
    getConcl (ExprUseRule c body ids) =
        let
            Just resTy = chaseAbs ids c
        in
            (c, L.intercalate "." ids, resTy)
    getConcl (ChoiceExprRule c body p ch) =
        let
            (c1, pSyntax, pTy) = getConcl p
            (c2, chSyntax, chTy) = getConcl ch
        in
            (c, "(" ++ pSyntax ++ ", " ++ chSyntax ++ ")#", Dsl.Primitive . Dsl.TyInt $ Dsl.TyImpure)
    getConcl (ChosenExprRule c body p ch) =
        let
            (c1, pSyntax, pTy) = getConcl p
            (c2, chSyntax, chTy) = getConcl ch
        in
            (c, "(" ++ pSyntax ++ ", " ++ chSyntax ++ ")?", Dsl.Primitive . Dsl.TyBool $ Dsl.TyImpure)

instance TypeDerivation TyRuleChoice where
    getConcl (ConstantChoiceRule c body n) = (c, "\"" ++ n ++ "\"", Dsl.Primitive Dsl.TyChoice)
    getConcl (ChoiceUseRule c body ids) = (c, L.intercalate "." ids, Dsl.Primitive Dsl.TyChoice)

instance TypeDerivation TyRuleBound where
    getConcl (RangeRule c body e1 e2) =
        let
            (c1, e1Syntax, e1Ty) = getConcl e1
            (c2, e2Syntax, e2Ty) = getConcl e2
        in
            (c, "[" ++ e1Syntax ++ ", " ++ e2Syntax ++ "]", Dsl.Primitive Dsl.TyBound)
    getConcl (SingleValueRule c body e) =
        let
            (c1, eSyntax, eTy) = getConcl e
        in
            (c, "[" ++ eSyntax ++ "]", Dsl.Primitive Dsl.TyBound)
    getConcl (BoundUseRule c body ids) = (c, L.intercalate "." ids, Dsl.Primitive Dsl.TyBound)

instance TypeDerivation TyRuleToken where
    getConcl (ConstantTokenRule c body t1 t2) = (c, "\"" ++ t1 ++ "\" \"" ++ t2 ++ "\"", Dsl.Primitive Dsl.TyToken)
    getConcl (SingleTokenRule c body t) = (c, "\"" ++ t ++ "\"", Dsl.Primitive Dsl.TyToken)
    getConcl (TokenUseRule c body ids) = (c, L.intercalate "." ids, Dsl.Primitive Dsl.TyToken)

instance TypeDerivation TyRuleParty where
    getConcl (RoleRule c body r) = (c, "\"" ++ r ++ "\"", Dsl.Primitive Dsl.TyParty)
    getConcl (PubKeyRule c body p) = (c, "\'" ++ p ++ "\'", Dsl.Primitive Dsl.TyParty)
    getConcl (PartyUseRule c body ids) = (c, L.intercalate "." ids, Dsl.Primitive Dsl.TyParty)

printParam :: AbsParam -> String
printParam (Param tau i) =
    printFullTy tau ++ " " ++ i
    where
        printFullTy tau = case tau of
            Dsl.Primitive theta -> printTheta theta
            Dsl.Arrow (Dsl.TyFun paramTys theta) -> "(" ++ (L.intercalate "," (printFullTy <$> paramTys)) ++ ") -> " ++ printTheta theta
        printTheta theta = case theta of
            Dsl.TyParty -> "party"
            Dsl.TyToken -> "token"
            Dsl.TyContract -> "contract"
            Dsl.TyCase -> "case"
            Dsl.TyBound -> "bound"
            Dsl.TyChoice -> "choice"
            Dsl.TyAction -> "action"
            Dsl.TyInt _ -> "int"
            Dsl.TyBool _ -> "bool"
            Dsl.TyPosix _ -> "posix"
            Dsl.TyModule _ -> "module"
            Dsl.TyStatement -> "statement"

type TypeCheck a = StateT Dsl.Context (Either String) a

typeCheckError :: String -> TypeCheck a
typeCheckError str = lift (Left str)

createUseError :: [AbsIdentifier] -> String -> TypeCheck a
createUseError ids ty = typeCheckError $ (L.intercalate "." ids) ++ " is not declared or is not a " ++ ty ++ "."

typeCheckBasicUse :: TypeDerivation a => [AbsIdentifier] -> Dsl.FaustusPrimType -> String -> a -> TypeCheck a
typeCheckBasicUse ids primTy primTyName derivation = do
    ctx <- get
    case chaseAbs ids ctx of
        Just (Dsl.Primitive lookupTy) -> if primTy == lookupTy then return derivation else createUseError ids primTyName
        _ -> createUseError ids primTyName

typeCheckCall :: TypeDerivation a => [AbsIdentifier] -> [AbsArg] -> Dsl.FaustusPrimType -> String -> (TyRuleTuple -> a) -> TypeCheck a
typeCheckCall ids args primTy primTyName derivation = do
    ctx <- get
    case chaseAbs ids ctx of
        Just (Dsl.Arrow (Dsl.TyFun paramTys resTy)) -> if resTy == primTy
            then (do
                tupleDeriv <- typeCheckTuple paramTys args
                return $ derivation tupleDeriv)
            else typeCheckError $ (L.intercalate "." ids) ++ " is not is not a " ++ primTyName ++ " function."
        _ -> typeCheckError $ (L.intercalate "." ids) ++ " is not is not a " ++ primTyName ++ " function."

typeCheckParty :: AbsParty -> TypeCheck TyRuleParty
typeCheckParty body@(PubKeyParty pk) = do
    ctx <- get
    return $ PubKeyRule ctx body pk
typeCheckParty body@(RoleParty r) = do
    ctx <- get
    return $ RoleRule ctx body r
typeCheckParty body@(IdentParty i) = do
    ctx <- get
    typeCheckBasicUse i Dsl.TyParty "party" (PartyUseRule ctx body i)

typeCheckToken :: AbsToken -> TypeCheck TyRuleToken
typeCheckToken body@(ConstantToken t1 t2) = do
    ctx <- get
    return $ ConstantTokenRule ctx body t1 t2
typeCheckToken body@(SingleToken t1) = do
    ctx <- get
    return $ SingleTokenRule ctx body t1
typeCheckToken body@(IdentToken i) = do
    ctx <- get
    typeCheckBasicUse i Dsl.TyToken "token" (TokenUseRule ctx body i)

typeCheckChoice :: AbsChoice -> TypeCheck TyRuleChoice
typeCheckChoice body@(ConstantChoice n) = do
    ctx <- get
    return $ ConstantChoiceRule ctx body n
typeCheckChoice body@(IdentChoice i) = do
    ctx <- get
    typeCheckBasicUse i Dsl.TyChoice "choice" (ChoiceUseRule ctx body i)

typeCheckArg :: (Dsl.FaustusFullType, AbsArg) -> TypeCheck TyRuleArg
typeCheckArg (Dsl.Arrow (Dsl.TyFun paramTys resTy), body@(ArrowArg ids)) = do
    ctx <- get
    case chaseAbs ids ctx of
        Just (Dsl.Arrow (Dsl.TyFun paramTys' resTy')) -> if paramTys' == paramTys && resTy' == resTy
            then return $ ArrowArgRule ctx body ids
            else typeCheckError $ L.intercalate "." ids ++ " does not have the expected function type."
        _ -> typeCheckError $ L.intercalate "." ids ++ " is not declared or is not a function type."
typeCheckArg (Dsl.Primitive Dsl.TyParty, body@(PartyArg p)) = typeCheckPrimArg body p typeCheckParty PartyArgRule
typeCheckArg (Dsl.Primitive Dsl.TyToken, body@(TokenArg t)) = typeCheckPrimArg body t typeCheckToken TokenArgRule
typeCheckArg (Dsl.Primitive Dsl.TyContract, body@(ContArg c)) = typeCheckPrimArg body c typeCheckCont ContArgRule
typeCheckArg (Dsl.Primitive Dsl.TyCase, body@(CaseArg c)) = typeCheckPrimArg body c typeCheckCase CaseArgRule
typeCheckArg (Dsl.Primitive Dsl.TyChoice, body@(ChoiceArg c)) = typeCheckPrimArg body c typeCheckChoice ChoiceArgRule
typeCheckArg (Dsl.Primitive Dsl.TyBound, body@(BoundArg b)) = typeCheckPrimArg body b typeCheckBound BoundArgRule
typeCheckArg (Dsl.Primitive Dsl.TyAction, body@(ActionArg a)) = typeCheckPrimArg body a typeCheckAction ActionArgRule
typeCheckArg (Dsl.Primitive Dsl.TyStatement, body@(StmtArg ss)) = do
    ctx <- get
    res <- mapM typeCheckStmt ss
    return $ StmtArgRule ctx body res
typeCheckArg (Dsl.Primitive (Dsl.TyBool tyPurity), body@(ExprArg e)) = do
    ctx <- get
    eDeriv <- typeCheckExpr e
    let (ctx1, eSyntax, eTy) = getConcl eDeriv
    case eTy of
        (Dsl.Primitive (Dsl.TyBool tyPurity')) -> if tyPurity' <= tyPurity then return $ ExprArgRule ctx body eDeriv else typeCheckError $ "Expected pure boolean expression."
        _ -> typeCheckError $ "Expected boolean expression."
typeCheckArg (Dsl.Primitive (Dsl.TyInt tyPurity), body@(ExprArg e)) = do
    ctx <- get
    eDeriv <- typeCheckExpr e
    let (ctx1, eSyntax, eTy) = getConcl eDeriv
    case eTy of
        (Dsl.Primitive (Dsl.TyInt tyPurity')) -> if tyPurity' <= tyPurity then return $ ExprArgRule ctx body eDeriv else typeCheckError $ "Expected pure integer expression."
        _ -> typeCheckError $ "Expected integer expression."
typeCheckArg (Dsl.Primitive (Dsl.TyPosix tyPurity), body@(ExprArg e)) = do
    ctx <- get
    eDeriv <- typeCheckExpr e
    let (ctx1, eSyntax, eTy) = getConcl eDeriv
    case eTy of
        (Dsl.Primitive (Dsl.TyPosix tyPurity')) -> if tyPurity' <= tyPurity then return $ ExprArgRule ctx body eDeriv else typeCheckError $ "Expected pure posix expression."
        _ -> typeCheckError $ "Expected posix expression."
typeCheckArg (Dsl.Primitive (Dsl.TyModule moduleCtx), body@(ModuleArg ds)) = do
    ctx <- get
    mDeriv <- mapM typeCheckDecl ds
    let (ctx1, mSyntax, Dsl.Primitive (Dsl.TyModule tyMArg)) = getConcl mDeriv
    if all (\(i, ty) -> moduleExportMatch i ty tyMArg) (toList moduleCtx)
    then return $ ModuleArgRule ctx body moduleCtx mDeriv
    else typeCheckError $ "Incorrect module type."
        
typeCheckPrimArg :: AbsArg -> t1 -> (t1 -> TypeCheck t2) -> (Dsl.Context -> AbsArg -> t2 -> TyRuleArg) -> TypeCheck TyRuleArg
typeCheckPrimArg outerBody innerBody typeChecker rule = do
    ctx <- get
    tyDeriv <- typeChecker innerBody
    return $ rule ctx outerBody tyDeriv

typeCheckTuple :: [Dsl.FaustusFullType] -> [AbsArg] -> TypeCheck TyRuleTuple
typeCheckTuple [] [] = do
    ctx <- get
    return $ TupleRule ctx [] []
typeCheckTuple tys args = do
    ctx <- get
    argDerivs <- mapM (typeCheckArg) (zip tys args)
    return $ TupleRule ctx args argDerivs

typeCheckExpr :: AbsExpr -> TypeCheck TyRuleExpr
typeCheckExpr body@(TrueExpr) = do
    ctx <- get
    return $ TrueRule ctx body
typeCheckExpr body@(FalseExpr) = do
    ctx <- get
    return $ FalseRule ctx body
typeCheckExpr body@(StartExpr) = do
    ctx <- get
    return $ StartRule ctx body
typeCheckExpr body@(EndExpr) = do
    ctx <- get
    return $ EndRule ctx body
typeCheckExpr body@(IntExpr i) = do
    ctx <- get
    return $ IntRule ctx body i
typeCheckExpr body@(PosixIntExpr i) = do
    ctx <- get
    return $ PosixRule ctx body i
typeCheckExpr body@(NotExpr e) = do
    ctx <- get
    innerDeriv <- typeCheckExpr e
    let (innerCtx, innerSyntax, innerTy) = getConcl innerDeriv
    case innerTy of
        (Dsl.Primitive (Dsl.TyBool purity)) -> return $ (NotRule ctx body innerDeriv)
        _ -> typeCheckError $ "Expected boolean expression: " ++ innerSyntax
typeCheckExpr body@(NegExpr e) = do
    ctx <- get
    innerDeriv <- typeCheckExpr e
    let (innerCtx, innerSyntax, innerTy) = getConcl innerDeriv
    case innerTy of
        (Dsl.Primitive (Dsl.TyInt purity)) -> return $ (NegRule ctx body innerDeriv)
        _ -> typeCheckError $ "Expected integer expression: " ++ innerSyntax
typeCheckExpr body@(AndExpr e1 e2) = do
    ctx <- get
    innerDeriv1 <- typeCheckExpr e1
    innerDeriv2 <- typeCheckExpr e2
    let (innerCtx1, innerSyntax1, innerTy1) = getConcl innerDeriv1
        (innerCtx2, innerSyntax2, innerTy2) = getConcl innerDeriv2
    case (innerTy1, innerTy2) of
        (Dsl.Primitive (Dsl.TyBool purity1), Dsl.Primitive (Dsl.TyBool purity2)) -> return $ (BoolOpRule ctx body innerDeriv1 innerDeriv2)
        _ -> typeCheckError $ "Expected boolean expressions:\n\t" ++ innerSyntax1 ++ "\n\t" ++ innerSyntax2
typeCheckExpr body@(OrExpr e1 e2) = do
    ctx <- get
    innerDeriv1 <- typeCheckExpr e1
    innerDeriv2 <- typeCheckExpr e2
    let (innerCtx1, innerSyntax1, innerTy1) = getConcl innerDeriv1
        (innerCtx2, innerSyntax2, innerTy2) = getConcl innerDeriv2
    case (innerTy1, innerTy2) of
        (Dsl.Primitive (Dsl.TyBool purity1), Dsl.Primitive (Dsl.TyBool purity2)) -> return $ (BoolOpRule ctx body innerDeriv1 innerDeriv2)
        _ -> typeCheckError $ "Expected boolean expressions:\n\t" ++ innerSyntax1 ++ "\n\t" ++ innerSyntax2
typeCheckExpr body@(AddExpr e1 e2) = do
    ctx <- get
    innerDeriv1 <- typeCheckExpr e1
    innerDeriv2 <- typeCheckExpr e2
    let (innerCtx1, innerSyntax1, innerTy1) = getConcl innerDeriv1
        (innerCtx2, innerSyntax2, innerTy2) = getConcl innerDeriv2
    case (innerTy1, innerTy2) of
        (Dsl.Primitive (Dsl.TyInt purity1), Dsl.Primitive (Dsl.TyInt purity2)) -> return $ (NumOpRule ctx body innerDeriv1 innerDeriv2)
        _ -> typeCheckError $ "Expected integer expressions:\n\t" ++ innerSyntax1 ++ "\n\t" ++ innerSyntax2
typeCheckExpr body@(CondExpr e1 e2 e3) = do
    ctx <- get
    innerDeriv1 <- typeCheckExpr e1
    innerDeriv2 <- typeCheckExpr e2
    innerDeriv3 <- typeCheckExpr e3
    let (innerCtx1, innerSyntax1, innerTy1) = getConcl innerDeriv1
        (innerCtx2, innerSyntax2, innerTy2) = getConcl innerDeriv2
        (innerCtx3, innerSyntax3, innerTy3) = getConcl innerDeriv3
    case innerTy1 of
        (Dsl.Primitive (Dsl.TyBool purity1)) -> case (innerTy2, innerTy3) of
            (Dsl.Primitive (Dsl.TyInt purity2), Dsl.Primitive (Dsl.TyInt purity3)) -> return $ (CondRule ctx body innerDeriv1 innerDeriv2 innerDeriv3)
            (Dsl.Primitive (Dsl.TyBool purity2), Dsl.Primitive (Dsl.TyBool purity3)) -> return $ (CondRule ctx body innerDeriv1 innerDeriv2 innerDeriv3)
            (Dsl.Primitive (Dsl.TyPosix purity2), Dsl.Primitive (Dsl.TyPosix purity3)) -> return $ (CondRule ctx body innerDeriv1 innerDeriv2 innerDeriv3)
            _ -> typeCheckError $ "Expressions do not have matching types:\n\t" ++ innerSyntax2 ++ "\n\t" ++ innerSyntax3
        _ -> typeCheckError $ "Expected boolean expression: " ++ innerSyntax1
typeCheckExpr body@(SubExpr e1 e2) = do
    ctx <- get
    innerDeriv1 <- typeCheckExpr e1
    innerDeriv2 <- typeCheckExpr e2
    let (innerCtx1, innerSyntax1, innerTy1) = getConcl innerDeriv1
        (innerCtx2, innerSyntax2, innerTy2) = getConcl innerDeriv2
    case (innerTy1, innerTy2) of
        (Dsl.Primitive (Dsl.TyInt purity1), Dsl.Primitive (Dsl.TyInt purity2)) -> return $ (NumOpRule ctx body innerDeriv1 innerDeriv2)
        _ -> typeCheckError $ "Expected integer expressions:\n\t" ++ innerSyntax1 ++ "\n\t" ++ innerSyntax2
typeCheckExpr body@(MulExpr e1 e2) = do
    ctx <- get
    innerDeriv1 <- typeCheckExpr e1
    innerDeriv2 <- typeCheckExpr e2
    let (innerCtx1, innerSyntax1, innerTy1) = getConcl innerDeriv1
        (innerCtx2, innerSyntax2, innerTy2) = getConcl innerDeriv2
    case (innerTy1, innerTy2) of
        (Dsl.Primitive (Dsl.TyInt purity1), Dsl.Primitive (Dsl.TyInt purity2)) -> return $ (NumOpRule ctx body innerDeriv1 innerDeriv2)
        _ -> typeCheckError $ "Expected integer expressions:\n\t" ++ innerSyntax1 ++ "\n\t" ++ innerSyntax2
typeCheckExpr body@(DivExpr e1 e2) = do
    ctx <- get
    innerDeriv1 <- typeCheckExpr e1
    innerDeriv2 <- typeCheckExpr e2
    let (innerCtx1, innerSyntax1, innerTy1) = getConcl innerDeriv1
        (innerCtx2, innerSyntax2, innerTy2) = getConcl innerDeriv2
    case (innerTy1, innerTy2) of
        (Dsl.Primitive (Dsl.TyInt purity1), Dsl.Primitive (Dsl.TyInt purity2)) -> return $ (NumOpRule ctx body innerDeriv1 innerDeriv2)
        _ -> typeCheckError $ "Expected integer expressions:\n\t" ++ innerSyntax1 ++ "\n\t" ++ innerSyntax2
typeCheckExpr body@(EqExpr e1 e2) = do
    ctx <- get
    innerDeriv1 <- typeCheckExpr e1
    innerDeriv2 <- typeCheckExpr e2
    let (innerCtx1, innerSyntax1, innerTy1) = getConcl innerDeriv1
        (innerCtx2, innerSyntax2, innerTy2) = getConcl innerDeriv2
    case (innerTy1, innerTy2) of
        (Dsl.Primitive (Dsl.TyInt purity1), Dsl.Primitive (Dsl.TyInt purity2)) -> return $ (CompOpRule ctx body innerDeriv1 innerDeriv2)
        (Dsl.Primitive (Dsl.TyBool purity1), Dsl.Primitive (Dsl.TyBool purity2)) -> return $ (CompOpRule ctx body innerDeriv1 innerDeriv2)
        (Dsl.Primitive (Dsl.TyPosix purity1), Dsl.Primitive (Dsl.TyPosix purity2)) -> return $ (CompOpRule ctx body innerDeriv1 innerDeriv2)
        _ -> typeCheckError $ "Expected integer expressions:\n\t" ++ innerSyntax1 ++ "\n\t" ++ innerSyntax2
typeCheckExpr body@(NEqExpr e1 e2) = do
    ctx <- get
    innerDeriv1 <- typeCheckExpr e1
    innerDeriv2 <- typeCheckExpr e2
    let (innerCtx1, innerSyntax1, innerTy1) = getConcl innerDeriv1
        (innerCtx2, innerSyntax2, innerTy2) = getConcl innerDeriv2
    case (innerTy1, innerTy2) of
        (Dsl.Primitive (Dsl.TyInt purity1), Dsl.Primitive (Dsl.TyInt purity2)) -> return $ (CompOpRule ctx body innerDeriv1 innerDeriv2)
        (Dsl.Primitive (Dsl.TyBool purity1), Dsl.Primitive (Dsl.TyBool purity2)) -> return $ (CompOpRule ctx body innerDeriv1 innerDeriv2)
        (Dsl.Primitive (Dsl.TyPosix purity1), Dsl.Primitive (Dsl.TyPosix purity2)) -> return $ (CompOpRule ctx body innerDeriv1 innerDeriv2)
        _ -> typeCheckError $ "Expected integer expressions:\n\t" ++ innerSyntax1 ++ "\n\t" ++ innerSyntax2
typeCheckExpr body@(LEExpr e1 e2) = do
    ctx <- get
    innerDeriv1 <- typeCheckExpr e1
    innerDeriv2 <- typeCheckExpr e2
    let (innerCtx1, innerSyntax1, innerTy1) = getConcl innerDeriv1
        (innerCtx2, innerSyntax2, innerTy2) = getConcl innerDeriv2
    case (innerTy1, innerTy2) of
        (Dsl.Primitive (Dsl.TyInt purity1), Dsl.Primitive (Dsl.TyInt purity2)) -> return $ (CompOpRule ctx body innerDeriv1 innerDeriv2)
        (Dsl.Primitive (Dsl.TyPosix purity1), Dsl.Primitive (Dsl.TyPosix purity2)) -> return $ (CompOpRule ctx body innerDeriv1 innerDeriv2)
        _ -> typeCheckError $ "Expected integer expressions:\n\t" ++ innerSyntax1 ++ "\n\t" ++ innerSyntax2
typeCheckExpr body@(LTExpr e1 e2) = do
    ctx <- get
    innerDeriv1 <- typeCheckExpr e1
    innerDeriv2 <- typeCheckExpr e2
    let (innerCtx1, innerSyntax1, innerTy1) = getConcl innerDeriv1
        (innerCtx2, innerSyntax2, innerTy2) = getConcl innerDeriv2
    case (innerTy1, innerTy2) of
        (Dsl.Primitive (Dsl.TyInt purity1), Dsl.Primitive (Dsl.TyInt purity2)) -> return $ (CompOpRule ctx body innerDeriv1 innerDeriv2)
        (Dsl.Primitive (Dsl.TyPosix purity1), Dsl.Primitive (Dsl.TyPosix purity2)) -> return $ (CompOpRule ctx body innerDeriv1 innerDeriv2)
        _ -> typeCheckError $ "Expected integer expressions:\n\t" ++ innerSyntax1 ++ "\n\t" ++ innerSyntax2
typeCheckExpr body@(GEExpr e1 e2) = do
    ctx <- get
    innerDeriv1 <- typeCheckExpr e1
    innerDeriv2 <- typeCheckExpr e2
    let (innerCtx1, innerSyntax1, innerTy1) = getConcl innerDeriv1
        (innerCtx2, innerSyntax2, innerTy2) = getConcl innerDeriv2
    case (innerTy1, innerTy2) of
        (Dsl.Primitive (Dsl.TyInt purity1), Dsl.Primitive (Dsl.TyInt purity2)) -> return $ (CompOpRule ctx body innerDeriv1 innerDeriv2)
        (Dsl.Primitive (Dsl.TyPosix purity1), Dsl.Primitive (Dsl.TyPosix purity2)) -> return $ (CompOpRule ctx body innerDeriv1 innerDeriv2)
        _ -> typeCheckError $ "Expected integer expressions:\n\t" ++ innerSyntax1 ++ "\n\t" ++ innerSyntax2
typeCheckExpr body@(GTExpr e1 e2) = do
    ctx <- get
    innerDeriv1 <- typeCheckExpr e1
    innerDeriv2 <- typeCheckExpr e2
    let (innerCtx1, innerSyntax1, innerTy1) = getConcl innerDeriv1
        (innerCtx2, innerSyntax2, innerTy2) = getConcl innerDeriv2
    case (innerTy1, innerTy2) of
        (Dsl.Primitive (Dsl.TyInt purity1), Dsl.Primitive (Dsl.TyInt purity2)) -> return $ (CompOpRule ctx body innerDeriv1 innerDeriv2)
        (Dsl.Primitive (Dsl.TyPosix purity1), Dsl.Primitive (Dsl.TyPosix purity2)) -> return $ (CompOpRule ctx body innerDeriv1 innerDeriv2)
        _ -> typeCheckError $ "Expected integer expressions:\n\t" ++ innerSyntax1 ++ "\n\t" ++ innerSyntax2
typeCheckExpr body@(AvailMoneyExpr p t) = do
    ctx <- get
    partyDeriv <- typeCheckParty p
    tokenDeriv <- typeCheckToken t
    return $ (AccountBalRule ctx body partyDeriv tokenDeriv)
typeCheckExpr body@(ChoiceExpr p ch) = do
    ctx <- get
    partyDeriv <- typeCheckParty p
    choiceDeriv <- typeCheckChoice ch
    return $ (ChoiceExprRule ctx body partyDeriv choiceDeriv)
typeCheckExpr body@(ChosenExpr p ch) = do
    ctx <- get
    partyDeriv <- typeCheckParty p
    choiceDeriv <- typeCheckChoice ch
    return $ (ChosenExprRule ctx body partyDeriv choiceDeriv)
typeCheckExpr body@(IdentExpr ids) = do
    ctx <- get
    case chaseAbs ids ctx of
        Just (Dsl.Primitive (Dsl.TyInt purity)) -> return $ ExprUseRule ctx body ids
        Just (Dsl.Primitive (Dsl.TyBool purity)) -> return $ ExprUseRule ctx body ids
        Just (Dsl.Primitive (Dsl.TyPosix purity)) -> return $ ExprUseRule ctx body ids
        _ -> typeCheckError $ L.intercalate "." ids ++ " is not declared or is not an integer, boolean, or posix time value."
typeCheckExpr body@(CallExpr ids args) = do
    ctx <- get
    case chaseAbs ids ctx of
        Just (Dsl.Arrow (Dsl.TyFun paramTys resTy@(Dsl.TyInt purity))) -> do
            argDeriv <- typeCheckTuple paramTys args
            return $ FunRule ctx body ids argDeriv
        Just (Dsl.Arrow (Dsl.TyFun paramTys resTy@(Dsl.TyBool purity))) -> do
            argDeriv <- typeCheckTuple paramTys args
            return $ FunRule ctx body ids argDeriv
        Just (Dsl.Arrow (Dsl.TyFun paramTys resTy@(Dsl.TyPosix purity))) -> do
            argDeriv <- typeCheckTuple paramTys args
            return $ FunRule ctx body ids argDeriv
        _ -> typeCheckError $ L.intercalate "." ids ++ " is not declared or is not an integer, boolean, or posix time function."

typeCheckBound :: AbsBound -> TypeCheck TyRuleBound
typeCheckBound body@(IdentBound ids) = do
    ctx <- get
    typeCheckBasicUse ids Dsl.TyBound "bound" (BoundUseRule ctx body ids)
typeCheckBound body@(DoubleBound e1 e2) = do
    ctx <- get
    e1Deriv <- typeCheckExpr e1
    e2Deriv <- typeCheckExpr e2
    let (ctx1, e1Syntax, e1Ty) = getConcl e1Deriv
        (ctx2, e2Syntax, e2Ty) = getConcl e2Deriv
    case (e1Ty, e2Ty) of
        (Dsl.Primitive (Dsl.TyInt Dsl.TyPure), Dsl.Primitive (Dsl.TyInt Dsl.TyPure)) -> return $ RangeRule ctx body e1Deriv e2Deriv
        _ -> typeCheckError $ "Bound expressions must be pure integer expressions."
typeCheckBound body@(SingleBound e) = do
    ctx <- get
    eDeriv <- typeCheckExpr e
    let (ctx1, eSyntax, eTy) = getConcl eDeriv
    case eTy of
        (Dsl.Primitive (Dsl.TyInt Dsl.TyPure)) -> return $ SingleValueRule ctx body eDeriv
        _ -> typeCheckError $ "Bound expressions must be pure integer expressions."

typeCheckAction :: AbsAction -> TypeCheck TyRuleAction
typeCheckAction body@(DepAct p1 e t p2) = do
    ctx <- get
    p1Deriv <- typeCheckParty p1
    eDeriv <- typeCheckExpr e
    tDeriv <- typeCheckToken t
    p2Deriv <- typeCheckParty p2
    let (ctx1, eSyntax, eTy) = getConcl eDeriv
    case eTy of
        Dsl.Primitive (Dsl.TyInt tyPurity) -> return $ DepositRule ctx body p1Deriv eDeriv tDeriv p2Deriv
        _ -> typeCheckError $ "Expected integer expression for deposit action."
typeCheckAction body@(ChoosesAct p ch) = do
    ctx <- get
    pDeriv <- typeCheckParty p
    chDeriv <- typeCheckChoice ch
    return $ ChoosesRule ctx body pDeriv chDeriv
typeCheckAction body@(ChoosesNotAct p ch) = do
    ctx <- get
    pDeriv <- typeCheckParty p
    chDeriv <- typeCheckChoice ch
    return $ ChoosesNotRule ctx body pDeriv chDeriv
typeCheckAction body@(ChoosesBoundsAct p ch bs) = do
    ctx <- get
    pDeriv <- typeCheckParty p
    chDeriv <- typeCheckChoice ch
    bDerivs <- mapM (typeCheckBound) bs
    return $ ChoosesWithinRule ctx body pDeriv chDeriv bDerivs
typeCheckAction body@(NotifyAct e) = do
    ctx <- get
    eDeriv <- typeCheckExpr e
    let (ctx1, eSyntax, eTy) = getConcl eDeriv 
    case eTy of
        Dsl.Primitive (Dsl.TyBool tyPurity) -> return $ NotifyRule ctx body eDeriv
        _ -> typeCheckError $ "Expected boolean expression for notify action."
typeCheckAction body@(IdentAct ids) = do
    ctx <- get
    typeCheckBasicUse ids Dsl.TyAction "action" (ActionUseRule ctx body ids)
typeCheckAction body@(CallAct ids args) = do
    ctx <- get
    typeCheckCall ids args Dsl.TyAction "action" (ActionCallRule ctx body ids)

typeCheckCase :: AbsCase -> TypeCheck TyRuleCase
typeCheckCase body@(GuardedContractExpression ge c) = do
    ctx <- get
    geDeriv <- typeCheckGuardExpression ge
    cDeriv <- typeCheckCont c
    return $ GuardedContractExpressionCaseRule ctx body geDeriv cDeriv
typeCheckCase body@(IdentCase ids) = do
    ctx <- get
    typeCheckBasicUse ids Dsl.TyCase "case" (CaseUseRule ctx body ids)
typeCheckCase body@(CallCase ids args) = do
    ctx <- get
    typeCheckCall ids args Dsl.TyCase "case" (CaseCallRule ctx body ids)

typeCheckGuardExpression :: AbsGuardExpression -> TypeCheck TyRuleGuardedContractExpression
typeCheckGuardExpression body@(ActionGuard a) = do
    ctx <- get
    aDeriv <- typeCheckAction a
    return $ ActionGuardRule ctx body aDeriv
typeCheckGuardExpression body@(GuardStmtsGuard g stmts) = do
    ctx <- get
    gDeriv <- typeCheckGuardExpression g
    stmtsDeriv <- typeCheckStmts stmts
    return $ GuardThenStmtsRule ctx body gDeriv stmtsDeriv
typeCheckGuardExpression body@(GuardThenGuard g1 g2) = do
    ctx <- get
    g1Deriv <- typeCheckGuardExpression g1
    g2Deriv <- typeCheckGuardExpression g2
    return $ GuardThenGuardRule ctx body g1Deriv g2Deriv
typeCheckGuardExpression body@(DisjointGuard e1 e2) = typeCheckGuardOp body e1 e2
typeCheckGuardExpression body@(InterleavedGuard e1 e2) = typeCheckGuardOp body e1 e2

typeCheckGuardOp :: AbsGuardExpression -> AbsGuardExpression -> AbsGuardExpression -> TypeCheck TyRuleGuardedContractExpression
typeCheckGuardOp body e1 e2 = do
    ctx <- get
    e1Deriv <- typeCheckGuardExpression e1
    e2Deriv <- typeCheckGuardExpression e2
    return $ GuardOpRule ctx body e1Deriv e2Deriv

typeCheckBasicDecl :: String -> t1 -> (t1 -> TypeCheck t2) -> (Dsl.Context -> t2 -> b) -> TypeCheck b
typeCheckBasicDecl i declBody typeChecker derivRule = do
    ctx <- get
    case chaseAbs [i] ctx of
        Nothing -> (do
            innerDeriv <- typeChecker declBody
            put ctx
            return $ derivRule ctx innerDeriv)
        _ -> typeCheckError $ i ++ " is already declared."

typeCheckParamDecl :: String -> [AbsParam] -> t1 -> (t1 -> TypeCheck t2) -> (Dsl.Context -> t2 -> b) -> TypeCheck b
typeCheckParamDecl i ps declBody typeChecker derivRule = do
    ctx <- get
    case chaseAbs [i] ctx of
        Nothing -> (do
            if all (\(Param ty pi) -> (chaseAbs [pi] ctx) == Nothing) ps
            then (do
                let bodyCtx = Prelude.foldr (\(Param ty pi) -> \oldCtx -> insert pi ty oldCtx) ctx ps
                put bodyCtx
                innerDeriv <- typeChecker declBody
                put ctx
                return $ derivRule ctx innerDeriv)
            else typeCheckError $ L.intercalate ", " ((\(Param ty pi) -> pi) <$> (Prelude.filter (\(Param ty pi) -> (chaseAbs [pi] ctx) /= Nothing) ps)) ++ ": is already declared.") 
        _ -> typeCheckError $ i ++ " is already declared."

isInt (Dsl.Primitive (Dsl.TyInt _)) = True
isInt _ = False

isBool (Dsl.Primitive (Dsl.TyBool _)) = True
isBool _ = False

isPosix (Dsl.Primitive (Dsl.TyPosix _)) = True
isPosix _ = False

moduleExportMatch i ty ctx = case Data.Map.lookup i ctx of
    Nothing -> False
    Just ty' -> ty == ty'
        || (isInt ty && isInt ty')
        || (isBool ty && isBool ty')
        || (isPosix ty && isPosix ty')
        || (case (ty, ty') of
            (Dsl.Primitive (Dsl.TyModule expectedCtx), Dsl.Primitive (Dsl.TyModule actualCtx)) -> all (\(i, ty) -> moduleExportMatch i ty actualCtx) (toList expectedCtx)
            (Dsl.Arrow (Dsl.TyFun expectedParams (Dsl.TyModule expectedCtx)), Dsl.Arrow (Dsl.TyFun actualParams (Dsl.TyModule actualCtx))) -> 
                expectedParams == actualParams && (all (\(i, ty) -> moduleExportMatch i ty actualCtx) (toList expectedCtx))
            _ -> False) -- Module check

typeCheckDecl :: AbsDecl -> TypeCheck TyRuleDecl
typeCheckDecl body@(PartyDecl i p) = typeCheckBasicDecl i p typeCheckParty (\ctx -> PartyDeclRule ctx body i)
typeCheckDecl body@(TokenDecl i t) = typeCheckBasicDecl i t typeCheckToken (\ctx -> TokenDeclRule ctx body i)
typeCheckDecl body@(BoundDecl i b) = typeCheckBasicDecl i b typeCheckBound (\ctx -> BoundDeclRule ctx body i)
typeCheckDecl body@(ChoiceDecl i ch) = typeCheckBasicDecl i ch typeCheckChoice (\ctx -> ChoiceDeclRule ctx body i)
typeCheckDecl body@(VarDecl i e) = typeCheckBasicDecl i e typeCheckExpr (\ctx -> VarDeclRule ctx body i)
typeCheckDecl body@(ActionDecl i a) = typeCheckBasicDecl i a typeCheckAction (\ctx -> ActionDeclRule ctx body i)
typeCheckDecl body@(ContDecl i c) = typeCheckBasicDecl i c typeCheckCont (\ctx -> ContractDeclRule ctx body i)
typeCheckDecl body@(CaseDecl i c) = typeCheckBasicDecl i c typeCheckCase (\ctx -> CaseDeclRule ctx body i)
typeCheckDecl body@(StmtDecl i s) = typeCheckBasicDecl i s typeCheckStmts (\ctx -> StatementDeclRule ctx body i)
typeCheckDecl body@(ModuleDecl i moduleCtx m) = do
    ctx <- get
    innerDerivs <- typeCheckDecls m
    put ctx
    let (_, _, Dsl.Primitive (Dsl.TyModule moduleCtx')) = getConcl innerDerivs
    if all (\(i, ty) -> moduleExportMatch i ty moduleCtx') (toList moduleCtx)
    then return $ ModuleRule ctx body i moduleCtx innerDerivs
    else typeCheckError $ i ++ " attempting to export undeclared identifier and type.\nExpected: " ++ show moduleCtx ++ "\nGot: " ++ show moduleCtx'
typeCheckDecl body@(FunDecl i ps e) = typeCheckParamDecl i ps e typeCheckExpr (\ctx -> FunDeclRule ctx body i ps)
typeCheckDecl body@(ActionParamDecl i ps a) = typeCheckParamDecl i ps a typeCheckAction (\ctx -> ActionParamDeclRule ctx body i ps)
typeCheckDecl body@(ContParamDecl i ps c) = typeCheckParamDecl i ps c typeCheckCont (\ctx -> ContractParamDeclRule ctx body i ps)
typeCheckDecl body@(CaseParamDecl i ps a) = typeCheckParamDecl i ps a typeCheckCase (\ctx -> CaseParamDeclRule ctx body i ps)
typeCheckDecl body@(StmtParamDecl i ps a) = typeCheckParamDecl i ps a typeCheckStmts (\ctx -> StatementParamDeclRule ctx body i ps)
typeCheckDecl body@(ModuleParamDecl i moduleCtx ps m) = do -- TODO save moduleCtx to derivation and use it in getConcl
    ctx <- get
    let bodyCtx = Prelude.foldr (\(Param ty pi) -> \oldCtx -> insert pi ty oldCtx) ctx ps
    put bodyCtx
    innerDerivs <- typeCheckDecls m
    put ctx
    let (_, _, Dsl.Primitive (Dsl.TyModule moduleCtx')) = getConcl innerDerivs
    if all (\(i, ty) -> moduleExportMatch i ty moduleCtx') (toList moduleCtx)
    then return $ ModuleParamRule ctx body i moduleCtx ps innerDerivs
    else typeCheckError $ i ++ " attempting to export undeclared identifier and type.\nExpected: " ++ show moduleCtx ++ "\nGot: " ++ show moduleCtx'
typeCheckDecl body@(OpenDecl ids) = do
    ctx <- get
    case chaseAbs ids ctx of
        Just (Dsl.Primitive (Dsl.TyModule innerCtx)) -> return $ ModuleOpenRule ctx body ids
        _ -> createUseError ids "module"
typeCheckDecl body@(OpenArgsDecl ids args) = do
    ctx <- get
    case chaseAbs ids ctx of
        Just (Dsl.Arrow (Dsl.TyFun paramTys (Dsl.TyModule innerCtx))) -> (do
            argsDeriv <- typeCheckTuple paramTys args
            return $ ModuleOpenCallRule ctx body ids argsDeriv)
        _ -> typeCheckError $ L.intercalate "." ids ++ " has not been declared as a parameterized module, or is not called with the correct argument types."

typeCheckDecls :: [AbsDecl] -> TypeCheck [TyRuleDecl]
typeCheckDecls decls = mapM typeCheckDecl decls

typeCheckStmt :: AbsStmt -> TypeCheck TyRuleStmt
typeCheckStmt body@(PayStmt p1 (Internal p2) e t) = do
    ctx <- get
    p1Deriv <- typeCheckParty p1
    p2Deriv <- typeCheckParty p2
    eDeriv <- typeCheckExpr e
    tDeriv <- typeCheckToken t
    let (_, _, eTy) = getConcl eDeriv
    case eTy of
        (Dsl.Primitive (Dsl.TyInt _)) -> return $ PayInternalRule ctx body p1Deriv p2Deriv eDeriv tDeriv
        _ -> typeCheckError $ "Expected integer expression for pay statement."
typeCheckStmt body@(PayStmt p1 (External p2) e t) = do
    ctx <- get
    p1Deriv <- typeCheckParty p1
    p2Deriv <- typeCheckParty p2
    eDeriv <- typeCheckExpr e
    tDeriv <- typeCheckToken t
    let (_, _, eTy) = getConcl eDeriv
    case eTy of
        (Dsl.Primitive (Dsl.TyInt _)) -> return $ PayExternalRule ctx body p1Deriv p2Deriv eDeriv tDeriv
        _ -> typeCheckError $ "Expected integer expression for pay statement."
typeCheckStmt body@(AssertStmt e) = do
    ctx <- get
    eDeriv <- typeCheckExpr e
    let (_, _, eTy) = getConcl eDeriv
    case eTy of
        (Dsl.Primitive (Dsl.TyBool _)) -> return $ AssertRule ctx body eDeriv
        _ -> typeCheckError $ "Expected boolean expression for assert statement."
typeCheckStmt body@(AssignStmt ids e) = do
    ctx <- get
    eDeriv <- typeCheckExpr e
    case chaseAbs ids ctx of -- TODO dot id or single id?
        Just (Dsl.Primitive (Dsl.TyInt _)) -> return $ ReassignRule ctx body (head ids) eDeriv
        Just (Dsl.Primitive (Dsl.TyBool _)) -> return $ ReassignRule ctx body (head ids) eDeriv
        Just (Dsl.Primitive (Dsl.TyPosix _)) -> return $ ReassignRule ctx body (head ids) eDeriv
        _ -> typeCheckError $ "Expected integer, boolean, or posix expression."
typeCheckStmt body@(IdentStmt ids) = do ctx <- get; typeCheckBasicUse ids Dsl.TyStatement "statement" (StmtUseRule ctx body ids)
typeCheckStmt body@(CallStmt ids args) = do ctx <- get; typeCheckCall ids args Dsl.TyStatement "statement" (StmtCallRule ctx body ids)

typeCheckStmts :: [AbsStmt] -> TypeCheck [TyRuleStmt]
typeCheckStmts stmts = mapM typeCheckStmt stmts

typeCheckCont :: AbsContract -> TypeCheck TyRuleCont
typeCheckCont body@CloseCont = do ctx <- get; return $ CloseRule ctx body
typeCheckCont body@(DeclCont d c) = do
    ctx <- get
    dDeriv <- typeCheckDecl d
    let (_, _, Dsl.Primitive (Dsl.TyModule dCtx)) = getConcl dDeriv
    modify (\_ -> union dCtx ctx)
    cDeriv <- typeCheckCont c
    return $ DeclRule ctx body dDeriv cDeriv
typeCheckCont body@(StmtCont s c) = do
    ctx <- get
    sDeriv <- typeCheckStmt s
    cDeriv <- typeCheckCont c
    return $ StmtRule ctx body sDeriv cDeriv
typeCheckCont body@(IfElseCont e c1 c2) = do
    ctx <- get
    eDeriv <- typeCheckExpr e
    c1Deriv <- typeCheckCont c1
    c2Deriv <- typeCheckCont c2
    let (_, _, eTy) = getConcl eDeriv
    case eTy of
        (Dsl.Primitive (Dsl.TyBool _)) -> return $ IfRule ctx body eDeriv c1Deriv c2Deriv
        _ -> typeCheckError $ "Expected boolean expression in if contract."
typeCheckCont body@(WhenCont cs e timeoutCont) = do
    ctx <- get
    caseDerivs <- mapM (typeCheckCase) cs
    eDeriv <- typeCheckExpr e
    timeoutContDeriv <- typeCheckCont timeoutCont
    let (_, _, eTy) = getConcl eDeriv
    case eTy of
        (Dsl.Primitive (Dsl.TyInt Dsl.TyPure)) -> return $ WhenRule ctx body caseDerivs eDeriv timeoutContDeriv -- TODO get rid of posix times?
        _ -> typeCheckError $ "Expected pure integer expression in timeout of when contract."
typeCheckCont body@(IdentCont ids) = do ctx <- get; typeCheckBasicUse ids Dsl.TyContract "contract" (ContUseRule ctx body ids)
typeCheckCont body@(CallCont ids args) = do ctx <- get; typeCheckCall ids args Dsl.TyContract "contract" (ContCallRule ctx body ids)

fullTypeCheckCont :: AbsContract -> Either String (TyRuleCont, Dsl.Context)
fullTypeCheckCont c = runStateT (typeCheckCont c) empty

-- >>> case typeCheckExpr empty (AvailMoneyExpr (RoleParty "role") (SingleToken "")) of Right (deriv) -> show (getConcl deriv); Left error -> "Error!"
-- (Error while loading modules for evaluation)
-- [7 of 9] Compiling Language.Faustus.Compiler ( /workspaces/faustus/src/Language/Faustus/Compiler.hs, interpreted )
-- <BLANKLINE>
-- /workspaces/faustus/src/Language/Faustus/Compiler.hs:422:14-24: error:
--     Not in scope: data constructor `DeclSeqRule'
--     Perhaps you meant `DeclRule' (imported from Language.Faustus.TypeChecker)
-- Failed, six modules loaded.
--

-- >>> case typeCheckExpr empty (NegExpr (IntExpr 10)) of Right (deriv) -> show (getConcl deriv); Left error -> "Error!"
-- "(fromList [],\"-( 10 )\",Primitive (TyInt TyPure))"
--

-- >>> case typeCheckExpr empty (AndExpr (IntExpr 10) (TrueExpr)) of Right (deriv) -> show (getConcl deriv); Left error -> error
-- "Expected boolean expressions: \n\t10\n\ttrue"
--

-- >>> case typeCheckExpr empty (CondExpr (OrExpr TrueExpr FalseExpr) (IntExpr 10) (StartExpr)) of Right (deriv) -> show ((\(a,b,c) -> c) . getConcl $ deriv); Left error -> error
-- "Primitive (TyInt TyImpure)"
--

-- >>> case typeCheckExpr empty (AndExpr (FalseExpr) (TrueExpr)) of Right (deriv) -> show (getConcl deriv); Left error -> error
-- "(fromList [],\"( false ) && ( true )\",Primitive (TyBool TyPure))"
--
