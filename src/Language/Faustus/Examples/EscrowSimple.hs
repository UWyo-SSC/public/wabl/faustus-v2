{-# LANGUAGE OverloadedStrings #-}
module Language.Faustus.Examples.EscrowSimple where

-- import           Language.Faustus
-- import           Language.Marlowe (Bound(..), TransactionInput(..), Input(..))
-- import qualified Language.Marlowe as M

-- {- What does the vanilla contract look like?
--    Using layout for scoping here

-- When
--   bobClaims
--     when
--       carolAgrees
--         Pay "alice" "bob" price
--       carolDisagrees
--         Close "alice"
--   aliceClaims
--     when
--       carolAgrees
--         Close "alice"
--       carolDisagrees
--         Pay "alice" "bob" price

-- -}

-- contract :: Contract
-- contract = defineProcessClaim $ When [Case (Deposit ("alice") ("alice") M.ada price)
--                       (When [ Case bobClaims (UseC (Identifier "processClaim") [PartyArgument "bob", PartyArgument "alice"])
--                             , Case aliceClaims (UseC (Identifier "processClaim") [PartyArgument "alice", PartyArgument "bob"])
--                             ]
--                             90
--                             Close)
--                 ]
--                 10
--                 Close

-- defineProcessClaim = LetC (Identifier "processClaim") [PartyParameter . Identifier $ "claimant", PartyParameter . Identifier $ "nonclaimant"]
--   (When 
--     [Case carolAgrees (Pay (UseParty . Identifier $ "nonclaimant") (Party . UseParty . Identifier $ "claimant") M.ada price Close)
--     , Case carolDisagrees (Pay (UseParty . Identifier $ "claimant") (Party . UseParty . Identifier $ "nonclaimant") M.ada price Close)] 100 Close)

-- aliceClaims, bobClaims, carolAgrees, carolDisagrees :: Action

-- aliceClaims    = Choice (ChoiceId "claim" "alice") [Bound 0 0]
-- bobClaims      = Choice (ChoiceId "claim" "bob")   [Bound 0 0]
-- carolAgrees    = Choice (ChoiceId "agree" "carol") [Bound 0 0]
-- carolDisagrees = Choice (ChoiceId "agree" "carol") [Bound 1 1]


-- -- Value under escrow
-- price :: Value
-- price = Constant 450

-- runBobClaimCarolAgree = playTrace 0 contract [inputs]
--   where inputs = TransactionInput {txInterval = M.TimeInterval 0 0, txInputs = map M.NormalInput [M.IDeposit "alice" "alice" M.ada 450, M.IChoice (M.ChoiceId "claim" "alice") 0, M.IChoice (M.ChoiceId "agree" "carol") 1]}

-- testShow = (read . show $ contract) :: Contract
