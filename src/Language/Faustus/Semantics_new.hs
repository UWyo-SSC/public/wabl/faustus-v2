{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# OPTIONS_GHC -Wno-name-shadowing -Wno-orphans #-}
module Language.Faustus.Semantics where

import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import           Data.Text (Text)
import qualified Data.Text as T
import           Data.Ratio
import           Data.String
import GHC.Generics (Generic)
import Language.Marlowe.Pretty (Pretty, prettyFragment)
import           Text.PrettyPrint.Leijen (text, encloseSep, lbracket, rbracket, space, comma)
import qualified Language.Marlowe.Semantics as MS
-- import           Data.List.Tools (dropUntil)

dropUntil p [] = []
dropUntil p xs@(y:ys) = if p y then xs else dropUntil p ys

  
newtype Identifier = Identifier Text
  deriving stock (Eq,Ord,Generic)

instance Pretty Identifier where
  prettyFragment = text . show

instance Show Identifier where
  showsPrec p (Identifier txt) r = showsPrec p (T.unpack txt) r
instance Read Identifier where
  readsPrec p x = [(Identifier (T.pack v), s) | (v, s) <- readsPrec p x]

instance IsString Identifier where
    fromString s = Identifier (T.pack s)

data PubKey = ConstantPubKey Text
    | UsePubKey Identifier
    deriving (Eq,Ord,Show,Read,Generic)

instance Pretty PubKey where
  prettyFragment = text . (\x -> "(" ++ x ++ ")") . show

type Party = PubKey

data ChoiceId = ChoiceId MS.ChoiceName Party
  deriving (Eq,Ord,Show,Read,Generic,Pretty)

data Value = AvailableMoney Party
            | Constant Integer
            | NegValue Value
            | AddValue Value Value
            | SubValue Value Value
            | MulValue Value Value
            | Scale Rational Value
            | ChoiceValue ChoiceId
            | SlotIntervalStart
            | SlotIntervalEnd
            | UseValue Identifier
            | Cond Observation Value Value
  deriving (Eq,Ord,Show,Read,Generic,Pretty)

data Observation = AndObs Observation Observation
                  | OrObs Observation Observation
                  | NotObs Observation
                  | ChoseSomething ChoiceId
                  | ValueGE Value Value
                  | ValueGT Value Value
                  | ValueLT Value Value
                  | ValueLE Value Value
                  | ValueEQ Value Value
                  | UseObservation Identifier
                  | TrueObs
                  | FalseObs
  deriving (Eq,Ord,Show,Read,Generic,Pretty)

data Action = Deposit Party Party Value
            | Choice ChoiceId [MS.Bound]
            | Notify Observation
            | UseAction Identifier
  deriving (Eq,Ord,Show,Read,Generic,Pretty)

data Payee = Account Party
            | Party Party
  deriving (Eq,Ord,Show,Read,Generic,Pretty)

data Parameter = ValueParameter Identifier
               | ObservationParameter Identifier
               | PubKeyParameter Identifier
               | ContractParameter Identifier
               | CaseParameter Identifier
               | ActionParameter Identifier
  deriving (Eq,Ord,Show,Read,Generic)

instance Pretty Parameter where
  prettyFragment = text . (\x -> "(" ++ x ++ ")") . show

data Argument = ValueArgument Value
              | ObservationArgument Observation
              | PubKeyArgument PubKey
              | ContractArgument Contract
              | CaseArgument Case
              | ActionArgument Action -- Deposit | Choice | Notify
  deriving (Eq,Ord,Show,Read,Generic)

instance Pretty Argument where
  prettyFragment = text . (\x -> "(" ++ x ++ ")") . show

data Case = Case Action Contract
          | UseCase Identifier Contract  
  deriving (Eq,Ord,Show,Read,Generic,Pretty)

data Contract = Close
              | Pay Party Payee Value Contract
              | If Observation Contract Contract
              | When [Case] MS.Timeout Contract
              | Let Identifier Value Contract
              | LetObservation Identifier Observation Contract
              | LetPubKey Identifier PubKey Contract
              | Assert Observation Contract
              | LetC Identifier [Parameter] Contract Contract
              | UseC Identifier [Argument]
              | LetCase Identifier Case Contract
              | LetAction Identifier Action Contract
  deriving (Eq,Ord,Show,Read,Generic,Pretty)

-- data AbstractionInformation = ContractAbstraction Identifier [Parameter] Contract
--    deriving (Eq,Ord,Show,Read)

data State = State { accounts       :: Map MS.Party MS.Money
                   , choices        :: Map MS.ChoiceId MS.ChosenNum
                   , boundValues    :: Map Identifier Integer
                   , boundPubKeys   :: Map Identifier MS.PubKey
                   , boundContracts :: [(Identifier, (Contract, [Parameter]))]
                   , boundCases     :: Map Identifier Case
                   , boundActions    :: Map Identifier Action
                   , minSlot        :: MS.Slot }
  deriving (Eq,Ord,Show,Read)

emptyState :: MS.Slot -> State
emptyState sn = State { accounts = Map.empty
                      , choices = Map.empty
                      , boundValues = Map.empty
                      , boundPubKeys = Map.empty
                      , boundContracts = []
                      , boundCases = Map.empty
                      , boundActions = Map.empty
                      , minSlot = sn }

-- EVALUATION

-- | Evaluate a @PubKey@ to @MS.PubKey@
evalPubKey :: State -> PubKey -> MS.PubKey
evalPubKey state (ConstantPubKey pk) = MS.PubKey pk
evalPubKey state (UsePubKey pkId) = Map.findWithDefault (MS.PubKey . T.pack $ "") pkId (boundPubKeys state)

-- | Evaluate a @Payee@ to @MS.Payee@
evalPayee :: State -> Payee -> MS.Payee
evalPayee state (Account acc) = MS.Account $ evalPubKey state acc
evalPayee state (Party pk) = MS.Party $ evalPubKey state pk

-- | Evaluate a @ChoiceId@ to @MS.ChoiceId@
evalChoiceId :: State -> ChoiceId -> MS.ChoiceId
evalChoiceId state (ChoiceId cname pk) = MS.ChoiceId cname (evalPubKey state pk)

-- | Evaluate a @Value@ to Integer
evalValue :: MS.Environment -> State -> Value -> Integer
evalValue env state value = let
    eval = evalValue env state
    in case value of
        AvailableMoney accId -> let
            balance = Map.findWithDefault (MS.Lovelace 0) (evalPubKey state accId) (accounts state)
            in MS.getLovelace balance
        Constant integer     -> integer
        NegValue val         -> negate (eval val)
        AddValue lhs rhs     -> eval lhs + eval rhs
        SubValue lhs rhs     -> eval lhs - eval rhs
        MulValue lhs rhs     -> eval lhs * eval rhs
        Scale s rhs          -> let (n, d) = (numerator s, denominator s) in
                                let nn = eval rhs * n in
                                let (q, r) = nn `quotRem` d in
                                if abs r * 2 < abs d then q else q + signum nn * signum d
        ChoiceValue choiceId ->
            Map.findWithDefault 0 (evalChoiceId state choiceId) (choices state)
        SlotIntervalStart    -> (MS.getSlot . MS.ivFrom . MS.slotInterval) env
        SlotIntervalEnd      -> (MS.getSlot . MS.ivTo . MS.slotInterval) env
        UseValue valId       -> Map.findWithDefault 0 valId (boundValues state)
        Cond cond thn els    -> if evalObservation env state cond then eval thn else eval els

-- | Evaluate an @Observation@ to Bool
evalObservation :: MS.Environment -> State -> Observation -> Bool
evalObservation env state obs = let
    evalObs = evalObservation env state
    evalVal = evalValue env state
    in case obs of
        AndObs lhs rhs          -> evalObs lhs && evalObs rhs
        OrObs lhs rhs           -> evalObs lhs || evalObs rhs
        NotObs subObs           -> not (evalObs subObs)
        ChoseSomething choiceId -> (evalChoiceId state choiceId) `Map.member` choices state
        ValueGE lhs rhs         -> evalVal lhs >= evalVal rhs
        ValueGT lhs rhs         -> evalVal lhs > evalVal rhs
        ValueLT lhs rhs         -> evalVal lhs < evalVal rhs
        ValueLE lhs rhs         -> evalVal lhs <= evalVal rhs
        ValueEQ lhs rhs         -> evalVal lhs == evalVal rhs
        UseObservation obsId    -> (Map.findWithDefault 0 obsId (boundValues state)) > 0
        TrueObs                 -> True
        FalseObs                -> False

-- | Evaluate @[Argument]@ to @State@
evalArguments :: MS.Environment -> State -> [Parameter] -> [Argument] -> State
evalArguments env state [] [] = state
evalArguments env state (ValueParameter valId:restParams) (ValueArgument val:restArgs) = let
    evaluatedValue = evalValue env state val
    boundVals = boundValues state
    newState = state { boundValues = Map.insert valId evaluatedValue boundVals }
    in evalArguments env newState restParams restArgs
evalArguments env state (ObservationParameter obsId:restParams) (ObservationArgument obs:restArgs) = let
    evaluatedValue = if evalObservation env state obs then 1 else 0
    boundVals = boundValues state
    newState = state { boundValues = Map.insert obsId evaluatedValue boundVals }
    in evalArguments env newState restParams restArgs
evalArguments env state (PubKeyParameter pkId:restParams) (PubKeyArgument pk:restArgs) = let
    evaluatedPubKey = evalPubKey state pk
    boundPks = boundPubKeys state
    newState = state { boundPubKeys = Map.insert pkId evaluatedPubKey boundPks }
    in evalArguments env newState restParams restArgs
evalArguments env state (CaseParameter id : restParams) (CaseArgument  caseArg : restArgs) = let
     newState = state { boundCases = Map.insert id caseArg (boundCases state) }
    in evalArguments env newState restParams restArgs

evalArguments env state (ActionParameter id : restParams) (ActionArgument  actionArg : restArgs) = let
     newState = state { boundActions = Map.insert id actionArg (boundActions state) }
    in evalArguments env newState restParams restArgs       
  
evalArguments env state (firstParam:restParams) (firstArgument:restArgs) = evalArguments env state restParams restArgs
evalArguments env state [] (firstArgument:restArgs) = state
evalArguments env state (firstParam:restParams) [] = state

-- Lookup a @Identifier@ in boundContracts
-- DANGER! / WARNING!  This is only guaranteed to work on typecheckedContracts
lookupContractIdInformation :: State -> Identifier -> Maybe (State, (Contract, [Parameter]))
lookupContractIdInformation state cId  = let
  boundContracts' = dropWhile (\(id,(_,_)) -> id /= cId) (boundContracts state)
  entry = snd . head $ boundContracts'
  state' = state {boundContracts = tail boundContracts'}
    in Just (state', entry)

-- REDUCE

data ReduceWarning = ReduceNoWarning
                   | ReduceNonPositivePay MS.Party MS.Payee Integer
                   | ReducePartialPay MS.Party MS.Payee MS.Money MS.Money
                                     -- ^ src    ^ dest ^ paid ^ expected
                   | ReduceValueShadowing Identifier Integer Integer
                                     -- oldVal ^  newVal ^
                   | ReduceObservationShadowing Identifier Integer Integer
                   | ReducePubKeyShadowing Identifier MS.PubKey MS.PubKey
                   | ReduceCaseShadowing Identifier Case
                   | ReduceActionShadowing Identifier Action
                   | ReduceAssertionFailed
  deriving (Eq,Ord,Show,Read)


data ReduceStepResult = Reduced ReduceWarning MS.ReduceEffect  State Contract
                      | NotReduced
                      | AmbiguousSlotIntervalReductionError
                      | UnboundIdentifierError
  deriving (Eq,Ord,Show,Read)

-- | Carry a step of the contract with no inputs
reduceContractStep :: MS.Environment -> State -> Contract -> ReduceStepResult
reduceContractStep env state contract = case contract of

    Close -> case MS.refundOne (accounts state) of
        Just ((party, money), newAccounts) -> let
            newState = state { accounts = newAccounts }
            in Reduced ReduceNoWarning (MS.ReduceWithPayment (MS.Payment party money)) newState Close
        Nothing -> NotReduced
    Pay accId payee val cont -> let
        amountToPay = evalValue env state val
        evaluatedAccId = evalPubKey state accId
        evaluatedPayee = evalPayee state payee
        in  if amountToPay <= 0
            then Reduced (ReduceNonPositivePay evaluatedAccId evaluatedPayee amountToPay) MS.ReduceNoPayment  state cont
            else let
                balance    = MS.moneyInAccount evaluatedAccId (accounts state) -- always positive
                moneyToPay = MS.Lovelace amountToPay -- always positive
                paidMoney  = min balance moneyToPay -- always positive
                newBalance = balance - paidMoney -- always positive
                newAccs    = MS.updateMoneyInAccount evaluatedAccId newBalance (accounts state)
                warning = if paidMoney < moneyToPay
                          then ReducePartialPay evaluatedAccId evaluatedPayee paidMoney moneyToPay
                          else ReduceNoWarning
                (payment, finalAccs) = MS.giveMoney evaluatedPayee paidMoney newAccs
                in Reduced warning payment (state { accounts = finalAccs }) cont

    If obs cont1 cont2 -> let
        cont = if evalObservation env state obs then cont1 else cont2
        in Reduced ReduceNoWarning MS.ReduceNoPayment state cont

    When _ timeout cont -> let
        startSlot = MS.ivFrom (MS.slotInterval env)
        endSlot   = MS.ivTo (MS.slotInterval env)
        -- if timeout in future – do not reduce
        in if | endSlot < timeout     -> NotReduced   
        -- if timeout in the past – reduce to timeout continuation
              | timeout <= startSlot  -> Reduced ReduceNoWarning MS.ReduceNoPayment state cont
        -- if timeout in the slot range – issue an ambiguity error
              | otherwise             ->AmbiguousSlotIntervalReductionError

    Let valId val cont -> let
        evaluatedValue = evalValue env state val
        boundVals = boundValues state
        newState = state { boundValues = Map.insert valId evaluatedValue boundVals }
        warn = case Map.lookup valId boundVals of
              Just oldVal -> ReduceValueShadowing valId oldVal evaluatedValue
              Nothing     -> ReduceNoWarning
        in Reduced warn MS.ReduceNoPayment newState cont

    LetObservation obsId obs cont -> let
        evaluatedValue = if evalObservation env state obs then 1 else 0
        boundVals = boundValues state
        newState = state { boundValues = Map.insert obsId evaluatedValue boundVals }
        warn = case Map.lookup obsId boundVals of
              Just oldVal -> ReduceObservationShadowing obsId oldVal evaluatedValue
              Nothing     -> ReduceNoWarning
        in Reduced warn MS.ReduceNoPayment newState cont

    LetPubKey pkId pk cont -> let
        evaluatedPubKey = evalPubKey state pk
        boundPks = boundPubKeys state
        newState = state { boundPubKeys = Map.insert pkId evaluatedPubKey boundPks }
        warn = case Map.lookup pkId boundPks of
              Just oldPk -> ReducePubKeyShadowing pkId oldPk evaluatedPubKey
              Nothing    -> ReduceNoWarning
        in Reduced warn MS.ReduceNoPayment newState cont

    Assert obs cont -> let
        warning = if evalObservation env state obs
                  then ReduceNoWarning
                  else ReduceAssertionFailed
        in Reduced warning MS.ReduceNoPayment  state cont

    LetC cId params body cont -> let
        newState = state{boundContracts = (cId, (body, params)):(boundContracts state)}
        in Reduced ReduceNoWarning MS.ReduceNoPayment newState cont

    UseC cId args -> case lookupContractIdInformation state cId of
        Nothing -> UnboundIdentifierError
        Just (newState,(body,params)) -> let
            bodyState = evalArguments env newState params args
            in Reduced ReduceNoWarning MS.ReduceNoPayment bodyState body
    LetCase cId faustusCase cont -> let
        newState = state { boundCases = Map.insert cId faustusCase (boundCases state) }
        warn = case Map.lookup cId (boundCases state)  of
              Just oldCase -> ReduceCaseShadowing cId oldCase 
              Nothing    -> ReduceNoWarning
        in Reduced warn MS.ReduceNoPayment newState cont
    LetAction id action cont -> let
        newState = state { boundActions = Map.insert id action (boundActions state) }
        warn = case Map.lookup id (boundActions state)  of
              Just oldAction -> ReduceActionShadowing id oldAction 
              Nothing    -> ReduceNoWarning
        in Reduced warn MS.ReduceNoPayment newState cont

data ReduceResult = ContractQuiescent [ReduceWarning] [MS.Payment] State Contract
                  | RRAmbiguousSlotIntervalError
                  | RRUnboundIdentifierError
  deriving (Eq,Ord,Show,Read)

-- | Reduce a contract until it cannot be reduced more
reduceContractUntilQuiescent :: MS.Environment -> State -> Contract -> ReduceResult
reduceContractUntilQuiescent env state contract = let
    reductionLoop
      :: MS.Environment -> State -> Contract -> [ReduceWarning] -> [MS.Payment] -> ReduceResult
    reductionLoop env state contract warnings payments =
        case reduceContractStep env state contract of
            Reduced warning effect newState cont -> let
                newWarnings = if warning == ReduceNoWarning then warnings
                              else warning : warnings
                newPayments = case effect of
                    MS.ReduceWithPayment payment -> payment : payments
                    MS.ReduceNoPayment           -> payments
                in reductionLoop env newState cont newWarnings newPayments
            AmbiguousSlotIntervalReductionError -> RRAmbiguousSlotIntervalError
            UnboundIdentifierError -> RRUnboundIdentifierError
            -- this is the last invocation of reductionLoop, so we can reverse lists
            NotReduced -> ContractQuiescent (reverse warnings) (reverse payments) state contract
    in reductionLoop env state contract [] []

data ApplyResult = Applied MS.ApplyWarning State Contract
                 | ApplyNoMatchError
                 | ApplyNoIdFound String
  deriving (Eq,Ord,Show,Read)

-- | Apply a single Input to the contract (assumes the contract is reduced)
applyCases :: MS.Environment -> State -> MS.Input -> [Case] -> ApplyResult
applyCases env state input cases = case (input, cases) of
    (MS.IDeposit accId1 party1 money, Case (Deposit accId2 party2 val) cont : rest) -> let
        amount = evalValue env state val
        evaluatedAccId2 = evalPubKey state accId2
        evaluatedParty2 = evalPubKey state party2
        warning = if amount > 0
                  then MS.ApplyNoWarning
                  else MS.ApplyNonPositiveDeposit party1 evaluatedAccId2 amount
        newState = state { accounts = MS.addMoneyToAccount accId1 money (accounts state) }
        in if accId1 == evaluatedAccId2 && party1 == evaluatedParty2 && MS.getLovelace money == amount
        then Applied warning newState cont
        else applyCases env state input rest
    (MS.IChoice choId1 choice, Case (Choice choId2 bounds) cont : rest) -> let
        newState = state { choices = Map.insert choId1 choice (choices state) }
        evaluatedChoiceId2 = evalChoiceId state choId2
        in if choId1 == evaluatedChoiceId2 && MS.inBounds choice bounds
        then Applied MS.ApplyNoWarning newState cont
        else applyCases env state input rest
    (MS.INotify, Case (Notify obs) cont : _) | evalObservation env state obs -> Applied MS.ApplyNoWarning state cont
    (_, Case (UseAction id) cont : rest) ->
      case Map.lookup id (boundActions state) of
        Just action -> applyCases env state input (Case action cont : rest)
        Nothing -> ApplyNoIdFound (show id)
    (_, (UseCase id contract) : rest) ->
      case Map.lookup id (boundCases state) of
        Just faustusCase -> applyCases env state input (faustusCase : rest)
        Nothing -> ApplyNoIdFound (show id)
    (_, _ : rest) -> applyCases env state input rest
    (_, [])       -> ApplyNoMatchError


applyInput :: MS.Environment -> State -> MS.Input -> Contract -> ApplyResult
applyInput env state input (When cases _ _) = applyCases env state input cases
applyInput _ _ _ _                          = ApplyNoMatchError

-- APPLY ALL

data TransactionWarning = TransactionNonPositiveDeposit MS.Party MS.Party Integer
                        | TransactionNonPositivePay MS.Party MS.Payee Integer
                        | TransactionPartialPay MS.Party MS.Payee MS.Money MS.Money
                                                -- ^ src    ^ dest ^ paid ^ expected
                        | TransactionValueShadowing Identifier Integer Integer
                                                -- oldVal ^  newVal ^
                        | TransactionObservationShadowing Identifier Integer Integer
                        | TransactionPubKeyShadowing Identifier MS.PubKey MS.PubKey
                        | TransactionAssertionFailed
  deriving (Eq,Ord,Show,Read)

convertReduceWarnings :: [ReduceWarning] -> [TransactionWarning]
convertReduceWarnings [] = []
convertReduceWarnings (first:rest) =
  (case first of
    ReduceNoWarning -> []
    ReduceNonPositivePay accId payee amount ->
            [TransactionNonPositivePay accId payee amount]
    ReducePartialPay accId payee paid expected ->
            [TransactionPartialPay accId payee paid expected]
    ReduceValueShadowing valId oldVal newVal ->
            [TransactionValueShadowing valId oldVal newVal]
    ReduceObservationShadowing valId oldVal newVal ->
            [TransactionObservationShadowing valId oldVal newVal]
    ReducePubKeyShadowing pkId oldPk newPk ->
            [TransactionPubKeyShadowing pkId oldPk newPk]
    ReduceAssertionFailed ->
            [TransactionAssertionFailed])
  ++ convertReduceWarnings rest

convertApplyWarning :: MS.ApplyWarning -> [TransactionWarning]
convertApplyWarning warn =
  case warn of
    MS.ApplyNoWarning -> []
    MS.ApplyNonPositiveDeposit party accId amount ->
            [TransactionNonPositiveDeposit party accId amount]

data ApplyAllResult = ApplyAllSuccess [TransactionWarning] [MS.Payment] State Contract
                    | ApplyAllNoMatchError
                    | ApplyAllUnboundIdentifierError
                    | ApplyAllAmbiguousSlotIntervalError
  deriving (Eq,Ord,Show)

-- | Apply a list of Inputs to the contract
applyAllInputs :: MS.Environment -> State -> Contract -> [MS.Input] -> ApplyAllResult
applyAllInputs env state contract inputs = let
    applyAllLoop
        :: MS.Environment
        -> State
        -> Contract
        -> [MS.Input]
        -> [TransactionWarning]
        -> [MS.Payment]
        -> ApplyAllResult
    applyAllLoop env state contract inputs warnings payments =
        case reduceContractUntilQuiescent env state contract of
            RRAmbiguousSlotIntervalError -> ApplyAllAmbiguousSlotIntervalError
            RRUnboundIdentifierError -> ApplyAllUnboundIdentifierError
            ContractQuiescent reduceWarns pays curState cont -> case inputs of
                [] -> ApplyAllSuccess (warnings ++ (convertReduceWarnings reduceWarns))
                                                    (payments ++ pays) curState cont
                (input : rest) -> case applyInput env curState input cont of
                    Applied applyWarn newState cont ->
                        applyAllLoop env newState cont rest
                                      (warnings ++ (convertReduceWarnings reduceWarns)
                                                ++ (convertApplyWarning applyWarn))
                                      (payments ++ pays)
                    ApplyNoMatchError -> ApplyAllNoMatchError
    in applyAllLoop env state contract inputs [] []

data TransactionError = TEAmbiguousSlotIntervalError
                      | TEApplyNoMatchError
                      | TEIntervalError MS.IntervalError
                      | TEUselessTransaction
                      | TEUnboundIdentifierError
  deriving (Eq,Ord,Show,Read)

data TOR = TOR { txOutWarnings :: [TransactionWarning]
               , txOutPayments :: [MS.Payment]
               , txOutState    :: State
               , txOutContract :: Contract }
  deriving (Eq,Ord,Show,Read)

data TransactionOutput =
    TransactionOutput TOR
  | Error TransactionError
  deriving (Eq,Ord,Show,Read)

data IntervalResult = IntervalTrimmed MS.Environment State
                    | IntervalError MS.IntervalError
  deriving (Eq,Ord,Show,Read)


fixInterval :: MS.SlotInterval -> State -> IntervalResult
fixInterval interval state = let
    MS.SlotInterval low high = interval
    curMinSlot = minSlot state
    -- newLow is both new "low" and new "minSlot" (the lower bound for slotNum)
    newLow = max low curMinSlot
    -- We know high is greater or equal than newLow (prove)
    curInterval = MS.SlotInterval newLow high
    env = MS.Environment { MS.slotInterval = curInterval }
    newState = state { minSlot = newLow }
    in if high < low then IntervalError (MS.InvalidInterval interval)
    else if high < curMinSlot then IntervalError (MS.IntervalInPastError curMinSlot interval)
    else IntervalTrimmed env newState

-- | Try to compute outputs of a transaction give its input
computeTransaction :: MS.TransactionInput -> State -> Contract -> TransactionOutput
computeTransaction tx state contract = let
    inputs = MS.txInputs tx
    in case fixInterval (MS.txInterval tx) state of
        IntervalTrimmed env fixState -> case applyAllInputs env fixState contract inputs of
            ApplyAllSuccess warnings payments newState cont -> let
                in  if (contract == cont) && ((contract /= Close) || (Map.null $ accounts state))
                    then Error TEUselessTransaction
                    else TransactionOutput (TOR { txOutWarnings = warnings
                                                , txOutPayments = payments
                                                , txOutState = newState
                                                , txOutContract = cont })
            ApplyAllNoMatchError -> Error TEApplyNoMatchError
            ApplyAllAmbiguousSlotIntervalError -> Error TEAmbiguousSlotIntervalError
            ApplyAllUnboundIdentifierError -> Error TEUnboundIdentifierError
        IntervalError error -> Error (TEIntervalError error)

playTraceAux :: TOR -> [MS.TransactionInput] -> TransactionOutput
playTraceAux res [] = TransactionOutput res
playTraceAux (TOR { txOutWarnings = warnings
                  , txOutPayments = payments
                  , txOutState = state
                  , txOutContract = cont }) (h:t) =
  let transRes = computeTransaction h state cont in
  case transRes of
    TransactionOutput transResRec ->
      playTraceAux (transResRec { txOutPayments = payments ++ (txOutPayments transResRec)
                                , txOutWarnings = warnings ++ (txOutWarnings transResRec)})
                   t
    Error _ -> transRes

playTrace :: MS.Slot -> Contract -> [MS.TransactionInput] -> TransactionOutput
playTrace sl c t = playTraceAux (TOR { txOutWarnings = []
                                     , txOutPayments = []
                                     , txOutState = emptyState sl
                                     , txOutContract = c }) t

-- | Calculates an upper bound for the maximum lifespan of a contract
contractLifespanUpperBound :: Contract -> Integer
contractLifespanUpperBound contract = case contract of
    Close -> 0
    Pay _ _ _ cont -> contractLifespanUpperBound cont
    If _ contract1 contract2 ->
        max (contractLifespanUpperBound contract1) (contractLifespanUpperBound contract2)
    When cases timeout subContract -> let
        contractsLifespans = fmap (\(Case _ cont) -> contractLifespanUpperBound cont) cases
        in maximum (MS.getSlot timeout : contractLifespanUpperBound subContract : contractsLifespans)
    Let _ _ cont -> contractLifespanUpperBound cont
    LetObservation _ _ cont -> contractLifespanUpperBound cont
    LetPubKey _ _ cont -> contractLifespanUpperBound cont
    Assert _ cont -> contractLifespanUpperBound cont
    LetC _ _ body cont -> maximum [contractLifespanUpperBound body, contractLifespanUpperBound cont]
    UseC _ _ -> 0
    LetCase _ _ cont -> contractLifespanUpperBound cont
    LetAction _ _ cont -> contractLifespanUpperBound cont
