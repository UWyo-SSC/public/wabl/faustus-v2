{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Language.Faustus.Semantics.Types where

import qualified Data.Text as T
import           Data.String
import           GHC.Generics (Generic)
import           Data.Text (Text)
import           Language.Marlowe.Pretty (Pretty, prettyFragment)
import           Data.ByteString (ByteString)
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import           Text.PrettyPrint.Leijen (text)
import qualified Language.Marlowe.Semantics.Types 
  ( POSIXTime,
    ChoiceName,
    Token)
import qualified Language.Marlowe.Semantics.Types as MST
import           Data.String                      (IsString(..))
import Data.Text.Encoding (encodeUtf8)

data FaustusPurityType = TyPure | TyImpure
  deriving (Eq,Ord,Show,Read)

data FaustusPrimType = TyParty
  | TyToken
  | TyContract
  | TyCase
  | TyGuardedContractExpression
  | TyChoice
  | TyBound
  | TyAction
  | TyBool FaustusPurityType
  | TyInt FaustusPurityType
  | TyPosix FaustusPurityType
  | TyModule Context
  | TyStatement
  deriving (Eq,Ord,Show,Read)

data FaustusArrowType = TyFun [FaustusFullType] FaustusPrimType
  deriving (Eq,Ord,Show,Read)

data FaustusFullType = Primitive FaustusPrimType | Arrow FaustusArrowType
  deriving (Eq,Ord,Show,Read)

type Context = Map String FaustusFullType

chase :: [String] -> Context -> Maybe FaustusFullType
chase [] c = Nothing
chase [id] c = Map.lookup id c
chase (id:ids) c = case Map.lookup id c of
  Just (Primitive (TyModule c1)) -> chase ids c1
  _ -> Nothing
