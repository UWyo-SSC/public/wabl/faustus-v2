

{-# LANGUAGE DerivingStrategies #-}

{-# OPTIONS_GHC -Wno-name-shadowing -Wno-orphans #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Use tuple-section" #-}
module Language.Faustus.Compiler where

import qualified Language.Faustus.Semantics.Types as Dsl
import Language.Faustus.Syntax
import Language.Faustus.TypeChecker
import qualified Language.Marlowe.Semantics.Types as M
import Data.Map
import Control.Monad.State
import Data.ByteString
import qualified Data.Text as T
import Data.Text.Encoding (encodeUtf8)
import Data.Bifunctor ( Bifunctor(second, first, bimap) )
import qualified Debug.Trace as DBG

type StoreLocation = Int

data CompilerMeaning = IntFun TyRuleExpr [AbsParam] CompilerEnv
    | BoolFun TyRuleExpr [AbsParam] CompilerEnv
    | Party M.Party
    | Token M.Token
    | Val Int
    | Obs Int
    | Bound M.Bound
    | BoundFun TyRuleBound [AbsParam] CompilerEnv
    | Action M.Action
    | ActionFun TyRuleAction [AbsParam] CompilerEnv
    | Contract M.Contract
    | ContractFun TyRuleCont [AbsParam] CompilerEnv
    | Case (Integer -> TyRuleCont -> Compile [M.Case])
    | CaseFun TyRuleCase [AbsParam] CompilerEnv
    | Stmt (M.Contract -> M.Contract)
    | StmtFun [TyRuleStmt] [AbsParam] CompilerEnv
    | Choice M.ChoiceName
    | Module CompilerEnv
    | ModuleFun [TyRuleDecl] Dsl.Context [AbsParam] CompilerEnv

extractParty (Party p) = return p
extractParty _ = lift $ Left "Compiler error when trying to extract party."

extractToken (Token t) = return t
extractToken _ = lift $ Left "Compiler error when trying to extract token."

extractVal (Val v) = return v
extractVal _ = lift $ Left "Compiler error when trying to extract value."

extractIntFun (IntFun v p e) = return (v, p, e)
extractIntFun _ = lift $ Left "Compiler error when trying to extract integer function."

extractObs (Obs o) = return o
extractObs _ = lift $ Left "Compiler error when trying to extract observation."

extractBoolFun (BoolFun v p e) = return (v, p, e)
extractBoolFun _ = lift $ Left "Compiler error when trying to extract boolean function."

extractBound (Bound b) = return b
extractBound _ = lift $ Left "Compiler error when trying to extract bound."

extractAction (Action a) = return a
extractAction _ = lift $ Left "Compiler error when trying to extract action."

extractActionFun (ActionFun b p e) = return (b, p, e)
extractActionFun _ = lift $ Left "Compiler error when trying to extract action function."

extractContract (Contract c) = return c
extractContract _ = lift $ Left "Compiler error when trying to extract contract."

extractContractFun (ContractFun b p e) = return (b, p, e)
extractContractFun _ = lift $ Left "Compiler error when trying to extract contract function."

extractCase (Case c) = return c
extractCase _ = lift $ Left "Compiler error when trying to extract case."

extractCaseFun (CaseFun b p e) = return (b, p, e)
extractCaseFun _ = lift $ Left "Compiler error when trying to extract case function."

extractStmt (Stmt s) = return s
extractStmt _ = lift $ Left "Compiler error when trying to extract statement."

extractStmtFun (StmtFun b p e) = return (b, p, e)
extractStmtFun _ = lift $ Left "Compiler error when trying to extract statement function."

extractChoice (Choice c) = return c
extractChoice _ = lift $ Left "Compiler error when trying to extract choice."

extractModule (Module m) = return m
extractModule _ = lift $ Left "Compiler error when trying to extract module."

extractModuleFun (ModuleFun v ctx p e) = return (v, ctx, p, e)
extractModuleFun _ = lift $ Left "Compiler error when trying to extract module function."

type CompilerEnv = Map AbsIdentifier CompilerMeaning

emptyCompilerEnv :: CompilerEnv
emptyCompilerEnv = Data.Map.empty

singletonEnv :: AbsIdentifier -> CompilerMeaning -> CompilerEnv
singletonEnv i m = Data.Map.insert i m emptyCompilerEnv

-- | The first argument takes precedence. In other words this is left biased the same as Data.Map.union
compilerEnvUnion :: CompilerEnv -> CompilerEnv -> CompilerEnv
compilerEnvUnion m1 m2 = m1 `Data.Map.union` m2

data CompilerStorable = CompiledBoolean Bool | CompiledInteger Integer | CompiledPOSIX Integer | InlineBoolean M.Observation | InlineInteger M.Value

data DummyStore = DummyStore { maxLoc :: Int, locs :: Map Int (Maybe CompilerStorable) }

emptyStore :: DummyStore
emptyStore = DummyStore 0 Data.Map.empty

storeUnion :: DummyStore -> DummyStore -> DummyStore
storeUnion s1 s2 = DummyStore (max (maxLoc s1) (maxLoc s2)) (locs s1 `Data.Map.union` locs s2)

type Compile a = StateT (CompilerEnv, DummyStore) (Either String) a

doWithScope :: Compile a -> Compile a
doWithScope action = do
    initialEnv <- gets fst
    res <- action
    modify (\(e, s) -> (initialEnv, s))
    return res

doWithTempScope :: CompilerEnv -> Compile a -> Compile a
doWithTempScope env action = do
    initialEnv <- gets fst
    modify (\(_, s) -> (env, s))
    res <- action
    modify (\(_, s) -> (initialEnv, s))
    return res

newLoc :: Compile StoreLocation
newLoc = do
    (e, DummyStore i locs) <- get
    put (e, DummyStore (i+1) (Data.Map.insert (i+1) Nothing locs))
    return (i+1)

itot = T.pack . show

sToBs :: String -> ByteString
sToBs = encodeUtf8 . T.pack

compileBasicUse :: (CompilerMeaning -> Compile a) -> AbsDotIdentifier -> Compile a
compileBasicUse proj [] = lift $ Left "Identifier not declared."
compileBasicUse proj [i] = do
    (env, st) <- get
    case Data.Map.lookup i env of
        Just v -> proj v
        _ -> lift . Left $ "Identifier " ++ i ++ " not declared."
compileBasicUse proj (i:is) = do
    (env, st) <- get
    case Data.Map.lookup i env of
        Just (Module v) -> (do
            put (v, st)
            ret <- compileBasicUse proj is
            put (env, st)
            return ret)
        Nothing -> lift $ Left "Module not declared."
        _ -> lift $ Left "Identifier not declared as module."

compileParty :: TyRuleParty -> Compile M.Party
compileParty (RoleRule ctx p r) = return . M.Role $ sToBs r
compileParty (PubKeyRule ctx p pk) = return . M.PubKey $ sToBs pk
compileParty (PartyUseRule ctx p i) = compileBasicUse extractParty i

compileToken :: TyRuleToken -> Compile M.Token
compileToken (ConstantTokenRule ctx syntax t1 t2) = return $ M.Token (sToBs t1) (sToBs t2)
compileToken (SingleTokenRule ctx syntax t) = return $ M.Token (sToBs t) (sToBs t)
compileToken (TokenUseRule ctx syntax i) = compileBasicUse extractToken i

compileChoice :: TyRuleChoice -> Compile M.ChoiceName
compileChoice (ConstantChoiceRule ctx syntax c) = return $ sToBs c
compileChoice (ChoiceUseRule ctx syntax i) = compileBasicUse extractChoice i

-- >>> case runStateT (compileExpr (TrueRule (fromList []) TrueExpr)) (emptyCompilerEnv, Store Data.Map.empty 0) of Right (a, s) -> a; _ -> error "didn't work"
-- Left TrueObs
--
compileExpr :: TyRuleExpr -> Compile (Either M.Observation M.Value)
compileExpr (TrueRule ctx syntax) = return $ Left M.TrueObs
compileExpr (FalseRule ctx syntax) = return $ Left M.FalseObs
compileExpr (StartRule ctx syntax) = return $ Right M.TimeIntervalStart
compileExpr (EndRule ctx syntax) = return $ Right M.TimeIntervalEnd
compileExpr (IntRule ctx syntax i) = return . Right $ M.Constant i
compileExpr (PosixRule ctx syntax i) = lift . Left $ "Cannot compile posix time to value!"
compileExpr (CondRule ctx syntax condExpr trueExpr falseExpr) = do
    condCompiled <- compileExpr condExpr
    let
        (Left condObs) = condCompiled
    -- ================================
    -- Optimization
    case condObs of
        M.TrueObs -> compileExpr trueExpr
        M.FalseObs -> compileExpr falseExpr
        _ -> (do
            compiledTrueExpr <- compileExpr trueExpr
            compiledFalseExpr <- compileExpr falseExpr
            let
                (Right trueVal) = compiledTrueExpr
                (Right falseVal) = compiledFalseExpr
            return $ Right (M.Cond condObs trueVal falseVal))
    -- ================================
compileExpr (AccountBalRule ctx syntax p t) = do
    compiledParty <- compileParty p
    compiledToken <- compileToken t
    return . Right $ M.AvailableMoney compiledParty compiledToken
compileExpr (NumOpRule ctx syntax l r) = do
    lCompiled <- compileExpr l
    rCompiled <- compileExpr r
    let
        (Right lVal) = lCompiled
        (Right rVal) = rCompiled
        -- ================================
        -- Optimization
        optimization = case (lVal, rVal) of
            (M.Constant lConst, M.Constant rConst) -> (\op mOp -> M.Constant $ lConst `op` rConst)
            _  -> (\op mOp -> lVal `mOp` rVal)
        -- ================================
    case syntax of
        AddExpr _ _ -> return . Right $ optimization (+) M.AddValue
        SubExpr _ _ -> return . Right $ optimization (-) M.SubValue
        MulExpr _ _ -> return . Right $ optimization (*) M.MulValue
        DivExpr _ _ -> return . Right $ optimization div M.DivValue
        _ -> lift . Left $ "Cannot compile non numeric operator as numeric operator."
compileExpr (NegRule ctx syntax v) = do
    compiledVal <- compileExpr v
    let (Right val) = compiledVal
    -- ================================
    -- Optimization
    return . Right $ case val of
        M.Constant i -> M.Constant (- i)
        _ -> M.NegValue val
    -- ================================
compileExpr (BoolOpRule ctx syntax l r) = do
    lCompiled <- compileExpr l
    rCompiled <- compileExpr r
    let
        (Left lObs) = lCompiled
        (Left rObs) = rCompiled
        -- ================================
        -- Optimization
        optimization = case (lObs, rObs) of
            (M.TrueObs, M.TrueObs) -> (\op mOp -> if True `op` True then M.TrueObs else M.FalseObs) -- Leaving in case xor is ever added.
            (M.FalseObs, M.TrueObs) -> (\op mOp -> if False `op` True then M.TrueObs else M.FalseObs)
            (M.TrueObs, M.FalseObs) -> (\op mOp -> if True `op` False then M.TrueObs else M.FalseObs)
            (M.FalseObs, M.FalseObs) -> (\op mOp -> if False `op` False then M.TrueObs else M.FalseObs)
            _  -> (\op mOp -> lObs `mOp` rObs)
        -- ================================
    case syntax of
        AndExpr _ _ -> return . Left $ optimization (&&) M.AndObs
        OrExpr _ _ -> return . Left $ optimization (||) M.OrObs
        _ -> lift . Left $ "Cannot compile non boolean operator as boolean operator."
compileExpr (CompOpRule ctx syntax l r) = do
    lCompiled <- compileExpr l
    rCompiled <- compileExpr r
    let
        (Right lVal) = lCompiled
        (Right rVal) = rCompiled
        -- ================================
        -- Optimization
        optimization = case (lVal, rVal) of
            (M.Constant lConst, M.Constant rConst) -> (\op mOp -> if lConst `op` rConst then M.TrueObs else M.FalseObs)
            _  -> (\op mOp -> lVal `mOp` rVal)
        -- ================================
    case syntax of
        LTExpr _ _ -> return . Left $ optimization (<) M.ValueLT
        LEExpr _ _ -> return . Left $ optimization (<=) M.ValueLE
        GTExpr _ _ -> return . Left $ optimization (>) M.ValueGT
        GEExpr _ _ -> return . Left $ optimization (>=) M.ValueGE
        EqExpr _ _ -> return . Left $ optimization (==) M.ValueEQ
        NEqExpr _ _ -> return . Left $ optimization (/=) (\lVal rVal -> M.OrObs (M.ValueLT lVal rVal) (M.ValueGT lVal rVal))
        _ -> lift . Left $ "Cannot compile non comparison operator as comparison operator."
compileExpr (NotRule ctx syntax b) = do
    compiledBool <- compileExpr b
    let (Left bool) = compiledBool
    -- ================================
    -- Optimization
    return . Left $ case bool of
        M.TrueObs -> M.FalseObs
        M.FalseObs -> M.TrueObs
        _ -> M.NotObs bool
    -- ================================
compileExpr rule@(FunRule ctx syntax i args) = do
    -- Perform inline replacements
    -- Compile the rest
    -- int add (int x, int y) { x + y }; // body and params are stored in environment
    -- add (2, 3) // this expands with inline replacements for x and y.
    -- 2 + 3
    let (_, _, Dsl.Primitive resTy) = getConcl rule
    case resTy of
        Dsl.TyInt _ -> compileFunCallHelper extractIntFun
        Dsl.TyBool _ -> compileFunCallHelper extractBoolFun
        _ -> lift $ Left "Expression is not an Integer or Bool."
    where
        compileFunCallHelper lookupFn = do
            (body, params, boundEnv) <- compileBasicUse lookupFn i
            let (TupleRule tupleCtx tupleSyntax argRules) = args
            (argEnv, argSt) <- compileArgsInline params argRules
            (e, s) <- get
            put (compilerEnvUnion argEnv boundEnv, storeUnion argSt s)
            compileExpr body
compileExpr (ExprUseRule ctx syntax i) = do
    let (_, _, Dsl.Primitive resTy) = getConcl (ExprUseRule ctx syntax i)
    case resTy of
        Dsl.TyInt _ -> (do
            compiledValLoc <- compileBasicUse extractVal i
            (DummyStore maxLoc locs) <- gets snd
            let (Just curStoredVal) = Data.Map.lookup compiledValLoc locs
            case curStoredVal of
                Just (CompiledInteger compiledVal) -> return . Right $ M.Constant compiledVal
                _ -> return . Right . M.UseValue . M.ValueId . T.pack . show $ compiledValLoc)
        Dsl.TyBool _ -> (do
            compiledObsLoc <- compileBasicUse extractObs i
            (DummyStore maxLoc locs) <- gets snd
            let (Just curStoredBool) = Data.Map.lookup compiledObsLoc locs
            case curStoredBool of
                Just (CompiledBoolean compiledObs) -> return . Left $ if compiledObs then M.TrueObs else M.FalseObs
                _ -> return . Left $ M.ValueGT (M.UseValue . M.ValueId . T.pack . show $ compiledObsLoc) (M.Constant 0))
compileExpr (ChoiceExprRule ctx syntax p c) = do
    compiledParty <- compileParty p
    compiledChoice <- compileChoice c
    return . Right $ M.ChoiceValue (M.ChoiceId compiledChoice compiledParty)
compileExpr (ChosenExprRule ctx syntax p c) = do
    compiledParty <- compileParty p
    compiledChoice <- compileChoice c
    return . Left $ M.ChoseSomething (M.ChoiceId compiledChoice compiledParty)

compileBound :: TyRuleBound -> Compile M.Bound
compileBound (RangeRule ctx syntax e1 e2) = do
    lCompiled <- compileExpr e1
    rCompiled <- compileExpr e2
    let
        (Right (M.Constant lVal)) = lCompiled
        (Right (M.Constant rVal)) = rCompiled
    return $ M.Bound lVal rVal
compileBound (SingleValueRule ctx syntax e) = do
    compiledVal <- compileExpr e
    let (Right (M.Constant v)) = compiledVal
    return $ M.Bound v v
compileBound (BoundUseRule ctx syntax i) = compileBasicUse extractBound i

compileAction :: TyRuleAction -> Compile M.Action
compileAction (DepositRule ctx syntax p1 e t p2) = do
    compiledP1 <- compileParty p1
    compiledE <- compileExpr e
    compiledT <- compileToken t
    compiledP2 <- compileParty p2
    let (Right val) = compiledE
    return $ M.Deposit compiledP2 compiledP1 compiledT val
compileAction (ChoosesRule ctx syntax p c) = do
    compiledP <- compileParty p
    compiledC <- compileChoice c
    return $ M.Choice (M.ChoiceId compiledC compiledP) [M.Bound 1 1]
compileAction (ChoosesNotRule ctx syntax p c) = do
    compiledP <- compileParty p
    compiledC <- compileChoice c
    return $ M.Choice (M.ChoiceId compiledC compiledP) [M.Bound 0 0]
compileAction (ChoosesWithinRule ctx syntax p c bs) = do
    compiledP <- compileParty p
    compiledC <- compileChoice c
    compiledBs <- mapM compileBound bs
    return $ M.Choice (M.ChoiceId compiledC compiledP) compiledBs
compileAction (NotifyRule ctx syntax e) = do
    compiledE <- compileExpr e
    let (Left obs) = compiledE
    return $ M.Notify obs
compileAction (ActionCallRule ctx syntax i args) = compileFunctionCallInline i args extractActionFun compileAction
compileAction (ActionUseRule ctx syntax i) = compileBasicUse extractAction i

compileFunctionCallInline :: AbsDotIdentifier -> TyRuleTuple -> (CompilerMeaning -> Compile (t, [AbsParam], CompilerEnv)) -> (t -> Compile b) -> Compile b
compileFunctionCallInline i args boundFn compileFn = do
    let (TupleRule tupleCtx tupleSyntax argRules) = args
    (body, params, boundEnv) <- compileBasicUse boundFn i
    (argEnv, argSt) <- compileArgsInline params argRules
    modify (\(e,s) -> (compilerEnvUnion argEnv boundEnv, storeUnion argSt s))
    compileFn body

compileCont :: TyRuleCont -> Compile M.Contract
compileCont (CloseRule ctx syntax) = return M.Close
compileCont (DeclRule ctx syntax d c) = do
    (compiledD, dEnv, dStore) <- compileDecl d
    modify (bimap (compilerEnvUnion dEnv) (storeUnion dStore))
    compiledC <- compileCont c
    return $ compiledD compiledC
compileCont (StmtRule ctx syntax s c) = do
    compiledS <- compileStmt s
    compiledC <- compileCont c
    return $ compiledS compiledC
compileCont (WhenRule ctx syntax cases t tc) = do
    baseState <- get
    compiledT <- compileExpr t
    case compiledT of
        Right (M.Constant tVal) -> (do
            compiledTC <- doWithScope $ compileCont tc
            casesRes1 <- doWithScope $ compileCases cases
            casesRes2 <- mapM (\c -> doWithScope $ c tVal tc) casesRes1
            let casesRes3 = Prelude.concat casesRes2
            --casesRes <- Prelude.concat <$> (doWithScope (compileCases cases) >>= mapM (\c -> doWithScope $ c tVal tc))
            return $ M.When casesRes3 (fromInteger tVal) compiledTC)
        _ -> error "Could not compile timeout to pure value."
compileCont (IfRule ctx syntax obs trueCont falseCont) = do
    baseState <- get
    compiledObs <- compileExpr obs
    compiledTrue <- compileCont trueCont
    put baseState
    compiledFalse <- compileCont falseCont
    put baseState
    case compiledObs of
        Left M.TrueObs -> return compiledTrue
        Left M.FalseObs -> return compiledFalse
        Left obs -> return $ M.If obs compiledTrue compiledFalse
        _ -> error "Could not compile expression as observation."
compileCont (ContUseRule ctx syntax i) = compileBasicUse extractContract i
compileCont (ContCallRule ctx syntax i (TupleRule _ _ args)) = do
    (body, params, boundEnv) <- compileBasicUse extractContractFun i
    (compiledArgCont, compiledArgEnv, compiledArgSt) <- compileArgs params args
    modify (\(e, s) -> (compilerEnvUnion compiledArgEnv boundEnv, storeUnion compiledArgSt s))
    compiledBody <- compileCont body
    return $ compiledArgCont compiledBody


compileCases :: [TyRuleCase] -> Compile [Integer -> TyRuleCont -> Compile [M.Case]]
compileCases [] = return []
compileCases (c:cs) = do
    initialState <- get
    compiledC <- compileCase c
    put initialState
    compiledCs <- compileCases cs
    put initialState
    return (compiledC:compiledCs)

compileDecl :: TyRuleDecl -> Compile (M.Contract -> M.Contract, CompilerEnv, DummyStore)
compileDecl (PartyDeclRule ctx syntax i p) = do
    compiledP <- compileParty p
    return (id, Data.Map.insert i (Party compiledP) Data.Map.empty, emptyStore)
compileDecl (TokenDeclRule ctx syntax i t) = do
    compiledT <- compileToken t
    return (id, Data.Map.insert i (Token compiledT) Data.Map.empty, emptyStore)
compileDecl (BoundDeclRule ctx syntax i b) = do
    compiledB <- compileBound b
    return (id, Data.Map.insert i (Bound compiledB) Data.Map.empty, emptyStore)
compileDecl (ChoiceDeclRule ctx syntax i c) = do
    compiledC <- compileChoice c
    return (id, Data.Map.insert i (Choice compiledC) Data.Map.empty, emptyStore)
compileDecl (VarDeclRule ctx syntax i e) = do
    let (_, _, Dsl.Primitive eTy) = getConcl e
    compiledE <- compileExpr e
    loc <- newLoc
    let declStore = DummyStore loc Data.Map.empty
    case eTy of
        Dsl.TyInt _ -> do
            let (Right val) = compiledE
                declEnv = Data.Map.insert i (Val loc) Data.Map.empty
            case val of
                M.Constant i -> return (id, declEnv, declStore { locs = Data.Map.insert loc (Just . CompiledInteger $ i) Data.Map.empty })
                _ -> do
                    let valId = M.ValueId . itot $ loc
                    return (M.Let valId val, declEnv, declStore { locs = Data.Map.insert loc Nothing Data.Map.empty })
        Dsl.TyBool _ -> do
            let (Left obs) = compiledE
                declEnv = Data.Map.insert i (Obs loc) Data.Map.empty
            case obs of
                M.TrueObs -> return (id, declEnv, declStore { locs = Data.Map.insert loc (Just . CompiledBoolean $ True) Data.Map.empty })
                M.FalseObs -> return (id, declEnv, declStore { locs = Data.Map.insert loc (Just . CompiledBoolean $ False) Data.Map.empty })
                _ -> do
                    let valId = M.ValueId . itot $ loc
                    return (M.Let valId (M.Cond obs (M.Constant 1) (M.Constant 0)), declEnv, declStore { locs = Data.Map.insert loc Nothing Data.Map.empty })
compileDecl (FunDeclRule ctx syntax i p e) = do
    let (_, _, Dsl.Arrow (Dsl.TyFun _ eTy)) = getConcl e
    (curEnv, _) <- get
    case eTy of
        Dsl.TyInt _ -> return (id, Data.Map.insert i (IntFun e p curEnv) Data.Map.empty, emptyStore)
        Dsl.TyBool _ -> return (id, Data.Map.insert i (BoolFun e p curEnv) Data.Map.empty, emptyStore)
compileDecl (ActionDeclRule ctx syntax i a) = do
    compiledA <- compileAction a
    return (id, Data.Map.insert i (Action compiledA) Data.Map.empty, emptyStore)
compileDecl (ActionParamDeclRule ctx syntax i p a) = do
    (curEnv, _) <- get
    return (id, singletonEnv i (ActionFun a p curEnv), emptyStore)
compileDecl (ContractDeclRule ctx syntax i c) = do
    compiledC <- compileCont c
    return (id, Data.Map.insert i (Contract compiledC) Data.Map.empty, emptyStore)
compileDecl (ContractParamDeclRule ctx syntax i p c) = do
    (curEnv, _) <- get
    return (id, Data.Map.insert i (ContractFun c p curEnv) Data.Map.empty, emptyStore)
compileDecl (CaseDeclRule ctx syntax i c) = do
    compiledC <- compileCase c
    return (id, Data.Map.insert i (Case compiledC) Data.Map.empty, emptyStore)
compileDecl (CaseParamDeclRule ctx syntax i p c) = do
    (curEnv, _) <- get
    return (id, Data.Map.insert i (CaseFun c p curEnv) Data.Map.empty, emptyStore)
compileDecl (StatementDeclRule ctx syntax i s) = do
    compiledS <- compileStmts s
    return (id, Data.Map.insert i (Stmt compiledS) Data.Map.empty, emptyStore)
compileDecl (StatementParamDeclRule ctx syntax i p s) = do
    (curEnv, _) <- get
    return (id, Data.Map.insert i (StmtFun s p curEnv) Data.Map.empty, emptyStore)
compileDecl (ModuleRule ctx syntax i moduleCtx d) = do
    (compiledD, newEnv, declStore) <- compileDecls d
    return (compiledD, Data.Map.insert i (Module newEnv) Data.Map.empty, declStore)
compileDecl (ModuleParamRule ctx syntax i moduleCtx p d) = do
    (curEnv, _) <- get
    return (id, Data.Map.insert i (ModuleFun d moduleCtx p curEnv) Data.Map.empty, emptyStore)
compileDecl (ModuleOpenRule ctx syntax i) = do
    mEnv <- compileBasicUse extractModule i
    return (id, mEnv, emptyStore)
compileDecl (ModuleOpenCallRule ctx syntax i (TupleRule _ _ args)) = do
    (body, moduleCtx, ps, bodyEnv) <- compileBasicUse extractModuleFun i
    (argsC, argsEnv, argsSt) <- compileArgs ps args
    modify (bimap (compilerEnvUnion argsEnv) (storeUnion argsSt))
    compileDecls body

compileDecls :: [TyRuleDecl] -> Compile (M.Contract -> M.Contract, CompilerEnv, DummyStore)
compileDecls [d] = compileDecl d
compileDecls (d:ds) = do
    initialState <- get
    (compiledCont, compiledEnv, compiledStore) <- compileDecl d
    modify (bimap (compilerEnvUnion compiledEnv) (storeUnion compiledStore))
    (compiledCont2, compiledEnv2, compiledStore2) <- compileDecls ds
    put initialState
    return (compiledCont . compiledCont2, compilerEnvUnion compiledEnv2 compiledEnv, storeUnion compiledStore2 compiledStore)

compileCase :: TyRuleCase -> Compile (Integer -> TyRuleCont -> Compile [M.Case])
compileCase (GuardedContractExpressionCaseRule ctx syntax guardedContractExpression cont) = do
    ge <- compileGuardedContractExpression guardedContractExpression
    return $ ge (compileCont cont)
compileCase (CaseCallRule ctx syntax i (TupleRule _ _ args)) = do -- case myCase (int x) {deposit x into account -> pay x ...; close}
    -- when [myCase(start*100)]
    -- let x = start * 100; when [deposit x into account -> pay x ...] THIS IS WRONG
    -- when [deposit 100 * start -> let x = 100 * start; pay x ...; close]
    -- lazy and eager evaluation are the same inside of actions.
    -- myCase is an abstraction of an action contract pair.
    -- myAct (int x) = {deposit x into account}
    -- myAct(start * 50) ==> deposit start * 50 into account
    (body, params, boundEnv) <- compileBasicUse extractCaseFun i
    compileCaseHelper params args boundEnv body
compileCase (CaseUseRule ctx syntax i) = compileBasicUse extractCase i

compileCaseHelper :: [AbsParam] -> [TyRuleArg] -> CompilerEnv -> TyRuleCase -> Compile (Integer -> TyRuleCont -> Compile [M.Case])
compileCaseHelper params args boundEnv (GuardedContractExpressionCaseRule ctx syntax guardedContractExpression cont) = do
    -- TODO move all arg logic into guarded contract expression logic
    (initialCtx, initialS) <- get
    (compiledArgsInlineCtx, compiledArgsInlineSt) <- compileArgsInline params args
    modify (\(e, s) -> (initialCtx, s))
    (compiledArgsCont, compiledArgsCtx, compiledArgsSt) <- compileArgs params args
    modify (\(e, s) -> (compilerEnvUnion compiledArgsInlineCtx boundEnv, storeUnion compiledArgsInlineSt s))
    ge <- doWithScope $ compileGuardedContractExpression guardedContractExpression
    -- compiledAction <- compileAction action
    modify (\(e, s) -> (compilerEnvUnion compiledArgsCtx boundEnv, storeUnion compiledArgsSt s))
    --return $ M.Case compiledAction (compiledArgsCont compiledCont)
    return (ge (compileCont cont))
compileCaseHelper params args boundEnv (CaseCallRule ctx syntax i (TupleRule _ _ innerArgs)) = do
    (compiledArgsInlineCtx, compiledArgsInlineSt) <- compileArgsInline params args
    modify (\(e, s) -> (compilerEnvUnion compiledArgsInlineCtx boundEnv, storeUnion compiledArgsInlineSt s))
    (caseBody, innerParams, innerCtx) <- compileBasicUse extractCaseFun i
    compileCaseHelper innerParams innerArgs innerCtx caseBody
compileCaseHelper params args boundEnv (CaseUseRule ctx syntax i) = do
    (compiledArgsInlineCtx, compiledArgsInlineSt) <- compileArgsInline params args
    modify (\(e, s) -> (compilerEnvUnion compiledArgsInlineCtx boundEnv, storeUnion compiledArgsInlineSt s))
    compileBasicUse extractCase i

compileGuardedContractExpression :: TyRuleGuardedContractExpression -> Compile (Compile M.Contract -> Integer -> TyRuleCont -> Compile [M.Case])
compileGuardedContractExpression (ActionGuardRule ctx syntax action) = do
    --DBG.traceM "inside ActionGuardRule"
    compiledAction <- compileAction action
    postActionEnv <- get
    return (\cont t tc -> do
        put postActionEnv
        compiledCont <- cont
        return [M.Case compiledAction compiledCont])
compileGuardedContractExpression (GuardThenStmtsRule ctx syntax g stmts) = do
    preCompileEnv <- gets fst
    g1CompiledRes <- compileGuardedContractExpression g -- Get env from compiling guard
    preStmtEnv <- get
    s <- compileStmts stmts -- Get env from compiling stmts
    postStmtEnv <- get
    return (\cont t tc -> do
        innerPreStmtSt <- get
        put postStmtEnv
        continuation <- cont
        let fullContinuation = return $ s continuation
        put innerPreStmtSt
        g1CompiledRes fullContinuation t tc)
compileGuardedContractExpression (GuardThenGuardRule ctx syntax g1 g2) = do
    g1Compiled <- compileGuardedContractExpression g1
    stAfterG1Compiled <- get
    g2Compiled <- compileGuardedContractExpression g2
    stAfterG2Compiled <- get
    return (\cont t tc -> do
        innerPreCompiledSt <- get
        put stAfterG1Compiled
        fullG2 <- g2Compiled cont t tc
        put stAfterG1Compiled
        compiledTcBeforeG2 <- compileCont tc
        let innerWhen = M.When fullG2 (fromInteger t) compiledTcBeforeG2
        put innerPreCompiledSt
        g1Compiled (return innerWhen) t tc)
compileGuardedContractExpression (GuardOpRule ctx (DisjointGuard _ _) e1 e2) = do
    preCompiledSt <- get
    compiledE1 <- compileGuardedContractExpression e1
    postCompiledE1St <- get
    put preCompiledSt
    compiledE2 <- doWithScope $ compileGuardedContractExpression e2
    postCompiledE2St <- get
    return (\cont t tc -> do
        put preCompiledSt
        fullE1 <- compiledE1 cont t tc
        put preCompiledSt
        fullE2 <- compiledE2 cont t tc
        return $ fullE1 ++ fullE2)
compileGuardedContractExpression (GuardOpRule ctx syntax@(InterleavedGuard syntaxE1 syntaxE2) e1 e2) = do
    let interleaved = interleave syntaxE1 syntaxE2
        rules = (\ge -> runStateT (typeCheckGuardExpression ge) ctx) <$> interleaved
    if Prelude.any (\r -> case r of Left err -> True; _ -> False) rules
    then lift . Left $ "Error running interleaving of guarded contract expressions."
    else do
        --DBG.traceShowM $ (\(_, a, _) -> a) . getConcl . (\(Right (rule, s)) -> rule) <$> rules
        --DBG.traceM "======================================="
        env1 <- get
        --DBG.traceM $ "rules len: " ++ show (Prelude.length rules)
        allCompiled <- mapM (\(Right (rule, s)) -> (do
            put env1
            compileGuardedContractExpression rule)) rules
        env2 <- get
        return (\cont t tc -> do
            res <- mapM (\ge -> ge cont t tc) allCompiled
            return $ Prelude.concat res)

interleave :: AbsGuardExpression -> AbsGuardExpression -> [AbsGuardExpression]
interleave = interleaveHelper True

-- interleave [deposit] [true -> false -> pay] ==> ([deposit -> true -> false -> pay, true -> deposit -> false -> pay, true -> false -> pay -> deposit])
-- This helper priortizes the actions in ge1 first when interleaving. It is recursively called with the flip variable set to True to then priortize ge2.
interleaveHelper :: Bool -> AbsGuardExpression -> AbsGuardExpression -> [AbsGuardExpression]
interleaveHelper flip ge1 ge2 = case ge1 of
    GuardThenGuard innerG1 innerG2 -> (GuardThenGuard innerG1 <$> interleave innerG2 ge2) ++ interleaveRight ge1 ge2
    DisjointGuard innerGe1 innerGe2 -> interleaveHelper False innerGe1 ge2 ++ interleaveHelper False innerGe2 ge2 ++ interleaveRight ge1 ge2
    InterleavedGuard innerGe1 innerGe2 -> Prelude.concatMap (`interleave` ge2) (interleave innerGe1 innerGe2)
    _ -> GuardThenGuard ge1 ge2 : interleaveRight ge1 ge2
    where
        interleaveRight ge1 ge2 = if flip then interleaveHelper False ge2 ge1 else []

compileStmt :: TyRuleStmt -> Compile (M.Contract -> M.Contract)
compileStmt (PayInternalRule ctx syntax p1 p2 e t) = do
    compiledP1 <- compileParty p1
    compiledP2 <- compileParty p2
    compiledE <- compileExpr e
    compiledT <- compileToken t
    case compiledE of
        Right v -> return $ M.Pay compiledP1 (M.Account compiledP2) compiledT v
        _ -> error "Could not compile expression in pay statement as value."
compileStmt (PayExternalRule ctx syntax p1 p2 e t) = do
    compiledP1 <- compileParty p1
    compiledP2 <- compileParty p2
    compiledE <- compileExpr e
    compiledT <- compileToken t
    case compiledE of
        Right v -> return $ M.Pay compiledP1 (M.Party compiledP2) compiledT v
        _ -> error "Could not compile expression in pay statement as value."
compileStmt (AssertRule ctx syntax e) = do
    compiledE <- compileExpr e
    case compiledE of
        Left obs -> return $ M.Assert obs
        _ -> error "Could not compile expression in assert statement as observation."
compileStmt (ReassignRule ctx syntax i e) = do -- let x = 10; contract myCont (int y) = { pay x to bob from alice; close }; x := 2; myCont(x)
    let (eCtx, eSyntax, Dsl.Primitive eTy) = getConcl e
    compiledE <- compileExpr e
    (curEnv, DummyStore curMax curLocs) <- get
    case (eTy, compiledE) of
        (Dsl.TyBool p, Left compiledObs) -> (do
            boundObs <- compileBasicUse extractObs [i]
            case compiledObs of
                M.TrueObs -> do
                    modify (\(e, s) -> (e, DummyStore curMax (Data.Map.insert boundObs (Just . CompiledBoolean $ True) curLocs)))
                    return id
                M.FalseObs -> do
                    modify (\(e, s) -> (e, DummyStore curMax (Data.Map.insert boundObs (Just . CompiledBoolean $ False) curLocs)))
                    return id
                _ -> do
                    modify (\(e, s) -> (e, DummyStore curMax (Data.Map.insert boundObs Nothing curLocs)))
                    return $ M.Let (M.ValueId . itot $ boundObs) (M.Cond compiledObs (M.Constant 1) (M.Constant 0)))
        (Dsl.TyInt p, Right compiledVal) -> (do
            boundVal <- compileBasicUse extractVal [i]
            case compiledVal of
                (M.Constant constantVal) -> do
                    modify (\(e, s) -> (e, DummyStore curMax (Data.Map.insert boundVal (Just . CompiledInteger $ constantVal) curLocs)))
                    return id
                _ -> return $ M.Let (M.ValueId . itot $ boundVal) compiledVal)
compileStmt (StmtCallRule ctx syntax i (TupleRule _ _ args)) = do
    (body, params, boundCtx) <- compileBasicUse extractStmtFun i
    (compiledArgsCont, compiledArgsCtx, compiledArgsSt) <- compileArgs params args
    modify (\(e, s) -> (compilerEnvUnion compiledArgsCtx boundCtx, storeUnion compiledArgsSt s))
    compiledBody <- compileStmts body
    return (compiledArgsCont . compiledBody)
compileStmt (StmtUseRule ctx syntax i) = compileBasicUse extractStmt i


compileStmts :: [TyRuleStmt] -> Compile (M.Contract -> M.Contract)
compileStmts [] = return id
compileStmts (s:ss) = do
    stmtCompiled <- compileStmt s
    stmtsCompiled <- compileStmts ss
    return (stmtCompiled . stmtsCompiled)

compileDeclInline :: TyRuleDecl -> Compile (CompilerEnv, DummyStore)
compileDeclInline (PartyDeclRule ctx syntax i p) = (\res -> (singletonEnv i . Party $ res, emptyStore)) <$> compileParty p
compileDeclInline (TokenDeclRule ctx syntax i t) = (\res -> (singletonEnv i . Token $ res, emptyStore)) <$> compileToken t
compileDeclInline (BoundDeclRule ctx syntax i b) = (\res -> (singletonEnv i . Bound $ res, emptyStore)) <$> compileBound b
compileDeclInline (ChoiceDeclRule ctx syntax i c) = (\res -> (singletonEnv i . Choice $ res, emptyStore)) <$> compileChoice c
compileDeclInline (VarDeclRule ctx syntax i e) = do
    res <- compileExpr e
    loc <- newLoc
    let declStore = emptyStore { maxLoc = loc }
    case res of
        Left obs -> return (singletonEnv i . Obs $ loc, declStore { locs = Data.Map.insert loc (Just . InlineBoolean $ obs) Data.Map.empty })
        Right val -> return (singletonEnv i . Val $ loc, declStore { locs = Data.Map.insert loc (Just . InlineInteger $ val) Data.Map.empty })
compileDeclInline (FunDeclRule ctx syntax i ps e) = let
    (eCtx, eSyntax, eTy) = getConcl e in do
    (curEnv, curS) <- get
    case eTy of
        (Dsl.Primitive (Dsl.TyInt purity)) -> return (singletonEnv i (IntFun e ps curEnv), emptyStore)
        (Dsl.Primitive (Dsl.TyBool purity)) -> return (singletonEnv i (BoolFun e ps curEnv), emptyStore)
        _ -> error "Function of type int or bool expected."
compileDeclInline (ActionDeclRule ctx syntax i a) = (\res -> (singletonEnv i . Action $ res, emptyStore)) <$> compileAction a
compileDeclInline (ActionParamDeclRule ctx syntax i ps a) = gets ((\res -> (res, emptyStore)) . singletonEnv i . ActionFun a ps . fst)
compileDeclInline (ContractDeclRule ctx syntax i c) = (\res -> (singletonEnv i . Contract $ res, emptyStore)) <$> compileCont c
compileDeclInline (ContractParamDeclRule ctx syntax i ps c) = gets ((\res -> (res, emptyStore)) . singletonEnv i . ContractFun c ps . fst)
compileDeclInline (CaseDeclRule ctx syntax i c) = (\res -> (singletonEnv i . Case $ res, emptyStore)) <$> compileCase c
compileDeclInline (CaseParamDeclRule ctx syntax i ps c) = gets ((\res -> (res, emptyStore)) . singletonEnv i . CaseFun c ps . fst)
compileDeclInline (StatementDeclRule ctx syntax i s) = (\res -> (singletonEnv i . Stmt $ res, emptyStore)) <$> compileStmts s
compileDeclInline (StatementParamDeclRule ctx syntax i ps s) = gets ((\res -> (res, emptyStore)) . singletonEnv i . StmtFun s ps . fst)
compileDeclInline (ModuleRule ctx syntax i moduleExportCtx ds) = first (singletonEnv i . Module) <$> compileDeclsInline ds
compileDeclInline (ModuleParamRule ctx syntax i moduleExportCtx ps ds) = gets ((\res -> (res, emptyStore)) . singletonEnv i . ModuleFun ds moduleExportCtx ps . fst)
compileDeclInline (ModuleOpenRule ctx syntax ids) = (\res -> (res, emptyStore)) <$> compileBasicUse extractModule ids
compileDeclInline (ModuleOpenCallRule ctx syntax ids args@(TupleRule tCtx syntaxArgs tyRuleArgs)) = do
    (curEnv, curS) <- get
    (body, exportCtx, params, boundEnv) <- compileBasicUse extractModuleFun ids
    (argEnv, argSt) <- compileArgsInline params tyRuleArgs
    modify (\(e, s) -> (compilerEnvUnion argEnv boundEnv, storeUnion argSt s))
    resEnv <- compileDeclsInline body
    modify (\(e, s) -> (curEnv, curS))
    return resEnv

compileDeclsInline :: [TyRuleDecl] -> Compile (CompilerEnv, DummyStore)
compileDeclsInline [d] = compileDeclInline d
compileDeclsInline (d:ds) = do
    (compiledEnv, compiledStore) <- compileDeclInline d
    modify (bimap (compilerEnvUnion compiledEnv) (storeUnion compiledStore))
    (compiledEnvs, compiledStores) <- compileDeclsInline ds
    return (compilerEnvUnion compiledEnvs compiledEnv, storeUnion compiledStores compiledStore)

compileArgs :: [AbsParam] -> [TyRuleArg] -> Compile (M.Contract -> M.Contract, CompilerEnv, DummyStore)
compileArgs [] [] = return (id, emptyCompilerEnv, emptyStore)
compileArgs (p:ps) (a:as) = do
    (dec, env, st) <- compileArg p a
    (decs, envs, sts) <- compileArgs ps as
    return (\c -> dec . decs $ c, compilerEnvUnion envs env, storeUnion sts st)

pairId i = (id, i, emptyStore)

compileArg :: AbsParam -> TyRuleArg -> Compile (M.Contract -> M.Contract, CompilerEnv, DummyStore)
compileArg (Param ty i) (ContArgRule ctx syntax c) = pairId . singletonEnv i . Contract <$> compileCont c
compileArg (Param ty i) (StmtArgRule ctx syntax s) = pairId . singletonEnv i . Stmt <$> compileStmts s
compileArg (Param ty i) (ModuleArgRule ctx syntax mCtx d) = do
    (c, e, s) <- compileDecls d
    return (c, singletonEnv i . Module $ e, s)
compileArg (Param ty i) (CaseArgRule ctx syntax c) = pairId . singletonEnv i . Case <$> compileCase c
compileArg (Param ty i) (ExprArgRule ctx syntax e) = do
    loc <- newLoc
    compileRes <- compileExpr e
    let argStore = emptyStore { maxLoc = loc }
    case compileRes of
        Left M.TrueObs -> return (id, Data.Map.insert i (Obs loc) Data.Map.empty, argStore { locs = Data.Map.insert loc (Just . CompiledBoolean $ True) Data.Map.empty })
        Left M.FalseObs -> return (id, Data.Map.insert i (Obs loc) Data.Map.empty, argStore { locs = Data.Map.insert loc (Just . CompiledBoolean $ False) Data.Map.empty })
        Right (M.Constant v) -> return (id, Data.Map.insert i (Val loc) Data.Map.empty, argStore { locs = Data.Map.insert loc (Just . CompiledInteger $ v) Data.Map.empty })
        Left obs -> return (M.Let (M.ValueId . itot $ loc) (M.Cond obs (M.Constant 1) (M.Constant 0)), Data.Map.insert i (Val loc) Data.Map.empty, argStore { locs = Data.Map.insert loc Nothing Data.Map.empty })
        Right val -> return (M.Let (M.ValueId . itot $ loc) val, Data.Map.insert i (Val loc) Data.Map.empty, argStore { locs = Data.Map.insert loc Nothing Data.Map.empty })
compileArg (Param ty i) (ActionArgRule ctx syntax a) = pairId . singletonEnv i . Action <$> compileAction a
compileArg (Param ty i) (BoundArgRule ctx syntax b) = pairId . singletonEnv i . Bound <$> compileBound b
compileArg (Param ty i) (PartyArgRule ctx syntax p) = pairId . singletonEnv i . Party <$> compileParty p
compileArg (Param ty i) (ChoiceArgRule ctx syntax c) = pairId . singletonEnv i . Choice <$> compileChoice c
compileArg (Param ty i) (TokenArgRule ctx syntax t) = pairId . singletonEnv i . Token <$> compileToken t
compileArg (Param ty i) (ArrowArgRule ctx syntax oldId) = do
    let Just (Dsl.Arrow (Dsl.TyFun pTys resTy)) = chaseAbs oldId ctx
    case resTy of
        Dsl.TyContract -> renameInSingleton
        Dsl.TyStatement -> renameInSingleton
        Dsl.TyCase -> renameInSingleton
        Dsl.TyBound -> renameInSingleton
        Dsl.TyAction -> renameInSingleton
        Dsl.TyInt _ -> renameInSingleton
        Dsl.TyBool _ -> renameInSingleton
        Dsl.TyModule _ -> renameInSingleton
        _ -> lift $ Left "Cannot compile argument as function type."
        where renameInSingleton = pairId . singletonEnv i <$> compileBasicUse return oldId

compileArgHelper arg compileFn modification = do
    compileRes <- compileFn arg
    return $ modification compileRes emptyCompilerEnv

-- Compiles arguments to functions so that they will be replace inline. For use
-- with everything but contracts, statements, modules, and cases.
compileArgsInline :: [AbsParam] -> [TyRuleArg] -> Compile (CompilerEnv, DummyStore)
compileArgsInline [] [] = return (emptyCompilerEnv, emptyStore)
compileArgsInline (p:ps) (arg:args) = do
    (compilerEnv, store) <- compileArgInline p arg
    (otherCompilerEnv, otherStore) <- compileArgsInline ps args
    return $ (compilerEnvUnion otherCompilerEnv compilerEnv, storeUnion otherStore store)

-- myAction (module m (|int x|)) { deposit m.x ... }
-- myAction(module m (|int x|) {var x = start}) ==> deposit start ...
-- normally let loc_1 = start; and context says { x = Use loc_1 }
compileArgInline :: AbsParam -> TyRuleArg -> Compile (CompilerEnv, DummyStore)
compileArgInline (Param ty i) (ContArgRule ctx syntax c) = (\res -> (singletonEnv i . Contract $ res, emptyStore)) <$> compileCont c
compileArgInline (Param ty i) (StmtArgRule ctx syntax s) = (\res -> (singletonEnv i . Stmt $ res, emptyStore)) <$> compileStmts s
compileArgInline (Param ty i) (ModuleArgRule ctx syntax mCtx d) = do
    (resEnv, resStore) <- compileDeclsInline d
    return (singletonEnv i . Module $ resEnv, resStore)
compileArgInline (Param ty i) (CaseArgRule ctx syntax c) = (\res -> (singletonEnv i . Case $ res, emptyStore)) <$> compileCase c
compileArgInline (Param ty i) (ExprArgRule ctx syntax e) = do
    compileRes <- compileExpr e
    loc <- newLoc
    let argStore = emptyStore { maxLoc = loc }
    case compileRes of
        Left obs -> return (singletonEnv i . Obs $ loc, argStore { locs = Data.Map.insert loc (Just . InlineBoolean $ obs) Data.Map.empty })
        Right val -> return (singletonEnv i . Obs $ loc, argStore { locs = Data.Map.insert loc (Just . InlineInteger $ val) Data.Map.empty })
compileArgInline (Param ty i) (ActionArgRule ctx syntax a) = (\res -> (singletonEnv i . Action $ res, emptyStore)) <$> compileAction a
compileArgInline (Param ty i) (BoundArgRule ctx syntax b) = (\res -> (singletonEnv i . Bound $ res, emptyStore)) <$> compileBound b
compileArgInline (Param ty i) (PartyArgRule ctx syntax p) = (\res -> (singletonEnv i . Party $ res, emptyStore)) <$> compileParty p
compileArgInline (Param ty i) (ChoiceArgRule ctx syntax c) = (\res -> (singletonEnv i . Choice $ res, emptyStore)) <$> compileChoice c
compileArgInline (Param ty i) (TokenArgRule ctx syntax t) = (\res -> (singletonEnv i . Token $ res, emptyStore)) <$> compileToken t
compileArgInline (Param ty i) (ArrowArgRule ctx syntax oldId) = do
    let Just (Dsl.Arrow (Dsl.TyFun pTys resTy)) = chaseAbs oldId ctx
    case resTy of
        Dsl.TyContract -> renameInSingleton
        Dsl.TyStatement -> renameInSingleton
        Dsl.TyCase -> renameInSingleton
        Dsl.TyBound -> renameInSingleton
        Dsl.TyAction -> renameInSingleton
        Dsl.TyInt _ -> renameInSingleton
        Dsl.TyBool _ -> renameInSingleton
        Dsl.TyModule _ -> renameInSingleton
        _ -> lift $ Left "Cannot compile argument as function type."
        where renameInSingleton = (\res -> (singletonEnv i res, emptyStore)) <$> compileBasicUse return oldId

fullCompileCont :: TyRuleCont -> Either String (M.Contract, (CompilerEnv, DummyStore))
fullCompileCont c = runStateT (compileCont c) (emptyCompilerEnv, DummyStore 1 Data.Map.empty)


-- >>> interleave (ActionGuard (IdentAct ["one"])) (ActionGuard (IdentAct ["two"]))
-- [ActionThenGuard (IdentAct ["one"]) (ActionGuard (IdentAct ["two"])),ActionThenGuard (IdentAct ["two"]) (ActionGuard (IdentAct ["one"]))]

-- Above should match: 
-- [ActionThenGuard (IdentAct ["one"]) (ActionGuard (IdentAct ["two"])),ActionThenGuard (IdentAct ["two"]) (ActionGuard (IdentAct ["one"]))]

-- >>> interleave (InterleavedGuard (ActionGuard (IdentAct ["one"])) (ActionGuard (IdentAct ["two"]))) (ActionGuard (IdentAct ["three"]))
-- [ActionThenGuard (IdentAct ["one"]) (ActionThenGuard (IdentAct ["two"]) (ActionGuard (IdentAct ["three"]))),ActionThenGuard (IdentAct ["one"]) (ActionThenGuard (IdentAct ["three"]) (ActionGuard (IdentAct ["two"]))),ActionThenGuard (IdentAct ["three"]) (ActionThenGuard (IdentAct ["one"]) (ActionGuard (IdentAct ["two"]))),ActionThenGuard (IdentAct ["two"]) (ActionThenGuard (IdentAct ["one"]) (ActionGuard (IdentAct ["three"]))),ActionThenGuard (IdentAct ["two"]) (ActionThenGuard (IdentAct ["three"]) (ActionGuard (IdentAct ["one"]))),ActionThenGuard (IdentAct ["three"]) (ActionThenGuard (IdentAct ["two"]) (ActionGuard (IdentAct ["one"])))]

-- Above should match:
-- [ActionThenGuard (IdentAct ["one"]) (ActionThenGuard (IdentAct ["two"]) (ActionGuard (IdentAct ["three"]))),ActionThenGuard (IdentAct ["one"]) (ActionThenGuard (IdentAct ["three"]) (ActionGuard (IdentAct ["two"]))),ActionThenGuard (IdentAct ["three"]) (ActionThenGuard (IdentAct ["one"]) (ActionGuard (IdentAct ["two"]))),ActionThenGuard (IdentAct ["two"]) (ActionThenGuard (IdentAct ["one"]) (ActionGuard (IdentAct ["three"]))),ActionThenGuard (IdentAct ["two"]) (ActionThenGuard (IdentAct ["three"]) (ActionGuard (IdentAct ["one"]))),ActionThenGuard (IdentAct ["three"]) (ActionThenGuard (IdentAct ["two"]) (ActionGuard (IdentAct ["one"])))]

-- >>> interleave (DisjointGuard (ActionGuard (IdentAct ["one"])) (ActionGuard (IdentAct ["two"]))) (ActionGuard (IdentAct ["three"]))
-- [ActionThenGuard (IdentAct ["one"]) (ActionGuard (IdentAct ["three"])),ActionThenGuard (IdentAct ["three"]) (ActionGuard (IdentAct ["one"])),ActionThenGuard (IdentAct ["two"]) (ActionGuard (IdentAct ["three"])),ActionThenGuard (IdentAct ["three"]) (ActionGuard (IdentAct ["two"]))]

-- Above should match:
-- [ActionThenGuard (IdentAct ["one"]) (ActionGuard (IdentAct ["three"])),ActionThenGuard (IdentAct ["three"]) (ActionGuard (IdentAct ["one"])),ActionThenGuard (IdentAct ["two"]) (ActionGuard (IdentAct ["three"])),ActionThenGuard (IdentAct ["three"]) (ActionGuard (IdentAct ["two"]))]

