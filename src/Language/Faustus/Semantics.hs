{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE StandaloneKindSignatures #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs #-}
{-# OPTIONS_GHC -Wno-name-shadowing -Wno-orphans  #-}
{-# LANGUAGE KindSignatures #-}
-- {-# LANGUAGE ImpredicativeTypes  #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneKindSignatures #-}
-- {-# LANGUAGE TypeFamilies #-}   -- subsumed by TypeFamilyDependencies
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilyDependencies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE RoleAnnotations  #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE UndecidableSuperClasses  #-}

module Language.Faustus.Semantics where

-- import           Crypto.Hash.SHA256 (hash)


import Data.Coerce
import Unsafe.Coerce
import Data.Proxy
import Type.Reflection hiding (Module)
import Data.Map (Map)
import qualified Data.Map.Strict as Map
import Data.Text (Text)
import qualified Data.Text as T
import Control.Monad.State
import Data.String ()
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import GHC.Generics (Generic)
import Text.PrettyPrint.Leijen (text, encloseSep, lbracket, rbracket, space, comma)
import Language.Marlowe.Semantics ()
import Language.Marlowe.Semantics.Types 
     --POSIXTime(..),  <<= if POSIXTime is ever implememted - import this 
    ( Money
    , ChosenNum
    , ivTo
    , ivFrom
    , Payee(..)
      )
import Language.Faustus.Syntax
    ( AbsArg(..)
    , AbsParam(..)
    , AbsToken(..)
    , AbsChoice(..)
    , AbsPayee(..)
    , AbsParty(..)
    , AbsBound(..)
    , AbsAction(..)
    , AbsExpr(..)
    , AbsCase(..)
    , AbsStmt(..)
    , AbsContract(..)
    , AbsDecl(..)
    , AbsIdentifier
    , AbsDotIdentifier
    , absParamToContext
    , absParamsToContext
    )
import Language.Faustus.Semantics.Types
    ( FaustusPurityType(..)
    , FaustusPrimType(..)
    , FaustusArrowType(..)
    , FaustusFullType(..)
    , Context
    )
import Language.Faustus.TypeChecker 
    ( TypeDerivation(..)
    , TyRuleParty(..)
    , TyRuleToken(..)
    , TyRuleBound(..)
    , TyRuleChoice(..)
    , TyRuleCont(..)
    , TyRuleStmt(..)
    , TyRuleCase(..)
    , TyRuleExpr(..)
    , TyRuleAction(..)
    , TyRuleDecl(..)
    , TyRuleArg(..)
    , TyRuleDotIdentifier(..)
    , TyRuleIdentifier(..)
    , TyRuleTuple(..)
    , getConcl
    , TyRuleGuardedContractExpression(..)
    )
import Language.Faustus.Compiler hiding (newLoc) 
import Language.Experimental.Named
import Language.Experimental.Target

-- | These should be real POSIXTimes (not Integers)as they are in Marlowe - but making them Integers here to match other Faustus code.
type POSIXTime = Integer
getPOSIXTime :: POSIXTime -> Integer
getPOSIXTime = id 

{- **** Here is a suitable real definition of POSIXTime.
-- newtype POSIXTime = POSIXTime Integer
-- getPOSIXTime :: POSIXTime -> POSIXTime {getPOSIXTime :: Integer}
-- getPOSIXTime = getPOSIXTime
--
-- instance Eq POSIXTime where p1 == p2 = getPOSIXTime p1 == getPOSIXTime p1  
-- instance Show POSIXTime where show = show . getPOSIXTime
-- instance Ord POSIXTime where p1 <= p2 = getPOSIXTime p1 <= getPOSIXTime p2
-- instance Num POSIXTime where 
--     p1 + p2  = POSIXTime $ getPOSIXTime p1 + getPOSIXTime p2
--     p1 * p2  = POSIXTime $ getPOSIXTime p1 * getPOSIXTime p2
--     abs  = POSIXTime . abs . getPOSIXTime
--     fromInteger = POSIXTime
--     signum = POSIXTime . signum . getPOSIXTime
--     negate = POSIXTime . negate . getPOSIXTime
-}

type TimeInterval = (Integer, Integer)   -- <== should be (POSIXTime, POSIXTime)
-- instance Functor TimeInterval where fmap f (TimeInterval (p1, p2)) = TimeInterval $ fmap f (p1, p2)
-- instance Eq TimeInterval where t1 ==  t2 = coerce t1 == coerce t2
-- instance Show TimeInterval where show (TimeInterval interval)  = show "TimeInterval " ++ show interval
-- instance Ord TimeInterval where (TimeInterval (t1,t2)) <= (TimeInterval (t3,t4)) = t1 < t3 || t1 == t3 && t2 <= t4

type Identifier = AbsIdentifier
type DotIdentifier = AbsDotIdentifier


-- * Inputs

data InputContent = IDeposit AbsParty AbsParty AbsToken Money
                  | IChoice ChoiceId ChosenNum
                  | INotify
  deriving (Eq,Ord,Show,Read)

data Input = NormalInput InputContent
           | MerkleizedInput InputContent ByteString
  deriving (Eq,Ord,Show,Read)

-- |Strip off the Input constructor to get to the InputContent
getInputContent :: Input -> InputContent
getInputContent (NormalInput inputContent) = inputContent
getInputContent (MerkleizedInput inputContent _) = inputContent

-- * Faustus Identifiers

-- type Identifier = AbsIdentifier
-- type DotIdentifier = AbsDotIdentifier

-- class (IsMeaningful a, Domain a ~ (b <== a)) => a `HasMeaning` b where
  


-- * FaustState, associated Types and operations

data ChoiceId = ChoiceId AbsChoice AbsParty
  deriving (Eq,Ord,Show,Read,Generic)

data Payment = Payment AbsParty AbsPayee AbsToken Money  

  deriving (Eq, Ord, Show, Read, Generic)

type Accounts = Map (AbsParty, AbsToken) Money

data FaustusState = State { minslot      :: POSIXTime
                          , accounts     :: Accounts
                          , choices      :: Map ChoiceId ChosenNum
                          , timeInterval :: TimeInterval 
                          , payments     :: [Payment]
                          , input        :: [Input]
                          , assertions   :: [(String,Bool)]}
  deriving (Eq, Ord, Show, Generic)


-- * Epsilon (storable) types. 

-- | Epsilon @\epsilon@ is the name used in the Faustus Language Reference to refer to types that can be stored 
--    in the Faustus Store. We avoided @Storable@ just because there is another library called @Storable@ (which we don't
--    currently use). The @Epsilon@ types and the operations on them are classified by the @Epsilon@ type class.


class Epsilon a where
  storableToInteger ::  a -> Integer
  storableFromInteger :: Proxy a -> Integer -> a  
  
-- |We define the type of Epsilon types (TyEpsilon) to consolodate the Epsilon types into a single type.
--   REMARK: May not need to do this.
-- data TyEpsilon where TyEpsilon :: forall a . (Epsilon a) => (Proxy a) -> a -> TyEpsilon 
-- coerceTyEpsilon :: TyEpsilon -> a
-- coerceTyEpsilon (TyEpsilon (Proxy :: Proxy a) x) = unsafeCoerce x 

instance Epsilon Bool where
  storableToInteger b
     | b = 1
     | not b = 0
  storableFromInteger (Proxy :: Proxy Bool) k
    | k == 0 = False
    | k /= 0 = True

instance Epsilon Integer where 
  storableToInteger = id
  storableFromInteger (Proxy :: Proxy Integer) k = k

-- * This needs to be reopend once POSIXTime is its own type and not justy a type synonym for Integer.

{- instance Epsilon POSIXTime where
    storableToInteger = getPOSIXTime 
    storableFromInteger (Proxy :: Proxy POSIXTime) = POSIXTime -- {getPOSIXTime = k} 
-}

-- ** Locations, Stores, and derefencing a location.

newtype Location = Loc Integer  
    deriving (Eq, Ord, Show, Read)

-- |nextLoc holds the next unused location, store is a map from location to Store
data Store = Store  {nextLoc :: Location, store ::  Map Location Integer}
    deriving (Show,Read)

dereference :: (Epsilon a) => Proxy a -> Location -> Store -> Eval a
dereference (prxy :: Proxy a) loc sto@(Store{nextLoc = max, store = sfun}) = do
  case Map.lookup loc sfun of
    Just k -> pure $ storableFromInteger prxy k
    Nothing -> Raise $ "dereference: bad location " ++ show loc ++ ", maximum location in store is " ++ show max ++ "."

-- |newLoc allocates a new location in the store, initializes it to some  value
newLoc :: (Epsilon a) => Store -> a -> (Location, Store)
newLoc s@(Store {nextLoc, store = sto}) val = 
    let new = incLoc nextLoc in
    (new, Store {nextLoc = new, store = Map.insert nextLoc (storableToInteger val) sto })  
        where incLoc (Loc k) = Loc (k + 1) 

-- * Eval type 

-- |Return type for evaluators.
data Eval a =  Eval a | Raise String

instance Functor Eval where
    fmap f (Eval a) = Eval $ f a
    fmap f (Raise s) = Raise s

instance Applicative Eval  where
    pure a = Eval a
    (Eval f) <*> (Raise s) = Raise s
    (Eval f) <*> (Eval a) = Eval $ f a
 
instance Monad Eval where
    (Raise s) >>= f = Raise s
    (Eval x) >>= f = f x

-- data TypedEval a b = TypedEval (Proxy a) (Either String a) 

-- * Domains, Meanings, and Environments

-- ** First we define types a single constructor.
data Party 
data Token 
data Bound 
data Choice 
newtype Epsilon a => EpsilonVar a = EpsilonVar a
newtype Epsilon a => EpsilonFun a = EpsilonFun a 
newtype Epsilon a => Expr a = Expr a 
data GuardedExpression 
data Action 
data Contract 
data Case 
data Stmt 
data Module 
data Decl
newtype IsArgType a => Argument a = Argument a 
data Param 
data Theta 
data Tau 
data Tuple
data Arrow 

-- * Faustus Argument Types.  These are the types that are allowed to be passed as arguments to parameterized abstractions.

-- |characterizes types which may be arguments to parameterized abstractions.
class IsArgType a 
instance IsArgType DotIdentifier
instance IsArgType Party
instance IsArgType Choice
instance IsArgType Token
instance IsArgType Bound
instance (Epsilon a) => IsArgType (EpsilonVar a)
instance (Epsilon a) => IsArgType (EpsilonFun a)
instance (Epsilon a) => IsArgType (Expr a)
instance IsArgType Action
instance IsArgType Contract 
instance IsArgType Stmt
instance IsArgType Module
instance IsArgType Case
instance IsArgType Arrow
instance IsArgType Tuple

-- * the IsMeaningful type class

class IsMeaningful a where
  type Rule a :: c
  type Domain a = b | b -> a  

instance IsMeaningful Param where  
  type Rule Param = TyRuleDecl 
  type Domain Param = AbsParam <== Param

instance forall a . IsArgType a => IsMeaningful (Argument a)  where
  type Rule (Argument a) = TyRuleArg
  type Domain (Argument a) = Env <== Argument a 

instance IsMeaningful Tuple where
    type Rule Tuple = TyRuleTuple
    type Domain Tuple = Env <== Tuple 

instance IsMeaningful Party  where
  type Rule Party = TyRuleParty
  type Domain Party = AbsParty <== Party 

instance IsMeaningful Token  where
  type Rule Token = TyRuleToken
  type Domain Token = AbsToken <== Token 

instance IsMeaningful Bound where
  type Rule Bound = TyRuleBound
  type Domain Bound = AbsBound <== Bound 

instance IsMeaningful Choice where
  type Rule Choice = TyRuleChoice
  type Domain Choice = AbsChoice <== Choice 

-- instance IsMeaningful (Expr Bool) where
--   type Rule (Expr Bool) = TyRuleExpr
--   type Domain (Expr Bool) =  Bool <== (Expr Bool)

instance  forall a . (Epsilon a) => IsMeaningful (Expr a) where
  type Rule (Expr a) = TyRuleExpr 
  type Domain (Expr a) = a <== Expr a 

instance IsMeaningful Integer where
  type Rule Integer = ()
  type Domain Integer = Integer <== Integer
  
instance forall a . (Epsilon a) => IsMeaningful (EpsilonVar a) where
  type Rule (EpsilonVar a)  = TyRuleDecl {-VarDeclRule -}
  type Domain (EpsilonVar a) = (Location, Store, a) <== EpsilonVar a 

instance forall a . (Epsilon a) => IsMeaningful (EpsilonFun a) where
  type Rule (EpsilonFun a) = TyRuleDecl {-FunDeclRule -}
  type Domain (EpsilonFun a) = ([AbsParam], Env, FaustusState -> Store -> a) <== EpsilonFun a 

instance IsMeaningful Action where
  type Rule Action = TyRuleAction
  type Domain Action = (FaustusState -> Store -> (Bool, FaustusState)) <== Action 
  
instance IsMeaningful GuardedExpression  where -- <======= TODO  Make this real!
  type Rule GuardedExpression = TyRuleGuardedContractExpression 
  type Domain GuardedExpression = () <== GuardedExpression 

type FunForm a = FaustusState -> Env -> Store -> a

instance IsMeaningful Contract  where 
  type Rule Contract = TyRuleCont
  type Domain Contract = (FaustusState -> Env -> Store ->  (FaustusState, Env, Store)) <==  Contract 

instance IsMeaningful Case where 
  type Rule Case = TyRuleCase
  type Domain Case = (FaustusState -> Env -> Store -> (FaustusState, Env, Store)) <== Case 

instance IsMeaningful Stmt  where
  type Rule Stmt = TyRuleStmt
  type Domain Stmt = (FaustusState -> Env -> Store ->  (FaustusState, Env, Store)) <== Stmt 

instance IsMeaningful [Stmt]  where
  type Rule [Stmt] = [TyRuleStmt]
  type Domain [Stmt] = (FaustusState -> Env -> Store ->  (FaustusState, Env, Store)) <== [Stmt] 

instance IsMeaningful Module where  
  type Rule Module  = TyRuleDecl
  {- ModuleRule, ModulePartamRule, ModuleOpenCallRule, ModuleOpenRule -}
  type Domain Module = Env <== Module

instance IsMeaningful Decl where
  type Rule Decl = TyRuleDecl
  type Domain Decl = (FaustusState -> Env -> Store -> (FaustusState, Env, Store)) <== Decl

instance IsMeaningful [Decl] where
  type Rule [Decl] = [TyRuleDecl]
  type Domain [Decl] = (FaustusState -> Env -> Store -> (FaustusState, Env, Store)) <== [Decl]

instance IsMeaningful Arrow  where -- <============TODO - make this right. 
  type Rule Arrow = TyRuleArg 
  type Domain Arrow = Named Arrow ([FaustusFullType], FaustusPrimType)

-- The infix constraint (==>>) captures a common constraint idiom that @a@ is meaningful and
--   that its meaning is @b@.  N.B. requires ConstraintKinds language extension.
--}
type a ==>> b = (IsMeaningful a, Domain a ~ (b <== a))

-- |consolidate the elements of all Meaningful types into a single type to make it into the range 
--    of the environment (@Env@)  @Map@.  
data Meaning where Meaning :: forall a b . a ==>> b => b -> Meaning 

-- -- |escape from Meaning using the function @the@ from the IsMeaningful instance of the @The@ type class.  
-- meaningToDomain ::  forall a b . a ==>> b => Meaning -> b
-- meaningToDomain mng = case mng of (Meaning val) -> unsafeCoerce @Meaning @b val 

-- ** Environments together with some functions on them.

-- | the type of environments.
type Env  =  Map Identifier Meaning

emptyEnv :: Env
emptyEnv = Map.empty

singleton :: Identifier -> Meaning -> Env
singleton ident meaning = Map.insert ident meaning emptyEnv

unionEnv :: Env -> Env -> Env
unionEnv = Map.union

-- type family LookupEnv :: Proxy a -> Identifier -> Env -> Eval (Domain a) 
  
-- | Recall Eval a id defined to be a type synonmy for Either String a 
maybeToEval:: Maybe a -> Eval a  
maybeToEval (Just x) = return x
maybeToEval _ = Raise "maybeToEval: Hmm. Impossible case."


data Lookup b = Lookup { getId :: Identifier
                       , context :: Context
                       , fType :: FaustusFullType
                       , environment :: Env
                       , meaning :: b}   

getLookup :: forall a b . (a ==>> b) => 
    DotIdentifier -> Context -> Env -> Eval (Lookup b)
getLookup [ident] ctx env = do
    case Map.lookup ident ctx of 
        Just ty -> do
            mng <- meaningOf @a @b [ident] env
            return $ Lookup ident ctx ty env mng
        _ -> Raise "getLookup: Final identifier not in context or env"
getLookup (ident:dotId) ctx env = do
    case Map.lookup ident ctx of
        Just (Primitive (TyModule newCtx)) ->  do
            newEnv <- meaningOf @Module [ident] env
            getLookup @a @b dotId newCtx newEnv
        _ -> Raise $ "getLookup: bad dot identifer, id:" ++ show (ident:dotId) ++ " not found."
getLookup [] _ _ = Raise "getLookup: Empty dot identifier."

-- |lookup a simple identifier in an environment. 
lookupEnv :: forall a b. a ==>> b => Identifier -> Env -> b 
lookupEnv id env = case (Map.lookup id env) of
    (Just (Meaning x)) ->  unsafeCoerce x
    _ -> error "lookupEnv:  Impossible case.  Ruled out by the Faustus typechecker."

-- |Reify the Meaning of a dot identifier with respect to an Environment. 
meaningOf :: forall a b . a ==>> b =>  DotIdentifier -> Env -> Eval b
meaningOf [id] env = return $ lookupEnv @a id env
meaningOf (id:dotId) env = do
    let newEnv = lookupEnv @Module id env
    meaningOf @a dotId newEnv
meaningOf [] _ = Raise "meaningOf: (IMPOSSIBLE) empty dotIdentifier."

-- * Otherwise useful functions.

-- |Reify the FaustusFullType of a dotIdentifier by *chasing* down the type in a Context.
chase :: DotIdentifier -> Context -> Eval FaustusFullType
chase [] ctx =  Raise "chase: Not found. Empty dot identifier."
chase [id] ctx = case Map.lookup id ctx of
                        Just ty -> return ty
                        Nothing -> Raise "chase: Not found!" 
chase (id:ids) ctx = case Map.lookup id ctx of
                        Just (Primitive (TyModule newContext)) -> chase ids newContext
                        _ -> Raise $ "chase: Not found. Looking for Module " ++ show id ++ "."

-- ** Some elementaty functions on the FaustusState.

timeIntervalStart, timeIntervalEnd :: FaustusState -> Integer -- <<=should be POSIXTime
timeIntervalStart state = fst $ timeInterval state
timeIntervalEnd state = snd $ timeInterval state

-- | A function, given a PosixTime that evaluates to an empty state. 
emptyState :: POSIXTime -> POSIXTime ->  FaustusState
emptyState t1 t2 = State { minslot = t1
                         , accounts = Map.empty
                         , choices = Map.empty
                         , timeInterval = (t1, t2)
                         , payments = []
                         , input = []
                         , assertions = [] }

-- | Close all accounts.
closeAllAccounts :: FaustusState -> FaustusState  -- TODO <<- make this right
closeAllAccounts state = state

-- ** Functions of the FaustusState.
moneyInAccount :: AbsParty -> AbsToken -> FaustusState -> Money
moneyInAccount party token state =
    Map.findWithDefault 0 (party, token) (accounts state) 

updateMoneyInAccount :: AbsParty -> AbsToken -> Money -> FaustusState -> Eval FaustusState
updateMoneyInAccount party token money state = do
    if money <= 0 then 
        return $ state {accounts = Map.delete (party, token) (accounts state)} 
    else return $ state {accounts = Map.insert (party, token) money (accounts state)}

{- | Add the given amount of money to an account (only if it is positive). 
     Return the FaustusState where the accounts field has been appropriately updated.
     -}
addMoneyToAccount :: AbsParty -> AbsToken -> Money -> FaustusState -> Eval FaustusState
addMoneyToAccount party token money state =
    if money <= 0 then return state
    else do
        let newBalance = money + moneyInAccount party token state
        updateMoneyInAccount party token newBalance state

-----------------------------------------------------------------------------
-- * Evaluators - one for each Meaningful type.
-----------------------------------------------------------------------------

evalParty :: forall a . Party ==>> a => Rule Party -> Env -> Eval a
evalParty (PubKeyRule _ _ p) env =  return (PubKeyParty p) 
evalParty (RoleRule _ _ p) env = return (RoleParty p)
evalParty (PartyUseRule _ _ dotId) env = meaningOf @Party dotId env

evalToken :: forall a . Token ==>> a => Rule Token -> Env -> Eval a
evalToken (ConstantTokenRule ctx syntax s1 s2) env = return $ ConstantToken s1 s2 
evalToken (SingleTokenRule _ token s) env = return $ SingleToken s
evalToken (TokenUseRule _ _  dotId) env = meaningOf @Token dotId env

evalChoice :: forall a . Choice ==>> a  => Rule Choice -> Env -> Eval a
evalChoice (ConstantChoiceRule _ c _)  env = return c
evalChoice (ChoiceUseRule _ _ dotId) env = meaningOf @Choice dotId env 

evalBound ::  forall a . (Bound ==>> a) => Rule Bound -> Env -> Eval a
evalBound (RangeRule _ b _ _) env = return b 
evalBound (SingleValueRule _ b _) env = return b
evalBound (BoundUseRule _ _ dotId) env = meaningOf @Bound dotId env 



-- instance The (Bool <== a) Bool

evalExpr :: forall a b . (Epsilon a, (Expr a) ==>> b) => 
    Rule (Expr a) ->  FaustusState ->  Env -> Store -> Eval a
evalExpr (TrueRule ctx syntax)  state env sto = return $ unsafeCoerce True 
evalExpr (FalseRule ctx syntax)  state env sto = return $ unsafeCoerce False
evalExpr (StartRule ctx syntax)  state env sto = return . unsafeCoerce $ timeIntervalStart state  
evalExpr (EndRule ctx syntax)  state env sto = return . unsafeCoerce $ timeIntervalEnd state
evalExpr (IntRule ctx syntax ident)  state env sto = return $ unsafeCoerce ident
evalExpr (PosixRule ctx syntax ident)  state env sto = return . unsafeCoerce $ storableFromInteger (Proxy :: Proxy POSIXTime) ident 
evalExpr (CondRule ctx syntax condExpr e1 e2)  state env sto = undefined
  -- do
  --   b <- evalExpr condExpr state env sto 
  --   case  the b of
  --        True  -> evalExpr e1  state env sto 
  --        False -> evalExpr e2  state env sto
evalExpr (AccountBalRule ctx syntax prule trule)  state env sto = do
    party <- evalParty prule env
    token <- evalToken trule env
    return . unsafeCoerce $ moneyInAccount party token state
evalExpr (NumOpRule ctx syntax l r) state env sto = undefined
  -- do
  --   lval <- evalExpr l state env sto 
  --   rval <- evalExpr r state env sto
  --   case syntax of
  --       (AddExpr _ _)-> return (lval + rval)
  --       (SubExpr _ _) -> return (lval - rval) 
  --       (MulExpr _ _) -> return (lval * rval)
  --       (DivExpr _ _) -> return (lval `div` rval)
  --       _ -> Raise  "evalExpr: Cannot eval non numeric operator as numeric operator."
evalExpr (NegRule ctx syntax exp)  state env sto = undefined 
--   do
--     k <- evalExpr exp state env sto
--     return . the $ (- k)
-- evalExpr (BoolOpRule ctx syntax lexp rexp)  state env stor = do
--     lval <- evalExpr lexp state env stor 
--     rval <- evalExpr rexp state env stor
--     case syntax of
--         (AndExpr _ _) -> return . the $ (lval && rval)
--         (OrExpr _ _) -> return . the $ (lval || rval)
--         _ -> Raise  "evalExpr: Cannot eval non Boolean operator as Boolean operator."
evalExpr (CompOpRule ctx syntax l r)  state env sto = undefined
  -- do
  --   lval <-  evalExpr l  state env sto
  --   rval <- evalExpr r  state env sto
  --   case syntax of
  --       LTExpr _ _ -> return . the $ lval < rval
  --       LEExpr _ _ -> return . the $ lval <= rval
  --       GTExpr _ _ -> return . the $ lval > rval
  --       EqExpr _ _ -> return . the $ lval == rval
  --       GEExpr _ _ -> return . the $ lval >= rval
  --       NEqExpr _ _ -> return . the $  lval /= rval
  --       _ -> Raise "evalExpr: Cannot eval non comparison operator as comparison operator."
evalExpr (NotRule ctx syntax bexp)  state env sto = undefined
  -- do
  --   b <- evalExpr bexp state env sto
  --   return . the $ not b
evalExpr (ChoiceExprRule ctx syntax party choice)  state env sto = do
    p <- evalParty party env
    ch <- evalChoice choice env
    case Map.lookup (ChoiceId ch p) (choices state) of
      Just ch -> return $ unsafeCoerce ch
      Nothing -> Raise "evalExpr: unfound choice." 
evalExpr (ChosenExprRule ctx syntax p c)  state env sto = do
    party <- evalParty p env
    choiceName <- evalChoice c env
    case Map.lookup (ChoiceId choiceName party) (choices state) of
      Just choice -> return $ unsafeCoerce choice
      Nothing -> Raise "evalExpr: Choice not found."
-- evalExpr rule@(ExprUseRule ctx syntax dotId)  state env sto = do
--     (Meaning mng) <- meaningOf @b dotId env 
--     return . unsafeCoerce mng
evalExpr fRule@(FunRule _ (CallExpr funId _) dotId argRule@(TupleRule _ args argRules))  state env sto = undefined 
-- do
--     let (ctx, _ , Primitive resTy) = getConcl fRule
--     newEnv <- evalArgs argRule dotId state env sto  
--     case resTy  of
--       (TyBool _) -> undefined
        
        -- do
        --   (params,f) <- meaningOf @a dotId env 
        --   return $ f state env sto


--  dotId env   
--       (TyInt _)  -> do 
--         Meaning (params, f) <- meaningOf dotId env
--         return . unsafeCoerce @Bool


    -- return $ f state (unionEnv env2 env) sto

-- | Combine the Environments for the Args in a Tuple using unionEnv.
evalArgs :: forall b . Tuple ==>> b => Rule Tuple-> DotIdentifier -> FaustusState -> Env -> Store -> Eval b
evalArgs (TupleRule _ [] []) _ _ _ _  = return $ unsafeCoerce emptyEnv
evalArgs (TupleRule ctx [arg] [argRule]) dotId state env sto = undefined
  -- do
  --   -- let (_, _ , _ ) = getConcl argRule
  --   (Lookup ident newCtx ty newEnv mng) <- getLookup @Tuple dotId ctx env
  --   x <- evalArg argRule ident state newEnv sto
  --   return . the $ singleton ident x  
-- evalArgs (TupleRule ctx (arg:args) (argRule:argRules)) dotId state env sto = do
--     let 
--     env1 <- evalArg argRule dotId arg state env sto
--     env2 <- evalArgs argRules dotId state env sto
--     return $ unionEnv env2 env1
evalArgs _ _ _ _ _  = Raise "evalArgs: Impossible case - mismatched number of Params and TyArgRules."

evalArg :: forall a b . (IsArgType a, Argument a ==>> b) => Rule (Argument a) -> Identifier -> FaustusState -> Env -> Store -> Eval b
evalArg (PartyArgRule ctx absArg party) ident state env sto = do
    p <- evalParty party env
    return . unsafeCoerce $ singleton ident (Meaning @Party p)
evalArg (TokenArgRule _ _ token) ident state env sto = do
    t <- evalToken token env 
    return . unsafeCoerce $ singleton ident (Meaning @Token t)
evalArg (ChoiceArgRule _ _ choice) ident state env sto = do
    ch <- evalChoice choice env 
    return . unsafeCoerce $ singleton ident (Meaning @Choice ch)
evalArg (BoundArgRule _ _ bound) ident state env sto = do
    bnd <- evalBound bound env
    return . unsafeCoerce $ singleton ident (Meaning @Bound bnd)
evalArg (ExprArgRule _ _  expr) ident state env sto = undefined 
-- do
--     e <- evalExpr expr state env sto
--     return . the $ singleton ident (Meaning @(Expr a) e) 
-- evalArg (ContArgRule _ _ contractRule) ident state env sto = 
--   do  -- lazy eval Contract arg
--     let f s newEnv st = evalContract contractRule s (newEnv `unionEnv` env) st
--     return . the $ singleton ident f
evalArg (StmtArgRule _ _  [stmt]) ident state env sto = do
    f <- evalStmts [stmt] state env sto
    let mng =  Meaning @[Stmt] (\s newEnv st -> f s (unionEnv newEnv env) st)
    return . unsafeCoerce $ (singleton ident mng) 
evalArg (ModuleArgRule ctx syntax mCtx [decl]) ident state env sto = undefined 
  -- do
    -- declMng <- evalDecls [decl] state env sto
    -- return . singleton ident $ the (Meaning @Decl (id, declMng)) 
evalArg  (CaseArgRule ctx syntax c) ident state env sto = undefined
  -- do
  -- singleton ident <$> evalCase c state env sto
evalArg  (ActionArgRule ctx syntax a) ident state env sto = undefined
  -- do  
  --   mng <- evalAction a state env sto
  --   return . the $ singleton ident mng
evalArg  (ArrowArgRule ctx syntax oldId) ident state env sto = undefined
  -- do
  --   Arrow (TyFun paramTys resTy) <- chase [ident] ctx
  --   case resTy of
  --       TyContract -> singleton ident <$> meaningOf @Contract oldId env
  --       TyStatement -> singleton ident <$> meaningOf @Stmt oldId env
  --       TyCase -> singleton ident <$> meaningOf @Case oldId env
  --       TyBound -> singleton ident <$> meaningOf @Bound oldId env
  --       TyAction -> singleton ident <$> meaningOf @Action oldId env
  --       TyInt _ -> singleton ident <$> meaningOf @Integer oldId env
  --       TyBool _ -> singleton ident <$> meaningOf @Bool oldId env
  --       TyModule _ -> singleton ident <$> meaningOf @Module oldId env
  --       _ -> Raise "evalArg: Cannot eval argument as function type."


-- | @ toGround @ takes 
toGround :: forall a b c . (a ==>> b, Target b ~ c, b ~ (FaustusState -> Env -> Store -> c)) 
    => Eval b -> FaustusState -> Env -> Store -> Eval c
toGround evalMe state env sto = do
    f <- evalMe 
    return $ f state env sto


evalContract :: forall a . Contract ==>> a  => Rule Contract -> FaustusState -> Env-> Store -> Eval a
evalContract (CloseRule _ _) state env sto = return . unsafeCoerce $ (\ _ _ _ -> (closeAllAccounts state, env, sto))
evalContract (DeclRule ctx syntax decl contract) state env sto = do
    (state', env', sto') <- toGround @Decl (evalDecl decl state env sto) state env sto
    f <- evalContract contract state' env'  sto'
    return . unsafeCoerce $ Meaning @Contract f
    

evalContract (StmtRule ctx c stmt contRule) state env sto = pure (\s e st -> (state, env, sto))
evalContract (WhenRule ctx c cases expr contRule) state env sto = pure (\s e st -> (state, env, sto))
evalContract (IfRule ctx c expr c1 c2) state env sto = pure (\s e st -> (state, env, sto))
evalContract (ContCallRule ctx c dotId tupleRule) state env sto = pure (\s e st -> (state, env, sto))  -- X(a1, .. an)
evalContract (ContUseRule ctx c dotId) state env sto = pure (\s e st -> (state, env, sto))

pays :: AbsParty -> AbsPayee -> Money -> Eval FaustusState
pays party payee money = undefined

pay :: AbsParty -> AbsPayee -> Token -> Integer -> FaustusState
pay _ _ _ _ = undefined



evalStmt :: forall a b . Stmt ==>> b => Rule Stmt -> FaustusState -> Env -> Store -> Eval (Target b)
evalStmt (PayInternalRule ctx syntax pRule1 pRule2 exprRule tokRule) state env sto = undefined 
-- do 
--     toParty <- evalParty pRule1 env
--     fromParty <- evalParty pRule2 env
--     amount <- evalExpr (_ :: Proxy Integer) exprRule state env sto
--     token <- evalToken tokRule env
--     return $ pay toParty (Account fromParty) token amount

evalStmt (PayExternalRule ctx syntax p1 p2 e t) state env sto = undefined
  -- do
--     dP1 <- evalParty p1 env
--     dP2 <- evalParty p2 env
--     dE <- evalExpr e state env sto
--     dT <- evalToken t env
--     case dE of
--         Right v -> return $ Pay dP1 dP2 dT v
--         _ -> error "Could not eval  expression in pay statement as value."
evalStmt (AssertRule ctx syntax e) state env sto = undefined
  -- do
--     dE <- evalExpr e
--     case E of
--         Raise obs -> return $ Assert obs
--         _ -> error "Could not eval  expression in assert statement as observation."
evalStmt (ReassignRule ctx syntax ident e) state env sto = undefined 
  -- do -- let x = 10; contract myCont (int y) = { pay x to bob from alice; close }; x := 2; myCont(x)
--     let (eCtx, eSyntax, Primitive eTy) = getConcl e
--     dE <- evalExpr e state env sto
--     case (eTy, dE) of
--         (TyBool p, b1) -> (do
--               b2 <- meaningOf castBool [i] env
--               case b of
--                 (GTExpr valId (IntExpr 0)) -> return $ Let valId (Cond b 1 0) -- TODO new location not old location
--                 _ -> do
--                     ident <- newLoc
--                     return $ Let (ValueId i) (Cond Obs 1 0))
--         (TyInt p, i1) -> (do
--               i2 <- eval meaningOf castInteger [i] env
--               case (boundVal, Val) of
--                   (valId, _) -> return $ Let valId Val -- TODO new location not old location.
--                   (_, constantVal) -> do
--                     modify (\(e, s) -> (insertEnv ident val, e, s))
--                     return $ id
--                   _ -> do
--                     ident <- newLoc
--                     return $ Let ident val)
evalStmt (StmtCallRule ctx syntax ident (TupleRule _ _ args)) state env sto = undefined
  -- do
--     (body, params, boundCtx) <- meaningOf caseStmtFun ident 
--     (argsCont, argsCtx) <- evalArgs params args
--     modify (\(e, s) -> (eval rEnvUnion ArgsCtx boundCtx, s))
--     body' <- evalStmts body state env sto
--     return (\c -> ArgsCont . body' $ c)

evalStmt (StmtUseRule ctx syntax i) state env sto = undefined 
-- eval BasicUse extractStmt i

evalStmts :: forall a . [Stmt] ==>> a => [Rule Stmt] -> FaustusState -> Env -> Store -> Eval a
evalStmts [] state env sto = return (\s e st -> (state, env, sto))
evalStmts (stmt:stmts) state env sto = do
    (newState, newEnv, newSto) <- evalStmt stmt state env sto
    stmtsEval <- evalStmts stmts newState newEnv newSto
    return stmtsEval 

evalAction :: forall a b . Action ==>> b => Rule Action -> FaustusState -> Env -> Store -> Eval b
evalAction (DepositRule ctx syntax party1 expr token party2) state env sto = undefined
evalAction (ChoosesRule ctx syntax party choice) state env sto = undefined
evalAction (ChoosesNotRule ctx syntax party choice) state env sto = undefined
evalAction (ChoosesWithinRule ctx syntax party choice [bounds]) state env sto = undefined
evalAction (NotifyRule ctx syntax expr) state env sto = undefined
evalAction (ActionCallRule ctx syntax dotId tuple) state env sto = undefined
evalAction (ActionUseRule ctx syntax dotId) state env sto = undefined

evalDecl :: forall a . Decl ==>> a  => Rule Decl -> FaustusState -> Env -> Store -> Eval a
evalDecl (PartyDeclRule ctx syntax ident partyRule) state env sto = do
    p <- evalParty partyRule env
    return $ unsafeCoerce (\s e st -> singleton ident (Meaning @Party p))
evalDecl (TokenDeclRule ctx syntax ident tok) state env sto = do
    t <- evalToken tok env
    return . unsafeCoerce $ (\s e st -> singleton ident (Meaning @Token t))
evalDecl (BoundDeclRule ctx syntax ident bRule) state env sto = do
    bnd <- evalBound bRule env
    return $ unsafeCoerce (id, singleton ident (Meaning @Bound bnd))
evalDecl (ChoiceDeclRule ctx syntax ident cRule) state env sto = do
    ch <- evalChoice cRule env
    return $ unsafeCoerce (id, singleton ident (Meaning @Choice ch))
evalDecl (VarDeclRule ctx (VarDecl _ _) ident expr) state env sto = do
    let (_, _, Primitive eTy) = getConcl expr
    case eTy of
        TyInt _ -> do
          val <- evalExpr expr state env sto
          let (loc,newSto) = newLoc sto val 
          return $ unsafeCoerce (id, singleton ident (Meaning @(EpsilonVar Integer) (loc, newSto, val)))
        TyBool _ -> do 
            val <- evalExpr expr state env sto
            let (loc,newSto) = newLoc sto val      
            return $ unsafeCoerce (id, singleton ident (Meaning @(EpsilonVar Bool) (loc, newSto, val)))
evalDecl (FunDeclRule ctx syntax ident params expr) state env sto = undefined
-- do    let (_, _, Arrow (TyFun _ eTy)) = getConcl expr
--     let f state'  = evalExpr expr state' env -- build in unsafeCoerce expr and unsafeCoerce current env. 
--     return $ unsafeCoerce (params, env, f)
evalDecl (ActionDeclRule ctx syntax ident action) state env sto = undefined
-- do    let f s  = evalAction action s env
--     return . unsafeCoerce $ singleton ident f
evalDecl (ActionParamDeclRule ctx syntax ident params action) state env sto = undefined
  -- do
  --   return $ (id, singleton ident (Meaning @Action a params env))
evalDecl (ContractDeclRule ctx syntax ident c) state env sto = undefined
  -- do
  --   let f state = evalContract c state env 
  --   return $ unsafeCoerce (id, singleton ident f)
evalDecl (ContractParamDeclRule ctx syntax ident p c) state env sto = undefined
  -- do
  --   (curEnv, _) <- get
  --   return $ (id, singleton ident  c p curEnv)
evalDecl (CaseDeclRule ctx syntax ident c) state env sto = undefined
  -- do
  --   evaldC <- evalCase c
  --   return $ (id, singleton ident (Case evaldC))
evalDecl (CaseParamDeclRule ctx syntax ident p c) state env sto = undefined
  -- do
  --   (curEnv, _) <- get
  --   return $ (id, singleton ident (CaseFun c p curEnv))
evalDecl (StatementDeclRule ctx syntax ident s) state env sto = undefined
  -- do
  --   evaldS <- evalStmts s
  --   return $ (id, singleton ident (Stmt evaldS))
evalDecl (StatementParamDeclRule ctx syntax ident p s) state env sto = undefined
  -- do
  --   (curEnv, _) <- get
  --   return $ (id, singleton ident (StmtFun s p curEnv))
evalDecl (ModuleRule ctx syntax ident moduleCtx d) state env sto = undefined
-- do
--     (evaldD, newEnv) <- evalDecls d
--     return $ (evaldD, singleton ident (Module newEnv))
evalDecl (ModuleParamRule ctx syntax ident moduleCtx p d) state env sto = undefined
-- do    (curEnv, _) <- get
--     return $ (id, singleton ident (ModuleFun d moduleCtx p curEnv))
-- evalDecl (ModuleOpenRule ctx syntax i) state env sto = undefine
-- do    mEnv <- evalBasicUse extractModule i
--     return $ (id, mEnv)
evalDecl (ModuleOpenCallRule ctx syntax ident (TupleRule _ _ args)) state env sto = undefined
-- do    (body, moduleCtx, ps, bodyEnv) <- evalBasicUse extractModuleFun i
--     (argsC, argsEnv) <- evalArgs ps args
--     modify (\(e, s) -> (evalrEnvUnion argsEnv e, s))
--     evalDecls body




evalDecls :: [Decl] ==>> a => Rule [Decl] -> [Decl] -> FaustusState -> Env -> Store -> Eval a
evalDecls [d] state env sto = undefined
--   evalDecl d state env sto
-- evalDecls (d:ds) state env sto = do
--     (evaldCont, evaldEnv) <- evalDecl d
--     modify (\(env, s) -> (evalEnvUnion evaldEnv env, s))
--     (evaldCont2, evaldEnv2) <- evalDecls ds
--     return $ (\c -> evaldCont . evaldCont2 $ c, evalrEnvUnion evaldEnv2 evaldEnv)
-- evalDecls [] _ _ = Raise  "evalDecls: Impossible case, empty decls."



