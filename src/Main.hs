{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Language.Faustus
import qualified Language.Marlowe as M
import System.Environment

main :: IO ()
main = do
    a <- getArgs
    inputCont <- getContents
    case parseToAbstractSyntax inputCont of
        Left err -> error (show err)
        Right res1 -> if "-s" `elem` a then print res1 else case fullTypeCheckCont res1 of
            Left err -> error (err)
            Right (res2, _) -> case fullCompileCont res2 of
                Left err -> error (err)
                Right (res3, _) -> if "-p" `elem` a then print . M.pretty $ res3 else print res3 --print . (if "-p" `elem` a then M.pretty else id) $ res3 --putStrLn $ "Correctly typed\n" ++ if False then (show res1) else ""
