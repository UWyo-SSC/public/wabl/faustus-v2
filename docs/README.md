# Faustus Language Reference Manual

The Faustus Language Reference Manual contains the full specification of the
Faustus Language as it is being worked on. The manual directory contains the
Latex source for building the reference manual. Building refman.tex builds the
reference manual.
